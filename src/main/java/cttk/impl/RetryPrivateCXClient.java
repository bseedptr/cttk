package cttk.impl;

import cttk.PrivateCXClient;
import cttk.TradingFeeProvider;
import cttk.dto.Balances;
import cttk.dto.BankAccount;
import cttk.dto.Transfers;
import cttk.dto.UserAccount;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrades;
import cttk.dto.Wallet;
import cttk.dto.Wallets;
import cttk.dto.WithdrawResult;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.util.RetryUtils;
import cttk.request.CancelOrderRequest;
import cttk.request.CreateMarketPriceOrderRequest;
import cttk.request.CreateOrderRequest;
import cttk.request.GetBankAccountRequest;
import cttk.request.GetOpenUserOrdersRequest;
import cttk.request.GetTransfersDeltaRequest;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserAccountRequest;
import cttk.request.GetUserOrderHistoryDeltaRequest;
import cttk.request.GetUserOrderHistoryRequest;
import cttk.request.GetUserOrderRequest;
import cttk.request.GetUserTradesDeltaRequest;
import cttk.request.GetUserTradesRequest;
import cttk.request.GetWalletRequest;
import cttk.request.WithdrawToBankRequest;
import cttk.request.WithdrawToWalletRequest;

public class RetryPrivateCXClient
    implements PrivateCXClient
{
    final long baseBackoffMillis;
    final int maxRetryCount;
    final PrivateCXClient client;
    final String nonceMessage;

    public RetryPrivateCXClient(long baseBackoffMillis, int maxRetryCount, PrivateCXClient client, String nonceMessage) {
        super();
        this.baseBackoffMillis = baseBackoffMillis;
        this.maxRetryCount = maxRetryCount;
        this.client = client;
        this.nonceMessage = nonceMessage;
    }

    @Override
    public String getCxId() {
        return client.getCxId();
    }

    @Override
    public UserAccount getUserAccount(GetUserAccountRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return RetryUtils.getDataWithRetryErrorMessage(baseBackoffMillis, maxRetryCount, () -> client.getUserAccount(request), nonceMessage);
    }

    @Override
    public TradingFeeProvider getTradingFeeProvider()
        throws CTTKException
    {
        return RetryUtils.getDataWithRetryErrorMessage(baseBackoffMillis, maxRetryCount, () -> client.getTradingFeeProvider(), nonceMessage);
    }

    @Override
    public BankAccount getBankAccount(GetBankAccountRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return RetryUtils.getDataWithRetryErrorMessage(baseBackoffMillis, maxRetryCount, () -> client.getBankAccount(request), nonceMessage);
    }

    @Override
    public WithdrawResult withdraw(WithdrawToBankRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return RetryUtils.getDataWithRetryErrorMessage(baseBackoffMillis, maxRetryCount, () -> client.withdraw(request), nonceMessage);
    }

    @Override
    public Wallet getWallet(GetWalletRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return RetryUtils.getDataWithRetryErrorMessage(baseBackoffMillis, maxRetryCount, () -> client.getWallet(request), nonceMessage);
    }

    @Override
    public Wallet getWallet(String currency)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return RetryUtils.getDataWithRetryErrorMessage(baseBackoffMillis, maxRetryCount, () -> client.getWallet(currency), nonceMessage);

    }

    @Override
    public Wallets getAllWallets()
        throws CTTKException
    {
        return RetryUtils.getDataWithRetryErrorMessage(baseBackoffMillis, maxRetryCount, () -> client.getAllWallets(), nonceMessage);

    }

    @Override
    public WithdrawResult withdraw(WithdrawToWalletRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return RetryUtils.getDataWithRetryErrorMessage(baseBackoffMillis, maxRetryCount, () -> client.withdraw(request), nonceMessage);

    }

    @Override
    public Balances getAllBalances()
        throws CTTKException
    {
        return RetryUtils.getDataWithRetryErrorMessage(baseBackoffMillis, maxRetryCount, () -> client.getAllBalances(), nonceMessage);
    }

    @Override
    public UserOrder createOrder(CreateOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return RetryUtils.getDataWithRetryErrorMessage(baseBackoffMillis, maxRetryCount, () -> client.createOrder(request), nonceMessage);

    }

    @Override
    public UserOrder createMarketPriceOrder(CreateMarketPriceOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return RetryUtils.getDataWithRetryErrorMessage(baseBackoffMillis, maxRetryCount, () -> client.createMarketPriceOrder(request), nonceMessage);

    }

    @Override
    public void cancelOrder(CancelOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        RetryUtils.getDataWithRetryErrorMessage(baseBackoffMillis, maxRetryCount, () -> {
            client.cancelOrder(request);
            return null;
        }, nonceMessage);

    }

    @Override
    public UserOrder getUserOrder(GetUserOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return RetryUtils.getDataWithRetryErrorMessage(baseBackoffMillis, maxRetryCount, () -> client.getUserOrder(request), nonceMessage);

    }

    @Override
    public UserOrders getOpenUserOrders(GetOpenUserOrdersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return RetryUtils.getDataWithRetryErrorMessage(baseBackoffMillis, maxRetryCount, () -> client.getOpenUserOrders(request), nonceMessage);

    }

    @Override
    public UserOrders getAllOpenUserOrders(boolean withTrades)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return RetryUtils.getDataWithRetryErrorMessage(baseBackoffMillis, maxRetryCount, () -> client.getAllOpenUserOrders(withTrades), nonceMessage);

    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return RetryUtils.getDataWithRetryErrorMessage(baseBackoffMillis, maxRetryCount, () -> client.getUserOrderHistory(request), nonceMessage);

    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return RetryUtils.getDataWithRetryErrorMessage(baseBackoffMillis, maxRetryCount, () -> client.getUserOrderHistory(request), nonceMessage);
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return RetryUtils.getDataWithRetryErrorMessage(baseBackoffMillis, maxRetryCount, () -> client.getUserTrades(request), nonceMessage);
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return RetryUtils.getDataWithRetryErrorMessage(baseBackoffMillis, maxRetryCount, () -> client.getUserTrades(request), nonceMessage);
    }

    @Override
    public Transfers getTransfers(GetTransfersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return RetryUtils.getDataWithRetryErrorMessage(baseBackoffMillis, maxRetryCount, () -> client.getTransfers(request), nonceMessage);
    }

    @Override
    public Transfers getTransfers(GetTransfersDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return RetryUtils.getDataWithRetryErrorMessage(baseBackoffMillis, maxRetryCount, () -> client.getTransfers(request), nonceMessage);

    }
}