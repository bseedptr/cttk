package cttk.impl.korbit.response;

import static cttk.impl.util.DateTimeUtils.zdtFromEpochMilli;

import java.math.BigDecimal;

import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.PricingType;
import cttk.dto.UserOrder;
import cttk.impl.korbit.KorbitUtils;
import cttk.impl.util.MathUtils;
import cttk.impl.util.ZoneIds;
import lombok.Data;

@Data
public class KorbitUserOrder {
    String id;
    String currency_pair;
    String side;
    BigDecimal avg_price;
    BigDecimal price;
    BigDecimal order_amount;
    BigDecimal filled_amount;
    BigDecimal order_total;
    BigDecimal filled_total;
    String created_at;
    String last_filled_at;
    String status;
    BigDecimal fee;

    public UserOrder toUserOrder() {
        if (id == null) {
            return null;
        }
        return new UserOrder()
            .setCurrencyPair(KorbitUtils.parseMarketSymbol(currency_pair))
            .setOid(id)
            .setDateTime(zdtFromEpochMilli(Long.parseLong(created_at), ZoneIds.SEOUL))
            .setSide(OrderSide.of(side))
            .setStatus(status)
            .setStatusType(getStatusType())
            .setPricingType(getPricingType())
            .setPrice(price)
            .setQuantity(order_amount)
            .setFilledQuantity(filled_amount)
            .setRemainingQuantity(MathUtils.subtract(order_amount, filled_amount))
            .setTotal(order_total)
            .setFee(fee);
    }

    private OrderStatus getStatusType() {
        if (status == null) return null;
        switch (status) {
            case "unfilled":
                return OrderStatus.UNFILLED;
            case "partially_filled":
                return OrderStatus.PARTIALLY_FILLED;
            case "filled":
                return OrderStatus.FILLED;
            default:
                return null;
        }
    }

    private PricingType getPricingType() {
        return price.equals(BigDecimal.ZERO) ? PricingType.MARKET : PricingType.LIMIT;
    }
}