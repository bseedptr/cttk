package cttk.impl.bithumb.response;

import cttk.TradingFeeProvider;
import cttk.dto.UserAccount;
import cttk.impl.feeprovider.DefaultTradingFeeProvider;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BithumbUserAccountResponse
    extends BithumbDataResponse<BithumbUserAccount>
{
    public UserAccount toUserAccount() {
        return new UserAccount()
            .setAid(data.account_id)
            .setCreatedDateTime(DateTimeUtils.zdtFromEpochMilli(data.created));
    }

    public TradingFeeProvider getTradingFeeProvider() {
        return new DefaultTradingFeeProvider().setFee(data.trade_fee);
    }
}

@Data
class BithumbUserAccount {
    Long created;
    String account_id;
    String trade_fee;
    String balance;
}
