package cttk.impl.binance;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Assert;
import org.junit.Test;

import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreams;
import cttk.dto.OrderBook;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.AbstractCXStreamTest;
import cttk.request.OpenOrderBookStreamRequest;
import cttk.util.ReopenPolicy;


public class BinanceStreamTest
    extends AbstractCXStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.BINANCE;
    }

    @Override
    protected long getThreadSleepMillis() {
        return 10000l;
    }

    @Test
    public void testOrderBookStreamWithPipe()
        throws InterruptedException, Exception
    {
        threadSleep();

        final OpenOrderBookStreamRequest request = new OpenOrderBookStreamRequest()
            .setIntervalInMilli(getIntervalInMilli())
            .setCurrencyPair(getCurrencyPair());
        final CountingCXStreamListener<OrderBook> listener = new CountingCXStreamListener<>(request, 1);
        final CXStream<OrderBook> stream = CXStreams.openOrderBookSnapshotStream(getCxId(), request, listener, ReopenPolicy.createDefault().setReopenOnClosed(true));
        listener.await();
        stream.close();

        Assert.assertTrue(listener.getCount() > 0);
        Assert.assertTrue(listener.hasRequestedData());
    }
    

    @Test
    public void testInvalidCurrencyPair()
        throws InterruptedException, Exception
    {
        threadSleep();

        final OpenOrderBookStreamRequest request = new OpenOrderBookStreamRequest()
            .setIntervalInMilli(getIntervalInMilli())
            .setCurrencyPair(getInvalidCurrencyPair());
        final CountingCXStreamListener<OrderBook> listener = spy(new CountingCXStreamListener<>(request, 1));
        final CXStream<OrderBook> stream = CXStreams.openOrderBookSnapshotStream(getCxId(), request, listener, ReopenPolicy.createDefault().setReopenOnClosed(true));
        listener.await();
        stream.close();
        
        verify(listener, atLeastOnce()).onOpen(any(CXStream.class));
        verify(listener, atLeastOnce()).onFailure(any(CXStream.class), any(UnsupportedCurrencyPairException.class)); 
        verify(listener, times(0)).onMessage(any(OrderBook.class));
    }
}
