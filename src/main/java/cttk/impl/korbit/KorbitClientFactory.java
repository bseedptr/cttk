package cttk.impl.korbit;

import cttk.CXIds;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.auth.AuthTokenProviderFactory;
import cttk.auth.Credential;
import cttk.exception.CTTKException;
import cttk.impl.AbstractEMPUDSOpenerCXClientFactory;

public class KorbitClientFactory
    extends AbstractEMPUDSOpenerCXClientFactory
{
    @Override
    public PublicCXClient create() {
        return new KorbitPublicClient();
    }

    @Override
    public PrivateCXClient create(Credential credential) {
        try {
            return new KorbitPrivateClient(credential, AuthTokenProviderFactory.getInstance(CXIds.KORBIT));
        } catch (CTTKException e) {
            throw new RuntimeException(e);
        }
    }
}
