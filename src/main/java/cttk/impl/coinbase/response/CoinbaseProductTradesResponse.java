package cttk.impl.coinbase.response;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.dto.Trade;
import cttk.dto.Trades;
import cttk.impl.coinbase.CoinbaseUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CoinbaseProductTradesResponse
    extends ArrayList<CoinbaseTrade>
{
    public Trades toTrades(CurrencyPair currencyPair) {
        return new Trades()
            .setCurrencyPair(currencyPair)
            .setTrades(getTradeList());
    }

    private List<Trade> getTradeList() {
        return this.stream()
            .map(i -> i.toTrade())
            .collect(Collectors.toList());
    }
}

@Data
class CoinbaseTrade {
    String time;
    BigInteger trade_id;
    BigDecimal price;
    BigDecimal size;
    String side;

    public Trade toTrade() {
        return new Trade()
            .setDateTime(CoinbaseUtils.parseZDT(time))
            .setSno(trade_id)
            .setTid(String.valueOf(trade_id))
            .setSide(OrderSide.of(side))
            .setPrice(price)
            .setQuantity(size)
            .setTotal(price.multiply(size));
    }
}
