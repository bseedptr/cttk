package cttk.impl.binance.response;

import cttk.dto.Wallet;
import lombok.Data;
import lombok.EqualsAndHashCode;

import static cttk.impl.binance.BinanceUtils.toStdCurrencySymbol;

@Data
@EqualsAndHashCode(callSuper = true)
public class BinanceWalletResponse
    extends BinanceResultResponse
{
    String asset;
    String address;
    String addressTag;

    public Wallet getWallet() {
        return new Wallet()
            .setCurrency(toStdCurrencySymbol(asset))
            .setAddress(address)
            .setDestinationTag(addressTag);
    }
}
