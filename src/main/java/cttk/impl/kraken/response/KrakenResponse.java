package cttk.impl.kraken.response;

import java.util.List;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class KrakenResponse<T> {
    List<String> error;
    T result;

    public boolean hasError() {
        return error != null && !error.isEmpty();
    }

    public String getErrorString() {
        if (error == null) return null;
        StringBuilder sb = new StringBuilder();
        for (String e : error) {
            if (sb.length() > 0) sb.append("\n");
            sb.append(e);
        }
        return sb.toString();
    }

    public boolean isInvalidAssetPairError() {
        if (error == null || error.isEmpty()) return false;
        for (String e : error) {
            if (e.contains("Invalid asset pair") || e.contains("Unknown asset pair")) return true;
        }
        return false;
    }
}
