package cttk.impl.huobi.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HuobiUserTradeResponse
    extends HuobiResponse
{
    HuobiUserTrade data;
}
