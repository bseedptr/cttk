package cttk.impl.bithumb.response;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.PricingType;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.impl.bithumb.BithumbUtils;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BithumbUserOrdersResponse
    extends BithumbDataResponse<List<BithumbUserOrder>>
{
    public UserOrders toUserOrders(PricingType pricingType) {
        UserOrders ret = new UserOrders();
        ret.setOrders(toUserOrderList(data, pricingType));
        return ret;
    }

    private List<UserOrder> toUserOrderList(List<BithumbUserOrder> bithumbUserOrders, PricingType pricingType) {
        if (bithumbUserOrders == null) return null;
        return bithumbUserOrders.stream().map(i -> i.toUserOrder(pricingType)).filter(i -> i != null).collect(Collectors.toList());
    }
}

@Data
class BithumbUserOrder {
    String order_currency;
    String payment_currency;
    String order_id;
    Long order_date;
    String type;
    String status;
    BigDecimal price;
    BigDecimal units;
    BigDecimal units_remaining;
    BigDecimal total;
    BigDecimal fee;
    Long date_completed;

    public UserOrder toUserOrder(PricingType pricingType) {
        if (order_id == null) {
            return null;
        }

        return new UserOrder()
            .setCurrencyPair(BithumbUtils.toStdCurrencySymbol(order_currency), BithumbUtils.toStdCurrencySymbol(payment_currency))
            .setOid(order_id)
            .setDateTime(DateTimeUtils.zdtFromEpochMicro(order_date))
            .setSide(OrderSide.of(type))
            .setStatus(status)
            .setStatusType(getStatusType())
            .setPricingType(pricingType)
            .setPrice(price)
            .setQuantity(units)
            .setFilledQuantity(MathUtils.subtract(units, units_remaining))
            .setRemainingQuantity(units_remaining)
            .setTotal(MathUtils.multiply(price, units))
            .setFee(fee)
            .setCompletedDateTime(DateTimeUtils.zdtFromEpochMilli(date_completed));
    }

    private OrderStatus getStatusType() {
        if ("placed".equalsIgnoreCase(status) && units != null) {
            if (units.compareTo(units_remaining) == 0) {
                return OrderStatus.UNFILLED;
            } else if (units.compareTo(units_remaining) > 0) {
                return OrderStatus.PARTIALLY_FILLED;
            }
        }

        if (units_remaining.compareTo(BigDecimal.ZERO) == 0) {
            return OrderStatus.FILLED;
        }

        return null;
    }
}
