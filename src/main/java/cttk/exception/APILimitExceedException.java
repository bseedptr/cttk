package cttk.exception;

public class APILimitExceedException
    extends CXAPIRequestException
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public APILimitExceedException() {
        super();
    }

    public APILimitExceedException(String message, Throwable cause) {
        super(message, cause);
    }

    public APILimitExceedException(String message) {
        super(message);
    }

    public APILimitExceedException(Throwable cause) {
        super(cause);
    }

    @Override
    public APILimitExceedException setCxId(String cxId) {
        super.setCxId(cxId);
        return this;
    }

    @Override
    public APILimitExceedException setResponseStatusCode(Integer responseStatusCode) {
        super.setResponseStatusCode(responseStatusCode);
        return this;
    }

    @Override
    public APILimitExceedException setExchangeMessage(String exchangeMessage) {
        super.setExchangeMessage(exchangeMessage);
        return this;
    }
}
