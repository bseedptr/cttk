package cttk.impl.hitbtc.response;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.ZonedDateTime;

import cttk.OrderSide;
import cttk.dto.Trade;
import cttk.impl.util.MathUtils;
import lombok.Data;

@Data
public class HitbtcTradesResponse {
    BigInteger id;
    BigDecimal price;
    BigDecimal quantity;
    String side;
    String timestamp;

    public Trade toTrade() {
        return new Trade()
            .setSno(id)
            .setTid(String.valueOf(id))
            .setDateTime(ZonedDateTime.parse(timestamp))
            .setSide(OrderSide.of(side))
            .setPrice(price)
            .setQuantity(quantity)
            .setTotal(MathUtils.multiply(price, quantity));
    }
}