package cttk.impl.upbit.stream.response;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.dto.OrderBook;
import cttk.dto.OrderBook.SimpleOrder;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;

@Data
public class UpbitWebsocketOrderBookResponse {
    long timestamp;
    String code;
    BigDecimal total_ask_size;
    BigDecimal total_bid_size;
    List<UpbitOrderBookTick> orderbook_units;
    String stream_type;

    public OrderBook toOrderBook(CurrencyPair currencyPair) {
        return new OrderBook()
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(timestamp))
            .setCurrencyPair(currencyPair)
            .setAsks(toAskList(orderbook_units))
            .setBids(toBidList(orderbook_units));
    }

    private List<SimpleOrder> toAskList(List<UpbitOrderBookTick> orders) {
        if (orders == null) return null;
        return orders.stream()
            .map(i -> new SimpleOrder(i.getAsk_price(), i.getAsk_size()))
            .collect(Collectors.toList());
    }

    private List<SimpleOrder> toBidList(List<UpbitOrderBookTick> orders) {
        if (orders == null) return null;
        return orders.stream()
            .map(i -> new SimpleOrder(i.getBid_price(), i.getBid_size()))
            .collect(Collectors.toList());
    }
}

@Data
class UpbitOrderBookTick {
    BigDecimal ask_price;
    BigDecimal bid_price;
    BigDecimal ask_size;
    BigDecimal bid_size;
}