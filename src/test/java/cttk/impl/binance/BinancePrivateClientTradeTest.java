package cttk.impl.binance;

import cttk.CXIds;
import cttk.OrderSide;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.impl.AbstractPrivateCXClientTradeTest;
import cttk.test.TradeTestUtils;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

import static cttk.impl.util.Objects.nvl;
import static org.hamcrest.Matchers.*;

public class BinancePrivateClientTradeTest
    extends AbstractPrivateCXClientTradeTest
{
    @Override
    public String getCxId() {
        return CXIds.BINANCE;
    }

    @Override
    @Test
    public void testInvalidQuantityIncrementPrecision()
        throws CTTKException
    {
        threadSleep();
        testInvalidQuantityIncrementPrecisionQuantity(new BigDecimal("0.2"));
    }

    @Test
    public void testCreateOrderThrowInvalidOrderExceptionWithMessage_PRICE_FILTER() {
        // given
        // see https://support.binance.com/hc/en-us/articles/115000594711-Trading-Rule
        // ex) min tick size of btc is 0.000001, thus set the price value to 0.0000001
        final BigDecimal price = new BigDecimal("1.0000001");
        final BigDecimal quantity = new BigDecimal("0.001");

        // when
        String exchangeMessage = null;
        try {
            TradeTestUtils.testCreateCancelOrder(client, getValidCurrencyPair(), OrderSide.BUY, quantity, price);
        } catch (CTTKException e) {
            if (e instanceof CXAPIRequestException) {
                exchangeMessage = ((CXAPIRequestException) e).getExchangeMessage();
            }
        }

        // then
        Assert.assertThat(exchangeMessage, containsString("Filter failure: PRICE_FILTER"));
    }


    @Test
    public void testCreateOrderThrowInvalidOrderExceptionWithMessage_MIN_NOTIONAL() {
        // given
        final BigDecimal price = new BigDecimal("0.010098");
        final BigDecimal quantity = new BigDecimal("0.001");

        // when
        String exchangeMessage = null;
        try {
            TradeTestUtils.testCreateCancelOrder(client, getValidCurrencyPair(), OrderSide.BUY, quantity, price);
        } catch (CTTKException e) {
            if (e instanceof CXAPIRequestException) {
                exchangeMessage = ((CXAPIRequestException) e).getExchangeMessage();
            }
        }

        // then
        Assert.assertThat(exchangeMessage, containsString("Filter failure: MIN_NOTIONAL"));
    }
}
