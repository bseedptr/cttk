package cttk.impl.bithumb.response;

import static cttk.TransferType.DEPOSIT;
import static cttk.TransferType.WITHDRAWAL;
import static cttk.impl.bithumb.BithumbUtils.isTransfer;
import static cttk.impl.bithumb.BithumbUtils.toStdCurrencySymbol;
import static cttk.util.StringUtils.parseBigDecimal;
import static cttk.util.StringUtils.toUpper;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import cttk.TransferStatus;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BithumbTransfersResponse
    extends BithumbDataResponse<List<BithumbTransfer>>
{
    public Transfers toTransfers(final String currency, final String accountId) {
        return new Transfers()
            .setTransfers(toUserTransferList(toStdCurrencySymbol(currency), accountId, data));
    }

    private List<Transfer> toUserTransferList(final String currency, final String accountId, List<BithumbTransfer> bithumbTransfers) {
        if (bithumbTransfers == null) return null;
        return bithumbTransfers.stream()
            .filter(i -> isTransfer(i.getMap().get("search")))
            .map(i -> i.toTransfer(currency, accountId))
            .collect(Collectors.toList());
    }
}

@Data
class BithumbTransfer {
    Map<String, String> map = new HashMap<>();

    @JsonAnyGetter
    public Map<String, String> any() {
        return this.map;
    }

    @JsonAnySetter
    public void set(String name, String value) {
        this.map.put(name, value);
    }

    public Transfer toTransfer(String currency, String accountId) {
        BigDecimal quantity = new BigDecimal(map.get("units").replaceAll("\\s", ""));
        quantity = quantity.abs();

        final long transferDateTimeMicros = Long.parseLong(map.get("transfer_date"));

        final String status = map.get("search");
        return new Transfer()
            .setTid(String.format("%s:%s:%s", accountId, currency, transferDateTimeMicros))
            // TODO should fill transaction data
            .setCurrency(toUpper(currency))
            .setType(status.equals("4") ? DEPOSIT : WITHDRAWAL)
            .setAmount(quantity)
            .setFee(parseBigDecimal(map.get("fee")))
            .setUpdatedDateTime(DateTimeUtils.zdtFromEpochMicro(transferDateTimeMicros))
            .setCompletedDateTime(DateTimeUtils.zdtFromEpochMicro(transferDateTimeMicros))
            .setStatus(status)
            .setStatusType(getStatusType(status));
    }

    private TransferStatus getStatusType(String status) {
        switch (Integer.parseInt(status)) {
            case 3:
            case 9:
                return TransferStatus.PENDING;
            case 4:
            case 5:
                return TransferStatus.SUCCESS;
        }
        return null;
    }
}
