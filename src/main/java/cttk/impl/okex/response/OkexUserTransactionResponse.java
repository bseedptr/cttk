package cttk.impl.okex.response;

import static cttk.impl.okex.OkexUtils.guessFeeCurrency;
import static cttk.impl.okex.OkexUtilsV3.isMakerV3;
import static cttk.impl.okex.OkexUtilsV3.toStdMarketSymbolV3;
import static java.math.BigDecimal.ZERO;
import static java.util.stream.Collectors.toList;

import java.math.BigDecimal;
import java.util.ArrayList;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class OkexUserTransactionResponse
    extends ArrayList<OkexUserTransaction>
{
    public UserTrades toUserTrades() {
        return new UserTrades()
            .setTrades(stream()
                .map(OkexUserTransaction::toUserTrade)
                .collect(toList()));
    }
}

@Data
class OkexUserTransaction {
    String exec_type;
    BigDecimal fee;
    String instrument_id;
    String ledger_id;
    String order_id;
    BigDecimal price;
    String side;
    BigDecimal size;
    String timestamp;

    UserTrade toUserTrade() {
        final CurrencyPair pair = toStdMarketSymbolV3(instrument_id);
        final BigDecimal refinedPrice = price == null ? ZERO : price; // 0 if market order
        final OrderSide orderSide = OrderSide.of(side);
        final String feeCurrency = guessFeeCurrency(orderSide, pair);
        return new UserTrade()
            .setTid(ledger_id)
            .setCurrencyPair(pair)
            .setOid(order_id)
            .setDateTime(DateTimeUtils.parseZdt(timestamp))
            .setSide(orderSide)
            .setPrice(refinedPrice)
            .setQuantity(size)
            .setFee(fee)
            .setFeeCurrency(feeCurrency)
            .setTotal(MathUtils.multiply(refinedPrice, size))
            .setIsMaker(isMakerV3(exec_type));
    }
}