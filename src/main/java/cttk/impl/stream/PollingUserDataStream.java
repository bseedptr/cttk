package cttk.impl.stream;

import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.PrivateCXClient;
import cttk.UserData;
import cttk.request.OpenUserDataStreamRequest;

public class PollingUserDataStream
    implements CXStream<UserData>
{
    protected PollingUserDataThread pollingUserDataThread;

    PollingUserDataStream(final PrivateCXClient client, final OpenUserDataStreamRequest request, final CXStreamListener<UserData> listener) {
        this.pollingUserDataThread = new PollingUserDataThread(this, client, request, listener);
        open();
    }

    protected PollingUserDataStream() { }

    @Override
    public CXStream<UserData> open() {
        if (!pollingUserDataThread.isAlive()) {
            pollingUserDataThread.start();
        }
        return this;
    }

    @Override
    public void close()
        throws Exception
    {
        pollingUserDataThread.close();
    }

    @Override
    public CXStreamListener<UserData> getListener() {
        return this.pollingUserDataThread.getListener();
    }
}