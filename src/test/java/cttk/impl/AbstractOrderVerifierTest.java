package cttk.impl;

import static cttk.test.TestUtils.hasTestProfile;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BinaryOperator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import cttk.CXClientFactory;
import cttk.OrderSide;
import cttk.OrderVerificationResult;
import cttk.OrderVerifier;
import cttk.OrderVerifierFactory;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.auth.CXCredentialsProvider;
import cttk.dto.MarketInfo;
import cttk.dto.OrderBook.SimpleOrder;
import cttk.dto.UserOrder;
import cttk.exception.CTTKException;
import cttk.exception.InvalidAmountMaxException;
import cttk.exception.InvalidAmountRangeException;
import cttk.exception.InvalidPriceMaxException;
import cttk.exception.InvalidPriceRangeException;
import cttk.exception.InvalidTotalMaxException;
import cttk.exception.InvalidTotalRangeException;
import cttk.impl.util.ExNotRaisedException;
import cttk.test.OrderbookPrices;
import cttk.test.TradeTestUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
abstract public class AbstractOrderVerifierTest
    extends AbstractCXClientTest
{
    protected PublicCXClient pubClient;
    protected PrivateCXClient client;
    protected MarketInfo marketInfo;
    protected OrderVerifier orderVerifier;
    protected static final Map<AbstractOrderVerifierTest, UserOrder> userOrderMap = new ConcurrentHashMap<>();
    protected BigDecimal defaultStep = new BigDecimal("0.000000001");

    @Before
    public void setup()
        throws CTTKException, IOException
    {
        org.junit.Assume.assumeTrue(hasTestProfile("test-order-verifier"));
        pubClient = CXClientFactory.createPublicCXClient(getCxId());
        client = CXClientFactory.createPrivateCXClient(getCxId(), CXCredentialsProvider.getDefaultCredential(getCxId()).getTradeKeyOrDataKey());
        marketInfo = pubClient.getMarketInfos().getMarketInfo(getValidCurrencyPair());
        orderVerifier = OrderVerifierFactory.create(getCxId(), marketInfo);

    }

    @Override
    protected long getThreadSleepMillis() {
        return 500l;
    }

    @Test
    public void testMinPrice()
        throws CTTKException
    {
        if (marketInfo.getMinPrice() == null) {
            log.info("[" + getCxId() + "] marketInfo does not have minPrice");
            org.junit.Assume.assumeNotNull(marketInfo.getMinPrice());
        }

        BigDecimal testPrice = step(marketInfo.getMinPrice(), true, marketInfo, BigDecimal::subtract, defaultStep);
        try {
            TradeTestUtils.testCreateCancelOrder(client, getValidCurrencyPair(), OrderSide.BUY, getBuyTestQuantity(), testPrice);
        } catch (InvalidPriceMaxException | InvalidTotalMaxException e) {
            throw new CTTKException("Wrong exception {} ", e);
        } catch (InvalidPriceRangeException | InvalidTotalRangeException e) {
            log.info("Correct exception caught");
            return;
        }
        throw new ExNotRaisedException("No Exception thrown from testMinPrice");

    }

    @Ignore("Not implemented yet")
    @Test
    public void testMinTotal()
        throws CTTKException
    {
    }

    @Test
    public void testMaxPrice()
        throws CTTKException
    {
        if (marketInfo.getMaxPrice() == null) {
            log.info("[" + getCxId() + "] marketInfo does not have maxPrice");
            org.junit.Assume.assumeNotNull(marketInfo.getMaxPrice());
        }

        BigDecimal testPrice = step(marketInfo.getMaxPrice(), true, marketInfo, BigDecimal::add, defaultStep);
        try {
            TradeTestUtils.testCreateCancelOrder(client, getValidCurrencyPair(), OrderSide.SELL, getSellTestQuantity(), testPrice);
        } catch (InvalidPriceMaxException | InvalidTotalMaxException e) {
            throw new CTTKException("Wrong exception {} ", e);
        } catch (InvalidPriceRangeException | InvalidTotalRangeException e) {
            log.info("Correct exception caught");
            return;
        }
        throw new ExNotRaisedException("No Exception thrown from testMaxPrice");

    }

    @Test
    public void testMinQuantity()
        throws CTTKException
    {
        if (marketInfo.getMinQuantity() == null) {
            log.info("[" + getCxId() + "] marketInfo does not have minQuantity");
            org.junit.Assume.assumeNotNull(marketInfo.getMinQuantity());
        }

        BigDecimal testQuantity = step(marketInfo.getMinQuantity(), false, marketInfo, BigDecimal::subtract, defaultStep);
        BigDecimal testBuyPrice = TradeTestUtils.getOrderbookPrices(pubClient, getValidCurrencyPair(), orderVerifier, new BigDecimal("2")).getBuyPrice();
        try {
            TradeTestUtils.testCreateCancelOrder(client, getValidCurrencyPair(), OrderSide.BUY, testQuantity, testBuyPrice);
        } catch (InvalidAmountMaxException | InvalidTotalMaxException e) {
            throw new CTTKException("Wrong exception {} ", e);
        } catch (InvalidAmountRangeException | InvalidTotalRangeException e) {
            log.info("Correct exception caught");
            return;
        }
        throw new ExNotRaisedException("No Exception thrown from testMinQuantity");

    }

    protected BigDecimal step(BigDecimal value, boolean isPrice, MarketInfo marketInfo, final BinaryOperator<BigDecimal> func, BigDecimal defaultValue) {
        if (isPrice) {
            if (marketInfo.getPriceIncrement() != null) {
                return func.apply(value, marketInfo.getPriceIncrement());
            } else if (marketInfo.getPricePrecision() != null) {
                return func.apply(value, BigDecimal.ONE.movePointLeft(marketInfo.getPricePrecision()));
            } else {
                return func.apply(value, defaultValue);
            }
        } else {
            if (marketInfo.getQuantityIncrement() != null) {
                return func.apply(value, marketInfo.getQuantityIncrement());
            } else if (marketInfo.getQuantityPrecision() != null) {
                return func.apply(value, BigDecimal.ONE.movePointLeft(marketInfo.getQuantityPrecision()));
            } else {
                return func.apply(value, defaultValue);
            }
        }
    }

    @Test
    public void testPricePrecisionIncrement()
        throws CTTKException
    {
        if (marketInfo.getPriceIncrement() == null && marketInfo.getPricePrecision() == null) {
            log.info("[" + getCxId() + "] Both getPriceIncrement and getPricePrecision are null");
            org.junit.Assume.assumeNotNull(marketInfo.getPriceIncrement());
        }

        // Get orderbook from the exchange,
        // Make spread for both buy and sell and test out if the
        // increment or precision on marketinfo and orderverify works correctly.
        OrderbookPrices prices = TradeTestUtils.getOrderbookPrices(pubClient, getValidCurrencyPair(), orderVerifier, new BigDecimal("1.5"));
        BigDecimal sellStartPrice = prices.getSellPrice();
        BigDecimal buyStartPrice = prices.getBuyPrice();

        OrderVerificationResult result;

        // Now we iterate for each buy and sell to try testing out if the increment/precision works correctly
        for (int i = 0; i < 3; i++) {
            result = orderVerifier.verify(new SimpleOrder(sellStartPrice, getSellTestQuantity()));

            if (!result.isValid()) {
                log.error("verify result = {}", result.getReasons());
            }

            Assert.assertTrue(result.isValid());
            TradeTestUtils.testCreateCancelOrder(client, getValidCurrencyPair(), OrderSide.SELL, getSellTestQuantity(), sellStartPrice);
            sellStartPrice = step(sellStartPrice, true, marketInfo, BigDecimal::add, defaultStep);

            threadSleep();

            result = orderVerifier.verify(new SimpleOrder(buyStartPrice, getBuyTestQuantity()));

            if (!result.isValid()) {
                log.error("verify result = {}", result.getReasons());
            }

            Assert.assertTrue(result.isValid());
            TradeTestUtils.testCreateCancelOrder(client, getValidCurrencyPair(), OrderSide.BUY, getBuyTestQuantity(), buyStartPrice);
            buyStartPrice = step(buyStartPrice, true, marketInfo, BigDecimal::subtract, defaultStep);

            threadSleep();
        }
    }

    @Test
    public void testQuantityPrecisionIncrement()
        throws CTTKException
    {
        if (marketInfo.getQuantityIncrement() == null && marketInfo.getQuantityPrecision() == null) {
            log.info("[" + getCxId() + "] Both quantityIncrement and quantityPrecision are null");
            org.junit.Assume.assumeNotNull(marketInfo.getQuantityIncrement());
        }

        OrderbookPrices prices = TradeTestUtils.getOrderbookPrices(pubClient, getValidCurrencyPair(), orderVerifier, new BigDecimal("1.5"));
        BigDecimal sellPrice = prices.getSellPrice();

        OrderVerificationResult result;
        BigDecimal startQuantity;

        startQuantity = getSellTestQuantity();
        // Now we iterate for each buy and sell to try testing out if the increment/precision works correctly
        for (int i = 0; i < 5; i++) {
            result = orderVerifier.verify(new SimpleOrder(sellPrice, startQuantity));
            if (!result.isValid()) {
                log.error("verify result = {}", result.getReasons());
            }
            Assert.assertTrue(result.isValid());

            TradeTestUtils.testCreateCancelOrder(client, getValidCurrencyPair(), OrderSide.SELL, startQuantity, sellPrice);
            startQuantity = step(startQuantity, false, marketInfo, BigDecimal::add, defaultStep);

            threadSleep();
        }
    }
}
