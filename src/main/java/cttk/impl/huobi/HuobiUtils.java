package cttk.impl.huobi;

import static cttk.OrderSide.BUY;
import static cttk.util.StringUtils.toUpper;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.databind.ObjectMapper;

import cttk.CXIds;
import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.PricingType;
import cttk.ScanDirection;
import cttk.TransferType;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXAuthorityException;
import cttk.exception.InsufficientBalanceException;
import cttk.exception.InvalidOrderException;
import cttk.exception.OrderNotFoundException;
import cttk.request.CreateOrderRequest;
import cttk.util.StringUtils;

public class HuobiUtils {
    public static ObjectMapper objectMapper = new ObjectMapper();
    final static String cxId = CXIds.HUOBI;

    public static final String getMarketSymbol(CurrencyPair currencyPair) {
        return currencyPair == null ? null : String.format("%s%s",
            toHuobiCurrencySymbol(currencyPair.getBaseCurrency()),
            toHuobiCurrencySymbol(currencyPair.getQuoteCurrency()));
    }

    public static final String toHuobiCurrencySymbol(String symbol) {
        if (symbol == null) return null;
        final String lower = symbol.trim().toLowerCase();
        switch (lower) {
            case "pro":
                return "propy";
            default:
                return lower;
        }
    }

    public static final String toStdCurrencySymbol(String symbol) {
        if (symbol == null) return null;
        final String upper = symbol.trim().toUpperCase();
        switch (upper) {
            case "PROPY":
                return "PRO";
            default:
                return upper;
        }
    }

    public static final CurrencyPair parseMarketSymbol(String symbol) {
        if (symbol == null) return null;
        final String upper = toUpper(symbol);
        final int len = upper.length();
        final int idx = upper.endsWith("USDT") ? len - 4 : len - 3;
        return CurrencyPair.of(
            toStdCurrencySymbol(upper.substring(0, idx)),
            toStdCurrencySymbol(upper.substring(idx)));
    }

    public static OrderSide toOrderSide(String type) {
        if (type == null) return null;
        final String lower = StringUtils.toLower(type);
        if (lower.startsWith("sell")) {
            return OrderSide.SELL;
        } else if (lower.startsWith("buy")) {
            return OrderSide.BUY;
        } else {
            return null;
        }
    }

    public static PricingType toPricingType(String type) {
        if (type == null) return null;
        final String lower = StringUtils.toLower(type);
        if (lower.endsWith("limit")) {
            return PricingType.LIMIT;
        } else if (lower.endsWith("market")) {
            return PricingType.MARKET;
        } else {
            return null;
        }
    }

    public static String toString(OrderSide orderSide) {
        if (orderSide == null) return null;
        switch (orderSide) {
            case BUY:
                return "buy";
            case SELL:
                return "sell";
            default:
                return null;
        }
    }

    public static String toTypesParamValueString(OrderSide orderSide) {
        if (orderSide == null) return null;
        switch (orderSide) {
            case BUY:
                return "buy-market;buy-limit;buy-ioc";
            case SELL:
                return "sell-market;sell-limit;sell-ioc";
            default:
                return null;
        }
    }

    public static String toDirectString(ScanDirection scanDirection) {
        if (scanDirection == null) return null;
        switch (scanDirection) {
            case PREV:
                return "prev";
            case NEXT:
                return "next";
            default:
                return null;
        }
    }

    public static String toTransferTypeString(TransferType transferType) {
        if (transferType == null) return null;
        switch (transferType) {
            case DEPOSIT:
                return "deposit";
            case WITHDRAWAL:
                return "withdraw";
            default:
                return null;
        }
    }

    public static String toOrderBookTypeParamValue(Integer precision)
        throws CTTKException
    {
        if (precision == null) return "step0";
        if (precision < 0 || precision > 5) {
            throw new CTTKException("invalid step: step" + precision).setCxId(CXIds.HUOBI);
        } else {
            return "step" + precision;
        }
    }

    public static String guessFeeCurrency(OrderSide side, CurrencyPair pair) {
        return side == BUY ? pair.getBaseCurrency() : pair.getQuoteCurrency();
    }
    
    public static void checkErrorByResponseMessage(Object requestObj, String body, Integer statusCode)
        throws CXAPIRequestException
    {
        // statusCode is 200 even though error was raised
        Map<?, ?> map;
        try {
            map = objectMapper.readValue(body, Map.class);
        } catch (IOException e) {
            throw new CXAPIRequestException("json parse error")
                .setCxId(cxId)
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode);
        }
        if ("error".equals(map.get("status"))) {
            final String exchangeErrorCode = Objects.toString(map.get("err-code"), "");
            final String exchangeMessage = Objects.toString(map.get("err-msg"), "");
            final String message = String.format("code: %s, message: %s", exchangeErrorCode, exchangeMessage);

            if (exchangeErrorCode.contains("base-symbol-error")
                || exchangeErrorCode.contains("base-currency-error")
                || exchangeMessage.contains("invalid symbol"))
            {
                throw CXAPIRequestException.createByRequestObj(requestObj, message)
                    .setCxId(cxId)
                    .setExchangeMessage(body)
                    .setResponseStatusCode(statusCode)
                    .setExchangeErrorCode(exchangeErrorCode);
            } else if (exchangeErrorCode.contains("api-signature-not-valid")
                || exchangeErrorCode.contains("api-signature-check-failed"))
            {
                throw new CXAuthorityException(message)
                    .setCxId(cxId)
                    .setExchangeMessage(body)
                    .setResponseStatusCode(statusCode)
                    .setExchangeErrorCode(exchangeErrorCode);
            } else if (exchangeErrorCode.contains("base-record-invalid")
                || exchangeErrorCode.contains("order-orderstate-error")
                || exchangeErrorCode.contains("order-queryorder-invalid"))
            {
                throw new OrderNotFoundException(message)
                    .setCxId(cxId)
                    .setExchangeMessage(body)
                    .setResponseStatusCode(statusCode)
                    .setExchangeErrorCode(exchangeErrorCode);
            } else {
                throw new CXAPIRequestException(message)
                    .setCxId(cxId)
                    .setExchangeMessage(body)
                    .setResponseStatusCode(statusCode)
                    .setExchangeErrorCode(exchangeErrorCode);
            }
        }
        if (map.containsKey("success") && !(Boolean) map.get("success")) {
            final String exchangeErrorCode = Objects.toString(map.get("code"), "-1");
            final String exchangeMessage = Objects.toString(map.get("err-msg"), "");
            final String message = String.format("code: %s, message: %s", exchangeErrorCode, exchangeMessage);
            switch (exchangeErrorCode) {
                case "200":   // success
                    return;
                case "13406": // Not an API user
                    throw new CXAuthorityException(message)
                        .setCxId(cxId)
                        .setExchangeMessage(body)
                        .setResponseStatusCode(statusCode)
                        .setExchangeErrorCode(exchangeErrorCode);
                case "13601": // Rebalance：Creation/Redemption Suspended
                case "10404": // Invalid ETF Symbol
                    throw CXAPIRequestException.createByRequestObj(requestObj, message)
                        .setCxId(cxId)
                        .setExchangeMessage(body)
                        .setResponseStatusCode(statusCode)
                        .setExchangeErrorCode(exchangeErrorCode);
                case "13403": // Insufficient Remaining amount
                case "13404": // etf is not ready for trade
                case "13405": // etf is not available for trade due to config error
                case "13410": // Incorrect API Signature
                case "13500": // System Error
                case "13603": // Creation/Redemption Suspended – Other reasons
                case "13604": // Creation Suspended
                case "13605": // Redemption Suspended
                case "13606": // Amount incorrect. For the cases when creation amount or redemption amount is not in the range of min/max amount, this code will be returned.
                default:
                    throw new CXAPIRequestException(message)
                        .setCxId(cxId)
                        .setExchangeMessage(body)
                        .setResponseStatusCode(statusCode)
                        .setExchangeErrorCode(exchangeErrorCode);
            }
        }
    }
}
