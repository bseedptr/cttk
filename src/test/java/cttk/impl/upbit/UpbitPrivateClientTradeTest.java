package cttk.impl.upbit;

import java.math.BigDecimal;

import org.junit.Test;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.impl.AbstractPrivateCXClientTradeTest;
import cttk.impl.util.ExNotRaisedException;

public class UpbitPrivateClientTradeTest
    extends AbstractPrivateCXClientTradeTest
{
    @Override
    public String getCxId() {
        return CXIds.UPBIT;
    }

    @Test(expected = ExNotRaisedException.class)
    @Override
    public void testInvalidQuantityIncrementPrecision()
        throws CTTKException
    {
        // Please refer to CTTK-502 for detailed explanation
        // Upbit automatically truncates any values below the precision
        super.testInvalidQuantityIncrementPrecision();
    }

    @Test
    @Override
    public void testInvalidPriceIncrementPrecision()
        throws CTTKException
    {
        super.testInvalidPriceIncrementPrecisionQuantity(new BigDecimal("0.15"));
    }
}
