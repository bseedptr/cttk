package cttk.impl.hitbtc;

import static cttk.dto.UserOrders.Category.ALL;
import static cttk.test.TestUtils.hasTestProfile;

import java.io.IOException;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import cttk.CXClientFactory;
import cttk.CXIds;
import cttk.PrivateCXClient;
import cttk.auth.CXCredentialsProvider;
import cttk.dto.UserOrders;
import cttk.dto.UserTrade;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.AbstractPrivateCXClientDataTest;
import cttk.impl.PostSettingPrivateCXClient;
import cttk.request.GetUserOrderHistoryRequest;

public class HitbtcPrivateClientDataTest
    extends AbstractPrivateCXClientDataTest
{
    @AfterClass
    public static void cleanup() {
        cleanup(HitbtcPrivateClientDataTest.class);
    }

    @Override
    public String getCxId() {
        return CXIds.HITBTC;
    }

    @Before
    @Override
    public void setup()
        throws IOException, CTTKException
    {
        org.junit.Assume.assumeTrue(hasTestProfile("test-data"));
        client = CXClientFactory.createPrivateCXClient(getCxId(), CXCredentialsProvider.getDefaultCredential(getCxId()).getTradeKey());
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetAllWallets()
        throws CTTKException
    {
        super.testGetAllWallets();
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetWallet()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetWallet();
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetUserAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetUserAccount();
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetBankAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetBankAccount();
    }

    @Test
    public void testGetOrderTrades()
        throws CTTKException
    {
        final PrivateCXClient orgClient = super.client instanceof PostSettingPrivateCXClient
            ? ((PostSettingPrivateCXClient) super.client).getClient()
            : super.client;

        if (orgClient instanceof HitbtcPrivateClient) {
            final HitbtcPrivateClient hitbtcClient = (HitbtcPrivateClient) orgClient;
            final List<UserTrade> userTrades = hitbtcClient.getOrderTrades("26549975134");
            Assert.assertNotNull(userTrades);
        }
    }

    @Test
    public void testGetUserOrderHistoryCategory()
        throws CTTKException
    {
        threadSleep();

        final GetUserOrderHistoryRequest request = new GetUserOrderHistoryRequest()
            .setCurrencyPair(getValidCurrencyPair());
        final UserOrders userOrders = client.getUserOrderHistory(request);

        logger.debug("{}", userOrders);

        Assert.assertEquals(userOrders.getCategory(), ALL);
    }

}
