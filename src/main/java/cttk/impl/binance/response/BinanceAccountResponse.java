package cttk.impl.binance.response;

import static cttk.impl.binance.BinanceUtils.toStdCurrencySymbol;
import static cttk.impl.util.DateTimeUtils.zdtFromEpochMilli;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import cttk.dto.Balance;
import cttk.dto.Balances;
import cttk.dto.UserAccount;
import cttk.impl.util.MathUtils;
import lombok.Data;

@Data
public class BinanceAccountResponse {
    Integer makerCommission;
    Integer takerCommission;
    Integer buyerCommission;
    Integer sellerCommission;
    Boolean canTrade;
    Boolean canWithdraw;
    Boolean canDeposit;
    Long updateTime;
    List<BinanceBalance> balances;

    public Balances getBalances() {
        return new Balances()
            .setBalances(balances.stream()
                .map(b -> new Balance()
                    .setCurrency(toStdCurrencySymbol(b.getAsset()))
                    .setAvailable(b.getFree())
                    .setInUse(b.getLocked())
                    .setTotal(b.getTotal()))
                .filter(b -> b.getTotal().compareTo(BigDecimal.ZERO) != 0)
                .collect(Collectors.toList()));
    }

    public UserAccount getUserAccount() {
        return new UserAccount()
            .setUpdatedDateTime(zdtFromEpochMilli(updateTime))
            .setMakerCommission(makerCommission)
            .setTakerCommission(takerCommission)
            .setBuyerCommission(buyerCommission)
            .setSellerCommission(sellerCommission)
            .setCanTrade(canTrade)
            .setCanWithdraw(canWithdraw)
            .setCanDeposit(canDeposit);
    }
}

@Data
class BinanceBalance {
    String asset;
    BigDecimal free;
    BigDecimal locked;

    public BigDecimal getTotal() {
        return MathUtils.add(free, locked);
    }
}
