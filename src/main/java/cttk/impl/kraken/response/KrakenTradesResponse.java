package cttk.impl.kraken.response;

import static cttk.util.StringUtils.parseBigDecimal;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import cttk.CurrencyPair;
import cttk.PricingType;
import cttk.dto.Trade;
import cttk.dto.Trades;
import cttk.impl.kraken.KrakenUtils;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class KrakenTradesResponse
    extends KrakenResponse<KrakenTrades>
{
    public Trades getTrade(KrakenAssetPairs krakenAssetPairs, final CurrencyPair currencyPair) {
        String pair = KrakenUtils.getMarketSymbol(krakenAssetPairs, currencyPair);
        return new Trades()
            .setCurrencyPair(currencyPair)
            .setTrades(toTradeList(this.result.getMap().get(pair)))
            .setLast(this.result.getLast());
    }

    private List<Trade> toTradeList(List<List<String>> list) {
        if (list == null) return null;
        return list.stream()
            .map(l -> {
                final BigInteger sno = MathUtils.multiply(l.get(2), "10000").toBigInteger();
                return new Trade()
                    .setSno(sno)
                    .setDateTime(DateTimeUtils.zdtFromEpochMilli(new BigDecimal(l.get(2)).multiply(new BigDecimal(1000)).longValue()))
                    .setSide(KrakenUtils.parseOrderSide(l.get(3)))
                    .setPricingType(PricingType.of(l.get(4)))
                    .setPrice(parseBigDecimal(l.get(0)))
                    .setQuantity(parseBigDecimal(l.get(1)))
                    .setTotal(MathUtils.multiply(l.get(0), l.get(1)));
            })
            .collect(Collectors.toList());
    }
}

@Data
class KrakenTrades {
    // array of array entries(<price>, <volume>, <time>, <buy/sell>, <market/limit>, <miscellaneous>)
    private Map<String, List<List<String>>> map = new HashMap<>();

    // last = id to be used as since when polling for new trade data
    private Long last;

    @JsonAnyGetter
    public Map<String, List<List<String>>> any() {
        return this.map;
    }

    @JsonAnySetter
    public void set(String name, List<List<String>> value) {
        this.map.put(name, value);
    }
}
