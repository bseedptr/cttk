package cttk.dto;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import cttk.CurrencyPair;

public class MarketInfosTest {
    @Test
    public void test() {
        final MarketInfos marketInfos = new MarketInfos();
        marketInfos.setMarketInfos(Arrays.asList(
            new MarketInfo()
                .setBaseCurrency("ETH")
                .setQuoteCurrency("BTC"),
            new MarketInfo()
                .setBaseCurrency("EOS")
                .setQuoteCurrency("ETH")));

        Assert.assertEquals(2, marketInfos.size());

        Assert.assertTrue(marketInfos.contains(CurrencyPair.of("ETH", "BTC")));
        Assert.assertFalse(marketInfos.contains(CurrencyPair.of("ETH", "KRW")));

        Assert.assertArrayEquals(marketInfos.getBaseCurrencies().toArray(new String[2]), new String[] { "ETH", "EOS" });
        Assert.assertArrayEquals(marketInfos.getQuoteCurrencies().toArray(new String[2]), new String[] { "BTC", "ETH" });
    }
}
