package cttk.dto;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cttk.AbstractCXIdHolder;
import cttk.CurrencyPair;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class MarketInfos
    extends AbstractCXIdHolder<MarketInfos>
{
    List<MarketInfo> marketInfos;

    @JsonIgnore
    public List<String> getBaseCurrencies() {
        return marketInfos == null ? null : marketInfos.stream()
            .map(p -> p.getBaseCurrency())
            .distinct()
            .collect(Collectors.toList());
    }

    @JsonIgnore
    public List<String> getQuoteCurrencies() {
        return marketInfos == null ? null : marketInfos.stream()
            .map(p -> p.getQuoteCurrency())
            .distinct()
            .collect(Collectors.toList());
    }

    @JsonIgnore
    public Set<String> getAllCurrencies() {
        if (marketInfos == null) return null;
        final Set<String> set = new HashSet<>();
        set.addAll(getBaseCurrencies());
        set.addAll(getQuoteCurrencies());
        return set;
    }

    public boolean contains(CurrencyPair pair) {
        return marketInfos != null && marketInfos.stream().filter(i -> i.matched(pair)).findFirst().isPresent();
    }

    public MarketInfo getMarketInfo(CurrencyPair pair) {
        if (marketInfos == null) return null;
        final Optional<MarketInfo> op = marketInfos.stream().filter(i -> i.matched(pair)).findFirst();
        return op.isPresent() ? op.get() : null;
    }

    public int size() {
        return marketInfos == null ? 0 : marketInfos.size();
    }
}
