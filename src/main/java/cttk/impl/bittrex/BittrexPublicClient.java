package cttk.impl.bittrex;

import static cttk.impl.bittrex.BittrexUtils.getMarketSymbol;

import cttk.PublicCXClient;
import cttk.dto.MarketInfos;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Tickers;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.bittrex.response.BittrexMarketHistoryResponse;
import cttk.impl.bittrex.response.BittrexMarketSummariesResponse;
import cttk.impl.bittrex.response.BittrexMarketSummaryResponse;
import cttk.impl.bittrex.response.BittrexMarketsResponse;
import cttk.impl.bittrex.response.BittrexOrderBookResponse;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ParamsBuilder;
import cttk.request.GetOrderBookRequest;
import cttk.request.GetRecentTradesRequest;
import cttk.request.GetTickerRequest;
import cttk.request.GetTradeHistoryRequest;

public class BittrexPublicClient
    extends AbstractBittrexClient
    implements PublicCXClient
{
    @Override
    public MarketInfos getMarketInfos()
        throws CTTKException
    {
        final HttpResponse response = requestGet("/public/getmarkets");

        checkResponseError(null, response);

        return convert(response, BittrexMarketsResponse.class, i -> i.toMarketInfos());
    }

    @Override
    public Ticker getTicker(GetTickerRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = super.requestGet("/public/getmarketsummary",
            ParamsBuilder.buildIfNotNull("market", getMarketSymbol(request)));

        checkResponseError(request, response);

        return convert(response, BittrexMarketSummaryResponse.class, i -> i.toTicker());
    }

    @Override
    public Tickers getAllTickers()
        throws CTTKException
    {
        HttpResponse response = requestGet("/public/getmarketsummaries");

        checkResponseError(null, response);

        return convert(response, BittrexMarketSummariesResponse.class, i -> i.toTickers());
    }

    @Override
    public OrderBook getOrderBook(GetOrderBookRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet("/public/getorderbook",
            ParamsBuilder.buildIfNotNull("market", getMarketSymbol(request)), "&type=both");

        checkResponseError(request, response);

        return convert(response, BittrexOrderBookResponse.class, i -> i.toOrderBook(request));
    }

    @Override
    public Trades getRecentTrades(GetRecentTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = super.requestGet("/public/getmarkethistory",
            ParamsBuilder.buildIfNotNull("market", getMarketSymbol(request)));

        checkResponseError(request, response);

        return convert(response, BittrexMarketHistoryResponse.class, i -> i.toTrades(request));
    }

    @Override
    public Trades getTradeHistory(GetTradeHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }
}
