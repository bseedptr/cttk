package cttk.impl.okex.stream;

import static cttk.impl.okex.OkexUtilsV3.createSignV3;
import static java.time.ZoneOffset.UTC;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.compress.compressors.deflate64.Deflate64CompressorInputStream;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.UserData;
import cttk.auth.Credential;
import cttk.impl.okex.OkexUtils;
import cttk.impl.okex.stream.response.OkexUserDataBalanceReponse;
import cttk.impl.okex.stream.response.OkexUserDataOrderResponse;
import cttk.impl.okex.stream.response.OkexUserDataResponse;
import cttk.impl.stream.AbstractWebsocketListener;
import cttk.impl.util.HttpMethod;
import cttk.impl.util.JsonUtils;
import cttk.impl.util.OParamsBuilder;
import cttk.impl.util.ParamsBuilder;
import cttk.request.OpenUserDataStreamRequest;
import okhttp3.Response;
import okhttp3.WebSocket;
import okio.ByteString;

public class OkexUserDataWebsocketListener
    extends AbstractWebsocketListener<UserData, OpenUserDataStreamRequest>
{
    private final ObjectMapper objectMapper = JsonUtils.createDefaultObjectMapper();
    private Credential credential;

    public OkexUserDataWebsocketListener(
        final CXStream<UserData> stream,
        final CXStreamListener<UserData> listener)
    {
        super(stream,
            "userDataStream",
            null, // ignore CurrencyPair
            null, // ignore converter
            listener,
            CXIds.OKEX);
    }

    public OkexUserDataWebsocketListener setCredential(Credential credential) {
        this.credential = credential;
        return this;
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        webSocket.send(getLoginPayload());
        logger.info("[{}] Open : {}", id, channel);
        this.listener.onOpen(stream);
    }

    @Override
    public void onClosed(WebSocket webSocket, int code, String reason) {
        logger.info("[{}] Closed: - {} {} {}", id, channel, code, reason);
        this.listener.onClosed(stream, code, reason);
    }

    @Override
    protected void handleMessage(WebSocket webSocket, String text)
        throws Exception
    {
        List<UserData> userDataList = convert(text);
        if (!userDataList.isEmpty()) {
            userDataList.forEach(listener::onMessage);
        }
    }

    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        t = OkexUtils.streamCheckResponseError(t, response);
        super.onFailure(webSocket, t, response);
    }

    @Override
    protected String toString(ByteString bytes)
        throws Exception
    {
        try (final ByteArrayOutputStream out = new ByteArrayOutputStream();
            final ByteArrayInputStream in = new ByteArrayInputStream(bytes.toByteArray());
            final Deflate64CompressorInputStream zin = new Deflate64CompressorInputStream(in))
        {
            final byte[] buffer = new byte[1024];
            int offset;
            while (-1 != (offset = zin.read(buffer))) {
                out.write(buffer, 0, offset);
            }
            return out.toString();
        } catch (final IOException e) {
            throw e;
        }
    }

    private List<UserData> convert(String message)
        throws Exception
    {
        List<UserData> userDataList = new ArrayList<>();
        final String eventType = getEventType(message);

        if (eventType.equals("login")) {
            // do nothing
        } else if (eventType.endsWith("_order")) {
            OkexUserDataOrderResponse[] responses = objectMapper.readValue(message, new TypeReference<OkexUserDataOrderResponse[]>() {});
            OkexUserDataOrderResponse response = responses[0];
            userDataList.add(UserData.of(response.toUserOrder()));
            if (response.isUserTrade()) {
                userDataList.add(UserData.of(response.toUserTrade()));
            }
        } else if (eventType.endsWith("_balance")) {
            OkexUserDataBalanceReponse[] responses = objectMapper.readValue(message, new TypeReference<OkexUserDataBalanceReponse[]>() {});
            userDataList.add(UserData.of(responses[0].toBalance()));
        } else {
            throw new Exception("Unknown eventType [" + eventType + "]");
        }

        return userDataList;
    }

    private <T extends OkexUserDataResponse<?>> String getEventType(String text)
        throws IOException
    {
        T[] responses = objectMapper.readValue(text, new TypeReference<T[]>() {});
        return responses[0].getChannel();
    }

    private String getLoginPayload() {
        final String timestamp = ZonedDateTime.now(UTC).toEpochSecond() + ""; // Only UNIX seconds work correctly. Do NOT use millis or ISO format.
        final String sign = createSignV3(timestamp, HttpMethod.GET, "/users/self/verify", credential.getSecretKey());

        final Map<String, String> parameters = ParamsBuilder.create()
            .put("api_key", credential.getAccessKey())
            .put("passphrase", credential.getPassword())
            .put("sign", sign)
            .put("timestamp", timestamp)
            .build();

        final Map<String, Object> authParams = OParamsBuilder.create()
            .put("event", "login")
            .put("parameters", parameters)
            .build();

        try {
            return objectMapper.writeValueAsString(authParams);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
