package cttk.impl.huobi.stream.response;

import lombok.Data;

@Data
public class HuobiWebsocketResponse<T> {
    String ch;
    long ts;
    T tick;
}
