package cttk.example;

import java.io.IOException;
import java.util.List;

import cttk.CTTK;
import cttk.CXClientFactory;
import cttk.CurrencyPair;
import cttk.PublicCXClient;
import cttk.dto.OrderBook;
import cttk.request.GetOrderBookRequest;
import cttk.test.TestConfig;

public class GetOrderBookExample {

    public static void main(String[] args)
        throws IOException
    {
        List<String> cxIds = CTTK.getSupportedCxIds();

        for (String cxId : cxIds) {
            final TestConfig testConfig = TestConfig.readFromClasspathYaml("config/" + cxId.toLowerCase() + ".yml");
            final CurrencyPair currencyPair = testConfig.getValidCurrencyPair();

            try {
                final PublicCXClient client = CXClientFactory.createPublicCXClient(cxId);

                final OrderBook orderBook = client.getOrderBook(new GetOrderBookRequest().setCurrencyPair(currencyPair));

                Examples.print(cxId, orderBook);

            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
        }
    }

}
