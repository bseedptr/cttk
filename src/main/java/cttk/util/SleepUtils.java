package cttk.util;

import cttk.exception.CXAPIRequestException;
import cttk.impl.util.Objects;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SleepUtils {
    private static final long TIME = 350l;

    /**
     * false if error or true.
     */
    public static boolean sleep(Long millis) {
        try {
            Thread.sleep(Objects.nvl(millis, TIME));
            return true;
        } catch (InterruptedException e) {
            log.error("An unexpected error occurred on delta API.", e);
            return false;
        }
    }

    /**
     * Raise an exception if error
     */
    public static void sleepWithException(Long millis, String cxId)
        throws CXAPIRequestException
    {
        try {
            Thread.sleep(Objects.nvl(millis, TIME));
        } catch (InterruptedException e) {
            log.error("An unexpected error occurred while sleep.", e);
            throw new CXAPIRequestException("An unexpected error occurred while sleep.").setCxId(cxId);
        }
    }
}
