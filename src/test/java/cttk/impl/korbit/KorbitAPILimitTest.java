package cttk.impl.korbit;

import org.junit.Test;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.impl.AbstractAPILimitTest;

public class KorbitAPILimitTest
    extends AbstractAPILimitTest
{
    @Override
    public String getCxId() {
        return CXIds.KORBIT;
    }

    @Test
    @Override
    public void testPublicAPILimit()
        throws CTTKException, InterruptedException
    {
        testCase((args) -> {
            return publicClient.getOrderBook(getValidCurrencyPair().getBaseCurrency(),
                getValidCurrencyPair().getQuoteCurrency(), 100);
        }, false, 1000);
        // We can't reach public API limit
        logger.info("We can't reach public API limit in " + getCxId());
    }
}
