package cttk.impl.coinone;

import cttk.CXIds;
import cttk.impl.AbstractOrderVerifierTest;

public class CoinoneOrderVerifierTest
    extends AbstractOrderVerifierTest
{
    @Override
    public String getCxId() {
        return CXIds.COINONE;
    }
}
