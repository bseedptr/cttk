package cttk.impl.korbit;

import org.junit.Assert;
import org.junit.Test;

import cttk.CurrencyPair;

public class KorbitUtilsTest {
    @Test
    public void testParseMarketSymbol() {
        final CurrencyPair pair = KorbitUtils.parseMarketSymbol("xrp_krw");
        Assert.assertEquals("XRP", pair.getBaseCurrency());
        Assert.assertEquals("KRW", pair.getQuoteCurrency());
    }
}
