package cttk.util;

import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import cttk.CXIds;
import cttk.dto.Balance;
import cttk.dto.Balances;

public class BalancesDiffTest {
    @Test
    public void testSame() {
        final Balances prev = new Balances()
            .setCxId(CXIds.BINANCE)
            .setBalances(Arrays.asList(b1()));

        final Balances curr = new Balances()
            .setCxId(CXIds.BINANCE)
            .setBalances(Arrays.asList(b1()));

        final BalancesDiff diff = BalancesDiff.diff(prev, curr);
        Assert.assertFalse(diff.isChanged());
    }

    @Test
    public void testAdded() {
        final Balances prev = new Balances()
            .setCxId(CXIds.BINANCE)
            .setBalances(Arrays.asList(b1()));

        final Balances curr = new Balances()
            .setCxId(CXIds.BINANCE)
            .setBalances(Arrays.asList(b1(), b2()));

        final BalancesDiff diff = BalancesDiff.diff(prev, curr);
        Assert.assertTrue(diff.isAdded());
    }

    @Test
    public void testRemoved() {
        final Balances prev = new Balances()
            .setCxId(CXIds.BINANCE)
            .setBalances(Arrays.asList(b1()));

        final Balances curr = new Balances()
            .setCxId(CXIds.BINANCE)
            .setBalances(Arrays.asList(b2()));

        final BalancesDiff diff = BalancesDiff.diff(prev, curr);
        Assert.assertTrue(diff.isRemoved());
    }

    @Test
    public void testValueChanged() {
        final Balances prev = new Balances()
            .setCxId(CXIds.BINANCE)
            .setBalances(Arrays.asList(b1()));

        final Balances curr = new Balances()
            .setCxId(CXIds.BINANCE)
            .setBalances(Arrays.asList(b1Changed()));

        final BalancesDiff diff = BalancesDiff.diff(prev, curr);
        Assert.assertTrue(diff.isValueChanged());
    }

    private Balance b1() {
        return new Balance()
            .setCurrency("BTC")
            .setAvailable(new BigDecimal("1.2"))
            .setInUse(new BigDecimal("0.8"))
            .setTotal(new BigDecimal("2.0"));
    }

    private Balance b1Changed() {
        return new Balance()
            .setCurrency("BTC")
            .setAvailable(new BigDecimal("1.3"))
            .setInUse(new BigDecimal("0.8"))
            .setTotal(new BigDecimal("2.1"));
    }

    private Balance b2() {
        return new Balance()
            .setCurrency("ETH")
            .setAvailable(new BigDecimal("12.3"))
            .setInUse(new BigDecimal("3.8"))
            .setTotal(new BigDecimal("16.1"));
    }
}
