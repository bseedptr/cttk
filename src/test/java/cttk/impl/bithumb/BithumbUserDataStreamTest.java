package cttk.impl.bithumb;

import cttk.CXIds;
import cttk.impl.AbstractUserDataStreamTest;

public class BithumbUserDataStreamTest
    extends AbstractUserDataStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.BITHUMB;
    }
}
