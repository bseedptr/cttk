package cttk.impl.bibox;

import cttk.CXIds;
import cttk.impl.AbstractPollingCXStreamTest;

public class BiboxPollingStreamTest
    extends AbstractPollingCXStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.BIBOX;
    }

    @Override
    protected long getThreadSleepMillis() {
        return 1000l;
    }
}
