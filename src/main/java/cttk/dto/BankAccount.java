package cttk.dto;

import cttk.AbstractCXIdHolder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class BankAccount
    extends AbstractCXIdHolder<BankAccount>
{
    private String accountNo;
    private String bankCode;
    private String bankName;
    private String bankUser;
}