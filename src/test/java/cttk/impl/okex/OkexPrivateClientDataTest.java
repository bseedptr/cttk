package cttk.impl.okex;

import static cttk.test.TestUtils.hasTestProfile;

import java.io.IOException;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import cttk.CXClientFactory;
import cttk.CXIds;
import cttk.auth.CXCredentialsProvider;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.AbstractPrivateCXClientDataTest;

public class OkexPrivateClientDataTest
    extends AbstractPrivateCXClientDataTest
{
    @AfterClass
    public static void cleanup() {
        cleanup(OkexPrivateClientDataTest.class);
    }

    @Override
    public String getCxId() {
        return CXIds.OKEX;
    }

    @Before
    public void setup()
        throws IOException, CTTKException
    {
        org.junit.Assume.assumeTrue(hasTestProfile("test-data"));
        client = CXClientFactory.createPrivateCXClient(getCxId(), CXCredentialsProvider.getDefaultCredential(getCxId()).getTradeKey());
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetUserAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetUserAccount();
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetBankAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetBankAccount();
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetWallet()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetWallet();
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetAllWallets()
        throws CTTKException
    {
        super.testGetAllWallets();
    }
}
