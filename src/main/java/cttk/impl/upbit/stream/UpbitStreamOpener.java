package cttk.impl.upbit.stream;

import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.CXStreamOpener;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.request.OpenOrderBookStreamRequest;
import cttk.request.OpenTickerStreamRequest;
import cttk.request.OpenTradeStreamRequest;

public class UpbitStreamOpener
    implements CXStreamOpener
{
    @Override
    public String getCxId() {
        return CXIds.UPBIT;
    }

    @Override
    public CXStream<Ticker> openTickerStream(final OpenTickerStreamRequest request, CXStreamListener<Ticker> listener) {
        return new UpbitWebsocketStream<Ticker, OpenTickerStreamRequest>(
            UpbitChannels.getTickerChannel(),
            request,
            UpbitWebsocketMessageConverterFactory.createTickerMessageConverter(request),
            listener);
    }

    @Override
    public CXStream<OrderBook> openOrderBookStream(final OpenOrderBookStreamRequest request, CXStreamListener<OrderBook> listener) {
        return new UpbitWebsocketStream<OrderBook, OpenOrderBookStreamRequest>(
            UpbitChannels.getOrderBookChannel(),
            request,
            UpbitWebsocketMessageConverterFactory.createOrderBookMessageConverter(request),
            listener);
    }

    @Override
    public CXStream<Trades> openTradeStream(final OpenTradeStreamRequest request, CXStreamListener<Trades> listener)
        throws CTTKException
    {
        return new UpbitWebsocketStream<Trades, OpenTradeStreamRequest>(
            UpbitChannels.getTradeChannel(),
            request,
            UpbitWebsocketMessageConverterFactory.createTradesMessageConverter(request),
            listener);
    }
}
