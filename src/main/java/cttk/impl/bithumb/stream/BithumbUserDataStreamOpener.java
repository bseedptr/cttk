package cttk.impl.bithumb.stream;

import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.UserData;
import cttk.exception.CTTKException;
import cttk.impl.stream.eachmarket.EachMarketPollingUserDataStreamOpener;
import cttk.request.OpenUserDataStreamRequest;

public class BithumbUserDataStreamOpener
    extends EachMarketPollingUserDataStreamOpener
{
    public BithumbUserDataStreamOpener(PublicCXClient publicClient, PrivateCXClient privateCXClient) {
        super(publicClient, privateCXClient);
    }

    @Override
    protected CXStream<UserData> createStream(OpenUserDataStreamRequest request, CXStreamListener<UserData> listener)
        throws CTTKException
    {
        return new BithumbPollingUserDataStream(publicClient, privateCXClient, request, listener);
    }
}
