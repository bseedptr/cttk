package cttk.impl.bibox.response;

import cttk.dto.Ticker;
import cttk.impl.bibox.BiboxUtils;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BiboxTickerResponse
    extends BiboxResultResponse<BiboxTicker>
{
    public Ticker toTicker() {
        return new Ticker()
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(result.timestamp))
            .setLastPrice(result.lastPrice)
            .setLowPrice(result.lowPrice)
            .setHighPrice(result.highPrice)
            .setVolume(result.volume)
            .setHighestBid(result.bidPrice)
            .setLowestAsk(result.askPrice)
            .setChangePercent(BiboxUtils.parsePercentString(result.percent));
    }
}
