package cttk.impl.okex;

import static cttk.impl.okex.OkexUtilsV3.toOkexMarketSymbolV3;
import static cttk.impl.util.Objects.nvl;
import static java.lang.String.format;

import cttk.PublicCXClient;
import cttk.dto.MarketInfos;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Tickers;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.okex.response.OkexAllTickersResponse;
import cttk.impl.okex.response.OkexMarketInfoResponse;
import cttk.impl.okex.response.OkexOrderBookResponse;
import cttk.impl.okex.response.OkexTickerResponse;
import cttk.impl.okex.response.OkexTradesResponse;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ParamsBuilder;
import cttk.request.GetOrderBookRequest;
import cttk.request.GetRecentTradesRequest;
import cttk.request.GetTickerRequest;
import cttk.request.GetTradeHistoryRequest;

public class OkexPublicClient
    extends AbstractOkexClient
    implements PublicCXClient
{
    @Override
    public MarketInfos getMarketInfos()
        throws CTTKException
    {
        final HttpResponse response = requestGet(SPOT_BASE_URL, "/instruments");

        checkResponseError(null, response);

        return convert(response, OkexMarketInfoResponse.class, OkexMarketInfoResponse::toMarketInfos);
    }

    @Override
    public Ticker getTicker(GetTickerRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet(SPOT_BASE_URL, format("/instruments/%s/ticker", toOkexMarketSymbolV3(request)));

        checkResponseError(request, response);

        return convert(response, OkexTickerResponse.class, OkexTickerResponse::toTicker);
    }

    @Override
    public Tickers getAllTickers()
        throws CTTKException
    {
        final HttpResponse response = requestGet(SPOT_BASE_URL, "/instruments/ticker");

        checkResponseError(null, response);

        return convert(response, OkexAllTickersResponse.class, OkexAllTickersResponse::toTickers);
    }

    @Override
    public OrderBook getOrderBook(GetOrderBookRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet(SPOT_BASE_URL, format("/instruments/%s/book", toOkexMarketSymbolV3(request)),
            ParamsBuilder.create()
                .put("size", nvl(request.getCount(), 200))
                .build());

        checkResponseError(request, response);

        return convert(response, OkexOrderBookResponse.class, (r -> r.toOrderBook(request)));
    }

    @Override
    public Trades getRecentTrades(GetRecentTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet(SPOT_BASE_URL, format("/instruments/%s/trades", toOkexMarketSymbolV3(request)),
            ParamsBuilder.create()
                .put("limit", nvl(request.getCount(), 100))
                .build());

        checkResponseError(request, response);

        return convert(response, OkexTradesResponse.class, OkexTradesResponse::toTrades);
    }

    @Override
    public Trades getTradeHistory(GetTradeHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }
}
