package cttk.impl.binance;

import cttk.CXIds;
import cttk.impl.AbstractPublicCXClientTest;

public class BinancePublicClientTest
    extends AbstractPublicCXClientTest
{
    @Override
    public String getCxId() {
        return CXIds.BINANCE;
    }

}
