package cttk.impl.coinone;

import java.io.IOException;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;

import cttk.CXIds;
import cttk.exception.APILimitExceedException;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXAuthorityException;
import cttk.exception.CXServerException;
import cttk.exception.OrderNotFoundException;
import cttk.impl.AbstractCXClient;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ThrowableFunction;
import okhttp3.Request;
import okhttp3.Response;

abstract class AbstractCoinoneClient
    extends AbstractCXClient
{
    protected static final String BASE_URL = "https://api.coinone.co.kr";

    public AbstractCoinoneClient() {
        super();
    }

    @Override
    public String getCxId() {
        return CXIds.COINONE;
    }

    protected HttpResponse requestGet(String endpoint)
        throws CTTKException
    {
        return requestGet(endpoint, null);
    }

    protected HttpResponse requestGet(String endpoint, Map<String, String> params)
        throws CTTKException
    {
        final Request request = new Request.Builder()
            .url(getHostUrl(endpoint, params))
            .build();

        debug(request);

        try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
            return toHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }

    }

    protected String getHostUrl(String endpoint) {
        return getHostUrl(BASE_URL, endpoint);
    }

    protected String getHostUrl(String endpoint, Map<String, String> params) {
        return getHostUrl(BASE_URL, endpoint, params);
    }

    @Override
    protected void preCheckErrorByResponseStatus(Object requestObj, HttpResponse response)
        throws CTTKException
    {
        if (response.getStatus() == 405) {
            throw new CXServerException()
                .setCxId(getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
        super.preCheckErrorByResponseStatus(requestObj, response);
    }

    // Coinone API doc - doc.coinone.co.kr/#api-ErrorCode
    @Override
    protected void checkErrorByResponseMessage(Object requestObj, HttpResponse response)
        throws CTTKException
    {
        final String body = response.getBody();
        final Integer statusCode = response.getStatus();
        try {
            if (body.contains("\"errorCode\"")) {
                Map<String, Object> error = objectMapper.readValue(body, new TypeReference<Map<String, Object>>() {});
                String exchangeErrorCode = error.containsKey("errorCode") ? (String) error.get("errorCode") : "";
                switch (exchangeErrorCode) {
                    case "0":
                        // 0 is Non error case
                        break;
                    case "4":
                        throw new APILimitExceedException()
                            .setCxId(this.getCxId())
                            .setExchangeMessage(body)
                            .setResponseStatusCode(statusCode)
                            .setExchangeErrorCode(exchangeErrorCode);
                    case "104":
                        // 104 is Order id not found error case, which is not an error in CTTK
                        throw new OrderNotFoundException()
                            .setCxId(this.getCxId())
                            .setExchangeMessage(body)
                            .setResponseStatusCode(statusCode)
                            .setExchangeErrorCode(exchangeErrorCode);
                    case "107":
                        throw new CXAPIRequestException()
                            .setCxId(this.getCxId())
                            .setExchangeMessage(body)
                            .setResponseStatusCode(statusCode)
                            .setExchangeErrorCode(exchangeErrorCode);
                    case "121":
                    case "150":
                    case "151":
                        // Error code 151: Coinone API version error
                        // However, this could also happen for wrong API key
                        throw new CXAuthorityException()
                            .setCxId(this.getCxId())
                            .setExchangeMessage(body)
                            .setResponseStatusCode(statusCode)
                            .setExchangeErrorCode(exchangeErrorCode);

                    default:
                        throw new CXAPIRequestException(exchangeErrorCode + " - HTTP response status is not okay")
                            .setCxId(this.getCxId())
                            .setExchangeMessage(body)
                            .setResponseStatusCode(statusCode)
                            .setExchangeErrorCode(exchangeErrorCode);
                }
            }
        } catch (IOException e) {
            throw new CXAPIRequestException("json parse error")
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode);
        }
    }

    @Override
    protected void postCheckErrorByResponseStatus(Object requestObj, HttpResponse response)
        throws CTTKException
    {
        if (!response.is2xxSuccessful()) {
            if (response.getBody().contains("currency does not have a valid value")) {
                return;
            } else {
                super.postCheckErrorByResponseStatus(requestObj, response);
            }
        }
    }

    @Override
    protected <T, R> R convert(HttpResponse response, Class<T> claz, ThrowableFunction<T, R> mapper)
        throws CTTKException
    {
        final String body = response.getBody();
        try {
            T coinoneResponse = objectMapper.readValue(body, claz);

            return mapper.apply(coinoneResponse);
        } catch (IOException e) {
            throw new CXAPIRequestException("json parse error ", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(response.getStatus());
        }
    }

}
