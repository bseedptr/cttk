package cttk.impl.bitfinex;

import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import cttk.CXIds;
import cttk.dto.Balances;
import cttk.dto.Transfers;
import cttk.dto.UserOrders;
import cttk.dto.UserTrades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.AbstractPrivateCXClientDataTest;
import cttk.impl.util.ExNotRaisedException;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserOrderHistoryRequest;
import cttk.request.GetUserTradesRequest;

public class BitfinexPrivateClientDataTest
    extends AbstractPrivateCXClientDataTest
{
    @AfterClass
    public static void cleanup() {
        cleanup(BitfinexPrivateClientDataTest.class);
    }

    @Override
    public String getCxId() {
        return CXIds.BITFINEX;
    }

    @Override
    protected long getThreadSleepMillis() {
        return 1000l;
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetAllWallets()
        throws CTTKException
    {
        threadSleep();
        super.testGetAllWallets();
    }

    @Test
    @Override
    public void testGetAllBalances()
        throws CTTKException
    {
        threadSleep();
        final Balances balances = client.getAllBalances();

        logger.debug("{}", balances);

        Assert.assertNotNull(balances);
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetUserAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();
        super.testGetUserAccount();
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetBankAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();
        super.testGetBankAccount();
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetWallet()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();
        super.testGetWallet();
    }

    @Test
    @Override
    public void testGetUserOrderHistoryWrongCurrency()
        throws CTTKException
    {
        threadSleep(30000l);

        final GetUserOrderHistoryRequest request = new GetUserOrderHistoryRequest()
            .setCurrencyPair(getInvalidCurrencyPair());
        try {
            client.getUserOrderHistory(request);
        } catch (UnsupportedCurrencyPairException e) {
            assertEquals(e.getBaseCurrency(), getInvalidCurrencyPair().getBaseCurrency());
            assertEquals(e.getQuoteCurrency(), getInvalidCurrencyPair().getQuoteCurrency());
            assertEquals(e.getCxId(), this.getCxId());
            threadSleep(30000l);
            return;
        }
        throw new ExNotRaisedException("Failed to raise correct exception");
    }

    @Test
    @Override
    public void testGetUserOrderHistory()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep(30000l);

        final GetUserOrderHistoryRequest request = new GetUserOrderHistoryRequest()
            .setCurrencyPair(getValidCurrencyPair());
        final UserOrders userOrders = client.getUserOrderHistory(request);

        logger.debug("{}", userOrders);

        Assert.assertNotNull(userOrders);
        Assert.assertNotNull(userOrders.first());
        Assert.assertEquals(request.getBaseCurrency(), userOrders.first().getBaseCurrency());
        Assert.assertEquals(request.getQuoteCurrency(), userOrders.first().getQuoteCurrency());
        Assert.assertNotNull(userOrders.first().getOid());
        Assert.assertNotNull(userOrders.first().getSide());
        Assert.assertNotNull(userOrders.first().getPrice());
        Assert.assertNotNull(userOrders.first().getQuantity());
        Assert.assertNotNull(userOrders.first().getTotal());
        threadSleep(30000l);
    }

    @Test
    @Override
    public void testGetTransfersWrongCurrency()
        throws CTTKException
    {
        threadSleep();

        final GetTransfersRequest request = new GetTransfersRequest()
            .setCurrency(invalidCurrency);
        final Transfers transfers = client.getTransfers(request);

        Assert.assertEquals(0, transfers.getTransfers().size());
    }

    @Test
    @Override
    public void testGetUserTradeWrongCurrency()
        throws CTTKException
    {
        threadSleep();

        final GetUserTradesRequest request = new GetUserTradesRequest()
            .setCurrencyPair(getInvalidCurrencyPair());
        final UserTrades userTrades = client.getUserTrades(request);

        Assert.assertEquals(0, userTrades.getTrades().size());
    }
}
