package cttk.impl.poloniex;

import cttk.CXIds;
import cttk.impl.AbstractPublicCXClientTest;

public class PoloniexPublicClientTest
    extends AbstractPublicCXClientTest
{
    @Override
    public String getCxId() {
        return CXIds.POLONIEX;
    }
}
