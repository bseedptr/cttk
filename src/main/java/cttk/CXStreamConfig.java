package cttk;

public interface CXStreamConfig {
    public static final long DEFAULT_INTERVAL_IN_MILLI = 1000l;
    public static final int DEFAULT_MAX_POLL_RETRY_COUNT = 3;

    long getIntervalInMilli();

    int getMaxPollRetryCount();

    public static CXStreamConfig getDefault() {
        return new CXStreamConfig() {
            @Override
            public long getIntervalInMilli() {
                return DEFAULT_INTERVAL_IN_MILLI;
            }

            @Override
            public int getMaxPollRetryCount() {
                return DEFAULT_MAX_POLL_RETRY_COUNT;
            }
        };
    }
}
