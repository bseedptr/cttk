package cttk.impl.bibox.response;

import lombok.Data;

@Data
abstract public class BiboxCommandResponse {
    protected String cmd;
}
