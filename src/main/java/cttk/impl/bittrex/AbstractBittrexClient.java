package cttk.impl.bittrex;

import static cttk.impl.util.Objects.nvl;

import java.io.IOException;
import java.util.Map;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXServerException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.AbstractCXClient;
import cttk.impl.bittrex.response.BittrexResponse;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ThrowableFunction;
import okhttp3.Request;
import okhttp3.Response;

abstract class AbstractBittrexClient
    extends AbstractCXClient
{
    protected static final String BASE_URL = "https://bittrex.com/api/v1.1";

    @Override
    public String getCxId() {
        return CXIds.BITTREX;
    }

    protected HttpResponse requestGet(String endpoint)
        throws CTTKException
    {
        return requestGet(endpoint, null);
    }

    protected HttpResponse requestGet(String endpoint, Map<String, String> params)
        throws CTTKException
    {
        return requestGet(endpoint, params, null);
    }

    protected HttpResponse requestGet(String endpoint, Map<String, String> params, String tailParam)
        throws CTTKException
    {
        final Request request = new Request.Builder()
            .url(getHostUrl(endpoint, params) + nvl(tailParam, ""))
            .build();

        debug(request);

        try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
            return toHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    protected String getHostUrl(String endpoint) {
        return getHostUrl(BASE_URL, endpoint);
    }

    protected String getHostUrl(String endpoint, Map<String, String> params) {
        return getHostUrl(BASE_URL, endpoint, params);
    }

    @Override
    protected void checkErrorByResponseMessage(Object requestObj, HttpResponse response)
        throws CTTKException
    {
        if (response.getBody().contains("INVALID_MARKET")) {
            throw CXAPIRequestException.createByRequestObj(requestObj)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    protected <T, R> R convert(HttpResponse response, Class<T> claz, ThrowableFunction<T, R> mapper)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        try {
            final T obj = objectMapper.readValue(response.getBody(), claz);

            if (obj instanceof BittrexResponse) {
                BittrexResponse<?> bittrexResponse = (BittrexResponse<?>) obj;
                if ("false".equals(bittrexResponse.getSuccess())) {
                    throw new CXAPIRequestException("Response status not success")
                        .setCxId(this.getCxId())
                        .setResponseStatusCode(response.getStatus())
                        .setExchangeMessage(response.getBody());
                }
            }

            return mapper.apply(obj);
        } catch (IOException e) {
            throw new CXAPIRequestException("json parse error ", e)
                .setCxId(this.getCxId())
                .setResponseStatusCode(response.getStatus())
                .setExchangeMessage(response.getBody());
        }
    }

}
