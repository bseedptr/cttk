package cttk.impl.coinone;

import static cttk.OrderSide.BUY;
import static cttk.OrderSide.SELL;

import org.junit.Assert;
import org.junit.Test;

import cttk.CurrencyPair.SimpleCurrencyPair;

public class CoinoneUtilsTest {
    @Test
    public void testGuessFeeCurrencyIfOderSideIsSELLThenAlwaysKRW() {
        String feeCurrencyOfETH = CoinoneUtils.guessFeeCurrency(SELL, new SimpleCurrencyPair("ETH", "KRW"));
        String feeCurrencyOfBTC = CoinoneUtils.guessFeeCurrency(SELL, new SimpleCurrencyPair("BTC", "KRW"));
        String feeCurrencyOfEOS = CoinoneUtils.guessFeeCurrency(SELL, new SimpleCurrencyPair("EOS", "KRW"));

        Assert.assertEquals("Should be always 'KRW' if order side is SELL.", "KRW", feeCurrencyOfETH);
        Assert.assertEquals("Should be always 'KRW' if order side is SELL.", "KRW", feeCurrencyOfBTC);
        Assert.assertEquals("Should be always 'KRW' if order side is SELL.", "KRW", feeCurrencyOfEOS);
    }

    @Test
    public void testGuessFeeCurrencyIfOderSideIsBUYThenBaseCurrency() {
        SimpleCurrencyPair pair = new SimpleCurrencyPair("ETH", "KRW");
        String feeCurrency = CoinoneUtils.guessFeeCurrency(BUY, pair);

        Assert.assertEquals("Should be'ETH'.", pair.getBaseCurrency(), feeCurrency);
    }
}