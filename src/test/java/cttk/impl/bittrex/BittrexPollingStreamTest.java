package cttk.impl.bittrex;

import cttk.CXIds;
import cttk.impl.AbstractPollingCXStreamTest;

public class BittrexPollingStreamTest
    extends AbstractPollingCXStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.BITTREX;
    }
}
