package cttk.impl.hitbtc;

import cttk.CXIds;
import cttk.impl.AbstractUserDataStreamTest;

public class HitbtcUserDataStreamTest
    extends AbstractUserDataStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.HITBTC;
    }

    @Override
    protected boolean useThread() {
        return true;
    }
}
