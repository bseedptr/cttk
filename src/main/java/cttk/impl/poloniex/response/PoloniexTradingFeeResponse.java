package cttk.impl.poloniex.response;

import cttk.TradingFeeProvider;
import cttk.impl.feeprovider.DefaultTradingFeeProvider;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PoloniexTradingFeeResponse {
    String takerFee;
    String makerFee;
    String thirtyDayVolume;
    String nextTier;

    public TradingFeeProvider getTradingFee() {
        return new DefaultTradingFeeProvider().setFee(this.takerFee, this.makerFee);
    }
}
