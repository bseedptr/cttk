package cttk.impl.bibox.stream;

import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.UserData;
import cttk.auth.Credential;
import cttk.impl.bibox.AbstractBiboxClient;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;

public class BiboxUserDataWebsocketStream extends AbstractBiboxClient
    implements CXStream<UserData>
{
    private static final String WSS_API_BIBOX_PRO_WS = "wss://push.bibox.com/";
    private final Credential credential;
    private final CXStreamListener<UserData> listener;

    protected WebSocket websocket;

    public BiboxUserDataWebsocketStream(
        final Credential credential,
        final CXStreamListener<UserData> listener)
    {
        this.credential = credential;
        this.listener = listener;

        open();
    }

    @Override
    public CXStream<UserData> open()
    {
        final OkHttpClient okHttpClient = createOkHttpClient(10, 25);

        websocket = okHttpClient.newWebSocket(
            new Request.Builder().url(WSS_API_BIBOX_PRO_WS).build(),
            new BiboxUserDataWebsocketListener(this, listener).setCredential(credential));

        okHttpClient.dispatcher().executorService().shutdown();
        return this;
    }

    @Override
    public void close()
    {
        if (websocket != null) websocket.close(NORMAL_CLOSURE_STATUS, null);
    }

    @Override
    public CXStreamListener<UserData> getListener() {
        return listener;
    }
}
