package cttk.impl.feeprovider;

import cttk.CurrencyPair;
import lombok.ToString;

@ToString(callSuper = true)
public class ByQuoteTradingFeeProvider
    extends AbstractMappedTradingFeeProvider<ByQuoteTradingFeeProvider>
{
    @Override
    public String getKey(CurrencyPair currencyPair) {
        return currencyPair == null ? null : currencyPair.getQuoteCurrency();
    }
}
