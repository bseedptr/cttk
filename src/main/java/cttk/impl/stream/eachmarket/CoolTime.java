package cttk.impl.stream.eachmarket;

import static cttk.CXIds.BITHUMB;
import static cttk.CXIds.COINONE;
import static cttk.CXIds.HUOBI;
import static cttk.CXIds.KORBIT;
import static cttk.impl.stream.eachmarket.PollingLevel.LEVEL1;
import static cttk.impl.stream.eachmarket.PollingLevel.LEVEL2;
import static cttk.impl.stream.eachmarket.PollingLevel.LEVEL3;

import java.util.HashMap;
import java.util.Map;

import cttk.exception.CTTKException;
import lombok.ToString;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@ToString
public class CoolTime {
    private final Map<PollingLevel, Integer> coolTimeMap = new HashMap<>();

    public static CoolTime createDefault(String cxId)
        throws CTTKException
    {
        switch (cxId) {
            case BITHUMB:
                return new CoolTime(6000, 10000, 30000);
            case COINONE:
                return new CoolTime(4000, 10000, 20000);
            case KORBIT:
                return new CoolTime(3000, 6000, 14000);
            case HUOBI:
                return new CoolTime(6000, 30000, 50000);
            default:
                throw new CTTKException("Default cool time is not defined.").setCxId(cxId);
        }
    }

    public CoolTime(int lv1CollTimeMillis, int lv2CoolTimeMillis, int lv3CoolTimeMillis) {
        coolTimeMap.put(LEVEL1, lv1CollTimeMillis);
        coolTimeMap.put(LEVEL2, lv2CoolTimeMillis);
        coolTimeMap.put(LEVEL3, lv3CoolTimeMillis);
    }

    public int get(PollingLevel pollingLevel) {
        return coolTimeMap.get(pollingLevel);
    }
}
