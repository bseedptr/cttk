package cttk.impl.coinbase;

import cttk.CXClientFactory;
import cttk.PublicCXClient;

public class CoinbaseClientFactory
    implements CXClientFactory
{
    @Override
    public PublicCXClient create() {
        return new CoinbasePublicClient();
    }
}
