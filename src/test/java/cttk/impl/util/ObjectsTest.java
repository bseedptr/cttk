package cttk.impl.util;

import org.junit.Assert;
import org.junit.Test;

import cttk.impl.util.Objects;

public class ObjectsTest {
    @Test
    public void testNvl() {
        Assert.assertEquals("A", Objects.nvl("A", "B"));
        Assert.assertEquals("B", Objects.nvl(null, "B"));
    }
}
