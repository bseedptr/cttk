package cttk.exception;

import java.math.BigDecimal;

import cttk.CurrencyPair;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class InvalidTotalMinException
    extends InvalidTotalRangeException
{
    private static final long serialVersionUID = 1L;

    public InvalidTotalMinException() {
        super();
    }

    public InvalidTotalMinException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidTotalMinException(String message) {
        super(message);
    }

    public InvalidTotalMinException(Throwable cause) {
        super(cause);
    }

    @Override
    public InvalidTotalMinException setCxId(String cxId) {
        super.setCxId(cxId);
        return this;
    }

    @Override
    public InvalidTotalMinException setResponseStatusCode(Integer responseStatusCode) {
        super.setResponseStatusCode(responseStatusCode);
        return this;
    }

    @Override
    public InvalidTotalMinException setExchangeMessage(String exchangeMessage) {
        super.setExchangeMessage(exchangeMessage);
        return this;
    }

    @Override
    public InvalidTotalMinException setExchangeErrorCode(String exchangeErrorCode) {
        super.setExchangeErrorCode(exchangeErrorCode);
        return this;
    }

    @Override
    public InvalidTotalMinException setCurrencyPair(CurrencyPair currencyPair) {
        super.setCurrencyPair(currencyPair);
        return this;
    }

    @Override
    public InvalidTotalMinException setOrderPrice(BigDecimal orderPrice) {
        super.setOrderPrice(orderPrice);
        return this;
    }

    @Override
    public InvalidTotalMinException setOrderQuantity(BigDecimal orderQuantity) {
        super.setOrderQuantity(orderQuantity);
        return this;
    }

    @Override
    public InvalidTotalMinException setBaseCurrency(String baseCurrency) {
        super.setBaseCurrency(baseCurrency);
        return this;
    }

    @Override
    public InvalidTotalMinException setQuoteCurrency(String quoteCurrency) {
        super.setQuoteCurrency(quoteCurrency);
        return this;
    }

    @Override
    public InvalidTotalMinException setTotal(BigDecimal total) {
        super.setTotal(total);
        return this;
    }
}