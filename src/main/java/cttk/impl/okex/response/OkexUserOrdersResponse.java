package cttk.impl.okex.response;

import static cttk.impl.okex.OkexUtils.guessFeeCurrency;
import static cttk.impl.okex.OkexUtilsV3.toStdMarketSymbolV3;
import static cttk.impl.okex.OkexUtilsV3.toStdOrderStatusV3;
import static java.math.BigDecimal.ZERO;
import static java.util.stream.Collectors.toList;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.PricingType;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class OkexUserOrdersResponse
    extends ArrayList<OkexUserOrder>
{
    public UserOrders toUserOrders() {
        return new UserOrders()
            .setOrders(stream()
                .map(o -> o.toUserOrder())
                .filter(o -> o != null)
                .collect(Collectors.toList()));
    }

    public UserTrades toUserTrades() {
        return new UserTrades()
            .setTrades(stream()
                .map(OkexUserOrder::toUserTrade)
                .collect(toList()));
    }
}

@Data
class OkexUserOrder {
    String created_at;
    BigDecimal executed_value;
    BigDecimal filled_notional;
    BigDecimal filled_size; // size corresponds to quantity
    String funds;
    String instrument_id;
    BigDecimal notional;
    String order_id;
    BigDecimal price;
    String product_id;
    String side;
    BigDecimal size;
    String status;
    String timestamp; // Same as created_at
    String type;

    UserOrder toUserOrder() {
        if (order_id == null) return null;
        final BigDecimal refinedPrice = getRefinedPrice();
        return new UserOrder()
            .setCurrencyPair(toStdMarketSymbolV3(instrument_id))
            .setOid(order_id)
            .setDateTime(DateTimeUtils.parseZdt(created_at))
            .setSide(OrderSide.of(side))
            .setStatus(status)
            .setStatusType(getStatusType())
            .setPricingType(PricingType.of(type))
            .setPrice(refinedPrice)
            .setQuantity(size)
            .setFilledQuantity(filled_size)
            .setRemainingQuantity(MathUtils.subtract(size, filled_size))
            .setTotal(MathUtils.multiply(refinedPrice, size));
    }

    UserTrade toUserTrade() {
        final CurrencyPair pair = toStdMarketSymbolV3(instrument_id);
        final OrderSide orderSide = OrderSide.of(side);
        final String feeCurrency = guessFeeCurrency(orderSide, pair);
        final BigDecimal refinedPrice = getRefinedPrice();
        return new UserTrade()
            .setTid(order_id)
            .setCurrencyPair(pair)
            .setOid(order_id)
            .setDateTime(DateTimeUtils.parseZdt(timestamp))
            .setSide(orderSide)
            .setStatus(status)
            .setPrice(refinedPrice)
            .setQuantity(size)
            .setFeeCurrency(feeCurrency)
            .setTotal(MathUtils.multiply(refinedPrice, size));
    }

    BigDecimal getRefinedPrice() {
        return price == null ? ZERO : price; // 0 if market order
    }

    OrderStatus getStatusType() {
        final OrderStatus orderStatus = toStdOrderStatusV3(status);
        return isFakePartiallyFilledStatus(orderStatus) ? OrderStatus.UNFILLED : orderStatus;
    }

    /**
     * If status was "partially_filled" and volume was "0", UNFILLED actually.
     */
    boolean isFakePartiallyFilledStatus(OrderStatus orderStatus) {
        return orderStatus == OrderStatus.PARTIALLY_FILLED && filled_size.compareTo(ZERO) == 0;
    }
}