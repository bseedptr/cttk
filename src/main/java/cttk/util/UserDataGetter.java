package cttk.util;

import static cttk.impl.util.Objects.nvl;

import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cttk.CXClientFactory;
import cttk.CurrencyPair;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.auth.Credential;
import cttk.dto.Balances;
import cttk.dto.MarketInfos;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCXException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.util.Timer;
import cttk.request.GetOpenUserOrdersRequest;
import cttk.request.GetTransfersDeltaRequest;
import cttk.request.GetUserOrderHistoryDeltaRequest;
import cttk.request.GetUserTradesDeltaRequest;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserDataGetter {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final PublicCXClient pubClient;
    private final PrivateCXClient prvClient;

    private final MarketInfos marketInfos;

    private long requestIntervalInMilli = 500;
    private long maxRetry = 3;

    public UserDataGetter(final String cxId, final Credential credential)
        throws UnsupportedCXException, CTTKException
    {
        this.pubClient = CXClientFactory.createPublicCXClient(cxId);
        this.prvClient = CXClientFactory.createPrivateCXClient(cxId, credential);

        this.marketInfos = this.pubClient.getMarketInfos();
    }

    public Balances getAllBalances()
        throws CTTKException
    {
        return this.prvClient.getAllBalances();
    }

    public UserOrders getAllOpenUserOrders()
        throws UnsupportedCurrencyPairException, CTTKException
    {
        try {
            return prvClient.getAllOpenUserOrders(false);
        } catch (UnsupportedMethodException e) {
            return getAllOpenUserOrders(marketInfos.getMarketInfos());
        }
    }

    private UserOrders getAllOpenUserOrders(List<? extends CurrencyPair> currencyPairList)
        throws UnsupportedCurrencyPairException, CTTKException
    {
        final UserOrders userOrders = new UserOrders();
        final Timer timer = new Timer();
        for (CurrencyPair currencyPair : currencyPairList) {
            timer.start();
            logger.debug("Get open order in [{}]", currencyPair.getPairString());
            final GetOpenUserOrdersRequest request = new GetOpenUserOrdersRequest().setCurrencyPair(currencyPair);
            userOrders.addAll(prvClient.getOpenUserOrders(request));
            timer.stop();

            final long sleepMillis = requestIntervalInMilli - timer.getIntervalInMilli();
            try {
                if (sleepMillis > 0) {
                    logger.debug("sleep {}ms", sleepMillis);
                    Thread.sleep(sleepMillis);
                }
            } catch (InterruptedException e) {
                // do nothing
            }
        }
        return userOrders;
    }

    public UserOrders getUserOrderHistory(
        final CurrencyPair currencyPair,
        final UserOrder lastUserOrder,
        final ZonedDateTime lastCheckDateTime,
        final ZonedDateTime now,
        final Integer count)
        throws UnsupportedCurrencyPairException, CTTKException
    {
        return prvClient.getUserOrderHistory(
            toGetUserOrderHistoryDeltaRequest(currencyPair, lastUserOrder, lastCheckDateTime, now, count)
                .setSleepTimeMillis(requestIntervalInMilli));
    }

    public UserOrders getUserOrderHistory(GetUserOrderHistoryDeltaRequest request)
        throws UnsupportedCurrencyPairException, CTTKException
    {
        if (request.getSleepTimeMillis() == null) {
            request.setSleepTimeMillis(requestIntervalInMilli);
        }
        return prvClient.getUserOrderHistory(request);
    }

    public UserTrades getUserTrades(
        final CurrencyPair currencyPair,
        final UserTrade lastUserTrade,
        final ZonedDateTime lastCheckDateTime,
        final ZonedDateTime now,
        final Integer count)
        throws UnsupportedCurrencyPairException, CTTKException
    {
        return prvClient.getUserTrades(
            toGetUserTradesDeltaRequest(currencyPair, lastUserTrade, lastCheckDateTime, now, count)
                .setSleepTimeMillis(requestIntervalInMilli));
    }

    public UserTrades getUserTrades(GetUserTradesDeltaRequest request)
        throws UnsupportedCurrencyPairException, CTTKException
    {
        if (request.getSleepTimeMillis() == null) {
            request.setSleepTimeMillis(requestIntervalInMilli);
        }
        return prvClient.getUserTrades(request);
    }

    public Transfers getTransfers(
        final String currency,
        final Transfer lastTransfer,
        final ZonedDateTime lastCheckDateTime,
        final ZonedDateTime now,
        final Integer count)
        throws UnsupportedCurrencyPairException, CTTKException
    {
        return prvClient.getTransfers(
            toGetTransfersDeltaRequest(currency, lastTransfer, lastCheckDateTime, now, count)
                .setSleepTimeMillis(requestIntervalInMilli));
    }

    public Transfers getTransfers(GetTransfersDeltaRequest request)
        throws UnsupportedCurrencyPairException, CTTKException
    {
        if (request.getSleepTimeMillis() == null) {
            request.setSleepTimeMillis(requestIntervalInMilli);
        }
        return prvClient.getTransfers(request);
    }

    public static final GetUserOrderHistoryDeltaRequest toGetUserOrderHistoryDeltaRequest(
        final CurrencyPair currencyPair,
        final UserOrder lastUserOrder,
        final ZonedDateTime lastCheckDateTime,
        final ZonedDateTime now,
        final Integer count)
    {
        final GetUserOrderHistoryDeltaRequest request = new GetUserOrderHistoryDeltaRequest()
            .setCurrencyPair(currencyPair);

        if (lastUserOrder != null) {
            request
                .setStartDateTime(nvl(lastCheckDateTime, lastUserOrder.getDateTime()))
                .setEndDateTime(now)
                .setFromId(lastUserOrder.getOid());
        } else if (lastCheckDateTime != null) {
            request
                .setStartDateTime(lastCheckDateTime)
                .setEndDateTime(now);
        }

        return request;
    }

    public static final GetUserTradesDeltaRequest toGetUserTradesDeltaRequest(
        final CurrencyPair currencyPair,
        final UserTrade lastUserTrade,
        final ZonedDateTime lastCheckDateTime,
        final ZonedDateTime now,
        final Integer count)
        throws UnsupportedCurrencyPairException, CTTKException
    {
        final GetUserTradesDeltaRequest request = new GetUserTradesDeltaRequest().setCurrencyPair(currencyPair);

        if (lastUserTrade != null) {
            request
                .setStartDateTime(nvl(lastCheckDateTime, lastUserTrade.getDateTime()))
                .setEndDateTime(now)
                .setFromId(lastUserTrade.getTid());
        } else if (lastCheckDateTime != null) {
            request
                .setStartDateTime(lastCheckDateTime)
                .setEndDateTime(now);
        }

        return request;
    }

    public static final GetTransfersDeltaRequest toGetTransfersDeltaRequest(
        final String currency,
        final Transfer lastTransfer,
        final ZonedDateTime lastCheckDateTime,
        final ZonedDateTime now,
        final Integer count)
        throws UnsupportedCurrencyPairException, CTTKException
    {
        final GetTransfersDeltaRequest request = new GetTransfersDeltaRequest().setCurrency(currency);
        if (lastTransfer != null) {
            request
                .setStartDateTime(nvl(lastCheckDateTime, lastTransfer.getCreatedDateTime(), lastTransfer.getUpdatedDateTime(), lastTransfer.getCompletedDateTime()))
                .setEndDateTime(now)
                .setFromId(lastTransfer.getTid());
        } else if (lastCheckDateTime != null) {
            request
                .setStartDateTime(lastCheckDateTime)
                .setEndDateTime(now);
        }

        return request;
    }
}
