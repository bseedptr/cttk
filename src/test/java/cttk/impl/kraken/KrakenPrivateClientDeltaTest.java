package cttk.impl.kraken;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.time.ZonedDateTime;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import cttk.auth.Credential;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.impl.AbstractPrivateClientDeltaTest;
import cttk.request.GetTransfersDeltaRequest;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserOrderHistoryDeltaRequest;
import cttk.request.GetUserTradesDeltaRequest;
import lombok.extern.slf4j.Slf4j;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Transfer.class)
@PowerMockIgnore("javax.net.ssl.*")
@Slf4j
public class KrakenPrivateClientDeltaTest
    extends AbstractPrivateClientDeltaTest
{
    KrakenPrivateClient client;
    GetUserOrderHistoryDeltaRequest orderHistoryRequest;
    GetUserTradesDeltaRequest tradesRequest;
    GetTransfersDeltaRequest transferRequest;

    @Mock
    Transfers transfers1, transfers2, transfers3;

    @Before
    public void setup() {
        PowerMockito.spy(Transfer.class);
        client = spy(new KrakenPrivateClient(mock(Credential.class)));
        orderHistoryRequest = new GetUserOrderHistoryDeltaRequest().setStartDateTime(ZonedDateTime.now().minusYears(2));
        tradesRequest = new GetUserTradesDeltaRequest().setStartDateTime(ZonedDateTime.now().minusYears(2));
        transferRequest = new GetTransfersDeltaRequest().setStartDateTime(ZonedDateTime.now().minusYears(2));
    }

    @Test
    public void testGetTransfersDeltaWhenNoMoreData()
        throws Exception
    {
        PowerMockito.when(Transfer.equalsById(any(), any())).thenReturn(false, false);
        when(transfers1.getList()).thenReturn(makeTransferFixtures("1", "2", "3"));
        when(transfers1.getOldest()).thenReturn(makeTransferFixture("1"));
        when(transfers1.isEmpty()).thenReturn(false);
        when(transfers2.getList()).thenReturn(makeTransferFixtures());
        when(transfers2.isEmpty()).thenReturn(true);
        doReturn(transfers1, transfers2).when(client).getTransfers(any(GetTransfersRequest.class));

        Transfers result = client.getTransfers(transferRequest);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat(ids, containsInAnyOrder("1", "2", "3"));
    }

    @Test
    public void testGetTransfersDeltaWhenDuplication()
        throws Exception
    {
        PowerMockito.when(Transfer.equalsById(any(), any())).thenReturn(false, false, true);
        when(transfers1.getList()).thenReturn(makeTransferFixtures("1", "2", "3"));
        when(transfers1.getOldest()).thenReturn(makeTransferFixture("1"));
        when(transfers2.getList()).thenReturn(makeTransferFixtures("3", "4", "5"));
        when(transfers2.getOldest()).thenReturn(makeTransferFixture("3"));
        when(transfers3.getList()).thenReturn(makeTransferFixtures("6"));
        doReturn(transfers1, transfers2).when(client).getTransfers(any(GetTransfersRequest.class));

        Transfers result = client.getTransfers(transferRequest);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat("Id should be unique.", ids, containsInAnyOrder("1", "2", "3", "4", "5"));
    }
}
