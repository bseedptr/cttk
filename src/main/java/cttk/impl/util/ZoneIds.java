package cttk.impl.util;

import java.time.ZoneId;

public class ZoneIds {
    public static final ZoneId SEOUL = ZoneId.of("Asia/Seoul");
    public static final ZoneId SHANGHAI = ZoneId.of("Asia/Shanghai");
}
