package cttk.impl.upbit;

import cttk.CXIds;
import cttk.impl.AbstractUserDataStreamTest;

public class UpbitUserDataStreamTest
    extends AbstractUserDataStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.UPBIT;
    }
}
