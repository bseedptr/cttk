package cttk.impl.korbit;

import java.io.IOException;
import java.util.Map;

import cttk.CXIds;
import cttk.exception.APILimitExceedException;
import cttk.exception.CTTKException;
import cttk.exception.CXAuthorityException;
import cttk.exception.CXServerException;
import cttk.impl.AbstractCXClient;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ThrowableFunction;
import okhttp3.Request;
import okhttp3.Response;

abstract class AbstractKorbitClient
    extends AbstractCXClient
{
    protected static final String BASE_URL = "https://api.korbit.co.kr/v1";

    protected static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36";

    public AbstractKorbitClient() {
        super();
    }

    @Override
    public String getCxId() {
        return CXIds.KORBIT;
    }

    protected HttpResponse requestGet(String endpoint)
        throws CTTKException
    {
        return requestGet(endpoint, null);
    }

    protected HttpResponse requestGet(String endpoint, Map<String, String> params)
        throws CTTKException
    {
        final Request request = new Request.Builder()
            .header("User-Agent", USER_AGENT)
            .url(getHostUrl(endpoint, params))
            .build();

        debug(request);

        try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
            return toHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    protected String getHostUrl(String endpoint) {
        return getHostUrl(BASE_URL, endpoint);
    }

    protected String getHostUrl(String endpoint, Map<String, String> params) {
        return getHostUrl(BASE_URL, endpoint, params);
    }

    protected <T> T convert(HttpResponse response, Class<T> claz)
        throws CTTKException
    {
        return convert(response, claz, ThrowableFunction.identity());
    }

    @Override
    protected void preCheckErrorByResponseStatus(Object requestObj, HttpResponse response)
        throws CTTKException
    {
        if (response.getStatus() == 403
            && response.getBody().contains("Access Denied"))
        {
            throw new APILimitExceedException("API limit exceeeded")
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        } else if (response.getStatus() == 401
            && response.getBody().contains("invalid_client"))
        {
            throw new CXAuthorityException("")
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
        super.preCheckErrorByResponseStatus(requestObj, response);
    }

    @Override
    protected void checkErrorByResponseMessage(Object requestObj, HttpResponse response)
        throws CTTKException
    {
        // For most errors Korbit doesn't have an error message
    }

}
