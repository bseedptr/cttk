package cttk.impl.okex;

import java.io.IOException;
import java.util.Map;

import okhttp3.Request;
import okhttp3.Response;
import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.exception.CXServerException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.AbstractCXClient;
import cttk.impl.util.HttpResponse;

public abstract class AbstractOkexClient
    extends AbstractCXClient
{
    protected static final String SPOT_BASE_URL = "https://www.okex.com/api/spot/v3";
    protected static final String ACCOUNT_BASE_URL = "https://www.okex.com/api/account/v3";
    protected static final okhttp3.MediaType JSON = okhttp3.MediaType.parse("application/json; charset=utf-8");

    @Override
    public String getCxId() {
        return CXIds.OKEX;
    }

    @Override
    protected HttpResponse toHttpResponse(Response httpResponse)
        throws IOException, UnsupportedCurrencyPairException, CTTKException
    {
        final String body = httpResponse.body().string();
        final HttpResponse response = new HttpResponse()
            .setHeaders(httpResponse.headers().toMultimap())
            .setStatus(httpResponse.code())
            .setBody(body);
        System.out.println(httpResponse.headers());
        debug(response);
        return response;
    }

    protected HttpResponse requestGet(String baseUrl, String path)
        throws CTTKException
    {
        return requestGet(baseUrl, path, null);
    }

    protected HttpResponse requestGet(String baseUrl, String path, Map<String, String> params)
        throws CTTKException
    {
        final String url = getHostUrl(baseUrl, path, params);
        final Request request = new Request.Builder()
            .url(url)
            .get()
            .build();

        debug(request);

        try (final Response httpResponse = super.httpClient.newCall(request).execute()) {
            return toHttpResponse(httpResponse);
        } catch (IOException ioe) {
            throw new CXServerException(ioe)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    @Override
    protected void checkErrorByResponseMessage(Object requestObj, HttpResponse response)
        throws CTTKException
    {
        if (!response.is4xx()) return;
        OkexUtils.checkErrorByResponseMessage(requestObj, response.getBody(), response.getStatus());
    }
}
