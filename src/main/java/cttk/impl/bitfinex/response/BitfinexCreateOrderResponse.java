package cttk.impl.bitfinex.response;

import static cttk.impl.bitfinex.BitfinexUtils.getPricingType;
import static cttk.impl.bitfinex.BitfinexUtils.stringTimestampToLong;

import java.math.BigDecimal;
import java.util.Arrays;

import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.dto.UserOrder;
import cttk.impl.bitfinex.BitfinexUtils;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;

@Data
public class BitfinexCreateOrderResponse {
    Long id;
    String symbol;
    String exchange;
    BigDecimal price;
    BigDecimal avg_execution_price;
    String side;
    String type;
    String timestamp;
    Boolean is_live;
    Boolean is_cancelled;
    Boolean is_hidden;
    Boolean was_forced;
    BigDecimal original_amount;
    BigDecimal remaining_amount;
    BigDecimal executed_amount;
    Long order_id;

    public UserOrder toUserOrder() {
        return new UserOrder()
            .setCurrencyPair(BitfinexUtils.parseMarketSymbol(symbol))
            .setOid(String.valueOf(id))
            .setDateTime(DateTimeUtils.zdtFromEpochSecond(stringTimestampToLong(timestamp)))
            .setSide(OrderSide.of(side))
            .setPrice(price)
            .setQuantity(original_amount.abs())
            .setAvgPrice(avg_execution_price)
            .setFilledQuantity(executed_amount.abs())
            .setRemainingQuantity(remaining_amount.abs())
            .setTotal(MathUtils.multiply(price, original_amount.abs()))
            .setStatus(getStatus())
            .setStatusType(getStatusType())
            .setPricingType(getPricingType(type));
    }

    private String getStatus() {
        return Arrays.toString(new boolean[] { is_live, is_cancelled, is_hidden, was_forced });
    }

    private OrderStatus getStatusType() {
        if (is_live) {
            if (executed_amount == null || executed_amount.compareTo(BigDecimal.ZERO) == 0) {
                return OrderStatus.UNFILLED;
            } else if (executed_amount != null
                && executed_amount.compareTo(BigDecimal.ZERO) > 0
                && executed_amount.compareTo(original_amount) < 0)
            {
                return OrderStatus.PARTIALLY_FILLED;
            }
        }

        if (executed_amount != null && executed_amount.compareTo(original_amount) == 0) {
            return OrderStatus.FILLED;
        }

        if (is_cancelled) return OrderStatus.CANCELED;

        return null;
    }
}
