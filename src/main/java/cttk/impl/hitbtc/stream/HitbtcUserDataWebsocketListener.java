package cttk.impl.hitbtc.stream;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.GZIPInputStream;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cttk.CXClientFactory;
import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.OrderStatus;
import cttk.PrivateCXClient;
import cttk.UserData;
import cttk.auth.Credential;
import cttk.dto.Balances;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrade;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.impl.hitbtc.HitbtcUtils;
import cttk.impl.hitbtc.stream.response.HitbtcUserDataBalanceResponse;
import cttk.impl.hitbtc.stream.response.HitbtcUserDataOrderResponse;
import cttk.impl.hitbtc.stream.response.HitbtcUserDataReportResponse;
import cttk.impl.stream.AbstractWebsocketListener;
import cttk.impl.util.JsonUtils;
import cttk.impl.util.OParamsBuilder;
import cttk.impl.util.ParamsBuilder;
import cttk.impl.util.ThrowableFunction;
import cttk.request.OpenUserDataStreamRequest;
import okhttp3.Response;
import okhttp3.WebSocket;
import okio.ByteString;

public class HitbtcUserDataWebsocketListener
    extends AbstractWebsocketListener<UserData, OpenUserDataStreamRequest>
{
    private final ObjectMapper objectMapper = JsonUtils.createDefaultObjectMapper();
    private PrivateCXClient privateCXClient;
    private Credential credential;
    private ExecutorService executor;

    public HitbtcUserDataWebsocketListener(
        final CXStream<UserData> stream,
        final CXStreamListener<UserData> listener)
    {
        super(stream,
            "userDataStream",
            null, // ignore CurrencyPair
            null, // ignore converter
            listener,
            CXIds.HITBTC);
    }

    public HitbtcUserDataWebsocketListener setCredential(Credential credential) {
        this.credential = credential;
        try {
            this.privateCXClient = CXClientFactory.createPrivateCXClient(CXIds.HITBTC, credential);
        } catch (CTTKException e) {
            logger.error("{}", e);
        }
        return this;
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        this.executor = Executors.newSingleThreadExecutor();
        webSocket.send(getAuthSocketSessionPayload());
        webSocket.send(getActiveOrdersPayload());
        webSocket.send(getTradingBalancePayload());
        webSocket.send(getSubscribeReportsPayload());

        logger.info("[{}] Open : {}", id, channel);
        this.listener.onOpen(stream);
    }

    @Override
    public void onClosed(WebSocket webSocket, int code, String reason) {
        logger.info("[{}] Closed: - {} {} {}", id, channel, code, reason);
        this.executor.shutdown();
        this.listener.onClosed(stream, code, reason);
    }

    private String getAuthSocketSessionPayload() {
        Map<String, String> authParams = ParamsBuilder.create()
            .put("algo", "BASIC")
            .put("pKey", credential.getAccessKey())
            .put("sKey", credential.getSecretKey())
            .build();

        Map<String, Object> params = OParamsBuilder.create()
            .put("method", "login")
            .put("params", authParams)
            .build();

        try {
            String payload = objectMapper.writeValueAsString(params);
            return payload;
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private String getActiveOrdersPayload() {
        Map<String, String> requiredParams = ParamsBuilder.create().build();
        Map<String, Object> params = OParamsBuilder.create()
            .put("method", "getOrders")
            .put("params", requiredParams)
            .put("id", id)
            .build();

        try {
            String payload = objectMapper.writeValueAsString(params);
            return payload;
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private String getTradingBalancePayload() {
        Map<String, String> requiredParams = ParamsBuilder.create().build();
        Map<String, Object> params = OParamsBuilder.create()
            .put("method", "getTradingBalance")
            .put("params", requiredParams)
            .put("id", id)
            .build();

        try {
            String payload = objectMapper.writeValueAsString(params);
            return payload;
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private String getSubscribeReportsPayload() {
        Map<String, String> requiredParams = ParamsBuilder.create().build();
        Map<String, Object> params = OParamsBuilder.create()
            .put("method", "subscribeReports")
            .put("params", requiredParams)
            .build();

        try {
            String payload = objectMapper.writeValueAsString(params);
            return payload;
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    protected void handleMessage(WebSocket webSocket, String text)
        throws CTTKException
    {
        HitbtcUtils.checkErrorByResponseMessage(request, text, null);
        if (text.contains("\"result\":true")) return;

        final List<UserData> userDataList = convert(text);
        if (userDataList != null && !userDataList.isEmpty()) {
            userDataList.forEach(d -> listener.onMessage(d));
        }
    }

    public List<UserData> convert(String message)
        throws CTTKException
    {
        List<UserData> userDataList = new ArrayList<>();

        if (message.contains("\"method\":\"report\"")) {
            UserOrder userOrder = convertJsonString(message, HitbtcUserDataReportResponse.class, i -> i.toUserOrder());
            userDataList.add(UserData.of(userOrder));
            if (userOrder.getStatusType() == OrderStatus.PARTIALLY_FILLED
                || userOrder.getStatusType() == OrderStatus.FILLED)
            {
                UserTrade userTrade = convertJsonString(message, HitbtcUserDataReportResponse.class, i -> i.toUserTrade());
                userDataList.add(UserData.of(userTrade));
            }
            updateBalances();
            return userDataList;
        }

        if (message.contains("clientOrderId")) {
            UserOrders userOrders = convertJsonString(message, HitbtcUserDataOrderResponse.class, i -> i.toUserOrders());
            userDataList.add(UserData.of(userOrders));
        } else {
            Balances balances = convertJsonString(message, HitbtcUserDataBalanceResponse.class, i -> i.toBalances());
            userDataList.add(UserData.of(balances));
        }

        return userDataList;
    }

    private <T, R> R convertJsonString(final String response, Class<T> claz, ThrowableFunction<T, R> mapper)
        throws CTTKException
    {
        try {
            return mapper.apply(objectMapper.readValue(response, claz));
        } catch (IOException e) {
            throw new CXAPIRequestException("json parse error ", e)
                .setCxId(CXIds.HITBTC)
                .setExchangeMessage(response);
        }
    }

    private void updateBalances() {
        executor.execute(() -> {
            try {
                Thread.sleep(500l);
                Balances balances = privateCXClient.getAllBalances();
                listener.onMessage(UserData.of(balances));
            } catch (InterruptedException e) {
                logger.error("{}", e);
            } catch (CTTKException e) {
                logger.error("{}", e);
            }
        });
    }

    @Override
    protected String toString(ByteString bytes)
        throws IOException
    {
        StringWriter sw = new StringWriter();
        try (
            final PrintWriter pw = new PrintWriter(sw);
            final ByteArrayInputStream in = new ByteArrayInputStream(bytes.toByteArray());
            final BufferedReader br = new BufferedReader(new InputStreamReader(new GZIPInputStream(in)));)
        {
            String line;
            for (int i = 0; (line = br.readLine()) != null; i++) {
                if (i > 0) pw.println();
                pw.print(line);
            }
        } catch (IOException e) {
            throw e;
        }
        return sw.toString();
    }
}