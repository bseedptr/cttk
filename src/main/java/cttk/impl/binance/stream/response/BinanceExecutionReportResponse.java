package cttk.impl.binance.stream.response;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.OrderSide;
import cttk.PricingType;
import cttk.dto.UserOrder;
import cttk.dto.UserTrade;
import cttk.impl.binance.BinanceUtils;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;

@Data
public class BinanceExecutionReportResponse {
    @JsonProperty("e")
    String eventType;

    @JsonProperty("E")
    Long unixTimeInMilli;

    @JsonProperty("s")
    String symbol;

    @JsonProperty("c")
    String clientOrderId;

    @JsonProperty("S")
    String side;

    @JsonProperty("o")
    String type;

    @JsonProperty("f")
    String timeInForce;

    @JsonProperty("q")
    BigDecimal quantity;

    @JsonProperty("p")
    BigDecimal price;

    @JsonProperty("P")
    BigDecimal stopPrice;

    @JsonProperty("F")
    BigDecimal icebergQuantity;

    @JsonProperty("C")
    String orgClientOrderId;

    @JsonProperty("x")
    String currentExecutionType;    // NEW, CANCELED, REJECTED, TRADE, EXPIRED

    @JsonProperty("X")
    String currentOrderStatus;

    @JsonProperty("r")
    String orderRejectionReason;

    @JsonProperty("i")
    Long orderId;

    @JsonProperty("l")
    BigDecimal lastExecutedQuantity;

    @JsonProperty("z")
    BigDecimal filledQuantity;

    @JsonProperty("L")
    BigDecimal lastExecutedPrice;

    @JsonProperty("n")
    BigDecimal commission;

    @JsonProperty("N")
    String commissionAsset;

    @JsonProperty("T")
    Long transactionUnixTimeInMilli;

    @JsonProperty("t")
    Long tradeId;

    @JsonProperty("w")
    Boolean isOrderWorkings;

    @JsonProperty("m")
    Boolean isMaker;

    public UserOrder toUserOrder() {
        return new UserOrder()
            .setCurrencyPair(BinanceUtils.parseMarketSymbol(symbol))
            .setOid(String.valueOf(orderId))
            .setClientOrderId(clientOrderId)
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(unixTimeInMilli))
            .setSide(OrderSide.of(side))
            .setPricingType(PricingType.of(type))
            .setStatus(currentOrderStatus)
            .setStatusType(BinanceUtils.getStatusType(currentOrderStatus))
            .setPrice(price)
            .setQuantity(quantity)
            .setFilledQuantity(filledQuantity)
            .setRemainingQuantity(MathUtils.subtract(quantity, filledQuantity))
            .setTotal(MathUtils.multiply(price, quantity));
    }

    public UserTrade toUserTrade() {
        return new UserTrade()
            .setCurrencyPair(BinanceUtils.parseMarketSymbol(symbol))
            .setTid(String.valueOf(tradeId))
            .setOid(String.valueOf(orderId))
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(transactionUnixTimeInMilli))
            .setSide(OrderSide.of(side))
            .setPrice(lastExecutedPrice)
            .setQuantity(lastExecutedQuantity)
            .setTotal(MathUtils.multiply(lastExecutedPrice, lastExecutedQuantity))
            .setFee(commission)
            .setFeeCurrency(commissionAsset)
            .setIsMaker(isMaker);
    }
}
