package cttk.impl.bithumb;

import static cttk.CXIds.BITHUMB;
import static cttk.dto.UserOrders.Category.OPEN;
import static cttk.util.DeltaUtils.sleepWithException;

import java.io.IOException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.PricingType;
import cttk.PrivateCXClient;
import cttk.TradingFeeProvider;
import cttk.auth.Credential;
import cttk.dto.Balances;
import cttk.dto.BankAccount;
import cttk.dto.Transfers;
import cttk.dto.UserAccount;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;
import cttk.dto.Wallet;
import cttk.dto.Wallets;
import cttk.dto.WithdrawResult;
import cttk.exception.CTTKException;
import cttk.exception.CXServerException;
import cttk.exception.InsufficientBalanceException;
import cttk.exception.InvalidAmountMaxException;
import cttk.exception.InvalidAmountMinException;
import cttk.exception.InvalidPriceIncrementPrecisionException;
import cttk.exception.InvalidPriceMaxException;
import cttk.exception.InvalidPriceMinException;
import cttk.exception.InvalidQuantityIncrementPrecisionException;
import cttk.exception.OrderNotFoundException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.bithumb.response.BithumbBalanceResponse;
import cttk.impl.bithumb.response.BithumbBankAccountResponse;
import cttk.impl.bithumb.response.BithumbCreateOrderResponse;
import cttk.impl.bithumb.response.BithumbOrderDetailResponse;
import cttk.impl.bithumb.response.BithumbStatusResponse;
import cttk.impl.bithumb.response.BithumbTransfersResponse;
import cttk.impl.bithumb.response.BithumbUserAccountResponse;
import cttk.impl.bithumb.response.BithumbUserOrdersResponse;
import cttk.impl.bithumb.response.BithumbUserTradesResponse;
import cttk.impl.bithumb.response.BithumbWalletResponse;
import cttk.impl.util.CryptoUtils;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.Objects;
import cttk.impl.util.ParamsBuilder;
import cttk.impl.util.UriUtils;
import cttk.request.CancelOrderRequest;
import cttk.request.CreateMarketPriceOrderRequest;
import cttk.request.CreateOrderRequest;
import cttk.request.GetBankAccountRequest;
import cttk.request.GetOpenUserOrdersRequest;
import cttk.request.GetTransfersDeltaRequest;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserAccountRequest;
import cttk.request.GetUserOrderHistoryDeltaRequest;
import cttk.request.GetUserOrderHistoryRequest;
import cttk.request.GetUserOrderRequest;
import cttk.request.GetUserTradesDeltaRequest;
import cttk.request.GetUserTradesRequest;
import cttk.request.GetWalletRequest;
import cttk.request.WithdrawToBankRequest;
import cttk.request.WithdrawToWalletRequest;
import cttk.util.DeltaUtils;
import cttk.util.StringUtils;
import okhttp3.Headers;
import okhttp3.Request;
import okhttp3.Response;

public class BithumbPrivateClient
    extends AbstractBithumbClient
    implements PrivateCXClient
{
    protected final Credential credential;
    private String accountId;

    public BithumbPrivateClient(Credential credential) {
        super();
        this.credential = credential;
        try {
            this.accountId = this.getUserAccount(new GetUserAccountRequest()).getAid();
        } catch (CTTKException e) {
            this.accountId = CryptoUtils.getMD5String(credential.getAccessKey());
            logger.error("Unable to get account id. accountId is set by access key {}", this.accountId, e);
        }
    }

    @Override
    public UserAccount getUserAccount(GetUserAccountRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("/info/account");

        checkResponseError(request, response);

        return convert(response, BithumbUserAccountResponse.class, (i -> i.toUserAccount()));
    }

    @Override
    public TradingFeeProvider getTradingFeeProvider()
        throws CTTKException
    {
        final HttpResponse response = requestPost("/info/account");

        checkResponseError(null, response);

        return convert(response, BithumbUserAccountResponse.class, (i -> i.getTradingFeeProvider()));
    }

    @Override
    public BankAccount getBankAccount(GetBankAccountRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("/trade/krw_deposit");

        checkResponseError(request, response);

        return convert(response, BithumbBankAccountResponse.class, (i -> i.toBankAccount()));
    }

    @Override
    public WithdrawResult withdraw(WithdrawToBankRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("/trade/krw_withdrawal",
            ParamsBuilder.create()
                .putIfNotNull("bank", request.getBankCode())
                .putIfNotNull("account", request.getAccountNo())
                .putIfNotNull("price", request.getAmount())
                .build());

        checkResponseError(request, response);

        convert(response, BithumbStatusResponse.class);

        // bithumb does not support withdraw id
        return new WithdrawResult();
    }

    @Override
    public Wallet getWallet(GetWalletRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("/info/wallet_address",
            ParamsBuilder.build("currency", request.getCurrency()));

        checkResponseError(request, response);

        return convert(response, BithumbWalletResponse.class, (i -> i.toWallet()));
    }

    @Override
    public Wallets getAllWallets()
        throws CTTKException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public WithdrawResult withdraw(WithdrawToWalletRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("/trade/btc_withdrawal",
            ParamsBuilder.create()
                .putIfNotNull("currency", request.getCurrency())
                .putIfNotNull("units", request.getQuantity())
                .putIfNotNull("address", request.getAddress())
                .putIfNotNull("destination", request.getDestinationTag())
                .putIfNotNull("destination", request.getPaymentId())
                .build());

        checkResponseError(request, response);

        convert(response, BithumbStatusResponse.class);

        // bithumb does not support withdraw id
        return new WithdrawResult();
    }

    @Override
    public Balances getAllBalances()
        throws CTTKException
    {
        final HttpResponse response = requestPost("/info/balance",
            ParamsBuilder.build("currency", "ALL"));

        checkResponseError(null, response);

        return convert(response, BithumbBalanceResponse.class, i -> i.getBalances());
    }

    /**
     * Canceled order cannot be checked
     * @param request
     * @return
     * @throws CTTKException
     * @throws UnsupportedCurrencyPairException
     */
    @Override
    public UserOrder getUserOrder(GetUserOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("/info/orders",
            ParamsBuilder.build("currency", request.getBaseCurrency()));

        try {
            checkResponseError(request, response);
        } catch (OrderNotFoundException e) {
            return getCloseUserOrderByOrderId(request);
        }

        final UserOrders userOrders = convert(response, BithumbUserOrdersResponse.class).toUserOrders(PricingType.LIMIT);
        if (userOrders.isNotEmpty()) {
            Optional<UserOrder> opOrder = userOrders.getOrders().stream()
                .filter(order -> order != null && order.getOid().equals(request.getOid())).findFirst();
            if (opOrder.isPresent()) {
                UserOrder userOrder = opOrder.get();
                if (request.isWithTrades()) {
                    userOrder.setTrades(getOrderTradeList(userOrder.getOid(), userOrder.getSide(), userOrder.getBaseCurrency()));
                }
                return userOrder;
            }
            return getCloseUserOrderByOrderId(request);
        }
        return null;
    }

    private UserOrder getCloseUserOrderByOrderId(GetUserOrderRequest request)
        throws CTTKException
    {
        List<UserTrade> orderTradeList = getOrderTradeList(request.getOid(), null, request.getBaseCurrency());
        if (orderTradeList != null && !orderTradeList.isEmpty()) {
            UserOrder userOrder = new UserOrder().setCurrencyPair(request).setOid(request.getOid()).setSide(request.getSide()).setStatusType(OrderStatus.FILLED);
            if (request.isWithTrades()) userOrder.setTrades(orderTradeList);
            return userOrder;
        }
        return null;
    }

    private List<UserTrade> getOrderTradeList(String orderId, OrderSide orderSide, String currency)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        if (orderId == null) return null;

        try {
            final HttpResponse response = requestPost("/info/order_detail",
                ParamsBuilder.create()
                    .put("order_id", orderId)
                    .put("type", BithumbUtils.toString(orderSide))
                    .put("currency", currency)
                    .build());

            checkResponseError(null, response);

            return convert(response, BithumbOrderDetailResponse.class, i -> i.getUserTradeList());
        } catch (CTTKException e) {
            if (e.getMessage().contains("거래 체결내역이 존재하지 않습니다")) {
                return null;
            } else {
                throw e;
            }
        }
    }

    @Override
    public UserOrders getOpenUserOrders(GetOpenUserOrdersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("/info/orders",
            ParamsBuilder.create()
                .putIfNotNull("currency", request.getBaseCurrency())
                .putIfNotNull("type", request.getSide())
                .putIfNotNull("count", request.getCount())
                .build());
        try {
            checkResponseError(request, response);
        } catch (OrderNotFoundException e) {
            return new UserOrders().setCategory(OPEN);
        }

        final UserOrders userOrders = convert(response, BithumbUserOrdersResponse.class, (i -> i.toUserOrders(PricingType.LIMIT)));
        if (request.isWithTrades()) {
            // TODO fill trades
        }

        return userOrders.filterBy(i -> OrderStatus.isOpen(i.getStatusType()));
    }

    @Override
    public UserOrders getAllOpenUserOrders(boolean withTrades)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(getCxId());
    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(getCxId());
    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(getCxId());
    }

    @Override
    public UserOrder createOrder(CreateOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final ZonedDateTime requestTime = ZonedDateTime.now(ZoneOffset.UTC);
        final HttpResponse response = requestPost("/trade/place",
            ParamsBuilder.create()
                .putIfNotNull("order_currency", request.getBaseCurrency())
                .putIfNotNull("payment_currency", request.getQuoteCurrency())
                .putIfNotNull("units", request.getQuantity())
                .putIfNotNull("price", request.getPrice())
                .putIfNotNull("type", BithumbUtils.toString(request.getSide()))
                .build());

        checkOrderResponseError(request, response);
        checkResponseError(request, response);

        return convert(response, BithumbCreateOrderResponse.class, (i -> i.toUserOrder(request))).setDateTime(requestTime);
    }

    @Override
    public void cancelOrder(CancelOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("/trade/cancel",
            ParamsBuilder.create()
                .putIfNotNull("currency", request.getBaseCurrency())
                .putIfNotNull("type", BithumbUtils.toString(request.getSide()))
                .putIfNotNull("order_id", request.getOrderId())
                .build());

        checkResponseError(request, response);

        convert(response, BithumbStatusResponse.class);
    }

    @Override
    public UserOrder createMarketPriceOrder(CreateMarketPriceOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost(getMarketPriceOrderEndpoint(request.getSide()),
            ParamsBuilder.create()
                .putIfNotNull("currency", request.getBaseCurrency())
                .putIfNotNull("units", request.getQuantity())
                .build());

        checkResponseError(request, response);

        return convert(response, BithumbCreateOrderResponse.class, (i -> i.toUserOrder()));
    }

    private String getMarketPriceOrderEndpoint(OrderSide type) {
        switch (type) {
            case SELL:
                return "/trade/market_sell";
            case BUY:
                return "/trade/market_buy";
            default:
                return null;
        }
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        if (!StringUtils.isEmpty(request.getOrderId())) {
            List<UserTrade> orderTradeList = getOrderTradeList(request.getOrderId(), null, request.getBaseCurrency());
            return new UserTrades().setList(orderTradeList);
        }
        final HttpResponse response = requestPost("/info/user_transactions",
            ParamsBuilder.create()
                .putIfNotNull("currency", request.getBaseCurrency())
                .putIfNotNull("offset", request.getOffset())
                .putIfNotNull("count", request.getCount())
                .putIfNotNull("searchGb", Objects.nvl(request.getStatus(), "0"))
                .build());

        checkResponseError(request, response);

        return convert(response, BithumbUserTradesResponse.class, (i -> i.toUserTrades(request.getBaseCurrency(), accountId)));
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP o: offset,<<<(exception)
        // Request twice and merge them
        return new UserTrades()
            .addAll(getUserTrades(request, "1"))  // 1 means 구매완료
            .addAll(getUserTrades(request, "2")); // 2 means 판매완료
    }

    private UserTrades getUserTrades(GetUserTradesDeltaRequest request, String status)
        throws CTTKException
    {
        final GetUserTradesRequest partialRequest = new GetUserTradesRequest()
            .setCurrencyPair(request)
            .setStatus(status)
            .setCount(50);

        if (request.hasStartDateTime()) {
            final UserTrades userTrades = new UserTrades();
            int offset = 0;
            while (true) {
                partialRequest.setOffset(offset);
                final UserTrades templUserTrades = getUserTrades(partialRequest);

                if (templUserTrades.isEmpty()) {
                    break;
                } else if (templUserTrades.hasOlderOne(request.getStartDateTime())) {
                    templUserTrades.filterByDateTimeGTE(request.getStartDateTime());
                    userTrades.addAll(templUserTrades);
                    break;
                } else {
                    userTrades.addAll(templUserTrades);
                }

                offset += templUserTrades.size();
                sleepWithException(request.getSleepTimeMillis(), BITHUMB);
            }

            return userTrades;
        } else {
            return getUserTrades(partialRequest);
        }
    }

    @Override
    public Transfers getTransfers(GetTransfersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("/info/user_transactions",
            ParamsBuilder.create()
                .putIfNotNull("currency", request.getCurrency())
                .putIfNotNull("offset", request.getOffset())
                .putIfNotNull("count", request.getCount())
                .putIfNotNull("searchGb", Objects.nvl(request.getStatus(), "0")) // 0: get all transaction first and filter by transfer type
                .build());

        checkResponseError(request, response);

        return convert(response, BithumbTransfersResponse.class, (i -> i.toTransfers(request.getCurrency(), accountId)));
    }

    @Override
    public Transfers getTransfers(GetTransfersDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP o: offset,<<<(exception)
        return new Transfers()
            .addAll(getTransfers(request, "4"))  // 4 means 입금
            .addAll(getTransfers(request, "5")); // 5 means 출금
    }

    private Transfers getTransfers(GetTransfersDeltaRequest request, String status)
        throws CTTKException
    {
        final GetTransfersRequest partialRequest = new GetTransfersRequest()
            .setCurrency(request)
            .setStatus(status)
            .setCount(50);

        if (request.hasStartDateTime()) {
            final Transfers transfers = new Transfers();
            int offset = 0;
            while (true) {
                partialRequest.setOffset(offset);
                final Transfers tempTransfers = getTransfers(partialRequest);

                if (tempTransfers.isEmpty()) {
                    break;
                } else if (tempTransfers.hasOlderOne(request.getStartDateTime())) {
                    tempTransfers.filterByDateTimeGTE(request.getStartDateTime());
                    transfers.addAll(tempTransfers);
                    break;
                } else {
                    transfers.addAll(tempTransfers);
                }

                offset += tempTransfers.size();
                sleepWithException(request.getSleepTimeMillis(), BITHUMB);
            }
            return transfers;
        } else {
            return getTransfers(partialRequest);
        }
    }

    private HttpResponse requestPost(String endpoint)
        throws CTTKException
    {
        return requestPost(endpoint, ParamsBuilder.create().build());
    }

    private HttpResponse requestPost(String endpoint, Map<String, String> params)
        throws CTTKException
    {
        params = putEndpoint(params, endpoint);

        final Request request = new Request.Builder()
            .url(getHostUrl(endpoint))
            .headers(createHttpHeaders(credential, endpoint, params))
            .post(super.createEncodedFormBody(params))
            .build();

        debug(request, params);

        try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
            return toHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    private Map<String, String> putEndpoint(Map<String, String> params, String endpoint) {
        if (params == null) params = new HashMap<>();
        params.put("endpoint", endpoint);
        return params;
    }

    private Headers createHttpHeaders(Credential credential, String endpoint, Map<String, String> params) {
        final String nonce = nonce();
        final String data = toDataString(endpoint, params, nonce);
        final String signature = CryptoUtils.hmacSha512HexBase64(data, credential.getSecretKey());

        return new Headers.Builder()
            .add("Api-Key", credential.getAccessKey())
            .add("Api-Sign", signature)
            .add("Api-Nonce", nonce)
            .add("api-client-type", "2")
            .build();
    }

    private static String nonce() {
        return String.valueOf(System.currentTimeMillis());
    }

    private static String toDataString(String endpoint, Map<String, String> params, String nonce) {
        return endpoint + ";" + UriUtils.toEncodedQueryParamsString(params) + ";" + nonce;
    }

    private void checkOrderResponseError(CreateOrderRequest request, HttpResponse response)
        throws CTTKException
    {
        String exchangeErrorCode;
        String exchangeMessage;
        try {
            final Map<?, ?> map = objectMapper.readValue(response.getBody(), HashMap.class);
            exchangeErrorCode = map.containsKey("status") ? String.valueOf(map.get("status")) : "";
            exchangeMessage = map.containsKey("message") ? String.valueOf(map.get("message")) : "";
        } catch (IOException e) {
            return;
        }
        if (exchangeErrorCode.equals("0000")) return;

        if (exchangeMessage.contains("매수금액이 사용가능 KRW 를 초과")) {
            throw new InsufficientBalanceException()
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus())
                .setExchangeErrorCode(exchangeErrorCode)
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity());
        } else if (exchangeMessage.contains("코인 수량은 소수점") && exchangeMessage.contains("째짜리 까지만 입력가능합니다.")) {
            throw new InvalidQuantityIncrementPrecisionException()
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus())
                .setExchangeErrorCode(exchangeErrorCode)
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity());
        } else if ((exchangeMessage.contains("주문가격은") && exchangeMessage.contains("단위로 입력이 가능 합니다."))
            || exchangeMessage.contains("금액(소수점)을 확인해 주십시오."))
        {
            throw new InvalidPriceIncrementPrecisionException()
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus())
                .setExchangeErrorCode(exchangeErrorCode)
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity());
        } else if (exchangeMessage.contains("주문량이 사용가능") && exchangeMessage.contains("초과하였습니다")) {
            throw new InsufficientBalanceException()
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus())
                .setExchangeErrorCode(exchangeErrorCode)
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity());
        } else if (exchangeMessage.contains("1회 최대 수량은") && exchangeMessage.contains("미만입니다.")) {
            throw new InvalidAmountMaxException()
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus())
                .setExchangeErrorCode(exchangeErrorCode)
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity());
        } else if (exchangeMessage.contains("1회 최대 금액은") && exchangeMessage.contains("미만입니다.")) {
            throw new InvalidPriceMaxException()
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus())
                .setExchangeErrorCode(exchangeErrorCode)
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity());
        } else if (exchangeMessage.contains("주문 가능 금액은") && exchangeMessage.contains("이상 가능합니다.")) {
            throw new InvalidPriceMinException()
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus())
                .setExchangeErrorCode(exchangeErrorCode)
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity());
        } else if (exchangeMessage.contains("주문 가능 금액은") && exchangeMessage.contains("이하 가능합니다.")) {
            throw new InvalidPriceMaxException()
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus())
                .setExchangeErrorCode(exchangeErrorCode)
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity());
        } else if (exchangeMessage.contains("최소 구매수량은")) {
            throw new InvalidAmountMinException()
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus())
                .setExchangeErrorCode(exchangeErrorCode)
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity());
        }
    }
}
