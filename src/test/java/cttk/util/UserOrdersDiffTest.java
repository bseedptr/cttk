package cttk.util;

import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import cttk.CXIds;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;

public class UserOrdersDiffTest {
    @Test
    public void testSame() {
        final UserOrders prev = new UserOrders()
            .setCxId(CXIds.BINANCE)
            .setOrders(Arrays.asList(o1()));

        final UserOrders curr = new UserOrders()
            .setCxId(CXIds.BINANCE)
            .setOrders(Arrays.asList(o1()));

        final UserOrdersDiff diff = UserOrdersDiff.diff(prev, curr);
        Assert.assertFalse(diff.isChanged());
    }

    @Test
    public void testAdded() {
        final UserOrders prev = new UserOrders()
            .setCxId(CXIds.BINANCE)
            .setOrders(Arrays.asList(o1()));

        final UserOrders curr = new UserOrders()
            .setCxId(CXIds.BINANCE)
            .setOrders(Arrays.asList(o1(), o2()));

        final UserOrdersDiff diff = UserOrdersDiff.diff(prev, curr);
        Assert.assertTrue(diff.isAdded());
    }

    @Test
    public void testRemoved() {
        final UserOrders prev = new UserOrders()
            .setCxId(CXIds.BINANCE)
            .setOrders(Arrays.asList(o1()));

        final UserOrders curr = new UserOrders()
            .setCxId(CXIds.BINANCE)
            .setOrders(Arrays.asList(o2()));

        final UserOrdersDiff diff = UserOrdersDiff.diff(prev, curr);
        Assert.assertTrue(diff.isRemoved());
    }

    @Test
    public void testValueChanged() {
        final UserOrders prev = new UserOrders()
            .setCxId(CXIds.BINANCE)
            .setOrders(Arrays.asList(o1()));

        final UserOrders curr = new UserOrders()
            .setCxId(CXIds.BINANCE)
            .setOrders(Arrays.asList(o1Filled()));

        final UserOrdersDiff diff = UserOrdersDiff.diff(prev, curr);
        Assert.assertTrue(diff.isValueChanged());
    }

    private UserOrder o1() {
        return new UserOrder()
            .setOid("o1")
            .setPrice(new BigDecimal("0.1"))
            .setQuantity(new BigDecimal("0.2"))
            .setFilledQuantity(new BigDecimal("0"));
    }

    private UserOrder o1Filled() {
        return new UserOrder()
            .setOid("o1")
            .setPrice(new BigDecimal("0.1"))
            .setQuantity(new BigDecimal("0.2"))
            .setFilledQuantity(new BigDecimal("0.1"));
    }

    private UserOrder o2() {
        return new UserOrder()
            .setOid("o2")
            .setPrice(new BigDecimal("0.2"))
            .setQuantity(new BigDecimal("0.1"))
            .setFilledQuantity(new BigDecimal("0"));
    }
}
