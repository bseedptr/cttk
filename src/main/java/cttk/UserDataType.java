package cttk;

public enum UserDataType {
    UserOrder, UserOrders,

    UserTrade, UserTrades,

    Transfer, Transfers,

    Balance, Balances
}
