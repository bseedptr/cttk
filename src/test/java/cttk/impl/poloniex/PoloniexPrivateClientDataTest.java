package cttk.impl.poloniex;

import java.time.ZonedDateTime;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import cttk.CXIds;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.dto.UserOrder;
import cttk.dto.UserTrades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.AbstractPrivateCXClientDataTest;
import cttk.impl.util.ExNotRaisedException;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserOrderRequest;
import cttk.request.GetUserTradesRequest;

public class PoloniexPrivateClientDataTest
    extends AbstractPrivateCXClientDataTest
{
    @AfterClass
    public static void cleanup() {
        cleanup(PoloniexPrivateClientDataTest.class);
    }

    @Override
    public String getCxId() {
        return CXIds.POLONIEX;
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetUserAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetUserAccount();
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetBankAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetBankAccount();
    }

    @Test(expected = UnsupportedMethodException.class)
    public void testGetUserOrderHistory()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetUserOrderHistory();
    }

    @Test
    public void testGetUserOrderCurrencyPairNotNull()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();

        final UserOrder testUserOrder = this.getTestUserOrder();

        Assert.assertNotNull(testUserOrder);

        final GetUserOrderRequest request = new GetUserOrderRequest()
            .setSide(testUserOrder.getSide())
            .setOid(testUserOrder.getOid())
            .setClientOrderId(testUserOrder.getClientOrderId());

        final UserOrder userOrder = client.getUserOrder(request);
        logger.debug("request : {}", request);
        logger.debug("userOrder : {}", userOrder);

        Assert.assertNotNull(userOrder);
        Assert.assertNotNull(userOrder.getBaseCurrency());
        Assert.assertNotNull(userOrder.getQuoteCurrency());
    }

    @Test(expected = UnsupportedMethodException.class)
    public void testGetUserOrderHistoryWrongCurrency()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetUserOrderHistoryWrongCurrency();
    }

    @Test
    @Override
    public void testGetTransfers()
        throws CTTKException
    {
        final Transfers transfers = client.getTransfers(
            new GetTransfersRequest()
                .setStartDateTime(ZonedDateTime.now().minusYears(2))
                .setEndDateTime(ZonedDateTime.now()));

        logger.info("{}", transfers.toString());

        Assert.assertNotNull(transfers);
        Assert.assertTrue(transfers.isNotEmpty());
        for (Transfer transfer : transfers.getTransfers()) {
            Assert.assertNotNull(transfer);
            Assert.assertNotNull(transfer.getTid());
            Assert.assertNotNull(transfer.getTxId());
            Assert.assertNotNull(transfer.getStatus());
            Assert.assertNotNull(transfer.getType());
        }
    }

    @Test(expected = ExNotRaisedException.class)
    @Override
    public void testGetTransfersWrongCurrency()
        throws CTTKException
    {
        // Poloniex doesn't need currency as a request parameter
        // We do not need to test for WrongCurrency
        throw new ExNotRaisedException(raiseExceptionMsg);
    }

    @Test
    public void testGetUserTradesByOrderId()
        throws CTTKException
    {
        String filledOrderId = "12326013850";

        UserTrades userTrades = client.getUserTrades(new GetUserTradesRequest().setOrderId(filledOrderId));

        Assert.assertNotNull(userTrades);
        Assert.assertFalse(userTrades.getList().isEmpty());
    }
}
