package cttk.impl.huobi.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class HuobiAccount {
    Long id;
    @JsonProperty("user-id")
    Long userId;
    String type;
    String state;
    @JsonProperty("list")
    List<HuobiBalance> balances;
}

@Data
class HuobiBalance {
    String currency;
    String type;
    String balance;
}