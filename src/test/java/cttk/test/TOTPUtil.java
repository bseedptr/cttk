package cttk.test;

import com.warrenstrange.googleauth.GoogleAuthenticator;

public class TOTPUtil {
    private static final GoogleAuthenticator authenticator = new GoogleAuthenticator();

    public static String getTotpCode(String secret) {
        return String.valueOf(authenticator.getTotpPassword(secret));
    }
}
