package cttk.impl.bibox.stream.response;

import java.io.IOException;

import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.impl.bibox.response.BiboxTrades;

public class BiboxWebsocketTradesResponse extends BiboxWebsocketListResponse<BiboxTrades> {
    public BiboxWebsocketTradesResponse() {
        super(BiboxTrades.class);
    }

    public Trades toTrades()
        throws IOException, CTTKException
    {
        return this.getBiboxDataFirst().toTrades();
    }
}
