package cttk.impl.chargingpolicy;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import cttk.CXIds;
import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.dto.ChargingPolicy;
import cttk.dto.ChargingPolicy.Criteria;
import cttk.exception.CTTKException;
import cttk.exception.InvalidChargingPolicyException;

public class DefaultChargingPolicyProviderTest {
    @Rule
    public ExpectedException ee = ExpectedException.none();

    private DefaultChargingPolicyProvider provider;
    private CurrencyPair pair;

    @Before
    public void setUp() {
        provider = new DefaultChargingPolicyProvider(CXIds.BINANCE);
        pair = CurrencyPair.of("EOS", "ETH");
    }

    @Test
    public void testGetChargingPoliciesWhenCurrencyPairIsNull()
        throws CTTKException
    {
        ee.expect(InvalidChargingPolicyException.class);
        ee.expectMessage("Both CurrencyPair and OrderSide are required.");

        provider.getChargingPolicies(null, OrderSide.BUY);
    }

    @Test
    public void testGetChargingPoliciesWhenOrderSideIsNull()
        throws CTTKException
    {
        ee.expect(InvalidChargingPolicyException.class);
        ee.expectMessage("Both CurrencyPair and OrderSide are required.");

        provider.getChargingPolicies(pair, null);
    }

    @Test
    public void getChargingPoliciesWhenBuy()
        throws CTTKException
    {
        List<ChargingPolicy> policies = provider.getChargingPolicies(pair, OrderSide.BUY);
        assertThat(policies, hasSize(1));

        ChargingPolicy policy = policies.get(0);
        assertThat(policy.getPairString(), is(pair.getPairString()));
        assertThat(policy.getOrderSide(), is(OrderSide.BUY));
        assertThat(policy.getFeeCurrency(), is(pair.getBaseCurrency()));
        assertThat(policy.getCriteria(), is(Criteria.SUB));
        assertFalse(policy.isCXToken());
    }

    @Test
    public void getChargingPoliciesWhenSell()
        throws CTTKException
    {
        List<ChargingPolicy> policies = provider.getChargingPolicies(pair, OrderSide.SELL);
        assertThat(policies, hasSize(1));

        ChargingPolicy policy = policies.get(0);
        assertThat(policy.getPairString(), is(pair.getPairString()));
        assertThat(policy.getOrderSide(), is(OrderSide.SELL));
        assertThat(policy.getFeeCurrency(), is(pair.getQuoteCurrency()));
        assertThat(policy.getCriteria(), is(Criteria.SUB));
        assertFalse(policy.isCXToken());
    }
}