package cttk;

import cttk.dto.OrderBook;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCXException;
import cttk.request.GetOrderBookRequest;
import cttk.request.OpenOrderBookStreamRequest;
import cttk.util.CXStreamPipe;
import cttk.util.OrderBookSnapshotPipe;
import cttk.util.ReopenPolicy;
import cttk.util.StringUtils;

public class CXStreams {
    public static final CXStream<OrderBook> openOrderBookSnapshotStream(
        String cxId,
        OpenOrderBookStreamRequest request,
        CXStreamListener<OrderBook> listener)
        throws UnsupportedCXException, CTTKException
    {
        switch (StringUtils.toUpper(cxId)) {
            case CXIds.HUOBI:
            case CXIds.UPBIT:
            case CXIds.BIBOX:
                return CXClientFactory.createCXStreamOpener(cxId)
                    .openOrderBookStream(request, listener);
            case CXIds.OKEX:
            case CXIds.BITFINEX:
            case CXIds.HITBTC:
                return CXClientFactory.createCXStreamOpener(cxId)
                    .openOrderBookStream(request, createOrderBookSnapshotPipe(listener));
            case CXIds.BINANCE:
                return CXClientFactory.createCXStreamOpener(cxId)
                    .openOrderBookStream(request, createBinanceOrderBookSnapshotPipe(cxId, request, listener));
            default:
                return CXClientFactory.createPollingCXStreamOpener(cxId).openOrderBookStream(request, listener);
        }
    }

    public static final CXStream<OrderBook> openOrderBookSnapshotStream(
        String cxId,
        OpenOrderBookStreamRequest request,
        CXStreamListener<OrderBook> listener,
        ReopenPolicy reopenPolicy)
        throws UnsupportedCXException, CTTKException
    {
        switch (StringUtils.toUpper(cxId)) {
            case CXIds.HUOBI:
            case CXIds.UPBIT:
            case CXIds.BIBOX:
                return CXClientFactory.createCXStreamOpener(cxId)
                    .openOrderBookStream(request, listener, reopenPolicy);
            case CXIds.OKEX:
            case CXIds.BITFINEX:
            case CXIds.HITBTC:
                return CXClientFactory.createCXStreamOpener(cxId)
                    .openOrderBookStream(request, createOrderBookSnapshotPipe(listener), reopenPolicy);
            case CXIds.BINANCE:
                return CXClientFactory.createCXStreamOpener(cxId)
                    .openOrderBookStream(request, createBinanceOrderBookSnapshotPipe(cxId, request, listener), reopenPolicy);
            default:
                return CXClientFactory.createPollingCXStreamOpener(cxId).openOrderBookStream(request, listener, reopenPolicy);
        }
    }

    private static OrderBookSnapshotPipe createBinanceOrderBookSnapshotPipe(
        final String cxId,
        final OpenOrderBookStreamRequest request,
        final CXStreamListener<OrderBook> listener)
    {
        return new OrderBookSnapshotPipe(listener) {
            @Override
            public void onOpen(CXStream<OrderBook> stream) {
                try {
                    final PublicCXClient client = CXClientFactory.createPublicCXClient(cxId);
                    final OrderBook initialOrderBook = client.getOrderBook(new GetOrderBookRequest()
                        .setCurrencyPair(request)
                        .setCount(request.getCount())
                        .setPrecision(request.getPrecision()));
                    this.onMessage(initialOrderBook);
                } catch (Exception e) {
                    try {
                        stream.close();
                        this.onFailure(stream, e);
                    } catch (Exception e1) {
                        this.onFailure(stream, e1);
                    }
                }
                listener.onOpen(stream);
            }
        };
    }

    public static final CXStreamPipe<OrderBook> createOrderBookSnapshotPipe(CXStreamListener<OrderBook> listener) {
        return new OrderBookSnapshotPipe(listener);
    }
}
