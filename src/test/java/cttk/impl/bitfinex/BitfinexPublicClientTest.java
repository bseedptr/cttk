package cttk.impl.bitfinex;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.AbstractPublicCXClientTest;
import cttk.impl.util.ExNotRaisedException;
import cttk.request.GetOrderBookRequest;

public class BitfinexPublicClientTest
    extends AbstractPublicCXClientTest
{
    @Override
    public String getCxId() {
        return CXIds.BITFINEX;
    }

    @Override
    protected long getThreadSleepMillis() {
        return 3500l;
    }

    @Test
    @Override
    public void testGetRecentTradesWithUnsupportedQuoteCurrency()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();
        super.testGetRecentTradesWithUnsupportedQuoteCurrency();
    }

    @Test
    @Override
    public void testGetOrderBookWrongCurrency()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();

        final GetOrderBookRequest request = new GetOrderBookRequest()
            .setCurrencyPair(getInvalidCurrencyPair())
            .setCount(25);
        try {
            client.getOrderBook(request);
        } catch (UnsupportedCurrencyPairException e) {
            logger.error("base cur = " + getInvalidCurrencyPair().getBaseCurrency());
            logger.error("quote cur = " + getInvalidCurrencyPair().getQuoteCurrency());
            assertEquals(getInvalidCurrencyPair().getBaseCurrency(), e.getBaseCurrency());
            assertEquals(getInvalidCurrencyPair().getQuoteCurrency(), e.getQuoteCurrency());
            assertEquals(this.getCxId(), e.getCxId());
            return;
        }

        throw new ExNotRaisedException(raiseExceptionMsg);
    }
}
