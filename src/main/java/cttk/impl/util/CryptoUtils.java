package cttk.impl.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.zip.GZIPInputStream;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class CryptoUtils {
    private static final String DEFAULT_ENCODING = "UTF-8";
    private static final String HMAC_SHA512 = "HmacSHA512";
    private static final String HMAC_SHA384 = "HmacSHA384";
    private static final String HMAC_SHA256 = "HmacSHA256";
    private static final String HMAC_MD5 = "HmacMD5";

    private static final String MD5 = "MD5";
    private static final String SHA256 = "SHA-256";

    private static final char HEX_DIGITS[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

    public static byte[] sha256(byte[] data) {
        try {
            MessageDigest digest = MessageDigest.getInstance(SHA256);
            return digest.digest(data);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static byte[] hmacSha512(String data, String key) {
        try {
            return hmacSha512(data, key.getBytes(DEFAULT_ENCODING));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static byte[] hmacSha512(String data, byte[] key) {
        return hmacSha512(data.getBytes(), key);
    }

    public static byte[] hmacSha512(byte[] data, byte[] key) {
        try {
            Mac mac = Mac.getInstance(HMAC_SHA512);
            mac.init(new SecretKeySpec(key, HMAC_SHA512));
            return mac.doFinal(data);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (InvalidKeyException e) {
            throw new RuntimeException(e);
        }
    }

    public static byte[] hmacSha384(String data, String key) {
        try {
            Mac mac = Mac.getInstance(HMAC_SHA384);
            mac.init(new SecretKeySpec(key.getBytes(DEFAULT_ENCODING), HMAC_SHA384));
            return mac.doFinal(data.getBytes());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (InvalidKeyException e) {
            throw new RuntimeException(e);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    // for v1 api in bitfinex
    public static String hmacDigest(String msg, String keyString) {
        String digest = null;
        try {
            SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"), HMAC_SHA384);
            Mac mac = Mac.getInstance(HMAC_SHA384);
            mac.init(key);

            byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));

            StringBuffer hash = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(0xFF & bytes[i]);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = hash.toString();
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        } catch (InvalidKeyException e) {
            throw new RuntimeException(e);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        return digest;
    }

    // for v1 api in bibox
    public static String hmacMD5(String msg, String keyString) {
        String digest = null;
        try {
            SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"), HMAC_MD5);
            Mac mac = Mac.getInstance(HMAC_MD5);
            mac.init(key);

            byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));

            StringBuffer hash = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(0xFF & bytes[i]);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = hash.toString();
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        } catch (InvalidKeyException e) {
            throw new RuntimeException(e);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        return digest;
    }

    public static byte[] hmacSha256(String data, String key) {
        try {
            Mac mac = Mac.getInstance(HMAC_SHA256);
            mac.init(new SecretKeySpec(key.getBytes(DEFAULT_ENCODING), HMAC_SHA256));
            return mac.doFinal(data.getBytes());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (InvalidKeyException e) {
            throw new RuntimeException(e);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getMD5String(String str) {
        try {
            if (str == null || str.trim().length() == 0) {
                return "";
            }
            byte[] bytes = str.getBytes();
            MessageDigest messageDigest = MessageDigest.getInstance(MD5);
            messageDigest.update(bytes);
            bytes = messageDigest.digest();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(HEX_DIGITS[(bytes[i] & 0xf0) >> 4] + ""
                    + HEX_DIGITS[bytes[i] & 0xf]);
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getJWT(String headerJson, String payloadJson, String secretKey) {
        String header = base64(headerJson.getBytes(StandardCharsets.UTF_8));
        String payload = base64(payloadJson.getBytes(StandardCharsets.UTF_8));
        String content = String.format("%s.%s", header, payload);

        byte[] signatureBytes = hmacSha256(content, secretKey);
        String signature = base64(signatureBytes);

        return String.format("%s.%s", content, signature);
    }

    public static byte[] hex(byte[] bytes) {
        if (bytes == null)
            return null;
        try {
            return hexString(bytes).getBytes(DEFAULT_ENCODING);
        }
        // will not happen
        catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    public static String hexString(byte[] bytes) {
        final char[] hexArray = "0123456789abcdef".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static String base64(String str) {
        if (str == null)
            return null;
        try {
            return base64(str.getBytes(DEFAULT_ENCODING));
        }
        // will not happen
        catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    public static String base64(byte[] bytes) {
        return Base64.getEncoder().encodeToString(bytes);
    }

    public static String hmacSha512HexBase64(String data, String key) {
        return base64(hex(hmacSha512(data, key)));
    }

    public static String hmacSha512Hex(String data, String key) {
        return hexString(hmacSha512(data, key));
    }

    public static String hmacSha384Hex(String data, String key) {
        return base64(hex(hmacSha384(data, key)));
    }

    public static String hmacSha256Hex(String data, String key) {
        return hexString(hmacSha256(data, key));
    }

    public static String hmacSha256Base64(String data, String key) {
        return base64(hmacSha256(data, key));
    }

    public static byte[] decodeBase64(String base64) { return Base64.getDecoder().decode(base64); }

    public static String decodeGzip(byte[] bytes)
        throws IOException
    {
        StringWriter sw = new StringWriter();
        try (
            final PrintWriter pw = new PrintWriter(sw);
            final ByteArrayInputStream in = new ByteArrayInputStream(bytes);
            final BufferedReader br = new BufferedReader(new InputStreamReader(new GZIPInputStream(in))))
        {
            String line;
            for (int i = 0; (line = br.readLine()) != null; i++) {
                if (i > 0) pw.println();
                pw.print(line);
            }
        }
        return sw.toString();
    }
}
