package cttk.impl.stream.eachmarket;

import static cttk.impl.stream.eachmarket.PollingLevel.LEVEL1;
import static cttk.impl.stream.eachmarket.PollingLevel.LEVEL2;
import static cttk.impl.stream.eachmarket.PollingLevel.LEVEL3;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.CurrencyPair;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.UserData;
import cttk.dto.UserOrder;
import cttk.exception.CTTKException;
import cttk.impl.PostSettingPrivateCXClient;
import cttk.request.OpenUserDataStreamRequest;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EachMarketPollingUserDataStream
    implements CXStream<UserData>
{
    private Map<PollingLevel, EachMarketPollingUserDataThread> threads;

    protected final CXStreamListener<UserData> listener;
    protected final LimitedCXClient limitedClient;
    protected final LevelPairMapInitializer initializer;
    protected final OpenUserDataStreamRequest request;
    protected LevelPairMap levelPairMap;

    protected final Map<CurrencyPair, UserOrder> lastCloseUserOrderMap = new HashMap<>();

    public EachMarketPollingUserDataStream(
        final PublicCXClient publicClient,
        final PrivateCXClient privateClient,
        final OpenUserDataStreamRequest request,
        final CXStreamListener<UserData> listener)
        throws CTTKException
    {
        Objects.requireNonNull(request.getRateLimit(), "Rate limit is required of each market polling stream.");
        Objects.requireNonNull(request.getCoolTime(), "Cool time is required of each market polling stream.");

        this.listener = listener;
        this.limitedClient = new LimitedCXClient(publicClient, getPostSettingPrivateCXClient(privateClient), request.getMaxPollRetryCount(), request.getRateLimit());
        this.initializer = new LevelPairMapInitializer(limitedClient);
        this.request = request;
        open();
    }

    private PostSettingPrivateCXClient getPostSettingPrivateCXClient(PrivateCXClient privateClient) {
        return privateClient instanceof PostSettingPrivateCXClient ? (PostSettingPrivateCXClient) privateClient : new PostSettingPrivateCXClient(privateClient);
    }

    @Override
    public CXStream<UserData> open()
        throws CTTKException
    {
        if (threads == null) {
            startPollingThreads();
        } else {
            restartPollingThreads();
        }
        return this;
    }

    @Override
    public void close()
        throws Exception
    {
        for (EachMarketPollingUserDataThread eachMarketPollingUserDataThread : threads.values()) {
            eachMarketPollingUserDataThread.close();
        }
        log.info("Stream closed");
    }

    @Override
    public CXStreamListener<UserData> getListener() {
        return listener;
    }

    private void startPollingThreads()
        throws CTTKException
    {
        threads = new HashMap<>();
        levelPairMap = initializer.init();

        threads.put(LEVEL1, createPollingThread(LEVEL1));
        threads.put(LEVEL2, createPollingThread(LEVEL2));
        threads.put(LEVEL3, createPollingThread(LEVEL3));

        threads.values().forEach(Thread::start);
    }

    private void restartPollingThreads() {
        threads.forEach((pollingLevel, oldThread) -> {
            if (!oldThread.isAlive()) {
                EachMarketPollingUserDataThread newThread = createPollingThread(pollingLevel);
                threads.put(pollingLevel, newThread);
                newThread.start();
            }
        });
    }

    protected EachMarketPollingUserDataThread createPollingThread(PollingLevel pollingLevel) {
        return new EachMarketPollingUserDataThread(this, pollingLevel, listener, limitedClient, request, levelPairMap, lastCloseUserOrderMap);
    }
}
