package cttk.impl.stream;

import cttk.CXStreamConfig;
import cttk.CXStreamListener;
import cttk.PublicCXClient;
import cttk.dto.OrderBook;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.util.ThrowableFunction;
import cttk.request.GetOrderBookRequest;

public class PollingOrderBookStream
    extends PollingCXStream<OrderBook>
{
    public PollingOrderBookStream(final PublicCXClient client, final CXStreamListener<OrderBook> listener, final GetOrderBookRequest request) {
        super(client, createOrderBookGetter(request), listener);
    }

    public PollingOrderBookStream(final PublicCXClient client, final CXStreamConfig config, final CXStreamListener<OrderBook> listener, final GetOrderBookRequest request) {
        super(client, createOrderBookGetter(request), config, listener);
    }

    private static final ThrowableFunction<PublicCXClient, OrderBook> createOrderBookGetter(final GetOrderBookRequest request) {
        return new ThrowableFunction<PublicCXClient, OrderBook>() {
            @Override
            public OrderBook apply(PublicCXClient client)
                throws CTTKException, UnsupportedCurrencyPairException
            {
                OrderBook orderBook = client.getOrderBook(request);
                if (orderBook != null && !orderBook.isMerged()) {
                    orderBook = orderBook.merge();
                }
                return orderBook;
            }
        };
    }
}
