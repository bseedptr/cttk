package cttk.impl.korbit;

import static cttk.util.StringUtils.toLower;
import static cttk.util.StringUtils.toUpper;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.TransferType;

public class KorbitUtils {
    public static String getMarketSymbol(CurrencyPair currencyPair) {
        return currencyPair == null ? null
            : toLower(currencyPair.getBaseCurrency()) + "_" + toLower(currencyPair.getQuoteCurrency());
    }

    public static CurrencyPair parseMarketSymbol(String symbol) {
        if (symbol == null || symbol.trim().isEmpty()) return null;
        symbol = symbol.trim();
        final String[] arr = symbol.split("_");
        return CurrencyPair.of(toUpper(arr[0]), toUpper(arr[1]));
    }

    public static String toStdCurrencySymbol(String currency) {
        return toUpper(currency);
    }

    public static String toString(OrderSide orderSide) {
        switch (orderSide) {
            case BUY:
                return "buy";
            case SELL:
                return "sell";
            default:
                return null;
        }
    }

    public static String toTransferTypeString(TransferType transferType) {
        if (transferType == null) return null;
        switch (transferType) {
            case DEPOSIT:
                return "deposit";
            case WITHDRAWAL:
                return "withdrawal";
            default:
                return null;
        }
    }
}
