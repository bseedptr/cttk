package cttk.impl.korbit;

import org.junit.Assert;
import org.junit.Test;

import cttk.CXIds;
import cttk.dto.Ticker;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.AbstractPublicCXClientTest;
import cttk.request.GetTickerRequest;

public class KorbitPublicClientTest
    extends AbstractPublicCXClientTest
{
    @Override
    public String getCxId() {
        return CXIds.KORBIT;
    }

    @Test
    @Override
    public void testGetTicker()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final GetTickerRequest request = new GetTickerRequest()
            .setCurrencyPair(getValidCurrencyPair());

        final Ticker ticker = client.getTicker(request);

        Assert.assertNotNull(ticker);
        Assert.assertNotNull(ticker.getChangePercent());
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetAllTickers()
        throws CTTKException
    {
        super.testGetAllTickers();
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetTradeHistory()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetTradeHistory();
    }

}