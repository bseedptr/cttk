package cttk.impl.kraken.response;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;
import cttk.impl.kraken.KrakenUtils;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
public class KrakenUserTradesResponse
    extends KrakenResponse<KrakenUserTrades>
{
    public UserTrades toUserTrades() {
        return new UserTrades()
            .setTrades(result == null ? null : result.getUserTrades());
    }
}

@Data
class KrakenUserTrades {
    Long count;
    Map<String, KrakenUserTrade> trades;

    List<UserTrade> getUserTrades() {
        if (trades == null) return null;
        return trades.entrySet().stream()
            .map(e -> e.getValue().toUserTrade().setTid(e.getKey()))
            .collect(Collectors.toList());
    }
}

@Data
@Accessors(chain = true)
class KrakenUserTrade {
    String ordertxid;
    String pair;
    BigDecimal time;
    String type;
    BigDecimal price;
    BigDecimal cost;
    BigDecimal fee;
    BigDecimal vol;
    BigDecimal margin;
    String misc;

    UserTrade toUserTrade() {
        CurrencyPair pair = KrakenUtils.parseMarketSymbol(this.pair);
        String feeCurrency = KrakenUtils.guessFeeCurrency(pair);
        return new UserTrade()
            .setCurrencyPair(pair)
            .setOid(ordertxid)
            .setDateTime(DateTimeUtils.zdtFromEpochSecond(time))
            .setSide(KrakenUtils.parseOrderSide(type))
            .setPrice(price)
            .setQuantity(vol)
            .setTotal(cost)
            .setFee(fee)
            .setFeeCurrency(feeCurrency);
    }
}
