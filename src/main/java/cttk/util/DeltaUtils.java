package cttk.util;

import static java.util.stream.Collectors.toMap;

import java.util.ArrayList;
import java.util.Map;
import java.util.function.Function;

import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;
import cttk.exception.CXAPIRequestException;
import cttk.impl.util.Objects;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DeltaUtils {
    private static final long TIME = 350l;

    /**
     * false if error or true.
     */
    public static boolean sleep(Long millis) {
        try {
            Thread.sleep(Objects.nvl(millis, TIME));
            return true;
        } catch (InterruptedException e) {
            log.error("An unexpected error occurred on delta API.", e);
            return false;
        }
    }

    /**
     * Raise an exception if error
     */
    public static void sleepWithException(Long millis, String cxId)
        throws CXAPIRequestException
    {
        try {
            Thread.sleep(Objects.nvl(millis, TIME));
        } catch (InterruptedException e) {
            log.error("An unexpected error occurred on delta API.", e);
            throw new CXAPIRequestException("An error occurred while processing delta API. Try agin later.").setCxId(cxId);
        }
    }

    public static Map<String, UserOrder> convertToMap(UserOrders userOrders) {
        return userOrders.getList().stream().collect(toMap(UserOrder::getOid, Function.identity(), (o1, o2) -> o1));
    }

    public static Map<String, UserTrade> convertToMap(UserTrades userTrades) {
        return userTrades.getList().stream().collect(toMap(UserTrade::getTid, Function.identity(), (o1, o2) -> o1));
    }

    public static Map<String, Transfer> convertToMap(Transfers transfers) {
        return transfers.getList().stream().collect(toMap(Transfer::getTid, Function.identity(), (o1, o2) -> o1));
    }

    public static UserOrders convertUserOrdersFromMap(Map<String, UserOrder> map) {
        return new UserOrders().addAll(new ArrayList<>(map.values()));
    }

    public static Transfers convertTransfersFromMap(Map<String, Transfer> map) {
        return new Transfers().addAll(new ArrayList<>(map.values()));
    }

    public static UserTrades convertUserTradesFromMap(Map<String, UserTrade> map) {
        return new UserTrades().addAll(new ArrayList<>(map.values()));
    }

    public static int calcNextSeconds(UserOrder one, UserOrder two) {
        return one != null && one.getOid().equals(two.getOid()) ? 0 : 1;
    }

    public static int calcNextSeconds(UserTrade one, UserTrade two) {
        return one != null && one.getTid().equals(two.getTid()) ? 0 : 1;
    }

    public static int calcNextSeconds(Transfer one, Transfer two) {
        return one != null && one.getTid().equals(two.getTid()) ? 0 : 1;
    }
}
