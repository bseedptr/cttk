package cttk.dto;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class UserOrderTest {
    @Test
    public void testEqualsById() {
        UserOrder a = new UserOrder().setOid("1");
        UserOrder b = new UserOrder().setOid("1");

        assertTrue(UserOrder.equalsById(a, b));
    }

    @Test
    public void testEqualsWhenBothAreNull() {
        UserOrder a = null;
        UserOrder b = null;

        assertTrue(UserOrder.equalsById(a, b));
    }

    @Test
    public void testEqualsByIdWhenEitherIsNull() {
        UserOrder a = null;
        UserOrder b = new UserOrder().setOid("2");
        assertFalse(UserOrder.equalsById(a, b));

        a = new UserOrder().setOid("1");
        b = null;
        assertFalse(UserOrder.equalsById(a, b));
    }

    @Test
    public void testEqualsByIdWhenIdIsDifferent() {
        UserOrder a = new UserOrder().setOid("1");
        UserOrder b = new UserOrder().setOid("2");

        assertFalse(UserOrder.equalsById(a, b));
    }
}