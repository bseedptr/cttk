package cttk.example;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.List;

import cttk.CTTK;
import cttk.CXClientFactory;
import cttk.CurrencyPair;
import cttk.OrderVerifier;
import cttk.OrderVerifierFactory;
import cttk.PublicCXClient;
import cttk.dto.MarketInfo;
import cttk.test.TestConfig;

public class OrderVerifierExample {
    public static void main(String[] args)
        throws IOException
    {
        List<String> cxIds = CTTK.getSupportedCxIds();

        try (PrintWriter pw = new PrintWriter("round.txt")) {

            for (String cxId : cxIds) {
                final TestConfig testConfig = TestConfig.readFromClasspathYaml("config/" + cxId.toLowerCase() + ".yml");
                final CurrencyPair currencyPair = testConfig.getValidCurrencyPair();

                try {
                    final PublicCXClient client = CXClientFactory.createPublicCXClient(cxId);

                    final MarketInfo marketInfo = client.getMarketInfos().getMarketInfo(currencyPair);

                    final OrderVerifier orderVerifier = OrderVerifierFactory.create(cxId, marketInfo);

                    final BigDecimal price = new BigDecimal("0.001023232323323");

                    final BigDecimal rounded = orderVerifier.roundPrice(price);

                    pw.println(cxId);
                    pw.println(currencyPair.getPairString());
                    pw.println(price);
                    pw.println(rounded);
                    pw.println(orderVerifier.checkPriceRange(rounded));
                } catch (Exception e) {
                    e.printStackTrace();
                    continue;
                }
            }
        }
    }
}
