package cttk.impl.poloniex;

import org.junit.Test;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.impl.AbstractAPILimitTest;

public class PoloniexAPILimitTest
    extends AbstractAPILimitTest
{
    @Override
    public String getCxId() {
        return CXIds.POLONIEX;
    }

    @Test
    @Override
    public void testPublicAPILimit()
        throws CTTKException, InterruptedException
    {
        testCase((args) -> {
            return publicClient.getAllTickers();
        }, false, 1000);
        // We can't reach public API limit
        logger.info("We can't reach public API limit in " + getCxId());
    }

}
