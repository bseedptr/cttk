package cttk.impl.bittrex.response;

import static cttk.util.StringUtils.parseBigInteger;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.FillType;
import cttk.OrderSide;
import cttk.dto.Trade;
import lombok.Data;

@Data
public class BittrexMarketHistory {
    @JsonProperty("Id")
    String id;
    @JsonProperty("TimeStamp")
    String timeStamp;
    @JsonProperty("Quantity")
    BigDecimal quantity;
    @JsonProperty("Price")
    BigDecimal price;
    @JsonProperty("Total")
    BigDecimal total;
    @JsonProperty("FillType")
    String fillType;
    @JsonProperty("OrderType")
    String orderType;

    public Trade toTrade() {
        return new Trade()
            .setSno(parseBigInteger(id))
            .setDateTime(OffsetDateTime.parse(timeStamp + "-00:00").atZoneSameInstant(ZoneOffset.UTC))
            .setTid(id)
            .setSide(OrderSide.of(orderType))
            .setFillType(FillType.of(fillType))
            .setQuantity(quantity)
            .setPrice(price)
            .setTotal(total);
    }
}
