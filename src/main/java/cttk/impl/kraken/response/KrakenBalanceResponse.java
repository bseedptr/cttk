package cttk.impl.kraken.response;

import static cttk.impl.kraken.KrakenUtils.toKrakenAssetSymbol;
import static cttk.impl.kraken.KrakenUtils.toStdCurrencySymbol;
import static cttk.util.StringUtils.parseBigDecimal;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import cttk.dto.Balance;
import cttk.dto.Balances;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class KrakenBalanceResponse
    extends KrakenResponse<Map<String, String>>
{
    public Balance getBalance(String currency) {
        if (this.result == null) return null;

        return new Balance()
            .setCurrency(currency)
            .setTotal(parseBigDecimal(this.result.get(toKrakenAssetSymbol(currency))));
    }

    public Balances toBalances() {
        return new Balances()
            .setBalances(getBalanceList());
    }

    private List<Balance> getBalanceList() {
        if (this.result == null) return null;

        return this.result.entrySet().stream()
            .map(e -> new Balance().setCurrency(toStdCurrencySymbol(e.getKey())).setTotal(parseBigDecimal(e.getValue())))
            .collect(Collectors.toList());
    }
}
