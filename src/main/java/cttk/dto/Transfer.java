package cttk.dto;

import static cttk.impl.util.Objects.nvl;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cttk.AbstractCurrencyHolder;
import cttk.DateTimeGettable;
import cttk.TransferStatus;
import cttk.TransferType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ToString(callSuper = true)
public class Transfer
    extends AbstractCurrencyHolder<Transfer>
    implements DateTimeGettable
{
    String userId;
    String chain;
    TransferType type;
    String tid;
    String txId;
    Long index;
    BigDecimal amount;
    BigDecimal fee;
    String status;
    TransferStatus statusType;
    String fromAddress; // from address (coinone)
    String address;
    String addressTag;
    ZonedDateTime createdDateTime;
    ZonedDateTime updatedDateTime;
    ZonedDateTime completedDateTime;
    Integer confirmations;
    String description;

    @Override
    @JsonIgnore
    public ZonedDateTime getDateTime() {
        return nvl(completedDateTime, updatedDateTime, createdDateTime);
    }

    public String getIndexAsString(long valueToAdd) {
        return index == null ? null : String.valueOf(index + valueToAdd);
    }

    public static boolean equalsById(Transfer a, Transfer b) {
        return a == null && b == null
            || a != null && b != null && a.getTid().equals(b.getTid());
    }
}