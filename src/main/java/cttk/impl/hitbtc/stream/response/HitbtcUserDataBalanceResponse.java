package cttk.impl.hitbtc.stream.response;

import static cttk.impl.hitbtc.HitbtcUtils.toStdCurrencySymbol;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import cttk.dto.Balance;
import cttk.dto.Balances;
import cttk.impl.util.MathUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HitbtcUserDataBalanceResponse
    extends HitbtcUserDataWebsocketResponse
{
    List<HitbtcUserDataBalance> result;

    public Balances toBalances() {
        return new Balances().setBalances(toBalanceList(result));
    }

    static List<Balance> toBalanceList(List<HitbtcUserDataBalance> list) {
        if (list == null) return null;

        return list.stream()
            .map(t -> t.toBalance())
            .collect(Collectors.toList());
    }
}

@Data
class HitbtcUserDataBalance {
    String currency;
    BigDecimal available;
    BigDecimal reserved;

    BigDecimal getTotal() {
        return MathUtils.add(available, reserved);
    }

    public Balance toBalance() {
        return new Balance()
            .setCurrency(toStdCurrencySymbol(currency))
            .setTotal(getTotal())
            .setAvailable(available)
            .setInUse(reserved);
    }
}