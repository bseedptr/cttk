package cttk.impl.kraken.response;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.dto.Wallet;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class KrakenDepositAddressesResponse
    extends KrakenResponse<List<KrakenDepositAddress>>
{
    public Wallet toWallet(String currency) {
        return new Wallet()
            .setCurrency(currency)
            .setAddresses(getWalletAddresses());
    }

    private List<String> getWalletAddresses() {
        if (this.result == null || this.result.isEmpty()) return null;
        return this.result.stream().map(i -> i.getAddress()).collect(Collectors.toList());
    }
}

@Data
class KrakenDepositAddress {
    String address;     // address = deposit address
    Long expiretm;      // expiretm = expiration time in unix timestamp, or 0 if not expiring
    @JsonProperty("new")
    Boolean isNew;      // new = whether or not address has ever been used
}