package cttk.impl.stream;

import static cttk.impl.stream.eachmarket.PollingLevel.LEVEL1;
import static cttk.impl.stream.eachmarket.PollingLevel.LEVEL2;
import static cttk.impl.stream.eachmarket.PollingLevel.LEVEL3;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.hasItems;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import cttk.CurrencyPair;
import cttk.CurrencyPair.SimpleCurrencyPair;
import cttk.exception.CTTKException;
import cttk.impl.stream.eachmarket.LevelPairMap;
import cttk.impl.stream.eachmarket.LevelPairMapInitializer;
import cttk.impl.stream.eachmarket.LimitedCXClient;
import lombok.extern.slf4j.Slf4j;

@RunWith(MockitoJUnitRunner.class)
@Slf4j
public class LevelPairMapInitializerTest {
    private static SimpleCurrencyPair BTC_KRW;
    private static SimpleCurrencyPair ETH_KRW;
    private static SimpleCurrencyPair EOS_KRW;
    private static SimpleCurrencyPair XRP_KRW;
    private static SimpleCurrencyPair LTC_KRW;
    private static List<CurrencyPair> ALL_PAIRS;

    @InjectMocks
    private LevelPairMapInitializer initializer;
    @Mock
    private LimitedCXClient client;

    @BeforeClass
    public static void setUpGlobal() {
        BTC_KRW = new SimpleCurrencyPair("btc", "krw");
        ETH_KRW = new SimpleCurrencyPair("eth", "krw");
        EOS_KRW = new SimpleCurrencyPair("eos", "krw");
        XRP_KRW = new SimpleCurrencyPair("xrp", "krw");
        LTC_KRW = new SimpleCurrencyPair("ltc", "krw");
        ALL_PAIRS = new ArrayList<>(asList(BTC_KRW, ETH_KRW, EOS_KRW, XRP_KRW, LTC_KRW));
    }

    @Test
    public void testInit()
        throws CTTKException
    {
        List<String> balances = new ArrayList<>(asList("eos", "ltc"));
        when(client.getAllCurrencyPairs()).thenReturn(ALL_PAIRS);
        when(client.getCurrenciesHaveBalance()).thenReturn(balances);
        when(client.isExistsOpenOrders(BTC_KRW)).thenReturn(true);
        when(client.isExistsOpenOrders(ETH_KRW)).thenReturn(true);
        when(client.isExistsOpenOrders(EOS_KRW)).thenReturn(false);
        when(client.isExistsOpenOrders(XRP_KRW)).thenReturn(false);
        when(client.isExistsOpenOrders(LTC_KRW)).thenReturn(false);

        LevelPairMap map = initializer.init();

        log.debug("{}", map);

        assertEquals(map.countAll(), 5);
        assertEquals(map.count(LEVEL1), 2);
        assertEquals(map.count(LEVEL2), 2);
        assertEquals(map.count(LEVEL3), 1);
        assertThat("Does not match pairs.", map.getPairs(LEVEL1), hasItems(BTC_KRW, ETH_KRW));
        assertThat("Does not match pairs.", map.getPairs(LEVEL2), hasItems(EOS_KRW, LTC_KRW));
        assertThat("Does not match pairs.", map.getPairs(LEVEL3), hasItems(XRP_KRW));

        InOrder o = inOrder(client);
        o.verify(client).getAllCurrencyPairs();
        o.verify(client, times(5)).isExistsOpenOrders(any(CurrencyPair.class));
        o.verify(client).getCurrenciesHaveBalance();
    }

    @Test(expected = CTTKException.class)
    public void testInitWhenErrorOccured()
        throws CTTKException
    {
        when(client.getAllCurrencyPairs()).thenThrow(new CTTKException());

        initializer.init();
    }
}