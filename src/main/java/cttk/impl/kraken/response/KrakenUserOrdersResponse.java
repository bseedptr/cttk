package cttk.impl.kraken.response;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import cttk.OrderStatus;
import cttk.PricingType;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrade;
import cttk.impl.kraken.KrakenUtils;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class KrakenUserOrdersResponse
    extends KrakenResponse<KrakenAllUserOrders>
{
    public UserOrders toUserOrdersFromOpen() {
        return new UserOrders()
            .setOrders(result.getOpen().getUserOrderList());

    }

    public UserOrders toUserOrdersFromClosed() {
        // support available count (= total count)
        return new UserOrders()
            .setOrders(result.getClosed().getUserOrderList());
    }
}

@Data
class KrakenAllUserOrders {
    private Long count;
    private KrakenUserOrders open;
    private KrakenUserOrders closed;
}

@Data
class KrakenUserOrders {
    private Map<String, KrakenUserOrder> map = new HashMap<>();

    @JsonAnyGetter
    public Map<String, KrakenUserOrder> any() {
        return this.map;
    }

    @JsonAnySetter
    public void set(String name, KrakenUserOrder value) {
        this.map.put(name, value);
    }

    @JsonIgnore
    public List<UserOrder> getUserOrderList() {
        if (map.isEmpty()) return null;

        return map.entrySet().stream()
            .map(e -> e.getValue().toUserOrder().setOid(e.getKey()))
            .filter(e -> e != null)
            .collect(Collectors.toList());
    }
}

@Data
class KrakenUserOrder {
    String refid;       //refid = Referral order transaction id that created this order
    String userref;     //userref = user reference id
    String status;      //status = status of order:
    //  pending = order pending book entry
    //  open = open order
    //  closed = closed order
    //  canceled = order canceled
    //  expired = order expired
    Long opentm;        //opentm = unix timestamp of when order was placed
    Long starttm;       //starttm = unix timestamp of order start time (or 0 if not set)
    Long expiretm;      //expiretm = unix timestamp of order end time (or 0 if not set)
    KrakenOrderDesc descr;       //descr = order description info

    BigDecimal vol;         //vol = volume of order (base currency unless viqc set in oflags)
    BigDecimal vol_exec;    //vol_exec = volume executed (base currency unless viqc set in oflags)
    BigDecimal cost;        //cost = total cost (quote currency unless unless viqc set in oflags)
    BigDecimal fee;         //fee = total fee (quote currency)
    BigDecimal price;       //price = average price (quote currency unless viqc set in oflags)
    BigDecimal stopprice;   //stopprice = stop price (quote currency, for trailing stops)
    BigDecimal limitprice;  //limitprice = triggered limit price (quote currency, when limit based order type triggered)
    String misc;        //misc = comma delimited list of miscellaneous info
    //    stopped = triggered by stop price
    //    touched = triggered by touch price
    //    liquidated = liquidation
    //    partial = partial fill
    String oflags;      //oflags = comma delimited list of order flags
    //    viqc = volume in quote currency
    //    fcib = prefer fee in base currency (default if selling)
    //    fciq = prefer fee in quote currency (default if buying)
    //    nompp = no market price protection
    List<String> trades;    //trades = array of trade ids related to order (if trades info requested and data available)

    public UserOrder toUserOrder() {
        return new UserOrder()
            .setCurrencyPair(KrakenUtils.parseMarketSymbol(this.descr.pair))
            .setOid(refid)
            .setDateTime(DateTimeUtils.zdtFromEpochSecond(opentm))
            .setSide(KrakenUtils.parseOrderSide(descr.type))
            .setStatus(status)
            .setStatusType(getStatusType())
            .setPricingType(PricingType.of(descr.ordertype.toUpperCase()))
            .setPrice(price)
            .setQuantity(vol)
            .setFilledQuantity(vol_exec)
            .setRemainingQuantity(MathUtils.subtract(vol, vol_exec))
            .setTotal(cost)
            .setFee(fee)
            .setTrades(getTradeList());
    }

    private OrderStatus getStatusType() {
        if ("pending".equals(status)) {
            return OrderStatus.UNFILLED;
        } else if ("open".equals(status)) {
            if (vol_exec == null || vol_exec.compareTo(BigDecimal.ZERO) == 0) {
                return OrderStatus.UNFILLED;
            } else if (vol_exec != null && vol_exec.compareTo(BigDecimal.ZERO) > 0 && vol_exec.compareTo(vol) < 0) {
                return OrderStatus.PARTIALLY_FILLED;
            }
        } else if ("closed".equals(status) && vol_exec.compareTo(vol) == 0) {
            return OrderStatus.FILLED;
        } else if ("canceled".equals(status)) {
            return OrderStatus.CANCELED;
        } else if ("expired".equals(status)) {
            return OrderStatus.EXPIRED;
        }

        return null;
    }

    List<UserTrade> getTradeList() {
        if (trades == null) return null;
        return trades.parallelStream()
            .map(i -> new UserTrade().setTid(i))
            .collect(Collectors.toList());
    }
}

@Data
class KrakenOrderDesc {
    String pair;    // pair = asset pair
    String type;    // type = type of order (buy/sell)
    String ordertype;// ordertype = order type (See Add standard order)
    String price;   // price = primary price
    String price2;  // price2 = secondary price
    String leverage;// leverage = amount of leverage
    String order;   // order = order description
    String close;   // close = conditional close order description (if conditional close set)
}
