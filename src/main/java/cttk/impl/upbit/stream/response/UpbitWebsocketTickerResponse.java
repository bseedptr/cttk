package cttk.impl.upbit.stream.response;

import static cttk.impl.util.MathUtils.multiply100;

import java.math.BigDecimal;

import cttk.CurrencyPair;
import cttk.dto.Ticker;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;

@Data
public class UpbitWebsocketTickerResponse {
    String code;
    BigDecimal opening_price;
    BigDecimal high_price;
    BigDecimal low_price;
    BigDecimal trade_price;
    BigDecimal prev_closing_price;

    BigDecimal acc_trade_price;
    String change;
    BigDecimal change_price;
    BigDecimal signed_change_price;
    BigDecimal change_rate;
    BigDecimal signed_change_rate;

    String ask_bid;
    BigDecimal trade_volume;
    BigDecimal acc_trade_volume;

    String trade_date;
    String trade_time;
    Long trade_timestamp;

    BigDecimal acc_ask_volume;
    BigDecimal acc_bid_volume;

    BigDecimal highest_52_week_price;
    String highest_52_week_date;
    BigDecimal lowest_52_week_price;
    String lowest_52_week_date;

    String trade_status;
    String market_state;
    String market_state_for_ios;
    boolean is_trading_suspended;
    String delisting_date;
    String market_warning;

    Long timestamp;
    BigDecimal acc_trade_price_24h;
    BigDecimal acc_trade_volume_24h;

    String stream_type;

    public Ticker toTicker(CurrencyPair currencyPair) {
        return new Ticker()
            .setCurrencyPair(currencyPair)
            .setLastPrice(trade_price)
            .setOpenPrice(opening_price)
            .setHighPrice(high_price)
            .setLowPrice(low_price)
            .setVolume(acc_trade_volume)
            .setQuoteVolume(acc_trade_price)
            .setChange(signed_change_price)
            .setChangePercent(multiply100(signed_change_rate))
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(timestamp));
    }
}