package cttk.impl.poloniex;

import static cttk.OrderSide.BUY;
import static cttk.impl.util.Objects.nvl;
import static cttk.util.StringUtils.toUpper;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.TransferStatus;
import cttk.impl.util.MathUtils;

public class PoloniexUtils {

    public static final String getMarketSymbolOrAll(CurrencyPair currencyPair) {
        return nvl(getMarketSymbol(currencyPair), "ALL");
    }

    public static final String getMarketSymbol(CurrencyPair currencyPair) {
        return currencyPair == null || currencyPair.isBothNull() ? null
            : toPoloniexCurrencySymbol(currencyPair.getQuoteCurrency()) + "_" + toPoloniexCurrencySymbol(currencyPair.getBaseCurrency());
    }

    public static final CurrencyPair parseMarketSymbol(String symbol) {
        String[] arr = symbol.split("_");
        return CurrencyPair.of(toStdCurrencySymbol(arr[1]), toStdCurrencySymbol(arr[0]));
    }

    public static String toString(OrderSide orderSide) {
        switch (orderSide) {
            case BUY:
                return "buy";
            case SELL:
                return "sell";
            default:
                return null;
        }
    }

    /**
     * Convert the given currency symbol to the standard symbol
     * @param currency the currency symbol
     * @return the standard currency symbol
     */
    public static String toStdCurrencySymbol(String currency) {
        if (currency == null) return null;
        final String upper = toUpper(currency);
        switch (upper) {
            case "BCHABC":
                return "BCH";
            default:
                return upper;
        }
    }

    public static String toPoloniexCurrencySymbol(String currency) {
        if (currency == null) return null;
        final String upper = toUpper(currency);
        switch (upper) {
            case "BCH":
                return "BCHABC";
            default:
                return upper;
        }
    }

    static final DateTimeFormatter DTF = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static ZonedDateTime parseZDT(String date) {
        if (date == null) return null;
        return LocalDateTime.parse(date, DTF).atZone(ZoneOffset.UTC);
    }

    public static String guessFeeCurrency(OrderSide side, CurrencyPair pair) {
        return side == BUY ? pair.getBaseCurrency() : pair.getQuoteCurrency();
    }

    public static TransferStatus parseTransferStatus(String type) {
        // TODO CTTK-352 handle all types of transfer status
        if (type == null) return null;
        final String upper = toUpper(type);
        if (upper.contains("COMPLETE")) {
            return TransferStatus.SUCCESS;
        } else if (upper.contains("AWAITING")) {
            return TransferStatus.PENDING;
        } else {
            return null;
        }
    }

    public static BigDecimal calculateFee(OrderSide side, BigDecimal feeRate, BigDecimal price, BigDecimal amount) {
        if (side == null || feeRate == null || price == null || amount == null) {
            return null;
        }

        BigDecimal roundValue;

        if (side == OrderSide.BUY) {
            roundValue = MathUtils.multiply(amount, feeRate);
        } else { // SELL
            BigDecimal amountPrice = MathUtils.multiply(amount, price);
            roundValue = MathUtils.multiply(amountPrice, feeRate);
        }
        // Rounding precision to 8 decimal points
        return roundValue.setScale(8, BigDecimal.ROUND_HALF_UP);
    }
}
