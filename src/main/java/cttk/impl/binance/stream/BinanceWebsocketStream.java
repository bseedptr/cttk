package cttk.impl.binance.stream;

import cttk.CXStreamListener;
import cttk.impl.stream.AbstractWebsocketStream;
import cttk.impl.stream.MessageConverter;
import cttk.request.AbstractCXStreamRequest;
import okhttp3.WebSocketListener;

public class BinanceWebsocketStream<T, R extends AbstractCXStreamRequest<R>>
    extends AbstractWebsocketStream<T, R>
{
    private static final String WSS_API_BINANCE_PRO_WS = "wss://stream.binance.com:9443/ws/";

    public BinanceWebsocketStream(
        final String channel,
        final R request,
        final MessageConverter<T> converter,
        final CXStreamListener<T> listener)
    {
        super(channel, request, converter, listener);
    }

    @Override
    protected String getUrl() {
        return WSS_API_BINANCE_PRO_WS + channel;
    }

    @Override
    protected WebSocketListener createWebSocketListener() {
        return new BinanceWebsocketListener<>(this, channel, request, converter, listener);
    }
}
