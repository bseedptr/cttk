package cttk.request;

import cttk.AbstractCurrencyPair;
import cttk.OrderSide;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class GetOpenUserOrdersRequest
    extends AbstractCurrencyPair<GetOpenUserOrdersRequest>
{
    OrderSide side;
    Integer offset;
    Integer count;
    Integer pageNo;
    private boolean withTrades;

    // upbit wait: 체결대기(default), done(전체 체결 완료), cancel(주문 취소)
    String state;

    public GetOpenUserOrdersRequest setSide(OrderSide side) {
        this.side = side;
        return this;
    }

    public GetOpenUserOrdersRequest setSide(String side) {
        this.side = OrderSide.of(side);
        return this;
    }
}
