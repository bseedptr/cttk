package cttk.impl.korbit;

import static cttk.CXIds.KORBIT;
import static cttk.dto.UserOrders.Category.ALL;
import static cttk.dto.UserOrders.Category.OPEN;
import static cttk.impl.korbit.KorbitUtils.getMarketSymbol;
import static cttk.util.DeltaUtils.sleepWithException;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import cttk.CXClientFactory;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.TradingFeeProvider;
import cttk.auth.AuthToken;
import cttk.auth.AuthTokenProvider;
import cttk.auth.Credential;
import cttk.dto.Balances;
import cttk.dto.BankAccount;
import cttk.dto.MarketInfos;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.dto.UserAccount;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;
import cttk.dto.Wallet;
import cttk.dto.Wallets;
import cttk.dto.WithdrawResult;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXServerException;
import cttk.exception.InvalidAmountMinException;
import cttk.exception.InvalidOrderException;
import cttk.exception.MaxNumOrderException;
import cttk.exception.OrderNotFoundException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.korbit.response.KorbitBallancesResponse;
import cttk.impl.korbit.response.KorbitCreateOrderResponse;
import cttk.impl.korbit.response.KorbitStatusResponse;
import cttk.impl.korbit.response.KorbitTransfer;
import cttk.impl.korbit.response.KorbitUserAccountResponse;
import cttk.impl.korbit.response.KorbitUserOpenOrder;
import cttk.impl.korbit.response.KorbitUserOrder;
import cttk.impl.korbit.response.KorbitUserTrade;
import cttk.impl.korbit.response.KorbitVolumeResponse;
import cttk.impl.korbit.response.KorbitWalletsResponse;
import cttk.impl.util.HttpHeaders;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ParamsBuilder;
import cttk.request.CancelOrderRequest;
import cttk.request.CancelWithdrawToWalletRequest;
import cttk.request.CreateMarketPriceOrderRequest;
import cttk.request.CreateOrderRequest;
import cttk.request.GetBankAccountRequest;
import cttk.request.GetOpenUserOrdersRequest;
import cttk.request.GetTransfersDeltaRequest;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserAccountRequest;
import cttk.request.GetUserOrderHistoryDeltaRequest;
import cttk.request.GetUserOrderHistoryRequest;
import cttk.request.GetUserOrderRequest;
import cttk.request.GetUserTradesDeltaRequest;
import cttk.request.GetUserTradesRequest;
import cttk.request.GetWalletRequest;
import cttk.request.WithdrawToBankRequest;
import cttk.request.WithdrawToWalletRequest;
import cttk.util.CTTKExceptionUtils;
import okhttp3.Headers;
import okhttp3.Request;
import okhttp3.Response;

public class KorbitPrivateClient
    extends AbstractKorbitClient
    implements PrivateCXClient
{
    protected final Credential credential;
    protected final AuthTokenProvider authTokenProvider;
    protected MarketInfos marketInfos;

    public KorbitPrivateClient(Credential credential, AuthTokenProvider authTokenProvider) {
        super();
        this.credential = credential;
        this.authTokenProvider = authTokenProvider;
        try {
            final PublicCXClient pubClient = CXClientFactory.createPublicCXClient(this.getCxId());
            marketInfos = pubClient.getMarketInfos();
        } catch (CTTKException e) {
            logger.error("Failed to getMarketInfos while creating CoinonePrivateClient.");
        }

    }

    @Override
    public UserAccount getUserAccount(GetUserAccountRequest request)
        throws CTTKException
    {
        final HttpResponse response = requestGetWithToken("/user/info", null);

        checkResponseError(request, response);

        return convert(response, KorbitUserAccountResponse.class, (i -> i.toUserAccount()));
    }

    /**
     * Canceled order cannot be checked
     * @param request
     * @return
     * @throws CTTKException
     * @throws UnsupportedCurrencyPairException
     */
    @Override
    public UserOrder getUserOrder(GetUserOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGetWithToken("/user/orders",
            ParamsBuilder.create()
                .putIfNotNull("currency_pair", getMarketSymbol(request))
                .putIfNotNull("id", request.getOid())
                //.putIfNotNull("status", request.getStatus())
                .build());

        checkResponseError(request, response);

        try {
            return convertList(response, KorbitUserOrder.class,
                (data) -> {
                    if (data != null && !data.isEmpty()) {
                        return data.get(0).toUserOrder();
                    }
                    return null;
                });
        } catch (IOException e) {
            throw new CXAPIRequestException(e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public UserOrders getOpenUserOrders(GetOpenUserOrdersRequest request)
        throws CTTKException
    {
        final HttpResponse response = requestGetWithToken("/user/orders/open",
            ParamsBuilder.create()
                .put("currency_pair", getMarketSymbol(request))
                .putIfNotNull("offset", request.getOffset())
                .putIfNotNull("limit", request.getCount())
                .build());

        checkResponseError(request, response);

        try {
            return convertList(response, KorbitUserOpenOrder.class,
                (data) -> new UserOrders().setOrders(toUserOrderList(data)));
        } catch (IOException e) {
            throw new CXAPIRequestException(e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }

    }

    @Override
    public UserOrders getAllOpenUserOrders(boolean withTrades)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // TODO support id parameter
        final HttpResponse response = requestGetWithToken("/user/orders",
            ParamsBuilder.create()
                .put("currency_pair", getMarketSymbol(request))
                .putIfNotNull("offset", request.getOffset())
                .putIfNotNull("limit", request.getCount())
                .build());
        try {
            checkResponseError(request, response);
        } catch (OrderNotFoundException e) {
            return new UserOrders().setCategory(OPEN);
        }

        UserOrders userOrders;
        try {
            userOrders = convertList(response, KorbitUserOrder.class,
                (data) -> new UserOrders().setOrders(toUserOrderList2(data)));
        } catch (IOException e) {
            throw new CXAPIRequestException(e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
        return userOrders.setCategory(ALL);
    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP o: offset,<<<(exception)
        // do not get 100,000 orders at a time
        // TODO [CTTK-360] It should be known to client that more than 100,000 trades cannot be read at a time
        final GetUserOrderHistoryRequest partialRequest = new GetUserOrderHistoryRequest()
            .setCurrencyPair(request)
            .setCount(40);

        if (request.hasStartDateTime()) {
            final UserOrders userOrders = new UserOrders();
            int offset = 0;
            while (true) {
                partialRequest.setOffset(offset);
                final UserOrders tempUserOrders = getUserOrderHistory(partialRequest);

                if (tempUserOrders.isEmpty()) {
                    break;
                } else if (tempUserOrders.hasOlderOne(request.getStartDateTime())) {
                    tempUserOrders.filterByDateTimeGTE(request.getStartDateTime());
                    userOrders.addAll(tempUserOrders);
                    break;
                } else {
                    userOrders.addAll(tempUserOrders);
                }

                offset += tempUserOrders.size();
                sleepWithException(request.getSleepTimeMillis(), KORBIT);
            }

            return userOrders.setCategory(ALL);
        } else {
            return getUserOrderHistory(partialRequest);
        }
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGetWithToken("/user/transactions",
            ParamsBuilder.create()
                .putIfNotNull("currency_pair", getMarketSymbol(request))
                .putIfNotNull("offset", request.getOffset())
                .putIfNotNull("limit", request.getCount())
                .putIfNotNull("order_id", request.getOrderId())
                .build());

        checkResponseError(request, response);

        try {
            return convertList(response, KorbitUserTrade.class,
                (data) -> new UserTrades()
                    .setTrades(toUserTradeList(data)));
        } catch (IOException e) {
            throw new CXAPIRequestException(e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP o: offset,<<<(exception)
        // do not get 100,000 trades at a time
        // TODO [CTTK-360] It should be known to client that more than 100,000 trades cannot be read at a time
        final GetUserTradesRequest partialRequest = new GetUserTradesRequest()
            .setCurrencyPair(request)
            .setCount(40);

        if (request.hasStartDateTime()) {
            final UserTrades userTrades = new UserTrades();
            int offset = 0;
            while (true) {
                partialRequest.setOffset(offset);
                final UserTrades tempUserTrades = getUserTrades(partialRequest);

                if (tempUserTrades.isEmpty()) {
                    break;
                } else if (tempUserTrades.hasOlderOne(request.getStartDateTime())) {
                    tempUserTrades.filterByDateTimeGTE(request.getStartDateTime());
                    userTrades.addAll(tempUserTrades);
                    break;
                } else {
                    userTrades.addAll(tempUserTrades);
                }

                offset += tempUserTrades.size();
                sleepWithException(request.getSleepTimeMillis(), KORBIT);
            }

            return userTrades;
        } else {
            return getUserTrades(partialRequest);
        }
    }

    @Override
    public Transfers getTransfers(GetTransfersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGetWithToken(
            "/user/transfers",
            ParamsBuilder.create()
                .putIfNotNull("currency", request.getCurrency())
                .putIfNotNull("type", KorbitUtils.toTransferTypeString(request.getType()))
                .putIfNotNull("offset", request.getOffset())
                .putIfNotNull("limit", request.getCount())
                .build());

        checkResponseError(request, response);

        try {
            return convertList(response, KorbitTransfer.class,
                (data) -> new Transfers().setTransfers(toTransferList(data)));
        } catch (IOException e) {
            throw new CXAPIRequestException(e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public Transfers getTransfers(GetTransfersDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP o: offset,<<<(exception)
        // do not get 100,000 trades at a time
        // TODO [CTTK-360] It should be known to client that more than 100,000 trades cannot be read at a time
        final GetTransfersRequest partialRequest = new GetTransfersRequest()
            .setCurrency(request)
            .setCount(40);

        if (request.hasStartDateTime()) {
            final Transfers transfers = new Transfers();
            int offset = 0;
            while (true) {
                partialRequest.setOffset(offset);
                final Transfers tempTransfers = getTransfers(partialRequest);

                if (tempTransfers.isEmpty()) {
                    break;
                } else if (tempTransfers.hasOlderOne(request.getStartDateTime())) {
                    tempTransfers.filterByDateTimeGTE(request.getStartDateTime());
                    transfers.addAll(tempTransfers);
                    break;
                } else {
                    transfers.addAll(tempTransfers);
                }

                offset += tempTransfers.size();
                sleepWithException(request.getSleepTimeMillis(), KORBIT);
            }

            return transfers;
        } else {
            return getTransfers(partialRequest);
        }
    }

    @Override
    public BankAccount getBankAccount(GetBankAccountRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGetWithToken("/user/accounts", null);

        checkResponseError(null, response);

        return convert(response, KorbitWalletsResponse.class, (i -> i.toBankAccount()));
    }

    @Override
    public Wallets getAllWallets()
        throws CTTKException
    {
        final HttpResponse response = requestGetWithToken("/user/accounts", null);

        checkResponseError(null, response);

        return convert(response, KorbitWalletsResponse.class, (i -> i.toWallets()));
    }

    public Balances getAllBalances()
        throws CTTKException
    {
        final HttpResponse response = requestGetWithToken("/user/balances", null);

        checkResponseError(null, response);

        return convert(response, KorbitBallancesResponse.class, (i -> i.toBalances()));
    }

    @Override
    public UserOrder createOrder(CreateOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPostWithToken("/user/orders/" + KorbitUtils.toString(request.getSide()),
            ParamsBuilder.create()
                .putIfNotNull("currency_pair", getMarketSymbol(request))
                .putIfNotNull("type", "limit")
                .putIfNotNull("price", request.getPrice().longValue())
                .putIfNotNull("coin_amount", request.getQuantity())
                .putIfNotNull("nonce", nonce())
                .build());

        checkOrderResponseError(request, response);
        checkResponseError(request, response);

        return fillResponseByRequest(request, convert(response, KorbitCreateOrderResponse.class, i -> i.toUserOrder()));
    }

    @Override
    public UserOrder createMarketPriceOrder(CreateMarketPriceOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPostWithToken("/user/orders/" + KorbitUtils.toString(request.getSide()),
            ParamsBuilder.create()
                .putIfNotNull("currency_pair", getMarketSymbol(request))
                .putIfNotNull("type", "market")
                .putIfNotNull("coin_amount", request.isSellSide() ? request.getQuantity() : null)
                .putIfNotNull("fiat_amount", request.isBuySide() ? request.getQuantity() : null)
                .putIfNotNull("nonce", nonce())
                .build());
        checkResponseError(request, response);

        return fillResponseByRequest(request, convert(response, KorbitCreateOrderResponse.class, i -> i.toUserOrder()));
    }

    @Override
    public void cancelOrder(CancelOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPostWithToken("/user/orders/cancel",
            ParamsBuilder.create()
                .putIfNotNull("currency_pair", getMarketSymbol(request))
                .putIfNotNull("id", request.getOrderId())
                .putIfNotNull("nonce", nonce())
                .build());

        final Integer httpStatusCode = response.getStatus();
        if (response.getBody().contains("\"not_authorized\"")) {
            throw new OrderNotFoundException()
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(httpStatusCode);
        }
        checkResponseError(request, response);

        try {
            convertList(response, KorbitStatusResponse.class, (data) -> Function.identity());
        } catch (IOException e) {
            throw new CXAPIRequestException(e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(httpStatusCode);
        }
    }

    @Override
    public WithdrawResult withdraw(WithdrawToWalletRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPostWithToken("/user/coins/out",
            ParamsBuilder.create()
                .putIfNotNull("currency", request.getCurrency())
                .putIfNotNull("amount", request.getQuantity())
                .putIfNotNull("address", request.getAddress())
                .putIfNotNull("fee_priority", request.getFeePriority())
                .putIfNotNull("nonce", nonce())
                .build());

        checkResponseError(request, response);

        return convert(response, KorbitStatusResponse.class, (i -> i.getWithdrawResult()));
    }

    public void cancelWithdraw(CancelWithdrawToWalletRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPostWithToken("/user/coins/out/cancel",
            ParamsBuilder.create()
                .putIfNotNull("currency", request.getCurrency())
                .putIfNotNull("id", request.getId())
                .putIfNotNull("nonce", nonce())
                .build());

        checkResponseError(request, response);

        convert(response, KorbitStatusResponse.class);
    }

    @Override
    public TradingFeeProvider getTradingFeeProvider()
        throws CTTKException
    {
        final HttpResponse response = requestGetWithToken("/user/volume",
            ParamsBuilder.create()
                .put("currency_pair", "all")
                .put("nonce", nonce())
                .build());

        checkResponseError(null, response);

        return convert(response, KorbitVolumeResponse.class, i -> i.getTradingFeeProvider());
    }

    @Override
    public WithdrawResult withdraw(WithdrawToBankRequest request)
        throws CTTKException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public Wallet getWallet(GetWalletRequest request)
        throws CTTKException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    private List<UserOrder> toUserOrderList(List<KorbitUserOpenOrder> korbitUserOpenOrders) {
        if (korbitUserOpenOrders == null) return null;
        return korbitUserOpenOrders.stream()
            .map(KorbitUserOpenOrder::toUserOrder)
            .filter(i -> i != null)
            .collect(Collectors.toList());
    }

    private List<UserOrder> toUserOrderList2(List<KorbitUserOrder> korbitUserOpenOrders) {
        if (korbitUserOpenOrders == null) return null;
        return korbitUserOpenOrders.stream()
            .map(i -> i.toUserOrder())
            .collect(Collectors.toList());
    }

    private List<UserTrade> toUserTradeList(List<KorbitUserTrade> korbitUserTrades) {
        if (korbitUserTrades == null) return null;
        return korbitUserTrades.stream().map(i -> i.toUserTrade()).collect(Collectors.toList());
    }

    private List<Transfer> toTransferList(List<KorbitTransfer> korbitTransfers) {
        if (korbitTransfers == null) return null;
        return korbitTransfers.stream().map(i -> i.toTransfer()).collect(Collectors.toList());
    }

    private HttpResponse requestGetWithToken(String endpoint, Map<String, String> params)
        throws CTTKException
    {
        final Request request = new Request.Builder()
            .headers(createHttpHeaders())
            .addHeader(HttpHeaders.USER_AGENT, USER_AGENT)
            .url(getHostUrl(endpoint, params))
            .build();

        debug(request);

        try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
            final HttpResponse response = new HttpResponse()
                .setStatus(httpResponse.code())
                .setBody(httpResponse.body().string());

            debug(response);

            return response;
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    private HttpResponse requestPostWithToken(String endpoint, Map<String, String> params)
        throws CTTKException
    {
        final Request request = new Request.Builder()
            .url(getHostUrl(endpoint))
            .headers(createHttpHeaders())
            .addHeader(HttpHeaders.USER_AGENT, USER_AGENT)
            .post(super.createEncodedFormBody(params))
            .build();

        debug(request, params);

        try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
            return toHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    private Headers createHttpHeaders()
        throws CTTKException
    {
        final AuthToken authToken = authTokenProvider.getAuthToken(credential);
        return new Headers.Builder()
            .add(HttpHeaders.AUTHORIZATION, "Bearer " + authToken.getAccessToken())
            .build();

    }

    private String nonce() {
        return String.valueOf(System.currentTimeMillis());
    }

    private void checkOrderResponseError(CreateOrderRequest request, HttpResponse response)
        throws CTTKException
    {
        if (!response.is4xx()) return;
        final String body = response.getBody();

        String exchangeMessage = null;
        try {
            Map<?, ?> map = objectMapper.readValue(body, Map.class);
            exchangeMessage = (String) map.get("status");
        } catch (IOException ignore) {}

        if (exchangeMessage != null) {
            if (exchangeMessage.contains("invalid_amount")) {
                throw new InvalidAmountMinException()
                    .setCxId(this.getCxId())
                    .setBaseCurrency(request.getBaseCurrency())
                    .setQuoteCurrency(request.getQuoteCurrency())
                    .setOrderPrice(request.getPrice())
                    .setOrderQuantity(request.getQuantity())
                    .setExchangeMessage(body)
                    .setResponseStatusCode(response.getStatus());
            } else if (exchangeMessage.contains("too_many_orders")) {
                throw new MaxNumOrderException()
                    .setCxId(this.getCxId())
                    .setBaseCurrency(request.getBaseCurrency())
                    .setQuoteCurrency(request.getQuoteCurrency())
                    .setOrderPrice(request.getPrice())
                    .setOrderQuantity(request.getQuantity())
                    .setExchangeMessage(body)
                    .setResponseStatusCode(response.getStatus());
            }
        }

        InvalidOrderException ex = CTTKExceptionUtils.createByOrderVerifier(request, null, this.getCxId(), marketInfos);
        if (ex != null) {
            throw ex.setCxId(this.getCxId())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity())
                .setExchangeMessage(body)
                .setResponseStatusCode(response.getStatus());
        }

        // Default fallback for Order response exception 
        throw new InvalidOrderException()
            .setCxId(this.getCxId())
            .setBaseCurrency(request.getBaseCurrency())
            .setQuoteCurrency(request.getQuoteCurrency())
            .setOrderPrice(request.getPrice())
            .setOrderQuantity(request.getQuantity())
            .setExchangeMessage(body)
            .setResponseStatusCode(response.getStatus());

    }
}
