package cttk.impl.bittrex;

import cttk.CXClientFactory;
import cttk.PublicCXClient;

public class BittrexClientFactory
    implements CXClientFactory
{
    @Override
    public PublicCXClient create() {
        return new BittrexPublicClient();
    }
}
