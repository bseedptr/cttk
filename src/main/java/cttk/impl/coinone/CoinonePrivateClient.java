package cttk.impl.coinone;

import static cttk.dto.UserOrders.Category.OPEN;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.core.JsonProcessingException;

import cttk.CXClientFactory;
import cttk.CurrencyPair.SimpleCurrencyPair;
import cttk.OrderVerificationResult;
import cttk.OrderVerifier;
import cttk.OrderVerifierFactory;
import cttk.PricingType;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.TradingFeeProvider;
import cttk.auth.Credential;
import cttk.dto.Balances;
import cttk.dto.BankAccount;
import cttk.dto.MarketInfo;
import cttk.dto.MarketInfos;
import cttk.dto.Transfers;
import cttk.dto.UserAccount;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrades;
import cttk.dto.Wallet;
import cttk.dto.Wallets;
import cttk.dto.WithdrawResult;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXServerException;
import cttk.exception.InsufficientBalanceException;
import cttk.exception.InvalidAmountMinException;
import cttk.exception.InvalidAmountRangeException;
import cttk.exception.InvalidOrderException;
import cttk.exception.InvalidPriceIncrementPrecisionException;
import cttk.exception.InvalidPriceMinException;
import cttk.exception.InvalidPriceRangeException;
import cttk.exception.InvalidQuantityIncrementPrecisionException;
import cttk.exception.InvalidTotalRangeException;
import cttk.exception.OrderNotFoundException;
import cttk.exception.UnsupportedCurrencyException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.coinone.response.CoinoneBallanceResponse;
import cttk.impl.coinone.response.CoinoneCreateOrderResponse;
import cttk.impl.coinone.response.CoinoneResponse;
import cttk.impl.coinone.response.CoinoneTransfersResponse;
import cttk.impl.coinone.response.CoinoneUserAccountResponse;
import cttk.impl.coinone.response.CoinoneUserOrderResponse;
import cttk.impl.coinone.response.CoinoneUserOrdersResponse;
import cttk.impl.coinone.response.CoinoneUserTradesResponse;
import cttk.impl.coinone.response.CoinoneWalletsResponse;
import cttk.impl.util.CryptoUtils;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ParamsBuilder;
import cttk.request.CancelOrderRequest;
import cttk.request.CreateMarketPriceOrderRequest;
import cttk.request.CreateOrderRequest;
import cttk.request.GetBankAccountRequest;
import cttk.request.GetOpenUserOrdersRequest;
import cttk.request.GetTransfersDeltaRequest;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserAccountRequest;
import cttk.request.GetUserOrderHistoryRequest;
import cttk.request.GetUserOrderRequest;
import cttk.request.GetUserTradesDeltaRequest;
import cttk.request.GetUserTradesRequest;
import cttk.request.GetWalletRequest;
import cttk.request.WithdrawToBankRequest;
import cttk.request.WithdrawToWalletRequest;
import okhttp3.Headers;
import okhttp3.Request;
import okhttp3.Response;

public class CoinonePrivateClient
    extends AbstractCoinoneClient
    implements PrivateCXClient
{
    protected final Credential credential;
    protected MarketInfos marketInfos;

    public CoinonePrivateClient(Credential credential) {
        super();
        this.credential = credential;
        try {
            final PublicCXClient pubClient = CXClientFactory.createPublicCXClient(this.getCxId());
            marketInfos = pubClient.getMarketInfos();
        } catch (CTTKException e) {
            logger.error("Failed to getMarketInfos while creating CoinonePrivateClient.");
        }
    }

    @Override
    public UserAccount getUserAccount(GetUserAccountRequest request)
        throws CTTKException
    {
        final HttpResponse response = requestPost("/v2/account/user_info/",
            ParamsBuilder.create()
                .putIfNotNull("access_token", credential.getAccessKey())
                .putIfNotNull("nonce", nonce())
                .build());

        checkResponseError(null, response);

        return convert(response, CoinoneUserAccountResponse.class, (i -> i.toUserAccount()));
    }

    @Override
    public BankAccount getBankAccount(GetBankAccountRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("/v2/account/user_info/",
            ParamsBuilder.create()
                .putIfNotNull("access_token", credential.getAccessKey())
                .putIfNotNull("nonce", nonce())
                .build());

        checkResponseError(null, response);

        return convert(response, CoinoneUserAccountResponse.class, (i -> i.toBankAccount()));
    }

    /**
     * Canceled order cannot be checked
     * @param request
     * @return
     * @throws CTTKException
     * @throws UnsupportedCurrencyPairException
     */
    @Override
    public UserOrder getUserOrder(GetUserOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("/v2/order/order_info/",
            ParamsBuilder.create()
                .putIfNotNull("access_token", credential.getAccessKey())
                .putIfNotNull("order_id", request.getOid())
                .putIfNotNull("currency", request.getBaseCurrency())
                .putIfNotNull("nonce", nonce())
                .build());
        try {
            checkResponseError(request, response);
        } catch (OrderNotFoundException e) {
            return null;
        }

        return convert(response, CoinoneUserOrderResponse.class, (i -> i.toUserOrder(request, PricingType.LIMIT)));
    }

    @Override
    public UserOrders getOpenUserOrders(GetOpenUserOrdersRequest request)
        throws CTTKException
    {
        final HttpResponse response = requestPost("/v2/order/limit_orders/",
            ParamsBuilder.create()
                .putIfNotNull("access_token", credential.getAccessKey())
                .putIfNotNull("currency", request.getBaseCurrency())
                .putIfNotNull("nonce", nonce())
                .build());

        checkResponseError(request, response);

        return convert(response, CoinoneUserOrdersResponse.class, (i -> i.toUserOrders(request, PricingType.LIMIT)));
    }

    @Override
    public UserOrders getAllOpenUserOrders(boolean withTrades)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("/v2/order/complete_orders/",
            ParamsBuilder.create()
                .putIfNotNull("access_token", credential.getAccessKey())
                .putIfNotNull("currency", request.getBaseCurrency())
                .putIfNotNull("nonce", nonce())
                .build());
        try {
            checkResponseError(request, response);
        } catch (OrderNotFoundException e) {
            return new UserOrders().setCategory(OPEN);
        }

        return convert(response, CoinoneUserTradesResponse.class, (i -> i.toUserOrders(request)));
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("/v2/order/complete_orders/",
            ParamsBuilder.create()
                .putIfNotNull("access_token", credential.getAccessKey())
                .putIfNotNull("currency", request.getBaseCurrency())
                .putIfNotNull("nonce", nonce())
                .build());

        checkResponseError(request, response);

        return convert(response, CoinoneUserTradesResponse.class, (i -> i.toUserTrades(request)));
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP n - N/A
        final UserTrades trades = getUserTrades(new GetUserTradesRequest().setCurrencyPair(request));
        if (trades != null && trades.isNotEmpty() && request.hasStartDateTime()) {
            trades.filterByDateTimeGTE(request.getStartDateTime());
        }
        return trades;
    }

    @Override
    public Transfers getTransfers(GetTransfersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("/v2/transaction/history/",
            ParamsBuilder.create()
                .putIfNotNull("access_token", credential.getAccessKey())
                .putIfNotNull("nonce", nonce())
                .putIfNotNull("currency", request.getCurrency().toLowerCase())
                .build());

        if (response.getBody().contains("\"errorCode\":\"101\"")) {
            throw new UnsupportedCurrencyException()
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus())
                .setCurrency(request.getCurrency())
                .setExchangeErrorCode("101");
        }

        checkResponseError(null, response);

        return convert(response, CoinoneTransfersResponse.class, (i -> i.toTransfers(request)));
    }

    @Override
    public Transfers getTransfers(GetTransfersDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP n - N/A
        final Transfers transfers = getTransfers(new GetTransfersRequest().setCurrency(request));

        if (request.hasStartDateTime() && transfers != null && transfers.isNotEmpty()) {
            transfers.filterByDateTimeGTE(request.getStartDateTime());
        }

        return transfers;
    }

    @Override
    public UserOrder createOrder(CreateOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("/v2/order/limit_" + CoinoneUtils.toString(request.getSide()) + "/",
            ParamsBuilder.create()
                .putIfNotNull("access_token", credential.getAccessKey())
                .putIfNotNull("price", request.getPrice().longValue())
                .putIfNotNull("qty", request.getQuantity())
                .putIfNotNull("currency", request.getBaseCurrency())
                .putIfNotNull("nonce", nonce())
                .build());

        final String body = response.getBody();
        final Integer statusCode = response.getStatus();
        if (body.contains("\"errorCode\":\"113\"") ||
            body.contains("\"errorCode\":\"107\"") ||
            body.contains("\"errorCode\":\"105\"") ||
            body.contains("\"errorCode\":\"1\""))
        {
            try {
                Map<?, ?> error = objectMapper.readValue(body, Map.class);
                String exchangeErrorCode = String.valueOf(error.get("errorCode"));

                MarketInfo marketInfo = marketInfos.getMarketInfo(request);
                if (Objects.isNull(marketInfo)) {
                    throw CXAPIRequestException.createByRequestObj(request)
                        .setCxId(this.getCxId())
                        .setExchangeMessage(body)
                        .setResponseStatusCode(statusCode)
                        .setExchangeErrorCode(exchangeErrorCode);
                }

                OrderVerifier orderVerifier = OrderVerifierFactory.create(this.getCxId(), marketInfo);
                OrderVerificationResult result = orderVerifier.verify(request);
                if (!result.isValid()) {
                    switch (result.getReasons().get(0)) {
                        case INVALID_PRICE_RANGE:
                            if (request.getPrice().compareTo(BigDecimal.ZERO) <= 0) {
                                throw new InvalidPriceMinException()
                                    .setCxId(this.getCxId())
                                    .setBaseCurrency(request.getBaseCurrency())
                                    .setQuoteCurrency(request.getQuoteCurrency())
                                    .setOrderPrice(request.getPrice())
                                    .setOrderQuantity(request.getQuantity())
                                    .setExchangeMessage(body)
                                    .setResponseStatusCode(statusCode)
                                    .setExchangeErrorCode(exchangeErrorCode);
                            }
                            throw new InvalidPriceRangeException()
                                .setCxId(this.getCxId())
                                .setBaseCurrency(request.getBaseCurrency())
                                .setQuoteCurrency(request.getQuoteCurrency())
                                .setOrderPrice(request.getPrice())
                                .setOrderQuantity(request.getQuantity())
                                .setExchangeMessage(body)
                                .setResponseStatusCode(statusCode)
                                .setExchangeErrorCode(exchangeErrorCode);
                        case INVALID_QUANTITY_RANGE:
                            if (request.getQuantity().compareTo(marketInfo.getMinQuantity()) <= 0) {
                                throw new InvalidAmountMinException()
                                    .setCxId(this.getCxId())
                                    .setBaseCurrency(request.getBaseCurrency())
                                    .setQuoteCurrency(request.getQuoteCurrency())
                                    .setOrderPrice(request.getPrice())
                                    .setOrderQuantity(request.getQuantity())
                                    .setExchangeMessage(body)
                                    .setResponseStatusCode(statusCode)
                                    .setExchangeErrorCode(exchangeErrorCode);
                            }
                            throw new InvalidAmountRangeException()
                                .setCxId(this.getCxId())
                                .setBaseCurrency(request.getBaseCurrency())
                                .setQuoteCurrency(request.getQuoteCurrency())
                                .setOrderPrice(request.getPrice())
                                .setOrderQuantity(request.getQuantity())
                                .setExchangeMessage(body)
                                .setResponseStatusCode(statusCode)
                                .setExchangeErrorCode(exchangeErrorCode);
                        case INVALID_PRICE_INCREMENT:
                            throw new InvalidPriceIncrementPrecisionException()
                                .setCxId(this.getCxId())
                                .setBaseCurrency(request.getBaseCurrency())
                                .setQuoteCurrency(request.getQuoteCurrency())
                                .setOrderPrice(request.getPrice())
                                .setOrderQuantity(request.getQuantity())
                                .setExchangeMessage(body)
                                .setResponseStatusCode(statusCode)
                                .setExchangeErrorCode(exchangeErrorCode);
                        case INVALID_QUANTITY_INCREMENT:
                            throw new InvalidQuantityIncrementPrecisionException()
                                .setCxId(this.getCxId())
                                .setBaseCurrency(request.getBaseCurrency())
                                .setQuoteCurrency(request.getQuoteCurrency())
                                .setOrderPrice(request.getPrice())
                                .setOrderQuantity(request.getQuantity())
                                .setExchangeMessage(body)
                                .setResponseStatusCode(statusCode)
                                .setExchangeErrorCode(exchangeErrorCode);
                        case INVALID_TOTAL_RANGE:
                            throw new InvalidTotalRangeException()
                                .setCxId(this.getCxId())
                                .setBaseCurrency(request.getBaseCurrency())
                                .setQuoteCurrency(request.getQuoteCurrency())
                                .setOrderPrice(request.getPrice())
                                .setOrderQuantity(request.getQuantity())
                                .setExchangeMessage(body)
                                .setResponseStatusCode(statusCode)
                                .setExchangeErrorCode(exchangeErrorCode);
                    }
                }
                throw new InvalidOrderException()
                    .setCxId(this.getCxId())
                    .setBaseCurrency(request.getBaseCurrency())
                    .setQuoteCurrency(request.getQuoteCurrency())
                    .setOrderPrice(request.getPrice())
                    .setOrderQuantity(request.getQuantity())
                    .setExchangeMessage(body)
                    .setResponseStatusCode(statusCode)
                    .setExchangeErrorCode(exchangeErrorCode);
            } catch (IOException e) {
                throw new CXAPIRequestException("Error parsing error\n" + body, e)
                    .setCxId(this.getCxId())
                    .setExchangeMessage(body)
                    .setResponseStatusCode(statusCode);
            }
        } else if (body.contains("\"errorCode\":\"103\"")) {
            // Lack of balance
            try {
                Map<?, ?> error = objectMapper.readValue(body, Map.class);
                throw new InsufficientBalanceException()
                    .setCxId(getCxId())
                    .setExchangeMessage(response.getBody())
                    .setResponseStatusCode(response.getStatus())
                    .setCurrencyPair(new SimpleCurrencyPair(request.getBaseCurrency(), request.getQuoteCurrency()))
                    .setOrderPrice(request.getPrice())
                    .setOrderQuantity(request.getQuantity())
                    .setExchangeErrorCode(String.valueOf(error.get("errorCode")));
            } catch (IOException e) {
                throw new CXAPIRequestException("Error parsing error\n" + body, e)
                    .setCxId(this.getCxId())
                    .setExchangeMessage(body)
                    .setResponseStatusCode(statusCode);
            }
        } else if (body.contains("\"errorCode\":\"101\"")) {
            try {
                Map<?, ?> error = objectMapper.readValue(body, Map.class);
                throw new InvalidPriceMinException()
                    .setCxId(getCxId())
                    .setExchangeMessage(response.getBody())
                    .setResponseStatusCode(response.getStatus())
                    .setCurrencyPair(new SimpleCurrencyPair(request.getBaseCurrency(), request.getQuoteCurrency()))
                    .setOrderPrice(request.getPrice())
                    .setOrderQuantity(request.getQuantity())
                    .setExchangeErrorCode(String.valueOf(error.get("errorCode")));
            } catch (IOException e) {
                throw new CXAPIRequestException("Error parsing error\n" + body, e)
                    .setCxId(this.getCxId())
                    .setExchangeMessage(body)
                    .setResponseStatusCode(statusCode);
            }
        }

        checkResponseError(request, response);

        return fillResponseByRequest(request, convert(response, CoinoneCreateOrderResponse.class, (i -> i.toUserOrder())));
    }

    @Override
    public void cancelOrder(CancelOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("/v2/order/cancel/",
            ParamsBuilder.create()
                .putIfNotNull("access_token", credential.getAccessKey())
                .putIfNotNull("order_id", request.getOrderId())
                .putIfNotNull("price", request.getPrice() == null ? null : request.getPrice().stripTrailingZeros())
                .putIfNotNull("qty", request.getQuantity() == null ? null : request.getQuantity().stripTrailingZeros())
                .putIfNotNull("is_ask", CoinoneUtils.toIsAsk(request.getSide()))
                .putIfNotNull("currency", request.getBaseCurrency())
                .putIfNotNull("nonce", nonce())
                .build());
        if (response.getBody().contains("\"errorCode\":\"101\"")) {
            // Order does not exist
            throw new OrderNotFoundException()
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus())
                .setExchangeErrorCode("101");
        }

        checkResponseError(request, response);

        convert(response, CoinoneResponse.class);
    }

    @Override
    public UserOrder createMarketPriceOrder(CreateMarketPriceOrderRequest request)
        throws CTTKException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public WithdrawResult withdraw(WithdrawToWalletRequest request)
        throws CTTKException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public WithdrawResult withdraw(WithdrawToBankRequest request)
        throws CTTKException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public TradingFeeProvider getTradingFeeProvider()
        throws CTTKException
    {
        final HttpResponse response = requestPost("/v2/account/user_info/",
            ParamsBuilder.create()
                .putIfNotNull("access_token", credential.getAccessKey())
                .putIfNotNull("nonce", nonce())
                .build());

        checkResponseError(null, response);

        return convert(response, CoinoneUserAccountResponse.class, (i -> i.getTradingFeeProvider()));
    }

    @Override
    public Wallets getAllWallets()
        throws CTTKException
    {
        final HttpResponse response = requestPost("/v2/account/deposit_address/",
            ParamsBuilder.create()
                .putIfNotNull("access_token", credential.getAccessKey())
                .putIfNotNull("nonce", nonce())
                .build());

        checkResponseError(null, response);

        return convert(response, CoinoneWalletsResponse.class, (i -> i.toWallets()));
    }

    @Override
    public Wallet getWallet(GetWalletRequest request)
        throws CTTKException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public Balances getAllBalances()
        throws CTTKException
    {
        final HttpResponse response = requestPost("/v2/account/balance/",
            ParamsBuilder.create()
                .putIfNotNull("access_token", credential.getAccessKey())
                .putIfNotNull("nonce", nonce())
                .build());

        checkResponseError(null, response);

        return convert(response, CoinoneBallanceResponse.class, (i -> i.getBalances()));
    }

    private HttpResponse requestPost(String endpoint, Map<String, String> params)
        throws CTTKException
    {
        final Request request = new Request.Builder()
            .url(getHostUrl(endpoint))
            .headers(createHttpHeaders(params))
            .post(super.createEncodedFormBody(params))
            .build();

        debug(request, params);

        try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
            return toHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    private Headers createHttpHeaders(Map<String, String> params) {
        String payload = null;
        try {
            payload = CryptoUtils.base64(objectMapper.writeValueAsString(params));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        final String signature = CryptoUtils.hmacSha512Hex(payload, credential.getSecretKey().toUpperCase());

        return new Headers.Builder()
            .add("X-COINONE-PAYLOAD", payload)
            .add("X-COINONE-SIGNATURE", signature)
            .build();
    }

    private String nonce() {
        return String.valueOf(System.currentTimeMillis());
    }
}
