package cttk.impl.chargingpolicy;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import cttk.CXIds;
import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.dto.ChargingPolicy;
import cttk.dto.ChargingPolicy.Criteria;
import cttk.exception.CTTKException;
import cttk.exception.InvalidChargingPolicyException;

public class AllChargingPolicyProviderTest {
    @Rule
    public ExpectedException ee = ExpectedException.none();

    private AllChargingPolicyProvider provider;
    private CurrencyPair pair;

    @Before
    public void setUp() {
        provider = new AllChargingPolicyProvider(CXIds.KRAKEN);
        pair = CurrencyPair.of("EOS", "ETH");
    }

    @Test
    public void testGetChargingPoliciesWhenCurrencyPairIsNull()
        throws CTTKException
    {
        ee.expect(InvalidChargingPolicyException.class);
        ee.expectMessage("Both CurrencyPair and OrderSide are required.");

        provider.getChargingPolicies(null, OrderSide.BUY);
    }

    @Test
    public void testGetChargingPoliciesWhenOrderSideIsNull()
        throws CTTKException
    {
        ee.expect(InvalidChargingPolicyException.class);
        ee.expectMessage("Both CurrencyPair and OrderSide are required.");

        provider.getChargingPolicies(pair, null);
    }

    @Test
    public void testGetChargingPoliciesWhenBuySide()
        throws CTTKException
    {
        List<ChargingPolicy> policies = provider.getChargingPolicies(pair, OrderSide.BUY);
        assertThat("Should be provide 2 charging polices regardless of order side.", policies, hasSize(2));
        assertNoDuplication(policies);
        assertBuyChargingPolicy(policies.get(0));
        assertBuyChargingPolicy(policies.get(1));
    }

    @Test
    public void testGetChargingPoliciesWhenSellSide()
        throws CTTKException
    {
        List<ChargingPolicy> policies = provider.getChargingPolicies(pair, OrderSide.SELL);
        assertThat("Should be provide 2 charging polices regardless of order side.", policies, hasSize(2));
        assertNoDuplication(policies);
        assertSellChargingPolicy(policies.get(0));
        assertSellChargingPolicy(policies.get(1));
    }

    private void assertNoDuplication(List<ChargingPolicy> policies) {
        assertThat(policies.get(0), not(is(policies.get(1))));
    }

    private void assertBuyChargingPolicy(ChargingPolicy policy) {
        assertThat(policy.getPairString(), is(pair.getPairString()));
        assertThat(policy.getOrderSide(), is(OrderSide.BUY));
        assertFalse(policy.isCXToken());
        if (policy.getFeeCurrency().equals(pair.getBaseCurrency())) {
            assertThat(policy.getFeeCurrency(), is(pair.getBaseCurrency()));
            assertThat(policy.getCriteria(), is(Criteria.SUB));
        } else {
            assertThat(policy.getFeeCurrency(), is(pair.getQuoteCurrency()));
            assertThat(policy.getCriteria(), is(Criteria.ADD));
        }
    }

    private void assertSellChargingPolicy(ChargingPolicy policy) {
        assertThat(policy.getPairString(), is(pair.getPairString()));
        assertThat(policy.getOrderSide(), is(OrderSide.SELL));
        if (policy.getFeeCurrency().equals(pair.getBaseCurrency())) {
            assertThat(policy.getFeeCurrency(), is(pair.getBaseCurrency()));
            assertThat(policy.getCriteria(), is(Criteria.ADD));
        } else {
            assertThat(policy.getFeeCurrency(), is(pair.getQuoteCurrency()));
            assertThat(policy.getCriteria(), is(Criteria.SUB));
        }
    }
}