package cttk.impl.korbit;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.Map;

import cttk.auth.AuthToken;
import cttk.auth.AuthTokenProvider;
import cttk.auth.Credential;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXAuthorityException;
import cttk.exception.CXServerException;
import cttk.impl.korbit.response.KorbitTokenResponse;
import cttk.impl.util.HttpHeaders;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ParamsBuilder;
import okhttp3.Request;
import okhttp3.Response;

public class KorbitAuthTokenProvider
    extends AbstractKorbitClient
    implements AuthTokenProvider
{
    @Override
    public AuthToken getAuthToken(Credential credential)
        throws CXAuthorityException, CTTKException
    {
        try {
            final HttpResponse response = requestPost("/oauth2/access_token",
                ParamsBuilder.create()
                    .putIfNotNull("client_id", credential.getAccessKey())
                    .putIfNotNull("client_secret", credential.getSecretKey())
                    .putIfNotNull("username", credential.getUserName())
                    .putIfNotNull("password", credential.getPassword())
                    .putIfNotNull("grant_type", "password")
                    .build());

            checkResponseError(null, response);
            KorbitTokenResponse ktr = convert(response, KorbitTokenResponse.class);

            return new AuthToken(
                ktr.getAccess_token(),
                ktr.getRefresh_token(),
                ZonedDateTime.now().plusSeconds(ktr.getExpires_in()));
        } catch (CXAPIRequestException e) {
            throw new CXAuthorityException(e)
                .setCxId(e.getCxId())
                .setExchangeMessage(e.getExchangeMessage())
                .setResponseStatusCode(e.getResponseStatusCode());
        }
    }

    @Override
    public void refreshAuthToken(Credential credential, AuthToken authToken)
        throws CXAuthorityException, CTTKException
    {
        try {
            final HttpResponse response = requestPost("/oauth2/access_token",
                ParamsBuilder.create()
                    .putIfNotNull("client_id", credential.getAccessKey())
                    .putIfNotNull("client_secret", credential.getSecretKey())
                    .putIfNotNull("refresh_token", authToken.getRefreshToken())
                    .putIfNotNull("grant_type", "refresh_token")
                    .build());

            KorbitTokenResponse ktr = convert(response, KorbitTokenResponse.class);
            authToken.setAccessToken(ktr.getAccess_token());
            authToken.setExpireDateTime(ZonedDateTime.now().plusSeconds(ktr.getExpires_in()));
        } catch (CXAPIRequestException e) {
            throw new CXAuthorityException(e)
                .setCxId(e.getCxId())
                .setExchangeMessage(e.getExchangeMessage())
                .setResponseStatusCode(e.getResponseStatusCode());
        }
    }

    private HttpResponse requestPost(String endpoint, Map<String, String> params)
        throws CTTKException
    {
        Request request = new Request.Builder()
            .url(getHostUrl(endpoint))
            .addHeader(HttpHeaders.USER_AGENT, USER_AGENT)
            .post(super.createEncodedFormBody(params))
            .build();

        try (Response httpResonse = super.httpClient.newCall(request).execute();) {
            return new HttpResponse()
                .setStatus(httpResonse.code())
                .setBody(httpResonse.body().string());
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }
}
