package cttk.impl.stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cttk.CXIdGettable;
import cttk.CXStream;
import cttk.CXStreamConfig;
import cttk.CXStreamListener;
import cttk.PublicCXClient;
import cttk.exception.CTTKException;
import cttk.exception.CXAuthorityException;
import cttk.impl.util.ThrowableFunction;

public class PollingCXStream<T>
    implements CXStream<T>, CXIdGettable
{
    protected Logger logger = LoggerFactory.getLogger(getClass());

    protected boolean isOpened = true;
    protected Thread thread;

    protected final PublicCXClient client;
    protected final ThrowableFunction<PublicCXClient, T> function;
    protected final long intervalInMilli;
    protected final int maxPollRetryCount;

    protected final CXStreamListener<T> listener;

    public PollingCXStream(PublicCXClient client, ThrowableFunction<PublicCXClient, T> function, CXStreamListener<T> listener) {
        this(client, function, CXStreamConfig.getDefault(), listener);
    }

    public PollingCXStream(
        final PublicCXClient client,
        final ThrowableFunction<PublicCXClient, T> function,
        final CXStreamConfig config,
        final CXStreamListener<T> listener)
    {
        super();
        this.client = client;
        this.function = function;
        this.intervalInMilli = config.getIntervalInMilli();
        this.maxPollRetryCount = config.getMaxPollRetryCount();
        this.listener = listener;

        open();
    }

    @Override
    public String getCxId() {
        return client == null ? null : client.getCxId();
    }

    @Override
    public CXStream<T> open() {
        if (thread == null) {
            startPollingThread();
        } else if (!isOpened) {
            isOpened = true;
            startPollingThread();
        }

        return this;
    }

    private void startPollingThread() {
        final PollingCXStream<T> cxStream = this;
        thread = new Thread() {
            @Override
            public void run() {
                Exception exp = null;
                while (isOpened) {
                    final long start = System.currentTimeMillis();
                    try {
                        T message = getMessageWithRetry();
                        if (message != null) {
                            listener.onMessage(message);
                        }
                    } catch (CTTKException e) {
                        exp = e;
                        logger.error("Unable to get data from source. {}", e.toString(), e);
                        break;
                    }
                    final long end = System.currentTimeMillis();
                    final long interval = end - start;
                    if (interval > intervalInMilli) continue;
                    try {
                        Thread.sleep(intervalInMilli - interval);
                    } catch (InterruptedException e) {
                        logger.error("Stream interrupted", e);
                    }
                }

                try {
                    close();
                } catch (Exception e) {
                    logger.error("Error while closing stream", e);
                }
                if (exp != null) {
                    listener.onFailure(cxStream, exp);
                }
            }
        };

        thread.start();
    }

    private T getMessageWithRetry()
        throws CTTKException
    {
        long backoffMillis = 500;
        Exception exp = null;
        for (int i = 0; i < maxPollRetryCount; i++) {
            try {
                return function.apply(client);
            } catch (CXAuthorityException e) {
                throw e;
            } catch (Exception e) {
                exp = e;
                if (i + 1 < maxPollRetryCount) {
                    logger.warn("Unable to get data. Retry after {}ms.", backoffMillis);
                    try {
                        Thread.sleep(backoffMillis);
                    } catch (InterruptedException e1) {
                        throw CTTKException.create(e1, getCxId());
                    }
                    backoffMillis *= 2;
                }
            }
        }
        logger.error("Unable to get data with {} retries.", maxPollRetryCount);
        throw CTTKException.createOrCast(exp, getCxId());
    }

    @Override
    public void close()
        throws Exception
    {
        isOpened = false;
        logger.info("Stream closed");
    }

    @Override
    public CXStreamListener<T> getListener() {
        return listener;
    }
}
