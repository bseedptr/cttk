package cttk.impl.bibox.stream;

import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.CXStreamOpener;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Trades;

import cttk.request.OpenOrderBookStreamRequest;
import cttk.request.OpenTickerStreamRequest;
import cttk.request.OpenTradeStreamRequest;

public class BiboxStreamOpener
    implements CXStreamOpener
{
    @Override
    public String getCxId() {
        return CXIds.BIBOX;
    }

    @Override
    public CXStream<Ticker> openTickerStream(final OpenTickerStreamRequest request, CXStreamListener<Ticker> listener) {
        return new BiboxWebsocketStream<Ticker, OpenTickerStreamRequest>(
            BiboxChannels.getTickerChannel(request),
            request,
            BiboxWebsocketMessageConverterFactory.createTickerMessageConverter(),
            listener);
    }

    @Override
    public CXStream<OrderBook> openOrderBookStream(final OpenOrderBookStreamRequest request, CXStreamListener<OrderBook> listener)
    {
        return new BiboxWebsocketStream<OrderBook, OpenOrderBookStreamRequest>(
            BiboxChannels.getOrderBookChannel(request),
            request,
            BiboxWebsocketMessageConverterFactory.createOrderBookMessageConverter(),
            listener);
    }

    @Override
    public CXStream<Trades> openTradeStream(final OpenTradeStreamRequest request, CXStreamListener<Trades> listener)
    {
        return new BiboxWebsocketStream<Trades, OpenTradeStreamRequest>(
            BiboxChannels.getTradeChannel(request),
            request,
            BiboxWebsocketMessageConverterFactory.createTradesMessageConverter(),
            listener);
    }
}
