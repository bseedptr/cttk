package cttk.impl.upbit.response;

import java.math.BigDecimal;

import cttk.TransferStatus;
import cttk.TransferType;
import cttk.dto.Transfer;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;

@Data
public class UpbitTransferResponse {
    String type;
    String uuid;
    String currency;
    String txid;
    String state;
    String created_at;
    String done_at;
    BigDecimal amount;
    BigDecimal fee;
    BigDecimal krw_amount;

    public Transfer toTransfer() {
        return new Transfer()
            .setCurrency(currency)
            .setType(TransferType.of(type))
            .setTid(uuid)
            .setTxId(txid)
            .setAmount(amount)
            .setFee(fee)
            .setStatus(state)
            .setStatusType(getStatusType())
            .setCreatedDateTime(DateTimeUtils.parseZdt(created_at))
            .setCompletedDateTime(DateTimeUtils.parseZdt(done_at));
    }

    private TransferStatus getStatusType() {
        switch (state.toUpperCase()) {
            case "SUBMITTING":
            case "SUBMITTED":
            case "ALMOST_ACCEPTED":
            case "PROCESSING":
                return TransferStatus.PENDING;
            case "ACCEPTED":
            case "DONE":
                return TransferStatus.SUCCESS;
            case "REJECTED":
                return TransferStatus.REJECTED;
            case "CANCELED":
                return TransferStatus.CANCELED;
        }
        return null;
    }
}
