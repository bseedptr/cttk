package cttk.impl.bithumb.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import cttk.dto.OrderBook;
import cttk.dto.OrderBooks;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BithumbAllOrderBooksResponse
    extends BithumbDataResponse<BithumbAllOrderBooks>
{
    public OrderBooks toOrderBooks() {
        return new OrderBooks()
            .setOrderBooks(data.toOrderBookList());
    }
}

@Data
class BithumbAllOrderBooks {
    String timestamp;
    String payment_currency;

    @JsonIgnore
    final Map<String, BithumbOrderBook> map = new HashMap<>();

    @JsonAnyGetter
    public Map<String, BithumbOrderBook> any() {
        return this.map;
    }

    @JsonAnySetter
    public void set(String name, BithumbOrderBook value) {
        this.map.put(name, value);
    }

    List<OrderBook> toOrderBookList() {
        return this.map.entrySet().stream().map(e -> toOrderBook(e.getKey(), e.getValue())).collect(Collectors.toList());
    }

    OrderBook toOrderBook(String baseCurrency, BithumbOrderBook bob) {
        return new OrderBook()
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(Long.parseLong(timestamp)))
            .setBaseCurrency(baseCurrency)
            .setQuoteCurrency(this.payment_currency)
            .setAsks(BithumbOrderBookResponse.toOrderList(bob.asks))
            .setBids(BithumbOrderBookResponse.toOrderList(bob.bids));
    }
}