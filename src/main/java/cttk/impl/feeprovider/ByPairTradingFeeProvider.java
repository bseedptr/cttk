package cttk.impl.feeprovider;

import cttk.CurrencyPair;
import lombok.ToString;

@ToString(callSuper = true)
public class ByPairTradingFeeProvider
    extends AbstractMappedTradingFeeProvider<ByPairTradingFeeProvider>
{
    @Override
    public String getKey(CurrencyPair currencyPair) {
        return currencyPair == null ? null : currencyPair.getPairString();
    }
}
