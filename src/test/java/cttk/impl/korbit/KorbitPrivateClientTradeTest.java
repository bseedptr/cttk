package cttk.impl.korbit;

import org.junit.Test;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.exception.InvalidOrderException;
import cttk.impl.AbstractPrivateCXClientTradeTest;
import cttk.impl.util.ExNotRaisedException;

public class KorbitPrivateClientTradeTest
    extends AbstractPrivateCXClientTradeTest
{
    @Override
    public String getCxId() {
        return CXIds.KORBIT;
    }

    // Korbit doesn't return any error codes for errors
    // We can't differentiate between UnsupportedCurrencyPairException and InvalidOrderException
    @Test(expected = InvalidOrderException.class)
    @Override
    public void testCreateBuyWrongCurrency()
        throws CTTKException
    {
        super.testCreateBuyWrongCurrency();
    }

    // Korbit returns only "status":"invalid_amount" for large order that exceeds account quantity
    @Test(expected = InvalidOrderException.class)
    @Override
    public void testCreateBuyLargeQuantity()
        throws CTTKException
    {
        super.testCreateBuyLargeQuantity();
    }

    @Test(expected = ExNotRaisedException.class)
    @Override
    public void testInvalidQuantityIncrementPrecision()
        throws CTTKException
    {
        // Please refer to CTTK-498 for detailed explanation
        // Korbit doesn't have any quantity precision
        super.testInvalidQuantityIncrementPrecision();
    }
}
