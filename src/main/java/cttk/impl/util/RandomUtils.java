package cttk.impl.util;

import java.util.Random;

public class RandomUtils {
    private static final Random RANDOM = new Random();

    public static int generateRandomInt() {
        return RANDOM.nextInt();
    }
}
