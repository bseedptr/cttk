package cttk.impl.coinbase.response;

import java.math.BigDecimal;

import cttk.dto.Ticker;
import lombok.Data;

/**
 * Keep last 24hours stats
 * @author	dj.lee
 * @since	2018-02-12
 */
@Data
public class CoinbaseProductStatsResponse {
    BigDecimal open;
    BigDecimal high;
    BigDecimal low;
    BigDecimal volume;
    BigDecimal last;
    BigDecimal volume_30day;

    public Ticker fillTicker(Ticker ticker) {
        if (ticker == null) return null;
        return ticker
            .setOpenPrice(open)
            .setHighPrice(high)
            .setLowPrice(low)
            .setQuoteVolume(volume)
            .setLastPrice(last);
    }
}
