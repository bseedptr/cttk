package cttk.impl.bibox.response;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.TransferType;
import cttk.dto.Transfer;
import cttk.impl.bibox.BiboxUtils;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BiboxUserWithdrawInfoResponse
    extends BiboxResultWrapperResponse<BiboxUserWithdrawInfo>
{
    public Transfer toTransfer() {
        return unwrap().toTransfer();
    }
}

@Data
class BiboxUserWithdrawInfo {
    String id;
    Long createdAt;
    String accountType;
    @JsonProperty("coin_symbol")
    String coinSymbol;
    @JsonProperty("to_address")
    String toAddress;
    String status;
    BigDecimal amount;
    BigDecimal fee;

    Transfer toTransfer() {
        return new Transfer()
            .setCurrency(coinSymbol)
            .setTid(id)
            .setType(TransferType.WITHDRAWAL)
            .setAmount(amount)
            .setStatus(status)
            .setAddress(toAddress)
            .setCreatedDateTime(DateTimeUtils.zdtFromEpochMilli(createdAt));
    }

    @JsonProperty("status")
    public void setStatus(Integer status) {
        this.status = BiboxUtils.parseWithdrawStatus(status);
    }
}