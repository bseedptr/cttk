package cttk.impl.chargingpolicy;

import java.util.List;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.dto.ChargingPolicy;
import cttk.dto.ChargingPolicy.Criteria;
import cttk.exception.CTTKException;
import lombok.Getter;

@Getter
public class CXTokenPayableChargingPolicyProvider
    extends DefaultChargingPolicyProvider
{
    private final String cxToken;

    public CXTokenPayableChargingPolicyProvider(String cxId, String cxToken) {
        super(cxId);
        this.cxToken = cxToken;
    }

    @Override
    public List<ChargingPolicy> getChargingPolicies(CurrencyPair currencyPair, OrderSide orderSide)
        throws CTTKException
    {
        checkParameters(currencyPair, orderSide);

        List<ChargingPolicy> policies = super.getChargingPolicies(currencyPair, orderSide);
        policies.add(new ChargingPolicy(currencyPair, orderSide, cxToken, Criteria.ADD, true));
        return policies;
    }
}
