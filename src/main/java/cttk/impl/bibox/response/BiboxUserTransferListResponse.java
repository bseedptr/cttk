package cttk.impl.bibox.response;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.TransferType;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.impl.bibox.BiboxUtils;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BiboxUserTransferListResponse
    extends BiboxResultWrapperResponse<BiboxUserTransferList>
{
    public Transfers toUserTransfers(final String accountId) {
        return unwrap().toUserTransfers(accountId, getTransferType());
    }

    private TransferType getTransferType() {
        if (result == null || result.length < 1) return null;
        String cmd = result[0].cmd;
        if (cmd == null) return null;
        if (cmd.endsWith("transferInList")) {
            return TransferType.DEPOSIT;
        } else if (cmd.endsWith("transferOutList")) {
            return TransferType.WITHDRAWAL;
        } else {
            return null;
        }
    }
}

@Data
class BiboxUserTransferList {
    Integer count;
    Integer page;
    List<BiboxUserTransfer> items;

    Transfers toUserTransfers(final String accountId, final TransferType transferType) {
        return new Transfers()
            .setTransfers(items.stream().map(i -> i.toTransfer(accountId, transferType)).collect(Collectors.toList()));
    }
}

@Data
class BiboxUserTransfer {
    @JsonProperty("coin_symbol")
    String coinSymbol;
    @JsonProperty("to_address")
    String toAddress;
    BigDecimal amount;
    BigDecimal fee;
    Integer confirmCount;
    long createdAt;
    String memo;
    Integer status;

    Transfer toTransfer(final String accountId, final TransferType transferType) {
        final String tid = String.format("%s:%s:%s", accountId, coinSymbol, createdAt);
        return new Transfer()
            .setTid(tid)
            .setUserId(accountId)
            .setCurrency(coinSymbol)
            .setType(transferType)
            .setAddress(toAddress)
            .setAmount(amount)
            .setFee(fee)
            .setConfirmations(confirmCount)
            .setCreatedDateTime(DateTimeUtils.zdtFromEpochMilli(createdAt))
            .setDescription(memo)
            .setStatus(BiboxUtils.parseWithdrawStatus(status));
    }
}