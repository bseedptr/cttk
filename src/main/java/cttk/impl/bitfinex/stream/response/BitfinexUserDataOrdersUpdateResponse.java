package cttk.impl.bitfinex.stream.response;

import static cttk.impl.bitfinex.BitfinexUtils.getPricingType;
import static cttk.impl.bitfinex.BitfinexUtils.getStatusType;
import static cttk.impl.bitfinex.BitfinexUtils.parseMarketSymbolWithT;
import static cttk.impl.util.DateTimeUtils.zdtFromEpochMilli;
import static cttk.util.StringUtils.parseBigDecimal;
import static cttk.util.StringUtils.parseLong;

import java.math.BigDecimal;
import java.util.ArrayList;

import cttk.CXIds;
import cttk.OrderSide;
import cttk.dto.UserOrder;
import cttk.impl.util.MathUtils;

public class BitfinexUserDataOrdersUpdateResponse
    extends ArrayList<String>
{
    public UserOrder toUserOrder() {
        UserOrder uo = new UserOrder();

        if (get(0) != null) {
            final String id = get(0);
            final String clientOId = get(2);
            final String symbol = get(3);
            final String orderStatus = get(13);
            final String orderPricingType = get(8);
            final Long mtsCreate = parseLong(get(4));
            final BigDecimal amount = parseBigDecimal(get(6));
            final BigDecimal price = parseBigDecimal(get(16));

            if (clientOId != null) {
                uo.setClientOrderId(clientOId);
            }
            uo.setCxId(CXIds.BITFINEX);
            uo.setCurrencyPair(parseMarketSymbolWithT(symbol));
            uo.setOid(id);
            uo.setStatus(orderStatus);
            uo.setStatusType(getStatusType(orderStatus));
            uo.setDateTime(zdtFromEpochMilli(mtsCreate));
            uo.setSide(amount.compareTo(BigDecimal.ZERO) > 0 ? OrderSide.BUY : OrderSide.SELL);
            uo.setPrice(price);
            uo.setQuantity(amount.abs());
            uo.setTotal(MathUtils.multiply(price, amount.abs()));
            uo.setPricingType(getPricingType(orderPricingType));

            return uo;
        }
        return uo;
    }
}
