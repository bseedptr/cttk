package cttk.impl.coinone.response;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import cttk.TradingFeeProvider;
import cttk.dto.BankAccount;
import cttk.dto.TradingFee;
import cttk.dto.UserAccount;
import cttk.impl.feeprovider.ByBaseTradingFeeProvider;
import cttk.util.StringUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CoinoneUserAccountResponse
    extends CoinoneResponse
{
    UserInfo userInfo;

    public UserAccount toUserAccount() {
        return userInfo == null ? null
            : new UserAccount()
                .setEmail(userInfo.emailInfo.getEmail())
                .setName(userInfo.mobileInfo.getUserName())
                .setPhone(userInfo.mobileInfo.getPhoneNumber())
                .setIsMobileAuthenticated(Boolean.valueOf(userInfo.mobileInfo.getIsAuthenticated()))
                .setIsEmailAuthenticated(Boolean.valueOf(userInfo.emailInfo.getIsAuthenticated()))
                .setSecurityLevel(userInfo.getSecurityLevel());
    }

    public BankAccount toBankAccount() {
        return userInfo == null ? null : new BankAccount()
            .setBankUser(userInfo.bankInfo.getDepositor())
            .setBankCode(userInfo.bankInfo.getBankCode())
            .setAccountNo(userInfo.bankInfo.getAccountNumber());
    }

    public TradingFeeProvider getTradingFeeProvider() {
        return new ByBaseTradingFeeProvider()
            .setFeeMap(userInfo.feeRate.getTradingFeeMap());
    }
}

@Data
class UserInfo {
    CoinoneVirtualAccountInfo virtualAccountInfo;
    CoinoneMobileInfo mobileInfo;
    CoinoneBankInfo bankInfo;
    CoinoneEmailInfo emailInfo;
    String securityLevel;
    CoinoneFeeRate feeRate;
}

@Data
class CoinoneVirtualAccountInfo {
    String depositor;
    String accountNumber;
    String bankName;
}

@Data
class CoinoneMobileInfo {
    String userName;
    String phoneNumber;
    String phoneCorp;
    String isAuthenticated;
}

@Data
class CoinoneBankInfo {
    String depositor;
    String bankCode;
    String accountNumber;
    String isAuthenticated;
}

@Data
class CoinoneEmailInfo {
    String email;
    String isAuthenticated;
}

@Data
class CoinoneFeeRate {
    public Map<String, CoinoneFee> map = new HashMap<>();

    @JsonAnyGetter
    public Map<String, CoinoneFee> any() {
        return this.map;
    }

    @JsonAnySetter
    public void set(String name, CoinoneFee value) {
        this.map.put(name, value);
    }

    public Map<String, TradingFee> getTradingFeeMap() {
        return map.entrySet().stream().collect(Collectors.toMap(
            e -> StringUtils.toUpper(e.getKey()),
            e -> new TradingFee()
                .setTakerFee(e.getValue().getTaker())
                .setMakerFee(e.getValue().getMaker())));
    }
}

@Data
class CoinoneFee {
    BigDecimal maker;
    BigDecimal taker;
}