package cttk.impl.okex.stream.response;

import java.util.List;

import lombok.Data;

@Data
public class OkexWebsocketResponse<T> {
    String table;
    List<T> data;

    public T unwrap() {
        return data.get(0);
    }
}
