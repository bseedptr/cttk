package cttk.impl.binance.response;

import java.util.List;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.dto.OrderBook;
import cttk.dto.OrderBook.SimpleOrder;
import lombok.Data;

@Data
public class BinanceOrderBookResponse {
    Long lastUpdateId;
    List<List<Object>> bids;
    List<List<Object>> asks;

    public OrderBook getOrderBook(CurrencyPair currencyPair) {
        return new OrderBook()
            .setCurrencyPair(currencyPair)
            .setLastUpdateId(lastUpdateId)
            .setBids(bids.stream().map(b -> SimpleOrder.of(String.valueOf(b.get(0)), String.valueOf(b.get(1)))).collect(Collectors.toList()))
            .setAsks(asks.stream().map(a -> SimpleOrder.of(String.valueOf(a.get(0)), String.valueOf(a.get(1)))).collect(Collectors.toList()));
    }
}
