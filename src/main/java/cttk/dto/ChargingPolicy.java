package cttk.dto;

import cttk.AbstractCurrencyPair;
import cttk.CurrencyPair;
import cttk.OrderSide;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class ChargingPolicy
    extends AbstractCurrencyPair<ChargingPolicy>
{
    OrderSide orderSide;
    String feeCurrency;
    Criteria criteria;
    boolean isCXToken;

    public ChargingPolicy(CurrencyPair currencyPair, OrderSide orderSide, String feeCurrency, Criteria criteria) {
        this(currencyPair, orderSide, feeCurrency, criteria, false);
    }

    public ChargingPolicy(CurrencyPair currencyPair, OrderSide orderSide, String feeCurrency, Criteria criteria, boolean isCXToken) {
        setCurrencyPair(currencyPair);
        this.orderSide = orderSide;
        this.feeCurrency = feeCurrency;
        this.criteria = criteria;
        this.isCXToken = isCXToken;
    }

    public boolean isNotCXToken() {
        return !isCXToken;
    }

    public enum Criteria {
        SUB, ADD
    }
}
