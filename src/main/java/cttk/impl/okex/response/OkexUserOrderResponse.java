package cttk.impl.okex.response;

import cttk.dto.UserOrder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class OkexUserOrderResponse
    extends OkexUserOrder
{
    @Override
    public UserOrder toUserOrder() {
        return super.toUserOrder();
    }
}
