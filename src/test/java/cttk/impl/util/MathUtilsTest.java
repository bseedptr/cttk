package cttk.impl.util;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.Assert;
import org.junit.Test;

public class MathUtilsTest {
    @Test
    public void testRound1() {
        final BigDecimal rounded = MathUtils.round(new BigDecimal("0.0456789101"), new BigDecimal("0.00000001"));
        Assert.assertEquals(new BigDecimal("0.04567891"), rounded);
    }

    @Test
    public void testRound2() {
        final BigDecimal rounded = MathUtils.round(new BigDecimal("0.0456789101"), 5);
        Assert.assertEquals(new BigDecimal("0.045679"), rounded);
    }

    @Test
    public void testIsNotZeroIfNullThenReturnFalse() {
        Assert.assertFalse("Null is considered false.", MathUtils.isNotZero(null));
    }

    @Test
    public void testIsNotZeroIfOneThenReturnTrue() {
        Assert.assertTrue(MathUtils.isNotZero(ONE));
    }

    @Test
    public void testIsNotZeroIfZeroThenReturnFalse() {
        Assert.assertFalse(MathUtils.isNotZero(ZERO));
    }

    @Test
    public void testIsNegative() {
        Assert.assertFalse(MathUtils.isNegative(null));
        Assert.assertFalse(MathUtils.isNegative(new BigDecimal("0")));
        Assert.assertFalse(MathUtils.isNegative(new BigDecimal("1")));
        Assert.assertTrue(MathUtils.isNegative(new BigDecimal("-0.1")));
        Assert.assertTrue(MathUtils.isNegative(new BigDecimal("-100")));
    }

    @Test
    public void testaddOneToLongValueString() {
        Assert.assertEquals(MathUtils.subOneToLongValueString("100"), "99");
        Assert.assertNull(MathUtils.subOneToLongValueString(null));
    }

    @Test
    public void testSubOneToLongValueString() {
        Assert.assertEquals(MathUtils.subOneToLongValueString("100"), "99");
        Assert.assertNull(MathUtils.subOneToLongValueString(null));
    }

    @Test
    public void testBigDecimalDivide() {
        Assert.assertEquals(new BigDecimal("0.00"), MathUtils.divide(new BigDecimal("1"), new BigDecimal("333")));
        Assert.assertEquals(new BigDecimal("0.00"), MathUtils.divide(new BigDecimal("0.01"), new BigDecimal("3")));
        Assert.assertEquals(new BigDecimal("0.3333"), MathUtils.divide(new BigDecimal("0.0001"), new BigDecimal("0.0003")));
        Assert.assertEquals(new BigDecimal("0.33"), MathUtils.divide(new BigDecimal("10000"), new BigDecimal("30000")));
    }
}
