package cttk.impl.poloniex.response;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.PricingType;
import cttk.dto.UserOrder;
import cttk.impl.poloniex.PoloniexUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PoloniexOrderStatusResponse {
    private Map<String, PoloniexOrderStatus> result = new HashMap<>();
    int success;

    public UserOrder toUserOrder(PricingType pricingType) {
        for (String key : result.keySet()) {
            PoloniexOrderStatus orderStatus = result.get(key);
            return new UserOrder()
                .setOid(key)
                .setSide(orderStatus.getOrderSide())
                .setDateTime(orderStatus.parseZdt())
                .setPrice(orderStatus.getRate())
                .setTotal(orderStatus.getTotal())
                .setStatus(orderStatus.getStatus())
                .setPricingType(pricingType)
                .setStatusType(orderStatus.getOrderStatusType())
                .setCurrencyPair(PoloniexUtils.parseMarketSymbol(orderStatus.getCurrencyPair()))
                .setQuantity(orderStatus.getStartingAmount())
                .setRemainingQuantity(orderStatus.getAmount())
                .setFilledQuantity(MathUtils.subtract(orderStatus.getStartingAmount(), orderStatus.getAmount()));
        }
        return null;

    }

}

@Data
class PoloniexOrderStatus {
    String status;
    BigDecimal rate;
    BigDecimal amount;
    String type;    // buy, sell
    String currencyPair;
    String date;
    BigDecimal total;
    BigDecimal startingAmount;

    private static final DateTimeFormatter DTF = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public ZonedDateTime parseZdt() {
        if (date == null || date.trim().isEmpty()) return null;
        return LocalDateTime.parse(date, DTF).atZone(ZoneOffset.UTC);
    }

    public OrderSide getOrderSide() {
        return OrderSide.of(type);
    }

    public OrderStatus getOrderStatusType() {
        if (status == null) {
            return null;
        }
        switch (status) {
            case "Open":
                return OrderStatus.UNFILLED;
            case "Partially filled":
                return OrderStatus.PARTIALLY_FILLED;
            default:
                return null;
        }
    }
}
