package cttk.impl.hitbtc;

import cttk.CXIds;
import cttk.impl.AbstractPollingCXStreamTest;

public class HitbtcPollingStreamTest
    extends AbstractPollingCXStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.HITBTC;
    }
}
