package cttk;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

public interface CXStream<T>
    extends AutoCloseable
{
    public static final int NORMAL_CLOSURE_STATUS = 1000;

    CXStream<T> open()
        throws Exception;

    void close()
        throws Exception;

    CXStreamListener<T> getListener();

    default OkHttpClient createOkHttpClient(long timeoutInSecond) {
        return createOkHttpClient(timeoutInSecond, 0);
    }

    default OkHttpClient createOkHttpClient(long timeoutInSecond, long pingIntervalInSecond) {
        return new OkHttpClient()
            .newBuilder()
            .readTimeout(timeoutInSecond, TimeUnit.SECONDS)
            .pingInterval(pingIntervalInSecond, TimeUnit.SECONDS)
            .build();
    }
}
