package cttk.impl.binance;

import java.io.IOException;
import java.util.Map;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.exception.CXServerException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.AbstractCXClient;
import cttk.impl.binance.response.BinanceServerTimeResponse;
import cttk.impl.util.HttpHeaders;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.MediaType;
import cttk.impl.util.ThrowableFunction;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.Response;

public abstract class AbstractBinanceClient
    extends AbstractCXClient
{
    protected final String PUBLIC_URL = "https://api.binance.com/api/v1";

    public AbstractBinanceClient() {
        super();
    }

    @Override
    public String getCxId() {
        return CXIds.BINANCE;
    }

    protected long getServerTime()
        throws CTTKException
    {
        final HttpResponse response = requestGet("time");

        checkResponseError(null, response);

        final long serverTime = convert(response, BinanceServerTimeResponse.class, i -> i.getServerTime());

        logger.debug("\n"
            + "    Server : {}\n"
            + "    Local  : {}", serverTime, System.currentTimeMillis());

        return serverTime;
    }

    protected HttpResponse requestGet(String path)
        throws CTTKException
    {
        return requestGet(path, null);
    }

    protected HttpResponse requestGet(String path, Map<String, String> params)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final Request request = new Request.Builder()
            .url(getHostUrl(path, params))
            .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED)
            .get()
            .build();

        debug(request);

        try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
            return toHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    protected String getHostUrl(String path) {
        return getHostUrl(PUBLIC_URL, path);
    }

    protected String getHostUrl(String path, Map<String, String> params) {
        return getHostUrl(PUBLIC_URL, path, params);
    }

    protected FormBody createFormBody(Map<String, String> params) {
        FormBody.Builder builder = new FormBody.Builder();

        if (params != null) params.forEach((k, v) -> builder.addEncoded(k, v));

        return builder.build();
    }

    @Override
    protected void checkErrorByResponseMessage(Object requestObj, HttpResponse response)
        throws CTTKException
    {
        BinanceUtils.checkErrorByResponseMessage(requestObj, response.getBody(), response.getStatus());
    }

    @Override
    protected <T, R> R convert(final HttpResponse response, Class<T> claz, ThrowableFunction<T, R> mapper)
        throws CTTKException
    {
        final String body = response.getBody();

        try {
            return mapper.apply(objectMapper.readValue(body, claz));
        } catch (IOException e) {
            throw CTTKException.create(e, getCxId());
        }

    }
}
