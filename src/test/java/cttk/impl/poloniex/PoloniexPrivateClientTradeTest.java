package cttk.impl.poloniex;

import java.math.BigDecimal;

import org.junit.Test;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.impl.AbstractPrivateCXClientTradeTest;
import cttk.impl.util.ExNotRaisedException;

public class PoloniexPrivateClientTradeTest
    extends AbstractPrivateCXClientTradeTest
{
    @Override
    public String getCxId() {
        return CXIds.POLONIEX;
    }

    @Test(expected = ExNotRaisedException.class)
    @Override
    public void testInvalidPriceIncrementPrecision()
        throws CTTKException
    {
        // Please refer to CTTK-501 for detailed explanation
        // Poloniex automatically truncates any values below the precision
        testInvalidPriceIncrementPrecisionQuantity(new BigDecimal("0.1"));
    }

    @Test(expected = ExNotRaisedException.class)
    @Override
    public void testInvalidQuantityIncrementPrecision()
        throws CTTKException
    {
        // Please refer to CTTK-501 for detailed explanation
        // Poloniex automatically truncates any values below the precision
        testInvalidQuantityIncrementPrecisionQuantity(new BigDecimal("0.1"));
    }
}
