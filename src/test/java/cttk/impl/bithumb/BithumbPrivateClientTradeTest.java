package cttk.impl.bithumb;

import cttk.CXIds;
import cttk.impl.AbstractPrivateCXClientTradeTest;

public class BithumbPrivateClientTradeTest
    extends AbstractPrivateCXClientTradeTest
{
    @Override
    public String getCxId() {
        return CXIds.BITHUMB;
    }

    @Override
    protected long getThreadSleepMillis() {
        return 5000l;
    }
}
