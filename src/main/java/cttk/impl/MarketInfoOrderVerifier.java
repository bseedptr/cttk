package cttk.impl;

import java.math.BigDecimal;

import cttk.AbstractCXIdHolder;
import cttk.OrderVerifier;
import cttk.dto.MarketInfo;

public class MarketInfoOrderVerifier
    extends AbstractCXIdHolder<MarketInfoOrderVerifier>
    implements OrderVerifier
{
    protected final MarketInfo marketInfo;

    public MarketInfoOrderVerifier(MarketInfo marketInfo) {
        super();
        this.marketInfo = marketInfo;
    }

    protected BigDecimal getPriceIncrement() {
        return marketInfo == null ? null : marketInfo.getPriceIncrement();
    }

    @Override
    public BigDecimal roundPrice(BigDecimal price) {
        return marketInfo.roundPrice(price);
    }

    @Override
    public boolean checkPriceRange(BigDecimal price) {
        if (price == null || price.compareTo(BigDecimal.ZERO) <= 0) return false;
        boolean isValid = true;
        if (isValid && marketInfo.getMinPrice() != null) {
            isValid = price.compareTo(marketInfo.getMinPrice()) >= 0;
        }
        if (isValid && marketInfo.getMaxPrice() != null && marketInfo.getMaxPrice().compareTo(BigDecimal.ZERO) > 0) {
            isValid = price.compareTo(marketInfo.getMaxPrice()) <= 0;
        }
        return isValid;
    }

    @Override
    public BigDecimal roundQuantity(BigDecimal quantity) {
        return marketInfo.roundQuantity(quantity);
    }

    @Override
    public boolean checkQuantityRange(BigDecimal quantity) {
        if (quantity == null || quantity.compareTo(BigDecimal.ZERO) <= 0) return false;
        boolean isValid = true;
        if (isValid && marketInfo.getMinQuantity() != null) {
            isValid = quantity.compareTo(marketInfo.getMinQuantity()) >= 0;
        }
        if (isValid && marketInfo.getMaxQuantity() != null) {
            isValid = quantity.compareTo(marketInfo.getMaxQuantity()) <= 0;
        }
        return isValid;
    }

    @Override
    public boolean checkTotalRange(BigDecimal total) {
        if (total == null) return false;
        boolean isValid = true;
        if (isValid && marketInfo.getMinTotal() != null) {
            isValid = total.compareTo(marketInfo.getMinTotal()) >= 0;
        }
        if (isValid && marketInfo.getMaxTotal() != null) {
            isValid = total.compareTo(marketInfo.getMaxTotal()) <= 0;
        }
        return isValid;
    }
}
