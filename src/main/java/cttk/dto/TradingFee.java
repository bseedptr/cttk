package cttk.dto;

import static cttk.util.StringUtils.parseBigDecimal;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cttk.impl.util.MathUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class TradingFee {
    BigDecimal takerFee;
    BigDecimal makerFee;

    public TradingFee setFees(BigDecimal takerFee, BigDecimal makerFee) {
        setTakerFee(takerFee);
        setMakerFee(makerFee);
        return this;
    }

    public TradingFee setFees(String takerFee, String makerFee) {
        setTakerFee(parseBigDecimal(takerFee));
        setMakerFee(parseBigDecimal(makerFee));
        return this;
    }

    @JsonIgnore
    public TradingFee setFees(BigDecimal fee) {
        this.takerFee = fee;
        this.makerFee = fee;
        return this;
    }

    @JsonIgnore
    public TradingFee setFees(String fee) {
        setFees(parseBigDecimal(fee));
        return this;
    }

    public TradingFee percentToRatio() {
        final BigDecimal A100 = new BigDecimal(100);
        this.takerFee = MathUtils.divide(this.takerFee, A100);
        this.makerFee = MathUtils.divide(this.makerFee, A100);
        return this;
    }
}
