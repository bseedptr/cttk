package cttk.impl.huobi;

import cttk.CXIds;
import cttk.impl.AbstractPrivateCXClientTradeTest;

public class HuobiPrivateClientTradeTest
    extends AbstractPrivateCXClientTradeTest
{
    @Override
    public String getCxId() {
        return CXIds.HUOBI;
    }
}
