package cttk;

public interface CXIdGettable {
    String getCxId();

    default <T extends AbstractCXIdHolder<T>> T setCxId(T obj) {
        return obj == null ? null : obj.setCxId(getCxId());
    }
}
