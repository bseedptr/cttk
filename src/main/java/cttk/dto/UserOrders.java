package cttk.dto;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cttk.AbstractCXIdHolder;
import cttk.CurrencyPairListHolder;
import cttk.DateTimeGettableListHolder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(chain = true)
public class UserOrders
    extends AbstractCXIdHolder<UserOrders>
    implements CurrencyPairListHolder<UserOrders, UserOrder>, DateTimeGettableListHolder<UserOrders, UserOrder>
{
    List<UserOrder> orders;
    Category category;

    @JsonIgnore
    @Override
    public List<UserOrder> getList() {
        return orders;
    }

    @Override
    public UserOrders setList(List<UserOrder> list) {
        this.orders = list;
        return this;
    }

    public Optional<UserOrder> findOneByOid(String oid) {
        return findOneBy(i -> i.getOid().equals(oid));
    }

    @JsonIgnore
    public Set<String> getDistinctOrderIds() {
        return orders == null ? null
            : orders.stream()
                .map(i -> i.getOid())
                .distinct()
                .collect(Collectors.toSet());
    }

    @JsonIgnore
    public UserTrades getUserTrades() {
        if (isEmpty()) return null;
        final UserTrades userTrades = new UserTrades();
        orders.forEach(i -> userTrades.addAll(i.getTrades()));
        return userTrades;
    }

    @JsonIgnore
    public UserOrder getLatestById() {
        return isEmpty()
            ? null
            : getList().stream()
                .max((a, b) -> Long.valueOf(a.getOid()).compareTo(Long.valueOf(b.getOid())))
                .get();
    }

    @JsonIgnore
    public UserOrder getOldestById() {
        return isEmpty()
            ? null
            : getList().stream()
                .max((a, b) -> Long.valueOf(b.getOid()).compareTo(Long.valueOf(a.getOid())))
                .get();
    }

    /**
     * <pre>
     * You can use Category to distinguish result of UserOrders. For example,
     *
     * UserOrders.Category category = privateClient.getUserOrderHistory(...);
     * if(category == UserOrders.Category.CLOSED)
     *   privateClient.getOpenUserOrders(...);
     * </pre>
     */
    public enum Category {
        ALL, OPEN, CLOSED
    }
}
