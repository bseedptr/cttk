package cttk.impl.coinone.response;

import static cttk.impl.util.DateTimeUtils.zdtFromEpochSecond;

import java.util.List;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.dto.OrderBook;
import cttk.impl.util.ZoneIds;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CoinoneOrderBookResponse
    extends CoinoneResponse
{
    String timestamp;
    String currency;
    List<CoinoneOrder> bid;
    List<CoinoneOrder> ask;

    public OrderBook toOrderBook(CurrencyPair currencyPair) {
        return new OrderBook()
            .setDateTime(zdtFromEpochSecond(Long.parseLong(timestamp), ZoneIds.SEOUL))
            .setCurrencyPair(currencyPair)
            .setBids(toOrderList(bid))
            .setAsks(toOrderList(ask));
    }

    private List<OrderBook.SimpleOrder> toOrderList(List<CoinoneOrder> coinoneOrders) {
        if (coinoneOrders == null)
            return null;
        return coinoneOrders.stream().map(CoinoneOrder::toOrder).collect(Collectors.toList());
    }
}

@Data
class CoinoneOrder {
    String price;
    String qty;

    public OrderBook.SimpleOrder toOrder() {
        return OrderBook.SimpleOrder.of(price, qty);
    }
}