package cttk.impl.okex;

import cttk.CXIds;
import cttk.impl.AbstractUserDataStreamTest;

public class OkexUserDataStreamTest
    extends AbstractUserDataStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.OKEX;
    }

    @Override
    protected boolean useThread() {
        return true;
    }
}
