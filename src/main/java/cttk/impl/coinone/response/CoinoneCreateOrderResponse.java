package cttk.impl.coinone.response;

import cttk.dto.UserOrder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CoinoneCreateOrderResponse
    extends CoinoneResponse
{
    String orderId;

    public UserOrder toUserOrder() {
        return new UserOrder()
            .setOid(orderId.toUpperCase());
    }
}