package cttk.impl.binance.response;

import java.math.BigDecimal;
import java.math.BigInteger;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.OrderSide;
import cttk.dto.Trade;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;

@Data
public class BinanceTradeResponse {
    BigInteger id;
    Long time;
    BigDecimal price;
    @JsonProperty("qty")
    BigDecimal quantity;
    Boolean isBuyerMaker;
    Boolean isBestMatch;

    public Trade getTrade() {
        return new Trade()
            .setSno(id)
            .setTid(String.valueOf(id))
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(time))
            .setSide(isBuyerMaker ? OrderSide.BUY : OrderSide.SELL)
            .setPrice(price)
            .setQuantity(quantity)
            .setTotal(MathUtils.multiply(price, quantity));
    }
}
