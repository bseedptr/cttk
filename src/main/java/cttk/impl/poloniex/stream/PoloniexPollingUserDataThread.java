package cttk.impl.poloniex.stream;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.OrderStatus;
import cttk.PrivateCXClient;
import cttk.UserData;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;
import cttk.exception.CTTKException;
import cttk.impl.stream.PollingUserDataThread;
import cttk.request.GetUserTradesRequest;
import cttk.request.OpenUserDataStreamRequest;

public class PoloniexPollingUserDataThread
    extends PollingUserDataThread
{
    private UserTrade prevUserTrade = new UserTrade().setDateTime(ZonedDateTime.now());

    PoloniexPollingUserDataThread(
        final CXStream<UserData> cxStream,
        final PrivateCXClient client,
        final OpenUserDataStreamRequest request,
        final CXStreamListener<UserData> listener)
    {
        super(cxStream, client, request, listener);
    }

    @Override
    protected List<UserOrder> getCloseOrdersDuringInterval() {
        UserTrades userTrades = getUserTradesFromLastUserTrade();
        if (userTrades.isEmpty()) return Collections.emptyList();
        prevUserTrade = userTrades.getLatest();

        Set<String> prevOidSet = getPrevOrderOIDSet();
        List<UserOrder> userOrderList = userTrades.getList().stream()
            .map(UserTrade::getOid)
            .distinct()
            .filter(oid -> !prevOidSet.contains(oid))
            .map(oid -> new UserOrder().setOid(oid))
            .collect(Collectors.toList());
        List<UserOrder> closeUserOrders = getUpdatedCloseOrdersEachOrders(userOrderList);
        fillUserTrades(userTrades, closeUserOrders);
        return closeUserOrders;
    }

    private UserTrades getUserTradesFromLastUserTrade() {
        UserTrades userTrades = getUserTradesAtOnce(new UserOrder().setDateTime(prevUserTrade.getDateTime()));
        if (userTrades == null) return new UserTrades();
        if (userTrades.isNotEmpty()) {
            userTrades.filterBy(userTrade -> userTrade.getDateTime().isAfter(prevUserTrade.getDateTime()));
        }
        return userTrades;
    }

    @Override
    protected List<UserOrder> getUpdatedCloseOrders(List<UserOrder> orders) {
        if (orders == null || orders.isEmpty()) return null;
        for (UserOrder order : orders) {
            if (!order.hasTrades()) {
                order.setTrades(getUserTrades(order));
            }
            order.setFilledQuantity(order.getTradedQuantity());
            if (order.hasTrades()) {
                order.setStatus("filled");
                order.setStatusType(OrderStatus.FILLED);
            } else {
                order.setStatus("canceled");
                order.setStatusType(OrderStatus.CANCELED);
            }
        }
        return orders;
    }

    private List<UserTrade> getUserTrades(UserOrder order) {
        try {
            UserTrades userTrades = client.getUserTrades(new GetUserTradesRequest().setOrderId(order.getOid()));
            return (userTrades == null || userTrades.isEmpty()) ? Collections.emptyList() : userTrades.getList();
        } catch (CTTKException e) {
            logger.error("unable to get user trades", e);
            return Collections.emptyList();
        }
    }

    private void fillUserTrades(UserTrades userTrades, List<UserOrder> closeUserOrders) {
        for (UserOrder closeUserOrder : closeUserOrders) {
            for (UserTrade userTrade : userTrades.getList()) {
                if (closeUserOrder.getOid().equals(userTrade.getOid())) {
                    List<UserTrade> trades = closeUserOrder.getTrades();
                    if (trades == null) {
                        trades = new ArrayList<>();
                    }
                    trades.add(userTrade);
                    closeUserOrder.setTrades(trades);
                }
            }
        }
    }

    @Override
    protected void onMessageChangedUserTrades(UserOrders ordersToCheckTrade) {
        UserTrades userTrades = ordersToCheckTrade.getUserTrades();
        if (userTrades != null && userTrades.isNotEmpty()) {
            listener.onMessage(UserData.of(userTrades));
        }
    }
}
