package cttk.impl.bittrex.response;

import java.util.List;

import lombok.Data;

@Data
public class BittrexOrderBook {
    List<BittrexOrder> buy;
    List<BittrexOrder> sell;
}
