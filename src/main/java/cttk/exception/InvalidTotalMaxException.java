package cttk.exception;

import java.math.BigDecimal;

import cttk.CurrencyPair;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class InvalidTotalMaxException
    extends InvalidTotalRangeException
{
    private static final long serialVersionUID = 1L;

    public InvalidTotalMaxException() {
        super();
    }

    public InvalidTotalMaxException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidTotalMaxException(String message) {
        super(message);
    }

    public InvalidTotalMaxException(Throwable cause) {
        super(cause);
    }

    @Override
    public InvalidTotalMaxException setCxId(String cxId) {
        super.setCxId(cxId);
        return this;
    }

    @Override
    public InvalidTotalMaxException setResponseStatusCode(Integer responseStatusCode) {
        super.setResponseStatusCode(responseStatusCode);
        return this;
    }

    @Override
    public InvalidTotalMaxException setExchangeMessage(String exchangeMessage) {
        super.setExchangeMessage(exchangeMessage);
        return this;
    }

    @Override
    public InvalidTotalMaxException setExchangeErrorCode(String exchangeErrorCode) {
        super.setExchangeErrorCode(exchangeErrorCode);
        return this;
    }

    @Override
    public InvalidTotalMaxException setCurrencyPair(CurrencyPair currencyPair) {
        super.setCurrencyPair(currencyPair);
        return this;
    }

    @Override
    public InvalidTotalMaxException setOrderPrice(BigDecimal orderPrice) {
        super.setOrderPrice(orderPrice);
        return this;
    }

    @Override
    public InvalidTotalMaxException setOrderQuantity(BigDecimal orderQuantity) {
        super.setOrderQuantity(orderQuantity);
        return this;
    }

    @Override
    public InvalidTotalMaxException setBaseCurrency(String baseCurrency) {
        super.setBaseCurrency(baseCurrency);
        return this;
    }

    @Override
    public InvalidTotalMaxException setQuoteCurrency(String quoteCurrency) {
        super.setQuoteCurrency(quoteCurrency);
        return this;
    }

    @Override
    public InvalidTotalMaxException setTotal(BigDecimal total) {
        super.setTotal(total);
        return this;
    }
}