package cttk.impl.huobi.response;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.dto.Trade;
import cttk.dto.Trades;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HuobiTradeResponse
    extends HuobiResponse
{
    HuobiTradeTick tick;

    public Trades getTrades(CurrencyPair currencyPair) {
        return new Trades()
            .setCurrencyPair(currencyPair)
            .setTrades(tick.getTradeList());
    }
}

@Data
class HuobiTradeTick {
    Long ts;
    Long id;
    List<HuobiTrade> data;

    public List<Trade> getTradeList() {
        return data == null
            ? null
            : data.stream().map(i -> i.toTrade()).collect(Collectors.toList());

    }
}

@Data
class HuobiTrade {
    BigInteger id;
    @JsonProperty("ts")
    Long timestamp;
    BigDecimal price;
    BigDecimal amount;
    String direction;

    public Trade toTrade() {
        return new Trade()
            .setSno(id)
            .setTid(String.valueOf(id))
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(timestamp))
            .setSide(OrderSide.of(direction))
            .setPrice(price)
            .setQuantity(amount)
            .setTotal(MathUtils.multiply(price, amount));
    }
}
