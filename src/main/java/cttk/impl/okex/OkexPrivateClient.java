package cttk.impl.okex;

import static cttk.dto.UserOrders.Category.ALL;
import static cttk.dto.UserOrders.Category.CLOSED;
import static cttk.dto.UserOrders.Category.OPEN;
import static cttk.impl.okex.OkexUtilsV3.createSignV3;
import static cttk.impl.okex.OkexUtilsV3.extractRequestPathV3;
import static cttk.impl.okex.OkexUtilsV3.refine;
import static cttk.impl.okex.OkexUtilsV3.toOkexCurrencySymbolV3;
import static cttk.impl.okex.OkexUtilsV3.toOkexMarketOrderQuantityV3;
import static cttk.impl.okex.OkexUtilsV3.toOkexMarketSymbolV3;
import static cttk.impl.okex.OkexUtilsV3.toOkexOrderSideV3;
import static cttk.impl.okex.OkexUtilsV3.toOkexOrderStatusV3;
import static cttk.impl.util.HttpMethod.GET;
import static cttk.impl.util.HttpMethod.POST;
import static cttk.impl.util.MathUtils.addOneToLongValueString;
import static cttk.impl.util.Objects.nvl;
import static cttk.util.DeltaUtils.sleep;
import static java.lang.String.format;
import static java.time.ZoneOffset.UTC;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

import cttk.CXClientFactory;
import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.TradingFeeProvider;
import cttk.auth.Credential;
import cttk.dto.Balances;
import cttk.dto.BankAccount;
import cttk.dto.MarketInfos;
import cttk.dto.Transfers;
import cttk.dto.UserAccount;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;
import cttk.dto.Wallet;
import cttk.dto.Wallets;
import cttk.dto.WithdrawResult;
import cttk.exception.CTTKException;
import cttk.exception.InsufficientBalanceException;
import cttk.exception.InvalidAmountMinException;
import cttk.exception.InvalidOrderException;
import cttk.exception.OrderNotFoundException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.feeprovider.DefaultTradingFeeProvider;
import cttk.impl.okex.response.OkexBalancesResponse;
import cttk.impl.okex.response.OkexCreateOrderResponse;
import cttk.impl.okex.response.OkexDepositResponse;
import cttk.impl.okex.response.OkexUserOrderResponse;
import cttk.impl.okex.response.OkexUserOrdersResponse;
import cttk.impl.okex.response.OkexUserTransactionResponse;
import cttk.impl.okex.response.OkexWithdrawalResponse;
import cttk.impl.util.HttpHeaders;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.MediaType;
import cttk.impl.util.ParamsBuilder;
import cttk.request.CancelOrderRequest;
import cttk.request.CreateMarketPriceOrderRequest;
import cttk.request.CreateOrderRequest;
import cttk.request.GetBankAccountRequest;
import cttk.request.GetOpenUserOrdersRequest;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserAccountRequest;
import cttk.request.GetUserOrderHistoryDeltaRequest;
import cttk.request.GetUserOrderHistoryRequest;
import cttk.request.GetUserOrderRequest;
import cttk.request.GetUserTradesDeltaRequest;
import cttk.request.GetUserTradesRequest;
import cttk.request.GetWalletRequest;
import cttk.request.WithdrawToBankRequest;
import cttk.request.WithdrawToWalletRequest;
import cttk.util.CTTKExceptionUtils;
import cttk.util.SleepUtils;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class OkexPrivateClient
    extends AbstractOkexClient
    implements PrivateCXClient
{
    private final Credential credential;
    protected MarketInfos marketInfos;

    public OkexPrivateClient(Credential credential) {
        this.credential = credential;
        try {
            final PublicCXClient pubClient = CXClientFactory.createPublicCXClient(this.getCxId());
            marketInfos = pubClient.getMarketInfos();
        } catch (CTTKException e) {
            logger.error("Failed to getMarketInfos while creating {} PrivateClient.", this.getCxId(), e);
        }
    }

    @Override
    public UserAccount getUserAccount(GetUserAccountRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public TradingFeeProvider getTradingFeeProvider()
        throws CTTKException
    {
        // FIXME consider trading volume
        return new DefaultTradingFeeProvider().setFee("0.002", "0.0015");
    }

    @Override
    public BankAccount getBankAccount(GetBankAccountRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public WithdrawResult withdraw(WithdrawToBankRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public Wallet getWallet(GetWalletRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public Wallets getAllWallets()
        throws CTTKException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public WithdrawResult withdraw(WithdrawToWalletRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(getCxId());
    }

    @Override
    public Balances getAllBalances()
        throws CTTKException
    {
        final HttpResponse response = requestGetWithSign(SPOT_BASE_URL, "/accounts");

        checkResponseError(null, response);

        return convert(response, OkexBalancesResponse.class, OkexBalancesResponse::toBalances);
    }

    /**
     * To get correct status of canceled order you must wait after canceling an order (1s~5s)
     * @param request
     * @return
     * @throws CTTKException
     * @throws UnsupportedCurrencyPairException
     */
    @Override
    public UserOrder getUserOrder(GetUserOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGetWithSign(SPOT_BASE_URL, format("/orders/%s", request.getOid()),

            ParamsBuilder.create()
                .put("instrument_id", toOkexMarketSymbolV3(request))
                .build());

        try {
            checkEmptyOrder(response);
            checkResponseError(request, response);
        } catch (OrderNotFoundException e) {
            return null;
        }

        return convert(response, OkexUserOrderResponse.class, OkexUserOrderResponse::toUserOrder);
    }

    @Override
    public UserOrders getOpenUserOrders(GetOpenUserOrdersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGetWithSign(SPOT_BASE_URL, "/orders_pending",
            ParamsBuilder.create()
                .put("instrument_id", toOkexMarketSymbolV3(request))
                .build());

        checkResponseError(request, response);

        return convert(response, OkexUserOrdersResponse.class, OkexUserOrdersResponse::toUserOrders);
    }

    @Override
    public UserOrders getAllOpenUserOrders(boolean withTrades)
        throws CTTKException
    {
        final UserOrders userOrders = new UserOrders();

        Long pageId = 0L;
        while (true) {
            final HttpResponse response = requestGetWithSign(SPOT_BASE_URL, "/orders_pending", ParamsBuilder.create()
                .putIfNotNull("from", pageId)
                .putIfNotNull("limit", 100)
                .build());

            checkResponseError(null, response);

            if (response.getHeaderValue("OK-BEFORE") == null) {
                break;
            } else {
                final UserOrders tempUserOrders = convert(response, OkexUserOrdersResponse.class, OkexUserOrdersResponse::toUserOrders);
                userOrders.addAll(tempUserOrders);
            }
            pageId = Long.parseUnsignedLong(response.getHeaderValue("OK-BEFORE")) + 1;
            SleepUtils.sleep(200L);
        }

        return userOrders.setCategory(OPEN);
    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGetWithSign(SPOT_BASE_URL, "/orders",
            ParamsBuilder.create()
                .put("status", toOkexOrderStatusV3(request.getStatus()))
                .put("instrument_id", toOkexMarketSymbolV3(request))
                .putIfNotNull("from", request.getFromId())
                .putIfNotNull("to", request.getTillId())
                .putIfNotNull("limit", nvl(request.getCount(), 100))
                .build());

        try {
            checkResponseError(request, response);
        } catch (OrderNotFoundException e) {
            return new UserOrders().setCategory(ALL);
        }

        final UserOrders userOrders = convert(response, OkexUserOrdersResponse.class, OkexUserOrdersResponse::toUserOrders);
        return userOrders.setCategory(ALL);
    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP i-a: id-asc,>>>(break)
        return new UserOrders()
            .addAll(getUserOrderHistoryDelta(request, OrderStatus.CANCELED))
            .addAll(getUserOrderHistoryDelta(request, OrderStatus.FILLED))
            .setCategory(CLOSED);
    }

    private UserOrders getUserOrderHistoryDelta(GetUserOrderHistoryDeltaRequest request, OrderStatus status)
        throws CTTKException
    {
        final GetUserOrderHistoryRequest partialRequest = new GetUserOrderHistoryRequest()
            .setCurrencyPair(request)
            .setStatus(status)
            .setCount(100);

        if (request.hasFromId()) {
            final UserOrders userOrders = new UserOrders();
            String fromId = request.getFromId();
            while (true) {
                partialRequest.setFromId(fromId);
                final UserOrders tempUserOrders = getUserOrderHistory(partialRequest);

                if (tempUserOrders.isEmpty()) break;
                userOrders.addAll(tempUserOrders);
                fromId = addOneToLongValueString(tempUserOrders.getLatest().getOid());

                if (!sleep(request.getSleepTimeMillis())) break;
            }

            return userOrders;
        } else {
            return getUserOrderHistory(partialRequest);
        }
    }

    @Override
    public UserOrder createOrder(CreateOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPostWithSign(SPOT_BASE_URL, "/orders",
            ParamsBuilder.create()
                .put("type", "limit")
                .put("side", toOkexOrderSideV3(request.getSide()))
                .put("instrument_id", toOkexMarketSymbolV3(request))
                .put("margin_trading", 1) // Always 1, in case of limit order
                .put("price", request.getPrice())
                .put("size", request.getQuantity())
                .build());

        checkOrderResponseError(request, response);
        checkResponseError(request, response);

        return fillResponseByRequest(request, convert(response, OkexCreateOrderResponse.class, OkexCreateOrderResponse::toUserOrder));
    }

    @Override
    public void cancelOrder(CancelOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPostWithSign(SPOT_BASE_URL, format("/cancel_orders/%s", request.getOrderId()),
            ParamsBuilder.create()
                .put("instrument_id", toOkexMarketSymbolV3(request))
                .putIfNotNull("client_oid", request.getClientOrderId())
                .build());

        checkResponseError(request, response);
    }

    @Override
    public UserOrder createMarketPriceOrder(CreateMarketPriceOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPostWithSign(SPOT_BASE_URL, "/orders",
            ParamsBuilder.create()
                .put("type", "market")
                .put("side", toOkexOrderSideV3(request.getSide()))
                .put("instrument_id", toOkexMarketSymbolV3(request))
                .put("margin_trading", 1) // Always 1, in case of limit order
                .put(toOkexMarketOrderQuantityV3(request.getSide()), request.getQuantity())
                .build());

        checkResponseError(request, response);

        return fillResponseByRequest(request, convert(response, OkexCreateOrderResponse.class, OkexCreateOrderResponse::toUserOrder));
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGetWithSign(SPOT_BASE_URL, "/orders",
            ParamsBuilder.create()
                .put("status", toOkexOrderStatusV3(OrderStatus.FILLED))
                .put("instrument_id", toOkexMarketSymbolV3(request))
                .putIfNotNull("from", request.getFromId()) // Provide 'oId' not 'tId', unlike other CXs.
                .putIfNotNull("to", request.getTillId())   // Provide 'oId' not 'tId', unlike other CXs.
                .putIfNotNull("limit", nvl(request.getCount(), 100))
                .build());

        checkResponseError(request, response);

        final UserOrders userOrders = convert(response, OkexUserOrdersResponse.class, OkexUserOrdersResponse::toUserOrders);

        return getUserTrades(request, userOrders);
    }

    private UserTrades getUserTrades(GetUserTradesRequest request, UserOrders userOrders)
        throws CTTKException
    {
        final UserTrades userTrades = new UserTrades();
        for (UserOrder userOrder : userOrders.getList()) {
            final UserTrades userTradesOfOrder = getUserTradesOfOrder(userOrder, request);

            userTrades.addAll(userTradesOfOrder);

            SleepUtils.sleepWithException(300L, getCxId());
        }
        return userTrades;
    }

    private UserTrades getUserTradesOfOrder(UserOrder userOrder, GetUserTradesRequest request)
        throws CTTKException
    {
        // id-desc,<<<(exception) - Similar to delta API logic
        final String oid = userOrder.getOid();
        final OrderSide orderSide = userOrder.getSide();
        request.setOrderId(oid);

        final UserTrades userTrades = new UserTrades();
        UserTrade prevFirst = null;
        while (true) {
            final UserTrades tempUserTrades = getPartialUserTradesDelta(request, orderSide);
            final UserTrade first = tempUserTrades.getOldestById();

            if (UserTrade.equalsById(prevFirst, first)) break;
            userTrades.addAll(tempUserTrades);
            prevFirst = first;

            SleepUtils.sleepWithException(300L, getCxId());
        }
        return userTrades;
    }

    private UserTrades getPartialUserTradesDelta(GetUserTradesRequest request, OrderSide orderSide)
        throws CTTKException
    {
        final UserTrades userTrades = new UserTrades();
        String fromId = "0";
        while (true) {
            final UserTrades tempUserTrades = getPartialUserTradesDelta(request.getOrderId(), request, fromId);
            if (tempUserTrades.isEmpty()) break;
            userTrades.addAll(tempUserTrades);
            fromId = addOneToLongValueString(userTrades.getLatestById().getOid());

            SleepUtils.sleepWithException(300L, getCxId());
        }
        return refine(userTrades, orderSide);
    }

    private UserTrades getPartialUserTradesDelta(String orderId, CurrencyPair currencyPair, String fromId)
        throws CTTKException
    {
        final HttpResponse response = requestGetWithSign(SPOT_BASE_URL, "/fills", ParamsBuilder.create()
            .put("order_id", orderId)
            .put("instrument_id", toOkexMarketSymbolV3(currencyPair))
            .put("from", fromId)
            .put("limit", 100)
            .build());

        return convert(response, OkexUserTransactionResponse.class, OkexUserTransactionResponse::toUserTrades);
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP i-a: id-asc,>>>(break)
        if (request.hasFromId()) {
            final GetUserTradesRequest partialRequest = new GetUserTradesRequest()
                .setCurrencyPair(request)
                .setCount(100);

            final UserTrades userTrades = new UserTrades();
            String fromId = request.getFromId();
            while (true) {
                partialRequest.setFromId(fromId);
                final UserTrades temp = getUserTrades(partialRequest);

                if (temp.isEmpty()) break;
                userTrades.addAll(temp);
                fromId = addOneToLongValueString(temp.getLatestById().getOid());

                if (!sleep(request.getSleepTimeMillis())) break;
            }

            return userTrades;
        } else {
            return getUserTrades(new GetUserTradesRequest().setCurrencyPair(request));
        }
    }

    @Override
    public Transfers getTransfers(GetTransfersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // No pagination, no delta API
        final Transfers withdrawalTransfers = getWithdrawalTransfers(request);
        final Transfers depositTransfers = getDepositTransfers(request);
        return new Transfers()
            .addAll(withdrawalTransfers)
            .addAll(depositTransfers);
    }

    private Transfers getWithdrawalTransfers(GetTransfersRequest request)
        throws CTTKException
    {
        final HttpResponse response = requestGetWithSign(ACCOUNT_BASE_URL, format("/withdrawal/history/%s", toOkexCurrencySymbolV3(request.getCurrency())));

        checkResponseError(request, response);

        return convert(response, OkexWithdrawalResponse.class, OkexWithdrawalResponse::toTransfers);
    }

    private Transfers getDepositTransfers(GetTransfersRequest request)
        throws CTTKException
    {
        final HttpResponse response = requestGetWithSign(ACCOUNT_BASE_URL, format("/deposit/history/%s", toOkexCurrencySymbolV3(request.getCurrency())));

        checkResponseError(request, response);

        return convert(response, OkexDepositResponse.class, OkexDepositResponse::toTransfers);
    }

    private HttpResponse requestGetWithSign(String baseUrl, String path)
        throws CTTKException
    {
        return requestGetWithSign(baseUrl, path, null);
    }

    private HttpResponse requestGetWithSign(String baseUrl, String path, Map<String, String> params)
        throws CTTKException
    {
        final String url = getHostUrl(baseUrl, path, params);
        final String requestPath = extractRequestPathV3(url);
        final String timestamp = ZonedDateTime.now(UTC).toString();
        final String sign = createSignV3(timestamp, GET, requestPath, credential.getSecretKey());
        final Request httpRequest = createReuestBuilder(url, timestamp, sign)
            .get()
            .build();

        debug(httpRequest);

        try (final Response httpResponse = super.httpClient.newCall(httpRequest).execute()) {
            return toHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new CTTKException(e.getMessage()).setCxId(getCxId());
        }
    }

    private HttpResponse requestPostWithSign(String baseUrl, String path, Map<String, String> params)
        throws CTTKException
    {
        try {
            final String url = getHostUrl(baseUrl, path, params);
            final String requestPath = extractRequestPathV3(url);
            final String timestamp = ZonedDateTime.now(UTC).toString();
            final String body = objectMapper.writeValueAsString(params);
            final String sign = createSignV3(timestamp, POST, requestPath, body, credential.getSecretKey());
            final Request httpRequest = createReuestBuilder(url, timestamp, sign)
                .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .post(RequestBody.create(JSON, body))
                .build();

            debug(httpRequest);

            Response httpResponse = super.httpClient.newCall(httpRequest).execute();
            return toHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new CTTKException(e.getMessage()).setCxId(getCxId());
        }
    }

    private Request.Builder createReuestBuilder(String url, String timestamp, String sign) {
        return new Request.Builder()
            .url(url)
            .addHeader("OK-ACCESS-TIMESTAMP", timestamp)
            .addHeader("OK-ACCESS-SIGN", sign)
            .addHeader("OK-ACCESS-KEY", credential.getAccessKey())
            .addHeader("OK-ACCESS-PASSPHRASE", credential.getPassword());
    }

    private void checkOrderResponseError(CreateOrderRequest request, HttpResponse response)
        throws CTTKException
    {
        if (!response.is4xx()) return;
        final String body = response.getBody();
        String exchangeErrorCode;
        // String exchangeMessage;

        try {
            final Map<?, ?> errorMap = objectMapper.readValue(body, HashMap.class);
            if (!errorMap.containsKey("code")) return;
            exchangeErrorCode = String.valueOf(errorMap.get("code"));
            // exchangeMessage = (String) errorMap.get("message");
        } catch (IOException e) {
            return;
        }

        switch (exchangeErrorCode) {
            case "33013": {
                // Failed to place an order
                InvalidOrderException ex = CTTKExceptionUtils.createByOrderVerifier(request, null, getCxId(), marketInfos);
                if (ex != null) {
                    throw ex.setCxId(this.getCxId())
                        .setBaseCurrency(request.getBaseCurrency())
                        .setQuoteCurrency(request.getQuoteCurrency())
                        .setOrderPrice(request.getPrice())
                        .setOrderQuantity(request.getQuantity())
                        .setExchangeMessage(response.getBody())
                        .setResponseStatusCode(response.getStatus())
                        .setExchangeErrorCode(exchangeErrorCode);
                }
                break;
            }
            case "33017": { // Greater than the maximum available balance
                throw new InsufficientBalanceException()
                    .setCxId(this.getCxId())
                    .setOrderPrice(request.getPrice())
                    .setOrderQuantity(request.getQuantity())
                    .setBaseCurrency(request.getBaseCurrency())
                    .setQuoteCurrency(request.getQuoteCurrency())
                    .setExchangeMessage(body)
                    .setResponseStatusCode(response.getStatus())
                    .setExchangeErrorCode(exchangeErrorCode);
            }
            case "33024": { //The transaction amount is less than the minimum transaction value
                throw new InvalidAmountMinException()
                    .setCxId(this.getCxId())
                    .setOrderPrice(request.getPrice())
                    .setOrderQuantity(request.getQuantity())
                    .setBaseCurrency(request.getBaseCurrency())
                    .setQuoteCurrency(request.getQuoteCurrency())
                    .setExchangeMessage(body)
                    .setResponseStatusCode(response.getStatus())
                    .setExchangeErrorCode(exchangeErrorCode);
            }
        }
    }

    private void checkEmptyOrder(HttpResponse response)
        throws OrderNotFoundException
    {
        final String body = response.getBody();
        if (response.is2xxSuccessful() && body.length() == 0)
            throw new OrderNotFoundException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(response.getStatus());
    }
}
