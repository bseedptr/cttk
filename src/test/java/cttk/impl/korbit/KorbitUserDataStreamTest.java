package cttk.impl.korbit;

import cttk.CXIds;
import cttk.impl.AbstractUserDataStreamTest;

public class KorbitUserDataStreamTest
    extends AbstractUserDataStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.KORBIT;
    }
}
