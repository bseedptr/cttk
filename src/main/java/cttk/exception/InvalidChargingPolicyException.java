package cttk.exception;

import cttk.CurrencyPair;
import cttk.OrderSide;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class InvalidChargingPolicyException
    extends CTTKException
{
    private static final long serialVersionUID = 1L;

    protected CurrencyPair currencyPair;
    protected OrderSide orderSide;

    public InvalidChargingPolicyException() {
        super();
    }

    public InvalidChargingPolicyException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidChargingPolicyException(String message) {
        super(message);
    }

    public InvalidChargingPolicyException(Throwable cause) {
        super(cause);
    }

    @Override
    public String toString() {
        return super.toString()
            + ", Currency pair = " + currencyPair
            + ", Order Side = " + orderSide;
    }

    @Override
    public InvalidChargingPolicyException setCxId(String cxId) {
        super.setCxId(cxId);
        return this;
    }
}
