package cttk.impl.poloniex.stream;

import cttk.CXStreamListener;
import cttk.PrivateCXClient;
import cttk.UserData;
import cttk.impl.stream.PollingUserDataStream;
import cttk.request.OpenUserDataStreamRequest;

class PoloniexPollingUserDataStream
    extends PollingUserDataStream
{
    PoloniexPollingUserDataStream(final PrivateCXClient client, final OpenUserDataStreamRequest request, final CXStreamListener<UserData> listener) {
        this.pollingUserDataThread = new PoloniexPollingUserDataThread(this, client, request, listener);
        open();
    }
}
