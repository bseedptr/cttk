package cttk.impl.bithumb;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.powermock.api.support.membermodification.MemberMatcher.method;

import java.time.ZonedDateTime;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberModifier;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import cttk.CXClientFactory;
import cttk.PublicCXClient;
import cttk.auth.Credential;
import cttk.dto.Transfers;
import cttk.dto.UserAccount;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.AbstractPrivateClientDeltaTest;
import cttk.request.GetTransfersDeltaRequest;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserOrderHistoryDeltaRequest;
import cttk.request.GetUserOrderHistoryRequest;
import cttk.request.GetUserTradesDeltaRequest;
import cttk.request.GetUserTradesRequest;
import cttk.util.DeltaUtils;
import lombok.extern.slf4j.Slf4j;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ CXClientFactory.class, BithumbPrivateClient.class, UserOrder.class, DeltaUtils.class })
@PowerMockIgnore("javax.net.ssl.*")
@Slf4j
public class BithumbPrivateClientDeltaTest
    extends AbstractPrivateClientDeltaTest
{
    BithumbPrivateClient client;
    GetUserOrderHistoryDeltaRequest orderHistoryRequest;
    GetUserTradesDeltaRequest tradesRequest;
    GetTransfersDeltaRequest transferRequest;

    @Mock
    UserOrders orders1, orders2, orders3;
    @Mock
    UserTrades trades1, trades2, trades3, trades4, trades5;
    @Mock
    Transfers transfers1, transfers2, transfers3, transfers4, transfers5;

    @Before
    public void setup()
        throws CTTKException
    {
        PowerMockito.spy(UserOrder.class);
        PowerMockito.spy(DeltaUtils.class);
        client = spy(createDummyPrivateClient());
        orderHistoryRequest = new GetUserOrderHistoryDeltaRequest().setStartDateTime(ZonedDateTime.now().minusYears(2));
        tradesRequest = new GetUserTradesDeltaRequest().setStartDateTime(ZonedDateTime.now().minusYears(2));
        transferRequest = new GetTransfersDeltaRequest().setStartDateTime(ZonedDateTime.now().minusYears(2));
    }

    BithumbPrivateClient createDummyPrivateClient()
        throws CTTKException
    {
        PowerMockito.mockStatic(CXClientFactory.class);
        PowerMockito.when(CXClientFactory.createPublicCXClient(anyString())).thenReturn(mock(PublicCXClient.class));
        MemberModifier.stub(method(BithumbPrivateClient.class, "getUserAccount")).toReturn(mock(UserAccount.class));
        return new BithumbPrivateClient(mock(Credential.class));
    }

    // CTTK-614
    @Test(expected = UnsupportedMethodException.class)
    public void testGetUserOrderHistoryDeltaWhenNoMoreData()
        throws Exception
    {
        PowerMockito.when(UserOrder.equalsById(any(), any())).thenReturn(false, false, true);
        PowerMockito.when(DeltaUtils.calcNextSeconds(any(UserOrder.class), any(UserOrder.class))).thenReturn(0);
        when(orders1.getList()).thenReturn(makeUserOrderFixtures("1", "2", "3"));
        when(orders1.getOldest()).thenReturn(makeUserOrderFixture("1"));
        when(orders1.getLatest()).thenReturn(makeUserOrderFixture("3"));
        when(orders2.getList()).thenReturn(makeUserOrderFixtures("4", "5"));
        when(orders2.getOldest()).thenReturn(makeUserOrderFixture("4"));
        when(orders2.getLatest()).thenReturn(makeUserOrderFixture("5"));
        when(orders3.getList()).thenReturn(makeUserOrderFixtures());
        doReturn(orders1, orders2, orders3).when(client).getUserOrderHistory(any(GetUserOrderHistoryRequest.class));

        UserOrders result = client.getUserOrderHistory(orderHistoryRequest);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat(ids, containsInAnyOrder("1", "2", "3", "4", "5"));
    }

    // CTTK-614
    @Test(expected = UnsupportedMethodException.class)
    public void testGetUserOrdersDeltaWhenDuplication()
        throws Exception
    {
        PowerMockito.when(UserOrder.equalsById(any(), any())).thenReturn(false, false, true);
        PowerMockito.when(DeltaUtils.calcNextSeconds(any(UserOrder.class), any(UserOrder.class))).thenReturn(0);
        when(orders1.getList()).thenReturn(makeUserOrderFixtures("1", "2", "3", "4"));
        when(orders1.getOldest()).thenReturn(makeUserOrderFixture("1"));
        when(orders1.getLatest()).thenReturn(makeUserOrderFixture("4"));
        when(orders2.getList()).thenReturn(makeUserOrderFixtures("3", "4", "5", "6", "7"));
        when(orders2.getOldest()).thenReturn(makeUserOrderFixture("3"));
        when(orders2.getLatest()).thenReturn(makeUserOrderFixture("7"));
        when(orders3.isEmpty()).thenReturn(true);
        doReturn(orders1, orders2, orders3).when(client).getUserOrderHistory(any(GetUserOrderHistoryRequest.class));

        UserOrders result = client.getUserOrderHistory(orderHistoryRequest);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat("Id should be unique.", ids, containsInAnyOrder("1", "2", "3", "4", "5", "6", "7"));
        verify(client, times(3)).getUserOrderHistory(any(GetUserOrderHistoryRequest.class));
    }

    @Test
    public void testGetUserTradesDeltaWhenNoMoreData()
        throws Exception
    {
        when(trades1.getList()).thenReturn(makeUserTradeFixtures("1", "2", "3"));
        when(trades1.isEmpty()).thenReturn(false);
        when(trades2.getList()).thenReturn(makeUserTradeFixtures());
        when(trades2.isEmpty()).thenReturn(true);
        doReturn(trades1, trades2).when(client).getUserTrades(any(GetUserTradesRequest.class));

        UserTrades result = client.getUserTrades(tradesRequest);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat(ids, containsInAnyOrder("1", "2", "3"));
        verify(trades1, never()).filterByDateTimeGTE(any());
        verify(trades2, never()).filterByDateTimeGTE(any());
    }

    @Test
    public void testGetUserTradesDeltaWhenHasOlderOne()
        throws Exception
    {
        when(trades1.getList()).thenReturn(makeUserTradeFixtures("1", "2", "3"));
        when(trades1.hasOlderOne(any())).thenReturn(false);
        when(trades2.getList()).thenReturn(makeUserTradeFixtures("4"));
        when(trades2.hasOlderOne(any())).thenReturn(true);
        when(trades3.getList()).thenReturn(makeUserTradeFixtures("101", "102", "103"));
        when(trades3.hasOlderOne(any())).thenReturn(false);
        when(trades4.getList()).thenReturn(makeUserTradeFixtures("104"));
        when(trades4.hasOlderOne(any())).thenReturn(true);
        when(trades5.getList()).thenReturn(makeUserTradeFixtures("105", "106", "107"));
        doReturn(trades1, trades2, trades3, trades4, trades5).when(client).getUserTrades(any(GetUserTradesRequest.class));

        UserTrades result = client.getUserTrades(tradesRequest);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat(ids, containsInAnyOrder("1", "2", "3", "4", "101", "102", "103", "104"));
        verify(trades1, never()).filterByDateTimeGTE(any());
        verify(trades2).filterByDateTimeGTE(any());
        verify(trades3, never()).filterByDateTimeGTE(any());
        verify(trades4).filterByDateTimeGTE(any());
        verifyZeroInteractions(trades5);
    }

    @Test
    public void testGetTransfersDeltaWhenNoMoreData()
        throws Exception
    {
        when(transfers1.getList()).thenReturn(makeTransferFixtures("1", "2", "3"));
        when(transfers1.isEmpty()).thenReturn(false);
        when(transfers2.getList()).thenReturn(makeTransferFixtures());
        when(transfers2.isEmpty()).thenReturn(true);
        doReturn(transfers1, transfers2).when(client).getTransfers(any(GetTransfersRequest.class));

        Transfers result = client.getTransfers(transferRequest);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat(ids, containsInAnyOrder("1", "2", "3"));
        assertThat(ids, containsInAnyOrder("1", "2", "3"));
        verify(transfers1, never()).filterByDateTimeGTE(any());
        verify(transfers2, never()).filterByDateTimeGTE(any());
    }

    @Test
    public void testGetTransfersDeltaWhenHasOlderOne()
        throws Exception
    {
        when(transfers1.getList()).thenReturn(makeTransferFixtures("1", "2", "3"));
        when(transfers1.hasOlderOne(any())).thenReturn(false);
        when(transfers2.getList()).thenReturn(makeTransferFixtures("4"));
        when(transfers2.hasOlderOne(any())).thenReturn(true);
        when(transfers3.getList()).thenReturn(makeTransferFixtures("5", "6", "7"));
        when(transfers3.hasOlderOne(any())).thenReturn(false);
        when(transfers4.getList()).thenReturn(makeTransferFixtures("101"));
        when(transfers4.hasOlderOne(any())).thenReturn(true);
        when(transfers5.getList()).thenReturn(makeTransferFixtures("105", "106", "107"));
        doReturn(transfers1, transfers2, transfers3, transfers4, transfers5).when(client).getTransfers(any(GetTransfersRequest.class));

        Transfers result = client.getTransfers(transferRequest);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat(ids, containsInAnyOrder("1", "2", "3", "4", "5", "6", "7", "101"));
        verify(transfers1, never()).filterByDateTimeGTE(any());
        verify(transfers2).filterByDateTimeGTE(any());
        verify(transfers3, never()).filterByDateTimeGTE(any());
        verify(transfers4).filterByDateTimeGTE(any());
        verifyZeroInteractions(transfers5);
    }
}
