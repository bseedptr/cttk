package cttk.impl.stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.exception.APILimitExceedException;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXServerException;
import cttk.request.AbstractCXStreamRequest;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public abstract class AbstractWebsocketListener<T, R extends AbstractCXStreamRequest<R>>
    extends WebSocketListener
{
    protected static final int NORMAL_CLOSURE_STATUS = 1000;

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    protected final String id;
    protected final String cxId;

    protected final CXStream<T> stream;

    protected final String channel;
    protected final R request;
    protected final MessageConverter<T> converter;
    protected final CXStreamListener<T> listener;

    public AbstractWebsocketListener(
        final CXStream<T> stream,
        final String channel,
        final R request,
        final MessageConverter<T> converter,
        final CXStreamListener<T> listener,
        final String cxId)
    {
        super();

        this.stream = stream;
        this.channel = channel;
        this.request = request;
        this.converter = converter;
        this.listener = listener;

        this.id = getId();
        this.cxId = cxId;
    }

    protected String getId() {
        return String.format("%s-%s-%020d", getClass().getSimpleName(), channel, System.currentTimeMillis());
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        logger.info("[{}] Open : {}", id, channel);
        this.listener.onOpen(stream);
    }

    @Override
    public void onMessage(WebSocket webSocket, String text) {
        try {
            handleMessage(webSocket, text);
        } catch (Exception e) {
            listener.onFailure(stream, e);
        }
    }

    @Override
    public void onMessage(WebSocket webSocket, ByteString bytes) {
        try {
            String message = toString(bytes);
            handleMessage(webSocket, message);
        } catch (Exception e) {
            listener.onFailure(stream, e);
        }
    }

    protected abstract void handleMessage(WebSocket webSocket, String text)
        throws Exception;

    protected abstract String toString(ByteString bytes)
        throws Exception;

    @Override
    public void onClosing(WebSocket webSocket, int code, String reason) {
        webSocket.close(code, reason);
        logger.info("[{}] Closing: - {} {} {}", id, channel, code, reason);
        this.listener.onClosing(stream, code, reason);
    }

    @Override
    public void onClosed(WebSocket webSocket, int code, String reason) {
        logger.info("[{}] Closed: - {} {} {}", id, channel, code, reason);
        this.listener.onClosed(stream, code, reason);
    }

    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        if (response == null) {
            if (!(t instanceof CTTKException)) {
                t = new CTTKException(t)
                    .setCxId(cxId);
            }
        } else {
            int statusCode = response.code();
            String exchangeMessage = response.body().toString();

            // Check if the Exception is already handled by overriding onFailure
            if (t instanceof CTTKException) {
                listener.onFailure(stream, t);
                return;
            }

            if (statusCode == 429) {
                t = new APILimitExceedException(t)
                    .setCxId(cxId)
                    .setExchangeMessage(exchangeMessage)
                    .setResponseStatusCode(statusCode);
            } else if (500 <= statusCode && statusCode < 600) {
                t = new CXServerException(t)
                    .setCxId(cxId)
                    .setExchangeMessage(exchangeMessage)
                    .setResponseStatusCode(statusCode);
            } else {
                t = new CXAPIRequestException(t)
                    .setCxId(cxId)
                    .setExchangeMessage(exchangeMessage)
                    .setResponseStatusCode(statusCode);
            }
        }

        listener.onFailure(stream, t);
    }
}
