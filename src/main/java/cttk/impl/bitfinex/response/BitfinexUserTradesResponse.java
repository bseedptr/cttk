package cttk.impl.bitfinex.response;

import static cttk.impl.bitfinex.BitfinexUtils.stringTimestampToLong;
import static cttk.impl.bitfinex.BitfinexUtils.toStdCurrencyPair;
import static cttk.impl.bitfinex.BitfinexUtils.toStdCurrencySymbol;

import java.math.BigDecimal;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.dto.UserTrade;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;

@Data
public class BitfinexUserTradesResponse {
    Long tid;
    Long order_id;
    BigDecimal price;
    BigDecimal amount;
    String timestamp;
    String exchange;
    String type;
    String fee_currency;
    BigDecimal fee_amount;

    public UserTrade toUserTrade(CurrencyPair currencyPair) {
        return new UserTrade()
            .setCurrencyPair(toStdCurrencyPair(currencyPair))
            .setTid(String.valueOf(tid))
            .setOid(String.valueOf(order_id))
            .setDateTime(DateTimeUtils.zdtFromEpochSecond(stringTimestampToLong(timestamp)))
            .setSide(OrderSide.of(type))
            .setPrice(price)
            .setQuantity(amount.abs())
            .setTotal(MathUtils.multiply(price, amount.abs()))
            .setFee(MathUtils.abs(fee_amount))
            .setFeeCurrency(toStdCurrencySymbol(fee_currency));
    }
}
