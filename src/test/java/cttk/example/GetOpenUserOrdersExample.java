package cttk.example;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cttk.CTTK;
import cttk.CXClientFactory;
import cttk.CXIds;
import cttk.CurrencyPair;
import cttk.PrivateCXClient;
import cttk.auth.CXCredentialsProvider;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.exception.UnsupportedCXException;
import cttk.exception.UnsupportedMethodException;
import cttk.request.GetOpenUserOrdersRequest;
import cttk.request.GetUserOrderHistoryRequest;
import cttk.test.TestConfig;

public class GetOpenUserOrdersExample {

    public static void main(String[] args)
        throws IOException
    {
        List<String> cxIds = CTTK.getSupportedCxIds();

        Set<String> errorCxIds = new HashSet<>(Arrays.asList(CXIds.BITFINEX));

        for (String cxId : cxIds) {
            if (!errorCxIds.contains(cxId)) continue;
            final TestConfig testConfig = TestConfig.readFromClasspathYaml("config/" + cxId.toLowerCase() + ".yml");
            final CurrencyPair currencyPair = testConfig.getValidCurrencyPair();

            try {
                final PrivateCXClient client = CXClientFactory.createPrivateCXClient(cxId, CXCredentialsProvider.getDefaultCredential(cxId).getDataKey());

                for (int i = 0; i < 2; i++) {
                    final UserOrders userOrders = i == 0
                        ? client.getOpenUserOrders(new GetOpenUserOrdersRequest().setCurrencyPair(currencyPair))
                        : client.getUserOrderHistory(new GetUserOrderHistoryRequest().setCurrencyPair(currencyPair));

                    System.out.println(String.format("[%10s]======================================================================", cxId));
                    for (UserOrder userOrder : userOrders.getOrders()) {
                        //Examples.print(cxId, userOrder);
                        System.out.println(userOrder.getStatusType());
                    }
                    System.out.println(String.format("[%10s]======================================================================", cxId));
                }

            } catch (UnsupportedMethodException e) {
                continue;
            } catch (UnsupportedCXException e) {
                continue;
            } catch (java.io.FileNotFoundException e) {
                continue;
            } catch (Exception e) {
                System.err.println(cxId);
                e.printStackTrace();
                continue;
            }
        }
    }
}
