package cttk.dto;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import cttk.impl.util.JsonUtils;

public class DeserializationTest {
    @Test
    public void testTickerDeserialization()
        throws IOException
    {
        final ObjectMapper objectMapper = JsonUtils.createDefaultObjectMapper();
        try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("data/ticker.json")) {
            Ticker ticker = objectMapper.readValue(is, Ticker.class);
            Assert.assertNotNull(ticker);
        }
    }
}
