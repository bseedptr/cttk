package cttk.impl.upbit;

import static cttk.OrderStatus.CANCELED;
import static cttk.OrderStatus.FILLED;
import static cttk.OrderStatus.UNFILLED;
import static cttk.util.StringUtils.toUpper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.databind.ObjectMapper;

import cttk.CXIds;
import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.exception.APILimitExceedException;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXAuthorityException;
import cttk.exception.OrderNotFoundException;
import cttk.exception.UnsupportedMethodException;

public class UpbitUtils {

    public static final BigDecimal[] PRICE_RANGES = {
        new BigDecimal("2000000"),
        new BigDecimal("1000000"),
        new BigDecimal("500000"),
        new BigDecimal("100000"),
        new BigDecimal("10000"),
        new BigDecimal("1000"),
        new BigDecimal("100"),
        new BigDecimal("10"),
    };

    public static final BigDecimal[] PRICE_INCREMENTS = {
        new BigDecimal("1000"),
        new BigDecimal("500"),
        new BigDecimal("100"),
        new BigDecimal("50"),
        new BigDecimal("10"),
        new BigDecimal("5"),
        new BigDecimal("1"),
        new BigDecimal("0.1"),
        new BigDecimal("0.01")
    };

    public static ObjectMapper objectMapper = new ObjectMapper();
    final static String cxId = CXIds.UPBIT;

    public static CurrencyPair parseMarketSymbol(String market) {
        final String[] arr = market.split("-");
        return CurrencyPair.of(toUpper(arr[1]), toUpper(arr[0]));
    }

    public static String getMarketSymbol(CurrencyPair currencyPair) {
        return currencyPair == null ? null : getMarketSymbol(currencyPair.getBaseCurrency(), currencyPair.getQuoteCurrency());
    }

    public static String getMarketSymbol(String baseCurrency, String quoteCurrency) {
        if (baseCurrency == null || quoteCurrency == null) return null;
        return quoteCurrency.toUpperCase() + "-" + baseCurrency.toUpperCase();
    }

    public static String urlEncodeUTF8(String s)
        throws CTTKException
    {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedMethodException(e);
        }
    }

    public static String urlEncodeUTF8(Map<?, ?> map)
        throws CTTKException
    {
        if (map == null) return null;

        StringBuilder sb = new StringBuilder();
        for (Map.Entry<?, ?> entry : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(String.format("%s=%s",
                urlEncodeUTF8(entry.getKey().toString()),
                urlEncodeUTF8(entry.getValue().toString())));
        }
        return sb.toString();
    }

    public static String toSideString(OrderSide side) {
        return OrderSide.BUY.equals(side) ? "bid" : "ask";
    }

    public static void checkErrorByResponseMessage(Object requestObj, String body, Integer statusCode)
        throws CXAPIRequestException
    {

        Map<?, ?> map;
        try {
            Map<?, ?> mapWrapper = objectMapper.readValue(body, Map.class);
            map = (Map<?, ?>) mapWrapper.get("error");
        } catch (IOException | NullPointerException e) {
            return;
        }

        if (map == null) return;

        final String exchangeMessage = Objects.toString(map.get("message"), "");
        final String exchangeErrorCode = Objects.toString(map.get("name"), "");
        final String message = String.format("name: %s, message: %s", exchangeErrorCode, exchangeMessage);

        if (exchangeMessage.contains("market does not have a valid value")
            || exchangeMessage.contains("Code not found")
            || exchangeMessage.contains("currency does not have a valid value"))
        {
            throw CXAPIRequestException.createByRequestObj(requestObj, message)
                .setCxId(cxId)
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setExchangeErrorCode(exchangeErrorCode);
        } else if (exchangeErrorCode.contains("no_authorization_i_p")
            || exchangeMessage.contains("Too many API requests"))
        {
            throw new APILimitExceedException(message)
                .setCxId(cxId)
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setExchangeErrorCode(exchangeErrorCode);
        } else if (exchangeMessage.contains("side is missing, side does not have a valid value")
            || exchangeErrorCode.contains("order_not_found"))
        {
            throw new OrderNotFoundException(message)
                .setCxId(cxId)
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setExchangeErrorCode(exchangeErrorCode);
        } else if (exchangeErrorCode.contains("invalid_access_key")
            || exchangeMessage.contains("jwt_verification")
            || exchangeMessage.contains("thirdparty_agreement_required"))
        {
            throw new CXAuthorityException(message)
                .setCxId(cxId)
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setExchangeErrorCode(exchangeErrorCode);
        } else {
            // "out_of_scope"
            // "Missing request parameter error. Check the required parametersnot"
            throw new CXAPIRequestException(message)
                .setCxId(cxId)
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setExchangeErrorCode(exchangeErrorCode);
        }
    }

    public static OrderStatus getStatusType(final String status) {
        if (status == null) {
            return null;
        }
        switch (status) {
            case "wait":
                return UNFILLED;
            case "done":
                return FILLED;
            case "cancel":
                return CANCELED;
            default:
                return null;
        }
    }

    public static String toStateString(final OrderStatus status) {
        if (status == null)
            return null;
        switch (status) {
            case UNFILLED:
                return "wait";
            case FILLED:
                return "done";
            case CANCELED:
                return "cancel";
            default:
                return null;
        }
    }
}
