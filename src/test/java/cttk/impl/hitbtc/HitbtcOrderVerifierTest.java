package cttk.impl.hitbtc;

import cttk.CXIds;
import cttk.impl.AbstractOrderVerifierTest;

public class HitbtcOrderVerifierTest
    extends AbstractOrderVerifierTest
{
    @Override
    public String getCxId() {
        return CXIds.HITBTC;
    }
}
