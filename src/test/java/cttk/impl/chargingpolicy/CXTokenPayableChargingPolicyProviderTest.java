package cttk.impl.chargingpolicy;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import cttk.CXIds;
import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.dto.ChargingPolicy;
import cttk.dto.ChargingPolicy.Criteria;
import cttk.exception.CTTKException;
import cttk.exception.InvalidChargingPolicyException;

public class CXTokenPayableChargingPolicyProviderTest {
    @Rule
    public ExpectedException ee = ExpectedException.none();

    private CXTokenPayableChargingPolicyProvider provider;
    private CurrencyPair pair;

    @Before
    public void setUp() {
        provider = new CXTokenPayableChargingPolicyProvider(CXIds.BINANCE, "BIX");
        pair = CurrencyPair.of("EOS", "ETH");
    }

    @Test
    public void testGetChargingPoliciesWhenCurrencyPairIsNull()
        throws CTTKException
    {
        ee.expect(InvalidChargingPolicyException.class);
        ee.expectMessage("Both CurrencyPair and OrderSide are required.");

        provider.getChargingPolicies(null, OrderSide.BUY);
    }

    @Test
    public void testGetChargingPoliciesWhenOrderSideIsNull()
        throws CTTKException
    {
        ee.expect(InvalidChargingPolicyException.class);
        ee.expectMessage("Both CurrencyPair and OrderSide are required.");

        provider.getChargingPolicies(pair, null);
    }

    @Test
    public void getChargingPoliciesWhenBuySide()
        throws CTTKException
    {
        List<ChargingPolicy> policies = provider.getChargingPolicies(pair, OrderSide.BUY);
        assertThat("Must provide 2 charging policies regardless of order side.", policies, hasSize(2));
        assertChargingPoliciesHaveBIX(policies);
        assertBuyChargingPolicy(policies.get(0));
        assertBuyChargingPolicy(policies.get(1));
    }

    @Test
    public void getChargingPoliciesWhenSellSide()
        throws CTTKException
    {
        List<ChargingPolicy> policies = provider.getChargingPolicies(pair, OrderSide.SELL);
        assertThat("Must provide 2 charging policies regardless of order side.", policies, hasSize(2));
        assertChargingPoliciesHaveBIX(policies);
        assertSellChargingPolicy(policies.get(0));
        assertSellChargingPolicy(policies.get(1));
    }

    private void assertChargingPoliciesHaveBIX(List<ChargingPolicy> policies) {
        List<String> feeCurrencies = policies.stream().map(ChargingPolicy::getFeeCurrency).collect(toList());
        assertThat("Must provide exchange token.", feeCurrencies, hasItems("BIX"));
    }

    private void assertBuyChargingPolicy(ChargingPolicy policy) {
        assertThat(policy.getPairString(), is(pair.getPairString()));
        assertThat(policy.getOrderSide(), is(OrderSide.BUY));
        if (policy.getFeeCurrency().equals("BIX")) {
            assertThat(policy.getFeeCurrency(), is("BIX"));
            assertThat(policy.getCriteria(), is(Criteria.ADD));
            assertTrue("Should return true if exchange token.", policy.isCXToken());
        } else {
            assertThat(policy.getFeeCurrency(), is(pair.getBaseCurrency()));
            assertThat(policy.getCriteria(), is(Criteria.SUB));
            assertFalse(policy.isCXToken());
        }
    }

    private void assertSellChargingPolicy(ChargingPolicy policy) {
        assertThat(policy.getPairString(), is(pair.getPairString()));
        assertThat(policy.getOrderSide(), is(OrderSide.SELL));
        if (policy.getFeeCurrency().equals("BIX")) {
            assertThat(policy.getFeeCurrency(), is("BIX"));
            assertThat(policy.getCriteria(), is(Criteria.ADD));
            assertTrue("Should return true if exchange token.", policy.isCXToken());
        } else {
            assertThat(policy.getFeeCurrency(), is(pair.getQuoteCurrency()));
            assertThat(policy.getCriteria(), is(Criteria.SUB));
            assertFalse(policy.isCXToken());
        }
    }
}