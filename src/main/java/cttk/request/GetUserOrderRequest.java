package cttk.request;

import cttk.AbstractCurrencyPair;
import cttk.OrderSide;
import cttk.dto.UserOrder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class GetUserOrderRequest
    extends AbstractCurrencyPair<GetUserOrderRequest>
{
    OrderSide side;
    String oid;
    String clientOrderId;
    boolean withTrades;

    // korbit
    Integer offset;
    Integer count;

    public static GetUserOrderRequest from(UserOrder userOrder) {
        return userOrder == null ? null : new GetUserOrderRequest()
            .setCurrencyPair(userOrder)
            .setSide(userOrder.getSide())
            .setOid(userOrder.getOid())
            .setClientOrderId(userOrder.getClientOrderId());
    }
}
