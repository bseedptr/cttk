package cttk.impl.korbit;

import static cttk.impl.korbit.KorbitUtils.getMarketSymbol;
import static cttk.impl.util.Objects.nvl;
import static cttk.util.StringUtils.toLower;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import cttk.PublicCXClient;
import cttk.dto.MarketInfos;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Tickers;
import cttk.dto.Trade;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.korbit.response.KorbitConstantsResponse;
import cttk.impl.korbit.response.KorbitOrderBookResponse;
import cttk.impl.korbit.response.KorbitTickerResponse;
import cttk.impl.korbit.response.KorbitTrade;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ParamsBuilder;
import cttk.request.GetOrderBookRequest;
import cttk.request.GetRecentTradesRequest;
import cttk.request.GetTickerRequest;
import cttk.request.GetTradeHistoryRequest;

public class KorbitPublicClient
    extends AbstractKorbitClient
    implements PublicCXClient
{
    public KorbitPublicClient() {
        super();
    }

    @Override
    public MarketInfos getMarketInfos()
        throws CTTKException
    {
        final HttpResponse response = requestGet("/constants");

        checkResponseError(null, response);

        return convert(response, KorbitConstantsResponse.class, i -> i.toMarketInfos());
    }

    @Override
    public Ticker getTicker(GetTickerRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        checkQuoteKRW(request);
        final HttpResponse response = requestGet("/ticker/detailed",
            ParamsBuilder.build("currency_pair", getMarketSymbol(request)));

        checkResponseError(request, response);

        return convert(response, KorbitTickerResponse.class, (i -> i.toTicker(request)));
    }

    @Override
    public Tickers getAllTickers()
        throws CTTKException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public OrderBook getOrderBook(GetOrderBookRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        checkQuoteKRW(request);
        final HttpResponse response = requestGet("/orderbook",
            ParamsBuilder.build("currency_pair", getMarketSymbol(request)));

        checkResponseError(request, response);

        return convert(response, KorbitOrderBookResponse.class, (i -> i.toOrderBook(request)));
    }

    @Override
    public Trades getRecentTrades(GetRecentTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        checkQuoteKRW(request);
        final HttpResponse response = requestGet("/transactions",
            ParamsBuilder.create()
                .putIfNotNull("currency_pair", getMarketSymbol(request))
                .putIfNotNull("time", nvl(toLower(request.getPeriod()), "day"))
                .build());

        checkResponseError(request, response);

        try {
            return convertList(response, KorbitTrade.class,
                (data) -> new Trades()
                    .setCurrencyPair(request)
                    .setTime(request.getPeriod())
                    .setTrades(toTrades(data)));
        } catch (IOException e) {
            throw new CXAPIRequestException(e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public Trades getTradeHistory(GetTradeHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    private List<Trade> toTrades(List<KorbitTrade> list) {
        if (list == null) return null;
        return list.stream().map(KorbitTrade::toTrade).collect(Collectors.toList());
    }

}
