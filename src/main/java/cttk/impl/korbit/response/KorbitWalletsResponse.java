package cttk.impl.korbit.response;

import lombok.Data;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import cttk.dto.BankAccount;
import cttk.dto.Wallet;
import cttk.dto.Wallets;

@Data
public class KorbitWalletsResponse {
    Deposit deposit;

    public Wallets toWallets() {
        return new Wallets()
            .setWallets(getWalletList(deposit));
    }

    public List<Wallet> getWalletList(Deposit deposit) {
        if (deposit == null || deposit.isEmpty()) {
            return null;
        }
        List<Wallet> walletList = new LinkedList<>();

        for (Map.Entry<String, HashMap<String, String>> currency : deposit.entrySet()) {
            Wallet wallet = new Wallet();
            if ("xrp".equals(currency.getKey())) {
                wallet.setDestinationTag(currency.getValue().get("destination_tag"));
            }
            wallet.setAddress(currency.getValue().get("address"));
            wallet.setCurrency(currency.getKey());
            walletList.add(wallet);
        }
        return walletList;
    }

    public BankAccount toBankAccount() {
        if (deposit == null || deposit.isEmpty() || deposit.get("krw") == null) {
            return new BankAccount();
        }
        Map<String, String> krwMap = deposit.get("krw");
        return new BankAccount()
            .setBankName(krwMap.get("bank_name"))
            .setAccountNo(krwMap.get("account_number"))
            .setBankUser(krwMap.get("account_name"));
    }
}

class Deposit
    extends HashMap<String, HashMap<String, String>>
{
    private static final long serialVersionUID = 1L;
}