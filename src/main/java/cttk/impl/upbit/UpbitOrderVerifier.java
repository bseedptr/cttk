package cttk.impl.upbit;

import static cttk.impl.upbit.UpbitUtils.PRICE_INCREMENTS;
import static cttk.impl.upbit.UpbitUtils.PRICE_RANGES;
import static cttk.impl.util.Objects.nvl;

import java.math.BigDecimal;

import cttk.dto.MarketInfo;
import cttk.impl.MarketInfoOrderVerifier;
import cttk.impl.util.MathUtils;

public class UpbitOrderVerifier
    extends MarketInfoOrderVerifier
{
    public UpbitOrderVerifier(MarketInfo marketInfo) {
        super(marketInfo);
    }

    @Override
    public BigDecimal roundPrice(BigDecimal price) {
        if (price == null) return null;

        if (marketInfo.getQuoteCurrency().equals("KRW")) {
            final BigDecimal increment = nvl(MathUtils.findIncrement(PRICE_RANGES, PRICE_INCREMENTS, price), getPriceIncrement());
            return increment == null ? price : MathUtils.round(price, increment);
        } else {
            return super.roundPrice(price);
        }
    }
}
