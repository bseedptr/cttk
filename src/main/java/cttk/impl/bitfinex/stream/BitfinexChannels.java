package cttk.impl.bitfinex.stream;

public class BitfinexChannels {
    public static final String getTickerChannel() {
        return "ticker";
    }

    public static final String getOrderBookChannel() {
        return "book";
    }

    public static final String getTradeChannel() {
        return "trades";
    }
}
