package cttk.impl.korbit;

import org.junit.Test;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.AbstractPrivateCXClientWithdrawTest;

public class KorbitPrivateClientWithdrawTest
    extends AbstractPrivateCXClientWithdrawTest
{
    @Override
    public String getCxId() {
        return CXIds.KORBIT;
    }

    // FIXME
    @Test
    @Override
    public void testWithdrawToBankAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
    }

    // FIXME
    @Test
    @Override
    public void testWithdrawBTCToWallet()
        throws CTTKException, UnsupportedCurrencyPairException
    {
    }
}
