package cttk.impl.okex.response;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.dto.OrderBook;
import cttk.dto.OrderBook.SimpleOrder;
import lombok.Data;

@Data
public class OkexOrderBookResponse {
    String timestamp;
    List<List<BigDecimal>> bids;
    List<List<BigDecimal>> asks;

    public OrderBook toOrderBook(CurrencyPair currencyPair) {
        return new OrderBook()
            .setCurrencyPair(currencyPair)
            .setBids(toBidOrderBooks())
            .setAsks(toAskOrderBooks());
    }

    List<SimpleOrder> toBidOrderBooks() {
        return bids.stream()
            .map(l -> new SimpleOrder(l.get(0), l.get(1), l.get(2)))
            .collect(Collectors.toList());
    }

    List<SimpleOrder> toAskOrderBooks() {
        return asks.stream()
            .map(l -> new SimpleOrder(l.get(0), l.get(1), l.get(2)))
            .collect(Collectors.toList());
    }
}
