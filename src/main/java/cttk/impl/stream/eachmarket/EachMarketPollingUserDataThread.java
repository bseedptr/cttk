package cttk.impl.stream.eachmarket;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.CurrencyPair;
import cttk.OrderStatus;
import cttk.UserData;
import cttk.dto.Balance;
import cttk.dto.Balances;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.exception.CTTKException;
import cttk.impl.stream.PollingUserDataThread;
import cttk.impl.util.ThrowableSupplier;
import cttk.request.GetUserOrderHistoryRequest;
import cttk.request.OpenUserDataStreamRequest;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EachMarketPollingUserDataThread
    extends PollingUserDataThread
{
    protected final LimitedCXClient cxClient;
    private final PollingLevel pollingLevel;
    private final CoolTime coolTime;
    private final LevelPairMap levelPairMap;
    private final ZonedDateTime instanceCreateTime = ZonedDateTime.now();
    private final Map<CurrencyPair, UserOrder> lastCloseUserOrderMap;

    public EachMarketPollingUserDataThread(
        final CXStream<UserData> cxStream,
        final PollingLevel pollingLevel,
        final CXStreamListener<UserData> listener,
        final LimitedCXClient cxClient,
        final OpenUserDataStreamRequest request,
        final LevelPairMap levelPairMap,
        final Map<CurrencyPair, UserOrder> lastCloseUserOrderMap)
    {
        super(cxStream, cxClient.getPrivateClient(), request, listener);
        super.setName("Thread-" + pollingLevel);
        this.pollingLevel = pollingLevel;
        this.cxClient = cxClient;
        this.coolTime = request.getCoolTime();
        this.levelPairMap = levelPairMap;
        this.lastCloseUserOrderMap = lastCloseUserOrderMap;
    }

    @Override
    public void run() {
        listener.onOpen(cxStream);
        while (isOpened) {
            try {
                runService();
                final int sleepTime = coolTime.get(pollingLevel);
                log.debug("Polling thread cool time: {}ms", sleepTime);
                sleep(sleepTime);
            } catch (InterruptedException e) {
                log.error("{}", e);
            } catch (Exception e) {
                log.error("Unable to get data from source", e);
                listener.onFailure(cxStream, e);
                break;
            }
        }
        logger.info("Stream closed");
        listener.onClosed(cxStream, 0, "");
    }

    @Override
    protected List<UserOrder> getCloseOrdersDuringInterval() {
        Set<String> prevOidSet = getPrevOrderOIDSet();
        List<UserOrder> closeOrdersDuringInterval = new ArrayList<>();
        for (CurrencyPair pair : levelPairMap.getPairs(pollingLevel)) {
            UserOrders closeOrders = getCloseUserOrdersDuringInterval(pair);
            if (closeOrders.isNotEmpty()) {
                lastCloseUserOrderMap.put(pair, closeOrders.getLatest());

                closeOrders.filterBy(closeOrder -> !prevOidSet.contains(closeOrder.getOid()));
                closeOrdersDuringInterval.addAll(closeOrders.getList());
            }
        }
        return closeOrdersDuringInterval;
    }

    private UserOrders getCloseUserOrdersDuringInterval(CurrencyPair pair) {
        try {
            final UserOrder lastCloseUserOrder = lastCloseUserOrderMap.containsKey(pair) ? lastCloseUserOrderMap.get(pair) : new UserOrder().setDateTime(instanceCreateTime);
            UserOrders userOrderHistory = cxClient.getUserOrderHistory(lastCloseUserOrder, pair);
            return userOrderHistory == null ? EMPTY_USER_ORDERS : userOrderHistory.filterBy(orderHistory -> orderHistory.getDateTime().isAfter(lastCloseUserOrder.getDateTime()));
        } catch (CTTKException e) {
            logger.error("unable to get order history", e);
            return EMPTY_USER_ORDERS;
        }
    }

    @Override
    protected UserOrders getAllOpenUserOrders()
        throws CTTKException
    {
        Balances balances = cxClient.getBalances();
        UserOrders collectedOrders = new UserOrders().setOrders(new ArrayList<>());
        for (CurrencyPair pair : levelPairMap.getPairs(pollingLevel)) {
            UserOrders orders = cxClient.getOpenUserOrders(pair);
            if (orders.isNotEmpty()) {
                collectedOrders.addAll(orders);
                levelPairMap.changeLevel(pair, PollingLevel.LEVEL1);
            } else {
                if (needToCheckBalance()) balances = getBalancesAndUpdateCheckTime();
                if (isIncludePair(balances, pair)) {
                    levelPairMap.changeLevel(pair, PollingLevel.LEVEL2);
                } else {
                    levelPairMap.changeLevel(pair, PollingLevel.LEVEL3);
                }
            }
        }
        return collectedOrders;
    }

    private boolean isIncludePair(Balances balances, CurrencyPair pair) {
        return balances
            .getBalances()
            .stream()
            .filter(Balance::isTotalExists)
            .map(Balance::getCurrency)
            .anyMatch(currency -> currency.equalsIgnoreCase(pair.getBaseCurrency()) || currency.equalsIgnoreCase(pair.getQuoteCurrency()));
    }

    @Override
    protected List<UserOrder> getUpdatedCloseOrders(List<UserOrder> orders) {
        List<UserOrder> updatedUserOrderList = getUserOrderHistory(orders);
        UserOrdersFindResult result = UserOrdersFindResult.findInSearchTargetList(orders, updatedUserOrderList);
        return result.existMissingOrder() ? getStatusUpdatedOrdersIncludeCanceledOrders(result) : result.getFoundOrders();
    }

    private List<UserOrder> getUserOrderHistory(List<UserOrder> orders) {
        List<UserOrder> userOrderList = new ArrayList<>(orders.size());
        Set<CurrencyPair> currencyPairSet = orders.stream().map(CurrencyPair::toSimpleCurrencyPair).collect(Collectors.toSet());
        for (CurrencyPair pair : currencyPairSet) {
            try {
                UserOrders userOrders = getDataWithRetry(() -> client.getUserOrderHistory(new GetUserOrderHistoryRequest().setStartDateTime(ZonedDateTime.now().minusMinutes(5)).setCurrencyPair(pair)));
                if (userOrders != null && userOrders.isNotEmpty()) userOrderList.addAll(userOrders.getList());
            } catch (CTTKException e) {
                logger.error("unable to get user order history", e);
                orders.stream().filter(pair::matched).forEach(statusUpdatedUserOrderFailList::add);
            }
        }
        return userOrderList;
    }

    private List<UserOrder> getStatusUpdatedOrdersIncludeCanceledOrders(UserOrdersFindResult result) {
        List<UserOrder> resultList = result.getFoundOrders();
        String status = OrderStatus.CANCELED.name().toLowerCase();
        for (UserOrder userOrder : result.getMissingOrders()) {
            resultList.add(userOrder.setStatusType(OrderStatus.CANCELED).setStatus(status));
        }
        return resultList;
    }

    @Override
    protected <T> T getDataWithRetry(ThrowableSupplier<T> supplier)
        throws CTTKException
    {
        return cxClient.getDataWithRetryAndCheckRateLimit(1, supplier);
    }
}