package cttk.impl.bitfinex.stream;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import cttk.CurrencyPair;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Trades;
import cttk.impl.bitfinex.stream.response.BitfinexBigDecimalListListResponse;
import cttk.impl.bitfinex.stream.response.BitfinexBigDecimalListResponse;
import cttk.impl.bitfinex.stream.response.BitfinexStringListListResponse;
import cttk.impl.bitfinex.stream.response.BitfinexStringListResponse;
import cttk.impl.bitfinex.stream.response.BitfinexWebsocketTickerResponse;
import cttk.impl.stream.MessageConverter;

public class BitfinexWebsocketMessageConverterFactory {

    private static final ObjectMapper createObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper;
    }

    public static MessageConverter<Ticker> createTickerMessageConverter(CurrencyPair currencyPair) {
        return new MessageConverter<Ticker>() {
            private final ObjectMapper mapper = createObjectMapper();

            @Override
            public Ticker convert(String message)
                throws Exception
            {
                return mapper.readValue(message, BitfinexWebsocketTickerResponse.class).toTicker(currencyPair);
            }
        };
    }

    public static MessageConverter<OrderBook> createOrderBookMessageConverter(CurrencyPair currencyPair) {
        return new MessageConverter<OrderBook>() {
            private final ObjectMapper mapper = createObjectMapper();

            @Override
            public OrderBook convert(String message)
                throws Exception
            {
                if (message.endsWith("]]]")) {
                    final String json = message.substring(message.indexOf("[["), message.length() - 1);
                    return mapper.readValue(json, BitfinexBigDecimalListListResponse.class).toOrderBook(currencyPair);
                } else {
                    return mapper.readValue(message, BitfinexBigDecimalListResponse.class).toOrderBook(currencyPair);
                }
            }
        };
    }

    public static MessageConverter<Trades> createTradesMessageConverter(CurrencyPair currencyPair) {
        return new MessageConverter<Trades>() {
            private final ObjectMapper mapper = createObjectMapper();

            @Override
            public Trades convert(String message)
                throws Exception
            {
                if (message.endsWith("]]]")) {
                    final String json = message.substring(message.indexOf("[["), message.length() - 1);
                    return mapper.readValue(json, BitfinexStringListListResponse.class).toTrades(currencyPair);
                } else {
                    return mapper.readValue(message, BitfinexStringListResponse.class).toTrades(currencyPair);
                }
            }
        };
    }
}
