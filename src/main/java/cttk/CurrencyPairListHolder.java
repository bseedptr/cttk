package cttk;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface CurrencyPairListHolder<C extends CurrencyPairListHolder<C, T>, T extends CurrencyPair> {
    List<T> getList();

    C setList(List<T> list);

    @JsonIgnore
    default List<CurrencyPair> getDistinctCurrencyPairList() {
        return getList() == null ? Collections.emptyList()
            : getList().stream()
                .map(CurrencyPair::toSimpleCurrencyPair)
                .distinct()
                .collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
    default C filterByCurrencyPair(CurrencyPair currencyPair) {
        if (getList() != null && currencyPair != null && (currencyPair.getBaseCurrency() != null || currencyPair.getQuoteCurrency() != null)) {
            final List<T> newList = getList().stream()
                .filter(i -> i.matched(currencyPair))
                .collect(Collectors.toList());
            setList(newList);
        }
        return (C) this;
    }
}
