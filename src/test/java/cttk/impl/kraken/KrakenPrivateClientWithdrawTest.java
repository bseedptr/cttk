package cttk.impl.kraken;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Test;

import cttk.CXIds;
import cttk.dto.WithdrawResult;
import cttk.exception.CTTKException;
import cttk.exception.InvalidWithdrawException;
import cttk.exception.UnsupportedCXException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.AbstractPrivateCXClientWithdrawTest;
import cttk.request.WithdrawToBankRequest;
import cttk.request.WithdrawToWalletRequest;

public class KrakenPrivateClientWithdrawTest
    extends AbstractPrivateCXClientWithdrawTest
{
    @Override
    public String getCxId() {
        return CXIds.KRAKEN;
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testWithdrawToBankAccount()
        throws UnsupportedCurrencyPairException, CTTKException
    {
        client.withdraw(new WithdrawToBankRequest()
            .setBankCode("020")
            .setAccountNo("1002153800385")
            .setAmount("10000"));
    }

    @Test
    @Override
    public void testWithdrawBTCToWallet()
        throws UnsupportedCurrencyPairException, CTTKException
    {
        WithdrawResult result = client.withdraw(new WithdrawToWalletRequest()
            .setWithdrawlKey("Poloniex-EOS")
            .setCurrency("EOS")
            .setQuantity("0.5"));

        assertNotNull(result);
        assertNotNull(result.getWithdrawId());
    }

    @Test(expected = InvalidWithdrawException.class)
    @Override
    public void testWrongWithdrawAPIKey()
        throws UnsupportedCXException, CTTKException, IOException
    {
        String wrongKey = "-- N/A --";

        client.withdraw(new WithdrawToWalletRequest()
            .setWithdrawlKey(wrongKey)
            .setCurrency("ETH")
            .setQuantity("0.5"));
    }

    @Test(expected = InvalidWithdrawException.class)
    public void testWithdrawWhenInvalidCurrency()
        throws UnsupportedCurrencyPairException, CTTKException
    {
        String nonExistsCurrency = "$COIN$";

        client.withdraw(new WithdrawToWalletRequest()
            .setWithdrawlKey("Poloniex-EOS")
            .setCurrency(nonExistsCurrency)
            .setQuantity("0.5"));
    }

    @Test(expected = InvalidWithdrawException.class)
    public void testWithdrawWhenInvalidQauntity()
        throws UnsupportedCurrencyPairException, CTTKException
    {
        String tooSmallQuantity = "0.0000000000000001";

        client.withdraw(new WithdrawToWalletRequest()
            .setWithdrawlKey("Poloniex-EOS")
            .setCurrency("EOS")
            .setQuantity(tooSmallQuantity));
    }
}
