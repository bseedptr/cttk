package cttk.impl.poloniex.response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;
import cttk.impl.poloniex.PoloniexUtils;

public class PoloniexAllTradesResponse
    extends HashMap<String, List<PoloniexTrade>>
{
    public UserTrades toUserTrades() {
        return new UserTrades()
            .setTrades(getUserTradeList());
    }

    List<UserTrade> getUserTradeList() {
        final List<UserTrade> list = new ArrayList<>();
        this.forEach((k, v) -> {
            if (v != null && !v.isEmpty()) {
                final CurrencyPair cp = PoloniexUtils.parseMarketSymbol(k);
                list.addAll(v.stream().map(i -> i.toUserTrade(cp)).collect(Collectors.toList()));
            }
        });
        return list;
    }
}
