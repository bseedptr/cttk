package cttk.impl.bitfinex.stream;

import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.UserData;
import cttk.auth.Credential;
import cttk.exception.CTTKException;
import cttk.impl.bitfinex.AbstractBitfinexClient;
import okhttp3.OkHttpClient;
import okhttp3.Request.Builder;
import okhttp3.WebSocket;

public class BitfinexUserDataWebsocketStream
    extends AbstractBitfinexClient
    implements CXStream<UserData>
{
    private static final String WSS_API_BITFINEX_WS = "wss://api.bitfinex.com/ws/2/";
    private final Credential credential;
    private final CXStreamListener<UserData> listener;

    protected WebSocket websocket;

    public BitfinexUserDataWebsocketStream(
        final Credential credential,
        final CXStreamListener<UserData> listener)
        throws CTTKException
    {
        this.credential = credential;
        this.listener = listener;

        open();
    }

    private String getUrl() {
        return WSS_API_BITFINEX_WS;
    }

    @Override
    public CXStreamListener<UserData> getListener() {
        return listener;
    }

    @Override
    public CXStream<UserData> open()
        throws CTTKException
    {
        openWebsocket();
        return this;
    }

    @Override
    public void close()
        throws Exception
    {
        closeWebsocket();
    }

    private void openWebsocket()
        throws CTTKException
    {
        final OkHttpClient okHttpClient = createOkHttpClient(10);

        websocket = okHttpClient.newWebSocket(
            new Builder().url(getUrl()).build(),
            new BitfinexUserDataWebsocketListener(this, listener).setCredential(credential));

        okHttpClient.dispatcher().executorService().shutdown();
    }

    private void closeWebsocket()
        throws CTTKException
    {
        if (websocket != null) websocket.close(NORMAL_CLOSURE_STATUS, null);
    }
}
