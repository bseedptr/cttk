package cttk.impl.stream.eachmarket;

import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.UserData;
import cttk.UserDataStreamOpener;
import cttk.exception.CTTKException;
import cttk.request.OpenUserDataStreamRequest;

public class EachMarketPollingUserDataStreamOpener
    implements UserDataStreamOpener
{
    protected final PublicCXClient publicClient;
    protected final PrivateCXClient privateCXClient;

    public EachMarketPollingUserDataStreamOpener(PublicCXClient publicClient, PrivateCXClient privateCXClient) {
        super();
        this.publicClient = publicClient;
        this.privateCXClient = privateCXClient;
    }

    @Override
    public CXStream<UserData> openUserDataStream(OpenUserDataStreamRequest request, CXStreamListener<UserData> listener)
        throws CTTKException
    {
        if (request != null) {
            if (request.getCoolTime() == null) {
                request.setCoolTime(CoolTime.createDefault(publicClient.getCxId()));
            }
            if (request.getRateLimit() == null) {
                request.setRateLimit(RateLimit.createDefault(publicClient.getCxId()));
            }
        }
        return createStream(request, listener);
    }

    protected CXStream<UserData> createStream(OpenUserDataStreamRequest request, CXStreamListener<UserData> listener)
        throws CTTKException
    {
        return new EachMarketPollingUserDataStream(publicClient, privateCXClient, request, listener);
    }
}
