package cttk.impl.feeprovider;

import cttk.CurrencyPair;
import cttk.dto.TradingFee;
import cttk.exception.CTTKException;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class DefaultTradingFeeProvider
    extends AbstractTradingFeeProvider<DefaultTradingFeeProvider>
{
    @Override
    public TradingFee getFee(CurrencyPair currencyPair)
        throws CTTKException
    {
        return fee;
    }
}
