package cttk.impl;

import static cttk.impl.util.Objects.nvl;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cttk.CXIds;
import cttk.CurrencyPair;
import cttk.auth.CXCredentials;
import cttk.auth.CXCredentialsProvider;
import cttk.auth.Credential;
import cttk.exception.UnsupportedCXException;
import cttk.test.TestConfig;

public abstract class AbstractCXClientTest {
    protected final Logger logger = LoggerFactory.getLogger(getClass());
    protected TestConfig testConfig;
    protected String raiseExceptionMsg = "Failed to raise proper Exception";
    protected int DEFAULT_API_LIMIT_RECOVERY_TIME_IN_SECOND = 5;

    public abstract String getCxId();

    public final CurrencyPair getValidCurrencyPair() {
        if (testConfig == null) readTestConfig();
        return testConfig == null ? null : testConfig.getValidCurrencyPair();
    }

    public final CurrencyPair getInvalidCurrencyPair() {
        if (testConfig == null) readTestConfig();
        return testConfig == null ? null : testConfig.getInvalidCurrencyPair();
    }

    public final String getTransferTestCurrency() {
        if (testConfig == null) readTestConfig();
        return testConfig == null ? null : testConfig.getTransferTestCurrency();
    }

    public final CurrencyPair getCurrencyPair() {
        return getValidCurrencyPair();
    }

    @Before
    public final void readTestConfig() {
        try {
            testConfig = TestConfig.readFromClasspathYaml("config/" + getCxId().toLowerCase() + ".yml");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected long getThreadSleepMillis() {
        return 0l;
    }

    protected void threadSleep(final long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            logger.error("Unexpected error while testing", e);
        }
    }

    protected void threadSleep() {
        final long threadSleepMillis = getThreadSleepMillis();
        if (threadSleepMillis > 0) threadSleep(threadSleepMillis);
    }

    protected CXCredentials getInvalidCredential(final String cxId)
        throws UnsupportedCXException, IOException
    {
        CXCredentials realCredentials = new CXCredentialsProvider().get(cxId);
        CXCredentials testCredentials = new CXCredentialsProvider().get(cxId);

        Credential[] credentialList = new Credential[] {
            realCredentials.getDataKey(),
            realCredentials.getTradeKey(),
            realCredentials.getWithdrawKey()
        };
        List<Credential> testCredentialList = new ArrayList<>(3);
        String override = null;
        if (cxId == CXIds.COINONE) {
            override = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
        }
        for (int i = 0; i < credentialList.length; i++) {
            Credential testKey = new Credential();

            if (credentialList[i] == null) {
                testCredentialList.add(testKey);
                continue;
            }

            if (credentialList[i].getUserName() != null) {
                int testUserNameLen = credentialList[i].getUserName().length();
                String testUserName = IntStream.range(0, testUserNameLen).mapToObj(j -> "a").collect(Collectors.joining(""));
                testKey.setUserName(testUserName);
            }
            if (credentialList[i].getPassword() != null) {
                int testPasswordLen = credentialList[i].getPassword().length();
                String testPassword = IntStream.range(0, testPasswordLen).mapToObj(j -> "a").collect(Collectors.joining(""));
                testKey.setPassword(testPassword);
            }
            if (credentialList[i].getAccessKey() != null) {
                int testAccessKeyLen = credentialList[i].getAccessKey().length();
                String testAccessKey = IntStream.range(0, testAccessKeyLen).mapToObj(j -> "a").collect(Collectors.joining(""));
                if (override != null) {
                    testAccessKey = override;
                }
                testKey.setAccessKey(testAccessKey);
            }
            if (credentialList[i].getSecretKey() != null) {
                int testSecretKeyLen = credentialList[i].getSecretKey().length();
                String testSecretKey = IntStream.range(0, testSecretKeyLen).mapToObj(j -> "a").collect(Collectors.joining(""));
                if (override != null) {
                    testSecretKey = override;
                }
                testKey.setSecretKey(testSecretKey);
            }
            testCredentialList.add(testKey);
        }

        testCredentials.setDataKey(testCredentialList.get(0));
        testCredentials.setTradeKey(testCredentialList.get(1));
        testCredentials.setWithdrawKey(testCredentialList.get(2));

        return testCredentials;
    }

    protected BigDecimal getBuyTestQuantity() {
        if (testConfig == null) readTestConfig();
        return testConfig == null ? null : nvl(testConfig.getBuyTestQuantity(), new BigDecimal("0.2"));
    }

    protected BigDecimal getSellTestQuantity() {
        if (testConfig == null) readTestConfig();
        return testConfig == null ? null : nvl(testConfig.getSellTestQuantity(), new BigDecimal("0.2"));
    }
}
