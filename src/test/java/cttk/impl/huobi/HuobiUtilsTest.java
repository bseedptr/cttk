package cttk.impl.huobi;

import cttk.CurrencyPair.SimpleCurrencyPair;
import cttk.dto.UserOrder;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.UnsupportedCurrencyPairException;
import org.junit.Assert;
import org.junit.Test;

import static cttk.OrderSide.BUY;
import static cttk.OrderSide.SELL;

public class HuobiUtilsTest {
    @Test
    public void testGuessFeeCurrencyIfOderSideIsSELLThenQuoteCurrency() {
        SimpleCurrencyPair pair = new SimpleCurrencyPair("EOS", "ETH");
        String feeCurrency = HuobiUtils.guessFeeCurrency(SELL, pair);

        Assert.assertEquals("Should be 'ETH' if order side is SELL.", pair.getQuoteCurrency(), feeCurrency);
    }

    @Test
    public void testGuessFeeCurrencyIfOderSideIsBUYThenBaseCurrency() {
        SimpleCurrencyPair pair = new SimpleCurrencyPair("EOS", "ETH");
        String feeCurrency = HuobiUtils.guessFeeCurrency(BUY, pair);

        Assert.assertEquals("Should be 'EOS' if order side is BUY.", pair.getBaseCurrency(), feeCurrency);
    }

    @Test(expected = CXAPIRequestException.class)
    public void testException0()
        throws CTTKException
    {
        String text = "{\n"
            + "    \"ts\":1490758171271,\n"
            + "    \"status\":\"error\",\n"
            + "    \"err-code\":\"invalid-parameter\",\n"
            + "    \"err-msg\":\"invalid period\"\n"
            + "}";
        HuobiUtils.checkErrorByResponseMessage(null, text, null);
    }

    @Test(expected = CXAPIRequestException.class)
    public void testException1()
        throws CTTKException
    {

        String text = "{\n"
            + "    \"ts\":1490758221221,\n"
            + "    \"status\":\"error\",\n"
            + "    \"err-code\":\"bad-request\",\n"
            + "    \"err-msg\":\"invalid size, valid range: [1,2000]\"\n"
            + "}";
        HuobiUtils.checkErrorByResponseMessage(null, text, null);
    }

    @Test(expected = UnsupportedCurrencyPairException.class)
    public void testException2()
        throws CTTKException
    {
        UserOrder c = new UserOrder();
        c.setBaseCurrency("c");
        c.setQuoteCurrency("q");

        String text = "{\n"
            + "    \"ts\":1490758171271,\n"
            + "    \"status\":\"error\",\n"
            + "    \"err-code\":\"invalid-parameter\",\n"
            + "    \"err-msg\":\"invalid symbol\"\n"
            + "}";
        HuobiUtils.checkErrorByResponseMessage(c, text, null);
    }

    @Test(expected = CXAPIRequestException.class)
    public void testException3()
        throws CTTKException
    {
        String text = "{\n"
            + "    \"code\":13404,\n"
            + "    \"data\":null,\n"
            + "    \"message\":null,\n"
            + "    \"success\":false\n"
            + "}";
        HuobiUtils.checkErrorByResponseMessage(null, text, null);
    }

    @Test
    public void testException4()
        throws CTTKException
    {
        String text = "{\n"
            + "    \"code\":200,\n"
            + "    \"data\":null,\n"
            + "    \"message\":null,\n"
            + "    \"success\":false\n"
            + "}";
        HuobiUtils.checkErrorByResponseMessage(null, text, null);
    }
}