package cttk;

import static cttk.util.StringUtils.toUpper;

public enum TransferType {
    WITHDRAWAL, DEPOSIT, // basic transfer types
    BANK_TO_EXCHANGE, EXCHANGE_TO_BANK, // hitbtc specific
    TRADE, MARGIN; // kraken specific

    public static TransferType of(String type) {
        if (type == null) return null;
        switch (toUpper(type)) {
            case "DEPOSIT": // bitfinex, hitbtc, korbit, kraken, upbit
            case "RECEIVE": // coinone
            case "DEPOSITS": // poloniex
            case "PAYIN":// hitbtc
                return TransferType.DEPOSIT;
            case "WITHDRAWAL": // bitfinex, korbit, kraken
            case "SEND": // coinone
            case "WITHDRAW": // hitbtc, upbit
            case "PAYOUT": // hitbtc
            case "WITHDRAWALS": //poloniex
                return TransferType.WITHDRAWAL;
            case "BANKTOEXCHANGE":
                return TransferType.BANK_TO_EXCHANGE;
            case "EXCHANGETOBANK":
                return TransferType.EXCHANGE_TO_BANK;
            // kraken
            case "TRADE":
                return TransferType.TRADE;
            case "MARGIN":
                return TransferType.MARGIN;
            default:
                return null;
        }
    }
}