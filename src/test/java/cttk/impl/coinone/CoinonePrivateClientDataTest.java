package cttk.impl.coinone;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import cttk.CXIds;
import cttk.dto.Balances;
import cttk.dto.BankAccount;
import cttk.dto.UserAccount;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.AbstractPrivateCXClientDataTest;
import cttk.request.GetBankAccountRequest;
import cttk.request.GetUserAccountRequest;

public class CoinonePrivateClientDataTest
    extends AbstractPrivateCXClientDataTest
{
    @AfterClass
    public static void cleanup() {
        cleanup(CoinonePrivateClientDataTest.class);
    }

    @Override
    public String getCxId() {
        return CXIds.COINONE;
    }

    @Override
    protected long getThreadSleepMillis() {
        return 500l;
    }

    @Test
    @Override
    public void testGetAllBalances()
        throws CTTKException
    {
        super.testGetAllBalances();
        final Balances balances = client.getAllBalances();

        Assert.assertNotNull(balances);
    }

    @Test
    @Override
    public void testGetUserAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        UserAccount userAccount = client.getUserAccount(new GetUserAccountRequest());

        Assert.assertNotNull(userAccount);
    }

    @Test
    @Override
    public void testGetBankAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        BankAccount bankAccount = client.getBankAccount(new GetBankAccountRequest());

        Assert.assertNotNull(bankAccount);
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetWallet()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetWallet();
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetAllOpenUserOrders()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetAllOpenUserOrders();
    }

    @Test(expected = CXAPIRequestException.class)
    // Coinone throws 107 parameter error for Wrong currency and it is not enough to distinguish what the error was (CTTK-560)
    public void testGetOpenUserOrdersWrongCurrency()
        throws CTTKException
    {

        super.testGetOpenUserOrdersWrongCurrency();
    }

    @Test(expected = CXAPIRequestException.class)
    // Coinone throws 107 parameter error for Wrong currency and it is not enough to distinguish what the error was (CTTK-560)
    public void testGetUserOrderHistoryWrongCurrency()
        throws CTTKException
    {
        super.testGetUserOrderHistoryWrongCurrency();
    }

    @Test(expected = CXAPIRequestException.class)
    // Coinone throws 107 parameter error for Wrong currency and it is not enough to distinguish what the error was (CTTK-560)
    public void testGetUserTradeWrongCurrency()
        throws CTTKException
    {
        super.testGetUserTradeWrongCurrency();
    }

}
