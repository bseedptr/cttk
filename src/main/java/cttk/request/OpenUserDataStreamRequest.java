package cttk.request;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import cttk.CXStreamConfig;
import cttk.CurrencyPair;
import cttk.impl.stream.eachmarket.CoolTime;
import cttk.impl.stream.eachmarket.RateLimit;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class OpenUserDataStreamRequest
    extends AbstractCXStreamRequest<OpenUserDataStreamRequest>
    implements CXStreamConfig
{
    public static final long DEFAULT_INTERVAL_IN_MILLI = 5000l;
    public static final int DEFAULT_MAX_POLL_RETRY_COUNT = 3;
    public static final long DEFAULT_BALANCE_CHECK_INTERVAL_IN_MILLI = 5 * 60 * 1000l;

    protected List<CurrencyPair> currencyPairs;
    protected long balanceCheckIntervalInMilli = DEFAULT_BALANCE_CHECK_INTERVAL_IN_MILLI;
    protected boolean checkBalanceAutomatically = true;
    protected RateLimit rateLimit;
    protected CoolTime coolTime;

    public OpenUserDataStreamRequest() {
        super();
        super.intervalInMilli = DEFAULT_INTERVAL_IN_MILLI;
        super.maxPollRetryCount = DEFAULT_MAX_POLL_RETRY_COUNT;
    }

    public OpenUserDataStreamRequest add(CurrencyPair pair) {
        if (this.currencyPairs == null) this.currencyPairs = new ArrayList<>();
        this.currencyPairs.add(pair);
        return this;
    }

    public OpenUserDataStreamRequest addAll(Collection<CurrencyPair> pairs) {
        if (this.currencyPairs == null) this.currencyPairs = new ArrayList<>();
        this.currencyPairs.addAll(pairs);
        return this;
    }
}
