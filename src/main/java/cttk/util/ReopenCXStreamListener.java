package cttk.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.impl.stream.AbstractWebsocketStream;

/**
 * This listener takes a listener and delete all the response to the listener.
 * If there is a failure (onFailure) or abnormal disconnection (onClosing), 
 * this listener tries to connect the server again.
 * 
 * @author	dj.lee
 * @since	2018-04-27
 * @param <T> the message type
 */
public class ReopenCXStreamListener<T>
    implements CXStreamListener<T>
{
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    private final CXStreamListener<T> listener;
    private final ReopenPolicy reopenPolicy;

    private int retryCount = 0;
    private long backoffInMilli = 500;

    public ReopenCXStreamListener(
        final CXStreamListener<T> listener,
        final ReopenPolicy reopenPolicy)
    {
        this.listener = listener;
        this.reopenPolicy = ReopenPolicy.createDefaultIfNull(reopenPolicy);
        this.backoffInMilli = this.reopenPolicy.getBackoffInMilli();
    }

    public ReopenCXStreamListener(
        final CXStreamListener<T> listener)
    {
        this(listener, null);
    }

    @Override
    public void onOpen(CXStream<T> stream) {
        resetRetries();
        listener.onOpen(stream);
    }

    @Override
    public void onMessage(T message) {
        listener.onMessage(message);
    }

    @Override
    public void onClosing(CXStream<T> stream, int code, String reason) {
        listener.onClosing(stream, code, reason);
    }

    @Override
    public void onClosed(CXStream<T> stream, int code, String reason) {
        if (reopenPolicy.isReopenOnClosed()
            && code != AbstractWebsocketStream.NORMAL_CLOSURE_STATUS
            && retryCount < reopenPolicy.getMaxRetries())
        {
            try {
                Thread.sleep(backoffInMilli);

                retryCount++;
                backoffInMilli = backoffInMilli * 2;

                stream.open();
            } catch (Exception e) {
                logger.error("Fail to reopen stream ({} tried)", retryCount, e);
            }
        } else {
            resetRetries();
            listener.onClosed(stream, code, reason);
        }
    }

    @Override
    public void onFailure(CXStream<T> stream, Throwable t) {
        if (retryCount < reopenPolicy.getMaxRetries()) {
            try {
                Thread.sleep(backoffInMilli);

                retryCount++;
                backoffInMilli = backoffInMilli * 2;

                stream.open();
            } catch (Exception e) {
                logger.error("Fail to reopen stream ({} tried)", retryCount, e);
            }
        } else {
            resetRetries();
            listener.onFailure(stream, t);
        }
    }

    private void resetRetries() {
        retryCount = 0;
        backoffInMilli = reopenPolicy.getBackoffInMilli();
    }

    public static <T> ReopenCXStreamListener<T> create(CXStreamListener<T> listener) {
        return new ReopenCXStreamListener<>(listener);
    }

    public static <T> ReopenCXStreamListener<T> create(CXStreamListener<T> listener, ReopenPolicy reopenPolicy) {
        return new ReopenCXStreamListener<>(listener, reopenPolicy);
    }
}
