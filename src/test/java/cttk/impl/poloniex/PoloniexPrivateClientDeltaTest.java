package cttk.impl.poloniex;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.time.ZonedDateTime;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import cttk.auth.Credential;
import cttk.dto.Transfers;
import cttk.dto.UserTrades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.AbstractPrivateClientDeltaTest;
import cttk.request.GetTransfersDeltaRequest;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserOrderHistoryDeltaRequest;
import cttk.request.GetUserTradesDeltaRequest;
import cttk.request.GetUserTradesRequest;
import lombok.extern.slf4j.Slf4j;

@RunWith(MockitoJUnitRunner.Silent.class)
@Slf4j
public class PoloniexPrivateClientDeltaTest
    extends AbstractPrivateClientDeltaTest
{
    PoloniexPrivateClient client;
    GetUserTradesDeltaRequest tradesRequest;
    GetTransfersDeltaRequest transferRequest;

    @Mock
    UserTrades trades1, trades2;
    @Mock
    Transfers transfers1, transfers2;

    @Before
    public void setup() {
        client = spy(new PoloniexPrivateClient(mock(Credential.class)));
        tradesRequest = new GetUserTradesDeltaRequest().setStartDateTime(ZonedDateTime.now().minusYears(2));
        transferRequest = new GetTransfersDeltaRequest().setStartDateTime(ZonedDateTime.now().minusYears(2));
    }

    @Test(expected = UnsupportedMethodException.class)
    public void testGetUserOrderHistoryDelta()
        throws UnsupportedCurrencyException, CTTKException
    {
        client.getUserOrderHistory(new GetUserOrderHistoryDeltaRequest());
    }

    @Test
    public void testGetUserTradesDeltaWhenNoMoreData()
        throws Exception
    {
        when(trades1.getList()).thenReturn(makeUserTradeFixtures("1", "2", "3"));
        when(trades1.getOldest()).thenReturn(makeUserTradeFixture("1"));
        when(trades1.getLatest()).thenReturn(makeUserTradeFixture("3"));
        when(trades1.isEmpty()).thenReturn(false);
        when(trades2.getList()).thenReturn(makeUserTradeFixtures());
        when(trades2.isEmpty()).thenReturn(true);
        doReturn(trades1, trades2).when(client).getUserTrades(any(GetUserTradesRequest.class));

        UserTrades result = client.getUserTrades(tradesRequest);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat(ids, containsInAnyOrder("1", "2", "3"));
    }

    @Test
    public void testGetUserTradesDeltaWhenDuplication()
        throws Exception
    {
        when(trades1.getList()).thenReturn(makeUserTradeFixtures("1", "2", "3"));
        when(trades1.getOldest()).thenReturn(makeUserTradeFixture("1"));
        when(trades1.getLatest()).thenReturn(makeUserTradeFixture("3"));
        when(trades2.getList()).thenReturn(makeUserTradeFixtures("3", "4", "5"));
        when(trades2.getOldest()).thenReturn(makeUserTradeFixture("3"));
        when(trades2.getLatest()).thenReturn(makeUserTradeFixture("5"));
        doReturn(trades1, trades2).when(client).getUserTrades(any(GetUserTradesRequest.class));

        UserTrades result = client.getUserTrades(tradesRequest);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat("Id should be unique.", ids, containsInAnyOrder("1", "2", "3", "4", "5"));
    }

    @Test
    public void testGetTransfersDeltaWhenNoMoreData()
        throws Exception
    {
        when(transfers1.getList()).thenReturn(makeTransferFixtures("1", "2", "3"));
        when(transfers1.isEmpty()).thenReturn(false);
        when(transfers1.getLatest()).thenReturn(makeTransferFixture("3"));
        when(transfers2.getList()).thenReturn(makeTransferFixtures());
        when(transfers2.isEmpty()).thenReturn(true);
        doReturn(transfers1, transfers2).when(client).getTransfers(any(GetTransfersRequest.class));

        Transfers result = client.getTransfers(transferRequest);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat(ids, containsInAnyOrder("1", "2", "3"));
    }

    @Test
    public void testGetTransfersDeltaWhenDuplication()
        throws Exception
    {
        when(transfers1.getLatest()).thenReturn(makeTransferFixture("3"));
        when(transfers1.getList()).thenReturn(makeTransferFixtures("1", "2", "3"));
        when(transfers2.getLatest()).thenReturn(makeTransferFixture("5"));
        when(transfers2.getList()).thenReturn(makeTransferFixtures("3", "4", "5"));
        doReturn(transfers1, transfers2).when(client).getTransfers(any(GetTransfersRequest.class));

        Transfers result = client.getTransfers(transferRequest);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat("Id should be unique.", ids, containsInAnyOrder("1", "2", "3", "4", "5"));
    }
}
