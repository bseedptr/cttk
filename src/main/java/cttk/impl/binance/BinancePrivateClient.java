package cttk.impl.binance;

import static cttk.TransferType.DEPOSIT;
import static cttk.TransferType.WITHDRAWAL;
import static cttk.dto.UserOrders.Category.ALL;
import static cttk.dto.UserOrders.Category.OPEN;
import static cttk.impl.binance.BinanceUtils.getMarketSymbol;
import static cttk.impl.binance.BinanceUtils.toBinanceCurrencySymbol;
import static cttk.impl.util.Objects.nvl;
import static cttk.util.DeltaUtils.sleep;
import static cttk.util.StringUtils.toUpper;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import cttk.CXClientFactory;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.TradingFeeProvider;
import cttk.auth.Credential;
import cttk.dto.Balances;
import cttk.dto.BankAccount;
import cttk.dto.MarketInfos;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.dto.UserAccount;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrades;
import cttk.dto.Wallet;
import cttk.dto.Wallets;
import cttk.dto.WithdrawResult;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXServerException;
import cttk.exception.InsufficientBalanceException;
import cttk.exception.InvalidAmountMaxException;
import cttk.exception.InvalidAmountRangeException;
import cttk.exception.InvalidOrderException;
import cttk.exception.InvalidPriceIncrementPrecisionException;
import cttk.exception.InvalidPriceMaxException;
import cttk.exception.InvalidPriceRangeException;
import cttk.exception.InvalidQuantityIncrementPrecisionException;
import cttk.exception.InvalidTotalMinException;
import cttk.exception.InvalidTotalRangeException;
import cttk.exception.MaxNumOrderException;
import cttk.exception.OrderNotFoundException;
import cttk.exception.UnsupportedCurrencyException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.binance.response.BinanceAccountResponse;
import cttk.impl.binance.response.BinanceDepositHistoryResponse;
import cttk.impl.binance.response.BinanceOrderResponse;
import cttk.impl.binance.response.BinanceResultResponse;
import cttk.impl.binance.response.BinanceUserTradeResponse;
import cttk.impl.binance.response.BinanceWalletResponse;
import cttk.impl.binance.response.BinanceWithdrawHistoryResponse;
import cttk.impl.binance.response.BinanceWithdrawResponse;
import cttk.impl.feeprovider.DefaultTradingFeeProvider;
import cttk.impl.util.CryptoUtils;
import cttk.impl.util.HttpHeaders;
import cttk.impl.util.HttpMethod;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.MathUtils;
import cttk.impl.util.MediaType;
import cttk.impl.util.ParamsBuilder;
import cttk.impl.util.ThrowableFunction;
import cttk.impl.util.UriUtils;
import cttk.request.CancelOrderRequest;
import cttk.request.CreateMarketPriceOrderRequest;
import cttk.request.CreateOrderRequest;
import cttk.request.GetBankAccountRequest;
import cttk.request.GetOpenUserOrdersRequest;
import cttk.request.GetTransfersDeltaRequest;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserAccountRequest;
import cttk.request.GetUserOrderHistoryDeltaRequest;
import cttk.request.GetUserOrderHistoryRequest;
import cttk.request.GetUserOrderRequest;
import cttk.request.GetUserTradesDeltaRequest;
import cttk.request.GetUserTradesRequest;
import cttk.request.GetWalletRequest;
import cttk.request.WithdrawToBankRequest;
import cttk.request.WithdrawToWalletRequest;
import cttk.util.CTTKExceptionUtils;
import cttk.util.DeltaUtils;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.Response;

public class BinancePrivateClient
    extends AbstractBinanceClient
    implements PrivateCXClient
{
    private final String PRIVATE_URL = "https://api.binance.com/api/v3";
    private final String TRADE_URL = "https://api.binance.com/api/v3";
    private final String WITHDRAW_URL = "https://api.binance.com/wapi/v3";

    private static final String HEADER_ACCESS_KEY = "X-MBX-APIKEY";

    private final Credential credential;

    private PublicCXClient pubClient = null;
    private MarketInfos marketInfos = null;

    public BinancePrivateClient(Credential credential) {
        super();
        this.credential = credential;

        // public client for getting getMarketInfo
        try {
            this.pubClient = CXClientFactory.createPublicCXClient(this.getCxId());
            this.marketInfos = pubClient.getMarketInfos();
        } catch (CTTKException e) {
            logger.error("Failed to getMarketInfos while creating BinancePrivateClient. UnsupporedCurrencyException for private data could not be working correctly");
        }

    }

    @Override
    public Balances getAllBalances()
        throws CTTKException
    {
        final HttpResponse response = request(HttpMethod.GET, PRIVATE_URL + "/account", null);

        checkResponseError(null, response);

        return convert(response, BinanceAccountResponse.class, (r -> r.getBalances()));
    }

    @Override
    public UserOrder getUserOrder(GetUserOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = request(HttpMethod.GET, PRIVATE_URL + "/order",
            ParamsBuilder.create()
                .put("symbol", getMarketSymbol(request))
                .put("orderId", request.getOid())
                .build());
        try {
            checkResponseError(request, response);
        } catch (OrderNotFoundException e) {
            return null;
        }
        return convert(response, BinanceOrderResponse.class, (r -> r.getUserOrder()));
    }

    @Override
    public UserOrders getOpenUserOrders(GetOpenUserOrdersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = request(HttpMethod.GET, PRIVATE_URL + "/openOrders",
            ParamsBuilder.buildIfNotNull("symbol", getMarketSymbol(request)));

        checkResponseError(request, response);

        try {
            return convertList(response, BinanceOrderResponse.class,
                l -> new UserOrders().setOrders(
                    l.stream().map(r -> r.getUserOrder()).filter(r -> r != null).collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getAllOpenUserOrders", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public UserOrders getAllOpenUserOrders(boolean withTrades)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = request(HttpMethod.GET, PRIVATE_URL + "/openOrders");

        checkResponseError(null, response);

        try {
            return convertList(response, BinanceOrderResponse.class,
                l -> new UserOrders().setOrders(
                    l.stream().map(r -> r.getUserOrder()).filter(r -> r != null).collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getAllOpenUserOrders", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());

        }
    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = request(HttpMethod.GET, PRIVATE_URL + "/allOrders",
            ParamsBuilder.create()
                .put("symbol", getMarketSymbol(request))
                .putIfNotNull("orderId", request.getFromId())
                .putIfNotNull("limit", request.getCount())
                // TODO use recvWindow
                //.putIfNotNull("recvWindow", request.getOffset())
                .build());
        try {
            checkResponseError(request, response);
        } catch (OrderNotFoundException e) {
            return new UserOrders().setCategory(OPEN);
        }

        try {
            final UserOrders userOrders = convertList(response, BinanceOrderResponse.class,
                l -> new UserOrders().setOrders(
                    l.stream().map(r -> r.getUserOrder()).filter(r -> r != null).collect(Collectors.toList())));
            return userOrders.setCategory(ALL);
        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getUserOrderHistory", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP i-a: id-asc,>>>(break)
        final GetUserOrderHistoryRequest partialRequest = new GetUserOrderHistoryRequest()
            .setCurrencyPair(request)
            .setCount(1000);
        // Sorting in asc order implicitly

        if (request.hasFromId()) {
            final UserOrders userOrders = new UserOrders();
            String fromId = request.getFromId();
            while (true) {
                partialRequest.setFromId(fromId);
                final UserOrders tempUserOrders = getUserOrderHistory(partialRequest);

                if (tempUserOrders.isEmpty()) break;
                userOrders.addAll(tempUserOrders);
                fromId = MathUtils.addOneToLongValueString(tempUserOrders.getLatest().getOid());

                if (!sleep(request.getSleepTimeMillis())) break;
            }

            return userOrders.setCategory(ALL);
        } else {
            return getUserOrderHistory(partialRequest);
        }
    }

    @Override
    public UserOrder createOrder(CreateOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = request(HttpMethod.POST, TRADE_URL + "/order",
            ParamsBuilder.create()
                .put("symbol", getMarketSymbol(request))
                .put("price", request.getPrice().toPlainString())
                .put("quantity", request.getQuantity().toPlainString())
                .put("side", BinanceUtils.toString(request.getSide()))
                .put("type", "LIMIT")
                .put("timeInForce", "GTC")
                .build());

        checkOrderResponseError(request, response);
        checkResponseError(request, response);

        return fillResponseByRequest(request, convert(response, BinanceOrderResponse.class, r -> r.getUserOrder()));
    }

    @Override
    public UserOrder createMarketPriceOrder(CreateMarketPriceOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = request(HttpMethod.POST, TRADE_URL + "/order",
            ParamsBuilder.create()
                .put("symbol", getMarketSymbol(request))
                .put("quantity", String.valueOf(request.getQuantity()))
                .put("side", BinanceUtils.toString(request.getSide()))
                .put("type", "MARKET")
                .build());

        checkResponseError(request, response);

        return convert(response, BinanceOrderResponse.class, (r -> r.getUserOrder()));
    }

    @Override
    public void cancelOrder(CancelOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = request(HttpMethod.DELETE, TRADE_URL + "/order",
            ParamsBuilder.create()
                .put("symbol", getMarketSymbol(request))
                .put("orderId", request.getOrderId())
                .build());

        checkResponseError(request, response);
    }

    @Override
    public Wallet getWallet(GetWalletRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = request(HttpMethod.GET, WITHDRAW_URL + "/depositAddress.html",
            ParamsBuilder.build("asset", toBinanceCurrencySymbol(request.getCurrency())));

        checkResponseError(null, response);

        return convertForWithdraw(response, BinanceWalletResponse.class, (r -> r.getWallet()));
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesRequest request)
        throws UnsupportedCurrencyPairException, CTTKException
    {
        final HttpResponse response = request(HttpMethod.GET, PRIVATE_URL + "/myTrades",
            ParamsBuilder.create()
                .putIfNotNull("symbol", getMarketSymbol(request))
                // TODO support recvWindow
                //.putIfNotNull("recvWindow", request.getPageNo())
                .putIfNotNull("limit", request.getCount())
                .putIfNotNull("fromId", request.getFromId())
                .build());

        checkResponseError(request, response);

        try {
            return convertList(response, BinanceUserTradeResponse.class,
                l -> new UserTrades().setTrades(l.stream().map(r -> r.getUserTrade(request)).collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getUserTrades while converting results", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP i-a: id-asc,>>>(break)
        final GetUserTradesRequest partialRequest = new GetUserTradesRequest()
            .setCurrencyPair(request)
            .setCount(1000);
        // Sorting in asc order implicitly

        if (request.hasFromId()) {
            final UserTrades userTrades = new UserTrades();
            String fromId = request.getFromId();
            while (true) {
                partialRequest.setFromId(fromId);
                final UserTrades tempUserTrades = getUserTrades(partialRequest);

                if (tempUserTrades.isEmpty()) break;
                userTrades.addAll(tempUserTrades);
                fromId = MathUtils.addOneToLongValueString(tempUserTrades.getLatest().getTid());

                if (!sleep(request.getSleepTimeMillis())) break;
            }

            return userTrades;
        } else {
            return getUserTrades(partialRequest);
        }
    }

    protected Transfers checkValidCurrency(String requestCurrency, Transfers transfers)
        throws CTTKException
    {
        for (Transfer trans : transfers.getList()) {
            if (trans.getCurrency().equals(requestCurrency))
                return transfers;
        }

        // We are manually checking correct currency request by checking with marketInfos
        if (marketInfos != null && !marketInfos.getAllCurrencies().contains(requestCurrency)) {
            throw new UnsupportedCurrencyException()
                .setCurrency(requestCurrency)
                .setCxId(this.getCxId());
        }

        // We don't have the currency in the list,
        // but requested currency is not invalid currency either.
        return transfers;
    }

    @Override
    public Transfers getTransfers(GetTransfersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        if (DEPOSIT.equals(request.getType())) {
            Transfers transfers = getDepositHistory(request);
            return checkValidCurrency(request.getCurrency(), transfers);
        } else if (WITHDRAWAL.equals(request.getType())) {
            Transfers transfers = getWithdrawHistory(request);
            return checkValidCurrency(request.getCurrency(), transfers);
        } else {
            final Transfers transfers = new Transfers();
            transfers.addAll(getDepositHistory(request));
            transfers.addAll(getWithdrawHistory(request));
            return checkValidCurrency(request.getCurrency(), transfers);
        }
    }

    @Override
    public Transfers getTransfers(GetTransfersDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP t-a: time-asc,>>>(break)
        final GetTransfersRequest partialRequest = new GetTransfersRequest()
            .setCurrency(request.getCurrency())
            .setStartDateTime(request.getStartDateTime())
            .setEndDateTime(nvl(request.getEndDateTime(), ZonedDateTime.now()))
            .setCount(1000);
        // Sorting in asc order implicitly

        if (request.hasStartDateTime()) {
            final ZonedDateTime startDateTime = partialRequest.getStartDateTime();
            final Map<String, Transfer> depositTransferMap = new LinkedHashMap<>();
            Transfer prevLast = null;
            while (true) {
                final Transfers transfers = getDepositHistory(partialRequest);
                final Transfer last = transfers.getLatest();

                if (transfers.isEmpty() || Transfer.equalsById(prevLast, last)) break;
                depositTransferMap.putAll(DeltaUtils.convertToMap(transfers));

                partialRequest.setStartDateTime(transfers.getMaxDateTime());
                prevLast = last;

                if (!sleep(request.getSleepTimeMillis())) break;
            }

            partialRequest.setStartDateTime(startDateTime);
            final Map<String, Transfer> withdrawTransferMap = new LinkedHashMap<>();
            prevLast = null;
            while (true) {
                final Transfers transfers = getWithdrawHistory(partialRequest);
                final Transfer last = transfers.getLatest();

                if (transfers.isEmpty() || Transfer.equalsById(prevLast, last)) break;
                withdrawTransferMap.putAll(DeltaUtils.convertToMap(transfers));

                partialRequest.setStartDateTime(transfers.getMaxDateTime());
                prevLast = last;

                if (!sleep(request.getSleepTimeMillis())) break;
            }

            return new Transfers()
                .addAll(DeltaUtils.convertTransfersFromMap(depositTransferMap))
                .addAll(DeltaUtils.convertTransfersFromMap(withdrawTransferMap));
        } else {
            return getTransfers(partialRequest);
        }
    }

    public Transfers getDepositHistory(GetTransfersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = request(HttpMethod.GET, WITHDRAW_URL + "/depositHistory.html",
            ParamsBuilder.create()
                .putIfNotNull("asset", toBinanceCurrencySymbol(request.getCurrency()))
                .putIfNotNull("startTime", request.getStartEpochInMilli())
                .putIfNotNull("endTime", request.getEndEpochInMilli())
                .build());

        checkResponseError(null, response);

        return convert(response, BinanceDepositHistoryResponse.class, r -> r.toTransfers());
    }

    public Transfers getWithdrawHistory(GetTransfersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = request(HttpMethod.GET, WITHDRAW_URL + "/withdrawHistory.html",
            ParamsBuilder.create()
                .putIfNotNull("asset", toBinanceCurrencySymbol(request.getCurrency()))
                .putIfNotNull("startTime", request.getStartEpochInMilli())
                .putIfNotNull("endTime", request.getEndEpochInMilli())
                .build());

        checkResponseError(null, response);

        return convert(response, BinanceWithdrawHistoryResponse.class, r -> r.toTransfers());
    }

    @Override
    public WithdrawResult withdraw(WithdrawToWalletRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = request(HttpMethod.POST, WITHDRAW_URL + "/withdraw.html",
            ParamsBuilder.create()
                .put("asset", toBinanceCurrencySymbol(request.getCurrency()))
                .put("amount", request.getQuantity())
                .put("address", request.getAddress())
                .putIfNotNull("addressTag", request.getDestinationTag())
                .build());

        checkResponseError(null, response);

        WithdrawResult withdrawResult = null;
        final String body = response.getBody();
        final Integer status = response.getStatus();
        try {
            withdrawResult = convertList(response, BinanceWithdrawResponse.class, l -> {
                if (l != null && !l.isEmpty()) {
                    return l.get(0).getWithdrawResult();
                } else {
                    throw new CXAPIRequestException("No WithdrawResult")
                        .setCxId(this.getCxId())
                        .setResponseStatusCode(status)
                        .setExchangeMessage(body);
                }
            });
        } catch (IOException e) {
            throw new CXAPIRequestException("Error while converting withdraw result from exchange", e)
                .setCxId(this.getCxId())
                .setResponseStatusCode(status)
                .setExchangeMessage(body);

        }
        return withdrawResult;
    }

    @Override
    public UserAccount getUserAccount(GetUserAccountRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = request(HttpMethod.GET, PRIVATE_URL + "/account", null);

        checkResponseError(null, response);

        return convert(response, BinanceAccountResponse.class, (r -> r.getUserAccount()));
    }

    @Override
    public TradingFeeProvider getTradingFeeProvider()
        throws CTTKException
    {
        return new DefaultTradingFeeProvider().setFee("0.001");
    }

    @Override
    public BankAccount getBankAccount(GetBankAccountRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public WithdrawResult withdraw(WithdrawToBankRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public Wallets getAllWallets()
        throws CTTKException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    protected HttpResponse request(HttpMethod httpMethod, String url)
        throws CTTKException
    {
        return request(httpMethod, url, null);
    }

    protected HttpResponse request(HttpMethod httpMethod, String url, Map<String, String> params)
        throws CTTKException
    {
        if (params == null) params = new HashMap<>();
        params.put("timestamp", getTimestamp());

        StringBuilder urlBuilder = new StringBuilder()
            .append(url)
            .append("?")
            .append(UriUtils.toEncodedQueryParamsString(params))
            .append("&signature=")
            .append(createSign(credential.getSecretKey(), params));

        Builder builder = new Request.Builder();
        builder.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED);
        builder.addHeader(HEADER_ACCESS_KEY, credential.getAccessKey());

        if (httpMethod == HttpMethod.GET) {
            builder.url(urlBuilder.toString()).get();
        } else {
            builder.url(url);
            params.put("signature", createSign(credential.getSecretKey(), params));

            FormBody formBody = createFormBody(params);

            if (httpMethod == HttpMethod.POST) {
                builder.post(formBody);
            } else if (httpMethod == HttpMethod.PATCH) {
                builder.patch(formBody);
            } else if (httpMethod == HttpMethod.PUT) {
                builder.put(formBody);
            } else if (httpMethod == HttpMethod.DELETE) {
                builder.delete(formBody);
            }
        }

        final Request request = builder.build();

        debug(request, params);

        try (Response httpResponse = super.httpClient.newCall(request).execute()) {
            return toHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    private ZonedDateTime serverTimeUpdatedDateTime;
    private boolean serverTimeMismatched = false;

    private String getTimestamp()
        throws CTTKException
    {
        long timestamp = System.currentTimeMillis();
        if (serverTimeUpdatedDateTime == null
            || serverTimeUpdatedDateTime.until(ZonedDateTime.now(), ChronoUnit.MINUTES) > 10
            || serverTimeMismatched)
        {
            final long serverTime = super.getServerTime();
            serverTimeUpdatedDateTime = ZonedDateTime.now();
            timestamp = serverTime;
            if (Math.abs(serverTime - System.currentTimeMillis()) > 1000) {
                serverTimeMismatched = true;
            } else {
                serverTimeMismatched = false;
            }
        }

        return String.valueOf(timestamp);
    }

    private String createSign(String secretKey, Map<String, String> params) {
        return CryptoUtils.hmacSha256Hex(UriUtils.toEncodedQueryParamsString(params), secretKey);
    }

    private <T extends BinanceResultResponse, R> R convertForWithdraw(HttpResponse response, Class<T> claz, ThrowableFunction<T, R> mapper)
        throws CTTKException
    {
        final String body = response.getBody();
        final Integer status = response.getStatus();
        try {
            final T obj = objectMapper.readValue(body, claz);
            if (obj.getSuccess() == null || obj.getSuccess() == false) {
                throw new CXAPIRequestException(body)
                    .setCxId(this.getCxId())
                    .setResponseStatusCode(status)
                    .setExchangeMessage(body);
            }

            return mapper.apply(obj);
        } catch (IOException ioe) {
            throw new CXAPIRequestException("json parse error\n" + body)
                .setCxId(this.getCxId())
                .setResponseStatusCode(status)
                .setExchangeMessage(body);
        }
    }

    private void checkOrderResponseError(CreateOrderRequest request, HttpResponse response)
        throws CTTKException
    {
        // status 400
        final String body = response.getBody();
        String exchangeErrorCode;
        String exchangeMessage;

        try {
            Map<?, ?> map = objectMapper.readValue(body, Map.class);
            exchangeErrorCode = map.get("code").toString();
            exchangeMessage = map.get("msg").toString();
        } catch (IOException | NumberFormatException | NullPointerException e) {
            return;
        }

        switch (exchangeErrorCode) {
            case "-1111":
                // Precision is over the maximum defined for this asset.
                throw new InvalidPriceIncrementPrecisionException()
                    .setCxId(this.getCxId())
                    .setBaseCurrency(request.getBaseCurrency())
                    .setQuoteCurrency(request.getQuoteCurrency())
                    .setExchangeMessage(body)
                    .setResponseStatusCode(response.getStatus())
                    .setExchangeErrorCode(exchangeErrorCode)
                    .setOrderQuantity(request.getQuantity())
                    .setOrderPrice(request.getPrice());
            case "-1121":  // Invalid symbol error
                throw new UnsupportedCurrencyPairException()
                    .setCxId(this.getCxId())
                    .setBaseCurrency(request.getBaseCurrency())
                    .setQuoteCurrency(request.getQuoteCurrency())
                    .setExchangeMessage(body)
                    .setResponseStatusCode(response.getStatus())
                    .setExchangeErrorCode(exchangeErrorCode);

            case "-1100": {
                // Illegal characters found in parameter
                if (exchangeMessage.contains("price")) {
                    throw new InvalidPriceRangeException()
                        .setCxId(this.getCxId())
                        .setOrderPrice(request.getPrice())
                        .setOrderQuantity(request.getQuantity())
                        .setBaseCurrency(request.getBaseCurrency())
                        .setQuoteCurrency(request.getQuoteCurrency())
                        .setExchangeMessage(body)
                        .setResponseStatusCode(response.getStatus())
                        .setExchangeErrorCode(exchangeErrorCode);
                } else if (exchangeMessage.contains("quantity")) {
                    throw new InvalidAmountRangeException()
                        .setCxId(this.getCxId())
                        .setOrderPrice(request.getPrice())
                        .setOrderQuantity(request.getQuantity())
                        .setBaseCurrency(request.getBaseCurrency())
                        .setQuoteCurrency(request.getQuoteCurrency())
                        .setExchangeMessage(body)
                        .setResponseStatusCode(response.getStatus())
                        .setExchangeErrorCode(exchangeErrorCode);
                }
                break;
            }
            case "-2010":  // Account has insufficient balance for requested action.
                throw new InsufficientBalanceException()
                    .setCxId(this.getCxId())
                    .setOrderPrice(request.getPrice())
                    .setOrderQuantity(request.getQuantity())
                    .setBaseCurrency(request.getBaseCurrency())
                    .setQuoteCurrency(request.getQuoteCurrency())
                    .setExchangeMessage(body)
                    .setResponseStatusCode(response.getStatus())
                    .setExchangeErrorCode(exchangeErrorCode);
            case "-1013":
                // Filter failure
                if (exchangeMessage.contains("Filter failure: PERCENT_PRICE")) {
                    throw new InvalidPriceRangeException()
                        .setCxId(this.getCxId())
                        .setBaseCurrency(request.getBaseCurrency())
                        .setQuoteCurrency(request.getQuoteCurrency())
                        .setOrderPrice(request.getPrice())
                        .setOrderQuantity(request.getQuantity())
                        .setExchangeMessage(body)
                        .setResponseStatusCode(response.getStatus())
                        .setExchangeErrorCode(exchangeErrorCode);
                } else if (exchangeMessage.contains("Invalid quantity.")) {
                    throw new InvalidAmountRangeException()
                        .setCxId(this.getCxId())
                        .setBaseCurrency(request.getBaseCurrency())
                        .setQuoteCurrency(request.getQuoteCurrency())
                        .setOrderPrice(request.getPrice())
                        .setOrderQuantity(request.getQuantity())
                        .setExchangeMessage(body)
                        .setResponseStatusCode(response.getStatus())
                        .setExchangeErrorCode(exchangeErrorCode);
                } else if (exchangeMessage.contains("Price * QTY is zero or less.")
                    || exchangeMessage.contains("Filter failure: MIN_NOTIONAL"))
                {
                    throw new InvalidTotalMinException()
                        .setCxId(this.getCxId())
                        .setBaseCurrency(request.getBaseCurrency())
                        .setQuoteCurrency(request.getQuoteCurrency())
                        .setOrderPrice(request.getPrice())
                        .setOrderQuantity(request.getQuantity())
                        .setExchangeMessage(body)
                        .setResponseStatusCode(response.getStatus())
                        .setExchangeErrorCode(exchangeErrorCode);
                } else if (exchangeMessage.contains("QTY is over the symbol's maximum QTY.")) {
                    throw new InvalidAmountMaxException()
                        .setCxId(this.getCxId())
                        .setBaseCurrency(request.getBaseCurrency())
                        .setQuoteCurrency(request.getQuoteCurrency())
                        .setOrderPrice(request.getPrice())
                        .setOrderQuantity(request.getQuantity())
                        .setExchangeMessage(body)
                        .setResponseStatusCode(response.getStatus())
                        .setExchangeErrorCode(exchangeErrorCode);
                } else if (exchangeMessage.contains("Price is over the symbol's maximum price.")) {
                    throw new InvalidPriceMaxException()
                        .setCxId(this.getCxId())
                        .setBaseCurrency(request.getBaseCurrency())
                        .setQuoteCurrency(request.getQuoteCurrency())
                        .setOrderPrice(request.getPrice())
                        .setOrderQuantity(request.getQuantity())
                        .setExchangeMessage(body)
                        .setResponseStatusCode(response.getStatus())
                        .setExchangeErrorCode(exchangeErrorCode);
                } else if (exchangeMessage.contains("MAX_NUM_ORDERS")
                    || exchangeMessage.contains("MAX_NUM_ALGO_ORDERS")
                    || exchangeMessage.contains("MAX_NUM_ICEBERG_ORDERS")
                    || exchangeMessage.contains("EXCHANGE_MAX_NUM_ORDERS")
                    || exchangeMessage.contains("EXCHANGE_MAX_NUM_ALGO_ORDERS")
                )
                {
                    throw new MaxNumOrderException()
                        .setCxId(this.getCxId())
                        .setBaseCurrency(request.getBaseCurrency())
                        .setQuoteCurrency(request.getQuoteCurrency())
                        .setOrderPrice(request.getPrice())
                        .setOrderQuantity(request.getQuantity())
                        .setExchangeMessage(body)
                        .setResponseStatusCode(response.getStatus())
                        .setExchangeErrorCode(exchangeErrorCode);
                } else if (exchangeMessage.contains("Filter failure: PRICE_FILTER")
                    || exchangeMessage.contains("LOT_SIZE")
                    || exchangeMessage.contains("MARKET_LOT_SIZE")
                )
                {
                    InvalidOrderException ex = CTTKExceptionUtils.createByOrderVerifier(request, null, this.getCxId(), marketInfos);
                    if (ex != null) {
                        throw ex.setCxId(this.getCxId())
                            .setBaseCurrency(request.getBaseCurrency())
                            .setQuoteCurrency(request.getQuoteCurrency())
                            .setOrderPrice(request.getPrice())
                            .setOrderQuantity(request.getQuantity())
                            .setExchangeMessage(body)
                            .setResponseStatusCode(response.getStatus());
                    }
                }
                throw new InvalidOrderException()
                    .setCxId(this.getCxId())
                    .setBaseCurrency(request.getBaseCurrency())
                    .setQuoteCurrency(request.getQuoteCurrency())
                    .setOrderPrice(request.getPrice())
                    .setOrderQuantity(request.getQuantity())
                    .setExchangeMessage(body)
                    .setResponseStatusCode(response.getStatus())
                    .setExchangeErrorCode(exchangeErrorCode);
        }
    }
}
