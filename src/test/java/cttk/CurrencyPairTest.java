package cttk;

import org.junit.Assert;
import org.junit.Test;

public class CurrencyPairTest {
    @Test
    public void testGetPairString() {
        final CurrencyPair pair = CurrencyPair.of("ETH", "BTC");
        Assert.assertEquals("ETH-BTC", pair.getPairString());
    }

    @Test
    public void testParseMarketSymbol() {
        final CurrencyPair pair = CurrencyPair.parseMarketSymbol("ETH-BTC");
        Assert.assertEquals("ETH", pair.getBaseCurrency());
        Assert.assertEquals("BTC", pair.getQuoteCurrency());
    }
}
