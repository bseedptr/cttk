package cttk.impl.coinone;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.ZonedDateTime;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import cttk.auth.Credential;
import cttk.dto.Transfers;
import cttk.dto.UserTrades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.AbstractPrivateClientDeltaTest;
import cttk.request.GetTransfersDeltaRequest;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserOrderHistoryDeltaRequest;
import cttk.request.GetUserTradesDeltaRequest;
import cttk.request.GetUserTradesRequest;

@RunWith(MockitoJUnitRunner.class)
public class CoinonePrivateClientDeltaTest
    extends AbstractPrivateClientDeltaTest
{
    CoinonePrivateClient client;

    @Before
    public void setup() {
        client = spy(new CoinonePrivateClient(mock(Credential.class)));
    }

    @Test(expected = UnsupportedMethodException.class)
    public void testGetUserOrderHistoryDelta()
        throws UnsupportedCurrencyException, CTTKException
    {
        client.getUserOrderHistory(new GetUserOrderHistoryDeltaRequest());
    }

    @Test
    public void testGetUserTradesDelta()
        throws Exception
    {
        UserTrades userTrades = mock(UserTrades.class);
        when(userTrades.isNotEmpty()).thenReturn(true);
        doReturn(userTrades).when(client).getUserTrades(any(GetUserTradesRequest.class));

        GetUserTradesDeltaRequest request = new GetUserTradesDeltaRequest().setStartDateTime(ZonedDateTime.now());
        client.getUserTrades(request);

        verify(userTrades).filterByDateTimeGTE(request.getStartDateTime());
    }

    @Test
    public void testGetTransfersDelta()
        throws Exception
    {
        Transfers transfers = mock(Transfers.class);
        when(transfers.isNotEmpty()).thenReturn(true);
        doReturn(transfers).when(client).getTransfers(any(GetTransfersRequest.class));

        GetTransfersDeltaRequest request = new GetTransfersDeltaRequest().setStartDateTime(ZonedDateTime.now());
        client.getTransfers(request);

        verify(transfers).filterByDateTimeGTE(request.getStartDateTime());
    }
}
