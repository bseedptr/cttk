package cttk.impl.okex;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;
import cttk.exception.CTTKException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OkexUtilsV3Test {
    @Rule
    public ExpectedException ee = ExpectedException.none();

    @Test
    public void toOkexCurrencySymbolV3() {
        assertEquals(OkexUtilsV3.toOkexCurrencySymbolV3(null), null);
        assertEquals(OkexUtilsV3.toOkexCurrencySymbolV3("ETH"), "ETH");
        assertEquals(OkexUtilsV3.toOkexCurrencySymbolV3("eth"), "ETH");
    }

    @Test
    public void toOkexMarketSymbolV3() {
        assertEquals(OkexUtilsV3.toOkexMarketSymbolV3(CurrencyPair.of("EOS", "ETH")), "EOS-ETH");
        assertEquals(OkexUtilsV3.toOkexMarketSymbolV3(CurrencyPair.of("eos", "eth")), "EOS-ETH");
    }

    @Test
    public void testToOkexOrderStatusV3WhenStatus()
        throws CTTKException
    {
        assertEquals(OkexUtilsV3.toOkexOrderStatusV3(null), "all");
        assertEquals(OkexUtilsV3.toOkexOrderStatusV3(OrderStatus.FILLED), "filled");
        assertEquals(OkexUtilsV3.toOkexOrderStatusV3(OrderStatus.UNFILLED), "open|ordering|cancelling|part_filled");
    }

    @Test
    public void testToOkexOrderStatusV3WhenStatusWhenNotSupported()
        throws CTTKException
    {
        ee.expect(CTTKException.class);
        ee.expectMessage("The status 'EXPIRED' is not supported.");

        OkexUtilsV3.toOkexOrderStatusV3(OrderStatus.EXPIRED);
    }

    @Test
    public void testRefineWhenOrderSideIsBUY() {
        List<UserTrade> list = new ArrayList<>();
        list.add(makeFixture("0.01", OrderSide.BUY));
        list.add(makeFixture("0.00", OrderSide.SELL));
        list.add(makeFixture("0.03", OrderSide.BUY));
        list.add(makeFixture("0.00", OrderSide.SELL));
        list.add(makeFixture("0.05", OrderSide.BUY));
        list.add(makeFixture("0.00", OrderSide.SELL));
        UserTrades userTrades = new UserTrades().setTrades(list);
        UserTrades refinedUserTrades = OkexUtilsV3.refine(userTrades, OrderSide.BUY);
        log.debug("{}", refinedUserTrades);

        assertEquals(refinedUserTrades.size(), 3);
        assertEquals(refinedUserTrades.get(0).getSide(), OrderSide.BUY);
        assertEquals(refinedUserTrades.get(0).getFee(), new BigDecimal("0.01"));
        assertEquals(refinedUserTrades.get(1).getSide(), OrderSide.BUY);
        assertEquals(refinedUserTrades.get(1).getFee(), new BigDecimal("0.03"));
        assertEquals(refinedUserTrades.get(2).getSide(), OrderSide.BUY);
        assertEquals(refinedUserTrades.get(2).getFee(), new BigDecimal("0.05"));
    }

    @Test
    public void testRefineWhenOrderSideIsSELL() {
        List<UserTrade> list = new ArrayList<>();
        list.add(makeFixture("0.01", OrderSide.BUY));
        list.add(makeFixture("0.00", OrderSide.SELL));
        list.add(makeFixture("0.03", OrderSide.BUY));
        list.add(makeFixture("0.00", OrderSide.SELL));
        list.add(makeFixture("0.05", OrderSide.BUY));
        list.add(makeFixture("0.00", OrderSide.SELL));
        UserTrades userTrades = new UserTrades().setTrades(list);
        UserTrades refinedUserTrades = OkexUtilsV3.refine(userTrades, OrderSide.SELL);
        log.debug("{}", refinedUserTrades);

        assertEquals(refinedUserTrades.size(), 3);
        assertEquals(refinedUserTrades.get(0).getSide(), OrderSide.SELL);
        assertEquals(refinedUserTrades.get(0).getFee(), new BigDecimal("0.01"));
        assertEquals(refinedUserTrades.get(1).getSide(), OrderSide.SELL);
        assertEquals(refinedUserTrades.get(1).getFee(), new BigDecimal("0.03"));
        assertEquals(refinedUserTrades.get(2).getSide(), OrderSide.SELL);
        assertEquals(refinedUserTrades.get(2).getFee(), new BigDecimal("0.05"));
    }

    @Test
    public void testRefineWhenUserTradesIsEmpty() {
        UserTrades emptyUserTrades = new UserTrades();
        UserTrades refinedUserTrades = OkexUtilsV3.refine(emptyUserTrades, OrderSide.SELL);

        assertTrue(refinedUserTrades.isEmpty());
    }

    private UserTrade makeFixture(String feeValue, OrderSide orderSide) {
        return new UserTrade()
            .setFee(new BigDecimal(feeValue))
            .setSide(orderSide);
    }
}