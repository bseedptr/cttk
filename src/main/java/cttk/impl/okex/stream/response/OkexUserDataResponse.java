package cttk.impl.okex.stream.response;

import lombok.Data;

@Data
public class OkexUserDataResponse<T> {
    int binary;
    String product; // ex) spot or margin
    String type; // ex) order, balance
    String base;
    String quote;
    String channel;
    T data;
}
