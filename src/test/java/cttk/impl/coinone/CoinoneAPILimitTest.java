package cttk.impl.coinone;

import org.junit.Test;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.impl.AbstractAPILimitTest;

public class CoinoneAPILimitTest
    extends AbstractAPILimitTest
{
    @Override
    public String getCxId() {
        return CXIds.COINONE;
    }

    @Test
    public void testPublicAPILimit()
        throws CTTKException, InterruptedException
    {
        try {
            Thread.sleep(DEFAULT_API_LIMIT_RECOVERY_TIME_IN_SECOND * 1000);
        } catch (InterruptedException e) {
            // ignore exception here 
        }
        testCase((args) -> {
            return publicClient.getAllTickers();
        }, false, 1000);
        // We can't reach public API limit
        logger.info("We can't reach public API limit in " + getCxId());
    }

    @Test
    public void testPrivateAPILimit()
        throws CTTKException, InterruptedException
    {
        try {
            Thread.sleep(DEFAULT_API_LIMIT_RECOVERY_TIME_IN_SECOND * 1000);
        } catch (InterruptedException e) {
            // ignore exception here 
        }
        testCase((args) -> {
            return privateClient.getAllBalances();
        }, false, countDownNum);
        // We can't reach private API limit
        logger.info("We can't reach private API limit in " + getCxId());
    }
}
