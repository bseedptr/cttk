package cttk.impl.util;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class ParamsBuilder {
    final Map<String, String> params = new HashMap<>();

    protected ParamsBuilder() {
        super();
    }

    public ParamsBuilder put(String key, Object value) {
        if (value instanceof BigDecimal) {
            params.put(key, ((BigDecimal) value).toPlainString());
        } else {
            params.put(key, String.valueOf(value));
        }
        return this;
    }

    public ParamsBuilder putIfNotNull(String key, Object value) {
        if (value != null) {
            if (value instanceof BigDecimal) {
                params.put(key, ((BigDecimal) value).toPlainString());
            } else {
                params.put(key, String.valueOf(value));
            }
        }
        return this;
    }

    public ParamsBuilder putDefaultIfNull(String key, Object value, Object defaultValue) {
        if (value instanceof BigDecimal) {
            params.put(key, ((BigDecimal) value).toPlainString());
        } else {
            params.put(key, String.valueOf(value == null ? defaultValue : value));
        }
        return this;
    }

    public ParamsBuilder putAll(Map<String, String> map) {
        if (map != null && !map.isEmpty()) params.putAll(map);
        return this;
    }

    public Map<String, String> build() {
        return params;
    }

    public static ParamsBuilder create() {
        return new ParamsBuilder();
    }

    public static Map<String, String> build(String key, Object value) {
        return ParamsBuilder.create().put(key, value).build();
    }

    public static Map<String, String> buildIfNotNull(String key, Object value) {
        return ParamsBuilder.create().putIfNotNull(key, value).build();
    }

    public static Map<String, String> buildEmpty() {
        return new HashMap<>();
    }
}
