package cttk.dto;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cttk.AbstractCurrencyPair;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(chain = true)
public class Trades
    extends AbstractCurrencyPair<Trades>
{
    List<Trade> trades;
    String time; // for Korbit: minute, hour, day
    Long last;

    public int size() {
        return trades == null ? 0 : trades.size();
    }

    public boolean hasTrades() {
        return size() > 0;
    }

    public boolean hasLast() {
        return last != null;
    }

    @JsonIgnore
    public BigInteger getMaxSno() {
        if (trades == null || trades.isEmpty()) return BigInteger.ZERO;
        return trades.stream()
            .filter(i -> i.getSno() != null)
            .map(i -> i.getSno())
            .max((a, b) -> a.compareTo(b)).orElse(BigInteger.ZERO);
    }

    public List<Trade> removeIfSnoLessThanOrEqual(BigInteger minSno) {
        if (trades == null || trades.isEmpty()) return null;
        trades = trades.parallelStream().filter(i -> i.getSno().compareTo(minSno) > 0).collect(Collectors.toList());
        return trades;
    }
}
