package cttk.impl.bibox;

import cttk.CXIds;
import cttk.impl.AbstractUserDataStreamTest;

public class BiboxUserDataStreamTest
    extends AbstractUserDataStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.BIBOX;
    }
}
