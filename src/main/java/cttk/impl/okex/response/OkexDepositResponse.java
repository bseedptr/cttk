package cttk.impl.okex.response;

import static cttk.impl.okex.OkexUtilsV3.toStdCurrencySymbolV3;
import static cttk.impl.okex.OkexUtilsV3.toStdDepositTransferStatusTypeV3;
import static java.util.stream.Collectors.toList;

import java.math.BigDecimal;
import java.util.ArrayList;

import cttk.TransferType;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class OkexDepositResponse
    extends ArrayList<OkexDeposit>
{
    public Transfers toTransfers() {
        return new Transfers()
            .setTransfers(stream()
                .map(OkexDeposit::toTransfer)
                .collect(toList()));
    }
}

@Data
class OkexDeposit {
    String currency;
    BigDecimal amount;
    String to;
    String txid;
    String timestamp;
    int status;

    Transfer toTransfer() {
        return new Transfer()
            .setTid(txid)
            .setTxId(txid)
            .setCurrency(toStdCurrencySymbolV3(currency))
            .setAddress(to)
            .setAmount(amount)
            .setStatus(String.valueOf(status))
            .setStatusType(toStdDepositTransferStatusTypeV3(status))
            .setType(TransferType.DEPOSIT)
            .setCompletedDateTime(DateTimeUtils.parseZdt(timestamp));
    }
}
