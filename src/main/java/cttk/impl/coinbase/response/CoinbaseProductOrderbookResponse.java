package cttk.impl.coinbase.response;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.dto.OrderBook;
import cttk.dto.OrderBook.SimpleOrder;
import lombok.Data;

@Data
public class CoinbaseProductOrderbookResponse {
    BigDecimal sequence;
    List<List<String>> asks;
    List<List<String>> bids;

    public OrderBook toOrderBook(CurrencyPair currencyPair) {
        return new OrderBook()
            .setCurrencyPair(currencyPair)
            .setMerged(false)
            .setAsks(toSimpleOrderList(asks))
            .setBids(toSimpleOrderList(bids));
    }

    private static List<SimpleOrder> toSimpleOrderList(List<List<String>> list) {
        if (list == null) return null;
        return list.stream().map(i -> new SimpleOrder(i.get(0), i.get(1))).collect(Collectors.toList());
    }
}
