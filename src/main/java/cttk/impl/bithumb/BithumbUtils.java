package cttk.impl.bithumb;

import static cttk.OrderSide.SELL;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import cttk.OrderSide;
import cttk.util.StringUtils;

public class BithumbUtils {
    public static final BigDecimal[] PRICE_RANGES = {
        new BigDecimal("1000000"),
        new BigDecimal("500000"),
        new BigDecimal("100000"),
        new BigDecimal("50000"),
        new BigDecimal("10000"),
        new BigDecimal("5000"),
        new BigDecimal("100"),
        new BigDecimal("10")
    };

    public static final BigDecimal[] PRICE_INCREMENTS = {
        new BigDecimal("1000"),
        new BigDecimal("500"),
        new BigDecimal("100"),
        new BigDecimal("50"),
        new BigDecimal("10"),
        new BigDecimal("5"),
        new BigDecimal("1"),
        new BigDecimal("0.1"),
        new BigDecimal("0.01")
    };

    public static List<String> SEARCH_CODES_FOR_TRADE = new LinkedList<>(Arrays.asList("1", "2"));
    public static List<String> SEARCH_CODES_FOR_TRANSFER = new LinkedList<>(Arrays.asList("4", "5"));

    public static String toStdCurrencySymbol(String currency) {
        return StringUtils.toUpper(currency);
    }

    public static String toString(OrderSide orderSide) {
        if (orderSide == null)
            return null;

        switch (orderSide) {
            case BUY:
                return "bid";
            case SELL:
                return "ask";
            default:
                return null;
        }
    }

    public static boolean isTrade(String search) {
        return SEARCH_CODES_FOR_TRADE.contains(search);
    }

    public static boolean isTransfer(String search) {
        return SEARCH_CODES_FOR_TRANSFER.contains(search);
    }

    public static String guessFeeCurrency(OrderSide side, String baseCurrency) {
        return side == SELL ? "KRW" : baseCurrency;
    }
}
