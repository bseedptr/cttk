package cttk.impl.bithumb.response;

import lombok.Data;

@Data
public class BithumbStatusResponse {

    public static final String VALID_STATUS = "0000";

    protected String status;
}
