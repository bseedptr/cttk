package cttk.impl.binance.response;

import lombok.Data;

@Data
public class BinanceListenKeyResponse {
    String listenKey;
}
