package cttk.impl.upbit;

import cttk.CXIds;
import cttk.impl.AbstractAPILimitTest;

public class UpbitAPILimitTest
    extends AbstractAPILimitTest
{
    @Override
    public String getCxId() {
        return CXIds.UPBIT;
    }
}
