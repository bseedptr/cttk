package cttk.impl.stream;

import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.CXStreamOpener;
import cttk.PublicCXClient;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.request.GetOrderBookRequest;
import cttk.request.GetRecentTradesRequest;
import cttk.request.GetTickerRequest;
import cttk.request.OpenOrderBookStreamRequest;
import cttk.request.OpenTickerStreamRequest;
import cttk.request.OpenTradeStreamRequest;

public class PollingCXStreamOpener
    implements CXStreamOpener
{
    private final PublicCXClient client;

    public PollingCXStreamOpener(PublicCXClient client) {
        super();
        this.client = client;
    }

    @Override
    public String getCxId() {
        return client == null ? null : client.getCxId();
    }

    @Override
    public CXStream<Ticker> openTickerStream(OpenTickerStreamRequest request, CXStreamListener<Ticker> listener) {
        return new PollingTickerStream(client, request, listener,
            new GetTickerRequest().setCurrencyPair(request));
    }

    @Override
    public CXStream<OrderBook> openOrderBookStream(OpenOrderBookStreamRequest request, CXStreamListener<OrderBook> listener) {
        return new PollingOrderBookStream(client, request, listener,
            new GetOrderBookRequest()
                .setCurrencyPair(request)
                .setCount(request.getCount())
                .setPrecision(request.getPrecision()));
    }

    @Override
    public CXStream<Trades> openTradeStream(OpenTradeStreamRequest request, CXStreamListener<Trades> listener)
        throws CTTKException
    {
        return new PollingTradeStream(client, request, listener,
            new GetRecentTradesRequest().setCurrencyPair(request));
    }

}
