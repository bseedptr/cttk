package cttk.exception;

import java.math.BigDecimal;

import cttk.CurrencyPair;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class InvalidAmountRangeException
    extends InvalidOrderException
{
    private static final long serialVersionUID = 1L;

    public InvalidAmountRangeException() {
        super();
    }

    public InvalidAmountRangeException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidAmountRangeException(String message) {
        super(message);
    }

    public InvalidAmountRangeException(Throwable cause) {
        super(cause);
    }

    @Override
    public InvalidAmountRangeException setCxId(String cxId) {
        super.setCxId(cxId);
        return this;
    }

    @Override
    public InvalidAmountRangeException setResponseStatusCode(Integer responseStatusCode) {
        super.setResponseStatusCode(responseStatusCode);
        return this;
    }

    @Override
    public InvalidAmountRangeException setExchangeMessage(String exchangeMessage) {
        super.setExchangeMessage(exchangeMessage);
        return this;
    }

    @Override
    public InvalidAmountRangeException setExchangeErrorCode(String exchangeErrorCode) {
        super.setExchangeErrorCode(exchangeErrorCode);
        return this;
    }

    @Override
    public InvalidAmountRangeException setCurrencyPair(CurrencyPair currencyPair) {
        super.setCurrencyPair(currencyPair);
        return this;
    }

    @Override
    public InvalidAmountRangeException setOrderPrice(BigDecimal orderPrice) {
        super.setOrderPrice(orderPrice);
        return this;
    }

    @Override
    public InvalidAmountRangeException setOrderQuantity(BigDecimal orderQuantity) {
        super.setOrderQuantity(orderQuantity);
        return this;
    }

    @Override
    public InvalidAmountRangeException setBaseCurrency(String baseCurrency) {
        super.setBaseCurrency(baseCurrency);
        return this;
    }

    @Override
    public InvalidAmountRangeException setQuoteCurrency(String quoteCurrency) {
        super.setQuoteCurrency(quoteCurrency);
        return this;
    }
}