package cttk.impl.bithumb.response;

import static cttk.impl.util.MathUtils.isNullOrZero;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import cttk.OrderStatus;
import cttk.PricingType;
import cttk.dto.UserOrder;
import cttk.dto.UserTrade;
import cttk.impl.util.MathUtils;
import cttk.request.CreateOrderRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BithumbCreateOrderResponse
    extends BithumbDataResponse<List<BithumbSimpleTrade>>
{
    private String order_id;

    public UserOrder toUserOrder() {
        return new UserOrder()
            .setOid(order_id)
            .setPricingType(PricingType.LIMIT)
            .setTrades(getUserTradeList());
    }

    public UserOrder toUserOrder(CreateOrderRequest request) {
        final UserOrder userOrder = new UserOrder()
            .setOid(order_id)
            .setCurrencyPair(request)
            .setSide(request.getSide())
            .setPrice(request.getPrice())
            .setQuantity(request.getQuantity())
            .setTotal(request.computeTotal())
            .setPricingType(PricingType.LIMIT)
            .setTrades(getUserTradeList());

        final BigDecimal filledQuantity = userOrder.getTradedQuantity();

        final OrderStatus orderStatus = isNullOrZero(filledQuantity)
            ? OrderStatus.UNFILLED
            : (MathUtils.equals(filledQuantity, request.getQuantity())
                ? OrderStatus.FILLED
                : OrderStatus.PARTIALLY_FILLED);

        return userOrder
            .setFilledQuantity(filledQuantity)
            .setRemainingQuantity(MathUtils.subtract(userOrder.getQuantity(), filledQuantity))
            .setStatusType(orderStatus);
    }

    List<UserTrade> getUserTradeList() {
        if (data == null) return null;
        return data.stream()
            .map(i -> i.toUserTrade())
            .collect(Collectors.toList());
    }

}

@Data
class BithumbSimpleTrade {
    String contNo;
    String price; //1Currency당 체결가
    String units; //체결 수량
    Long total; //KRW 체결가
    String fee;

    UserTrade toUserTrade() {
        return new UserTrade()
            .setTid(contNo)
            .setPrice(new BigDecimal(price))
            .setQuantity(new BigDecimal(units))
            .setTotal(new BigDecimal(total))
            .setFee(new BigDecimal(fee));
    }
}
