package cttk.impl.bithumb.response;

import cttk.dto.Wallet;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BithumbWalletResponse
    extends BithumbDataResponse<BithumWallet>
{
    public Wallet toWallet() {
        return new Wallet()
            .setCurrency(data.currency)
            .setAddress(data.wallet_address);
    }
}

@Data
class BithumWallet {
    String currency;
    String wallet_address;
}