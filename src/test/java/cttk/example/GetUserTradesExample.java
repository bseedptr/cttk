package cttk.example;

import java.io.IOException;
import java.util.List;

import cttk.CTTK;
import cttk.CXClientFactory;
import cttk.CurrencyPair;
import cttk.PrivateCXClient;
import cttk.auth.CXCredentialsProvider;
import cttk.dto.UserTrades;
import cttk.request.GetUserTradesRequest;
import cttk.test.TestConfig;

public class GetUserTradesExample {

    public static void main(String[] args)
        throws IOException
    {
        List<String> cxIds = CTTK.getSupportedCxIds();

        for (String cxId : cxIds) {
            final TestConfig testConfig = TestConfig.readFromClasspathYaml("config/" + cxId.toLowerCase() + ".yml");
            final CurrencyPair currencyPair = testConfig.getValidCurrencyPair();

            try {
                PrivateCXClient client = CXClientFactory.createPrivateCXClient(cxId, CXCredentialsProvider.getDefaultCredential(cxId).getDataKey());

                UserTrades userTrades = client.getUserTrades(new GetUserTradesRequest().setCurrencyPair(currencyPair));

                Examples.print(cxId, userTrades);

            } catch (java.io.FileNotFoundException e) {
                continue;
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
        }
    }

}
