package cttk.impl.bithumb.response;

import static cttk.util.StringUtils.parseBigInteger;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import cttk.OrderSide;
import cttk.dto.Trade;
import cttk.dto.Trades;
import cttk.impl.util.ZoneIds;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BithumbTradesResponse
    extends BithumbDataResponse<List<BithumbTrade>>
{
    public Trades toTrades() {
        return new Trades().setTrades(toTradeList(this.data));
    }

    private List<Trade> toTradeList(List<BithumbTrade> bithumbTrade) {
        if (bithumbTrade == null) return null;
        return bithumbTrade.stream().map(BithumbTrade::toTrade).collect(Collectors.toList());
    }
}

@Data
class BithumbTrade {
    String cont_no;
    String transaction_date;
    String type;
    BigDecimal units_traded;
    BigDecimal price;
    BigDecimal total;

    public Trade toTrade() {
        return new Trade()
            .setSno(parseBigInteger(cont_no))
            .setTid(cont_no)
            .setDateTime(parseZdt(transaction_date))
            .setSide(OrderSide.of(type))
            .setQuantity(units_traded)
            .setPrice(price)
            .setTotal(total);
    }

    private static final DateTimeFormatter DTF = DateTimeFormatter.ofPattern("yyyy-MM-dd H:mm:ss");

    private static ZonedDateTime parseZdt(String str) {
        if (str == null || str.trim().isEmpty()) return null;
        return LocalDateTime.parse(str, DTF).atZone(ZoneIds.SEOUL);
    }
}