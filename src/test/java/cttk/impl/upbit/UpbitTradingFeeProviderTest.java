package cttk.impl.upbit;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;

import cttk.CurrencyPair;
import cttk.TradingFeeProvider;
import cttk.dto.TradingFee;
import cttk.exception.CTTKException;

public class UpbitTradingFeeProviderTest {
    @Test
    public void testKrwBasic()
        throws CTTKException
    {
        final TradingFeeProvider tradingFeeProvider = new UpbitTradingFeeProvider();
        final TradingFee fee = tradingFeeProvider.getFee(CurrencyPair.of("BTC", "KRW"));
        Assert.assertNotNull(fee);
        Assert.assertEquals(fee.getMakerFee(), new BigDecimal("0.0005"));
    }

    @Test
    public void testBtcBasic()
        throws CTTKException
    {
        final TradingFeeProvider tradingFeeProvider = new UpbitTradingFeeProvider();
        final TradingFee fee = tradingFeeProvider.getFee(CurrencyPair.of("ETH", "BTC"));
        Assert.assertNotNull(fee);
        Assert.assertEquals(fee.getMakerFee(), new BigDecimal("0.0025"));
    }

    @Test
    public void testKrwCustom()
        throws CTTKException
    {
        System.setProperty("cttk.upbit.trading-fee-rate.krw", "0.00139");
        final TradingFeeProvider tradingFeeProvider = new UpbitTradingFeeProvider();
        final TradingFee fee = tradingFeeProvider.getFee(CurrencyPair.of("ETH", "KRW"));
        Assert.assertNotNull(fee);
        Assert.assertEquals(fee.getMakerFee(), new BigDecimal("0.00139"));
    }
}
