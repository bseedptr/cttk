package cttk.impl.bibox.response;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.PricingType;
import cttk.dto.UserOrder;
import cttk.dto.UserTrade;
import cttk.impl.bibox.BiboxUtils;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;

@Data
public class BiboxUserOrderPending {
    String id;
    Long createdAt;
    String accountType;
    @JsonProperty("coin_symbol")
    String coinSymbol;
    @JsonProperty("currency_symbol")
    String currencySymbol;
    OrderSide orderSide;
    @JsonProperty("order_from")
    Integer orderFrom;
    String orderType;
    BigDecimal price;
    BigDecimal amount;
    BigDecimal money;
    @JsonProperty("deal_amount")
    BigDecimal dealAmount;
    @JsonProperty("deal_money")
    BigDecimal dealMoney;
    @JsonProperty("deal_percent")
    String dealPercent;
    BigDecimal unexecuted;
    String status;
    OrderStatus orderStatus;

    public UserOrder toUserOrder() {
        return new UserOrder()
            .setCurrencyPair(coinSymbol, currencySymbol)
            .setOid(id)
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(createdAt))
            .setSide(orderSide)
            .setStatus(status)
            .setStatusType(orderStatus)
            .setPricingType(PricingType.of(orderType.toUpperCase()))
            .setPrice(price)
            .setQuantity(amount)
            .setFilledQuantity(dealAmount)
            .setRemainingQuantity(unexecuted)
            .setTotal(money);
    }

    public UserTrade toUserTrade() {
        // Bibox rest client manages userTrade with orderId, not tradeId
        // put orderId instead of userId
        // dealAmount, dealMoney are accumulated values
        return new UserTrade()
            .setCurrencyPair(coinSymbol, currencySymbol)
            .setOid(id)
            .setTid(id)
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(createdAt))
            .setSide(orderSide)
            .setStatus(status)
            .setPrice(price)
            .setQuantity(dealAmount)
            .setTotal(dealMoney);
    }

    public boolean isOpenOrder() {
        return OrderStatus.isOpen(orderStatus);
    }

    @JsonProperty("account_type")
    public void setAccountType(Integer type) {
        this.accountType = BiboxUtils.parseAccountType(type);
    }

    @JsonProperty("order_side")
    public void setOrderSide(String side) {
        this.orderSide = BiboxUtils.parseOrderSide(side);
    }

    @JsonProperty("order_type")
    public void setOrderType(Integer code) {
        this.orderType = BiboxUtils.parseOrderType(code);
    }

    @JsonProperty("status")
    public void setStatus(Integer code) {
        this.status = BiboxUtils.parseOrderStatus(code);
        this.orderStatus = BiboxUtils.parseOrderStatusType(status);
    }
}
