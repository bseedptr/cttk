package cttk.impl.huobi.stream;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Trades;
import cttk.impl.huobi.stream.response.HuobiWebsocketOrderBookResponse;
import cttk.impl.huobi.stream.response.HuobiWebsocketTickerResponse;
import cttk.impl.huobi.stream.response.HuobiWebsocketTradesResponse;
import cttk.impl.stream.MessageConverter;

public class HuobiWebsocketMessageConverterFactory {

    private static final ObjectMapper createObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper;
    }

    public static MessageConverter<Ticker> createTickerMessageConverter() {
        return new MessageConverter<Ticker>() {
            private final ObjectMapper mapper = createObjectMapper();

            @Override
            public Ticker convert(String message)
                throws Exception
            {
                return mapper.readValue(message, HuobiWebsocketTickerResponse.class).toTicker();
            }

        };
    }

    public static MessageConverter<OrderBook> createOrderBookMessageConverter() {
        return new MessageConverter<OrderBook>() {
            private final ObjectMapper mapper = createObjectMapper();

            @Override
            public OrderBook convert(String message)
                throws Exception
            {
                return mapper.readValue(message, HuobiWebsocketOrderBookResponse.class).toOrderBook();
            }

        };
    }

    public static MessageConverter<Trades> createTradesMessageConverter() {
        return new MessageConverter<Trades>() {
            private final ObjectMapper mapper = createObjectMapper();

            @Override
            public Trades convert(String message)
                throws Exception
            {
                return mapper.readValue(message, HuobiWebsocketTradesResponse.class).toTrades();
            }

        };
    }
}
