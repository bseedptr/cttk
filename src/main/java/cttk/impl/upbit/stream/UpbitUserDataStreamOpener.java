package cttk.impl.upbit.stream;

import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.PrivateCXClient;
import cttk.UserData;
import cttk.UserDataStreamOpener;
import cttk.exception.CTTKException;
import cttk.request.OpenUserDataStreamRequest;

public class UpbitUserDataStreamOpener
    implements UserDataStreamOpener
{
    private final PrivateCXClient client;

    public UpbitUserDataStreamOpener(PrivateCXClient client) {
        this.client = client;
    }

    @Override
    public CXStream<UserData> openUserDataStream(OpenUserDataStreamRequest request, CXStreamListener<UserData> listener)
        throws CTTKException
    {
        return new UpbitPollingUserDataStream(client, request, listener);
    }
}
