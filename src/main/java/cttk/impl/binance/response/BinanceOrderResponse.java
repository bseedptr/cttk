package cttk.impl.binance.response;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.PricingType;
import cttk.dto.UserOrder;
import cttk.impl.binance.BinanceUtils;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;

@Data
public class BinanceOrderResponse {
    Boolean isWorking;
    Long orderId;
    Long transactTime;
    Long time;
    String symbol;
    String clientOrderId;
    BigDecimal price;
    @JsonProperty("origClientOrderId")
    String originalClientOrderId;
    @JsonProperty("origQty")
    BigDecimal originalQuantity;
    @JsonProperty("executedQty")
    BigDecimal executedQuantity;
    String status;
    String timeInForce;
    String type;
    String side;
    BigDecimal stopPrice;
    @JsonProperty("icebergQty")
    BigDecimal icebergQuantity;
    List<Fill> fills;
    @JsonProperty("cummulativeQuoteQty")
    BigDecimal cummulativeQuoteQuantity;
    Long updateTime;

    public UserOrder getUserOrder() {
        if (orderId == null) {
            return null;
        }
        UserOrder userOrder = new UserOrder()
            .setCurrencyPair(BinanceUtils.parseMarketSymbol(symbol))
            .setOid(String.valueOf(orderId))
            .setClientOrderId(clientOrderId)
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(time))
            .setSide(OrderSide.of(side))
            .setPricingType(PricingType.of(type))
            .setStatus(status)
            .setStatusType(BinanceUtils.getStatusType(status))
            .setPrice(price)
            .setQuantity(originalQuantity)
            .setFilledQuantity(executedQuantity)
            .setRemainingQuantity(MathUtils.subtract(originalQuantity, executedQuantity))
            .setTotal(MathUtils.multiply(price, originalQuantity));

        if (userOrder.getStatusType() == OrderStatus.CANCELED && updateTime != null) {
            userOrder.setCanceledDateTime(DateTimeUtils.zdtFromEpochMilli(updateTime));
        } else if (userOrder.getStatusType() == OrderStatus.FILLED && updateTime != null) {
            userOrder.setCompletedDateTime(DateTimeUtils.zdtFromEpochMilli(updateTime));
        }
        return userOrder;
    }
}

@Data
class Fill {
    String price;
    @JsonProperty("qty")
    String quantity;
    String commission;
    String commissionAsset;
}
