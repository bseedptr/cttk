package cttk.impl.poloniex;

import cttk.CXIds;
import cttk.impl.AbstractUserDataStreamTest;

public class PoloniexUserDataStreamTest
    extends AbstractUserDataStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.POLONIEX;
    }

    @Override
    protected boolean useThread() {
        return true;
    }
}
