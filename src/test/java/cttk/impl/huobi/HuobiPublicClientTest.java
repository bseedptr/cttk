package cttk.impl.huobi;

import org.junit.Test;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.AbstractPublicCXClientTest;

public class HuobiPublicClientTest
    extends AbstractPublicCXClientTest
{
    @Override
    public String getCxId() {
        return CXIds.HUOBI;
    }

    @Override
    protected long getThreadSleepMillis() {
        return 1000l;
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetAllTickers()
        throws CTTKException
    {
        super.testGetAllTickers();
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetTradeHistory()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetTradeHistory();
    }
}