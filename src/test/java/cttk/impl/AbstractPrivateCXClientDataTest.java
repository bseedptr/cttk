package cttk.impl;

import static cttk.test.TestUtils.hasTestProfile;
import static cttk.util.StringUtils.toUpper;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cttk.CXClientFactory;
import cttk.CXIds;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.TradingFeeProvider;
import cttk.TransferStatus;
import cttk.TransferType;
import cttk.auth.CXCredentialsProvider;
import cttk.dto.Balance;
import cttk.dto.Balances;
import cttk.dto.BankAccount;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.dto.UserAccount;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;
import cttk.dto.Wallet;
import cttk.dto.Wallets;
import cttk.exception.CTTKException;
import cttk.exception.CXAuthorityException;
import cttk.exception.UnsupportedCXException;
import cttk.exception.UnsupportedCurrencyException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.util.ExNotRaisedException;
import cttk.request.GetBankAccountRequest;
import cttk.request.GetOpenUserOrdersRequest;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserAccountRequest;
import cttk.request.GetUserOrderHistoryRequest;
import cttk.request.GetUserOrderRequest;
import cttk.request.GetUserTradesRequest;
import cttk.test.TradeTestUtils;

abstract public class AbstractPrivateCXClientDataTest
    extends AbstractCXClientTest
{
    private static final Logger LOG = LoggerFactory.getLogger(AbstractPrivateCXClientDataTest.class);

    protected PublicCXClient pubClient;
    protected PrivateCXClient client;
    protected String invalidCurrency = "AASJDKLQWE";

    protected static final Map<AbstractPrivateCXClientDataTest, UserOrder> userOrderMap = new ConcurrentHashMap<>();

    @Before
    public void setup()
        throws CTTKException, IOException
    {
        org.junit.Assume.assumeTrue(hasTestProfile("test-data"));
        client = CXClientFactory.createPrivateCXClient(getCxId(), CXCredentialsProvider.getDefaultCredential(getCxId()).getDataKey());
        pubClient = CXClientFactory.createPublicCXClient(getCxId());

        initialize(this);
    }

    private synchronized static void initialize(
        AbstractPrivateCXClientDataTest test)
        throws CTTKException
    {
        // Do nothing if an order is already created for an exchange
        if (findUserOrder(test.getClass()) != null) {
            LOG.info("TEST with an existing order");
            return;
        } else {
            LOG.info("TEST with a new order");
        }

        // TODO is it really necessary to cancel existing orders? No for now.
        // cancelAllOpenOrders();

        // Create new order for data testing
        try {
            final UserOrder createdOrder = TradeTestUtils.createUnreachableBuyOrder(test.pubClient, test.client, test.getValidCurrencyPair(), test.getBuyTestQuantity());
            userOrderMap.put(test, createdOrder);

            test.threadSleep();

            // Add shutdown hook for canceling order 
            // in case if program is manually exited before completing all tests
            Runtime.getRuntime().addShutdownHook(new Thread() {
                public void run() {
                    LOG.info("ShutdownHook for [{}]", test.getClass());
                    cleanup(test.getClass());
                }
            });

        } catch (UnsupportedMethodException e) {
            LOG.error("[" + test.getCxId() + "] does not support createOrder yet, without creating order, we will continue with the testing");
        }
    }

    protected static synchronized <T extends AbstractPrivateCXClientDataTest> Map.Entry<AbstractPrivateCXClientDataTest, UserOrder> findUserOrder(Class<T> claz) {
        for (Map.Entry<AbstractPrivateCXClientDataTest, UserOrder> e : userOrderMap.entrySet()) {
            if (e.getKey().getClass().getName().equals(claz.getName())) return e;
        }
        return null;
    }

    protected static synchronized <T extends AbstractPrivateCXClientDataTest> void cleanup(Class<T> claz) {
        final Map.Entry<AbstractPrivateCXClientDataTest, UserOrder> entry = findUserOrder(claz);
        if (entry != null) {
            LOG.info("Clean up for [{}] ", claz);
            final AbstractPrivateCXClientDataTest test = entry.getKey();
            try {
                TradeTestUtils.testCancelOrder(test.client, entry.getValue());
            } catch (CTTKException e) {
                LOG.error("Failed to cancel order = " + entry.getValue());
            }
            userOrderMap.remove(test);
        } else {
            LOG.info("No order to clean up for [{}] ", claz);
        }
    }

    protected void cancelAllOpenOrders()
        throws UnsupportedCurrencyPairException, CTTKException
    {
        UserOrders orders = null;
        try {
            orders = client.getAllOpenUserOrders(false);
        } catch (UnsupportedMethodException e) {
            // This exchange needs to have currency pair to get open orders
            orders = client.getOpenUserOrders(new GetOpenUserOrdersRequest()
                .setCurrencyPair(getValidCurrencyPair()));
        }

        if (orders != null && orders.isNotEmpty()) {
            for (UserOrder openOrder : orders.getOrders()) {
                TradeTestUtils.testCancelOrder(client, openOrder);
            }
        }
    }

    protected UserOrder getTestUserOrder()
        throws UnsupportedCurrencyPairException, CTTKException
    {
        final UserOrders userOrders = client.getOpenUserOrders(new GetOpenUserOrdersRequest().setCurrencyPair(getCurrencyPair()));
        return userOrders == null || userOrders.isEmpty() ? null : userOrders.first();
    }

    @Test
    public void testWrongDataAPIKey()
        throws UnsupportedCXException, CTTKException, IOException
    {
        try {
            PrivateCXClient wrongKeyClient = CXClientFactory.createPrivateCXClient(getCxId(),
                this.getInvalidCredential(getCxId()).getDataKey());
            wrongKeyClient.getAllBalances();
        } catch (CXAuthorityException e) {
            assertEquals(this.getCxId(), e.getCxId());
            assertNotNull(e.getResponseStatusCode());
            assertNotNull(e.getExchangeMessage());
        }
    }

    @Test
    public void testGetUserAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();

        final UserAccount userAccount = client.getUserAccount(new GetUserAccountRequest());

        logger.debug("{}", userAccount);

        Assert.assertNotNull(userAccount);
        Assert.assertNotNull(userAccount.getAid());
    }

    @Test
    public void testGetTradingFee()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();

        final TradingFeeProvider tradingFeeProvider = client.getTradingFeeProvider();

        logger.debug("{}", getValidCurrencyPair());
        logger.debug("{}", tradingFeeProvider);

        Assert.assertNotNull(tradingFeeProvider);
        Assert.assertNotNull(tradingFeeProvider.getFee(getValidCurrencyPair()));
    }

    @Test
    public void testGetBankAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();

        final BankAccount bankAccount = client.getBankAccount(new GetBankAccountRequest());
        logger.debug("{}", bankAccount);

        Assert.assertNotNull(bankAccount);
        Assert.assertNotNull(bankAccount.getAccountNo());
    }

    @Test
    public void testGetWallet()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();

        final String currency = getTransferTestCurrency();
        final Wallet wallet = client.getWallet(currency);

        logger.debug("{}", wallet);

        Assert.assertNotNull(wallet);
        Assert.assertNotNull(wallet.getAddress());
        Assert.assertEquals(currency, wallet.getCurrency());
    }

    @Test
    public void testGetAllWallets()
        throws CTTKException
    {
        threadSleep();

        final Wallets wallets = client.getAllWallets();

        logger.debug("{}", wallets);

        Assert.assertNotNull(wallets);
        Assert.assertTrue(wallets.size() > 0);
    }

    @Test
    public void testGetAllBalances()
        throws CTTKException
    {
        threadSleep();

        final Balances balances = client.getAllBalances();

        logger.debug("{}", balances);

        Assert.assertNotNull(balances);
        Assert.assertNotNull(balances.getCxId());
        Assert.assertTrue(balances.size() > 0);
        for (Balance b : balances.getBalances()) {
            Assert.assertNotNull(b.getCurrency());
            Assert.assertTrue(b.getCurrency().matches("[A-Z0-9]+"));
            Assert.assertNotNull(b.getTotal());
        }
    }

    @Test
    public void testGetUserOrder()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();

        final UserOrder testUserOrder = this.getTestUserOrder();
        Assert.assertNotNull(testUserOrder);

        final GetUserOrderRequest request = GetUserOrderRequest.from(testUserOrder);

        final UserOrder userOrder = client.getUserOrder(request);

        logger.debug("{}", userOrder);

        Assert.assertNotNull(userOrder);
        Assert.assertNotNull(userOrder.getOid());
        Assert.assertNotNull(userOrder.getPricingType());
        if (!getCxId().equalsIgnoreCase(CXIds.POLONIEX)) Assert.assertNotNull(userOrder.getSide());
        Assert.assertNotNull(userOrder.getPrice());
        Assert.assertNotNull(userOrder.getQuantity());
        Assert.assertNotNull(userOrder.getTotal());
        Assert.assertEquals(toUpper(testUserOrder.getOid()), toUpper(userOrder.getOid()));
    }

    @Test
    public void testGetUserOrderWrongID()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();
        String nonExistID = "999999999";
        final GetUserOrderRequest request = new GetUserOrderRequest()
            .setCurrencyPair(getValidCurrencyPair())
            .setOid(nonExistID)
            .setClientOrderId(nonExistID);

        final UserOrder userOrder = client.getUserOrder(request);

        logger.debug("{}", userOrder);

        // All CTTK CX should return null for non exist user order
        Assert.assertNull(userOrder);
    }

    @Test
    public void testGetAllOpenUserOrders()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();
        final UserOrders userOrders = client.getAllOpenUserOrders(true);

        logger.debug("{}", userOrders);

        Assert.assertNotNull(userOrders);
        Assert.assertTrue(userOrders.isNotEmpty());
        for (UserOrder userOrder : userOrders.getOrders()) {
            Assert.assertNotNull(userOrder);
            Assert.assertNotNull(userOrder.getOid());
            Assert.assertNotNull(userOrder.getSide());
            Assert.assertNotNull(userOrder.getPrice());
            Assert.assertNotNull(userOrder.getQuantity());
            Assert.assertNotNull(userOrder.getTotal());
            Assert.assertNotNull(userOrder.getStatusType());
            Assert.assertNotNull(userOrder.getPricingType());
        }
    }

    @Test
    public void testGetOpenUserOrdersWrongCurrency()
        throws CTTKException
    {
        threadSleep();

        final GetOpenUserOrdersRequest request = new GetOpenUserOrdersRequest()
            .setCurrencyPair(getInvalidCurrencyPair());
        try {
            client.getOpenUserOrders(request);
        } catch (UnsupportedCurrencyPairException e) {
            assertEquals(e.getBaseCurrency(), getInvalidCurrencyPair().getBaseCurrency());
            assertEquals(e.getQuoteCurrency(), getInvalidCurrencyPair().getQuoteCurrency());
            assertEquals(e.getCxId(), this.getCxId());
            return;
        }
        throw new ExNotRaisedException("Failed to raise correct exception");

    }

    @Test
    public void testGetOpenUserOrders()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();

        final GetOpenUserOrdersRequest request = new GetOpenUserOrdersRequest()
            .setCurrencyPair(getValidCurrencyPair());
        final UserOrders userOrders = client.getOpenUserOrders(request);

        logger.debug("{}", userOrders);

        Assert.assertNotNull(userOrders);
        Assert.assertTrue(userOrders.isNotEmpty());
        for (UserOrder userOrder : userOrders.getOrders()) {
            Assert.assertNotNull(userOrder);
            Assert.assertEquals(request.getBaseCurrency(), userOrder.getBaseCurrency());
            Assert.assertEquals(request.getQuoteCurrency(), userOrder.getQuoteCurrency());
            Assert.assertNotNull(userOrder.getOid());
            Assert.assertNotNull(userOrder.getSide());
            Assert.assertNotNull(userOrder.getPricingType());
            Assert.assertNotNull(userOrder.getPrice());
            Assert.assertNotNull(userOrder.getQuantity());
            Assert.assertNotNull(userOrder.getTotal());
        }
    }

    @Test
    public void testGetUserOrderHistoryWrongCurrency()
        throws CTTKException
    {
        threadSleep();

        final GetUserOrderHistoryRequest request = new GetUserOrderHistoryRequest()
            .setCurrencyPair(getInvalidCurrencyPair());
        try {
            client.getUserOrderHistory(request);
        } catch (UnsupportedCurrencyPairException e) {
            assertEquals(e.getBaseCurrency(), getInvalidCurrencyPair().getBaseCurrency());
            assertEquals(e.getQuoteCurrency(), getInvalidCurrencyPair().getQuoteCurrency());
            assertEquals(e.getCxId(), this.getCxId());
            return;
        }
        throw new ExNotRaisedException("Failed to raise correct exception");
    }

    @Test
    public void testGetUserOrderHistory()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();

        final GetUserOrderHistoryRequest request = new GetUserOrderHistoryRequest()
            .setCurrencyPair(getValidCurrencyPair());
        final UserOrders userOrders = client.getUserOrderHistory(request);

        logger.debug("{}", userOrders);

        Assert.assertNotNull(userOrders);
        Assert.assertNotNull(userOrders.getCategory());
        Assert.assertNotNull(userOrders.getOrders());
        Assert.assertTrue(userOrders.isNotEmpty());
        for (UserOrder userOrder : userOrders.getOrders()) {
            Assert.assertNotNull(userOrder);
            if (getCxId().equals(CXIds.HITBTC) && getCxId().equals(CXIds.BITFINEX)) {
                Assert.assertEquals(request.getBaseCurrency(), userOrder.getBaseCurrency());
                Assert.assertEquals(request.getQuoteCurrency(), userOrder.getQuoteCurrency());
            }
            Assert.assertNotNull(userOrder.getOid());
            Assert.assertNotNull(userOrder.getSide());
            Assert.assertNotNull(userOrder.getPricingType());
            Assert.assertNotNull(userOrder.getPrice());
            Assert.assertNotNull(userOrder.getQuantity());
            Assert.assertNotNull(userOrder.getTotal());
        }

        if (getCxId().equals(CXIds.BITFINEX)) {
            Assert.assertEquals(userOrders.getCategory(), UserOrders.Category.CLOSED);
        }
    }

    @Test
    public void testGetUserTradeWrongCurrency()
        throws CTTKException
    {
        threadSleep();

        final GetUserTradesRequest request = new GetUserTradesRequest()
            .setCurrencyPair(getInvalidCurrencyPair());
        try {
            client.getUserTrades(request);
        } catch (UnsupportedCurrencyPairException e) {
            assertEquals(e.getBaseCurrency(), getInvalidCurrencyPair().getBaseCurrency());
            assertEquals(e.getQuoteCurrency(), getInvalidCurrencyPair().getQuoteCurrency());
            assertEquals(e.getCxId(), this.getCxId());
            return;
        }
        throw new ExNotRaisedException("Failed to raise correct exception");
    }

    @Test
    public void testGetUserTrades()
        throws UnsupportedCurrencyPairException, CTTKException
    {
        threadSleep();

        // Some CXs such as Huobi, Okex provide very limited trade histories of 2~7 days.
        // For now, we set startDateTime to fairly long but, it is not a complete solution.
        final GetUserTradesRequest request = new GetUserTradesRequest()
            .setCurrencyPair(getValidCurrencyPair())
            .setBy("timestamp") // for Hitbtc
            .setStartDateTime(ZonedDateTime.now().minusYears(1));
        final UserTrades userTrades = client.getUserTrades(request);

        logger.debug("{}", userTrades);

        Assert.assertNotNull(userTrades);
        Assert.assertTrue(userTrades.isNotEmpty());
        for (UserTrade userTrade : userTrades.getTrades()) {
            if (!getCxId().equals(CXIds.BITHUMB)) {
                Assert.assertNotNull(userTrade.getOid());
            }
            Assert.assertEquals(request.getBaseCurrency(), userTrade.getBaseCurrency());
            Assert.assertEquals(request.getQuoteCurrency(), userTrade.getQuoteCurrency());
            Assert.assertNotNull(userTrade.getSide());
            Assert.assertNotNull(userTrade.getPrice());
            Assert.assertNotNull(userTrade.getQuantity());
            Assert.assertNotNull(userTrade.getTotal());

            if (!getCxId().equals(CXIds.OKEX)) {
                Assert.assertNotNull(userTrade.getFee());
                Assert.assertNotNull(userTrade.getFeeCurrency());
            }
        }
    }

    @Test
    public void testGetTransfersWrongCurrency()
        throws CTTKException
    {
        threadSleep();

        final GetTransfersRequest request = new GetTransfersRequest()
            .setCurrency(invalidCurrency);
        try {
            client.getTransfers(request);
        } catch (UnsupportedCurrencyException e) {
            assertEquals(e.getCurrency(), invalidCurrency);
            assertEquals(e.getCxId(), this.getCxId());
            return;
        }
        throw new ExNotRaisedException("Failed to raise correct exception");
    }

    @Test
    public void testGetTransfers()
        throws UnsupportedCurrencyPairException, CTTKException
    {
        threadSleep();

        final GetTransfersRequest request = new GetTransfersRequest()
            .setCurrency(getTransferTestCurrency());
        final Transfers transfers = client.getTransfers(request);

        logger.debug("{}", transfers);

        Assert.assertNotNull(transfers);
        Assert.assertTrue(transfers.isNotEmpty());
        for (Transfer transfer : transfers.getTransfers()) {
            Assert.assertNotNull(transfer);
            Assert.assertNotNull(transfer.getCurrency());
            Assert.assertNotNull(transfer.getType());
            Assert.assertNotNull(transfer.getTid());

            // do not check txId for Bithumb and Kraken
            if (!transfers.getCxId().equals(CXIds.BITHUMB)
                && !transfers.getCxId().equals(CXIds.KRAKEN)
                && transfer.getType().equals(TransferType.WITHDRAWAL)
                && transfer.getStatusType().equals(TransferStatus.SUCCESS))
            {
                System.out.println(transfer);
                // there is no transaction id in offchain transaction of Bitfinex
                if (!transfers.getCxId().equals(CXIds.BITFINEX)
                    || !transfer.getDescription().contains("offchain"))
                {
                    Assert.assertNotNull(transfer.getTxId());
                }
            }
            Assert.assertNotNull(transfer.getAmount());
            if (!transfers.getCxId().equals(CXIds.BITHUMB)
                && !transfers.getCxId().equals(CXIds.KRAKEN)
                && !transfers.getCxId().equals(CXIds.UPBIT)
                && transfer.getType().equals(TransferType.WITHDRAWAL))
            {
                // do not check address for Bithumb, Kraken and Upbit
                Assert.assertNotNull(transfer.getAddress());
            }
            if (!transfers.getCxId().equals(CXIds.COINONE)
                && !transfers.getCxId().equals(CXIds.KRAKEN))
            {
                // do not check any transfer status for Coinone and Kraken
                Assert.assertNotNull(transfer.getStatus());
                Assert.assertNotNull(transfer.getStatusType());
            }
        }
    }
}
