package cttk.impl.okex;

import static cttk.CXIds.OKEX;
import static cttk.OrderSide.BUY;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

import java.math.BigDecimal;
import java.util.List;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.TransferStatus;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;
import cttk.exception.CTTKException;
import cttk.impl.util.CryptoUtils;
import cttk.impl.util.HttpMethod;

public class OkexUtilsV3 {
    public static String toOkexCurrencySymbolV3(String currency) {
        return currency == null ? null : currency.trim().toUpperCase();
    }

    public static String toOkexMarketSymbolV3(CurrencyPair currencyPair) {
        if (currencyPair == null) return null;
        final String base = toOkexCurrencySymbolV3(currencyPair.getBaseCurrency());
        final String quote = toOkexCurrencySymbolV3(currencyPair.getQuoteCurrency());
        return base + "-" + quote;
    }

    public static String toOkexOrderStatusV3(OrderStatus status)
        throws CTTKException
    {
        if (status == null)
            return "all";
        if (status == OrderStatus.FILLED)
            return "filled";
        if (status == OrderStatus.CANCELED)
            return "cancelled";
        else if (status == OrderStatus.UNFILLED)
            return "open|ordering|cancelling|part_filled";
        else
            throw new CTTKException(format("The status '%s' is not supported.", status)).setCxId(OKEX);
    }

    public static String toOkexOrderSideV3(OrderSide orderSide) {
        if (orderSide == null) return null;
        switch (orderSide) {
            case BUY:
                return "buy";
            case SELL:
                return "sell";
            default:
                return null;
        }
    }

    /**
     * 'notional', 'size' corresponds to BUY and SELL respectively.
     */
    public static String toOkexMarketOrderQuantityV3(OrderSide orderSide) {
        return orderSide == BUY ? "notional" : "size";
    }

    public static String toStdCurrencySymbolV3(String currency) {
        return currency == null ? null : currency.trim().toUpperCase();
    }

    public static CurrencyPair toStdMarketSymbolV3(String symbol) {
        if (symbol == null) return null;
        final String[] t = symbol.split("-");
        return CurrencyPair.of(toOkexCurrencySymbolV3(t[0]), toOkexCurrencySymbolV3(t[1]));
    }

    public static OrderStatus toStdOrderStatusV3(final String status) {
        switch (status) {
            case "all":
            case "ordering":
            case "open":
                return OrderStatus.UNFILLED;
            case "part_filled":
                return OrderStatus.PARTIALLY_FILLED;
            case "filled":
                return OrderStatus.FILLED;
            case "canceling":
            case "cancelled":
                return OrderStatus.CANCELED;
            default:
                return null;
        }
    }

    public static TransferStatus toStdWithdrawalTransferStatusTypeV3(int status) {
        switch (status) {
            case -3: // pending cancel
            case -2: // cancelled
                return TransferStatus.CANCELED;
            case -1: // failed
                return TransferStatus.FAILED;
            case 0: // pending
            case 1: // sending
            case 3: // email confirmation
            case 4: // manual confirmation
            case 5: // awaiting identity confirmation
                return TransferStatus.PENDING;
            case 2: // sent
                return TransferStatus.SUCCESS;
        }
        return null;
    }

    public static TransferStatus toStdDepositTransferStatusTypeV3(int status) {
        switch (status) {
            case 0: // waiting for confirmation
            case 1: // confirmation account
                return TransferStatus.PENDING;
            case 2: // recharge success
                return TransferStatus.SUCCESS;
        }
        return null;
    }

    public static boolean isMakerV3(final String execType) {
        return execType.equals("M");
    }

    /**
     * @see OkexUtilsV3#createSignV3(String, HttpMethod, String, String, String)
     */
    public static String createSignV3(String timestamp, HttpMethod method, String requestPath, String secretKey) {
        return createSignV3(timestamp, method, requestPath, "", secretKey);
    }

    /**
     * Create Okex v3 sign.
     *
     * @param timestamp REST: Pass timestamp as ISO 8601 format. ex) 2018-12-30T15:13:22Z
     *                  WebSocket: Pass timestamp as UNIX seconds(not millis). ex) 1544165306
     * @param requestPath Query string should be appended and starts with '/api'.
     *                    ex)/api/spot/v3/fills?order_id=100&nbsp;instrument_id=EOS-ETH
     * @param body Json string. "" if there is no data.
     *             ex) {"product_id":"BTC-USD","order_id":"100"}
     * @see "https://www.okex.com/docs/en/#summary-yan-zheng"
     */
    public static String createSignV3(String timestamp, HttpMethod method, String requestPath, String body, String secretKey) {
        final String prehash = timestamp + method + requestPath + body;
        return CryptoUtils.hmacSha256Base64(prehash, secretKey);
    }

    /**
     * Request path means a string starts with '/api', also query string included.
     *
     * ex) https://www.okex.com/api/spot/v3/orders?limit=100: /api/spot/v3/orders?limit=100
     */
    public static String extractRequestPathV3(String url) {
        return url.substring(url.indexOf("/api"));
    }

    /**
     * Bid trade data is provided on buy side. Ask trade data is provided on ask side.
     * However fee is provided on buy side always. Collect separately them and fill fees finally.
     */
    public static UserTrades refine(UserTrades userTrades, OrderSide orderSide) {
        UserTrades refinedUserTrades = new UserTrades();

        if (userTrades.isNotEmpty()) {
            // Filter by orderSide
            final List<UserTrade> filteredUserTrades = userTrades
                .getList()
                .stream()
                .filter(i -> i.getSide() == orderSide)
                .collect(toList());

            // Collect fees
            final List<BigDecimal> allFees = userTrades
                .getList()
                .stream()
                .filter(i -> i.getSide() == BUY)
                .map(UserTrade::getFee)
                .collect(toList());

            // Fill fees
            for (int i = 0; i < filteredUserTrades.size(); i++)
                filteredUserTrades.get(i).setFee(allFees.get(i));

            refinedUserTrades.setTrades(filteredUserTrades);
        }

        return refinedUserTrades;
    }
}
