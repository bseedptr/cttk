package cttk;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cttk.util.StringUtils;
import lombok.Data;

public interface CurrencyPair {

    public static final String DELIMITER = "-";

    String getBaseCurrency();

    String getQuoteCurrency();

    default boolean matched(CurrencyPair other) {
        return StringUtils.equals(getBaseCurrency(), other.getBaseCurrency())
            && StringUtils.equals(getQuoteCurrency(), other.getQuoteCurrency());
    }

    @JsonIgnore
    default boolean isBothNull() {
        return getBaseCurrency() == null && getQuoteCurrency() == null;
    }

    @JsonIgnore
    default boolean isEitherIsNull() {
        return getBaseCurrency() == null || getQuoteCurrency() == null;
    }

    @JsonIgnore
    default String getPairString() {
        return getPairString(getBaseCurrency(), getQuoteCurrency());
    }

    default CurrencyPair toSimpleCurrencyPair() {
        return CurrencyPair.of(getBaseCurrency(), getQuoteCurrency());
    }

    public static String getPairString(String base, String quote) {
        return String.format("%s%s%s", base, DELIMITER, quote);
    }

    public static String getPairString(CurrencyPair pair) {
        return pair == null ? null : getPairString(pair.getBaseCurrency(), pair.getQuoteCurrency());
    }

    public static CurrencyPair of(String baseCurrency, String quoteCurrency) {
        return new SimpleCurrencyPair(baseCurrency, quoteCurrency);
    }

    public static CurrencyPair toUpper(CurrencyPair currencyPair) {
        return currencyPair == null ? null
            : of(StringUtils.toUpper(currencyPair.getBaseCurrency()), StringUtils.toUpper(currencyPair.getQuoteCurrency()));
    }

    public static CurrencyPair parseMarketSymbol(String symbol) {
        if (symbol == null || symbol.isEmpty()) return CurrencyPair.of(null, null);
        String[] arr = symbol.split(DELIMITER);
        return CurrencyPair.of(arr[0], arr[1]);
    }

    @Data
    static class SimpleCurrencyPair
        implements CurrencyPair
    {
        String baseCurrency;
        String quoteCurrency;

        public SimpleCurrencyPair() {
            super();
        }

        public SimpleCurrencyPair(String baseCurrency, String quoteCurrency) {
            this.baseCurrency = baseCurrency;
            this.quoteCurrency = quoteCurrency;
        }
    }
}
