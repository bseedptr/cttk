package cttk.dto;

import java.util.List;

import cttk.AbstractCXIdHolder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class OrderBooks
    extends AbstractCXIdHolder<OrderBooks>
{
    List<OrderBook> orderBooks;

    public int size() {
        return orderBooks == null ? 0 : orderBooks.size();
    }
}
