package cttk.exception;

import cttk.CurrencyGettable;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class UnsupportedCurrencyException
    extends CXAPIRequestException
    implements CurrencyGettable
{
    private static final long serialVersionUID = 1L;

    protected String currency;

    public UnsupportedCurrencyException() {
        super();
    }

    public UnsupportedCurrencyException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsupportedCurrencyException(String message) {
        super(message);
    }

    public UnsupportedCurrencyException(Throwable cause) {
        super(cause);
    }

    @Override
    public String toString() {
        return super.toString()
            + ", Currency = " + currency;
    }

    @Override
    public UnsupportedCurrencyException setCxId(String cxId) {
        super.setCxId(cxId);
        return this;
    }

    @Override
    public UnsupportedCurrencyException setResponseStatusCode(Integer responseStatusCode) {
        super.setResponseStatusCode(responseStatusCode);
        return this;
    }

    @Override
    public UnsupportedCurrencyException setExchangeMessage(String exchangeMessage) {
        super.setExchangeMessage(exchangeMessage);
        return this;
    }
}
