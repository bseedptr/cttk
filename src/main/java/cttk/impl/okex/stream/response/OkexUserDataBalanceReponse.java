package cttk.impl.okex.stream.response;

import static cttk.impl.okex.OkexUtils.toStdCurrencySymbol;
import static cttk.impl.util.MathUtils.add;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Map.Entry;

import cttk.dto.Balance;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class OkexUserDataBalanceReponse
    extends OkexUserDataResponse<OkexUserDataBalance>
{
    public Balance toBalance() {
        Entry<String, BigDecimal> free = super.data.info.free.entrySet().stream().findFirst().get();
        Entry<String, BigDecimal> freezed = super.data.info.freezed.entrySet().stream().findFirst().get();
        return new Balance()
            .setCurrency(toStdCurrencySymbol(free.getKey()))
            .setTotal(add(free.getValue(), freezed.getValue()))
            .setAvailable(free.getValue())
            .setInUse(freezed.getValue());
    }
}

@Data
class OkexUserDataBalance {
    Info info;

    @Data
    class Info {
        Map<String, BigDecimal> free; //ex) "btc" : 5814.8
        Map<String, BigDecimal> freezed; //ex) "btc" : 0
    }
}
