package cttk.impl.upbit;

import cttk.CXIds;
import cttk.impl.AbstractPollingCXStreamTest;

public class UpbitPollingStreamTest
    extends AbstractPollingCXStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.UPBIT;
    }
}
