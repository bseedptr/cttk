package cttk;

import static cttk.CXIds.BIBOX;
import static cttk.CXIds.BINANCE;
import static cttk.CXIds.BITFINEX;
import static cttk.CXIds.BITHUMB;
import static cttk.CXIds.BITTREX;
import static cttk.CXIds.COINBASE;
import static cttk.CXIds.COINONE;
import static cttk.CXIds.HITBTC;
import static cttk.CXIds.HUOBI;
import static cttk.CXIds.KORBIT;
import static cttk.CXIds.KRAKEN;
import static cttk.CXIds.OKEX;
import static cttk.CXIds.POLONIEX;
import static cttk.CXIds.UPBIT;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isOneOf;
import static org.junit.Assert.assertThat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCXException;
import cttk.impl.chargingpolicy.AllChargingPolicyProvider;
import cttk.impl.chargingpolicy.ByQuoteChargingPolicyProvider;
import cttk.impl.chargingpolicy.CXTokenPayableChargingPolicyProvider;
import cttk.impl.chargingpolicy.DefaultChargingPolicyProvider;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ChargingPolicyProviderFactoryTest {
    @Rule
    public ExpectedException ee = ExpectedException.none();

    @Test
    public void testCreateWhenCXIdIsNull()
        throws CTTKException
    {
        ee.expect(UnsupportedCXException.class);

        ChargingPolicyProviderFactory.create(null);
    }

    @Test
    public void testCreate()
        throws CTTKException
    {
        for (String cxId : CTTK.getSupportedCxIds()) {
            try {
                ChargingPolicyProvider provider = ChargingPolicyProviderFactory.create(cxId);
                if (cxId.equals(BIBOX))
                    assertBibox(provider);
                else if (cxId.equals(BINANCE))
                    assertBinance(provider);
                else if (cxId.equals(HITBTC) || cxId.equals(UPBIT))
                    assertThat(provider, instanceOf(ByQuoteChargingPolicyProvider.class));
                else if (cxId.equals(KRAKEN))
                    assertThat(provider, instanceOf(AllChargingPolicyProvider.class));
                else if (isUsingDefaultPolicy(cxId))
                    assertThat(provider, instanceOf(DefaultChargingPolicyProvider.class));
            } catch (UnsupportedCXException e) {
                assertUnsupportedCX(cxId);
            }
        }
    }

    private boolean isUsingDefaultPolicy(String cxId) {
        return cxId.equals(BITFINEX) || cxId.equals(BITHUMB) || cxId.equals(COINONE) ||
            cxId.equals(HUOBI) || cxId.equals(KORBIT) || cxId.equals(OKEX) || cxId.equals(POLONIEX);
    }

    private void assertBibox(ChargingPolicyProvider provider) {
        assertThat(provider, instanceOf(CXTokenPayableChargingPolicyProvider.class));
        assertThat(((CXTokenPayableChargingPolicyProvider) provider).getCxToken(), is("BIX"));
    }

    private void assertBinance(ChargingPolicyProvider provider) {
        assertThat(provider, instanceOf(CXTokenPayableChargingPolicyProvider.class));
        assertThat(((CXTokenPayableChargingPolicyProvider) provider).getCxToken(), is("BNB"));
    }

    private void assertUnsupportedCX(String cxId) {
        assertThat(String.format("'%s' is not specified explicitly on ChargingPolicyProviderFactory.", cxId), cxId, isOneOf(BITTREX, COINBASE));
        log.debug("{} is not supported for now.", cxId);
    }
}