package cttk.impl;

import static cttk.test.TestUtils.hasTestProfile;

import org.junit.Before;

import cttk.CXClientFactory;
import cttk.exception.CTTKException;

public abstract class AbstractPollingCXStreamTest
    extends AbstractCXStreamTest
{
    @Before
    @Override
    public void setup()
        throws CTTKException
    {
        org.junit.Assume.assumeFalse(hasTestProfile("skip-public"));
        streamOpener = CXClientFactory.createPollingCXStreamOpener(getCxId());
    }
}
