package cttk.request;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cttk.AbstractCurrencyPair;
import cttk.OrderSide;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class CreateMarketPriceOrderRequest
    extends AbstractCurrencyPair<CreateMarketPriceOrderRequest>
{
    OrderSide side;
    BigDecimal quantity;

    @JsonIgnore
    public boolean isSellSide() {
        return side == OrderSide.SELL;
    }

    @JsonIgnore
    public boolean isBuySide() {
        return side == OrderSide.BUY;
    }

    public CreateMarketPriceOrderRequest setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
        return this;
    }

    public CreateMarketPriceOrderRequest setQuantity(String quantity) {
        this.quantity = new BigDecimal(quantity);
        return this;
    }
}
