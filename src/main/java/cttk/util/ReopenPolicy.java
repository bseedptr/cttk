package cttk.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class ReopenPolicy {

    public static final int DEFAULT_MAX_RETRIES = 3;
    public static final long DEFAULT_BACKOFF_IN_MILLIS = 500l;

    private int maxRetries = DEFAULT_MAX_RETRIES;
    private long backoffInMilli = DEFAULT_BACKOFF_IN_MILLIS;
    private boolean reopenOnClosed = false;

    public static ReopenPolicy createDefault() {
        return new ReopenPolicy(DEFAULT_MAX_RETRIES, DEFAULT_BACKOFF_IN_MILLIS, false);
    }

    public static ReopenPolicy createDefaultIfNull(ReopenPolicy policy) {
        return policy != null ? policy : createDefault();
    }
}
