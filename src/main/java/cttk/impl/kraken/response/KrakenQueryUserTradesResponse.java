package cttk.impl.kraken.response;

import java.util.HashMap;
import java.util.Map;

import cttk.dto.UserTrade;

public class KrakenQueryUserTradesResponse
    extends KrakenResponse<Map<String, KrakenUserTrade>>
{
    public Map<String, UserTrade> toUserTradeMap() {
        if (result == null) return null;
        final Map<String, UserTrade> ret = new HashMap<>();
        result.forEach((k, v) -> {
            UserTrade ut = v.toUserTrade()
                .setTid(k);
            ret.put(k, ut);
        });
        return ret;
    }
}
