package cttk.impl.bibox.stream;

import com.fasterxml.jackson.databind.ObjectMapper;

import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Trades;
import cttk.impl.bibox.stream.response.BiboxWebsocketOrderBookResponse;
import cttk.impl.bibox.stream.response.BiboxWebsocketTickerResponse;
import cttk.impl.bibox.stream.response.BiboxWebsocketTradesResponse;
import cttk.impl.stream.MessageConverter;
import cttk.impl.util.JsonUtils;

public class BiboxWebsocketMessageConverterFactory {

    public static MessageConverter<Ticker> createTickerMessageConverter() {
        return new MessageConverter<Ticker>() {
            private final ObjectMapper mapper = JsonUtils.createDefaultObjectMapper();

            @Override
            public Ticker convert(String message)
                throws Exception
            {
                return mapper.readValue(message, BiboxWebsocketTickerResponse.class).toTicker();
            }
        };
    }

    public static MessageConverter<OrderBook> createOrderBookMessageConverter() {
        return new MessageConverter<OrderBook>() {
            private final ObjectMapper mapper = JsonUtils.createDefaultObjectMapper();

            @Override
            public OrderBook convert(String message)
                throws Exception
            {
                return mapper.readValue(message, BiboxWebsocketOrderBookResponse.class).toOrderBook();
            }
        };
    }

    public static MessageConverter<Trades> createTradesMessageConverter() {
        return new MessageConverter<Trades>() {
            private final ObjectMapper mapper = JsonUtils.createDefaultObjectMapper();

            @Override
            public Trades convert(String message)
                throws Exception
            {
                return mapper.readValue(message, BiboxWebsocketTradesResponse.class).toTrades();
            }
        };
    }
}
