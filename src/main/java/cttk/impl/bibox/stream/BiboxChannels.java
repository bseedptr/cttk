package cttk.impl.bibox.stream;

import cttk.CurrencyPair;

import static cttk.impl.bibox.BiboxUtils.getMarketSymbol;
import static cttk.util.StringUtils.toUpper;

public class BiboxChannels {
    public static final String getTickerChannel(CurrencyPair currencyPair) { return String.format("bibox_sub_spot_%s_ticker", getMarketSymbol(currencyPair)); }

    public static final String getOrderBookChannel(CurrencyPair currencyPair) { return String.format("bibox_sub_spot_%s_depth", getMarketSymbol(currencyPair)); }

    public static final String getTradeChannel(CurrencyPair currencyPair) { return String.format("bibox_sub_spot_%s_deals", getMarketSymbol(currencyPair)); }

    public static final String getUserDataChannel() { return "bibox_sub_spot_ALL_ALL_login"; }
}
