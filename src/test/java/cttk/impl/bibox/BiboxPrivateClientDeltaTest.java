package cttk.impl.bibox;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.powermock.api.support.membermodification.MemberMatcher.method;

import java.time.ZonedDateTime;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberModifier;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import cttk.CXClientFactory;
import cttk.PublicCXClient;
import cttk.auth.Credential;
import cttk.dto.Transfers;
import cttk.dto.UserAccount;
import cttk.dto.UserOrders;
import cttk.dto.UserTrades;
import cttk.exception.CTTKException;
import cttk.impl.AbstractPrivateClientDeltaTest;
import cttk.request.GetTransfersDeltaRequest;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserOrderHistoryDeltaRequest;
import cttk.request.GetUserOrderHistoryRequest;
import cttk.request.GetUserTradesDeltaRequest;
import cttk.request.GetUserTradesRequest;
import lombok.extern.slf4j.Slf4j;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ CXClientFactory.class, BiboxPrivateClient.class })
@PowerMockIgnore("javax.net.ssl.*")
@Slf4j
public class BiboxPrivateClientDeltaTest
    extends AbstractPrivateClientDeltaTest
{
    BiboxPrivateClient client;
    GetUserOrderHistoryDeltaRequest orderHistoryRequest;
    GetUserTradesDeltaRequest tradesRequest;
    GetTransfersDeltaRequest transferRequest;

    @Mock
    UserOrders orders1, orders2, orders3;
    @Mock
    UserTrades trades1, trades2, trades3;
    @Mock
    Transfers transfers1, transfers2, transfers3;

    @Before
    public void setup()
        throws Exception
    {
        client = PowerMockito.spy(createDummyPrivateClient());
        orderHistoryRequest = new GetUserOrderHistoryDeltaRequest().setStartDateTime(ZonedDateTime.now().minusYears(2));
        tradesRequest = new GetUserTradesDeltaRequest().setStartDateTime(ZonedDateTime.now().minusYears(2));
        transferRequest = new GetTransfersDeltaRequest().setStartDateTime(ZonedDateTime.now().minusYears(2));
    }

    BiboxPrivateClient createDummyPrivateClient()
        throws CTTKException
    {
        PowerMockito.mockStatic(CXClientFactory.class);
        PowerMockito.when(CXClientFactory.createPublicCXClient(anyString())).thenReturn(mock(PublicCXClient.class));
        MemberModifier.stub(method(BiboxPrivateClient.class, "getUserAccount")).toReturn(mock(UserAccount.class));
        return new BiboxPrivateClient(mock(Credential.class));
    }

    @Test
    public void testGetUserOrderHistoryDeltaWhenNoMoreData()
        throws Exception
    {
        when(orders1.getList()).thenReturn(makeUserOrderFixtures("1", "2", "3"));
        when(orders1.isEmpty()).thenReturn(false);
        when(orders2.getList()).thenReturn(makeUserOrderFixtures());
        when(orders2.isEmpty()).thenReturn(true);
        doReturn(orders1, orders2).when(client).getUserOrderHistory(any(GetUserOrderHistoryRequest.class));

        UserOrders result = client.getUserOrderHistory(orderHistoryRequest);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat(ids, containsInAnyOrder("1", "2", "3"));
        verify(orders1, never()).filterByDateTimeGTE(orderHistoryRequest.getStartDateTime());
        verify(orders2, never()).filterByDateTimeGTE(orderHistoryRequest.getStartDateTime());
    }

    @Test
    public void testGetUserOrderHistoryDeltaWhenHasOlderOne()
        throws Exception
    {
        when(orders1.getList()).thenReturn(makeUserOrderFixtures("1", "2", "3"));
        when(orders1.hasOlderOne(any())).thenReturn(false);
        when(orders2.getList()).thenReturn(makeUserOrderFixtures("4"));
        when(orders2.hasOlderOne(any())).thenReturn(true);
        when(orders3.getList()).thenReturn(makeUserOrderFixtures("5", "6", "7"));
        doReturn(orders1, orders2, orders3).when(client).getUserOrderHistory(any(GetUserOrderHistoryRequest.class));

        UserOrders result = client.getUserOrderHistory(orderHistoryRequest);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat(ids, containsInAnyOrder("1", "2", "3", "4"));
        verify(orders2).filterByDateTimeGTE(orderHistoryRequest.getStartDateTime());
        verifyZeroInteractions(orders3);
    }

    @Test
    public void testGetUserTradesDeltaWhenNoMoreData()
        throws Exception
    {
        when(trades1.getList()).thenReturn(makeUserTradeFixtures("1", "2", "3"));
        when(trades1.isEmpty()).thenReturn(false);
        when(trades2.getList()).thenReturn(makeUserTradeFixtures());
        when(trades2.isEmpty()).thenReturn(true);
        doReturn(trades1, trades2).when(client).getUserTrades(any(GetUserTradesRequest.class));

        UserTrades result = client.getUserTrades(tradesRequest);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat(ids, containsInAnyOrder("1", "2", "3"));
        verify(trades1, never()).filterByDateTimeGTE(tradesRequest.getStartDateTime());
        verify(trades2, never()).filterByDateTimeGTE(tradesRequest.getStartDateTime());
    }

    @Test
    public void testGetUserTradesDeltaWhenHasOlderOne()
        throws Exception
    {
        when(trades1.getList()).thenReturn(makeUserTradeFixtures("1", "2", "3"));
        when(trades1.hasOlderOne(any())).thenReturn(false);
        when(trades2.getList()).thenReturn(makeUserTradeFixtures("4"));
        when(trades2.hasOlderOne(any())).thenReturn(true);
        when(trades3.getList()).thenReturn(makeUserTradeFixtures("5", "6", "7"));
        doReturn(trades1, trades2, trades3).when(client).getUserTrades(any(GetUserTradesRequest.class));

        UserTrades result = client.getUserTrades(tradesRequest);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat(ids, containsInAnyOrder("1", "2", "3", "4"));
        verify(trades1, never()).filterByDateTimeGTE(tradesRequest.getStartDateTime());
        verify(trades2).filterByDateTimeGTE(tradesRequest.getStartDateTime());
        verifyZeroInteractions(trades3);
    }

    @Test
    public void testGetTransfersDeltaWhenNoMoreData()
        throws Exception
    {
        when(transfers1.getList()).thenReturn(makeTransferFixtures("1", "2", "3"));
        when(transfers1.isEmpty()).thenReturn(false);
        when(transfers2.getList()).thenReturn(makeTransferFixtures());
        when(transfers2.isEmpty()).thenReturn(true);
        PowerMockito.doReturn(transfers1, transfers2).when(client, "getWithdraws", any(GetTransfersRequest.class));

        Transfers result = Whitebox.invokeMethod(client, "getTransfersDelta", transferRequest, true);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat(ids, containsInAnyOrder("1", "2", "3"));
        verify(transfers1, never()).filterByDateTimeGTE(transferRequest.getStartDateTime());
        verify(transfers2, never()).filterByDateTimeGTE(transferRequest.getStartDateTime());
    }

    @Test
    public void testGetTransfersDeltaWhenHasOlderOne()
        throws Exception
    {
        when(transfers1.getList()).thenReturn(makeTransferFixtures("1", "2", "3"));
        when(transfers1.hasOlderOne(any())).thenReturn(false);
        when(transfers2.getList()).thenReturn(makeTransferFixtures("4"));
        when(transfers2.hasOlderOne(any())).thenReturn(true);
        when(transfers3.getList()).thenReturn(makeTransferFixtures("5", "6", "7"));
        PowerMockito.doReturn(transfers1, transfers2, transfers3).when(client, "getWithdraws", any(GetTransfersRequest.class));

        Transfers result = Whitebox.invokeMethod(client, "getTransfersDelta", transferRequest, true);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat(ids, containsInAnyOrder("1", "2", "3", "4"));
        verify(transfers1, never()).filterByDateTimeGTE(transferRequest.getStartDateTime());
        verify(transfers2).filterByDateTimeGTE(transferRequest.getStartDateTime());
        verifyZeroInteractions(transfers3);
    }

    @Test
    public void testGetAllOpenUserOrdersPagenation()
        throws Exception
    {
        PowerMockito.doReturn(
            (new UserOrders()).addAll(makeUserOrderFixtures("1", "2")),
            (new UserOrders()).addAll(makeUserOrderFixtures("3", "4")),
            null).when(client).getAllOpenUserOrdersWithPageNo(anyInt());
        client.getAllOpenUserOrders(any(Boolean.class));
        verify(client, times(3)).getAllOpenUserOrdersWithPageNo(anyInt());
    }
}
