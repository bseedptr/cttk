package cttk.impl.kraken;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import cttk.CXIds;
import cttk.OrderSide;
import cttk.exception.CTTKException;
import cttk.exception.OrderNotFoundException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.AbstractPrivateCXClientTradeTest;
import cttk.impl.util.ExNotRaisedException;
import cttk.request.CancelOrderRequest;

// For minimum pricing and amount info for Kraken, refer to this page
// https://support.kraken.com/hc/en-us/articles/205893708-What-is-the-minimum-order-size-
public class KrakenPrivateClientTradeTest
    extends AbstractPrivateCXClientTradeTest
{
    @Override
    public String getCxId() {
        return CXIds.KRAKEN;
    }

    @Test
    @Override
    public void testCancelOrderWrongID()
        throws UnsupportedCurrencyPairException, CTTKException
    {

        String nonExistID = "QQQQQQ-QQQQ9-QQQ9QQ";

        try {
            client.cancelOrder(new CancelOrderRequest()
                .setCurrencyPair(getValidCurrencyPair())
                .setOrderId(nonExistID)
                .setSide(OrderSide.BUY));
        } catch (OrderNotFoundException e) {
            assertEquals(this.getCxId(), e.getCxId());
            assertNotNull(e.getExchangeMessage());
            assertNotNull(e.getResponseStatusCode());
            return;
        }
        throw new ExNotRaisedException(raiseExceptionMsg);

    }

    @Override
    @Test(expected = ExNotRaisedException.class)
    public void testInvalidQuantityIncrementPrecision()
        throws CTTKException
    {
        // Refer CTTK-499 for detailed explanation
        // Kraken automatically truncates the order with invalid precision
        super.testInvalidQuantityIncrementPrecision();
    }
}
