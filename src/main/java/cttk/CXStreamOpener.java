package cttk;

import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.request.OpenOrderBookStreamRequest;
import cttk.request.OpenTickerStreamRequest;
import cttk.request.OpenTradeStreamRequest;
import cttk.util.ReopenCXStreamListener;
import cttk.util.ReopenPolicy;

public interface CXStreamOpener
    extends CurrencySetter
{
    CXStream<Ticker> openTickerStream(OpenTickerStreamRequest request, CXStreamListener<Ticker> listener)
        throws CTTKException;

    default CXStream<Ticker> openTickerStream(OpenTickerStreamRequest request, CXStreamListener<Ticker> listener, ReopenPolicy reopenPolicy)
        throws CTTKException
    {
        return openTickerStream(request, ReopenCXStreamListener.create(listener, reopenPolicy));
    }

    CXStream<OrderBook> openOrderBookStream(OpenOrderBookStreamRequest request, CXStreamListener<OrderBook> listener)
        throws CTTKException;

    default CXStream<OrderBook> openOrderBookStream(OpenOrderBookStreamRequest request, CXStreamListener<OrderBook> listener, ReopenPolicy reopenPolicy)
        throws CTTKException
    {
        return openOrderBookStream(request, ReopenCXStreamListener.create(listener, reopenPolicy));
    }

    CXStream<Trades> openTradeStream(OpenTradeStreamRequest request, CXStreamListener<Trades> listener)
        throws CTTKException;

    default CXStream<Trades> openTradeStream(OpenTradeStreamRequest request, CXStreamListener<Trades> listener, ReopenPolicy reopenPolicy)
        throws CTTKException
    {
        return openTradeStream(request, ReopenCXStreamListener.create(listener, reopenPolicy));
    }
}
