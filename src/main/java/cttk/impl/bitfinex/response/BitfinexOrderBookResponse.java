package cttk.impl.bitfinex.response;

import static cttk.impl.bitfinex.BitfinexUtils.toBigDecimal;

import java.math.BigDecimal;
import java.util.ArrayList;

import cttk.dto.OrderBook;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BitfinexOrderBookResponse
    extends ArrayList<Object>
{
    public boolean isAsk() {
        return toBigDecimal(this.get(2)).compareTo(BigDecimal.ZERO) < 0;
    }

    public OrderBook.SimpleOrder toSimpleOrder() {
        return new OrderBook.SimpleOrder()
            .setPrice(toBigDecimal(this.get(0)))
            .setQuantity(toBigDecimal(this.get(2)).abs());
    }
}
