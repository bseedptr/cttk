package cttk.impl.bittrex;

import static cttk.test.TestUtils.hasTestProfile;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import cttk.CXClientFactory;
import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.impl.AbstractAPILimitTest;

public class BittrexAPILimitTest
    extends AbstractAPILimitTest
{
    @Override
    public String getCxId() {
        return CXIds.BITTREX;
    }

    @Before
    @Override
    public void setup()
        throws CTTKException, IOException
    {
        org.junit.Assume.assumeTrue(hasTestProfile("test-api-limit"));
        publicClient = CXClientFactory.createPublicCXClient(getCxId());
    }

    @Test
    @Override
    public void testPrivateAPILimit()
        throws CTTKException, InterruptedException
    {
        // No private client implemented yet
        logger.info("No private client implemented for " + getCxId() + " yet");
    }

    @Test
    public void testPublicAPILimit()
        throws CTTKException, InterruptedException
    {
        testCase((args) -> {
            return publicClient.getAllTickers();
        }, false, 1000);
        // We can't reach public API limit
        logger.info("We can't reach public API limit in " + getCxId());
    }

}
