package cttk.impl.huobi.response;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.OrderStatus;
import cttk.dto.UserOrder;
import cttk.impl.huobi.HuobiUtils;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;

@Data
public class HuobiUserOrder {
    Long id;
    @JsonProperty("account-id")
    Long accountId;
    @JsonProperty("user-id")
    Long userId;
    @JsonProperty("created-at")
    Long createTimestamp;
    @JsonProperty("canceled-at")
    Long cancelTimestamp;
    @JsonProperty("finished-at")
    Long finishTimestamp;
    String symbol;
    BigDecimal amount;
    BigDecimal price;
    String type;
    @JsonProperty("field-amount")
    BigDecimal fieldAmount;
    @JsonProperty("field-cash-amount")
    BigDecimal filedCashAmount;
    @JsonProperty("field-fees")
    BigDecimal fieldFees;
    String source;
    String state;
    String exchange;
    String batch;

    public UserOrder getUserOrder() {
        if (id == null) {
            return null;
        }

        return new UserOrder()
            .setCurrencyPair(HuobiUtils.parseMarketSymbol(symbol))
            .setOid(String.valueOf(id))
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(createTimestamp))
            .setSide(HuobiUtils.toOrderSide(type))
            .setPricingType(HuobiUtils.toPricingType(type))
            .setStatus(state)
            .setStatusType(getStatusType())
            .setPrice(price)
            .setQuantity(amount)
            .setFilledQuantity(fieldAmount)
            .setRemainingQuantity(MathUtils.subtract(amount, fieldAmount))
            .setTotal(MathUtils.multiply(price, amount))
            .setCompletedDateTime(finishTimestamp > 0 ? DateTimeUtils.zdtFromEpochMilli(finishTimestamp) : null)
            .setCanceledDateTime(cancelTimestamp > 0 ? DateTimeUtils.zdtFromEpochMilli(cancelTimestamp) : null);
    }

    private OrderStatus getStatusType() {
        if (cancelTimestamp > 0) return OrderStatus.CANCELED;
        if (state == null) return null;
        switch (state) {
            case "pre-submitted":
            case "submitting":
            case "submitted":
                return OrderStatus.UNFILLED;
            case "partial-filled":
                return OrderStatus.PARTIALLY_FILLED;
            case "filled":
                return OrderStatus.FILLED;
            case "canceled":
                return OrderStatus.CANCELED;
            default:
                return null;
        }
    }
}
