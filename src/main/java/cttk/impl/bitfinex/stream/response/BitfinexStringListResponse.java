package cttk.impl.bitfinex.stream.response;

import static cttk.impl.bitfinex.BitfinexUtils.toStdCurrencyPair;
import static cttk.util.StringUtils.parseBigInteger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.dto.Trade;
import cttk.dto.Trades;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;

public class BitfinexStringListResponse
    extends ArrayList<String>
{
    public Trades toTrades(CurrencyPair currencyPair) {
        if (get(1).equals("te")) return null;
        final BigDecimal price = new BigDecimal(get(5));
        final BigDecimal quantity = new BigDecimal(get(6));

        final String tid = get(3);
        final Trade trade = new Trade()
            .setSno(parseBigInteger(tid))
            .setTid(tid)
            .setDateTime(DateTimeUtils.zdtFromEpochSecond(Long.parseLong(get(4))))
            .setSide(quantity.compareTo(BigDecimal.ZERO) > 0 ? OrderSide.BUY : OrderSide.SELL)
            .setPrice(price)
            .setQuantity(quantity.abs())
            .setTotal(MathUtils.multiply(price, quantity.abs()));

        return new Trades()
            .setCurrencyPair(toStdCurrencyPair(currencyPair))
            .setTrades(Arrays.asList(trade));
    }
}
