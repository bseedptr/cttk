package cttk.impl.korbit;

import cttk.CXIds;
import cttk.impl.AbstractOrderVerifierTest;

public class KorbitOrderVerifierTest
    extends AbstractOrderVerifierTest
{
    @Override
    public String getCxId() {
        return CXIds.KORBIT;
    }
}
