package cttk.impl.bitfinex;

import static cttk.util.StringUtils.concat;
import static cttk.util.StringUtils.toUpper;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;

import lombok.extern.slf4j.Slf4j;

import com.fasterxml.jackson.databind.ObjectMapper;

import cttk.CXIds;
import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.PricingType;
import cttk.SortType;
import cttk.exception.APILimitExceedException;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXAuthorityException;
import cttk.exception.OrderNotFoundException;

@Slf4j
public class BitfinexUtils {
    public static ObjectMapper objectMapper = new ObjectMapper();
    final static String cxId = CXIds.BITFINEX;

    public static String getMarketSymbol(CurrencyPair currencyPair) {
        return currencyPair == null ? null
            : concat(toBitfinexCurrencySymbol(currencyPair.getBaseCurrency()), toBitfinexCurrencySymbol(currencyPair.getQuoteCurrency()));
    }

    public static String getMarketSymbolWithT(CurrencyPair currencyPair) {
        return currencyPair == null ? null : "t" + getMarketSymbol(currencyPair);
    }

    public static CurrencyPair parseMarketSymbol(String symbol) {
        return symbol == null ? null
            : CurrencyPair.of(toStdCurrencySymbol(symbol.substring(0, 3)), toStdCurrencySymbol(symbol.substring(3, 6)));
    }

    public static CurrencyPair parseMarketSymbolWithT(String symbol) {
        return symbol == null ? null
            : CurrencyPair.of(toStdCurrencySymbol(symbol.substring(1, 4)), toStdCurrencySymbol(symbol.substring(4, 7)));
    }

    public static CurrencyPair toStdCurrencyPair(CurrencyPair currencyPair) {
        return currencyPair == null ? null
            : CurrencyPair.of(toStdCurrencySymbol(currencyPair.getBaseCurrency()), toStdCurrencySymbol(currencyPair.getQuoteCurrency()));
    }

    public static String toStdCurrencySymbol(String symbol) {
        if (symbol == null) return null;

        final String upper = toUpper(symbol);
        switch (upper) {
            case "QTM":
                return "QTUM";
            case "USD":
                return "USDT";
            case "BAB":
                return "BCH";
            default:
                return upper;
        }
    }

    public static CurrencyPair toBitfinexCurrencyPair(CurrencyPair currencyPair) {
        return currencyPair == null ? null
            : CurrencyPair.of(toBitfinexCurrencySymbol(currencyPair.getBaseCurrency()), toBitfinexCurrencySymbol(currencyPair.getQuoteCurrency()));
    }

    public static String toBitfinexCurrencySymbol(String symbol) {
        if (symbol == null) return null;

        final String upper = toUpper(symbol);
        switch (upper) {
            case "QTUM":
                return "QTM";
            case "USDT":
                return "USD";
            case "BCH":
                return "BAB";
            case "USD":
                throw new RuntimeException("USD is not supported. Use USDT instead.");
            default:
                return upper;
        }
    }

    public static String toString(OrderSide orderSide) {
        if (orderSide == null) return null;
        switch (orderSide) {
            case BUY:
                return "buy";
            case SELL:
                return "sell";
            default:
                return null;
        }
    }

    public static String toString(SortType sort) {
        if (sort == null) return null;
        switch (sort) {
            case DESC:
                return "0";
            case ASC:
                return "1";
            default:
                return null;
        }
    }

    public static Integer toInteger(SortType sort) {
        if (sort == null) return null;
        switch (sort) {
            case DESC:
                return 0;
            case ASC:
                return 1;
            default:
                return null;
        }
    }

    public static OrderStatus getStatusType(final String status) {
        if (status == null) return null;
        if (status.startsWith("ACTIVE")) {
            return OrderStatus.UNFILLED;
        } else if (status.startsWith("EXECUTED")) {
            return OrderStatus.FILLED;
        } else if (status.startsWith("PARTIALLY")) {
            return OrderStatus.PARTIALLY_FILLED;
        } else if (status.startsWith("CANCELED")) {
            return OrderStatus.CANCELED;
        } else {
            return null;
        }
    }

    public static PricingType getPricingType(final String type) {
        if (type == null || type.trim().isEmpty()) return null;
        if (type.toLowerCase().endsWith("limit")) {
            return PricingType.LIMIT;
        } else if (type.toLowerCase().endsWith("market")) {
            return PricingType.MARKET;
        } else {
            return null;
        }
    }

    public static BigDecimal toBigDecimal(Object object) {
        if (object instanceof Double) {
            return BigDecimal.valueOf((double) object);
        } else {
            return BigDecimal.valueOf((double) ((Integer) object).intValue());
        }
    }

    public static Long toLong(Object object) {
        if (object instanceof Integer) {
            return new Long((Integer) object);
        } else {
            return (Long) object;
        }
    }

    public static Long stringTimestampToLong(String timestamp) {
        return new Long(new BigDecimal(timestamp).longValue());
    }

    public static String toOrderBookPrecisionParamValue(Integer precision)
        throws CTTKException
    {
        if (precision == null) return "P0";
        switch (precision) {
            case 0:
            case 1:
            case 2:
            case 3:
                return "P" + precision;
            default:
                throw new CTTKException("invalid precision: P" + precision).setCxId(CXIds.BITFINEX);
        }
    }

    public static String nonce() {
        return String.valueOf(System.currentTimeMillis());
    }

    public static void checkErrorByResponseMessage(Object requestObj, String body, Integer statusCode)
        throws CXAPIRequestException
    {
        String exchangeErrorCode = "";
        String exchangeMessage = "";
        if (body.length() == 0) return;
        if (body.charAt(0) == '{') {
            Map<?, ?> map = null;
            try {
                map = objectMapper.readValue(body, Map.class);
                if (map.containsKey("code")) { // websocket
                    exchangeErrorCode = Objects.toString(map.get("code"), "");
                    exchangeMessage = Objects.toString(map.get("msg"), "");

                } else if (map.containsKey("message") || map.containsKey("error")) { // rest
                    exchangeErrorCode = Objects.toString(map.get("error"), "");
                    exchangeMessage = Objects.toString(map.get("message"), "");
                }
            } catch (IOException e) {
                log.error("Unable to parse:: {}", body, e);
            }
        } else if (body.charAt(0) == '[') {
            ArrayList<?> list = null;
            try {
                list = objectMapper.readValue(body, ArrayList.class);
            } catch (IOException | NumberFormatException | IndexOutOfBoundsException | NullPointerException e) {
                log.error("Unable to parse:: {}", body, e);
            }
            if (list != null && list.size() > 0 && !"error".equals(Objects.toString(list.get(0), "")))
                return;
            exchangeErrorCode = list.size() > 1 ? Objects.toString(list.get(1), "") : "";
            exchangeMessage = list.size() > 2 ? Objects.toString(list.get(2), "") : "";
        }

        if (exchangeErrorCode.isEmpty() && exchangeMessage.isEmpty()) {
            return;
        }

        String message = String.format("error: %s, message: %s", exchangeErrorCode, exchangeMessage);

        if (exchangeMessage.contains("ERR_RATE_LIMIT")
            || exchangeMessage.contains("Ratelimit")
            || exchangeMessage.contains("You are being rate limited")
            || exchangeMessage.contains("ratelimit: error"))
        {
            throw new APILimitExceedException(message)
                .setCxId(cxId)
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setExchangeErrorCode(exchangeErrorCode);
        } else if (exchangeMessage.contains("Order could not be cancelled.")
            || exchangeMessage.contains("No such order found."))
        {
            throw new OrderNotFoundException(message)
                .setCxId(cxId)
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setExchangeErrorCode(exchangeErrorCode);
        } else if (exchangeMessage.contains("symbol: invalid")
            || exchangeMessage.contains("Unknown symbol"))
        {
            throw CXAPIRequestException.createByRequestObj(requestObj, message)
                .setCxId(cxId)
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setExchangeErrorCode(exchangeErrorCode);
        } else if (exchangeMessage.contains("Could not find a key matching the given X-BFX-APIKEY.")
            || exchangeMessage.contains("This API key does not have permission"))
        {
            throw new CXAuthorityException(message)
                .setCxId(cxId)
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setExchangeErrorCode(exchangeErrorCode);
        } else {
            // "temporarily_unavailable"
            // "Nonce is too small."
            // "No summary found."
            // "Cannot evaluate your available balance, please try again"
            // "event: invalid"
            // "channel: invalid"
            // "channel: unknown"
            // "precision: invalid"
            // "unsubscribe: invalid"
            throw new CXAPIRequestException(message)
                .setCxId(cxId)
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setExchangeErrorCode(exchangeErrorCode);
        }
    }
}
