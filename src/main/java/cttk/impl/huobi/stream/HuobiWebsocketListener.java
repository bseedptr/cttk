package cttk.impl.huobi.stream;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.zip.GZIPInputStream;

import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.impl.huobi.HuobiUtils;
import cttk.impl.stream.AbstractWebsocketListener;
import cttk.impl.stream.MessageConverter;
import cttk.request.AbstractCXStreamRequest;
import okhttp3.Response;
import okhttp3.WebSocket;
import okio.ByteString;

public class HuobiWebsocketListener<T, R extends AbstractCXStreamRequest<R>>
    extends AbstractWebsocketListener<T, R>
{
    public HuobiWebsocketListener(
        final CXStream<T> stream,
        final String channel,
        final R request,
        final MessageConverter<T> converter,
        final CXStreamListener<T> listener)
    {
        super(stream, channel, request, converter, listener, CXIds.HUOBI);
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        webSocket.send(getSubscriptionPayload());
        logger.info("[{}] Open : {}", id, channel);
        this.listener.onOpen(stream);
    }

    private String getSubscriptionPayload() {
        return "{\"sub\": \"" + channel + "\", \"id\": \"" + id + "\"}";
    }

    @Override
    protected void handleMessage(WebSocket webSocket, String text)
        throws Exception
    {
        HuobiUtils.checkErrorByResponseMessage(request, text, null);
        if (text.contains("\"subbed\":")) {
            logger.info("[{}] Subscribed: {}", id, channel);
        } else if (text.startsWith("{\"ping\":")) {
            webSocket.send(text.replaceFirst("ping", "pong"));
        } else {
            T message = converter.convert(text);
            if (message != null) listener.onMessage(message);
        }
    }

    @Override
    public void onClosing(WebSocket webSocket, int code, String reason) {
        webSocket.send(getUnsubscribePayload());
        webSocket.close(code, null);
        logger.info("[{}] Closing: {} - {} {}", id, channel, code, reason);
        this.listener.onClosing(stream, code, reason);
    }

    private String getUnsubscribePayload() {
        return "{\"unsub\": \"" + channel + "\", \"id\": \"" + id + "\"}";
    }

    protected String toString(ByteString bytes)
        throws Exception
    {
        StringWriter sw = new StringWriter();
        try (
            final PrintWriter pw = new PrintWriter(sw);
            final ByteArrayInputStream in = new ByteArrayInputStream(bytes.toByteArray());
            final BufferedReader br = new BufferedReader(new InputStreamReader(new GZIPInputStream(in)));)
        {
            String line;
            for (int i = 0; (line = br.readLine()) != null; i++) {
                if (i > 0) pw.println();
                pw.print(line);
            }
        }
        return sw.toString();
    }
}