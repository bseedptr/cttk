package cttk.impl.coinone.response;

import java.math.BigDecimal;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.PricingType;
import cttk.dto.UserOrder;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import cttk.impl.util.ZoneIds;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CoinoneUserOrderResponse
    extends CoinoneResponse
{
    String status;
    CoinoneInfo info;

    public UserOrder toUserOrder(CurrencyPair currencyPair, PricingType pricingType) {
        if (info == null) return null;
        return new UserOrder()
            .setCurrencyPair(currencyPair)
            .setOid(info.getOrderId())
            .setDateTime(DateTimeUtils.zdtFromEpochSecond(info.getTimestamp(), ZoneIds.SEOUL))
            .setSide(OrderSide.of(info.getType()))
            .setStatusType(info.getStatusType())
            .setPricingType(pricingType)
            .setPrice(info.getPrice())
            .setQuantity(info.getQty())
            .setFilledQuantity(MathUtils.subtract(info.getQty(), info.getRemainQty()))
            .setRemainingQuantity(info.getRemainQty())
            .setTotal(MathUtils.multiply(info.getPrice(), info.getQty()))
            .setFee(info.getFee())
            .setFeeRate(info.getFeeRate());
    }
}

@Data
class CoinoneInfo {
    String orderId;
    String currency;
    Long timestamp;
    BigDecimal price;
    BigDecimal qty;
    BigDecimal remainQty;
    BigDecimal feeRate;
    BigDecimal fee;
    String type;

    public OrderStatus getStatusType() {
        if (qty.compareTo(remainQty) == 0) {
            return OrderStatus.UNFILLED;
        } else if (remainQty.compareTo(BigDecimal.ZERO) == 0) {
            return OrderStatus.FILLED;
        } else if (remainQty.compareTo(qty) < 0) {
            return OrderStatus.PARTIALLY_FILLED;
        }
        return null;
    }
}
