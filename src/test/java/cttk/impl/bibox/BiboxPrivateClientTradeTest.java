package cttk.impl.bibox;

import static org.hamcrest.CoreMatchers.is;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import org.junit.Assert;
import org.junit.Test;

import cttk.CXIds;
import cttk.OrderSide;
import cttk.dto.UserTrades;
import cttk.exception.CTTKException;
import cttk.exception.InvalidOrderException;
import cttk.exception.InvalidTotalMinException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.AbstractPrivateCXClientTradeTest;
import cttk.impl.util.ExNotRaisedException;
import cttk.request.GetUserTradesDeltaRequest;
import cttk.test.TradeTestUtils;

public class BiboxPrivateClientTradeTest
    extends AbstractPrivateCXClientTradeTest
{
    @Override
    public String getCxId() {
        return CXIds.BIBOX;
    }

    @Override
    @Test(expected = ExNotRaisedException.class)
    public void testInvalidPriceIncrementPrecision()
        throws CTTKException
    {
        // Refer CTTK-491 for detailed explanation
        // Bibox automatically truncates the order with invalid precision
        super.testInvalidPriceIncrementPrecision();
    }

    @Override
    @Test(expected = ExNotRaisedException.class)
    public void testInvalidQuantityIncrementPrecision()
        throws CTTKException
    {
        // Refer CTTK-491 for detailed explanation
        // Bibox automatically truncates the order with invalid precision
        super.testInvalidQuantityIncrementPrecision();
    }

    @Test
    public void testGetUserTradesDelta()
        throws UnsupportedCurrencyPairException, CTTKException
    {
        threadSleep();

        final GetUserTradesDeltaRequest request = new GetUserTradesDeltaRequest()
            .setCurrencyPair(getValidCurrencyPair())
            .setStartDateTime(ZonedDateTime.parse("2018-08-01T00:00:00Z"));
        final UserTrades userTrades = client.getUserTrades(request);

        logger.debug("{}", userTrades);

        Assert.assertNotNull(userTrades);
        Assert.assertNotNull(userTrades.first());
        Assert.assertEquals(request.getBaseCurrency(), userTrades.first().getBaseCurrency());
        Assert.assertEquals(request.getQuoteCurrency(), userTrades.first().getQuoteCurrency());
    }

    @Test(expected = InvalidTotalMinException.class)
    public void testCreateOrderInvalidTotalMinException()
        throws Exception
    {

        BigDecimal minQuantity = new BigDecimal("0.0001").stripTrailingZeros();
        BigDecimal minPrice = new BigDecimal("0.00000001").stripTrailingZeros();

        try {
            TradeTestUtils.testCreateCancelOrder(client, getValidCurrencyPair(), OrderSide.BUY, minQuantity, minPrice);
        } catch (InvalidOrderException e) {
            assertError(e, minQuantity, minPrice);
            throw e;
        }
    }

    private void assertError(InvalidOrderException error, BigDecimal quantity, BigDecimal price) {
        Assert.assertThat(error.getBaseCurrency(), is(getValidCurrencyPair().getBaseCurrency()));
        Assert.assertThat(error.getQuoteCurrency(), is(getValidCurrencyPair().getQuoteCurrency()));
        Assert.assertThat(error.getOrderQuantity(), is(quantity));
        Assert.assertThat(error.getOrderPrice(), is(price));
        Assert.assertThat(error.getCxId(), is(this.getCxId()));
    }

}
