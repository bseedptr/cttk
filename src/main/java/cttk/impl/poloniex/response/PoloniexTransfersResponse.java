package cttk.impl.poloniex.response;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import cttk.TransferStatus;
import cttk.TransferType;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.impl.poloniex.PoloniexUtils;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;

@Data
public class PoloniexTransfersResponse {
    List<PoloniexTransfer> deposits;
    List<PoloniexTransfer> withdrawals;

    public Transfers toTransfers() {
        return new Transfers()
            .setTransfers(getTransferList());
    }

    List<Transfer> getTransferList() {
        List<Transfer> transferList = new LinkedList<>();
        if (deposits.size() != 0) {
            transferList.addAll(deposits.stream().map(i -> i.toDeposit()).collect(Collectors.toList()));
        }

        if (withdrawals.size() != 0) {
            transferList.addAll(withdrawals.stream().map(i -> i.toWithdrawl()).collect(Collectors.toList()));
        }
        return transferList;
    }
}

@Data
class PoloniexTransfer {
    String txid;
    String currency;
    String address;
    BigDecimal amount;
    Integer confirmations;
    Long timestamp;
    String status;
    TransferStatus statusType;
    Integer withdrawalNumber;

    Transfer toWithdrawl() {
        String[] statusSplit = null;
        if (status != null && status.contains(":")) {
            statusSplit = status.split(":");
        }
        if (statusSplit == null || statusSplit.length < 2) {
            statusSplit = new String[] { status, null };
        }

        return new Transfer()
            .setCurrency(PoloniexUtils.toStdCurrencySymbol(currency))
            .setType(TransferType.WITHDRAWAL)
            .setTid(withdrawalNumber + "")
            .setTxId(statusSplit[1])
            .setAmount(amount)
            .setStatus(statusSplit[0])
            .setStatusType(PoloniexUtils.parseTransferStatus(statusSplit[0]))
            .setAddress(address)
            .setCompletedDateTime(DateTimeUtils.zdtFromEpochSecond(timestamp))
            .setConfirmations(confirmations);
    }

    Transfer toDeposit() {
        return new Transfer()
            .setCurrency(PoloniexUtils.toStdCurrencySymbol(currency))
            .setType(TransferType.DEPOSIT)
            .setTid(txid)
            .setTxId(txid)
            .setAmount(amount)
            .setStatus(status)
            .setStatusType(PoloniexUtils.parseTransferStatus(status))
            .setAddress(address)
            .setCompletedDateTime(DateTimeUtils.zdtFromEpochSecond(timestamp))
            .setConfirmations(confirmations);
    }
}