package cttk.impl.bibox.stream.response;

import java.io.IOException;

import cttk.dto.OrderBook;
import cttk.exception.CTTKException;
import cttk.impl.bibox.response.BiboxOrderBook;

public class BiboxWebsocketOrderBookResponse extends BiboxWebsocketListResponse<BiboxOrderBook> {
    public BiboxWebsocketOrderBookResponse() {
        super(BiboxOrderBook.class);
    }

    public OrderBook toOrderBook()
        throws IOException, CTTKException
    {
        return this.getBiboxDataFirst().toOrderBook();
    }
}
