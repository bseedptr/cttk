package cttk.impl.coinone.response;

import static cttk.impl.coinone.CoinoneUtils.toStdCurrencySymbol;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import cttk.dto.Balance;
import cttk.dto.Balances;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CoinoneBallanceResponse
    extends CoinoneResponse
{
    List<CoinoneNormalWallet> normalWallets;
    Map<String, CoinoneBalance> map = new HashMap<>();

    @JsonAnyGetter
    public Map<String, CoinoneBalance> any() {
        return this.map;
    }

    @JsonAnySetter
    public void set(String name, CoinoneBalance value) {
        this.map.put(name, value);
    }

    public Balances getBalances() {
        return new Balances().setBalances(getBalanceList());
    }

    public List<Balance> getBalanceList() {
        if (this.map.entrySet() == null) return null;
        return this.map.entrySet().stream()
            .map(i -> i.getValue().toBalance(toStdCurrencySymbol(i.getKey())))
            .collect(Collectors.toList());
    }
}

@Data
class CoinoneBalance {
    BigDecimal avail;
    BigDecimal balance;

    public Balance toBalance(String currency) {
        final Balance b = new Balance()
            .setCurrency(currency)
            .setTotal(balance)
            .setAvailable(avail);
        b.setInUse(b.getTotal().subtract(b.getAvailable()));
        return b;
    }
}

@Data
class CoinoneNormalWallet {
    BigDecimal balance;
    String label;
}