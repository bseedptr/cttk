package cttk.impl.stream;

public interface MessageConverter<T> {
    T convert(String message)
        throws Exception;
}
