package cttk.impl.bitfinex;

import static cttk.impl.bitfinex.BitfinexUtils.getMarketSymbol;
import static cttk.impl.bitfinex.BitfinexUtils.getMarketSymbolWithT;
import static cttk.impl.bitfinex.BitfinexUtils.parseMarketSymbol;
import static cttk.impl.bitfinex.BitfinexUtils.parseMarketSymbolWithT;
import static cttk.impl.bitfinex.BitfinexUtils.toBitfinexCurrencySymbol;
import static cttk.impl.bitfinex.BitfinexUtils.toStdCurrencyPair;
import static cttk.impl.bitfinex.BitfinexUtils.toStdCurrencySymbol;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import cttk.CurrencyPair;
import cttk.CurrencyPair.SimpleCurrencyPair;
import cttk.exception.APILimitExceedException;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.InvalidOrderException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.util.HttpResponse;
import cttk.request.GetOrderBookRequest;

public class BitfinexUtilsTest {
    @Rule
    public ExpectedException ee = ExpectedException.none();

    @Test
    public void testGetMarketSymbol() {
        assertNull(getMarketSymbol(null));

        CurrencyPair pair = new SimpleCurrencyPair("EOS", "ETH");
        assertEquals("EOSETH", getMarketSymbol(pair));
    }

    @Test
    public void testGetMarketSymbolWithT() {
        assertNull(getMarketSymbolWithT(null));

        CurrencyPair pair = new SimpleCurrencyPair("EOS", "ETH");
        assertEquals("tEOSETH", getMarketSymbolWithT(pair));
    }

    @Test
    public void testParseMarketSymbol() {
        assertNull(parseMarketSymbol(null));

        CurrencyPair pair = parseMarketSymbol("EOSETH");
        assertEquals(pair.getBaseCurrency(), "EOS");
        assertEquals(pair.getQuoteCurrency(), "ETH");
    }

    @Test
    public void testParseMarketSymbolWithT() {
        assertNull(parseMarketSymbolWithT(null));

        CurrencyPair pair = parseMarketSymbolWithT("tEOSETH");
        assertEquals(pair.getBaseCurrency(), "EOS");
        assertEquals(pair.getQuoteCurrency(), "ETH");
    }

    @Test
    public void testNormalizeCurrencyPair() {
        assertNull(toStdCurrencyPair(null));

        CurrencyPair originalPair = new SimpleCurrencyPair("QTM", "USD");
        CurrencyPair normalizedPair = toStdCurrencyPair(originalPair);
        assertEquals("QTUM", normalizedPair.getBaseCurrency());
        assertEquals("USDT", normalizedPair.getQuoteCurrency());
    }

    @Test
    public void testToStdCurrencySymbol() {
        assertEquals("Should be normalized.", "QTUM", toStdCurrencySymbol("QTM"));
        assertEquals("Should be normalized.", "USDT", toStdCurrencySymbol("USD"));
    }

    @Test
    public void testToBitfinexCurrencySymbolWhenUSD() {
        ee.expect(RuntimeException.class);
        ee.expectMessage("USD is not supported. Use USDT instead.");

        toBitfinexCurrencySymbol("USD");
    }

    @Test
    public void testToBitfinexCurrencySymbol() {
        assertEquals("Should be normalized.", "QTM", toBitfinexCurrencySymbol("QTUM"));
        assertEquals("Should be normalized.", "USD", toBitfinexCurrencySymbol("USDT"));
    }

    @Test(expected = CXAPIRequestException.class)
    public void testException0()
        throws CTTKException
    {
        HttpResponse r1 = new HttpResponse().setStatus(400).setBody("{\"message\":\"Nonce is too small.\"}");
        BitfinexUtils.checkErrorByResponseMessage(null, r1.getBody(), r1.getStatus());
    }

    @Test(expected = APILimitExceedException.class)
    public void testException1()
        throws CTTKException
    {
        HttpResponse r2 = new HttpResponse().setStatus(429).setBody("[\"error\",11010,\"ratelimit: error\"]");
        BitfinexUtils.checkErrorByResponseMessage(null, r2.getBody(), r2.getStatus());
    }

    @Test(expected = CXAPIRequestException.class)
    public void testException3()
        throws CTTKException
    {
        String r = "{\"msg\": \"event: invalid (asubscribe)\", \"code\": 10000, \"event\": \"error\"}";
        BitfinexUtils.checkErrorByResponseMessage(null, r, null);
    }

    @Test(expected = CXAPIRequestException.class)
    public void testException4()
        throws CTTKException
    {
        String r = "{\"msg\": \"event: invalid (UNK)\", \"code\": 10000, \"event\": \"error\"}";
        BitfinexUtils.checkErrorByResponseMessage(null, r, null);
    }

    @Test(expected = CXAPIRequestException.class)
    public void testException5()
        throws CTTKException
    {
        String r = "{\"pair\": \"tBTCUSD\", \"msg\": \"channel: invalid\", \"code\": 10300, \"event\": \"error\", \"prec\": \"P0\"}";
        BitfinexUtils.checkErrorByResponseMessage(null, r, null);
    }

    @Test(expected = CXAPIRequestException.class)
    public void testException6()
        throws CTTKException
    {
        String r = "{\"code\": 10300, \"prec\": \"P0\", \"msg\": \"channel: unknown\", \"pair\": \"tBTCUSD\", \"event\": \"error\", \"channel\": \"abook\"}";
        BitfinexUtils.checkErrorByResponseMessage(null, r, null);
    }

    @Test(expected = CXAPIRequestException.class)
    public void testException7()
        throws CTTKException
    {
        String r = "{\"code\": 10300, \"symbol\": \"UNKUNK\", \"prec\": \"P0\", \"pair\": \"NKUNK\", \"msg\": \"symbol: invalid\", \"event\": \"error\", \"channel\": \"book\"}";
        BitfinexUtils.checkErrorByResponseMessage(null, r, null);
    }

    @Test(expected = UnsupportedCurrencyPairException.class)
    public void testException8()
        throws CTTKException
    {
        GetOrderBookRequest ob = new GetOrderBookRequest()
            .setBaseCurrency("baseCurrency")
            .setQuoteCurrency("quoteCurrency")
            .setCount(1)
            .setPrecision(1);
        String r = "{\"code\": 10300, \"symbol\": \"atBTCUSD\", \"prec\": \"P0\", \"msg\": \"symbol: invalid\", \"pair\": \"tBTCUSD\", \"event\": \"error\", \"channel\": \"book\"}";
        BitfinexUtils.checkErrorByResponseMessage(ob, r, null);
    }

    @Test(expected = CXAPIRequestException.class)
    public void testException9()
        throws CTTKException
    {
        String r = "{\"code\": 10300, \"symbol\": \"tBTCUSD\", \"prec\": \"Pa0\", \"msg\": \"precision: invalid\", \"pair\": \"BTCUSD\", \"event\": \"error\", \"channel\": \"book\"}";
        BitfinexUtils.checkErrorByResponseMessage(null, r, null);
    }

    @Test(expected = UnsupportedCurrencyPairException.class)
    public void testException10()
        throws CTTKException
    {
        GetOrderBookRequest ob = new GetOrderBookRequest()
            .setBaseCurrency("baseCurrency")
            .setQuoteCurrency("quoteCurrency")
            .setCount(1)
            .setPrecision(1);
        String r = "{\"code\": 10300, \"symbol\": \"tBTCUSDa\", \"prec\": \"P0\", \"msg\": \"symbol: invalid\", \"pair\": \"BTCUSDa\", \"event\": \"error\", \"channel\": \"ticker\"}";
        BitfinexUtils.checkErrorByResponseMessage(ob, r, null);
    }

    @Test(expected = CXAPIRequestException.class)
    public void testException11()
        throws CTTKException
    {
        String r = "{\"msg\": \"unsubscribe: invalid\", \"code\": 10400, \"event\": \"error\"}";
        BitfinexUtils.checkErrorByResponseMessage(null, r, null);
    }

    @Test
    public void testException12()
        throws CTTKException
    {
        BitfinexUtils.checkErrorByResponseMessage(null, "", null);
        BitfinexUtils.checkErrorByResponseMessage(null, "{}", null);
        BitfinexUtils.checkErrorByResponseMessage(null, "[]", null);
    }

}