package cttk.impl.upbit;

import static cttk.CXIds.UPBIT;
import static cttk.dto.UserOrders.Category.CLOSED;
import static cttk.dto.UserOrders.Category.OPEN;
import static cttk.impl.util.Objects.nvl;
import static cttk.util.DeltaUtils.sleepWithException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonProcessingException;

import cttk.CXClientFactory;
import cttk.OrderStatus;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.SortType;
import cttk.TradingFeeProvider;
import cttk.auth.Credential;
import cttk.dto.Balances;
import cttk.dto.BankAccount;
import cttk.dto.MarketInfos;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.dto.UserAccount;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrades;
import cttk.dto.Wallet;
import cttk.dto.Wallets;
import cttk.dto.WithdrawResult;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXServerException;
import cttk.exception.InsufficientBalanceException;
import cttk.exception.InvalidAmountMinException;
import cttk.exception.InvalidPriceIncrementPrecisionException;
import cttk.exception.InvalidTotalMaxException;
import cttk.exception.InvalidTotalMinException;
import cttk.exception.OrderNotFoundException;
import cttk.exception.UnsupportedCurrencyException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.upbit.response.UpbitBalanceResponse;
import cttk.impl.upbit.response.UpbitCreateOrderResponse;
import cttk.impl.upbit.response.UpbitTransferResponse;
import cttk.impl.upbit.response.UpbitUserOrderResponse;
import cttk.impl.upbit.response.UpbitWithdrawResponse;
import cttk.impl.util.CryptoUtils;
import cttk.impl.util.HttpHeaders;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.MathUtils;
import cttk.impl.util.ParamsBuilder;
import cttk.request.CancelOrderRequest;
import cttk.request.CreateMarketPriceOrderRequest;
import cttk.request.CreateOrderRequest;
import cttk.request.GetBankAccountRequest;
import cttk.request.GetOpenUserOrdersRequest;
import cttk.request.GetTransfersDeltaRequest;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserAccountRequest;
import cttk.request.GetUserOrderHistoryDeltaRequest;
import cttk.request.GetUserOrderHistoryRequest;
import cttk.request.GetUserOrderRequest;
import cttk.request.GetUserTradesDeltaRequest;
import cttk.request.GetUserTradesRequest;
import cttk.request.GetWalletRequest;
import cttk.request.WithdrawToBankRequest;
import cttk.request.WithdrawToWalletRequest;
import okhttp3.Headers;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UpbitPrivateClient
    extends AbstractUpbitClient
    implements PrivateCXClient
{
    private static final okhttp3.MediaType JSON = okhttp3.MediaType.parse("application/json; charset=utf-8");
    private static final String JWT_ALG = "HS256";
    private static final String JWT_TYPE = "JWT";

    public static final Map<String, String> JWT_HEADER = new HashMap<String, String>() {
        {
            put("alg", JWT_ALG);
            put("typ", JWT_TYPE);
        }
    };

    protected final Credential credential;
    protected MarketInfos marketInfos;

    public UpbitPrivateClient(Credential credential) {
        super();
        this.credential = credential;
        try {
            final PublicCXClient pubClient = CXClientFactory.createPublicCXClient(this.getCxId());
            marketInfos = pubClient.getMarketInfos();
        } catch (CTTKException e) {
            logger.error("Failed to getMarketInfos while creating CoinonePrivateClient.");
        }

    }

    @Override
    public UserAccount getUserAccount(GetUserAccountRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public TradingFeeProvider getTradingFeeProvider()
        throws CTTKException
    {
        return new UpbitTradingFeeProvider();
    }

    @Override
    public BankAccount getBankAccount(GetBankAccountRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public Wallet getWallet(GetWalletRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public UserOrder createMarketPriceOrder(CreateMarketPriceOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public Wallets getAllWallets()
        throws CTTKException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public UserOrders getAllOpenUserOrders(boolean withTrades)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final UserOrders userOrders = new UserOrders();

        for (int i = 1; true; i++) {
            final UserOrders tempUserOrders = getAllOpenUserOrdersWithPageNo(i);

            if (tempUserOrders == null || tempUserOrders.isEmpty()) {
                break;
            } else {
                userOrders.addAll(tempUserOrders);
            }

            try {
                Thread.sleep(200L);
            } catch (InterruptedException e) {
                logger.error("Unexpected error", e);
            }
        }

        return userOrders.setCategory(OPEN);
    }

    protected UserOrders getAllOpenUserOrdersWithPageNo(int pageNo)
        throws CTTKException
    {
        Map<String, String> queryParams = ParamsBuilder.create()
            .putIfNotNull("state", "wait")
            .putIfNotNull("page", pageNo)
            .putIfNotNull("order_by", "asc")
            .build();

        final HttpResponse response = requestGetWithToken("/v1/orders", queryParams);
        checkResponseError(null, response);

        try {
            return convertList(response, UpbitUserOrderResponse.class,
                e -> new UserOrders()
                    .setOrders(e.stream()
                        .filter(i -> i.getPrice() != null)
                        .map(UpbitUserOrderResponse::toUserOrder)
                        .filter(i -> i != null)
                        .collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException(e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public WithdrawResult withdraw(WithdrawToBankRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        Map<String, String> queryParams = ParamsBuilder.create()
            .putIfNotNull("amount", request.getAmount())
            .build();

        final HttpResponse response = requestPostWithToken("/v1/withdraws/krw", queryParams);

        checkResponseError(request, response);

        return convert(response, UpbitWithdrawResponse.class, (i -> i.toWithdrawResult()));
    }

    @Override
    public WithdrawResult withdraw(WithdrawToWalletRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        Map<String, String> queryParams = ParamsBuilder.create()
            .putIfNotNull("currency", request.getCurrency())
            .putIfNotNull("amount", request.getQuantity())
            .putIfNotNull("address", request.getAddress())
            .build();

        final HttpResponse response = requestPostWithToken("/v1/withdraws/coin", queryParams);

        checkResponseError(request, response);

        return convert(response, UpbitWithdrawResponse.class, (i -> i.toWithdrawResult()));
    }

    @Override
    public Balances getAllBalances()
        throws CTTKException
    {
        final HttpResponse response = requestGetWithToken("/v1/accounts", null);

        checkResponseError(null, response);

        try {
            return convertList(response, UpbitBalanceResponse.class,
                e -> new Balances().setBalances(e.stream().map(UpbitBalanceResponse::toBalance).collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getAllBalances", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    /**
     * To get correct status of canceled order, you must wait after canceling an order (2s)
     * @param request
     * @return
     * @throws CTTKException
     * @throws UnsupportedCurrencyPairException
     */
    @Override
    public UserOrder getUserOrder(GetUserOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        Map<String, String> queryParams = ParamsBuilder.create()
            .putIfNotNull("uuid", request.getOid())
            .build();

        final HttpResponse response = requestGetWithToken("/v1/order", queryParams);
        try {
            checkResponseError(request, response);
        } catch (OrderNotFoundException e) {
            return null;
        }
        return convert(response, UpbitUserOrderResponse.class, (i -> i.toUserOrder()));
    }

    @Override
    public UserOrders getOpenUserOrders(GetOpenUserOrdersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        Map<String, String> queryParams = ParamsBuilder.create()
            .putIfNotNull("market", UpbitUtils.getMarketSymbol(request.getBaseCurrency(), request.getQuoteCurrency()))
            .putIfNotNull("state", "wait")
            .putIfNotNull("page", request.getPageNo())
            .build();

        final HttpResponse response = requestGetWithToken("/v1/orders", queryParams);

        checkResponseError(request, response);

        try {
            return convertList(response, UpbitUserOrderResponse.class,
                e -> new UserOrders()
                    .setOrders(e.stream()
                        .filter(i -> i.getPrice() != null)
                        .map(UpbitUserOrderResponse::toUserOrder)
                        .filter(i -> i != null)
                        .collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getOpenUserOrders", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGetWithToken("/v1/orders",
            ParamsBuilder.create()
                .putIfNotNull("market", UpbitUtils.getMarketSymbol(request))
                .putIfNotNull("state", nvl(UpbitUtils.toStateString(request.getStatus()), "done"))
                .putIfNotNull("order_by", nvl(request.getSort(), SortType.ASC).name().toLowerCase())
                .putIfNotNull("page", request.getPageNo())
                .build());
        try {
            checkResponseError(request, response);
        } catch (OrderNotFoundException e) {
            return new UserOrders().setCategory(OPEN);
        }

        final UserOrders userOrders;
        try {
            userOrders = convertList(response, UpbitUserOrderResponse.class,
                e -> new UserOrders()
                    .setOrders(e.stream()
                        .filter(i -> i.getPrice() != null)
                        .map(UpbitUserOrderResponse::toUserOrder)
                        .filter(i -> i != null)
                        .collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getUserOrderHistory", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
        return userOrders.setCategory(CLOSED);
    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return new UserOrders()
            .addAll(getUserOrderHistory(request, OrderStatus.FILLED))
            .addAll(getUserOrderHistory(request, OrderStatus.CANCELED))
            .setCategory(CLOSED);
    }

    private UserOrders getUserOrderHistory(GetUserOrderHistoryDeltaRequest request, OrderStatus status)
        throws CTTKException
    {
        // GROUP p: page,<<<(exception)
        final GetUserOrderHistoryRequest partialRequest = new GetUserOrderHistoryRequest()
            .setCurrencyPair(request)
            .setCount(50)
            .setSort(SortType.DESC)
            .setStatus(status);

        final UserOrders userOrders = new UserOrders();
        if (request.hasStartDateTime()) {
            int pageNo = 1;
            while (true) {
                partialRequest.setPageNo(pageNo++);
                final UserOrders tempUserOrders = getUserOrderHistory(partialRequest);

                if (tempUserOrders.isEmpty()) {
                    break;
                } else if (tempUserOrders.hasOlderOne(request.getStartDateTime())) {
                    tempUserOrders.filterByDateTimeGTE(request.getStartDateTime());
                    userOrders.addAll(tempUserOrders);
                    break;
                } else {
                    userOrders.addAll(tempUserOrders);
                }

                sleepWithException(request.getSleepTimeMillis(), UPBIT);
            }
            return userOrders.setCategory(CLOSED);
        } else {
            return getUserOrderHistory(partialRequest);
        }
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        Map<String, String> params = ParamsBuilder.create()
            .putIfNotNull("market", UpbitUtils.getMarketSymbol(request.getBaseCurrency(), request.getQuoteCurrency()))
            .putIfNotNull("state", "done")
            .putIfNotNull("page", request.getPageNo())
            .build();

        final HttpResponse response = requestGetWithToken("/v1/orders", params);

        checkResponseError(request, response);

        try {
            return convertList(response, UpbitUserOrderResponse.class,
                e -> new UserTrades()
                    .setTrades(e.stream()
                        .filter(i -> i.getPrice() != null)
                        .map(UpbitUserOrderResponse::toUserTrade)
                        .collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getUserTrades", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP p: page,<<<(exception)
        final GetUserTradesRequest partialRequest = new GetUserTradesRequest()
            .setCurrencyPair(request)
            .setCount(50);

        final UserTrades userTrades = new UserTrades();
        if (request.hasStartDateTime()) {
            int pageNo = 1;
            while (true) {
                partialRequest.setPageNo(pageNo++);
                final UserTrades tempUserTrades = getUserTrades(partialRequest);

                if (tempUserTrades.isEmpty()) {
                    break;
                } else if (tempUserTrades.hasOlderOne(request.getStartDateTime())) {
                    tempUserTrades.filterByDateTimeGTE(request.getStartDateTime());
                    userTrades.addAll(tempUserTrades);
                    break;
                } else {
                    userTrades.addAll(tempUserTrades);
                }

                sleepWithException(request.getSleepTimeMillis(), UPBIT);
            }

            return userTrades;
        } else {
            return getUserTrades(partialRequest);
        }
    }

    @Override
    public Transfers getTransfers(GetTransfersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final Transfers transfers = new Transfers();

        transfers.addTransfers(getWithdraws(request));
        transfers.addTransfers(getDeposits(request));

        return transfers;
    }

    private List<Transfer> getWithdraws(GetTransfersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        Map<String, String> queryParams = ParamsBuilder.create()
            .putIfNotNull("currency", request.getCurrency())
            .putIfNotNull("state", request.getState())
            .putIfNotNull("limit", request.getCount())
            .build();

        final HttpResponse response = requestGetWithToken("/v1/withdraws", queryParams);

        checkResponseError(request, response);
        if (response.getBody().contains("currency does not have a valid value")) {
            throw new UnsupportedCurrencyException()
                .setCurrency(request.getCurrency())
                .setCxId("UPBIT");
        }

        try {
            return convertList(response, UpbitTransferResponse.class,
                e -> e.stream().map(UpbitTransferResponse::toTransfer).collect(Collectors.toList()));
        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getWithdraws", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    private List<Transfer> getDeposits(GetTransfersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        Map<String, String> queryParams = ParamsBuilder.create()
            .putIfNotNull("currency", request.getCurrency())
            .putIfNotNull("state", request.getState())
            .putIfNotNull("limit", request.getCount())
            .build();

        final HttpResponse response = requestGetWithToken("/v1/deposits", queryParams);

        checkResponseError(request, response);

        if (response.getBody().contains("currency does not have a valid value")) {
            throw new UnsupportedCurrencyException()
                .setCurrency(request.getCurrency())
                .setCxId("UPBIT");
        }

        try {
            return convertList(response, UpbitTransferResponse.class,
                e -> e.stream().map(UpbitTransferResponse::toTransfer).collect(Collectors.toList()));
        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getDeposits", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public Transfers getTransfers(GetTransfersDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP n - N/A
        final Transfers transfers = getTransfers(new GetTransfersRequest().setCurrency(request));

        if (request.hasStartDateTime() && transfers != null && transfers.isNotEmpty()) {
            transfers.filterByDateTimeGTE(request.getStartDateTime());
        }

        return transfers;
    }

    @Override
    public UserOrder createOrder(CreateOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        Map<String, String> queryParams = ParamsBuilder.create()
            .putIfNotNull("market", UpbitUtils.getMarketSymbol(request.getBaseCurrency(), request.getQuoteCurrency()))
            .putIfNotNull("side", UpbitUtils.toSideString(request.getSide()))
            .putIfNotNull("volume", request.getQuantity())
            .putIfNotNull("price", request.getPrice())
            .putIfNotNull("ord_type", "limit")
            .build();

        final HttpResponse response = requestPostWithToken("/v1/orders", queryParams);
        checkOrderResponseError(request, response);
        checkResponseError(request, response);

        return fillResponseByRequest(request, convert(response, UpbitCreateOrderResponse.class, i -> i.toUserOrder()));
    }

    @Override
    public void cancelOrder(CancelOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        Map<String, String> queryParams = ParamsBuilder.create()
            .putIfNotNull("uuid", request.getOrderId())
            .build();

        final HttpResponse response = requestDeleteWithToken("/v1/order", queryParams);
        checkResponseError(request, response);
    }

    private HttpResponse requestGetWithToken(String endpoint, Map<String, String> queryParams)
        throws CTTKException
    {
        Map<String, String> params = ParamsBuilder.create()
            .putIfNotNull("access_key", credential.getAccessKey())
            .putIfNotNull("nonce", nonce())
            .putIfNotNull("query", UpbitUtils.urlEncodeUTF8(queryParams))
            .build();

        final Request request = new Request.Builder()
            .url(getHostUrl(endpoint, queryParams))
            .headers(createHttpHeaders(params))
            .build();

        debug(request);

        try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
            return toHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    private HttpResponse requestPostWithToken(String endpoint, Map<String, String> queryParams)
        throws CTTKException
    {
        Map<String, String> params = ParamsBuilder.create()
            .putIfNotNull("access_key", credential.getAccessKey())
            .putIfNotNull("nonce", nonce())
            .putIfNotNull("query", UpbitUtils.urlEncodeUTF8(queryParams))
            .build();

        final Request request;
        try {
            request = new Request.Builder()
                .url(getHostUrl(endpoint))
                .headers(createHttpHeaders(params))
                .post(RequestBody.create(JSON, objectMapper.writeValueAsString(queryParams)))
                .build();
        } catch (JsonProcessingException e) {
            throw CTTKException.create(e, getCxId());
        }

        debug(request, params);

        try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
            return toHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    private HttpResponse requestDeleteWithToken(String endpoint, Map<String, String> queryParams)
        throws CTTKException
    {
        Map<String, String> params = ParamsBuilder.create()
            .putIfNotNull("access_key", credential.getAccessKey())
            .putIfNotNull("nonce", nonce())
            .putIfNotNull("query", UpbitUtils.urlEncodeUTF8(queryParams))
            .build();

        final Request request = new Request.Builder()
            .url(getHostUrl(endpoint, queryParams))
            .headers(createHttpHeaders(params))
            .delete()
            .build();

        debug(request, params);

        try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
            return toHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    private Headers createHttpHeaders(Map<String, String> params)
        throws CTTKException
    {
        try {
            final String header = objectMapper.writeValueAsString(JWT_HEADER);
            final String payload = objectMapper.writeValueAsString(params);
            final String signature = CryptoUtils.getJWT(header, payload, credential.getSecretKey());

            return new Headers.Builder()
                .add(HttpHeaders.AUTHORIZATION, "Bearer " + signature)
                .build();
        } catch (JsonProcessingException e) {
            throw CTTKException.create(e, getCxId());
        }
    }

    private String nonce() {
        return String.valueOf(System.currentTimeMillis());
    }

    private void checkOrderResponseError(CreateOrderRequest request, HttpResponse response)
        throws CTTKException
    {
        if (!response.is4xx()) return;
        Map<?, ?> map;
        try {
            Map<?, ?> mapWrapper = objectMapper.readValue(response.getBody(), Map.class);
            map = (Map<?, ?>) mapWrapper.get("error");
        } catch (IOException | NullPointerException e) {
            return;
        }

        if (map == null) return;

        // final String exchangeMessage = Objects.toString(map.get("message"), "");
        final String exchangeErrorCode = Objects.toString(map.get("name"), "");

        if (exchangeErrorCode.contains("under_min_total_bid")) {
            throw new InvalidTotalMinException()
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity())
                .setExchangeErrorCode(exchangeErrorCode)
                .setTotal(MathUtils.multiply(request.getPrice(), request.getQuantity()));

        } else if (exchangeErrorCode.contains("over_krw_funds_bid")) {
            throw new InvalidTotalMaxException()
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity())
                .setExchangeErrorCode(exchangeErrorCode);
        } else if (exchangeErrorCode.contains("insufficient_funds_bid")) {
            // insufficient_funds_ask
            // insufficient_funds_bid
            throw new InsufficientBalanceException()
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity())
                .setExchangeErrorCode(exchangeErrorCode);
        } else if (exchangeErrorCode.contains("invalid_funds_bid")
            || exchangeErrorCode.contains("invalid_price_bid"))
        {
            throw new InvalidPriceIncrementPrecisionException()
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity())
                .setExchangeErrorCode(exchangeErrorCode);
        } else if (exchangeErrorCode.contains("min_trade_size_bid")) {
            throw new InvalidAmountMinException()
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity())
                .setExchangeErrorCode(exchangeErrorCode);
        }
    }
}
