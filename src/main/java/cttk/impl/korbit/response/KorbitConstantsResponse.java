package cttk.impl.korbit.response;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.dto.MarketInfo;
import cttk.dto.MarketInfos;
import cttk.impl.korbit.KorbitUtils;
import lombok.Data;

@Data
public class KorbitConstantsResponse {
    Map<String, KorbitMarketInfo> exchange;

    public MarketInfos toMarketInfos() {
        return new MarketInfos().setMarketInfos(toMarketInfoList());
    }

    public List<MarketInfo> toMarketInfoList() {
        return exchange == null ? null
            : exchange.entrySet().stream()
                .map(e -> e.getValue().toMarketInfo(KorbitUtils.parseMarketSymbol(e.getKey())))
                .collect(Collectors.toList());
    }
}

@Data
class KorbitMarketInfo {
    BigDecimal tick_size;
    BigDecimal min_price;
    BigDecimal max_price;
    BigDecimal order_min_size;
    BigDecimal order_max_size;

    public MarketInfo toMarketInfo(CurrencyPair currencyPair) {
        return new MarketInfo()
            .setCurrencyPair(currencyPair)
            .setPriceIncrement(tick_size)
            .setMinPrice(min_price)
            .setMaxPrice(max_price)
            .setMinQuantity(order_min_size)
            .setMaxQuantity(order_max_size);
    }
}
