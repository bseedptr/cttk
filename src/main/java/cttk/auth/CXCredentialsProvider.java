package cttk.auth;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import cttk.exception.UnsupportedCXException;

public class CXCredentialsProvider {
    final ObjectMapper mapper = createYAMLObjectMapper();
    final String baseDir;

    public CXCredentialsProvider() {
        this(Paths.get(getUserHome(), ".cttk/credentials").toString());
    }

    public CXCredentialsProvider(String baseDir) {
        this.baseDir = baseDir;
    }

    public CXCredentials get(final String cxId)
        throws UnsupportedCXException, IOException
    {
        try (InputStream is = new FileInputStream(Paths.get(baseDir, cxId.toLowerCase() + ".yml").toFile())) {
            return mapper.readValue(is, CXCredentials.class);
        }
    }

    private static final ObjectMapper createYAMLObjectMapper() {
        final ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper;
    }

    private static String getUserHome() {
        return System.getProperty("user.home");
    }

    public static CXCredentials getDefaultCredential(final String cxId)
        throws UnsupportedCXException, IOException
    {
        return new CXCredentialsProvider().get(cxId);
    }
}
