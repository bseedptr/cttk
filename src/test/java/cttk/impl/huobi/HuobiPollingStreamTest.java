package cttk.impl.huobi;

import cttk.CXIds;
import cttk.impl.AbstractPollingCXStreamTest;

public class HuobiPollingStreamTest
    extends AbstractPollingCXStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.HUOBI;
    }
}
