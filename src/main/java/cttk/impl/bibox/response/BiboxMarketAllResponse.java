package cttk.impl.bibox.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import cttk.dto.MarketInfo;
import cttk.dto.MarketInfos;
import cttk.dto.Ticker;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Data
@EqualsAndHashCode(callSuper = true)
public class BiboxMarketAllResponse
    extends BiboxResultResponse<List<BiboxMarketAll>>
{
    public MarketInfos toMarketInfos() {
        return new MarketInfos()
            .setMarketInfos(result.stream().map(BiboxMarketAll::toMarketInfo).collect(Collectors.toList()));
    }
}

@Data
class BiboxMarketAll {
    long id;
    @JsonProperty("coin_symbol")
    String coinSymbol;
    @JsonProperty("currency_symbol")
    String currencySymbol;
    BigDecimal last;
    BigDecimal high;
    BigDecimal change;
    String percent;
    BigDecimal vol24H;
    BigDecimal amount;
    @JsonProperty("last_cny")
    BigDecimal lastCny;
    @JsonProperty("high_cny")
    BigDecimal highCny;
    @JsonProperty("low_cny")
    BigDecimal lowCny;
    @JsonProperty("last_usd")
    BigDecimal lastUsd;
    @JsonProperty("high_usd")
    BigDecimal highUsd;
    @JsonProperty("low_usd")
    BigDecimal lowUsd;

    public MarketInfo toMarketInfo() {
        return new MarketInfo()
            .setCurrencyPair(coinSymbol, currencySymbol);
    }

    public Ticker toTicker() {
        return new Ticker()
            .setCurrencyPair(coinSymbol, currencySymbol)
            .setId(String.valueOf(id))
            .setLastPrice(last)
            .setHighPrice(high)
            .setVolume(vol24H);
    }
}
