package cttk.request;

import java.time.ZonedDateTime;

import cttk.AbstractCurrencyPair;
import cttk.DurationGettable;
import cttk.ScanDirection;
import cttk.SortType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class GetUserTradesRequest
    extends AbstractCurrencyPair<GetUserTradesRequest>
    implements DurationGettable
{
    Integer offset = 0;
    Integer pageNo = 1;
    Integer count = 20;
    String status;
    String by;
    String fromId;
    String tillId;
    ZonedDateTime startDateTime;
    ZonedDateTime endDateTime;
    String orderId;
    SortType sort;
    ScanDirection scanDirection;

    public boolean isScanDirectionPrev() {
        return scanDirection == ScanDirection.PREV;
    }
}
