package cttk.impl.bibox.stream;

import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.UserData;
import cttk.UserDataStreamOpener;
import cttk.auth.Credential;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.request.OpenTradeStreamRequest;
import cttk.request.OpenUserDataStreamRequest;

public class BiboxUserDataStreamOpener
    implements UserDataStreamOpener
{
    private final Credential credential;

    public BiboxUserDataStreamOpener(Credential credential) {
        super();
        this.credential = credential;
    }

    @Override
    public CXStream<UserData> openUserDataStream(OpenUserDataStreamRequest request, CXStreamListener<UserData> listener)
    {
        return new BiboxUserDataWebsocketStream(credential, listener);
    }
}
