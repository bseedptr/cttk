package cttk.impl.hitbtc;

import org.junit.Test;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.impl.AbstractAPILimitTest;

public class HitbtcAPILimitTest
    extends AbstractAPILimitTest
{
    @Override
    public String getCxId() {
        return CXIds.HITBTC;
    }

    @Test
    @Override
    public void testPublicAPILimit()
        throws CTTKException, InterruptedException
    {
        try {
            Thread.sleep(DEFAULT_API_LIMIT_RECOVERY_TIME_IN_SECOND * 1000);
        } catch (InterruptedException e) {
            // ignore exception here 
        }
        testCase((args) -> {
            return publicClient.getAllTickers();
        }, false, 50000);
        // We can't reach public API limit
        logger.info("We can't reach public API limit in " + getCxId());
    }

}
