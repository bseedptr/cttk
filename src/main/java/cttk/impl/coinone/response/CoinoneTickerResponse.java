package cttk.impl.coinone.response;

import static cttk.impl.util.DateTimeUtils.zdtFromEpochSecond;

import java.math.BigDecimal;

import cttk.CurrencyPair;
import cttk.dto.Ticker;
import cttk.impl.util.ZoneIds;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CoinoneTickerResponse
    extends CoinoneResponse
{
    String timestamp;
    String currency;

    BigDecimal first;
    BigDecimal last;
    BigDecimal low;
    BigDecimal high;
    BigDecimal volume;

    BigDecimal yesterday_first;
    BigDecimal yesterday_last;
    BigDecimal yesterday_low;
    BigDecimal yesterday_high;
    BigDecimal yesterday_volume;

    public Ticker toTicker(CurrencyPair currencyPair) {
        return new Ticker()
            .setDateTime(zdtFromEpochSecond(Long.parseLong(timestamp), ZoneIds.SEOUL))
            .setCurrencyPair(currencyPair)
            .setOpenPrice(first)
            .setLastPrice(last)
            .setLowPrice(low)
            .setHighPrice(high)
            .setVolume(volume);
    }
}