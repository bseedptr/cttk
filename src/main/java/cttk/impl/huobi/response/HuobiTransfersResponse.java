package cttk.impl.huobi.response;

import java.util.List;
import java.util.stream.Collectors;

import cttk.dto.Transfers;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HuobiTransfersResponse
    extends HuobiResponse
{
    List<HuobiTransfer> data;

    public Transfers getTransfers() {
        return new Transfers()
            .setTransfers(data.stream().map(d -> d.getTransfer()).collect(Collectors.toList()));
    }
}
