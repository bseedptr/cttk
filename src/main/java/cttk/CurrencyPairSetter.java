package cttk;

public interface CurrencyPairSetter
    extends CXIdGettable
{
    default <T extends AbstractCurrencyPair<T>> T setCxIdAndCurrencyPair(T obj, CurrencyPair currencyPair) {
        return obj == null ? null : setCxId(setCurrencyPair(obj, currencyPair));
    }

    default <T extends AbstractCurrencyPair<T>> T setCurrencyPair(T obj, CurrencyPair currencyPair) {
        return obj != null && currencyPair != null && !currencyPair.isBothNull()
            ? obj.setCurrencyPair(currencyPair)
            : obj;
    }
}
