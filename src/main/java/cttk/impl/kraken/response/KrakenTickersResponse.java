package cttk.impl.kraken.response;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.dto.Ticker;
import cttk.dto.Tickers;
import cttk.impl.kraken.KrakenUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class KrakenTickersResponse
    extends KrakenResponse<Map<String, KrakenTicker>>
{
    public Ticker getTicker(KrakenAssetPairs krakenAssetPairs, final CurrencyPair currencyPair) {
        if (currencyPair == null) return null;
        String pair = KrakenUtils.getMarketSymbol(krakenAssetPairs, currencyPair);
        KrakenTicker krakenTicker = this.result.get(pair);
        return krakenTicker == null ? null : krakenTicker.toTicker(currencyPair);
    }

    public Tickers toTickers() {
        return new Tickers().setTickers(getTickers());
    }

    private List<Ticker> getTickers() {
        return this.result.entrySet().stream()
            .map(e -> e.getValue().toTicker(KrakenUtils.parseMarketSymbol(e.getKey())))
            .collect(Collectors.toList());
    }
}

@Data
class KrakenTicker {
    List<BigDecimal> a;  // a = ask array(<price>, <whole lot volume>, <lot volume>)
    List<BigDecimal> b;  // b = bid array(<price>, <whole lot volume>, <lot volume>)
    List<BigDecimal> c;  // c = last trade closed array(<price>, <lot volume>)
    List<BigDecimal> v;  // v = volume array(<today>, <last 24 hours>)
    List<BigDecimal> p;  // p = volume weighted average price array(<today>, <last 24 hours>)
    List<Long> t;  // t = number of trades array(<today>, <last 24 hours>)
    List<BigDecimal> l;  // l = low array(<today>, <last 24 hours>)
    List<BigDecimal> h;  // h = high array(<today>, <last 24 hours>)
    BigDecimal o; //o = today's opening price

    public Ticker toTicker(CurrencyPair currencyPair) {
        return new Ticker()
            .setCurrencyPair(currencyPair)
            .setOpenPrice(o)
            .setLastPrice(c.get(0))
            .setVolume(v.get(1))
            .setAvgPrice(p.get(1))
            .setNumOfTrades(t.get(1))
            .setLowestAsk(a.get(0))
            .setHighestBid(b.get(0))
            .setLowPrice(l.get(1))
            .setHighPrice(h.get(1));

    }
}