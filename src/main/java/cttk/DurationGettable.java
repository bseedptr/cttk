package cttk;

import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface DurationGettable {
    ZonedDateTime getStartDateTime();

    ZonedDateTime getEndDateTime();

    @JsonIgnore
    default boolean hasStartDateTime() {
        return getStartDateTime() != null;
    }

    @JsonIgnore
    default boolean hasEndDateTime() {
        return getEndDateTime() != null;
    }

    @JsonIgnore
    default Long getStartEpochInSecond() {
        return getStartDateTime() == null ? null : getStartDateTime().toEpochSecond();
    }

    @JsonIgnore
    default Long getEndEpochInSecond() {
        return getEndDateTime() == null ? null : getEndDateTime().toEpochSecond();
    }

    @JsonIgnore
    default Long getStartEpochInMilli() {
        return getStartDateTime() == null ? null : getStartDateTime().toInstant().toEpochMilli();
    }

    @JsonIgnore
    default Long getEndEpochInMilli() {
        return getEndDateTime() == null ? null : getEndDateTime().toInstant().toEpochMilli();
    }
}
