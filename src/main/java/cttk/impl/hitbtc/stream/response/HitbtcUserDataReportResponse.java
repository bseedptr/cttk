package cttk.impl.hitbtc.stream.response;

import static cttk.impl.hitbtc.HitbtcUtils.getStatusType;
import static cttk.impl.hitbtc.HitbtcUtils.parseMarketSymbol;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import cttk.OrderSide;
import cttk.PricingType;
import cttk.dto.UserOrder;
import cttk.dto.UserTrade;
import cttk.impl.util.MathUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HitbtcUserDataReportResponse
    extends HitbtcUserDataWebsocketResponse
{
    HitbtcUserDataReport params;

    public UserOrder toUserOrder() {
        return new UserOrder()
            .setCurrencyPair(parseMarketSymbol(params.symbol))
            .setOid(params.id)
            .setClientOrderId(params.clientOrderId)
            .setDateTime(ZonedDateTime.parse(params.createdAt))
            .setSide(OrderSide.of(params.side))
            .setPricingType(PricingType.of(params.type))
            .setStatus(params.status)
            .setStatusType(getStatusType(params.status))
            .setPrice(params.price)
            .setQuantity(params.quantity)
            .setFilledQuantity(params.cumQuantity)
            .setRemainingQuantity(MathUtils.subtract(params.quantity, params.cumQuantity))
            .setTotal(MathUtils.multiply(params.price, params.quantity))
            .setCompletedDateTime(ZonedDateTime.parse(params.updatedAt));
    }

    public UserTrade toUserTrade() {
        return new UserTrade()
            .setCurrencyPair(parseMarketSymbol(params.symbol))
            .setFeeCurrency(parseMarketSymbol(params.symbol).getQuoteCurrency())
            .setOid(params.id)
            .setTid(params.tradeId)
            .setClientOrderId(params.clientOrderId)
            .setDateTime(ZonedDateTime.parse(params.updatedAt))
            .setSide(OrderSide.of(params.side))
            .setStatus(params.status)
            .setPrice(params.tradePrice)
            .setQuantity(params.tradeQuantity)
            .setTotal(MathUtils.multiply(params.tradePrice, params.tradeQuantity))
            .setFee(params.tradeFee);
    }
}

@Data
class HitbtcUserDataReport {
    String id;
    String clientOrderId;
    String symbol;
    String side;
    String status;
    String type;
    String timeInForce;
    BigDecimal price;
    BigDecimal quantity;
    BigDecimal cumQuantity;
    String createdAt;
    String updatedAt;
    BigDecimal stopPrice;
    String expireTime;
    String reportType;
    String tradeId;
    BigDecimal tradeQuantity;
    BigDecimal tradePrice;
    BigDecimal tradeFee;
    String originalRequestClientOrderId;
}
