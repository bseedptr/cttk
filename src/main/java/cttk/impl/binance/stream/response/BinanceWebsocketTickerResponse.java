package cttk.impl.binance.stream.response;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.dto.Ticker;
import cttk.impl.binance.BinanceUtils;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;

@Data
public class BinanceWebsocketTickerResponse {
    @JsonProperty("E")
    Long eventTime;
    @JsonProperty("e")
    String eventType;
    @JsonProperty("s")
    String symbol;
    @JsonProperty("p")
    BigDecimal priceChange;
    @JsonProperty("P")
    BigDecimal priceChangePercent;
    @JsonProperty("w")
    BigDecimal weightedAveragePrice;
    @JsonProperty("c")
    BigDecimal closePrice;
    @JsonProperty("Q")
    BigDecimal closeQuantity;
    @JsonProperty("b")
    BigDecimal bidPrice;
    @JsonProperty("B")
    BigDecimal bidQuantity;
    @JsonProperty("a")
    BigDecimal askPrice;
    @JsonProperty("A")
    BigDecimal askQuantity;
    @JsonProperty("o")
    BigDecimal openPrice;
    @JsonProperty("h")
    BigDecimal highPrice;
    @JsonProperty("l")
    BigDecimal lowPrice;
    @JsonProperty("v")
    BigDecimal totalBaseAssetVolume;
    @JsonProperty("q")
    BigDecimal totalQuoteAssetVolume;
    @JsonProperty("F")
    Long firstTradeId;
    @JsonProperty("L")
    Long lastTradeId;
    @JsonProperty("n")
    Long numOfTrades;

    public Ticker toTicker() {
        try {
            return new Ticker()
                .setCurrencyPair(BinanceUtils.parseMarketSymbol(symbol))
                .setDateTime(DateTimeUtils.zdtFromEpochMilli(eventTime))
                .setHighestBid(bidPrice)
                .setLowestAsk(askPrice)
                .setHighPrice(highPrice)
                .setLowPrice(lowPrice)
                .setOpenPrice(openPrice)
                .setLastPrice(closePrice)
                .setVolume(totalBaseAssetVolume)
                .setQuoteVolume(totalQuoteAssetVolume)
                .setChange(priceChange)
                .setChangePercent(priceChangePercent)
                .setNumOfTrades(numOfTrades);
        } catch (Exception e) {
            return new Ticker();
        }
    }
}
