package cttk.dto;

import java.util.List;

import cttk.AbstractCXIdHolder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Tickers
    extends AbstractCXIdHolder<Tickers>
{
    List<Ticker> tickers;

    public int size() {
        return tickers == null ? 0 : tickers.size();
    }
}
