package cttk.impl.kraken.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class KrakenDepositMethod {
    String method;  //method = name of deposit method
    String limit;   //limit = maximum net amount that can be deposited right now, or false if no limit
    String fee;     //fee = amount of fees that will be paid
    @JsonProperty("gen-address")
    Boolean hasAddressSetupFee; //address-setup-fee = whether or not method has an address setup fee (optional)
}
