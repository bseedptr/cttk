package cttk.impl.poloniex.stream;

import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.PrivateCXClient;
import cttk.UserData;
import cttk.UserDataStreamOpener;
import cttk.exception.CTTKException;
import cttk.request.OpenUserDataStreamRequest;

public class PoloniexUserDataStreamOpener
    implements UserDataStreamOpener
{
    private final PrivateCXClient client;

    public PoloniexUserDataStreamOpener(PrivateCXClient client) {
        this.client = client;
    }

    @Override
    public CXStream<UserData> openUserDataStream(OpenUserDataStreamRequest request, CXStreamListener<UserData> listener)
        throws CTTKException
    {
        return new PoloniexPollingUserDataStream(client, request, listener);
    }
}
