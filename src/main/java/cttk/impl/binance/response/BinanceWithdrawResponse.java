package cttk.impl.binance.response;

import cttk.dto.WithdrawResult;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BinanceWithdrawResponse
    extends BinanceResultResponse
{
    String id;

    public WithdrawResult getWithdrawResult() {
        return new WithdrawResult().setWithdrawId(id);
    }
}
