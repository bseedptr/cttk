package cttk.impl.bithumb.response;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.dto.Ticker;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.ZoneIds;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BithumbTickerResponse
    extends BithumbDataResponse<BithumbTicker>
{
    public Ticker toTicker() {
        return new Ticker()
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(data.date, ZoneIds.SEOUL))
            .setOpenPrice(data.opening_price)
            .setLastPrice(data.closing_price)
            .setLowPrice(data.min_price)
            .setHighPrice(data.max_price)
            .setAvgPrice(data.average_price)
            .setVolume(data.units_traded)
            .setVolume7day(data.volume_7day)
            .setHighestBid(data.buy_price)
            .setLowestAsk(data.sell_price)
            .setChange(data.fluctate24h)
            .setChangePercent(data.fluctate24hPercent);
    }
}

@Data
class BithumbTicker {
    BigDecimal opening_price;
    BigDecimal closing_price;
    BigDecimal min_price;
    BigDecimal max_price;
    BigDecimal average_price;
    BigDecimal units_traded;
    BigDecimal volume_1day;
    BigDecimal volume_7day;
    BigDecimal buy_price;
    BigDecimal sell_price;
    @JsonProperty("24H_fluctate")
    BigDecimal fluctate24h;
    @JsonProperty("24H_fluctate_rate")
    BigDecimal fluctate24hPercent;
    Long date;
}