package cttk.impl.upbit;

import cttk.CurrencyPair;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import org.junit.Assert;
import org.junit.Test;

public class UpbitUtilsTest {
    @Test
    public void testParseMarketSymbol() {
        final CurrencyPair currencyPair = UpbitUtils.parseMarketSymbol("KRW-EOS");
        Assert.assertEquals("EOS", currencyPair.getBaseCurrency());
        Assert.assertEquals("KRW", currencyPair.getQuoteCurrency());
    }

    @Test(expected = CXAPIRequestException.class)
    public void testException0()
        throws CTTKException
    {
        UpbitUtils.checkErrorByResponseMessage(null, "", null);
        String text = "{\n"
            + "    \"error\": {\n"
            + "    \"name\":\"error name\",\n"
            + "        \"message\":\"error message\"\n"
            + "}\n"
            + "}";
        UpbitUtils.checkErrorByResponseMessage(null, text, 400);
    }

    @Test(expected = CXAPIRequestException.class)
    public void testException1()
        throws CTTKException
    {
        UpbitUtils.checkErrorByResponseMessage(null, "", null);
        String text = "{\"error\":{\"name\":\"WRONG_FORMAT\",\"message\":\"Format이 맞지 않습니다.\"}}";
        UpbitUtils.checkErrorByResponseMessage(null, text, 400);
    }

    @Test
    public void testException2()
        throws CTTKException
    {
        String text1 = "{\"type\":\"trade\",\"code\":\"KRW-BTC\",\"timestamp\":1544187442669,\"trade_date\":\"2018-12-07\",\"trade_time\":\"12:57:22\",\"trade_timestamp\":1544187442000,\"trade_price\":3845000.0,\"trade_volume\":0.32012067,\"ask_bid\":\"ASK\",\"prev_closing_price\":3965000.00000000,\"change\":\"FALL\",\"change_price\":120000.00000000,\"sequential_id\":1544187442000003,\"stream_type\":\"SNAPSHOT\"}";
        UpbitUtils.checkErrorByResponseMessage(null, text1, null);
        String text2 = "{}";
        UpbitUtils.checkErrorByResponseMessage(null, text2, null);
        String text3 = "[]";
        UpbitUtils.checkErrorByResponseMessage(null, text3, null);
    }
}
