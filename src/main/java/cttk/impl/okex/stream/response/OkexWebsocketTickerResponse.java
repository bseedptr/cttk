package cttk.impl.okex.stream.response;

import java.math.BigDecimal;

import cttk.dto.Ticker;
import cttk.impl.okex.OkexUtilsV3;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class OkexWebsocketTickerResponse
    extends OkexWebsocketResponse<OkexWebsocketTicker>
{
    public Ticker toTicker() {
        return new Ticker()
            .setCurrencyPair(OkexUtilsV3.toStdMarketSymbolV3(unwrap().instrument_id))
            .setDateTime(DateTimeUtils.parseZdt(unwrap().timestamp))
            .setOpenPrice(unwrap().open_24h)
            .setLastPrice(unwrap().last)
            .setHighPrice(unwrap().high_24h)
            .setLowPrice(unwrap().low_24h)
            .setHighestBid(unwrap().best_bid)
            .setLowestAsk(unwrap().best_ask)
            .setVolume(unwrap().base_volume_24h)
            .setQuoteVolume(unwrap().quote_volume_24h);
    }
}

@Data
class OkexWebsocketTicker {
    String instrument_id;
    BigDecimal last;
    BigDecimal best_bid;
    BigDecimal best_ask;
    BigDecimal open_24h;
    BigDecimal high_24h;
    BigDecimal low_24h;
    BigDecimal base_volume_24h;
    BigDecimal quote_volume_24h;
    String timestamp;
}
