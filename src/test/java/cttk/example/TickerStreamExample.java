package cttk.example;

import java.util.List;

import cttk.CTTK;
import cttk.CXClientFactory;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.CXStreamOpener;
import cttk.CurrencyPair;
import cttk.dto.Ticker;
import cttk.exception.UnsupportedCXException;
import cttk.exception.UnsupportedMethodException;
import cttk.request.OpenTickerStreamRequest;
import cttk.test.TestConfig;
import cttk.util.ReopenPolicy;

public class TickerStreamExample {
    public static void main(String[] args)
        throws UnsupportedCXException
    {
        List<String> cxIds = CTTK.getSupportedCxIds();
        for (String cxId : cxIds) {
            try {
                final CXStreamOpener opener = CXClientFactory.createCXStreamOpener(cxId);
                final TestConfig testConfig = TestConfig.readFromClasspathYaml("config/" + cxId.toLowerCase() + ".yml");
                final CurrencyPair currencyPair = testConfig.getValidCurrencyPair();
                final CXStream<Ticker> stream = opener.openTickerStream(
                    new OpenTickerStreamRequest().setCurrencyPair(currencyPair),
                    createPrintTicker(),
                    ReopenPolicy.createDefault());

                Thread.sleep(2000);
                stream.close();
            } catch (UnsupportedMethodException e) {
                continue;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static CXStreamListener<Ticker> createPrintTicker() {
        return new CXStreamListener<Ticker>() {

            @Override
            public void onMessage(Ticker ticker) {
                System.out.println(ticker.getCxId());
                System.out.println(ticker.getPairString());
                System.out.println(ticker.getLastPrice());
                System.out.println(ticker.getVolume());
                System.out.println(ticker.getQuoteVolume());
                System.out.println(ticker.getChangePercent());
                System.out.println("========================================");
            }

            @Override
            public void onFailure(CXStream<Ticker> stream, Throwable t) {
                t.printStackTrace();
            }
        };
    }
}
