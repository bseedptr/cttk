package cttk;

import java.util.List;

import cttk.dto.ChargingPolicy;
import cttk.exception.CTTKException;

public interface ChargingPolicyProvider {
    List<ChargingPolicy> getChargingPolicies(CurrencyPair currencyPair, OrderSide orderSide)
        throws CTTKException;
}
