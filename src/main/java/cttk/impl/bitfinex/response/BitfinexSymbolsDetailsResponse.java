package cttk.impl.bitfinex.response;

import java.math.BigDecimal;

import cttk.dto.MarketInfo;
import cttk.impl.bitfinex.BitfinexUtils;
import lombok.Data;

@Data
public class BitfinexSymbolsDetailsResponse {
    String pair;
    Integer price_precision;
    BigDecimal initial_margin;
    BigDecimal minimum_margin;
    BigDecimal maximum_order_size;
    BigDecimal minimum_order_size;
    String expiration;
    Boolean margin;

    public MarketInfo toMarketInfo() {
        return new MarketInfo()
            .setCurrencyPair(BitfinexUtils.parseMarketSymbol(pair))
            .setPricePrecision(price_precision)
            .setMinQuantity(minimum_order_size)
            .setMaxQuantity(maximum_order_size);
    }
}
