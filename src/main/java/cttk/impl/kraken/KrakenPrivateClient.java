package cttk.impl.kraken;

import static cttk.CXIds.KRAKEN;
import static cttk.dto.UserOrders.Category.ALL;
import static cttk.dto.UserOrders.Category.CLOSED;
import static cttk.dto.UserOrders.Category.OPEN;
import static cttk.impl.kraken.KrakenUtils.getMarketSymbol;
import static cttk.util.DeltaUtils.sleepWithException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import cttk.exception.InsufficientBalanceException;
import cttk.exception.InvalidAmountRangeException;
import cttk.exception.InvalidPriceIncrementPrecisionException;
import cttk.exception.InvalidPriceMaxException;
import cttk.exception.InvalidPriceRangeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cttk.CurrencyPair;
import cttk.PrivateCXClient;
import cttk.TradingFeeProvider;
import cttk.auth.Credential;
import cttk.dto.Balance;
import cttk.dto.Balances;
import cttk.dto.BankAccount;
import cttk.dto.TradingFee;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.dto.UserAccount;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;
import cttk.dto.Wallet;
import cttk.dto.Wallets;
import cttk.dto.WithdrawResult;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXServerException;
import cttk.exception.InvalidOrderException;
import cttk.exception.InvalidWithdrawException;
import cttk.exception.OrderNotFoundException;
import cttk.exception.UnsupportedCurrencyException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.feeprovider.CachedTradingFeeProvider;
import cttk.impl.kraken.response.KrakenBalanceResponse;
import cttk.impl.kraken.response.KrakenCancelOrderResponse;
import cttk.impl.kraken.response.KrakenCreateOrderResponse;
import cttk.impl.kraken.response.KrakenDepositAddressesResponse;
import cttk.impl.kraken.response.KrakenDepositMethod;
import cttk.impl.kraken.response.KrakenDepositMethodsResponse;
import cttk.impl.kraken.response.KrakenQueryUserOrdersResponse;
import cttk.impl.kraken.response.KrakenQueryUserTradesResponse;
import cttk.impl.kraken.response.KrakenTradeBalanceResponse;
import cttk.impl.kraken.response.KrakenTradeVolumeResponse;
import cttk.impl.kraken.response.KrakenTransfersResponse;
import cttk.impl.kraken.response.KrakenUserOrdersResponse;
import cttk.impl.kraken.response.KrakenUserTradesResponse;
import cttk.impl.kraken.response.KrakenWithdrawResponse;
import cttk.impl.util.ArrayUtils;
import cttk.impl.util.CryptoUtils;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ParamsBuilder;
import cttk.impl.util.UriUtils;
import cttk.request.CancelOrderRequest;
import cttk.request.CreateMarketPriceOrderRequest;
import cttk.request.CreateOrderRequest;
import cttk.request.GetBankAccountRequest;
import cttk.request.GetOpenUserOrdersRequest;
import cttk.request.GetTransfersDeltaRequest;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserAccountRequest;
import cttk.request.GetUserOrderHistoryDeltaRequest;
import cttk.request.GetUserOrderHistoryRequest;
import cttk.request.GetUserOrderRequest;
import cttk.request.GetUserTradesDeltaRequest;
import cttk.request.GetUserTradesRequest;
import cttk.request.GetWalletRequest;
import cttk.request.WithdrawToBankRequest;
import cttk.request.WithdrawToWalletRequest;
import cttk.util.DeltaUtils;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;

public class KrakenPrivateClient
    extends AbstractKrakenClient
    implements PrivateCXClient
{
    protected static final String VERSION = "0";
    protected static final String PRIVATE_API_BASE_URL = "https://api.kraken.com/" + VERSION + "/private/";

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    protected final Credential credential;

    public KrakenPrivateClient(Credential credential) {
        super();
        logger.debug("{}", credential);
        this.credential = credential;
    }

    @Override
    public UserAccount getUserAccount(GetUserAccountRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public TradingFeeProvider getTradingFeeProvider()
        throws CTTKException
    {
        return new CachedTradingFeeProvider(new TradingFeeProvider() {
            @Override
            public TradingFee getFee(CurrencyPair currencyPair)
                throws CTTKException
            {
                // https://api.kraken.com/0/private/TradeVolume
                final HttpResponse response = requestPost("TradeVolume",
                    ParamsBuilder.create()
                        .putIfNotNull("pair", getMarketSymbol(getAssetPairs(), currencyPair))
                        .put("fee-info", "true")
                        .build());

                checkResponseError(currencyPair, response);

                return convert(response, KrakenTradeVolumeResponse.class, i -> i.getTradingFee(getAssetPairs(), currencyPair));
            }
        });

    }

    @Override
    public BankAccount getBankAccount(GetBankAccountRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public WithdrawResult withdraw(WithdrawToBankRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public Wallet getWallet(GetWalletRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return getKrakenWallet(request.getCurrency(), KrakenUtils.toKrakenAssetSymbol(request.getCurrency()));
    }

    private Wallet getKrakenWallet(final String currency, final String krakenAssetSymbol)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response1 = requestPost("DepositMethods",
            ParamsBuilder.create()
                .putIfNotNull("aclass", "currency")
                .putIfNotNull("asset", krakenAssetSymbol)
                .build());
        checkResponseError(null, response1);

        final List<KrakenDepositMethod> depositMethods = convert(response1, KrakenDepositMethodsResponse.class).getResult();

        if (depositMethods == null || depositMethods.isEmpty()) {
            throw new UnsupportedCurrencyException(currency);
        }

        final KrakenDepositMethod depositMethod = depositMethods.get(0);
        if (depositMethods.size() > 1) {
            logger.info("multiple deposit methods : {}", krakenAssetSymbol);
        }

        // TODO control TPS
        /*try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            throw new CTTKException("getKrakenWallet should wait 500 ms between call. but thread was interrupted", e);
        }*/

        final HttpResponse response2 = requestPost("DepositAddresses",
            ParamsBuilder.create()
                .putIfNotNull("aclass", "currency")
                .putIfNotNull("asset", krakenAssetSymbol)
                .putIfNotNull("method", depositMethod.getMethod())
                .putIfNotNull("new", "0")   // be cautious. "false" is considered as true.
                .build());
        checkResponseError(null, response2);

        return convert(response2, KrakenDepositAddressesResponse.class, i -> i.toWallet(currency));
    }

    @Override
    public Wallets getAllWallets()
        throws CTTKException
    {
        final HttpResponse response = requestPost("Balance");
        checkResponseError(null, response);

        final Balances balances = convert(response, KrakenBalanceResponse.class, i -> i.toBalances());

        // TODO control TPS
        //            try {
        //                Thread.sleep(200);
        //            } catch (InterruptedException e1) {
        //                throw new CTTKException(e1);
        //            }

        final List<Wallet> uwl = new ArrayList<>(balances.size());
        for (Balance ub : balances.getBalances()) {
            // TODO check later
            if (ub.getCurrency().equals("USD")) continue;
            try {
                uwl.add(getKrakenWallet(ub.getCurrency(), KrakenUtils.toKrakenAssetSymbol(ub.getCurrency())));
            } catch (CTTKException e) {
                if (e.getMessage().contains("No funding method")) {
                    logger.info("No funding method for {}", ub.getCurrency());
                } else {
                    throw e;
                }
            }
        }

        return new Wallets().setWallets(uwl);
    }

    @Override
    public WithdrawResult withdraw(WithdrawToWalletRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        if (!request.hasWithdrawlKey()) {
            throw new CTTKException("withdrawlKey should be set").setCxId(this.getCxId());
        }

        final HttpResponse response = requestPost("Withdraw",
            ParamsBuilder.create()
                .putIfNotNull("asset", KrakenUtils.toKrakenAssetSymbol(request.getCurrency()))
                .putIfNotNull("key", request.getWithdrawlKey()) // Kraken require 'key' instead of address
                .putIfNotNull("amount", request.getQuantity())
                .build());

        checkWithdrawResponseError(request, response);
        checkResponseError(request, response);

        return convert(response, KrakenWithdrawResponse.class).toWithdrawResult();
    }

    Balance fillBalance(Balance balance)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("TradeBalance",
            ParamsBuilder.build("asset", KrakenUtils.toKrakenAssetSymbol(balance.getCurrency())));

        checkResponseError(null, response);

        final KrakenTradeBalanceResponse ktbr = convert(response, KrakenTradeBalanceResponse.class);
        return ktbr.fillBalance(balance);
    }

    @Override
    public Balances getAllBalances()
        throws CTTKException
    {
        final HttpResponse response = requestPost("Balance");

        checkResponseError(null, response);

        final Balances balances = convert(response, KrakenBalanceResponse.class, i -> i.toBalances());

        // TODO check if additional data are required
        //        if (balances.hasBalances()) {
        //            for (Balance b : balances.getBalances()) {
        //                fillBalance(b);
        //            }
        //        }

        return balances;
    }

    @Override
    public UserOrder getUserOrder(GetUserOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("QueryOrders",
            ParamsBuilder.create()
                .put("txid", request.getOid())
                .putIfNotNull("trades", request.isWithTrades())
                .build());
        try {
            checkResponseError(request, response);
        } catch (OrderNotFoundException e) {
            return null;
        }
        final UserOrder order = convert(response, KrakenQueryUserOrdersResponse.class, i -> i.toUserOrder());

        if (request.isWithTrades()) {
            final List<String> tids = order.getTids();
            final Map<String, UserTrade> map = getUserTradeMap(tids);
            order.setTrades(filterUserTrade(tids, map));
        }

        return order;
    }

    private Map<String, UserTrade> getUserTradeMap(Collection<String> tids)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        if (tids == null || tids.isEmpty()) return null;
        final HttpResponse response = requestPost("QueryTrades",
            ParamsBuilder.build("txid", String.join(",", tids)));

        checkResponseError(null, response);

        return convert(response, KrakenQueryUserTradesResponse.class, i -> i.toUserTradeMap());
    }

    private static final List<UserTrade> filterUserTrade(Collection<String> tids, Map<String, UserTrade> map) {
        if (tids == null || map == null) return null;
        final List<UserTrade> ret = new ArrayList<>();
        tids.forEach(tid -> {
            if (map.containsKey(tid)) ret.add(map.get(tid));
        });
        return ret;
    }

    @Override
    public UserOrders getOpenUserOrders(GetOpenUserOrdersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // TODO [CTTK-457] getOpenUserOrders of Kraken does not properly handle wrong currency for now.
        // It should be refined with Kraken market meta data
        final UserOrders userOrders = getAllOpenUserOrders(request.isWithTrades());

        return userOrders == null ? null
            : userOrders.filterByCurrencyPair(request);
    }

    @Override
    public UserOrders getAllOpenUserOrders(boolean withTrades)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("OpenOrders",
            ParamsBuilder.buildIfNotNull("trades", withTrades));

        checkResponseError(null, response);

        return convert(response, KrakenUserOrdersResponse.class, i -> i.toUserOrdersFromOpen());
    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // TODO only closed orders?,
        final HttpResponse response = requestPost("ClosedOrders",
            ParamsBuilder.create()
                .putIfNotNull("trades", request.isWithTrades())
                .putIfNotNull("start", request.getStartEpochInSecond())
                .putIfNotNull("end", request.getEndEpochInSecond())
                .putIfNotNull("ofs", request.getOffset())
                .build());
        try {
            checkResponseError(request, response);
        } catch (OrderNotFoundException e) {
            return new UserOrders().setCategory(OPEN);
        }

        final UserOrders userOrders = convert(response, KrakenUserOrdersResponse.class, i -> i.toUserOrdersFromClosed());

        // Filter out wrong currencyPairs
        CurrencyPair currencyPair = request.toSimpleCurrencyPair();
        if (currencyPair == null || currencyPair.isBothNull()) {
            return userOrders.setCategory(CLOSED);
        } else {
            return userOrders.setCategory(CLOSED).filterByCurrencyPair(currencyPair);
        }
    }

    private UserOrders getUserOrderHistoryById(GetUserOrderHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("ClosedOrders",
            ParamsBuilder.create()
                .putIfNotNull("trades", request.isWithTrades())
                .putIfNotNull("start", request.getFromId())
                .putIfNotNull("end", request.getTillId())
                .putIfNotNull("ofs", request.getOffset())
                .build());
        try {
            checkResponseError(request, response);
        } catch (OrderNotFoundException e) {
            return new UserOrders().setCategory(OPEN);
        }

        final UserOrders userOrders = convert(response, KrakenUserOrdersResponse.class, i -> i.toUserOrdersFromClosed());

        // Filter out wrong currencyPairs
        CurrencyPair currencyPair = request.toSimpleCurrencyPair();
        if (currencyPair == null || currencyPair.isBothNull()) {
            return userOrders.setCategory(CLOSED);
        } else {
            return userOrders.setCategory(CLOSED).filterByCurrencyPair(currencyPair);
        }
    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP t-d: time-desc,<<<(exception)
        // GROUP i-d: id-desc,<<<(exception)
        if (request.hasStartDateTime()) {
            final GetUserOrderHistoryRequest partialRequest = new GetUserOrderHistoryRequest()
                .setCurrencyPair(request)
                .setStartDateTime(request.getStartDateTime().minusSeconds(1)) // Since kraken excludes start data, we need to adjust start data
                .setEndDateTime(request.getEndDateTime());
            // Sorting in desc order implicitly

            final Map<String, UserOrder> map = new LinkedHashMap<>();
            UserOrder prevFirst = null;
            UserOrder prevLast = null;
            while (true) {
                final UserOrders userOrders = getUserOrderHistory(partialRequest);
                final UserOrder first = userOrders.getOldest();
                final UserOrder last = userOrders.getLatest();

                if (userOrders.isEmpty() || UserOrder.equalsById(prevFirst, first)) break;
                map.putAll(DeltaUtils.convertToMap(userOrders));

                final int s = DeltaUtils.calcNextSeconds(prevLast, last);
                partialRequest.setEndDateTime(first.getDateTime().plusSeconds(s));
                prevFirst = first;
                prevLast = last;

                sleepWithException(request.getSleepTimeMillis(), KRAKEN);
            }

            return DeltaUtils.convertUserOrdersFromMap(map)
                .filterByDateTimeGTE(request.getStartDateTime()) // Exclude adjusted start data above
                .setCategory(ALL);
        } else if (request.hasFromId()) {
            // There's no way to get very first data for now, since Kraken exclude the fromId
            // Also it is impossible go back to the past by fromId, because fromId is just a 'string' so we can not estimate it
            final GetUserOrderHistoryRequest partialRequest = new GetUserOrderHistoryRequest()
                .setCurrencyPair(request)
                .setFromId(request.getFromId());

            final UserOrders userOrders = new UserOrders();
            final UserOrders firstUserOrders = getUserOrderHistory(partialRequest);
            String tillId = firstUserOrders.getLatest().getOid();
            while (true) {
                partialRequest.setTillId(tillId);
                final UserOrders tempUserOrders = getUserOrderHistoryById(partialRequest);

                if (tempUserOrders.isEmpty()) break;
                userOrders.addAll(tempUserOrders);
                tillId = tempUserOrders.getOldest().getOid();

                sleepWithException(request.getSleepTimeMillis(), KRAKEN);
            }

            return userOrders.setCategory(CLOSED);
        } else {
            return getUserOrderHistory(new GetUserOrderHistoryRequest().setCurrencyPair(request));
        }
    }

    @Override
    public UserOrder createOrder(CreateOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        //pair = asset pair
        //type = type of order (buy/sell)
        //ordertype = order type:
        //    market
        //    limit (price = limit price)
        //    stop-loss (price = stop loss price)
        //    take-profit (price = take profit price)
        //    stop-loss-profit (price = stop loss price, price2 = take profit price)
        //    stop-loss-profit-limit (price = stop loss price, price2 = take profit price)
        //    stop-loss-limit (price = stop loss trigger price, price2 = triggered limit price)
        //    take-profit-limit (price = take profit trigger price, price2 = triggered limit price)
        //    trailing-stop (price = trailing stop offset)
        //    trailing-stop-limit (price = trailing stop offset, price2 = triggered limit offset)
        //    stop-loss-and-limit (price = stop loss price, price2 = limit price)
        //    settle-position
        //price = price (optional.  dependent upon ordertype)
        //price2 = secondary price (optional.  dependent upon ordertype)
        //volume = order volume in lots
        //leverage = amount of leverage desired (optional.  default = none)
        //oflags = comma delimited list of order flags (optional):
        //    viqc = volume in quote currency (not available for leveraged orders)
        //    fcib = prefer fee in base currency
        //    fciq = prefer fee in quote currency
        //    nompp = no market price protection
        //    post = post only order (available when ordertype = limit)
        //starttm = scheduled start time (optional):
        //    0 = now (default)
        //    +<n> = schedule start time <n> seconds from now
        //    <n> = unix timestamp of start time
        //expiretm = expiration time (optional):
        //    0 = no expiration (default)
        //    +<n> = expire <n> seconds from now
        //    <n> = unix timestamp of expiration time
        //userref = user reference id.  32-bit signed number.  (optional)
        //validate = validate inputs only.  do not submit order (optional)
        //
        //optional closing order to add to system when order gets filled:
        //    close[ordertype] = order type
        //    close[price] = price
        //    close[price2] = secondary price
        final HttpResponse response = requestPost("AddOrder",
            ParamsBuilder.create()
                .putIfNotNull("pair", getMarketSymbol(getAssetPairs(), request))
                .putIfNotNull("type", KrakenUtils.toString(request.getSide()))
                .put("ordertype", "limit")
                .putIfNotNull("price", request.getPrice())
                .putIfNotNull("volume", request.getQuantity())
                .build());

        checkOrderResponseError(request, response);
        checkResponseError(request, response);

        return fillResponseByRequest(request, convert(response, KrakenCreateOrderResponse.class, i -> i.toUserOrder()));
    }

    @Override
    public void cancelOrder(CancelOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // Static order id format check for Kraken (CTTK-397)
        if (!KrakenUtils.checkOrderIDFormat(request.getOrderId())) {
            throw new CTTKException("Order id is not in the right format")
                .setCxId(this.getCxId());
        }

        final HttpResponse response = requestPost("CancelOrder",
            ParamsBuilder.build("txid", request.getOrderId()));

        checkResponseError(request, response);

        convert(response, KrakenCancelOrderResponse.class);
    }

    @Override
    public UserOrder createMarketPriceOrder(CreateMarketPriceOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("AddOrder",
            ParamsBuilder.create()
                .putIfNotNull("pair", getMarketSymbol(getAssetPairs(), request))
                .putIfNotNull("type", KrakenUtils.toString(request.getSide()))
                .put("ordertype", "market")
                .putIfNotNull("volume", request.getQuantity())
                .build());

        checkResponseError(request, response);

        return convert(response, KrakenCreateOrderResponse.class, i -> i.toUserOrder());
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("TradesHistory",
            ParamsBuilder.create()
                .put("type", "all")         // TODO refine here
                .put("trades", "true")
                .putIfNotNull("ofs", request.getOffset())
                .putIfNotNull("start", request.getStartEpochInSecond())
                .putIfNotNull("end", request.getEndEpochInSecond())
                .build());

        checkResponseError(request, response);

        final UserTrades trades = convert(response, KrakenUserTradesResponse.class, i -> i.toUserTrades());
        trades.filterByCurrencyPair(request);

        return trades;
    }

    private UserTrades getUserTradesByFromId(GetUserTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("TradesHistory",
            ParamsBuilder.create()
                .put("type", "all")
                .put("trades", "true")
                .putIfNotNull("ofs", request.getOffset())
                .putIfNotNull("start", request.getFromId())
                .putIfNotNull("end", request.getTillId())
                .build());

        checkResponseError(request, response);

        final UserTrades trades = convert(response, KrakenUserTradesResponse.class, i -> i.toUserTrades());
        trades.filterByCurrencyPair(request);

        return trades;
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP t-d: time-desc,<<<(exception)
        // GROUP i-d: id-desc,<<<(exception)
        if (request.hasStartDateTime()) {
            final GetUserTradesRequest partialRequest = new GetUserTradesRequest()
                .setCurrencyPair(request)
                .setStartDateTime(request.getStartDateTime().minusHours(1))
                .setEndDateTime(request.getEndDateTime());
            // Sorting in desc order implicitly

            final Map<String, UserTrade> map = new LinkedHashMap<>();
            UserTrade prevFirst = null;
            UserTrade prevLast = null;
            while (true) {
                final UserTrades userTrades = getUserTrades(partialRequest);
                final UserTrade first = userTrades.getOldest();
                final UserTrade last = userTrades.getLatest();

                if (userTrades.isEmpty() || UserTrade.equalsById(prevFirst, first)) break;
                map.putAll(DeltaUtils.convertToMap(userTrades));

                final int s = DeltaUtils.calcNextSeconds(prevLast, last);
                partialRequest.setEndDateTime(first.getDateTime().plusSeconds(s));
                prevFirst = first;
                prevLast = last;

                sleepWithException(request.getSleepTimeMillis(), KRAKEN);
            }

            return DeltaUtils.convertUserTradesFromMap(map).filterByDateTimeGTE(request.getStartDateTime()); // Exclude adjusted start data above
        } else if (request.hasFromId()) {
            // There's no way to get very first data for now, since Kraken exclude the fromId
            // Also it is impossible go back to the past by fromId, because fromId is just a 'string' so we can not estimate it
            final GetUserTradesRequest partialRequest = new GetUserTradesRequest()
                .setCurrencyPair(request)
                .setFromId(request.getFromId());

            final UserTrades userTrades = new UserTrades();
            final UserTrades firstUserTrades = getUserTradesByFromId(partialRequest);
            String tillId = firstUserTrades.getLatest().getOid();
            while (true) {
                partialRequest.setTillId(tillId);
                final UserTrades tempUserTrades = getUserTradesByFromId(partialRequest);

                if (tempUserTrades.isEmpty()) break;
                userTrades.addAll(tempUserTrades);
                tillId = tempUserTrades.getLatest().getOid();

                sleepWithException(request.getSleepTimeMillis(), KRAKEN);
            }

            return userTrades;
        } else {
            return getUserTrades(new GetUserTradesRequest().setCurrencyPair(request));
        }
    }

    @Override
    public Transfers getTransfers(GetTransfersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("Ledgers",
            ParamsBuilder.create()
                .put("aclass", "currency")
                .put("asset", KrakenUtils.toKrakenAssetSymbol(request.getCurrency()))
                .put("type", "all")         // TODO refine here
                .putIfNotNull("start", request.getStartEpochInSecond())
                .putIfNotNull("end", request.getEndEpochInSecond())
                .putIfNotNull("ofs", request.getOffset())
                .build());

        checkResponseError(request, response);

        final Transfers transfers = convert(response, KrakenTransfersResponse.class, i -> i.toTransfers());

        return transfers;
    }

    private Transfers getTransfersByFromId(GetTransfersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("Ledgers",
            ParamsBuilder.create()
                .put("aclass", "currency")
                .put("asset", KrakenUtils.toKrakenAssetSymbol(request.getCurrency()))
                .put("type", "all")
                .putIfNotNull("start", request.getFromId())
                .putIfNotNull("end", request.getTillId())
                .putIfNotNull("ofs", request.getOffset())
                .build());

        checkResponseError(request, response);

        final Transfers transfers = convert(response, KrakenTransfersResponse.class, i -> i.toTransfers());

        return transfers;
    }

    @Override
    public Transfers getTransfers(GetTransfersDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP t-d: time-desc,<<<(exception)
        // GROUP i-d: id-desc,<<<(exception)
        if (request.hasStartDateTime()) {
            final GetTransfersRequest partialRequest = new GetTransfersRequest()
                .setCurrency(request)
                .setStartDateTime(request.getStartDateTime().minusSeconds(1)) // Since kraken excludes start data, we need to adjust start data
                .setEndDateTime(request.getEndDateTime());
            // Sorting in desc order implicitly

            final Map<String, Transfer> map = new LinkedHashMap<>();
            Transfer prevFirst = null;
            Transfer prevLast = null;
            while (true) {
                final Transfers transfers = getTransfers(partialRequest);
                final Transfer first = transfers.getOldest();
                final Transfer last = transfers.getLatest();

                if (transfers.isEmpty() || Transfer.equalsById(prevFirst, first)) break;
                map.putAll(DeltaUtils.convertToMap(transfers));

                final int s = DeltaUtils.calcNextSeconds(prevLast, last);
                partialRequest.setEndDateTime(first.getDateTime().plusSeconds(s));
                prevFirst = first;
                prevLast = last;

                sleepWithException(request.getSleepTimeMillis(), KRAKEN);
            }

            return DeltaUtils.convertTransfersFromMap(map).filterByDateTimeGTE(request.getStartDateTime()); // Exclude adjusted start data above
        } else if (request.hasFromId()) {
            // There's no way to get very first data for now, since Kraken exclude the fromId
            // Also it is impossible go back to the past by fromId, because fromId is just a 'string' so we can not estimate it
            final GetTransfersRequest partialRequest = new GetTransfersRequest()
                .setCurrency(request)
                .setFromId(request.getFromId());

            final Transfers transfers = new Transfers();
            final Transfers firstTransfers = getTransfersByFromId(partialRequest);
            String tillId = firstTransfers.getLatest().getTid();
            while (true) {
                partialRequest.setTillId(tillId);
                final Transfers tempTransfers = getTransfersByFromId(partialRequest);

                if (tempTransfers.isEmpty()) break;
                transfers.addAll(tempTransfers);
                tillId = tempTransfers.getLatest().getTid();

                sleepWithException(request.getSleepTimeMillis(), KRAKEN);
            }

            return transfers;
        } else {
            return getTransfers(new GetTransfersRequest().setCurrency(request));
        }
    }

    private HttpResponse requestPost(final String method)
        throws CTTKException
    {
        return requestPost(method, null);
    }

    private HttpResponse requestPost(final String method, Map<String, String> params)
        throws CTTKException
    {
        params = putNonce(params);

        final HttpUrl.Builder builder = HttpUrl.parse(PRIVATE_API_BASE_URL).newBuilder().addPathSegments(method);

        final HttpUrl url = builder.build();
        final String path = "/" + String.join("/", url.pathSegments());
        final String uri = url.toString();

        final Request request = new Request.Builder()
            .url(uri)
            .headers(createHttpHeaders(credential, path, params))
            .post(super.createEncodedFormBody(params))
            .build();

        debug(request, params);

        try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
            return toHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    private Map<String, String> putNonce(Map<String, String> params) {
        if (params == null) params = new HashMap<>();
        params.put("nonce", nonce());
        return params;
    }

    private static String nonce() {
        return String.valueOf(System.currentTimeMillis());
    }

    private Headers createHttpHeaders(Credential credential, String path, Map<String, String> params) {
        String postData = UriUtils.toEncodedQueryParamsString(params);
        final byte[] encoded = (params.get("nonce") + postData).getBytes();
        final byte[] message = ArrayUtils.concat(path.getBytes(), CryptoUtils.sha256(encoded));
        final byte[] key = Base64.getDecoder().decode(credential.getSecretKey());
        String signature = Base64.getEncoder().encodeToString(CryptoUtils.hmacSha512(message, key));

        return new Headers.Builder()
            .add("Api-Key", credential.getAccessKey())
            .add("Api-Sign", signature)
            .build();
    }

    private void checkWithdrawResponseError(WithdrawToWalletRequest request, HttpResponse response)
        throws CTTKException
    {
        try {
            @SuppressWarnings("unchecked")
            Map<String, List<String>> map = objectMapper.readValue(response.getBody(), HashMap.class);
            List<String> errors = map.get("error");
            if (!errors.isEmpty()) {
                String message = errors.get(0);
                throw new InvalidWithdrawException()
                    .setCxId(this.getCxId())
                    .setWithdrwalKey(request.getWithdrawlKey())
                    .setWithdrawalCurrency(request.getCurrency())
                    .setWithdrawalAmount(request.getQuantity())
                    .setExchangeMessage(message)
                    .setResponseStatusCode(response.getStatus());
            }
        } catch (IOException e) {
            throw new CXAPIRequestException("json parse error ", e)
                .setCxId(this.getCxId())
                .setResponseStatusCode(response.getStatus())
                .setExchangeMessage(response.getBody());
        }
    }

    private void checkOrderResponseError(CreateOrderRequest request, HttpResponse response)
        throws CTTKException
    {
        final String body = response.getBody();

        if (body.contains("EOrder:Invalid price:") && body.contains("price can only be specified up to")) {
            throw new InvalidPriceIncrementPrecisionException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(response.getStatus())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity());
        } else if (body.contains("EGeneral:Invalid arguments:price")
            || body.contains("EOrder:Invalid price"))
        {
            throw new InvalidPriceRangeException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(response.getStatus())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity());
        } else if (body.contains("EGeneral:Invalid arguments:volume")) {
            throw new InvalidAmountRangeException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(response.getStatus())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity());
        } else if (body.contains("EOrder:Order minimum not met")) {
            throw new InvalidOrderException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(response.getStatus())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity());
        }
        // Not enough balance for the order
        else if (body.contains("EOrder:Insufficient")) {
            throw new InsufficientBalanceException()
                .setCxId(getCxId())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity())
                .setExchangeMessage(body)
                .setResponseStatusCode(response.getStatus());
        }
    }
}