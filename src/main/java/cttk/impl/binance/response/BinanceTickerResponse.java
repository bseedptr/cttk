package cttk.impl.binance.response;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.dto.Ticker;
import cttk.impl.binance.BinanceUtils;
import lombok.Data;

@Data
public class BinanceTickerResponse {
    String symbol;
    BigDecimal priceChange;
    BigDecimal priceChangePercent;
    BigDecimal weightedAvgPrice;
    BigDecimal prevClosePrice;
    BigDecimal lastPrice;
    @JsonProperty("lastQty")
    BigDecimal lastQuantity;
    @JsonProperty("bidQty")
    BigDecimal bidQuantity;
    @JsonProperty("askQty")
    BigDecimal askQuantity;
    BigDecimal bidPrice;
    BigDecimal askPrice;
    BigDecimal openPrice;
    BigDecimal highPrice;
    BigDecimal lowPrice;
    BigDecimal volume;
    BigDecimal quoteVolume;
    Long openTime;
    Long closeTime;
    Long firstId;
    Long lastId;
    Long count;

    public Ticker getTicker() {
        return new Ticker()
            .setCurrencyPair(BinanceUtils.parseMarketSymbol(symbol))
            .setOpenPrice(openPrice)
            .setHighPrice(highPrice)
            .setLowPrice(lowPrice)
            .setLastPrice(lastPrice)
            .setAvgPrice(weightedAvgPrice)
            .setHighestBid(bidPrice)
            .setLowestAsk(askPrice)
            .setVolume(volume)
            .setQuoteVolume(quoteVolume)
            .setChange(priceChange)
            .setChangePercent(priceChangePercent);
    }
}
