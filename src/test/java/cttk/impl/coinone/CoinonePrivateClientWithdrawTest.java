package cttk.impl.coinone;

import org.junit.Test;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.AbstractPrivateCXClientWithdrawTest;
import cttk.request.WithdrawToBankRequest;
import cttk.request.WithdrawToWalletRequest;

public class CoinonePrivateClientWithdrawTest
    extends AbstractPrivateCXClientWithdrawTest
{
    @Override
    public String getCxId() {
        return CXIds.COINONE;
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testWithdrawToBankAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        client.withdraw(new WithdrawToBankRequest());
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testWithdrawBTCToWallet()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        client.withdraw(new WithdrawToWalletRequest());
    }
}
