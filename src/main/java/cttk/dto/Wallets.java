package cttk.dto;

import java.util.List;

import cttk.AbstractCXIdHolder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Wallets
    extends AbstractCXIdHolder<Wallets>
{
    List<Wallet> wallets;

    public int size() {
        return wallets == null ? 0 : wallets.size();
    }
}
