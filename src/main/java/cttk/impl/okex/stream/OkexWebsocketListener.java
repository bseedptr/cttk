package cttk.impl.okex.stream;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.commons.compress.compressors.deflate64.Deflate64CompressorInputStream;

import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.dto.OrderBook;
import cttk.impl.okex.OkexUtils;
import cttk.impl.stream.AbstractWebsocketListener;
import cttk.impl.stream.MessageConverter;
import cttk.request.AbstractCXStreamRequest;
import okhttp3.Response;
import okhttp3.WebSocket;
import okio.ByteString;

public class OkexWebsocketListener<T, R extends AbstractCXStreamRequest<R>>
    extends AbstractWebsocketListener<T, R>
{
    private boolean isFirst = true;

    public OkexWebsocketListener(
        final CXStream<T> stream,
        final String channel,
        final R request,
        final MessageConverter<T> converter,
        final CXStreamListener<T> listener)
    {
        super(stream, channel, request, converter, listener, CXIds.OKEX);
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        webSocket.send(getSubscriptionPayload());
        logger.info("[{}] Open : {}", id, channel);
        this.listener.onOpen(stream);
    }

    private String getSubscriptionPayload() {
        return "{\"op\": \"subscribe\", \"args\": [\"" + channel + "\"]}";
    }

    @Override
    protected void handleMessage(WebSocket webSocket, String text)
        throws Exception
    {
        OkexUtils.checkStreamErrorByResponseMessage(request, text);
        if (text.contains("\"channel\"")) {
            logger.info("Subscribed: {}", channel);
        } else if (text.contains("\"ping\"")) {
            webSocket.send(text.replaceFirst("ping", "pong"));
        } else {
            T message = converter.convert(text);
            if (message != null) {
                if (message instanceof OrderBook) {
                    if (!isFirst) ((OrderBook) message).setFull(false);
                    isFirst = false;
                }
                listener.onMessage(message);
            }
        }
    }

    @Override
    protected String toString(ByteString bytes)
        throws Exception
    {
        try (final ByteArrayOutputStream out = new ByteArrayOutputStream();
            final ByteArrayInputStream in = new ByteArrayInputStream(bytes.toByteArray());
            final Deflate64CompressorInputStream zin = new Deflate64CompressorInputStream(in))
        {
            final byte[] buffer = new byte[1024];
            int offset;
            while (-1 != (offset = zin.read(buffer))) {
                out.write(buffer, 0, offset);
            }
            return out.toString();
        } catch (IOException e) {
            throw e;
        }
    }

    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        t = OkexUtils.streamCheckResponseError(t, response);
        super.onFailure(webSocket, t, response);
    }
}
