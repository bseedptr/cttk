package cttk.exception;

import java.math.BigDecimal;

import cttk.CurrencyPair;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class InvalidOrderException
    extends CXAPIRequestException
    implements CurrencyPair
{
    private static final long serialVersionUID = 1L;

    protected String baseCurrency;
    protected String quoteCurrency;
    protected BigDecimal orderPrice;
    protected BigDecimal orderQuantity;

    public InvalidOrderException() {
        super();
    }

    public InvalidOrderException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidOrderException(String message) {
        super(message);
    }

    public InvalidOrderException(Throwable cause) {
        super(cause);
    }

    @Override
    public String toString() {
        return super.toString()
            + ", Base Currency = " + baseCurrency
            + ", Quote Currency = " + quoteCurrency
            + ", Order Price = " + orderPrice
            + ", Order Quantity = " + orderQuantity;
    }

    @Override
    public InvalidOrderException setCxId(String cxId) {
        super.setCxId(cxId);
        return this;
    }

    @Override
    public InvalidOrderException setResponseStatusCode(Integer responseStatusCode) {
        super.setResponseStatusCode(responseStatusCode);
        return this;
    }

    @Override
    public InvalidOrderException setExchangeMessage(String exchangeMessage) {
        super.setExchangeMessage(exchangeMessage);
        return this;
    }

    @Override
    public InvalidOrderException setExchangeErrorCode(String exchangeErrorCode) {
        super.setExchangeErrorCode(exchangeErrorCode);
        return this;
    }

    public InvalidOrderException setCurrencyPair(CurrencyPair currencyPair) {
        this.baseCurrency = currencyPair == null ? null : currencyPair.getBaseCurrency();
        this.quoteCurrency = currencyPair == null ? null : currencyPair.getQuoteCurrency();
        return this;
    }
}
