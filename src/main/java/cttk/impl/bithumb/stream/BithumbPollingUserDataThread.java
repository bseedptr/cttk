package cttk.impl.bithumb.stream;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.CurrencyPair;
import cttk.OrderStatus;
import cttk.UserData;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrades;
import cttk.exception.CTTKException;
import cttk.impl.stream.eachmarket.EachMarketPollingUserDataThread;
import cttk.impl.stream.eachmarket.LevelPairMap;
import cttk.impl.stream.eachmarket.LimitedCXClient;
import cttk.impl.stream.eachmarket.PollingLevel;
import cttk.request.GetUserOrderRequest;
import cttk.request.OpenUserDataStreamRequest;

public class BithumbPollingUserDataThread
    extends EachMarketPollingUserDataThread
{
    BithumbPollingUserDataThread(
        final CXStream<UserData> cxStream,
        final PollingLevel pollingLevel,
        final CXStreamListener<UserData> listener,
        final LimitedCXClient cxClient,
        final OpenUserDataStreamRequest request,
        final LevelPairMap levelPairMap,
        final Map<CurrencyPair, UserOrder> lastCloseUserOrderMap)
    {
        super(cxStream, pollingLevel, listener, cxClient, request, levelPairMap, lastCloseUserOrderMap);
    }

    @Override
    protected List<UserOrder> getCloseOrdersDuringInterval() {
        // see comment in cttk-564
        return Collections.emptyList();
    }

    @Override
    protected List<UserOrder> getUpdatedCloseOrders(List<UserOrder> orders) {
        return getUpdatedCloseOrdersEachOrders(orders);
    }

    protected UserOrder getUserOrder(UserOrder order)
        throws CTTKException
    {
        UserOrder updatedUserOrder = cxClient.getDataWithRetryAndCheckRateLimit(2, () -> client.getUserOrder(GetUserOrderRequest.from(order).setWithTrades(true)));
        if (updatedUserOrder == null) {
            return order.setStatusType(OrderStatus.CANCELED).setStatus(OrderStatus.CANCELED.name().toLowerCase());
        }
        return updatedUserOrder;
    }

    @Override
    protected void onMessageChangedUserTrades(UserOrders ordersToCheckTrade) {
        UserTrades userTrades = ordersToCheckTrade.getUserTrades();
        if (userTrades != null && userTrades.isNotEmpty()) {
            listener.onMessage(UserData.of(userTrades));
        }
    }
}
