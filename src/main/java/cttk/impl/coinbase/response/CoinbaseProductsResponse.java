package cttk.impl.coinbase.response;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.stream.Collectors;

import cttk.dto.MarketInfo;
import cttk.dto.MarketInfos;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CoinbaseProductsResponse
    extends ArrayList<CoinbaseProduct>
{
    public MarketInfos toMarketInfos() {
        return new MarketInfos()
            .setMarketInfos(this.stream()
                .map(i -> i.toMarketInfo())
                .collect(Collectors.toList()));
    }
}

@Data
class CoinbaseProduct {
    String id;
    String base_currency;
    String quote_currency;
    BigDecimal base_min_size;
    BigDecimal base_max_size;
    BigDecimal quote_increment;
    String display_name;
    String status;
    boolean margin_enabled;
    String status_message;
    BigDecimal min_market_funds;
    BigDecimal max_market_funds;
    boolean post_only;
    boolean limit_only;
    boolean cancel_only;

    public MarketInfo toMarketInfo() {
        return new MarketInfo()
            .setStatus(status)
            .setCurrencyPair(base_currency, quote_currency)
            .setPriceIncrement(quote_increment)
            .setMinPrice(quote_increment)
            .setQuantityIncrement(base_min_size)
            .setMinQuantity(base_min_size)
            .setMaxQuantity(base_max_size);
    }
}