package cttk.impl.bithumb.response;

import static cttk.impl.bithumb.BithumbUtils.guessFeeCurrency;
import static cttk.impl.bithumb.BithumbUtils.isTrade;
import static cttk.util.StringUtils.parseBigDecimal;
import static cttk.util.StringUtils.toUpper;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import cttk.OrderSide;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BithumbUserTradesResponse
    extends BithumbDataResponse<List<BithumbUserTrade>>
{
    public UserTrades toUserTrades(final String currency, final String accountId) {
        UserTrades ret = new UserTrades();
        ret.setTrades(toUserTradeList(currency.toLowerCase(), data, accountId));
        return ret;
    }

    private List<UserTrade> toUserTradeList(final String currency, List<BithumbUserTrade> bithumbUserTrades, String accountId) {
        if (bithumbUserTrades == null) return null;
        return bithumbUserTrades.stream()
            .filter(i -> isTrade(i.getMap().get("search")))
            .map(i -> i.toUserTrade(currency, accountId))
            .collect(Collectors.toList());
    }
}

@Data
class BithumbUserTrade {
    Map<String, String> map = new HashMap<>();

    @JsonAnyGetter
    public Map<String, String> any() {
        return this.map;
    }

    @JsonAnySetter
    public void set(String name, String value) {
        this.map.put(name, value);
    }

    public UserTrade toUserTrade(String currency, String accountId) {
        BigDecimal quantity = new BigDecimal(map.get("units").replaceAll("\\s", ""));
        OrderSide side = quantity.compareTo(BigDecimal.ZERO) > 0 ? OrderSide.BUY : OrderSide.SELL;
        String baseCurrency = toUpper(currency);
        String feeCurrency = guessFeeCurrency(side, baseCurrency);
        String fee = map.get("fee").split("\\s+")[0];
        BigDecimal total = parseBigDecimal(map.get("price"));

        return new UserTrade()
            .setTid(getTid(accountId, baseCurrency))
            .setBaseCurrency(baseCurrency)
            .setQuoteCurrency("KRW")
            .setDateTime(DateTimeUtils.zdtFromEpochMicro(Long.parseLong(map.get("transfer_date"))))
            .setQuantity(quantity.abs())
            .setSide(side)
            .setPrice(parseBigDecimal(map.get(currency + "1krw")))
            .setFee(parseBigDecimal(fee))
            .setFeeCurrency(feeCurrency)
            .setTotal(total == null ? null : total.abs());
    }

    private String getTid(String accountId, String currency) {
        return accountId + ":" + map.get("transfer_date") + ":" + currency + ":KRW";
    }
}