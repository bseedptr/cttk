package cttk.impl.bitfinex;

import cttk.CXIds;
import cttk.impl.AbstractUserDataStreamTest;

public class BitfinexUserDataStreamTest
    extends AbstractUserDataStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.BITFINEX;
    }

    @Override
    protected boolean useThread() {
        return true;
    }
}
