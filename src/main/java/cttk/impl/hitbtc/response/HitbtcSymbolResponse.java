package cttk.impl.hitbtc.response;

import java.math.BigDecimal;

import cttk.dto.MarketInfo;
import cttk.impl.hitbtc.HitbtcUtils;
import lombok.Data;

@Data
public class HitbtcSymbolResponse {
    String id;
    String baseCurrency;
    String quoteCurrency;
    BigDecimal quantityIncrement;
    BigDecimal tickSize;
    BigDecimal takeLiquidityRate;
    BigDecimal provideLiquidityRate;
    String feeCurrency;

    public MarketInfo toMarketInfo() {
        return new MarketInfo()
            .setCurrencyPair(HitbtcUtils.toStdCurrencySymbol(baseCurrency), HitbtcUtils.toStdCurrencySymbol(quoteCurrency))
            .setPriceIncrement(tickSize)
            .setQuantityIncrement(quantityIncrement);
    }
}