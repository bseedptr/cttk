package cttk.impl.bibox.response;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.dto.OrderBook;
import cttk.impl.bibox.BiboxUtils;
import lombok.Data;

@Data
public class BiboxOrderBook {
    String pair;
    @JsonProperty("update_time")
    Long updateTime;
    List<BiboxOrderBookPriceVolumePair> bids;
    List<BiboxOrderBookPriceVolumePair> asks;

    public OrderBook toOrderBook() {
        return new OrderBook()
            .setCurrencyPair(BiboxUtils.parseMarketSymbol(pair))
            .setBids(bids.stream().map(BiboxOrderBookPriceVolumePair::toSimpleOrder).collect(Collectors.toList()))
            .setAsks(asks.stream().map(BiboxOrderBookPriceVolumePair::toSimpleOrder).collect(Collectors.toList()));
    }
}
