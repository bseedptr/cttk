package cttk.impl.stream;

import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.UserData;
import cttk.UserDataStreamOpener;
import cttk.exception.CTTKException;
import cttk.request.OpenUserDataStreamRequest;
import cttk.util.ReopenPolicy;

public class PostSettingUserDataStreamOpener
    implements UserDataStreamOpener
{
    private final String cxId;
    private final UserDataStreamOpener opener;

    public PostSettingUserDataStreamOpener(String cxId, UserDataStreamOpener opener) {
        this.cxId = cxId;
        this.opener = opener;
    }

    @Override
    public CXStream<UserData> openUserDataStream(OpenUserDataStreamRequest request, CXStreamListener<UserData> listener)
        throws CTTKException
    {
        return opener.openUserDataStream(request, new PostSettingUserDataStreamListener(cxId, listener));
    }

    @Override
    public CXStream<UserData> openUserDataStream(OpenUserDataStreamRequest request, CXStreamListener<UserData> listener, ReopenPolicy reopenPolicy)
        throws CTTKException
    {
        return opener.openUserDataStream(request, new PostSettingUserDataStreamListener(cxId, listener));
    }
}
