package cttk.impl.kraken.response;

import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.dto.MarketInfos;
import lombok.Data;

@Data
public class KrakenAssetPairs {
    private final KrakenAssetsResponse assets;
    private final KrakenAssetPairsResponse assetPairs;
    private final Map<String, String> krakenToStdCurrencySymbolMap;
    private final Map<String, String> stdToKrakenCurrencySymbolMap;

    public KrakenAssetPairs(KrakenAssetsResponse assets, KrakenAssetPairsResponse assetPairs) {
        this.assets = assets;
        this.assetPairs = assetPairs;

        this.krakenToStdCurrencySymbolMap = assets.getKrakenToStdCurrencySymbolMap();
        this.stdToKrakenCurrencySymbolMap = assets.getStdToKrakenCurrencySymbolMap();
    }

    public Set<String> getAllAssetPairs() {
        return this.assetPairs.getResult() == null ? null : this.assetPairs.getResult().keySet();
    }

    public CurrencyPair getStdCurrencyPair(String assetPairStr) {
        KrakenAssetPair assetPair = assetPairs.result.get(assetPairStr);
        return CurrencyPair.of(
            krakenToStdCurrencySymbolMap.get(assetPair.getBase()),
            krakenToStdCurrencySymbolMap.get(assetPair.getQuote()));
    }

    public String getKrakenAssetPair(String base, String quote) {
        Map.Entry<String, KrakenAssetPair> assetPair = null;
        try {
            assetPair = assetPairs.result.entrySet()
                .stream()
                .filter(i -> i.getValue().base.equals(base) && i.getValue().quote.equals(quote) && !i.getValue().getAltname().endsWith(".d"))
                .reduce((a, b) -> {
                    throw new IllegalStateException("Multiple elements: " + a + ", " + b);
                })
                .get();
        } catch (NoSuchElementException ne) {
            return base + quote;
        }
        return assetPair.getKey();
    }

    public MarketInfos toMarketInfos() {
        return new MarketInfos().setMarketInfos(
            assetPairs.result.values().stream()
                .filter(p -> !p.getAltname().endsWith(".d"))
                .map(p -> p.toMarketInfo()
                    .setCurrencyPair(krakenToStdCurrencySymbolMap.get(p.getBase()), krakenToStdCurrencySymbolMap.get(p.getQuote())))
                .collect(Collectors.toList()));
    }
}
