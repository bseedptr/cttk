package cttk.impl.kraken.response;

import java.util.List;

import cttk.dto.UserOrder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class KrakenCreateOrderResponse
    extends KrakenResponse<KrakenCreateOrderResult>
{
    public UserOrder toUserOrder() {
        return result.getUserOrder();
    }
}

@Data
class KrakenCreateOrderResult {
    //descr = order description info
    //    order = order description
    //    close = conditional close order description (if conditional close set)
    //txid = array of transaction ids for order (if order was added successfully)

    KrakenOrderDescr descr;
    List<String> txid;

    public UserOrder getUserOrder() {
        // TODO handle multiple txids
        return txid == null || txid.isEmpty() ? null : new UserOrder().setOid(txid.get(0));
    }
}

@Data
class KrakenOrderDescr {
    String order;
    String close;
}