package cttk.impl.kraken;

import cttk.CXClientFactory;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.UserDataStreamOpener;
import cttk.auth.Credential;
import cttk.impl.RetryPrivateCXClient;
import cttk.impl.stream.PollingUserDataStreamOpener;

public class KrakenClientFactory
    implements CXClientFactory
{
    @Override
    public PublicCXClient create() {
        return new KrakenPublicClient();
    }

    @Override
    public PrivateCXClient create(Credential credential) {
        return new RetryPrivateCXClient(5, 5, new KrakenPrivateClient(credential), "EAPI:Invalid nonce");
    }

    @Override
    public UserDataStreamOpener createUserDataStreamOpener(Credential credential) {
        return new PollingUserDataStreamOpener(create(credential));
    }
}
