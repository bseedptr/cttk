package cttk.impl.huobi.stream.response;

import java.math.BigDecimal;

import cttk.dto.Ticker;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HuobiWebsocketTickerResponse
    extends HuobiWebsocketResponse<HuobiTickerTick>
{
    public Ticker toTicker() {
        return new Ticker()
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(ts))
            .setOpenPrice(tick.getOpen())
            .setHighPrice(tick.getHigh())
            .setLowPrice(tick.getLow())
            .setLastPrice(tick.getClose())
            .setVolume(tick.getAmount())
            .setQuoteVolume(tick.getVol());
    }
}

@Data
class HuobiTickerTick {
    long id;
    long version;
    BigDecimal open;
    BigDecimal high;
    BigDecimal low;
    BigDecimal close;
    BigDecimal amount;
    BigDecimal vol;
}