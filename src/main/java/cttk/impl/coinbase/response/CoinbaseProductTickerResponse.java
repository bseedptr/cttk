package cttk.impl.coinbase.response;

import java.math.BigDecimal;

import cttk.CurrencyPair;
import cttk.dto.Ticker;
import cttk.impl.coinbase.CoinbaseUtils;
import lombok.Data;

@Data
public class CoinbaseProductTickerResponse {
    Long trade_id;
    BigDecimal price;
    BigDecimal size;
    BigDecimal bid;
    BigDecimal ask;
    BigDecimal volume;
    String time;

    public Ticker toTicker(CurrencyPair currencyPair) {
        return new Ticker()
            .setCurrencyPair(currencyPair)
            // open
            // high
            // low
            .setLastPrice(price)
            .setLowestAsk(ask)
            .setHighestBid(bid)
            .setQuoteVolume(volume)
            .setDateTime(CoinbaseUtils.parseZDT(time));
    }
}