package cttk.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import cttk.CurrencyPair;
import cttk.dto.UserOrder;
import cttk.exception.APILimitExceedException;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXAuthorityException;
import cttk.exception.CXServerException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.JsonUtils;
import cttk.impl.util.MathUtils;
import cttk.impl.util.ThrowableFunction;
import cttk.request.CreateMarketPriceOrderRequest;
import cttk.request.CreateOrderRequest;
import okhttp3.ConnectionPool;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public abstract class AbstractCXClient {
    protected final OkHttpClient httpClient;
    protected final ObjectMapper objectMapper;
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    public AbstractCXClient() {
        this(false);
    }

    public AbstractCXClient(boolean ignoreSsl) {
        this.httpClient = ignoreSsl
            ? OkHttpClientFactory.createUnsafeOkHttpClient(getClass())
            : OkHttpClientFactory.createOkHttpClient(getClass());

        this.objectMapper = JsonUtils.createDefaultObjectMapper();
    }

    public AbstractCXClient(ConnectionPool connectionPool) {
        this.httpClient = OkHttpClientFactory.createOkHttpClient(connectionPool);
        this.objectMapper = JsonUtils.createDefaultObjectMapper();
    }

    public abstract String getCxId();

    protected void checkQuoteKRW(CurrencyPair currencyPair)
        throws UnsupportedCurrencyPairException
    {
        if (!"KRW".equalsIgnoreCase(currencyPair.getQuoteCurrency())) {
            throw new UnsupportedCurrencyPairException()
                .setCurrencyPair(currencyPair)
                .setCxId(getCxId());
        }
    }

    protected void debug(final Request request) {
        logger.debug("\n"
            + "    REQUEST : {}\n"
            + "    HEADERS : {}", request, request.headers());
    }

    protected void debug(final Request request, final Map<?, ?> params) {
        logger.debug("\n"
            + "    REQUEST : {}\n"
            + "    PARAMS  : {}", request, params);
    }

    protected void debug(final Response response) {
        logger.debug("\n"
            + "    RESPONSE: {}", response);
    }

    protected void debug(final HttpResponse response) {
        logger.debug("\n"
            + "    RESPONSE: {}", response);
    }

    protected String getHostUrl(final String baseUrl, final String path) {
        return getHostUrl(baseUrl, path, null);
    }

    protected String getHostUrl(final String baseUrl, final String path, final Map<String, String> params) {
        HttpUrl.Builder builder = HttpUrl.parse(baseUrl).newBuilder();
        if (path != null) {
            builder.addPathSegments(path.startsWith("/") ? path.substring(1) : path);
        }
        if (params != null && !params.isEmpty()) {
            params.forEach((k, v) -> builder.addQueryParameter(k, v));
        }
        return builder.build().toString();
    }

    protected static RequestBody createEncodedFormBody(Map<String, String> params) {
        if (params == null || params.isEmpty()) return new FormBody.Builder().build();
        final FormBody.Builder builder = new FormBody.Builder();
        params.forEach((k, v) -> builder.addEncoded(k, v));
        return builder.build();
    }

    protected HttpResponse toHttpResponse(Response httpResponse)
        throws IOException, CTTKException, UnsupportedCurrencyPairException
    {
        final String body = httpResponse.body().string();
        final HttpResponse response = new HttpResponse()
            .setStatus(httpResponse.code())
            .setBody(body);

        debug(response);

        return response;

    }

    protected void checkResponseError(Object requestObj, HttpResponse response)
        throws CTTKException
    {
        preCheckErrorByResponseStatus(requestObj, response);
        checkErrorByResponseMessage(requestObj, response);
        postCheckErrorByResponseStatus(requestObj, response);
    }

    protected void preCheckErrorByResponseStatus(Object requestObj, HttpResponse response)
        throws CTTKException
    {
        switch (response.getStatus()) {
            case 401:
            case 403:
                throw new CXAuthorityException()
                    .setCxId(getCxId())
                    .setExchangeMessage(response.getBody())
                    .setResponseStatusCode(response.getStatus());
            case 429:
                throw new APILimitExceedException()
                    .setCxId(getCxId())
                    .setExchangeMessage(response.getBody())
                    .setResponseStatusCode(response.getStatus());
            default:
                break;
        }
    }

    protected abstract void checkErrorByResponseMessage(Object requestObj, HttpResponse response)
        throws CTTKException;

    protected void postCheckErrorByResponseStatus(Object requestObj, HttpResponse response)
        throws CTTKException
    {
        if (500 <= response.getStatus() && response.getStatus() < 600) {
            throw new CXServerException()
                .setCxId(getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        } else if (!response.is2xxSuccessful()) {
            throw new CXAPIRequestException("HTTP response status is not okay")
                .setCxId(getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    protected <T> T convert(HttpResponse response, Class<T> claz)
        throws CTTKException
    {
        return convert(response, claz, ThrowableFunction.identity());
    }

    protected <T, R> R convert(final HttpResponse response, Class<T> claz, ThrowableFunction<T, R> mapper)
        throws CTTKException
    {
        try {
            return mapper.apply(objectMapper.readValue(response.getBody(), claz));
        } catch (IOException e) {
            throw CTTKException.create(e, getCxId());
        }

    }

    protected <T> List<T> convertList(final HttpResponse response, Class<T> claz)
        throws CTTKException, UnsupportedCurrencyPairException, IOException
    {
        return convertList(response, claz, ThrowableFunction.identity());
    }

    protected <T, R> R convertList(final HttpResponse response, Class<T> claz, ThrowableFunction<List<T>, R> mapper)
        throws CTTKException, UnsupportedCurrencyPairException, IOException
    {
        final List<T> list = objectMapper
            .readValue(response.getBody(), objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, claz));
        return mapper.apply(list);
    }

    protected static UserOrder fillResponseByRequest(CreateOrderRequest request, UserOrder response) {
        if (request == null || response == null) return response;
        if (response.isBothNull()) response.setCurrencyPair(request);
        if (response.getSide() == null) response.setSide(request.getSide());
        if (response.getPrice() == null) response.setPrice(request.getPrice());
        if (response.getQuantity() == null) response.setQuantity(request.getQuantity());
        if (response.getTotal() == null) response.setTotal(MathUtils.multiply(request.getPrice(), request.getQuantity()));
        return response;
    }

    protected static UserOrder fillResponseByRequest(CreateMarketPriceOrderRequest request, UserOrder response) {
        if (request == null || response == null) return response;
        if (response.isBothNull()) response.setCurrencyPair(request);
        if (response.getSide() == null) response.setSide(request.getSide());
        if (response.getQuantity() == null) response.setQuantity(request.getQuantity());
        return response;
    }
}
