package cttk.exception;

import cttk.CXIdGettable;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class CTTKException
    extends Exception
    implements CXIdGettable
{
    private static final long serialVersionUID = 1L;

    protected String cxId;

    public CTTKException() {
        super();
    }

    public CTTKException(String message, Throwable cause) {
        super(message, cause);
    }

    public CTTKException(String message) {
        super(message);
    }

    public CTTKException(Throwable cause) {
        super(cause);
    }

    @Override
    public String toString() {
        return super.toString() + ", [" + cxId + "] : ";
    }

    public static CTTKException create(Exception e) {
        return new CTTKException(e);
    }

    public static CTTKException create(Exception e, String cxId) {
        return new CTTKException(e).setCxId(cxId);
    }

    public static CTTKException create(String message, Exception e, String cxId) {
        return new CTTKException(message, e).setCxId(cxId);
    }

    public static CTTKException createOrCast(Exception e) {
        if (e instanceof CTTKException) {
            return (CTTKException) e;
        } else {
            return new CTTKException(e);
        }
    }

    public static CTTKException createOrCast(Exception e, String cxId) {
        if (e instanceof CTTKException) {
            return (CTTKException) e;
        } else {
            return new CTTKException(e).setCxId(cxId);
        }
    }
}
