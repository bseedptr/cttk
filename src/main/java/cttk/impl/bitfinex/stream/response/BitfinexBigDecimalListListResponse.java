package cttk.impl.bitfinex.stream.response;

import static cttk.impl.bitfinex.BitfinexUtils.toStdCurrencyPair;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.dto.OrderBook;
import cttk.dto.OrderBook.SimpleOrder;

public class BitfinexBigDecimalListListResponse
    extends ArrayList<ArrayList<BigDecimal>>
{
    public OrderBook toOrderBook(CurrencyPair currencyPair) {

        final List<SimpleOrder> bids = this.stream().filter(i -> i.get(2).compareTo(BigDecimal.ZERO) >= 0)
            .map(i -> new SimpleOrder(i.get(0), i.get(2).abs(), i.get(1)))
            .collect(Collectors.toList());

        final List<SimpleOrder> asks = this.stream().filter(i -> i.get(2).compareTo(BigDecimal.ZERO) <= 0)
            .map(i -> new SimpleOrder(i.get(0), i.get(2).abs(), i.get(1)))
            .collect(Collectors.toList());

        return new OrderBook()
            .setCurrencyPair(toStdCurrencyPair(currencyPair))
            .setDateTime(ZonedDateTime.now())
            .setBids(bids)
            .setAsks(asks);
    }
}
