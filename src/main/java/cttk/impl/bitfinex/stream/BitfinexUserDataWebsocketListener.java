package cttk.impl.bitfinex.stream;

import static cttk.impl.bitfinex.BitfinexUtils.nonce;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.GZIPInputStream;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cttk.CXClientFactory;
import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.PrivateCXClient;
import cttk.UserData;
import cttk.auth.Credential;
import cttk.dto.Balances;
import cttk.dto.UserOrder;
import cttk.dto.UserTrade;
import cttk.exception.CTTKException;
import cttk.impl.bitfinex.BitfinexUtils;
import cttk.impl.bitfinex.stream.response.BitfinexUserDataOrdersSnapshotResponse;
import cttk.impl.bitfinex.stream.response.BitfinexUserDataOrdersUpdateResponse;
import cttk.impl.bitfinex.stream.response.BitfinexUserDataTradesUpdateResponse;
import cttk.impl.stream.AbstractWebsocketListener;
import cttk.impl.util.CryptoUtils;
import cttk.impl.util.JsonUtils;
import cttk.impl.util.ParamsBuilder;
import cttk.request.GetUserOrderRequest;
import cttk.request.OpenUserDataStreamRequest;
import okhttp3.Response;
import okhttp3.WebSocket;
import okio.ByteString;

public class BitfinexUserDataWebsocketListener
    extends AbstractWebsocketListener<UserData, OpenUserDataStreamRequest>
{
    private static final int STRING_LIST_STARTING_INDEX = 8;
    private final ObjectMapper objectMapper = JsonUtils.createDefaultObjectMapper();
    private PrivateCXClient privateCXClient;
    private Credential credential;
    private ExecutorService executor;

    public BitfinexUserDataWebsocketListener(
        final CXStream<UserData> stream,
        final CXStreamListener<UserData> listener)
    {
        super(stream,
            "userDataStream",
            null, // ignore CurrencyPair
            null, // ignore converter
            listener,
            CXIds.BITFINEX);
    }

    public BitfinexUserDataWebsocketListener setCredential(Credential credential) {
        this.credential = credential;
        try {
            this.privateCXClient = CXClientFactory.createPrivateCXClient(CXIds.BITFINEX, credential);
        } catch (CTTKException e) {
            logger.error("{}", e);
        }

        return this;
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        this.executor = Executors.newSingleThreadExecutor();
        webSocket.send(getSubscriptionPayload());
        logger.info("[{}] Open : {}", id, channel);
        this.listener.onOpen(stream);
    }

    @Override
    public void onClosed(WebSocket webSocket, int code, String reason) {
        logger.info("[{}] Closed: - {} {} {}", id, channel, code, reason);
        this.executor.shutdown();
        this.listener.onClosed(stream, code, reason);
    }

    public String getSubscriptionPayload() {
        String event = "auth";
        String apiNonce = nonce();
        String apiPaylod = "AUTH" + apiNonce;
        String authSig = CryptoUtils.hexString(CryptoUtils.hmacSha384(apiPaylod, credential.getSecretKey()));

        Map<String, String> params = ParamsBuilder.create()
            .put("apiKey", credential.getAccessKey())
            .put("authNonce", apiNonce)
            .put("authPayload", apiPaylod)
            .put("authSig", authSig)
            .put("event", event)
            .build();

        try {
            String payload = objectMapper.writeValueAsString(params);
            return payload;
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    protected void handleMessage(WebSocket webSocket, String text)
        throws Exception
    {
        BitfinexUtils.checkErrorByResponseMessage(request, text, null);
        final List<UserData> userDataList = convert(text);
        if (userDataList != null && !userDataList.isEmpty()) {
            userDataList.forEach(d -> listener.onMessage(d));
        }
    }

    public List<UserData> convert(String message)
        throws Exception
    {
        List<UserData> userDataList = new ArrayList<>();
        final String eventType = getEventType(message);

        if ("\"os\"".equals(eventType)) {
            if (isNotEmptyOrders(message)) {
                final String json = message.substring(message.indexOf("[["), message.length() - 1);
                final BitfinexUserDataOrdersSnapshotResponse response = objectMapper.readValue(json, BitfinexUserDataOrdersSnapshotResponse.class);
                response.toUserOrderList().stream().forEach(i -> userDataList.add(UserData.of(i)));

                updateBalances();
            }
        } else if ("\"on\"".equals(eventType) || "\"ou\"".equals(eventType) || "\"oc\"".equals(eventType)) {
            final String json = message.substring(STRING_LIST_STARTING_INDEX, message.length() - 1);
            final BitfinexUserDataOrdersUpdateResponse response = objectMapper.readValue(json, BitfinexUserDataOrdersUpdateResponse.class);
            userDataList.add(UserData.of(response.toUserOrder()));

            updateBalances();
        } else if ("\"tu\"".equals(eventType)) {
            final String json = message.substring(STRING_LIST_STARTING_INDEX, message.length() - 1);
            final BitfinexUserDataTradesUpdateResponse response = objectMapper.readValue(json, BitfinexUserDataTradesUpdateResponse.class);

            final UserTrade userTrade = response.toUserTrade();
            userDataList.add(UserData.of(userTrade));
            final UserOrder userOrder = privateCXClient.getUserOrder(new GetUserOrderRequest().setOid(userTrade.getOid()));
            userDataList.add(UserData.of(userOrder));
            updateBalances();
        }
        /* NOTE
         *
         * ws, wu: wallet snapshot and update related CHANNEL_IDs, currently it does not give available information. so we call updateBalnaces() manually now.
         * te: trades executed related CHANNEL_IDs, now we only mapping 'tu' information for userDataStream
         * hb: heartbeat info related CHANNEL_IDs, currently ignored
         *
         * */

        return userDataList;
    }

    private void updateBalances() {
        executor.execute(() -> {
            try {
                Thread.sleep(1000l);
                Balances balances = privateCXClient.getAllBalances();
                listener.onMessage(UserData.of(balances));
            } catch (InterruptedException e) {
                logger.error("{}", e);
            } catch (CTTKException e) {
                logger.error("{}", e);
            }
        });
    }

    private String getEventType(String message) {
        String[] split = message.split(",");
        String eventType = split[1];
        return eventType;
    }

    /**
     * If there are orders, message contains "[[".
     */
    private boolean isNotEmptyOrders(String message) {
        return message.indexOf("[[") != -1;
    }

    @Override
    protected String toString(ByteString bytes)
        throws Exception
    {
        StringWriter sw = new StringWriter();
        try (
            final PrintWriter pw = new PrintWriter(sw);
            final ByteArrayInputStream in = new ByteArrayInputStream(bytes.toByteArray());
            final BufferedReader br = new BufferedReader(new InputStreamReader(new GZIPInputStream(in)));)
        {
            String line;
            for (int i = 0; (line = br.readLine()) != null; i++) {
                if (i > 0) pw.println();
                pw.print(line);
            }
        }
        return sw.toString();
    }

    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        if (t instanceof java.io.EOFException) {
            webSocket.close(NORMAL_CLOSURE_STATUS, null);
            logger.info("[{}] Closing: - {}", id, channel);
        } else {
            super.onFailure(webSocket, t, response);
        }
    }
}