package cttk.impl.kraken;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cttk.impl.kraken.response.KrakenResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cttk.CXIds;
import cttk.exception.APILimitExceedException;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXAuthorityException;
import cttk.exception.CXServerException;
import cttk.exception.InsufficientBalanceException;
import cttk.exception.InvalidOrderException;
import cttk.exception.OrderNotFoundException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.AbstractCXClient;
import cttk.impl.kraken.response.KrakenAssetPairs;
import cttk.impl.kraken.response.KrakenAssetPairsResponse;
import cttk.impl.kraken.response.KrakenAssetsResponse;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ThrowableFunction;
import cttk.impl.util.Timer;
import cttk.request.CreateOrderRequest;
import cttk.util.StringUtils;
import okhttp3.Request;
import okhttp3.Response;

abstract class AbstractKrakenClient
    extends AbstractCXClient
{
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private static final String PUBLIC_API_BASE_URL = "https://api.kraken.com/0/public/";
    private static final long ASSET_PAIR_CACHE_LIMIT_IN_MILLI = 1000 * 600;

    private static long lastCacheEpochInMilli = 0;
    private static KrakenAssetPairs assetPairs;

    @Override
    public String getCxId() {
        return CXIds.KRAKEN;
    }

    protected synchronized KrakenAssetPairs getAssetPairs()
        throws CTTKException
    {
        if (assetPairs == null || System.currentTimeMillis() - lastCacheEpochInMilli > ASSET_PAIR_CACHE_LIMIT_IN_MILLI) {
            Timer timer = Timer.create();

            HttpResponse assetsResponse = requestGet("Assets");
            checkResponseError(null, assetsResponse);

            HttpResponse assetPairsResponse = requestGet("AssetPairs");
            checkResponseError(null, assetPairsResponse);

            assetPairs = new KrakenAssetPairs(
                convert(assetsResponse, KrakenAssetsResponse.class),
                convert(assetPairsResponse, KrakenAssetPairsResponse.class));
            logger.info("Kraken assets are loaded in cache ({}ms)", timer.stop().getIntervalInMilli());
            lastCacheEpochInMilli = System.currentTimeMillis();
        }
        return assetPairs;
    }

    protected HttpResponse requestGet(String path)
        throws CTTKException
    {
        return requestGet(path, null);
    }

    protected HttpResponse requestGet(final String path, final Map<String, String> params)
        throws CTTKException
    {
        final Request request = new Request.Builder()
            .url(getHostUrl(path, params))
            .build();

        debug(request);

        try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
            return toHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    protected String getHostUrl(String endpoint) {
        return getHostUrl(PUBLIC_API_BASE_URL, endpoint);
    }

    protected String getHostUrl(String endpoint, Map<String, String> params) {
        return getHostUrl(PUBLIC_API_BASE_URL, endpoint, params);
    }

    @Override
    protected <T, R> R convert(HttpResponse response, Class<T> claz, ThrowableFunction<T, R> mapper)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        try {
            T obj = objectMapper.readValue(response.getBody(), claz);
            return mapper.apply(obj);
        } catch (IOException e) {
            throw new CXAPIRequestException("json parse error ", e)
                .setCxId(this.getCxId())
                .setResponseStatusCode(response.getStatus())
                .setExchangeMessage(response.getBody());
        }
    }

    @Override
    protected void checkErrorByResponseMessage(Object requestObj, HttpResponse response)
        throws CTTKException
    {
        // statusCode is 200 even though error was raised
        final KrakenResponse krakenResponse;
        try {
            krakenResponse = convert(response, KrakenResponse.class);
        } catch (CXAPIRequestException e) {
            // Some of the exceptions with the http status code can be parsed by the later postCheckErrorByResponseStatus function
            return;
        }

        if (!krakenResponse.hasError()) return;

        final String exchangeErrorCode = (String) krakenResponse.getError().get(0);

        if (exchangeErrorCode.contains("Unknown asset pair")
            || exchangeErrorCode.contains("Invalid asset pair")
            || exchangeErrorCode.contains("Unknown asset"))
        {
            throw CXAPIRequestException.createByRequestObj(requestObj)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        } else if (exchangeErrorCode.contains("Invalid key")) {
            throw new CXAuthorityException()
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
        // API limit
        else if (exchangeErrorCode.contains("Rate limit exceeded")
            || exchangeErrorCode.contains("EGeneral:Temporary lockout"))
        {
            throw new APILimitExceedException("API limit exceeeded")
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus())
                .setExchangeErrorCode(exchangeErrorCode);
        }
        // OrderNotFound
        else if (exchangeErrorCode.contains("EOrder:Invalid order")
            || exchangeErrorCode.contains("\"EOrder:Unknown order\""))
        {
            throw new OrderNotFoundException()
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus())
                .setExchangeErrorCode(exchangeErrorCode);
        } else if (exchangeErrorCode.contains("EGeneral:Internal error")) {
            throw new CXAPIRequestException()
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus())
                .setExchangeErrorCode(exchangeErrorCode);
        } else {
            throw new CXAPIRequestException(krakenResponse.getErrorString())
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus())
                .setExchangeErrorCode(exchangeErrorCode);
        }
    }
}
