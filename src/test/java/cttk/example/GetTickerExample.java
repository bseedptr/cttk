package cttk.example;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import cttk.CTTK;
import cttk.CXClientFactory;
import cttk.CXIds;
import cttk.CurrencyPair;
import cttk.PublicCXClient;
import cttk.dto.Ticker;
import cttk.request.GetTickerRequest;
import cttk.test.TestConfig;

public class GetTickerExample {

    public static void main(String[] args)
        throws IOException
    {
        List<String> cxIds = CTTK.getSupportedCxIds();

        cxIds= Arrays.asList(CXIds.COINBASE);
        
        for (String cxId : cxIds) {
            final TestConfig testConfig = TestConfig.readFromClasspathYaml("config/" + cxId.toLowerCase() + ".yml");
            final CurrencyPair currencyPair = testConfig.getValidCurrencyPair();

            try {
                final PublicCXClient client = CXClientFactory.createPublicCXClient(cxId);

                final Ticker ticker = client.getTicker(new GetTickerRequest().setCurrencyPair(currencyPair));

                Examples.print(cxId, ticker);

            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
        }
    }

}
