package cttk.impl.korbit.response;

import static cttk.impl.util.DateTimeUtils.zdtFromEpochMilli;

import java.math.BigDecimal;

import cttk.CurrencyPair;
import cttk.dto.Ticker;
import cttk.impl.util.ZoneIds;
import lombok.Data;

@Data
public class KorbitTickerResponse {
    String timestamp;
    BigDecimal last;
    BigDecimal bid;
    BigDecimal ask;
    BigDecimal low;
    BigDecimal high;
    BigDecimal volume;
    BigDecimal change;
    BigDecimal changePercent;

    public Ticker toTicker(CurrencyPair currencyPair) {
        return new Ticker()
            .setCurrencyPair(currencyPair)
            .setDateTime(zdtFromEpochMilli(Long.parseLong(timestamp), ZoneIds.SEOUL))
            .setLastPrice(last)
            .setHighestBid(bid)
            .setLowestAsk(ask)
            .setLowPrice(low)
            .setHighPrice(high)
            .setVolume(volume)
            .setChange(change)
            .setChangePercent(changePercent);

    }
}