package cttk.example;

import java.math.RoundingMode;
import java.util.List;

import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.CXStreams;
import cttk.dto.OrderBook;
import cttk.dto.OrderBook.SimpleOrder;
import cttk.request.OpenOrderBookStreamRequest;

public class OpenOrderBookSnapshotStreamExample {

    public static void main(String[] args)
        throws Exception
    {
        final String cxId = CXIds.BITFINEX;

        final CXStream<OrderBook> stream = CXStreams.openOrderBookSnapshotStream(cxId,
            new OpenOrderBookStreamRequest().setCurrencyPair("ETH", "BTC"),
            createPrintOrderBook());

        Thread.sleep(60000);
        stream.close();
    }

    public static CXStreamListener<OrderBook> createPrintOrderBook() {
        return new CXStreamListener<OrderBook>() {

            @Override
            public void onMessage(OrderBook orderBook) {
                //orderBook = orderBook.group(5);
                List<SimpleOrder> bids = SimpleOrder.sortByPriceDesc(orderBook.getBids());
                List<SimpleOrder> asks = SimpleOrder.sortByPriceAsc(orderBook.getAsks());

                System.out.println(String.format("BIDS: %d, ASKS: %d", bids.size(), asks.size()));
                for (int i = 0; i < 10; i++) {
                    SimpleOrder bid = bids.get(i);
                    SimpleOrder ask = asks.get(i);

                    System.out.println(String.format("%10s %10s | %-10s %10s",
                        bid.getQuantity().setScale(2, RoundingMode.HALF_UP),
                        bid.getPrice().setScale(6, RoundingMode.HALF_UP),
                        ask.getPrice().setScale(6, RoundingMode.HALF_UP),
                        ask.getQuantity().setScale(2, RoundingMode.HALF_UP)));
                }

                System.out.println("========================================");
            }

            @Override
            public void onFailure(CXStream<OrderBook> stream, Throwable t) {
                t.printStackTrace();
            }
        };
    }
}
