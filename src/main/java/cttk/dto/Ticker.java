package cttk.dto;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import cttk.AbstractCurrencyPair;
import cttk.DateTimeHolder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(chain = true)
public class Ticker
    extends AbstractCurrencyPair<Ticker>
    implements DateTimeHolder<Ticker>
{
    String id;
    ZonedDateTime dateTime;

    BigDecimal openPrice;
    BigDecimal lastPrice;
    BigDecimal lowPrice;
    BigDecimal highPrice;
    BigDecimal avgPrice;

    BigDecimal volume;
    BigDecimal volume7day;
    BigDecimal quoteVolume;

    BigDecimal highestBid;
    BigDecimal lowestAsk;

    BigDecimal change;
    BigDecimal changePercent;

    Long numOfTrades;
    Boolean isFrozen;
}
