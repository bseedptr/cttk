package cttk.impl.bitfinex;

import static cttk.impl.bitfinex.BitfinexUtils.getMarketSymbolWithT;
import static cttk.impl.bitfinex.BitfinexUtils.toOrderBookPrecisionParamValue;
import static cttk.impl.bitfinex.BitfinexUtils.toStdCurrencyPair;
import static cttk.impl.util.Objects.nvl;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

import cttk.PublicCXClient;
import cttk.dto.MarketInfos;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Tickers;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.impl.bitfinex.response.BitfinexOrderBookResponse;
import cttk.impl.bitfinex.response.BitfinexRecentTradesResponse;
import cttk.impl.bitfinex.response.BitfinexSymbolsDetailsResponse;
import cttk.impl.bitfinex.response.BitfinexTickerResponse;
import cttk.impl.bitfinex.response.BitfinexTickersResponse;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ParamsBuilder;
import cttk.request.GetOrderBookRequest;
import cttk.request.GetRecentTradesRequest;
import cttk.request.GetTickerRequest;
import cttk.request.GetTradeHistoryRequest;

public class BitfinexPublicClient
    extends AbstractBitfinexClient
    implements PublicCXClient
{
    public BitfinexPublicClient() {
        super();
    }

    @Override
    public MarketInfos getMarketInfos()
        throws CTTKException
    {
        final HttpResponse response = requestGet("/v1/symbols_details");

        checkResponseError(null, response);

        try {
            return convertList(
                response,
                BitfinexSymbolsDetailsResponse.class,
                data -> new MarketInfos().setMarketInfos(data.stream().map(i -> i.toMarketInfo()).collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getMarketInfos", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public Tickers getAllTickers()
        throws CTTKException
    {
        final String symbols = getSymbols().stream().collect(Collectors.joining(","));
        final HttpResponse response = requestGet("/v2/tickers", ParamsBuilder.buildIfNotNull("symbols", symbols));

        checkResponseError(null, response);

        try {
            return convertList(response, BitfinexTickersResponse.class,
                (data) -> new Tickers()
                    .setTickers(data.stream().map(i -> i.toTicker()).collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getAllTickers", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    public List<String> getSymbols()
        throws CTTKException
    {
        final HttpResponse response = requestGet("/v1/symbols");

        checkResponseError(null, response);

        try {
            return convertList(response, String.class, (data) -> data.stream().map(i -> "t" + i.toUpperCase()).collect(Collectors.toList()));
        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getSymbols", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public Ticker getTicker(GetTickerRequest request)
        throws CTTKException
    {
        final HttpResponse response = requestGet("/v2/ticker/"
            + BitfinexUtils.getMarketSymbolWithT(request));

        checkResponseError(request, response);

        return convert(response, BitfinexTickerResponse.class, i -> i.toTicker(request));
    }

    @Override
    public OrderBook getOrderBook(GetOrderBookRequest request)
        throws CTTKException
    {
        checkOrderBookCountParamValue(request.getCount());
        final HttpResponse response = requestGet(
            "/v2/book/" + getMarketSymbolWithT(request) + "/" + toOrderBookPrecisionParamValue(request.getPrecision()),
            ParamsBuilder.build("len", nvl(request.getCount(), 100)));

        checkResponseError(request, response);

        try {
            return convertList(response, BitfinexOrderBookResponse.class,
                (data) -> new OrderBook()
                    .setDateTime(ZonedDateTime.now())
                    .setCurrencyPair(toStdCurrencyPair(request))
                    .setBids(data.stream().filter(i -> !i.isAsk()).map(i -> i.toSimpleOrder()).collect(Collectors.toList()))
                    .setAsks(data.stream().filter(i -> i.isAsk()).map(i -> i.toSimpleOrder()).collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getOrderBook", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    private void checkOrderBookCountParamValue(Integer count)
        throws CTTKException
    {
        if (count != null
            && count != 25
            && count != 100)
        {
            throw new CTTKException("invalid count (len): " + count).setCxId(this.getCxId());
        }
    }

    @Override
    public Trades getRecentTrades(GetRecentTradesRequest request)
        throws CTTKException
    {
        HttpResponse response = requestGet("/v2/trades/" + BitfinexUtils.getMarketSymbolWithT(request) + "/hist",
            ParamsBuilder.create()
                .putIfNotNull("limit", request.getCount())
                .build());

        checkResponseError(request, response);

        try {
            return convertList(response, BitfinexRecentTradesResponse.class,
                (data) -> new Trades()
                    .setCurrencyPair(toStdCurrencyPair(request))
                    .setTrades(data.stream().map(i -> i.toTrade()).collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getRecentTrades", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public Trades getTradeHistory(GetTradeHistoryRequest request)
        throws CTTKException
    {
        HttpResponse response = requestGet("/v2/trades/" + BitfinexUtils.getMarketSymbolWithT(request) + "/hist",
            ParamsBuilder.create()
                .put("start", request.getStartEpochInMilli())
                .put("end", request.getEndEpochInMilli())
                .putIfNotNull("limit", request.getCount())
                .put("sort", "1")
                .build());

        checkResponseError(request, response);

        try {
            return convertList(response, BitfinexRecentTradesResponse.class,
                (data) -> new Trades()
                    .setCurrencyPair(toStdCurrencyPair(request))
                    .setTrades(data.stream().map(i -> i.toTrade()).collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getTradeHistory", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }
}
