package cttk.impl.poloniex;

import static cttk.CXIds.POLONIEX;
import static cttk.impl.poloniex.PoloniexUtils.getMarketSymbol;
import static cttk.impl.poloniex.PoloniexUtils.getMarketSymbolOrAll;
import static cttk.impl.poloniex.PoloniexUtils.toPoloniexCurrencySymbol;
import static cttk.impl.util.Objects.nvl;
import static cttk.util.DeltaUtils.sleep;
import static cttk.util.DeltaUtils.sleepWithException;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import cttk.OrderStatus;
import cttk.PricingType;
import cttk.PrivateCXClient;
import cttk.TradingFeeProvider;
import cttk.auth.Credential;
import cttk.dto.Balances;
import cttk.dto.BankAccount;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.dto.UserAccount;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;
import cttk.dto.Wallet;
import cttk.dto.Wallets;
import cttk.dto.WithdrawResult;
import cttk.exception.CTTKException;
import cttk.exception.CXServerException;
import cttk.exception.InsufficientBalanceException;
import cttk.exception.InvalidAmountMinException;
import cttk.exception.InvalidOrderException;
import cttk.exception.InvalidPriceMinException;
import cttk.exception.InvalidPriceRangeException;
import cttk.exception.InvalidTotalMinException;
import cttk.exception.MaxNumOrderException;
import cttk.exception.OrderNotFoundException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.poloniex.response.PoloniexAllTradesResponse;
import cttk.impl.poloniex.response.PoloniexAllUserOrdersResponse;
import cttk.impl.poloniex.response.PoloniexBalancesResponse;
import cttk.impl.poloniex.response.PoloniexCreateOrderResponse;
import cttk.impl.poloniex.response.PoloniexMessageResponse;
import cttk.impl.poloniex.response.PoloniexOrderStatusResponse;
import cttk.impl.poloniex.response.PoloniexTradesResponse;
import cttk.impl.poloniex.response.PoloniexTradingFeeResponse;
import cttk.impl.poloniex.response.PoloniexTransfersResponse;
import cttk.impl.poloniex.response.PoloniexUserOrdersResponse;
import cttk.impl.poloniex.response.PoloniexWalletsResponse;
import cttk.impl.util.CryptoUtils;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ParamsBuilder;
import cttk.impl.util.UriUtils;
import cttk.request.CancelOrderRequest;
import cttk.request.CreateMarketPriceOrderRequest;
import cttk.request.CreateOrderRequest;
import cttk.request.GetBankAccountRequest;
import cttk.request.GetOpenUserOrdersRequest;
import cttk.request.GetTransfersDeltaRequest;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserAccountRequest;
import cttk.request.GetUserOrderHistoryRequest;
import cttk.request.GetUserOrderRequest;
import cttk.request.GetUserTradesDeltaRequest;
import cttk.request.GetUserTradesRequest;
import cttk.request.GetWalletRequest;
import cttk.request.WithdrawToBankRequest;
import cttk.request.WithdrawToWalletRequest;
import cttk.util.DeltaUtils;
import cttk.util.StringUtils;
import okhttp3.Headers;
import okhttp3.Request;
import okhttp3.Response;

public class PoloniexPrivateClient
    extends AbstractPoloniexClient
    implements PrivateCXClient
{
    protected static final String BASE_URL = "https://poloniex.com/tradingApi";

    protected final Credential credential;

    public PoloniexPrivateClient(Credential credential) {
        super();
        this.credential = credential;

    }

    @Override
    public UserAccount getUserAccount(GetUserAccountRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public TradingFeeProvider getTradingFeeProvider()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("returnFeeInfo");

        checkResponseError(null, response);

        return convert(response, PoloniexTradingFeeResponse.class, i -> i.getTradingFee());
    }

    @Override
    public BankAccount getBankAccount(GetBankAccountRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public WithdrawResult withdraw(WithdrawToBankRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public Wallet getWallet(GetWalletRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("returnDepositAddresses");

        checkResponseError(request, response);

        if (isEmptyArrayBody(response)) return new Wallet().setCurrency(request.getCurrency());
        return convert(response, PoloniexWalletsResponse.class, (i -> i.toWallet(request.getCurrency())));
    }

    @Override
    public Wallets getAllWallets()
        throws CTTKException
    {
        final HttpResponse response = requestPost("returnDepositAddresses");

        checkResponseError(null, response);

        if (isEmptyArrayBody(response)) return new Wallets();
        return convert(response, PoloniexWalletsResponse.class, i -> i.toWallets());
    }

    @Override
    public WithdrawResult withdraw(WithdrawToWalletRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("withdraw", ParamsBuilder.create()
            .putIfNotNull("currency", toPoloniexCurrencySymbol(request.getCurrency()))
            .putIfNotNull("amount", request.getQuantity())
            .putIfNotNull("address", request.getAddress())
            .putIfNotNull("paymentId", request.getPaymentId())
            .build());

        checkResponseError(request, response);

        convert(response, PoloniexMessageResponse.class);

        // poloniex does not provide withdraw id
        return new WithdrawResult();
    }

    @Override
    public Balances getAllBalances()
        throws CTTKException
    {
        final HttpResponse response = requestPost("returnCompleteBalances");
        if (isEmptyArrayBody(response)) return new Balances();
        checkResponseError(null, response);
        return convert(response, PoloniexBalancesResponse.class, i -> i.toBalances());
    }

    /**
     * Canceled order cannot be checked
     * @param request
     * @return
     * @throws CTTKException
     * @throws UnsupportedCurrencyPairException
     */
    @Override
    public UserOrder getUserOrder(GetUserOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {

        final HttpResponse response = requestPost("returnOrderStatus",
            ParamsBuilder.build("orderNumber", request.getOid()));

        try {
            checkResponseError(request, response);
        } catch (OrderNotFoundException e) {
            final HttpResponse orderTradeResponse = getOrderTradesResponse(request.getOid());

            if (orderTradeResponse == null) {
                // Order does not exist in open orders and also in trades
                // This means either cancelled order before filled
                // Or this orderNumber is invalid
                return null;
            } else {
                // By here we know that trades exist but not the order (Already filled order)
                final UserOrder orderFromTrades = convert(orderTradeResponse, PoloniexTradesResponse.class,
                    i -> i.toUserOrder(request.getOid(), PricingType.LIMIT, OrderStatus.FILLED));
                return orderFromTrades;
            }
        }
        // Order is either open or partially filled
        final UserOrder order = convert(response, PoloniexOrderStatusResponse.class, i -> i.toUserOrder(PricingType.LIMIT));

        // For Partially filled order we need to set the trades
        if (order.getStatusType() == OrderStatus.PARTIALLY_FILLED) {
            UserTrades trade = convert(getOrderTradesResponse(request.getOid()), PoloniexTradesResponse.class,
                i -> i.toUserTrades(order));

            order.setTrades(trade.getTrades());
            UserTrade tradeOne = trade.get(0);

            BigDecimal fee = BigDecimal.ZERO;
            // Fill in some trade specific infomation to UserOrder
            for (int i = 0; i < trade.size(); i++) {
                fee = trade.get(0).getFee().add(fee);
            }
            order.setFee(fee);
            order.setFeeRate(tradeOne.getFeeRate());
            order.setFeeCurrency(tradeOne.getFeeCurrency());

        }

        return order;
    }

    @Override
    public UserOrders getOpenUserOrders(GetOpenUserOrdersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("returnOpenOrders",
            ParamsBuilder.build("currencyPair", getMarketSymbol(request)));

        checkResponseError(request, response);

        final UserOrders orders = convert(response, PoloniexUserOrdersResponse.class, i -> i.toUserOrders(request, PricingType.LIMIT));
        if (request.isWithTrades()) fillTrades(orders);
        return orders;
    }

    @Override
    public UserOrders getAllOpenUserOrders(boolean withTrades)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("returnOpenOrders",
            ParamsBuilder.build("currencyPair", "ALL"));

        checkResponseError(null, response);

        final UserOrders orders = convert(response, PoloniexAllUserOrdersResponse.class, i -> i.toUserOrders(PricingType.LIMIT));
        if (withTrades) fillTrades(orders);
        return orders;
    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    protected UserOrders fillTrades(UserOrders orders) {
        if (orders != null && orders.isNotEmpty()) {
            orders.getOrders().forEach(i -> fillTrades(i, getOrderTrades(i.getOid())));
        }
        return orders;
    }

    protected UserOrder fillTrades(UserOrder order, List<UserTrade> trades) {
        if (order == null) return null;
        order.setTrades(trades);
        if (order.hasTrades()) {
            order.setFilledQuantity(order.getTradedQuantity());
            if (order.getQuantity().compareTo(order.getFilledQuantity()) == 0) {
                order.setStatus("filled");
                order.setStatusType(OrderStatus.FILLED);
            } else {
                order.setStatus("partially-filled");
                order.setStatusType(OrderStatus.PARTIALLY_FILLED);
            }
        } else {
            order.setStatus("new");
            order.setStatusType(OrderStatus.UNFILLED);
        }
        return order;
    }

    @Override
    public UserOrder createOrder(CreateOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        ZonedDateTime requestTime = DateTimeUtils.zdtFromEpochMilli(System.currentTimeMillis());
        final HttpResponse response = requestPost(PoloniexUtils.toString(request.getSide()),
            ParamsBuilder.create()
                .putIfNotNull("currencyPair", getMarketSymbol(request))
                .putIfNotNull("rate", request.getPrice())
                .putIfNotNull("amount", request.getQuantity())
                .build());

        checkOrderResponseError(request, response);
        checkResponseError(request, response);

        UserOrder createOrderResult = convert(response, PoloniexCreateOrderResponse.class, PoloniexCreateOrderResponse::toUserOrder);

        // Create order was immediately filled
        if (createOrderResult.hasTrades()) {
            UserOrder userOrder = getUserOrder(new GetUserOrderRequest().setOid(createOrderResult.getOid())).setDateTime(requestTime);
            return fillResponseByRequest(request, userOrder);
        }
        return fillResponseByRequest(request, createOrderResult);
    }

    @Override
    public void cancelOrder(CancelOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("cancelOrder",
            ParamsBuilder.build("orderNumber", request.getOrderId()));

        checkResponseError(request, response);

        convert(response, PoloniexMessageResponse.class);
    }

    @Override
    public UserOrder createMarketPriceOrder(CreateMarketPriceOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        if (StringUtils.isEmpty(request.getOrderId())) {
            final String symbol = getMarketSymbolOrAll(request);
            final HttpResponse response = requestPost("returnTradeHistory",
                ParamsBuilder.create()
                    .putIfNotNull("currencyPair", symbol)
                    .put("start", nvl(request.getStartEpochInSecond(), ZonedDateTime.now().minusMonths(3).toEpochSecond()))
                    .put("end", nvl(request.getEndEpochInSecond(), ZonedDateTime.now().toEpochSecond()))
                    .putIfNotNull("limit", request.getCount())
                    .build());

            checkResponseError(request, response);

            if ("ALL".equals(symbol)) {
                if (response.getBody().equals("[]")) {
                    return new UserTrades();
                } else {
                    return convert(response, PoloniexAllTradesResponse.class, i -> i.toUserTrades());
                }
            } else {
                return convert(response, PoloniexTradesResponse.class, i -> i.toUserTrades(request));
            }
        } else {
            return getUserTradesByOrderId(request.getOrderId());
        }
    }

    private UserTrades getUserTradesByOrderId(String orderId) {
        List<UserTrade> orderTrades = getOrderTrades(orderId);
        if (orderTrades.isEmpty())
            return null;
        return new UserTrades().setList(orderTrades).setCxId(getCxId());
    }

    private List<UserTrade> getOrderTrades(String orderId) {
        HttpResponse orderTradesResponse = getOrderTradesResponse(orderId);
        if (orderTradesResponse == null) return Collections.emptyList();
        try {
            return convert(orderTradesResponse, PoloniexTradesResponse.class, i -> i.getUserTradeList(null));
        } catch (CTTKException e) {
            logger.info("No trades in order {}", orderId);
            return Collections.emptyList();
        }
    }

    private HttpResponse getOrderTradesResponse(String oid) {
        try {
            final HttpResponse response = requestPost("returnOrderTrades",
                ParamsBuilder.build("orderNumber", oid));
            checkResponseError(null, response);
            // TODO
            if (response.getBody().contains("Order not found")) {
                logger.info("No trades in order {}", oid);
                return null;
            }

            return response;
        } catch (CTTKException e) {
            logger.info("No trades in order {}", oid);
            return null;
        }
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP t-d: time-desc,<<<(exception)
        final GetUserTradesRequest partialRequest = new GetUserTradesRequest()
            .setCurrencyPair(request)
            .setStartDateTime(request.getStartDateTime())
            .setEndDateTime(nvl(request.getEndDateTime(), ZonedDateTime.now()))
            .setCount(10000);

        if (request.hasStartDateTime()) {
            final Map<String, UserTrade> map = new LinkedHashMap<>();
            UserTrade prevFirst = null;
            UserTrade prevLast = null;
            while (true) {
                final UserTrades userTrades = getUserTrades(partialRequest);
                final UserTrade first = userTrades.getOldest();
                final UserTrade last = userTrades.getLatest();

                if (userTrades.isEmpty() || UserTrade.equalsById(prevFirst, first)) break;
                map.putAll(DeltaUtils.convertToMap(userTrades));

                final int s = DeltaUtils.calcNextSeconds(prevLast, last);
                partialRequest.setEndDateTime(first.getDateTime().plusSeconds(s));
                prevFirst = first;
                prevLast = last;

                sleepWithException(request.getSleepTimeMillis(), POLONIEX);
            }

            return DeltaUtils.convertUserTradesFromMap(map);
        } else {
            return getUserTrades(partialRequest);
        }
    }

    @Override
    public Transfers getTransfers(GetTransfersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("returnDepositsWithdrawals",
            ParamsBuilder.create()
                .putIfNotNull("start", request.getStartEpochInSecond())
                .putIfNotNull("end", nvl(request.getEndEpochInSecond(), ZonedDateTime.now().toEpochSecond())) // end is required
                .build());

        checkResponseError(request, response);

        return convert(response, PoloniexTransfersResponse.class, i -> i.toTransfers());
    }

    @Override
    public Transfers getTransfers(GetTransfersDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP t-a: time-asc,>>>(break)
        final GetTransfersRequest partialRequest = new GetTransfersRequest()
            .setCurrency(request.getCurrency())
            .setStartDateTime(request.getStartDateTime())
            .setEndDateTime(nvl(request.getEndDateTime(), ZonedDateTime.now()))
            .setCount(1000);
        // Sorting in asc order implicitly

        if (request.hasStartDateTime()) {
            final Map<String, Transfer> map = new LinkedHashMap<>();
            Transfer prevLast = null;
            while (true) {
                final Transfers transfers = getTransfers(partialRequest);
                final Transfer last = transfers.getLatest();

                if (transfers.isEmpty() || Transfer.equalsById(prevLast, last)) break;
                map.putAll(DeltaUtils.convertToMap(transfers));

                partialRequest.setStartDateTime(transfers.getMaxDateTime());
                prevLast = last;

                if (!sleep(request.getSleepTimeMillis())) break;
            }

            return DeltaUtils.convertTransfersFromMap(map);
        } else {
            return getTransfers(partialRequest);
        }
    }

    protected HttpResponse requestPost(String command)
        throws CTTKException
    {
        return requestPost(command, null);
    }

    protected HttpResponse requestPost(String endpoint, Map<String, String> params)
        throws CTTKException
    {
        params = putCommand(params, endpoint);

        final Request request = new Request.Builder()
            .url(BASE_URL)
            .headers(createHttpHeaders(credential, endpoint, params))
            .post(super.createEncodedFormBody(params))
            .build();

        debug(request, params);

        try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
            return toHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    protected Headers createHttpHeaders(Credential credential, String endpoint, Map<String, String> params) {

        final String data = UriUtils.toEncodedQueryParamsString(params);
        final String signature = CryptoUtils.hmacSha512Hex(data, credential.getSecretKey());

        final HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Key", credential.getAccessKey());
        headers.put("Sign", signature);

        return new Headers.Builder()
            .add("Key", credential.getAccessKey())
            .add("Sign", signature)
            .build();
    }

    protected Map<String, String> putCommand(Map<String, String> params, String command) {
        if (params == null) params = new HashMap<>();
        params.put("command", command);
        params.put("nonce", nonce());
        return params;
    }

    protected static String nonce() {
        return String.valueOf(System.currentTimeMillis());
    }

    protected static final boolean isEmptyArrayBody(HttpResponse httpResponse) {
        return httpResponse != null && httpResponse.is2xxSuccessful() && httpResponse.getBody().equals("[]");
    }

    private void checkOrderResponseError(CreateOrderRequest request, HttpResponse response)
        throws CTTKException
    {
        final String body = response.getBody();
        final Integer statusCode = response.getStatus();

        String exchangeMessage;
        try {
            Map<?, ?> map = objectMapper.readValue(body, Map.class);
            exchangeMessage = (String) map.get("error");
        } catch (IOException e) {
            return;
        }
        if (exchangeMessage == null) return;

        if (exchangeMessage.contains("Amount must be at least")) {
            throw new InvalidAmountMinException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity());
        } else if (exchangeMessage.contains("Rate must be greater than zero.")) {
            throw new InvalidPriceMinException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity());
        } else if (exchangeMessage.contains("Total must be at least")) {
            throw new InvalidTotalMinException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity())
                .setTotal(request.getPrice().multiply(request.getQuantity()));
        } else if (exchangeMessage.contains("Invalid rate parameter.")) {
            throw new InvalidPriceRangeException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity());
        } else if (exchangeMessage.contains("Not enough")) {
            throw new InsufficientBalanceException()
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity());
        } else if (exchangeMessage.contains("Error placing order")
            || exchangeMessage.contains("Invalid amount parameter"))
        {
            throw new InvalidOrderException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity());
        } else if (exchangeMessage.contains("You may not have more than") && exchangeMessage.contains("open orders in a single market.")) {
            throw new MaxNumOrderException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency())
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity());
        }
    }
}
