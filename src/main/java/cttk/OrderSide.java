package cttk;

import static cttk.util.StringUtils.toUpper;

public enum OrderSide {
    SELL, BUY;

    public static OrderSide of(String side) {
        if (side == null) return null;
        switch (toUpper(side)) {
            case "ASK":
            case "SELL":
                return OrderSide.SELL;
            case "BID":
            case "BUY":
                return OrderSide.BUY;
            default:
                return null;
        }
    }
}
