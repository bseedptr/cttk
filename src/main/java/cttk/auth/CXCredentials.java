package cttk.auth;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CXCredentials {
    Credential dataKey;
    Credential tradeKey;
    Credential withdrawKey;

    public String getAccessKey() {
        return dataKey == null ? null : dataKey.getAccessKey();
    }

    public String getSecretKey() {
        return dataKey == null ? null : dataKey.getSecretKey();
    }

    public Credential getTradeKeyOrDataKey() {
        return ifnull(tradeKey, dataKey);
    }

    public Credential getWithdrawKeyOrDataKey() {
        return ifnull(withdrawKey, dataKey);
    }

    private static <T> T ifnull(T a, T b) {
        return a == null ? b : a;
    }
}
