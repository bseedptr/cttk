package cttk.impl.bittrex.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.dto.OrderBook.SimpleOrder;
import lombok.Data;

@Data
public class BittrexOrder {
    @JsonProperty("Quantity")
    String quantity;
    @JsonProperty("Rate")
    String rate;

    public SimpleOrder toSimpleOrder() {
        return new SimpleOrder(rate, quantity);
    }
}
