package cttk.impl.bibox.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import cttk.OrderSide;
import cttk.dto.UserTrade;
import cttk.impl.bibox.BiboxUtils;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class BiboxUserOrderHistory {
    String id;
    Long createdAt;
    String accountType;
    @JsonProperty("coin_symbol")
    String coinSymbol;
    @JsonProperty("currency_symbol")
    String currencySymbol;
    OrderSide orderSide;
    String orderType;
    BigDecimal price;
    BigDecimal amount;
    BigDecimal money;
    BigDecimal fee;
    boolean isPayBix;
    @JsonProperty("fee_symbol")
    String feeSymbol;

    public UserTrade toUserTrade() {
        return new UserTrade()
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(createdAt))
            .setCurrencyPair(coinSymbol, currencySymbol)
            .setTid(id)
            .setOid(id)
            .setSide(orderSide)
            .setPrice(price)
            .setQuantity(amount)
            .setFee(fee)
            .setFeeCurrency(feeSymbol)
            .setTotal(MathUtils.multiply(price, amount));
    }

    @JsonProperty("account_type")
    public void setAccountType(Integer code) {
        this.accountType = BiboxUtils.parseAccountType(code);
    }

    @JsonProperty("order_side")
    public void setOrderSide(String side) {
        this.orderSide = BiboxUtils.parseOrderSide(side);
    }

    @JsonProperty("order_type")
    public void setOrderType(Integer code) {
        this.orderType = BiboxUtils.parseOrderType(code);
    }

    @JsonProperty("pay_bix")
    public void setPayBix(Integer code) {
        this.isPayBix = BiboxUtils.parsePayBix(code);
    }
}
