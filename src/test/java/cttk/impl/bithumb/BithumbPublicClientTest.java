package cttk.impl.bithumb;

import org.junit.Assert;
import org.junit.Test;

import cttk.CXIds;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.AbstractPublicCXClientTest;
import cttk.request.GetTradeHistoryRequest;

public class BithumbPublicClientTest
    extends AbstractPublicCXClientTest
{
    @Override
    public String getCxId() {
        return CXIds.BITHUMB;
    }

    @Test
    public void testGetTradeHistory()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();

        final GetTradeHistoryRequest request = new GetTradeHistoryRequest()
            .setCurrencyPair(getValidCurrencyPair())
            .setLastTradeId(37417l);

        final Trades trades = client.getTradeHistory(request);

        logger.debug("{}", trades);

        Assert.assertNotNull(trades);
        Assert.assertEquals(request.getBaseCurrency(), trades.getBaseCurrency());
        Assert.assertEquals(request.getQuoteCurrency(), trades.getQuoteCurrency());
    }
}
