package cttk;

import cttk.dto.Balance;
import cttk.dto.Balances;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;
import lombok.ToString;

@ToString(callSuper = true)
public class UserData {
    final UserDataType type;
    final Object data;

    private UserData(UserDataType type, Object data) {
        this.type = type;
        this.data = data;
    }

    public UserDataType getType() {
        return type;
    }

    public Object getData() {
        return data;
    }

    public UserOrder asUserOrder() {
        return data instanceof UserOrder ? (UserOrder) data : null;
    }

    public UserOrders asUserOrders() {
        return data instanceof UserOrders ? (UserOrders) data : null;
    }

    public UserTrade asUserTrade() {
        return data instanceof UserTrade ? (UserTrade) data : null;
    }

    public UserTrades asUserTrades() {
        return data instanceof UserTrades ? (UserTrades) data : null;
    }

    public Transfer asTransfer() {
        return data instanceof Transfer ? (Transfer) data : null;
    }

    public Transfers asTransfers() {
        return data instanceof Transfers ? (Transfers) data : null;
    }

    public Balance asBalance() {
        return data instanceof Balance ? (Balance) data : null;
    }

    public Balances asBalances() {
        return data instanceof Balances ? (Balances) data : null;
    }

    public static UserData of(UserOrder data) {
        return new UserData(UserDataType.UserOrder, data);
    }

    public static UserData of(UserOrders data) {
        return new UserData(UserDataType.UserOrders, data);
    }

    public static UserData of(UserTrade data) {
        return new UserData(UserDataType.UserTrade, data);
    }

    public static UserData of(UserTrades data) {
        return new UserData(UserDataType.UserTrades, data);
    }

    public static UserData of(Transfer data) {
        return new UserData(UserDataType.Transfer, data);
    }

    public static UserData of(Transfers data) {
        return new UserData(UserDataType.Transfers, data);
    }

    public static UserData of(Balance data) {
        return new UserData(UserDataType.Balance, data);
    }

    public static UserData of(Balances data) {
        return new UserData(UserDataType.Balances, data);
    }
}
