package cttk.impl.hitbtc.stream.response;

import static cttk.impl.hitbtc.HitbtcUtils.toStdCurrencyPair;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.dto.OrderBook;
import cttk.dto.OrderBook.SimpleOrder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HitbtcWebsocketOrderBookResponse
    extends HitbtcWebsocketResponse<HitbtcOrderBookTick>
{
    public OrderBook toOrderBook(CurrencyPair currencyPair) {
        return new OrderBook()
            .setCurrencyPair(toStdCurrencyPair(currencyPair))
            .setDateTime(ZonedDateTime.now())
            .setAsks(toSimpleOrderList(params.ask))
            .setBids(toSimpleOrderList(params.bid));
    }

    static List<SimpleOrder> toSimpleOrderList(List<HitbtcOrderBookTickOrder> orders) {
        if (orders == null) return null;
        return orders.stream()
            .map(i -> new SimpleOrder(i.getPrice(), i.getSize()))
            .collect(Collectors.toList());
    }
}

@Data
class HitbtcOrderBookTick {
    List<HitbtcOrderBookTickOrder> ask;
    List<HitbtcOrderBookTickOrder> bid;
    String symbol;
    String sequence;
}

@Data
class HitbtcOrderBookTickOrder {
    String price;
    String size;
}