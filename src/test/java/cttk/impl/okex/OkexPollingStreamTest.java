package cttk.impl.okex;

import cttk.CXIds;
import cttk.impl.AbstractPollingCXStreamTest;

public class OkexPollingStreamTest
    extends AbstractPollingCXStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.OKEX;
    }
}
