package cttk.impl.kraken.response;

import java.util.List;

import cttk.dto.UserOrder;

public class KrakenQueryUserOrdersResponse
    extends KrakenResponse<KrakenUserOrders>
{
    public UserOrder toUserOrder() {
        if (result == null) return null;
        List<UserOrder> orders = result.getUserOrderList();
        return orders == null ? null : orders.get(0);
    }
}
