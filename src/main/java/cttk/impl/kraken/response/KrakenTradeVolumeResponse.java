package cttk.impl.kraken.response;

import java.util.Map;

import cttk.CurrencyPair;
import cttk.dto.TradingFee;
import cttk.impl.kraken.KrakenUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class KrakenTradeVolumeResponse
    extends KrakenResponse<KrakenTradeVolume>
{
    public TradingFee getTradingFee(KrakenAssetPairs krakenAssetPairs, final CurrencyPair currencyPair) {
        return result == null ? null : result.getTradingFee(krakenAssetPairs, currencyPair).percentToRatio();
    }
}

//{
//"currency": "ZUSD",
//"volume": "0.0000",
//"fees": {
//  "XETHXXBT": {
//      "fee": "0.2600",
//      "minfee": "0.1000",
//      "maxfee": "0.2600",
//      "nextfee": "0.2400",
//      "nextvolume": "50000.0000",
//      "tiervolume": "0.0000"
//  }
//},
//"fees_maker": {
//  "XETHXXBT": {
//      "fee": "0.1600",
//      "minfee": "0.0000",
//      "maxfee": "0.1600",
//      "nextfee": "0.1400",
//      "nextvolume": "50000.0000",
//      "tiervolume": "0.0000"
//  }
//}
//}
@Data
class KrakenTradeVolume {
    String currency;
    String volume;
    Map<String, KrakenFee> fees;
    Map<String, KrakenFee> fees_maker;

    public TradingFee getTradingFee(KrakenAssetPairs krakenAssetPairs, CurrencyPair currencyPair) {
        String assetPair = KrakenUtils.getMarketSymbol(krakenAssetPairs, currencyPair);
        KrakenFee takerFee = fees.get(assetPair);
        KrakenFee makerFee = fees_maker.get(assetPair);

        return new TradingFee().setFees(takerFee == null ? null : takerFee.fee, makerFee == null ? null : makerFee.fee);
    }
}

@Data
class KrakenFee {
    String fee;
    String minfee;
    String nextfee;
    String nextvolume;
    String tiervolume;
}
