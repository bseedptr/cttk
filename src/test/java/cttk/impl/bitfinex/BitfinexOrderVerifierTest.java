package cttk.impl.bitfinex;

import cttk.CXIds;
import cttk.impl.AbstractOrderVerifierTest;

public class BitfinexOrderVerifierTest
    extends AbstractOrderVerifierTest
{
    @Override
    public String getCxId() {
        return CXIds.BITFINEX;
    }

    @Override
    protected long getThreadSleepMillis() {
        return 8000l;
    }
}
