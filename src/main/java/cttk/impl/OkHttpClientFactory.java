package cttk.impl;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;

public class OkHttpClientFactory {
    // It is efficient to keep connection pool for each cryptocurrency exchange.
    // Each cryptocurrency exchange is implemented as an individual class, and hence connection pool is managed for each implementing class
    private static final Map<Class<?>, OkHttpClient> MAP = new HashMap<>();

    public static OkHttpClient createOkHttpClient(Class<?> claz) {
        return getBaseClient(claz).newBuilder().build();
    }

    private static OkHttpClient getBaseClient(Class<?> claz) {
        if (!MAP.containsKey(claz)) {
            MAP.put(claz, new OkHttpClient.Builder()
                .connectionPool(new ConnectionPool())
                .build());
        }
        return MAP.get(claz);
    }

    public static OkHttpClient createOkHttpClient(ConnectionPool connectionPool) {
        return new OkHttpClient.Builder()
            .connectionPool(connectionPool)
            .build();
    }

    /**
     * Create an OkHttpClient that ignores all SSL certificates.
     * It is highly discouraged to use this method.
     * @param <T> class type
     * @param claz the class
     * @return Unsafe OkHttpClient
     */
    public static <T> OkHttpClient createUnsafeOkHttpClient(Class<?> claz) {
        try {
            final X509TrustManager trustManager = new X509TrustManager() {
                @Override
                public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType)
                    throws CertificateException
                {
                }

                @Override
                public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType)
                    throws CertificateException
                {
                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            };

            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] { trustManager };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            return getBaseClient(claz).newBuilder()
                .sslSocketFactory(sslSocketFactory, trustManager)
                .hostnameVerifier(
                    new HostnameVerifier()
                    {
                        @Override
                        public boolean verify(String hostname, SSLSession session) {
                            return true;
                        }
                    })
                .build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
