package cttk.impl.poloniex.response;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import cttk.OrderSide;
import cttk.dto.UserOrder;
import cttk.dto.UserTrade;
import cttk.impl.poloniex.PoloniexUtils;
import lombok.Data;

@Data
public class PoloniexCreateOrderResponse {
    Long orderNumber;
    List<PoloniexUserTrade> resultingTrades;

    public UserOrder toUserOrder() {
        return new UserOrder()
            .setOid(String.valueOf(this.orderNumber))
            .setTrades(getUserTradeList());
        // TODO fill avg price and executed amount and etc...
    }

    List<UserTrade> getUserTradeList() {
        if (resultingTrades == null) return null;
        return resultingTrades.stream()
            .map(i -> i.toUserTrade())
            .collect(Collectors.toList());
    }

    @Data
    static class PoloniexUserTrade {
        String tradeID;
        String date;
        String type;
        BigDecimal rate;
        BigDecimal amount;
        BigDecimal total;

        UserTrade toUserTrade() {
            return new UserTrade()
                .setTid(tradeID)
                .setDateTime(PoloniexUtils.parseZDT(date))
                .setSide(OrderSide.of(type))
                .setPrice(rate)
                .setQuantity(amount)
                .setTotal(total);
        }
    }
}
