package cttk.impl.bittrex.response;

import java.util.List;

import cttk.dto.Ticker;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BittrexMarketSummaryResponse
    extends BittrexResponse<List<BittrexMarketSummary>>
{
    public Ticker toTicker() {
        return result == null || result.isEmpty() ? null : result.get(0).toTicker();
    }
}
