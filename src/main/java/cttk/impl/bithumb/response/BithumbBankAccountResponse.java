package cttk.impl.bithumb.response;

import cttk.dto.BankAccount;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BithumbBankAccountResponse
    extends BithumbDataResponse<Void>
{
    String account;
    String bank;
    String BankUser;

    public BankAccount toBankAccount() {
        return new BankAccount()
            .setAccountNo(account)
            .setBankName(bank)
            .setBankUser(BankUser);
    }
}
