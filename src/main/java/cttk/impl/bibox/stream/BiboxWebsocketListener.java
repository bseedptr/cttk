package cttk.impl.bibox.stream;

import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.impl.bibox.BiboxUtils;
import cttk.impl.stream.AbstractWebsocketListener;
import cttk.impl.stream.MessageConverter;
import cttk.request.AbstractCXStreamRequest;
import cttk.util.StringUtils;
import okhttp3.Response;
import okhttp3.WebSocket;
import okio.ByteString;

public class BiboxWebsocketListener<T, R extends AbstractCXStreamRequest<R>>
    extends AbstractWebsocketListener<T, R>
{
    public BiboxWebsocketListener(
        final CXStream<T> stream,
        final String channel,
        final R request,
        final MessageConverter<T> converter,
        final CXStreamListener<T> listener)
    {
        super(stream, channel, request, converter, listener, CXIds.BIBOX);
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        webSocket.send(getSubscriptionPayload());
        logger.info("[{}] Open : {}", id, channel);
        this.listener.onOpen(stream);
    }

    private String getSubscriptionPayload() {
        return String.format("{\"event\":\"addChannel\",\"channel\":\"%s\"}", channel);
    }

    protected void handleMessage(WebSocket webSocket, String text)
        throws Exception
    {
        if (!StringUtils.isEmpty(text) && text.contains("{\"ping\"")) {
            webSocket.send(text.replace("\"ping\"", "\"pong\""));
            return;
        }

        BiboxUtils.checkStreamErrorByResponseMessage(request, text);
        T message = converter.convert(text);
        if (message != null) listener.onMessage(message);
    }

    protected String toString(ByteString bytes)
    {
        return bytes == null ? null : bytes.hex();
    }
}
