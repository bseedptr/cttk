package cttk.impl.okex.stream.response;

import static java.util.stream.Collectors.joining;

import java.util.ArrayList;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
public class OkexWebsocketErrorResponse
    extends ArrayList<OkexWebsocketErrorResponse.OkexWebsocketError>
{
    public boolean isOnlyUnsupportedCurrencyPairError() {
        return size() == 1 && getFirst().isUnsupportedCurrencyPairError();
    }

    public OkexWebsocketError getFirst() {
        return get(0);
    }

    public String getAllLongMessages() {
        return stream().map(OkexWebsocketError::getLongMessage).collect(joining(" / "));
    }

    @Data
    public static class OkexWebsocketError {
        private String channel;
        private OkexWebsocketErrorData data;

        public boolean isUnsupportedCurrencyPairError() {
            return getCode() == 20103;
        }

        public String getLongMessage() {
            return String.format("%s(%d): %s", channel, getCode(), getMessage());
        }

        public int getCode() {
            return data.getError_code();
        }

        public String getMessage() {
            return data.getError_msg();
        }

        public String getBaseCurrency() {
            return channel.split("_")[3];
        }

        public String getQuoteCurrency() {
            return channel.split("_")[4];
        }
    }
}

@Data
class OkexWebsocketErrorData {
    String result;
    String error_msg;
    int error_code;
}