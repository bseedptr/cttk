package cttk.impl.bibox.stream.response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cttk.OrderStatus;
import cttk.UserData;
import cttk.dto.UserOrder;
import cttk.exception.CTTKException;
import cttk.impl.bibox.response.BiboxUserOrderHistory;
import cttk.impl.bibox.response.BiboxUserOrderPending;
import cttk.util.StringUtils;
import lombok.Data;

public class BiboxWebsocketUserDataResponse extends BiboxWebsocketListResponse<BiboxWebsocketUserData> {
    public BiboxWebsocketUserDataResponse() {
        super(BiboxWebsocketUserData.class);
    }

    public List<UserData> toUserDataList()
        throws IOException, CTTKException
    {
        return this.getBiboxDataFirst().toUserDataList();
    }
}

@Data
class BiboxWebsocketUserData {
    private BiboxUserOrderPending orderpending;
    private BiboxUserOrderHistory history;
    private BiboxWebsocketUserDataAssets assets;
    private String result;

    public boolean isLogin() {
        return StringUtils.equals(result, "订阅成功");
    }

    List<UserData> toUserDataList() {
        List<UserData> userDataList = new ArrayList<>();
        // when you create an order - orderpending
        // when currency is traded with price of the order - orderpending
        // - with different price of the order - history + orderpending
        if (orderpending != null) {
            final UserOrder userOrder = orderpending.toUserOrder();
            userDataList.add(UserData.of(userOrder));
            if (userOrder.getStatusType() == OrderStatus.FILLED) {
                // UserTrade when UserOrder status is FILLED because BIBOX doesn't manage partial UserTrades
                userDataList.add(UserData.of(orderpending.toUserTrade()));
            }
        }
        // if (history != null) userDataList.add(UserData.of(history.toUserTrade()));
        if (assets != null) userDataList.add(UserData.of(assets.toBalances()));
        return userDataList;
    }
}
