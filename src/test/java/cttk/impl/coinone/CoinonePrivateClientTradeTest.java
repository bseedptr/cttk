package cttk.impl.coinone;

import cttk.CXIds;
import cttk.impl.AbstractPrivateCXClientTradeTest;

public class CoinonePrivateClientTradeTest
    extends AbstractPrivateCXClientTradeTest
{
    @Override
    public String getCxId() {
        return CXIds.COINONE;
    }

    @Override
    protected long getThreadSleepMillis() {
        return 1000l;
    }
}
