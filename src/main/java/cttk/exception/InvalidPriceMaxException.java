package cttk.exception;

import java.math.BigDecimal;

import cttk.CurrencyPair;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class InvalidPriceMaxException
    extends InvalidPriceRangeException
{
    private static final long serialVersionUID = 1L;

    public InvalidPriceMaxException() {
        super();
    }

    public InvalidPriceMaxException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidPriceMaxException(String message) {
        super(message);
    }

    public InvalidPriceMaxException(Throwable cause) {
        super(cause);
    }

    @Override
    public InvalidPriceMaxException setCxId(String cxId) {
        super.setCxId(cxId);
        return this;
    }

    @Override
    public InvalidPriceMaxException setResponseStatusCode(Integer responseStatusCode) {
        super.setResponseStatusCode(responseStatusCode);
        return this;
    }

    @Override
    public InvalidPriceMaxException setExchangeMessage(String exchangeMessage) {
        super.setExchangeMessage(exchangeMessage);
        return this;
    }

    @Override
    public InvalidPriceMaxException setExchangeErrorCode(String exchangeErrorCode) {
        super.setExchangeErrorCode(exchangeErrorCode);
        return this;
    }

    @Override
    public InvalidPriceMaxException setCurrencyPair(CurrencyPair currencyPair) {
        super.setCurrencyPair(currencyPair);
        return this;
    }

    @Override
    public InvalidPriceMaxException setOrderPrice(BigDecimal orderPrice) {
        super.setOrderPrice(orderPrice);
        return this;
    }

    @Override
    public InvalidPriceMaxException setOrderQuantity(BigDecimal orderQuantity) {
        super.setOrderQuantity(orderQuantity);
        return this;
    }

    @Override
    public InvalidPriceMaxException setBaseCurrency(String baseCurrency) {
        super.setBaseCurrency(baseCurrency);
        return this;
    }

    @Override
    public InvalidPriceMaxException setQuoteCurrency(String quoteCurrency) {
        super.setQuoteCurrency(quoteCurrency);
        return this;
    }
}