package cttk.impl.poloniex.response;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PoloniexMessageResponse {
    String response;
}
