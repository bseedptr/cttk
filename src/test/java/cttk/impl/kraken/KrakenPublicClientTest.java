package cttk.impl.kraken;

import static cttk.test.TestUtils.hasTestProfile;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.junit.Assert;
import org.junit.Test;

import cttk.CXIds;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.AbstractPublicCXClientTest;
import cttk.request.GetTradeHistoryRequest;

public class KrakenPublicClientTest
    extends AbstractPublicCXClientTest
{
    @Override
    public String getCxId() {
        return CXIds.KRAKEN;
    }

    @Override
    protected long getThreadSleepMillis() {
        return 1000l;
    }

    @Test
    @Override
    public void testGetTradeHistory()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        org.junit.Assume.assumeTrue(hasTestProfile("test-kraken-trade-history"));
        threadSleep();

        final ZonedDateTime endDateTime = ZonedDateTime.of(2016, 03, 01, 10, 10, 10, 0, ZoneId.of("UTC"));
        final GetTradeHistoryRequest request = new GetTradeHistoryRequest()
            .setCurrencyPair(getValidCurrencyPair())
            .setLastTradeId(new Long("1456996428774820425"))
            .setEndDateTime(endDateTime);

        final Trades trades = client.getTradeHistory(request);

        logger.debug("{}", trades);

        Assert.assertNotNull(trades);
        Assert.assertEquals(request.getBaseCurrency(), trades.getBaseCurrency());
        Assert.assertEquals(request.getQuoteCurrency(), trades.getQuoteCurrency());
    }
}
