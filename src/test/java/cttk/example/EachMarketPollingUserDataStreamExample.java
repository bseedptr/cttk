package cttk.example;

import static cttk.CXIds.BITHUMB;

import cttk.CXClientFactory;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.UserData;
import cttk.auth.CXCredentialsProvider;
import cttk.impl.stream.eachmarket.CoolTime;
import cttk.impl.stream.eachmarket.RateLimit;
import cttk.request.OpenUserDataStreamRequest;

public class EachMarketPollingUserDataStreamExample {
    public static void main(String[] args)
        throws Exception
    {
        // You can set cxId BITHUMB, COINONE, HUOBI and KORBIT for each market polling version.
        // Also you can customize "RateLimit" and "CoolTime" for each exchange(using chained setter).
        final String cxId = BITHUMB;
        final CXStream<UserData> userDataStream = CXClientFactory
            .createUserDataStreamOpener(cxId, CXCredentialsProvider.getDefaultCredential(cxId).getDataKey())
            .openUserDataStream(new OpenUserDataStreamRequest()
                .setRateLimit(RateLimit.createDefault(cxId))
                .setCoolTime(CoolTime.createDefault(cxId)), CXStreamListener.createPrint());

        Thread.sleep(600000l);
        userDataStream.close();
    }
}
