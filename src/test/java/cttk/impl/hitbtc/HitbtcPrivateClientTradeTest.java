package cttk.impl.hitbtc;

import org.junit.Test;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.impl.AbstractPrivateCXClientTradeTest;
import cttk.impl.util.ExNotRaisedException;

public class HitbtcPrivateClientTradeTest
    extends AbstractPrivateCXClientTradeTest
{
    @Override
    public String getCxId() {
        return CXIds.HITBTC;
    }

    @Override
    @Test(expected = ExNotRaisedException.class)
    public void testInvalidPriceIncrementPrecision()
        throws CTTKException
    {
        // Refer CTTK-496 for detailed explanation
        // Hitbtc automatically truncates the order with invalid precision
        super.testInvalidPriceIncrementPrecision();
    }

    @Override
    @Test(expected = ExNotRaisedException.class)
    public void testInvalidQuantityIncrementPrecision()
        throws CTTKException
    {
        // Refer CTTK-496 for detailed explanation
        // Hitbtc automatically truncates the order with invalid precision
        super.testInvalidQuantityIncrementPrecision();
    }
}
