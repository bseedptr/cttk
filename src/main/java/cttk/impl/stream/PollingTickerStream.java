package cttk.impl.stream;

import cttk.CXStreamConfig;
import cttk.CXStreamListener;
import cttk.PublicCXClient;
import cttk.dto.Ticker;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.util.ThrowableFunction;
import cttk.request.GetTickerRequest;

public class PollingTickerStream
    extends PollingCXStream<Ticker>
{
    public PollingTickerStream(final PublicCXClient client, final CXStreamListener<Ticker> listener, final GetTickerRequest request) {
        super(client, createTickerGetter(request), listener);
    }

    public PollingTickerStream(final PublicCXClient client, final CXStreamConfig config, final CXStreamListener<Ticker> listener, final GetTickerRequest request) {
        super(client, createTickerGetter(request), config, listener);
    }

    private static final ThrowableFunction<PublicCXClient, Ticker> createTickerGetter(final GetTickerRequest request) {
        return new ThrowableFunction<PublicCXClient, Ticker>() {
            @Override
            public Ticker apply(PublicCXClient client)
                throws CTTKException, UnsupportedCurrencyPairException
            {
                return client.getTicker(request);
            }
        };
    }
}
