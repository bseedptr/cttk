package cttk.impl.binance;

import cttk.CXIds;
import cttk.impl.AbstractAPILimitTest;

public class BinanceAPILimitTest
    extends AbstractAPILimitTest
{
    @Override
    public String getCxId() {
        return CXIds.BINANCE;
    }

}
