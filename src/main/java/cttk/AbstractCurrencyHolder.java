package cttk;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
public abstract class AbstractCurrencyHolder<T extends AbstractCurrencyHolder<T>>
    extends AbstractCXIdHolder<T>
    implements CurrencyGettable
{
    protected String currency;

    @SuppressWarnings("unchecked")
    public T setCurrency(String currency) {
        this.currency = currency;
        return (T) this;
    }

    @SuppressWarnings("unchecked")
    public T setCurrency(CurrencyGettable currency) {
        this.currency = currency == null ? null : currency.getCurrency();
        return (T) this;
    }

    @SuppressWarnings("unchecked")
    public T setCurrencyIfNull(String currency) {
        if (this.currency == null || this.currency.isEmpty()) this.currency = currency;
        return (T) this;
    }
}
