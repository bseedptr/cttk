package cttk.impl.huobi.response;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cttk.dto.Balance;
import cttk.dto.Balances;
import cttk.impl.huobi.HuobiUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HuobiBalancesResponse
    extends HuobiResponse
{
    final String TRADE = "trade";
    final String FROZEN = "frozen";

    HuobiUserBalances data;

    public Balances getBalances() {
        return data == null ? null : data.getBalances();
    }
}

@Data
class HuobiUserBalances {
    Long id;
    String type;
    String state;
    List<HuobiUserBalance> list;

    public Balances getBalances() {
        Map<String, Balance> balanceMap = new HashMap<>();

        list.forEach(i -> {
            final String symbol = HuobiUtils.toStdCurrencySymbol(i.getCurrency());
            balanceMap.putIfAbsent(symbol, new Balance()
                .setCurrency(symbol)
                .setInUse(new BigDecimal("0"))
                .setAvailable(new BigDecimal("0")));
            final Balance b = balanceMap.get(symbol);
            switch (i.getType().toUpperCase()) {
                case "TRADE":
                    b.setAvailable(i.getBalance());
                    break;
                case "FROZEN":
                    b.setInUse(i.getBalance());
                    break;
            }
        });

        balanceMap.values().forEach(b -> {
            b.setTotal(b.getAvailable().add(b.getInUse()));
        });

        return new Balances()
            .setBalances(new ArrayList<>(balanceMap.values()));
    }
}

@Data
class HuobiUserBalance {
    String currency;
    String type;
    BigDecimal balance;
}
