package cttk.impl.bibox.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.dto.UserAccount;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BiboxUserInfoResponse
    extends BiboxResultWrapperResponse<BiboxUserInfo>
{
    public UserAccount toUserAccount() {
        return unwrap().toUserAccount();
    }
}

@Data
class BiboxUserInfo {
    @JsonProperty("user_id")
    String userId;
    String email;
    String phone;
    @JsonProperty("ip_province")
    String ipProvince;
    @JsonProperty("login_ip_address")
    String loginIPaddress;
    boolean isLock;
    boolean isActive;
    String createdAt;

    UserAccount toUserAccount() {
        return new UserAccount()
            .setAid(userId)
            .setEmail(email)
            .setPhone(phone)
            .setCanTrade(isActive)
            .setCanWithdraw(isActive)
            .setCanDeposit(isActive);
    }

    @JsonProperty("is_lock")
    public void setLock(int code) {
        this.isLock = code == 1;
    }

    @JsonProperty("is_active")
    public void setActive(int code) {
        this.isActive = code == 1;
    }
}
