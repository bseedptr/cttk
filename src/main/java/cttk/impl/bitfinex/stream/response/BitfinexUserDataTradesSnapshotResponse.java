package cttk.impl.bitfinex.stream.response;

import static cttk.impl.bitfinex.BitfinexUtils.toStdCurrencySymbol;
import static cttk.util.StringUtils.parseBigDecimal;
import static cttk.util.StringUtils.parseLong;

import java.math.BigDecimal;
import java.util.ArrayList;

import cttk.CXIds;
import cttk.OrderSide;
import cttk.dto.UserTrade;
import cttk.impl.bitfinex.BitfinexUtils;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;

public class BitfinexUserDataTradesSnapshotResponse
    extends ArrayList<String>
{
    public UserTrade toUserTrade() {
        UserTrade ut = new UserTrade();

        if (get(0) != null) {
            final Long id = parseLong(get(0));
            final String pair = get(1);
            final Long mtsCreate = parseLong(get(2));
            final Long oId = parseLong(get(3));
            final BigDecimal amount = parseBigDecimal(get(4));
            final BigDecimal price = parseBigDecimal(get(5));
            final BigDecimal total = price.multiply(amount.abs());
            final Long isMaker = parseLong(get(8));
            final BigDecimal fee = MathUtils.abs(parseBigDecimal(get(9)));
            final String feeCurrency = toStdCurrencySymbol(get(10));

            ut.setCxId(CXIds.BITFINEX);
            ut.setCurrencyPair(BitfinexUtils.parseMarketSymbolWithT(pair));
            ut.setTid(String.valueOf(id));
            ut.setOid(String.valueOf(oId));
            ut.setDateTime(DateTimeUtils.zdtFromEpochMilli(mtsCreate));
            ut.setSide(amount.compareTo(BigDecimal.ZERO) > 0 ? OrderSide.BUY : OrderSide.SELL);
            ut.setQuantity(amount.abs());
            ut.setPrice(price);
            ut.setTotal(total);
            ut.setIsMaker(isMaker == 1 ? true : false);
            ut.setFee(fee);
            ut.setFeeCurrency(feeCurrency);
        }

        return ut;
    }
}