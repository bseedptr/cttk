package cttk.impl.huobi.stream;

import cttk.CXStreamListener;
import cttk.impl.stream.AbstractWebsocketStream;
import cttk.impl.stream.MessageConverter;
import cttk.request.AbstractCXStreamRequest;
import okhttp3.WebSocketListener;

public class HuobiWebsocketStream<T, R extends AbstractCXStreamRequest<R>>
    extends AbstractWebsocketStream<T, R>
{
    private static final String WSS_API_HUOBI_PRO_WS = "wss://api.huobi.pro/ws";

    public HuobiWebsocketStream(
        final String channel,
        final R request,
        final MessageConverter<T> converter,
        final CXStreamListener<T> listener)
    {
        super(channel, request, converter, listener);
    }

    @Override
    protected String getUrl() {
        return WSS_API_HUOBI_PRO_WS;
    }

    @Override
    protected WebSocketListener createWebSocketListener() {
        return new HuobiWebsocketListener<>(this, channel, request, converter, listener);
    }
}
