package cttk.impl.binance.response;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.dto.UserTrade;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;

@Data
public class BinanceUserTradeResponse {
    Long id;
    Long orderId;
    Long time;
    BigDecimal price;
    @JsonProperty("qty")
    BigDecimal quantity;
    BigDecimal commission;
    String commissionAsset;
    Boolean isBuyer;
    Boolean isMaker;
    Boolean isBestMatch;

    public UserTrade getUserTrade(CurrencyPair currencyPair) {
        return new UserTrade()
            .setCurrencyPair(currencyPair)
            .setTid(String.valueOf(id))
            .setOid(String.valueOf(orderId))
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(time))
            .setSide(isBuyer ? OrderSide.BUY : OrderSide.SELL)
            .setPrice(price)
            .setQuantity(quantity)
            .setTotal(MathUtils.multiply(price, quantity))
            .setFee(commission)
            .setFeeCurrency(commissionAsset)
            .setIsMaker(isMaker)
            .setIsBestMatch(isBestMatch);
    }
}
