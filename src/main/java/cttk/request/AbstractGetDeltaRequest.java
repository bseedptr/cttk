package cttk.request;

import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cttk.AbstractCurrencyPair;
import cttk.DurationGettable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public abstract class AbstractGetDeltaRequest<T extends AbstractGetDeltaRequest<T>>
    extends AbstractCurrencyPair<T>
    implements DurationGettable
{
    private String fromId;
    private ZonedDateTime startDateTime;
    private ZonedDateTime endDateTime;
    private Long sleepTimeMillis;

    @SuppressWarnings("unchecked")
    public T setFromId(String fromId) {
        this.fromId = fromId;
        return (T) this;
    }

    @SuppressWarnings("unchecked")
    public T setStartDateTime(ZonedDateTime startDateTime) {
        this.startDateTime = startDateTime;
        return (T) this;
    }

    @SuppressWarnings("unchecked")
    public T setEndDateTime(ZonedDateTime endDateTime) {
        this.endDateTime = endDateTime;
        return (T) this;
    }

    @SuppressWarnings("unchecked")
    public T setSleepTimeMillis(Long sleepTimeMillis) {
        this.sleepTimeMillis = sleepTimeMillis;
        return (T) this;
    }

    @JsonIgnore
    public boolean hasFromId() {
        return fromId != null;
    }
}
