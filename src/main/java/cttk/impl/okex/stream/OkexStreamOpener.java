package cttk.impl.okex.stream;

import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.CXStreamOpener;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.request.OpenOrderBookStreamRequest;
import cttk.request.OpenTickerStreamRequest;
import cttk.request.OpenTradeStreamRequest;

public class OkexStreamOpener
    implements CXStreamOpener
{
    @Override
    public String getCxId() {
        return CXIds.OKEX;
    }

    @Override
    public CXStream<Ticker> openTickerStream(final OpenTickerStreamRequest request, CXStreamListener<Ticker> listener) {
        return new OkexWebsocketStream<Ticker, OpenTickerStreamRequest>(
            OkexChannels.getTickerChannel(request),
            request,
            OkexWebsocketMessageConverterFactory.createTickerMessageConverter(),
            listener);
    }

    @Override
    public CXStream<OrderBook> openOrderBookStream(final OpenOrderBookStreamRequest request, CXStreamListener<OrderBook> listener) {
        return new OkexWebsocketStream<OrderBook, OpenOrderBookStreamRequest>(
            OkexChannels.getOrderBookChannel(request),
            request,
            OkexWebsocketMessageConverterFactory.createOrderBookMessageConverter(),
            listener);
    }

    @Override
    public CXStream<Trades> openTradeStream(final OpenTradeStreamRequest request, CXStreamListener<Trades> listener)
        throws CTTKException
    {
        return new OkexWebsocketStream<Trades, OpenTradeStreamRequest>(
            OkexChannels.getTradeChannel(request),
            request,
            OkexWebsocketMessageConverterFactory.createTradesMessageConverter(),
            listener);
    }
}
