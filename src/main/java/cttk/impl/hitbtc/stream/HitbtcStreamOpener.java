package cttk.impl.hitbtc.stream;

import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.CXStreamOpener;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.request.OpenOrderBookStreamRequest;
import cttk.request.OpenTickerStreamRequest;
import cttk.request.OpenTradeStreamRequest;

public class HitbtcStreamOpener
    implements CXStreamOpener
{
    @Override
    public String getCxId() {
        return CXIds.HITBTC;
    }

    @Override
    public CXStream<Ticker> openTickerStream(final OpenTickerStreamRequest request, CXStreamListener<Ticker> listener) {
        return new HitbtcWebsocketStream<Ticker, OpenTickerStreamRequest>(
            HitbtcChannels.getTickerChannel(),
            request,
            HitbtcWebsocketMessageConverterFactory.createTickerMessageConverter(request),
            listener);
    }

    @Override
    public CXStream<OrderBook> openOrderBookStream(final OpenOrderBookStreamRequest request, CXStreamListener<OrderBook> listener) {
        return new HitbtcWebsocketStream<OrderBook, OpenOrderBookStreamRequest>(
            HitbtcChannels.getOrderBookChannel(),
            request,
            HitbtcWebsocketMessageConverterFactory.createOrderBookMessageConverter(request),
            listener);
    }

    @Override
    public CXStream<Trades> openTradeStream(final OpenTradeStreamRequest request, CXStreamListener<Trades> listener)
        throws CTTKException
    {
        return new HitbtcWebsocketStream<Trades, OpenTradeStreamRequest>(
            HitbtcChannels.getTradeChannel(),
            request,
            HitbtcWebsocketMessageConverterFactory.createTradesMessageConverter(request),
            listener);
    }
}
