package cttk.impl.okex;

import cttk.CXClientFactory;
import cttk.CXStreamOpener;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.UserDataStreamOpener;
import cttk.auth.Credential;
import cttk.impl.okex.stream.OkexStreamOpener;
import cttk.impl.okex.stream.OkexUserDataStreamOpener;

public class OkexClientFactory
    implements CXClientFactory
{
    @Override
    public PublicCXClient create() {
        return new OkexPublicClient();
    }

    @Override
    public PrivateCXClient create(Credential credential) {
        return new OkexPrivateClient(credential);
    }

    @Override
    public CXStreamOpener createCXStreamOpener() {
        return new OkexStreamOpener();
    }

    @Override
    public UserDataStreamOpener createUserDataStreamOpener(Credential credential) {
        return new OkexUserDataStreamOpener(credential);
    }
}
