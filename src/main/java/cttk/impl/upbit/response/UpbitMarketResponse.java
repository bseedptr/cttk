package cttk.impl.upbit.response;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.CurrencyPair;
import cttk.dto.MarketInfo;
import cttk.impl.upbit.UpbitUtils;
import lombok.Data;

@Data
public class UpbitMarketResponse {
    String market;
    @JsonProperty("korean-name")
    String koreanName;
    @JsonProperty("english-name")
    String englishName;

    public MarketInfo toMarketInfo() {
        final CurrencyPair currencyPair = UpbitUtils.parseMarketSymbol(market);
        return new MarketInfo().setCurrencyPair(currencyPair)
            .setPriceIncrement(getPriceIncrement(currencyPair.getQuoteCurrency()))
            .setMinTotal(getMinTotal(currencyPair.getQuoteCurrency()));
    }

    private BigDecimal getPriceIncrement(String quoteCurrency) {
        switch (quoteCurrency) {
            case "BTC":
                return new BigDecimal("0.00000001");
            case "ETH":
                return new BigDecimal("0.00000001");
            case "USDT":
                return new BigDecimal("0.001");
            case "KRW":
                // depends on current market price
            default:
                return null;
        }
    }

    private BigDecimal getMinTotal(String quoteCurrency) {
        switch (quoteCurrency) {
            case "KRW":
                return new BigDecimal("500");
            case "BTC":
                return new BigDecimal("0.0005");
            case "ETH":
                return new BigDecimal("0.0005");
            case "USDT":
                return new BigDecimal("0.0005");
            default:
                return null;
        }
    }

}