package cttk.impl.huobi.stream;

import cttk.CurrencyPair;
import cttk.impl.huobi.HuobiUtils;

public class HuobiChannels {
    public static final String getTickerChannel(final CurrencyPair currencyPair) {
        return "market." + HuobiUtils.getMarketSymbol(currencyPair) + ".detail";
    }

    public static final String getOrderBookChannel(final CurrencyPair currencyPair) {
        return "market." + HuobiUtils.getMarketSymbol(currencyPair) + ".depth.step0";
    }

    public static final String getTradesChannel(final CurrencyPair currencyPair) {
        return "market." + HuobiUtils.getMarketSymbol(currencyPair) + ".trade.detail";
    }
}
