package cttk.impl.poloniex;

import static cttk.impl.poloniex.PoloniexUtils.getMarketSymbol;
import static cttk.impl.util.Objects.nvl;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import cttk.PublicCXClient;
import cttk.dto.MarketInfo;
import cttk.dto.MarketInfos;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Tickers;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.exception.CXServerException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.poloniex.response.PoloniexOrderBookResponse;
import cttk.impl.poloniex.response.PoloniexTickersResponse;
import cttk.impl.poloniex.response.PoloniexTradesResponse;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ParamsBuilder;
import cttk.request.GetOrderBookRequest;
import cttk.request.GetRecentTradesRequest;
import cttk.request.GetTickerRequest;
import cttk.request.GetTradeHistoryRequest;
import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;

public class PoloniexPublicClient
    extends AbstractPoloniexClient
    implements PublicCXClient
{
    protected static final String BASE_URL = "https://poloniex.com/public";

    @Override
    public MarketInfos getMarketInfos()
        throws CTTKException
    {
        final HttpResponse response = requestGet("returnTicker");

        checkResponseError(null, response);

        final MarketInfos marketInfos = convert(response, PoloniexTickersResponse.class, i -> i.toMarketInfos());
        final List<MarketInfo> list = marketInfos.getMarketInfos();
        if (list != null) {
            for (MarketInfo info : list) {
                info.setPricePrecision(8)
                    .setQuantityPrecision(8)
                    .setMinQuantity(new BigDecimal("0.0001"))
                    .setMinTotal(new BigDecimal("0.0001"));
            }
        }
        return marketInfos;
    }

    @Override
    public Ticker getTicker(GetTickerRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet("returnTicker");

        checkResponseError(request, response);

        Ticker ticker = null;
        try {
            ticker = convert(response, PoloniexTickersResponse.class, (i -> i.toTicker(request)));
        } catch (UnsupportedCurrencyPairException e) {
            e.setCxId(this.getCxId());
            e.setBaseCurrency(request.getBaseCurrency());
            e.setQuoteCurrency(request.getQuoteCurrency());
            e.setExchangeMessage(response.getBody());
            e.setResponseStatusCode(response.getStatus());
            throw e;
        }
        return ticker;
    }

    @Override
    public Tickers getAllTickers()
        throws CTTKException
    {
        final HttpResponse response = requestGet("returnTicker");

        checkResponseError(null, response);

        return convert(response, PoloniexTickersResponse.class, (i -> i.toTickers()));
    }

    @Override
    public OrderBook getOrderBook(GetOrderBookRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet("returnOrderBook",
            ParamsBuilder.create()
                .putIfNotNull("currencyPair", getMarketSymbol(request))
                .putIfNotNull("depth", nvl(request.getCount(), 100))
                .build());

        checkResponseError(request, response);
        OrderBook orderbook = null;
        try {
            orderbook = convert(response, PoloniexOrderBookResponse.class, (i -> i.toOrderBook(request)));
        } catch (UnsupportedCurrencyPairException e) {
            e.setCxId(this.getCxId());
            e.setBaseCurrency(request.getBaseCurrency());
            e.setQuoteCurrency(request.getQuoteCurrency());
            e.setExchangeMessage(response.getBody());
            e.setResponseStatusCode(response.getStatus());
            throw e;
        }
        return orderbook;
    }

    @Override
    public Trades getRecentTrades(GetRecentTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet("returnTradeHistory",
            ParamsBuilder.create()
                .putIfNotNull("currencyPair", getMarketSymbol(request))
                .build());

        checkResponseError(request, response);

        return convert(response, PoloniexTradesResponse.class, (i -> i.toTrades(request)));
    }

    @Override
    public Trades getTradeHistory(GetTradeHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet("returnTradeHistory",
            ParamsBuilder.create()
                .putIfNotNull("currencyPair", getMarketSymbol(request))
                .putIfNotNull("start", request.getStartEpochInSecond())
                .putIfNotNull("end", request.getEndEpochInSecond())
                .build());

        checkResponseError(request, response);

        return convert(response, PoloniexTradesResponse.class, (i -> i.toTrades(request)));
    }

    protected HttpResponse requestGet(String command)
        throws CTTKException
    {
        return requestGet(command, null);
    }

    protected HttpResponse requestGet(final String command, final Map<String, String> params)
        throws CTTKException
    {
        final Request request = new Request.Builder()
            .url(getHostUrl(command, params))
            .build();

        debug(request);

        try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
            return toHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    protected static String getHostUrl(final String command, final Map<String, String> params) {
        final HttpUrl.Builder builder = HttpUrl.parse(BASE_URL).newBuilder()
            .addQueryParameter("command", command);

        if (params != null && !params.isEmpty()) {
            params.forEach((k, v) -> builder.addQueryParameter(k, v));
        }
        return builder.build().toString();
    }
}
