package cttk.test;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class OrderbookPrices {
    private BigDecimal buyPrice;
    private BigDecimal sellPrice;
}
