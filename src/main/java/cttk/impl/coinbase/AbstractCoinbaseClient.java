package cttk.impl.coinbase;

import java.io.IOException;
import java.util.Map;

import cttk.CXIds;
import cttk.exception.APILimitExceedException;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXServerException;
import cttk.impl.AbstractCXClient;
import cttk.impl.coinbase.response.CoinbaseErrorResponse;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ThrowableFunction;
import okhttp3.Request;
import okhttp3.Response;

abstract class AbstractCoinbaseClient
    extends AbstractCXClient
{
    protected static final String BASE_URL = "https://api.gdax.com";

    public AbstractCoinbaseClient() {
        super();
    }

    @Override
    public String getCxId() {
        return CXIds.COINBASE;
    }

    protected HttpResponse requestGet(String path)
        throws CTTKException
    {
        return requestGet(path, null);
    }

    protected HttpResponse requestGet(String path, Map<String, String> params)
        throws CTTKException
    {
        final Request request = new Request.Builder()
            .url(getHostUrl(path, params))
            .build();

        debug(request);

        try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
            final HttpResponse response = new HttpResponse()
                .setStatus(httpResponse.code())
                .setBody(httpResponse.body().string())
                .setHeaders(httpResponse.headers().toMultimap());

            debug(response);

            return response;
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    protected String getHostUrl(String endpoint) {
        return getHostUrl(BASE_URL, endpoint);
    }

    protected String getHostUrl(String endpoint, Map<String, String> params) {
        return getHostUrl(BASE_URL, endpoint, params);
    }

    @Override
    protected void checkErrorByResponseMessage(Object requestObj, HttpResponse response)
        throws CTTKException
    {
        if (response.getBody().contains("Rate limit exceeded")) {
            throw new APILimitExceedException()
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        } else if (response.getBody().contains("NotFound")) {
            throw CXAPIRequestException.createByRequestObj(requestObj)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    protected <T, R> R convert(HttpResponse response, Class<T> claz, ThrowableFunction<T, R> mapper)
        throws CTTKException
    {
        if (response.is2xxSuccessful()) {
            try {
                T obj = objectMapper.readValue(response.getBody(), claz);
                return mapper.apply(obj);
            } catch (IOException e) {
                throw new CXAPIRequestException("json parse error ", e)
                    .setCxId(this.getCxId())
                    .setExchangeMessage(response.getBody())
                    .setResponseStatusCode(response.getStatus());
            }
        } else {
            try {
                final CoinbaseErrorResponse obj = objectMapper.readValue(response.getBody(), CoinbaseErrorResponse.class);
                throw new CXAPIRequestException("Coinone response error " + obj)
                    .setCxId(this.getCxId())
                    .setExchangeMessage(response.getBody())
                    .setResponseStatusCode(response.getStatus());
            } catch (IOException e) {
                throw new CXAPIRequestException("json parse error ", e)
                    .setCxId(this.getCxId())
                    .setExchangeMessage(response.getBody())
                    .setResponseStatusCode(response.getStatus());
            }
        }
    }
}
