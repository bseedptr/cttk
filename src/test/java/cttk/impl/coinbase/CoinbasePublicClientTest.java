package cttk.impl.coinbase;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import cttk.CXIds;
import cttk.dto.Ticker;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.AbstractPublicCXClientTest;
import cttk.request.GetTickerRequest;

public class CoinbasePublicClientTest
    extends AbstractPublicCXClientTest
{
    @Override
    public String getCxId() {
        return CXIds.COINBASE;
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetAllTickers()
        throws CTTKException
    {
        super.testGetAllTickers();
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetTradeHistory()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetTradeHistory();
    }

    @Test
    @Override
    public void testGetTicker()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();

        final GetTickerRequest request = new GetTickerRequest().setCurrencyPair(getValidCurrencyPair());
        final Ticker ticker = client.getTicker(request);

        logger.debug("{}", ticker);

        assertNotNull(ticker);
        assertNotNull(ticker.getCxId());
        assertEquals(request.getBaseCurrency(), ticker.getBaseCurrency());
        assertNotNull(ticker.getDateTime());
        assertNotNull(ticker.getLastPrice());
        assertNotNull(ticker.getLowPrice());
        assertNotNull(ticker.getHighPrice());
        // For Coinbase, getVolume is null however, quoteVolume value is filled
        assertNotNull(ticker.getQuoteVolume());

    }

}
