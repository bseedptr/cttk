package cttk.impl.okex.stream.response;

import static java.util.stream.Collectors.toList;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import cttk.OrderSide;
import cttk.dto.Trade;
import cttk.dto.Trades;
import cttk.impl.okex.OkexUtilsV3;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class OkexWebsocketTradesResponse
    extends OkexWebsocketResponse<OkexWebsocketTrade>
{
    public Trades toTrades() {
        return new Trades()
            .setCurrencyPair(OkexUtilsV3.toStdMarketSymbolV3(unwrap().instrument_id))
            .setTrades(toTradeList());
    }

    List<Trade> toTradeList() {
        return data.stream().map(OkexWebsocketTrade::toTrade).collect(toList());
    }
}

@Data
class OkexWebsocketTrade {
    String instrument_id;
    BigDecimal price;
    String side;
    BigDecimal size;
    String timestamp;
    String trade_id;

    Trade toTrade() {
        return new Trade()
            .setSno(new BigInteger(trade_id))
            .setTid(trade_id)
            .setSide(OrderSide.of(side))
            .setPrice(price)
            .setQuantity(size)
            .setDateTime(DateTimeUtils.parseZdt(timestamp))
            .setTotal(MathUtils.multiply(price, size));
    }
}
