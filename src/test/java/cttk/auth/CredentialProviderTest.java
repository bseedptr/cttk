package cttk.auth;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import cttk.CXIds;
import cttk.exception.UnsupportedCXException;

public class CredentialProviderTest {
    @Test
    public void test()
        throws IOException, UnsupportedCXException
    {
        CXCredentials credentials = new CXCredentialsProvider("src/test/resources/credentials").get(CXIds.BITHUMB);
        Assert.assertNotNull(credentials);
        Assert.assertNotNull(credentials.getAccessKey());
        Assert.assertNotNull(credentials.getSecretKey());
    }
}
