package cttk.impl.binance.stream.response;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.dto.OrderBook;
import cttk.dto.OrderBook.SimpleOrder;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;

@Data
public class BinanceWebsocketOrderBookResponse {
    @JsonProperty("E")
    Long eventTime;
    @JsonProperty("U")
    Long firstUpdateId;
    @JsonProperty("u")
    Long lastUpdateId;
    @JsonProperty("e")
    String eventType;
    @JsonProperty("s")
    String symbol;
    @JsonProperty("b")
    List<List<Object>> bids;
    @JsonProperty("a")
    List<List<Object>> asks;

    public OrderBook toOrderBook() {
        try {
            return new OrderBook()
                .setBaseCurrency(symbol.substring(0, 3))
                .setQuoteCurrency(symbol.substring(3))
                .setFull(false)
                .setFirstUpdateId(firstUpdateId)
                .setLastUpdateId(lastUpdateId)
                .setDateTime(DateTimeUtils.zdtFromEpochMilli(eventTime))
                .setBids(bids == null || bids.isEmpty() ? new ArrayList<>() : bids.stream().map(b -> SimpleOrder.of(String.valueOf(b.get(0)), String.valueOf(b.get(1)))).collect(Collectors.toList()))
                .setAsks(asks == null || asks.isEmpty() ? new ArrayList<>() : asks.stream().map(a -> SimpleOrder.of(String.valueOf(a.get(0)), String.valueOf(a.get(1)))).collect(Collectors.toList()));
        } catch (Exception e) {
            return new OrderBook();
        }
    }
}
