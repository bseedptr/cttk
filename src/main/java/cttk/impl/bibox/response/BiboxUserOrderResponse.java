package cttk.impl.bibox.response;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.PricingType;
import cttk.dto.UserOrder;
import cttk.impl.bibox.BiboxUtils;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BiboxUserOrderResponse
    extends BiboxResultWrapperResponse<BiboxUserOrder>
{
    public UserOrder toUserOrder() {
        return unwrap().toUserOrder();
    }
}

@Data
class BiboxUserOrder {
    String id;
    Long createdAt;
    String accountType;
    String pair;
    @JsonProperty("coin_symbol")
    String coinSymbol;
    @JsonProperty("currency_symbol")
    String currencySymbol;
    OrderSide orderSide;
    String orderType;
    BigDecimal price;
    BigDecimal amount;
    BigDecimal money;
    @JsonProperty("deal_amount")
    BigDecimal dealAmount;
    @JsonProperty("deal_percent")
    String dealPercent;
    String unexecuted;
    String status;
    OrderStatus orderStatus;

    UserOrder toUserOrder() {
        if (id == null)
            return null;

        PricingType pricingType = null;
        if (orderType != null) {
            pricingType = PricingType.of(orderType.toUpperCase());
        }

        return new UserOrder()
            .setCurrencyPair(coinSymbol, currencySymbol)
            .setOid(id)
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(createdAt))
            .setSide(orderSide)
            .setStatus(status)
            .setStatusType(orderStatus)
            .setPrice(price)
            .setPricingType(pricingType)
            .setQuantity(amount)
            .setFilledQuantity(dealAmount)
            .setRemainingQuantity(MathUtils.subtract(amount, dealAmount))
            .setTotal(MathUtils.multiply(price, amount));
    }

    @JsonProperty("account_type")
    public void setAccountType(Integer type) {
        this.accountType = BiboxUtils.parseAccountType(type);
    }

    @JsonProperty("order_side")
    public void setOrderSide(String side) {
        this.orderSide = BiboxUtils.parseOrderSide(side);
    }

    @JsonProperty("order_type")
    public void setOrderType(Integer type) {
        this.orderType = BiboxUtils.parseOrderType(type);
    }

    @JsonProperty("status")
    public void setStatus(Integer code) {
        this.status = BiboxUtils.parseOrderStatus(code);
        this.orderStatus = BiboxUtils.parseOrderStatusType(status);
    }
}