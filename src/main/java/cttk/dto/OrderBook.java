package cttk.dto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZonedDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cttk.AbstractCurrencyPair;
import cttk.DateTimeHolder;
import cttk.PricedQuantity;
import cttk.impl.util.Objects;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(chain = true)
public class OrderBook
    extends AbstractCurrencyPair<OrderBook>
    implements DateTimeHolder<OrderBook>
{
    boolean isFull = true;
    boolean isMerged = true;
    Long firstUpdateId;
    Long lastUpdateId;
    ZonedDateTime dateTime;
    List<SimpleOrder> asks;
    List<SimpleOrder> bids;

    @JsonIgnore
    public Optional<BigDecimal> getLowestAsk() {
        if (asks == null || asks.isEmpty()) return Optional.empty();
        return asks.stream().map(i -> i.getPrice()).min((a, b) -> a.compareTo(b));
    }

    @JsonIgnore
    public Optional<BigDecimal> getHighestBid() {
        if (bids == null || bids.isEmpty()) return Optional.empty();
        return bids.stream().map(i -> i.getPrice()).max((a, b) -> a.compareTo(b));
    }

    public OrderBook group(final int scale) {
        return new OrderBook()
            .setCxId(cxId)
            .setCurrencyPair(this)
            .setFull(isFull)
            .setFirstUpdateId(firstUpdateId)
            .setLastUpdateId(lastUpdateId)
            .setDateTime(dateTime)
            .setAsks(group(asks, scale))
            .setBids(group(bids, scale));
    }

    private static List<SimpleOrder> group(List<SimpleOrder> list, final int scale) {
        if (list == null) return null;
        return list.stream()
            .collect(Collectors.groupingBy(i -> i.getPrice().setScale(scale, RoundingMode.HALF_UP)))
            .entrySet().parallelStream()
            .map(e -> new SimpleOrder(e.getKey(), e.getValue().parallelStream().map(a -> a.quantity).reduce((a, b) -> a.add(b)).get()))
            .sorted((a, b) -> a.price.compareTo(b.price))
            .collect(Collectors.toList());
    }

    public OrderBook merge() {
        return new OrderBook()
            .setCxId(cxId)
            .setCurrencyPair(this)
            .setFull(isFull)
            .setFirstUpdateId(firstUpdateId)
            .setLastUpdateId(lastUpdateId)
            .setDateTime(dateTime)
            .setAsks(merge(asks))
            .setBids(merge(bids));
    }

    private static List<SimpleOrder> merge(List<SimpleOrder> list) {
        if (list == null) return null;
        return list.stream()
            .collect(Collectors.groupingBy(i -> i.getPrice()))
            .entrySet().parallelStream()
            .map(e -> new SimpleOrder(e.getKey(), e.getValue().parallelStream().map(a -> a.quantity).reduce((a, b) -> a.add(b)).get()))
            .sorted((a, b) -> a.price.compareTo(b.price))
            .collect(Collectors.toList());
    }

    @Data
    @Accessors(chain = true)
    public static class SimpleOrder
        implements PricedQuantity
    {
        BigDecimal price;
        BigDecimal quantity;
        BigDecimal count;

        public SimpleOrder() {
        }

        public SimpleOrder(BigDecimal price, BigDecimal quantity) {
            this.price = price;
            this.quantity = quantity;
        }

        public SimpleOrder(BigDecimal price, BigDecimal quantity, BigDecimal count) {
            this.price = price;
            this.quantity = count.compareTo(BigDecimal.ZERO) == 0 ? new BigDecimal("0") : quantity;
            this.count = count;
        }

        public SimpleOrder(String price, String quantity) {
            this.price = new BigDecimal(price);
            this.quantity = new BigDecimal(quantity);
        }

        public static SimpleOrder of(BigDecimal price, BigDecimal quantity) {
            return new SimpleOrder(price, quantity);
        }

        public static SimpleOrder of(String price, String quantity) {
            return new SimpleOrder(price, quantity);
        }

        public static List<SimpleOrder> sortByPriceAsc(List<SimpleOrder> orderList) {
            if (orderList != null) orderList.sort(PRICE_ASC_COMPARATOR);
            return orderList;
        }

        public static List<SimpleOrder> sortByPriceDesc(List<SimpleOrder> orderList) {
            if (orderList != null) orderList.sort(PRICE_DESC_COMPARATOR);
            return orderList;
        }

        public static final Comparator<SimpleOrder> PRICE_ASC_COMPARATOR = new Comparator<SimpleOrder>() {
            @Override
            public int compare(SimpleOrder o1, SimpleOrder o2) {
                final BigDecimal p1 = Objects.nvl(o1.getPrice(), BigDecimal.ZERO);
                final BigDecimal p2 = Objects.nvl(o2.getPrice(), BigDecimal.ZERO);
                return p1.compareTo(p2);
            }
        };

        public static final Comparator<SimpleOrder> PRICE_DESC_COMPARATOR = new Comparator<SimpleOrder>() {
            @Override
            public int compare(SimpleOrder o1, SimpleOrder o2) {
                return PRICE_ASC_COMPARATOR.compare(o2, o1);
            }
        };
    }

}
