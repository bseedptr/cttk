package cttk.impl.hitbtc;

import cttk.CXClientFactory;
import cttk.CXStreamOpener;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.UserDataStreamOpener;
import cttk.auth.Credential;
import cttk.impl.hitbtc.stream.HitbtcStreamOpener;
import cttk.impl.hitbtc.stream.HitbtcUserDataStreamOpener;

public class HitbtcClientFactory
    implements CXClientFactory
{
    @Override
    public PublicCXClient create() {
        return new HitbtcPublicClient();
    }

    @Override
    public PrivateCXClient create(Credential credential) {
        return new HitbtcPrivateClient(credential);
    }

    @Override
    public CXStreamOpener createCXStreamOpener() {
        return new HitbtcStreamOpener();
    }

    @Override
    public UserDataStreamOpener createUserDataStreamOpener(Credential credential) {
        return new HitbtcUserDataStreamOpener(credential);
    }
}
