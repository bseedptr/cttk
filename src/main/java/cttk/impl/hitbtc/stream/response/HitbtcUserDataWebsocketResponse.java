package cttk.impl.hitbtc.stream.response;

import lombok.Data;

@Data
public class HitbtcUserDataWebsocketResponse {
    String jsonrpc;
    String method;
}
