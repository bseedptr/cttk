package cttk.impl.bitfinex.stream;

import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.CXStreamOpener;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.request.OpenOrderBookStreamRequest;
import cttk.request.OpenTickerStreamRequest;
import cttk.request.OpenTradeStreamRequest;

public class BitfinexStreamOpener
    implements CXStreamOpener
{
    @Override
    public String getCxId() {
        return CXIds.BITFINEX;
    }

    @Override
    public CXStream<Ticker> openTickerStream(final OpenTickerStreamRequest request, CXStreamListener<Ticker> listener) {
        return new BitfinexWebsocketStream<Ticker, OpenTickerStreamRequest>(
            BitfinexChannels.getTickerChannel(),
            request,
            BitfinexWebsocketMessageConverterFactory.createTickerMessageConverter(request),
            listener);
    }

    @Override
    public CXStream<OrderBook> openOrderBookStream(final OpenOrderBookStreamRequest request, CXStreamListener<OrderBook> listener) {
        return new BitfinexWebsocketStream<OrderBook, OpenOrderBookStreamRequest>(
            BitfinexChannels.getOrderBookChannel(),
            request,
            BitfinexWebsocketMessageConverterFactory.createOrderBookMessageConverter(request),
            listener);
    }

    @Override
    public CXStream<Trades> openTradeStream(final OpenTradeStreamRequest request, CXStreamListener<Trades> listener)
        throws CTTKException
    {
        return new BitfinexWebsocketStream<Trades, OpenTradeStreamRequest>(
            BitfinexChannels.getTradeChannel(),
            request,
            BitfinexWebsocketMessageConverterFactory.createTradesMessageConverter(request),
            listener);
    }
}
