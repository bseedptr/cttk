package cttk.impl.okex.stream.response;

import static java.util.stream.Collectors.toList;

import java.math.BigDecimal;
import java.util.List;

import cttk.dto.OrderBook;
import cttk.dto.OrderBook.SimpleOrder;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class OkexWebsocketOrderBookResponse
    extends OkexWebsocketResponse<OkexWebsocketOrderBook>
{
    String action;

    public OrderBook toOrderBook() {
        return new OrderBook()
            .setDateTime(DateTimeUtils.parseZdt(unwrap().timestamp))
            .setAsks(unwrap().toAsks())
            .setBids(unwrap().toBids());
    }
}

@Data
class OkexWebsocketOrderBook {
    String instrument_id;
    String timestamp;
    String checksum;
    List<BigDecimal[]> asks;
    List<BigDecimal[]> bids;

    List<SimpleOrder> toAsks() {
        return asks.stream().map(ask -> SimpleOrder.of(ask[0], ask[1])).collect(toList());
    }

    List<SimpleOrder> toBids() {
        return bids.stream().map(bid -> SimpleOrder.of(bid[0], bid[1])).collect(toList());
    }
}