package cttk.impl.bithumb;

import static cttk.dto.UserOrders.Category.OPEN;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import cttk.CXIds;
import cttk.CurrencyPair;
import cttk.OrderStatus;
import cttk.dto.BankAccount;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.AbstractPrivateCXClientDataTest;
import cttk.request.GetBankAccountRequest;
import cttk.request.GetUserOrderHistoryRequest;
import cttk.request.GetUserOrderRequest;
import cttk.request.GetUserTradesRequest;

public class BithumbPrivateClientDataTest
    extends AbstractPrivateCXClientDataTest
{
    @AfterClass
    public static void cleanup() {
        cleanup(BithumbPrivateClientDataTest.class);
    }

    @Override
    public String getCxId() {
        return CXIds.BITHUMB;
    }

    @Test
    @Override
    public void testGetBankAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final BankAccount bankAccount = client.getBankAccount(new GetBankAccountRequest());

        logger.debug("{}", bankAccount);

        Assert.assertNotNull(bankAccount);
        Assert.assertNotNull(bankAccount.getBankName());
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetAllWallets()
        throws CTTKException
    {
        super.testGetAllWallets();
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetAllOpenUserOrders()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetAllOpenUserOrders();
    }

    // CTTK-614
    @Test(expected = UnsupportedMethodException.class)
    public void testGetUserOrderHistory()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetUserOrderHistory();
    }

    // CTTK-614
    @Test(expected = UnsupportedMethodException.class)
    public void testGetUserOrderHistoryWrongCurrency()
        throws CTTKException
    {
        super.testGetUserOrderHistoryWrongCurrency();
    }

    // CTTK-614
    @Test(expected = UnsupportedMethodException.class)
    public void testGetUserOrderHistoryCategory()
        throws CTTKException
    {
        threadSleep();

        final GetUserOrderHistoryRequest request = new GetUserOrderHistoryRequest()
            .setCurrencyPair(getValidCurrencyPair());
        final UserOrders userOrders = client.getUserOrderHistory(request);

        logger.debug("{}", userOrders);

        Assert.assertEquals(userOrders.getCategory(), OPEN);
    }

    @Test
    public void testGetUserOrderCanceledAndFilled()
        throws CTTKException
    {
        CurrencyPair pair = new CurrencyPair.SimpleCurrencyPair("ETH", "KRW");
        String cancelOrderId = "1547605871390157";
        String filledOrderId = "1547533672278663";
        UserOrder canceledUserOrder = client.getUserOrder(new GetUserOrderRequest().setOid(cancelOrderId).setCurrencyPair(pair));
        Assert.assertNull(canceledUserOrder);

        UserOrder filledUserOrder = client.getUserOrder(new GetUserOrderRequest().setOid(filledOrderId).setCurrencyPair(pair));
        Assert.assertNotNull(filledUserOrder);
        Assert.assertEquals(filledUserOrder.getStatusType(), OrderStatus.FILLED);
        Assert.assertEquals(filledUserOrder.getOid(), filledOrderId);
        Assert.assertNull(filledUserOrder.getTrades());

        UserOrder filledUserOrderWithTrades = client.getUserOrder(new GetUserOrderRequest().setOid(filledOrderId).setCurrencyPair(pair).setWithTrades(true));
        Assert.assertFalse(filledUserOrderWithTrades.getTrades().isEmpty());
    }

    @Test
    public void testGetUserTrades()
        throws CTTKException
    {
        CurrencyPair pair = new CurrencyPair.SimpleCurrencyPair("ETH", "KRW");
        String filledOrderId = "1547533672278663";

        UserTrades userTrades = client.getUserTrades(new GetUserTradesRequest().setOrderId(filledOrderId).setCurrencyPair(pair));
        Assert.assertNotNull(userTrades);
        Assert.assertFalse(userTrades.getTrades().isEmpty());
    }
}
