package cttk.impl.korbit;

import cttk.CXIds;
import cttk.impl.AbstractPollingCXStreamTest;

public class KorbitPollingStreamTest
    extends AbstractPollingCXStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.KORBIT;
    }
}
