package cttk.impl.bitfinex;

import static cttk.CXIds.BITFINEX;
import static cttk.dto.UserOrders.Category.CLOSED;
import static cttk.impl.bitfinex.BitfinexUtils.nonce;
import static cttk.impl.bitfinex.BitfinexUtils.toBitfinexCurrencySymbol;
import static cttk.impl.util.Objects.nvl;
import static cttk.util.DeltaUtils.sleep;
import static cttk.util.DeltaUtils.sleepWithException;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonProcessingException;

import cttk.CXClientFactory;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.SortType;
import cttk.TradingFeeProvider;
import cttk.auth.Credential;
import cttk.dto.Balances;
import cttk.dto.BankAccount;
import cttk.dto.MarketInfos;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.dto.UserAccount;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;
import cttk.dto.Wallet;
import cttk.dto.Wallets;
import cttk.dto.WithdrawResult;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXServerException;
import cttk.exception.InsufficientBalanceException;
import cttk.exception.InvalidAmountMinException;
import cttk.exception.InvalidAmountRangeException;
import cttk.exception.InvalidOrderException;
import cttk.exception.InvalidPriceMaxException;
import cttk.exception.InvalidPriceMinException;
import cttk.exception.InvalidPriceRangeException;
import cttk.exception.OrderNotFoundException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.bitfinex.response.BitfinexBallancesResponse;
import cttk.impl.bitfinex.response.BitfinexCancelOrderResponse;
import cttk.impl.bitfinex.response.BitfinexCreateOrderResponse;
import cttk.impl.bitfinex.response.BitfinexTransferResponse;
import cttk.impl.bitfinex.response.BitfinexUserAccountResponse;
import cttk.impl.bitfinex.response.BitfinexUserOrderResponse;
import cttk.impl.bitfinex.response.BitfinexUserTradesResponse;
import cttk.impl.bitfinex.response.BitfinexWithdrawResponse;
import cttk.impl.util.CryptoUtils;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.OParamsBuilder;
import cttk.impl.util.ParamsBuilder;
import cttk.request.CancelOrderRequest;
import cttk.request.CreateMarketPriceOrderRequest;
import cttk.request.CreateOrderRequest;
import cttk.request.GetBankAccountRequest;
import cttk.request.GetOpenUserOrdersRequest;
import cttk.request.GetTransfersDeltaRequest;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserAccountRequest;
import cttk.request.GetUserOrderHistoryRequest;
import cttk.request.GetUserOrderRequest;
import cttk.request.GetUserTradesDeltaRequest;
import cttk.request.GetUserTradesRequest;
import cttk.request.GetWalletRequest;
import cttk.request.WithdrawToBankRequest;
import cttk.request.WithdrawToWalletRequest;
import cttk.util.DeltaUtils;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class BitfinexPrivateClient
    extends AbstractBitfinexClient
    implements PrivateCXClient
{
    private static final String EXCHANGE = "bitfinex";
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private static final BigDecimal MARKET_RANDOM_PRICE = new BigDecimal("1000");

    private final Credential credential;

    private PublicCXClient pubClient = null;
    private MarketInfos marketInfos = null;
    private List<String> validPairs = null;

    public BitfinexPrivateClient(Credential credential) {
        super();
        this.credential = credential;

        // public client for getting getMarketInfo
        try {
            this.pubClient = CXClientFactory.createPublicCXClient(this.getCxId());
            this.marketInfos = pubClient.getMarketInfos();
            this.validPairs = marketInfos.getMarketInfos().stream().map(i -> i.getPairString()).collect(Collectors.toList());
        } catch (CTTKException e) {
            logger.error("Failed to getMarketInfos while creating " + this.getCxId() + ". UnsupporedCurrencyException for private data could not be working correctly");
        }
    }

    @Override
    public UserOrders getOpenUserOrders(GetOpenUserOrdersRequest request)
        throws CTTKException
    {
        final UserOrders userOrders = getAllOpenUserOrders(request.isWithTrades());

        if (userOrders == null) return null;
        UserOrders filteredUserOrders = userOrders.filterByCurrencyPair(request);

        if (filteredUserOrders.getOrders().size() == 0 && !validPairs.contains(request.getPairString())) {
            throw new UnsupportedCurrencyPairException()
                .setCxId(this.getCxId())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency());
        }

        return filteredUserOrders;
    }

    @Override
    public UserOrders getAllOpenUserOrders(boolean withTrades)
        throws CTTKException
    {
        final String endpoint = "/v1/orders";

        final HttpResponse response = requestPostForV1(endpoint,
            ParamsBuilder.create()
                .putIfNotNull("request", endpoint)
                .putIfNotNull("nonce", nonce())
                .build());

        checkResponseError(null, response);

        try {
            return convertList(response, BitfinexUserOrderResponse.class,
                (data) -> new UserOrders()
                    .setOrders(data.stream().map(i -> i.toUserOrder()).filter(i -> i != null).collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getAllOpenUserOrders", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryRequest request)
        throws CTTKException
    {
        final String endpoint = "/v1/orders/hist";

        final HttpResponse response = requestPostForV1(endpoint,
            ParamsBuilder.create()
                .putIfNotNull("request", endpoint)
                .putIfNotNull("limit", request.getCount())
                .putIfNotNull("nonce", nonce())
                .build());
        try {
            checkResponseError(request, response);
        } catch (OrderNotFoundException e) {
            return new UserOrders().setCategory(CLOSED);
        }

        UserOrders filteredUserOrders;
        try {
            final UserOrders userOrders = convertList(response, BitfinexUserOrderResponse.class,
                (data) -> new UserOrders()
                    .setOrders(data.stream().map(i -> i.toUserOrder()).filter(i -> i != null)
                        .collect(Collectors.toList())));
            if (userOrders == null) return null;
            filteredUserOrders = request.isBothNull()
                ? userOrders.setCategory(CLOSED)
                : userOrders.filterByCurrencyPair(request).setCategory(CLOSED);

        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getUserOrderHistory", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }

        if (!request.isBothNull() && filteredUserOrders.getOrders().size() == 0 && !validPairs.contains(request.getPairString())) {
            throw new UnsupportedCurrencyPairException()
                .setCurrencyPair(request)
                .setCxId(getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }

        return filteredUserOrders;
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesRequest request)
        throws CTTKException
    {
        final String endpoint = "/v1/mytrades";

        final HttpResponse response = requestPostForV1(endpoint,
            OParamsBuilder.create()
                .putIfNotNull("request", endpoint)
                .putIfNotNull("nonce", nonce())
                .putIfNotNull("symbol", BitfinexUtils.getMarketSymbol(request))
                .putIfNotNull("limit_trades", request.getCount())
                .putIfNotNull("timestamp", request.getStartEpochInSecond())
                .putIfNotNull("until", request.getEndEpochInSecond())
                .putIfNotNull("reverse", BitfinexUtils.toInteger(request.getSort()))
                .build());

        checkResponseError(request, response);

        try {
            return convertList(response, BitfinexUserTradesResponse.class,
                (data) -> new UserTrades()
                    .setTrades(data.stream().map(i -> i.toUserTrade(request)).collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getUserTrades", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesDeltaRequest request)
        throws CTTKException
    {
        // GROUP t-a: time-asc,>>>(break)
        final GetUserTradesRequest partialRequest = new GetUserTradesRequest()
            .setCurrencyPair(request)
            .setStartDateTime(request.getStartDateTime())
            .setEndDateTime(nvl(request.getEndDateTime(), ZonedDateTime.now()))
            .setCount(50)
            .setSort(SortType.ASC); // Need to set asc explicitly

        if (request.hasStartDateTime()) {
            final Map<String, UserTrade> map = new LinkedHashMap<>();
            UserTrade prevLast = null;
            while (true) {
                final UserTrades userTrades = getUserTrades(partialRequest);
                final UserTrade last = userTrades.getLatest();

                if (userTrades.isEmpty() || UserTrade.equalsById(prevLast, last)) break;
                map.putAll(DeltaUtils.convertToMap(userTrades));

                partialRequest.setStartDateTime(userTrades.getMaxDateTime());
                prevLast = last;

                if (!sleep(request.getSleepTimeMillis())) break;
            }

            return DeltaUtils.convertUserTradesFromMap(map);
        } else {
            return getUserTrades(partialRequest);
        }
    }

    @Override
    public Transfers getTransfers(GetTransfersRequest request)
        throws CTTKException
    {
        final String endpoint = "/v1/history/movements";

        final HttpResponse response = requestPostForV1(endpoint,
            ParamsBuilder.create()
                .putIfNotNull("request", endpoint)
                .putIfNotNull("nonce", nonce())
                .putIfNotNull("currency", toBitfinexCurrencySymbol(request.getCurrency()))
                .putIfNotNull("method", request.getMethod())
                .putIfNotNull("since", request.getStartEpochInSecond())
                .putIfNotNull("until", request.getEndEpochInSecond())
                .build());

        checkResponseError(null, response);

        try {
            return convertList(response, BitfinexTransferResponse.class,
                (data) -> new Transfers()
                    .setTransfers(data.stream().map(i -> i.toTransfer()).collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getTransfers", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public Transfers getTransfers(GetTransfersDeltaRequest request)
        throws CTTKException
    {
        // GROUP t-d: time-desc,<<<(exception)
        final GetTransfersRequest partialRequest = new GetTransfersRequest()
            .setCurrency(request.getCurrency())
            .setStartDateTime(request.getStartDateTime())
            .setEndDateTime(nvl(request.getEndDateTime(), ZonedDateTime.now()))
            .setCount(1000);

        if (request.hasStartDateTime()) {
            final Map<String, Transfer> map = new LinkedHashMap<>();
            Transfer prevFirst = null;
            Transfer prevLast = null;
            while (true) {
                final Transfers transfers = getTransfers(partialRequest);
                final Transfer first = transfers.getOldest();
                final Transfer last = transfers.getLatest();

                if (transfers.isEmpty() || Transfer.equalsById(prevFirst, first)) break;
                map.putAll(DeltaUtils.convertToMap(transfers));

                final int s = DeltaUtils.calcNextSeconds(prevLast, last);
                partialRequest.setEndDateTime(first.getDateTime().plusSeconds(s));
                prevFirst = first;
                prevLast = last;

                sleepWithException(request.getSleepTimeMillis(), BITFINEX);
            }

            return DeltaUtils.convertTransfersFromMap(map);
        } else {
            return getTransfers(partialRequest);
        }
    }

    @Override
    public UserOrder getUserOrder(GetUserOrderRequest request)
        throws CTTKException
    {
        final String endpoint = "/v1/order/status";

        final HttpResponse response = requestPostForV1(endpoint,
            OParamsBuilder.create()
                .putIfNotNull("request", endpoint)
                .putIfNotNull("nonce", nonce())
                .putIfNotNull("order_id", Long.parseLong(request.getOid()))
                .build());
        try {
            checkResponseError(request, response);
        } catch (OrderNotFoundException e) {
            return null;
        }
        return convert(response, BitfinexUserOrderResponse.class, (i -> i.toUserOrder()));
    }

    @Override
    public UserAccount getUserAccount(GetUserAccountRequest request)
        throws CTTKException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public Balances getAllBalances()
        throws CTTKException
    {
        final String endpoint = "/v1/balances";

        final HttpResponse response = requestPostForV1(endpoint,
            ParamsBuilder.create()
                .putIfNotNull("request", endpoint)
                .putIfNotNull("nonce", nonce())
                .build());

        checkResponseError(null, response);
        try {
            return convertList(response, BitfinexBallancesResponse.class,
                (data) -> new Balances()
                    .setBalances(data.stream().map(i -> i.toBalance())
                        .filter(i -> i.getTotal().compareTo(BigDecimal.ZERO) != 0)
                        .collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getAllBalances", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public UserOrder createOrder(CreateOrderRequest request)
        throws CTTKException
    {
        final String endpoint = "/v1/order/new";

        final HttpResponse response = requestPostForV1(endpoint,
            ParamsBuilder.create()
                .putIfNotNull("request", endpoint)
                .putIfNotNull("nonce", nonce())
                .putIfNotNull("symbol", BitfinexUtils.getMarketSymbol(request))
                .putIfNotNull("amount", request.getQuantity())
                .putIfNotNull("price", request.getPrice())
                .putIfNotNull("exchange", EXCHANGE)
                .putIfNotNull("side", BitfinexUtils.toString(request.getSide()))
                .putIfNotNull("type", "exchange limit")
                .build());

        checkOrderResponseError(request, response);
        checkResponseError(request, response);

        return fillResponseByRequest(request, convert(response, BitfinexCreateOrderResponse.class, i -> i.toUserOrder()));
    }

    @Override
    public UserOrder createMarketPriceOrder(CreateMarketPriceOrderRequest request)
        throws CTTKException
    {
        final String endpoint = "/v1/order/new";

        final HttpResponse response = requestPostForV1(endpoint,
            ParamsBuilder.create()
                .putIfNotNull("request", endpoint)
                .putIfNotNull("nonce", nonce())
                .putIfNotNull("symbol", BitfinexUtils.getMarketSymbol(request))
                .putIfNotNull("amount", request.getQuantity())
                .putIfNotNull("price", MARKET_RANDOM_PRICE)
                .putIfNotNull("exchange", EXCHANGE)
                .putIfNotNull("side", BitfinexUtils.toString(request.getSide()))
                .putIfNotNull("type", "exchange market")
                .build());

        checkResponseError(request, response);

        return fillResponseByRequest(request, convert(response, BitfinexCreateOrderResponse.class, i -> i.toUserOrder()));
    }

    @Override
    public void cancelOrder(CancelOrderRequest request)
        throws CTTKException
    {
        final String endpoint = "/v1/order/cancel";

        final HttpResponse response = requestPostForV1(endpoint,
            OParamsBuilder.create()
                .putIfNotNull("request", endpoint)
                .putIfNotNull("nonce", nonce())
                .putIfNotNull("order_id", Long.parseLong(request.getOrderId()))
                .build());

        checkResponseError(request, response);

        convert(response, BitfinexCancelOrderResponse.class);
    }

    @Override
    public WithdrawResult withdraw(WithdrawToWalletRequest request)
        throws CTTKException
    {
        final String endpoint = "/v1/withdraw";

        // TODO: should know what wallet_type is meaning for
        final HttpResponse response = requestPostForV1(endpoint,
            ParamsBuilder.create()
                .putIfNotNull("request", endpoint)
                .putIfNotNull("nonce", nonce())
                .putIfNotNull("withdraw_type", toBitfinexCurrencySymbol(request.getCurrency()))
                .putIfNotNull("walletselected", "exchange")
                .putIfNotNull("amount", request.getQuantity())
                .putIfNotNull("address", request.getAddress())
                .build());

        checkResponseError(null, response);

        return convert(response, BitfinexWithdrawResponse.class, (i -> i.getWithdrawResult()));
    }

    @Override
    public WithdrawResult withdraw(WithdrawToBankRequest request)
        throws CTTKException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public BankAccount getBankAccount(GetBankAccountRequest request)
        throws CTTKException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public Wallets getAllWallets()
        throws CTTKException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public TradingFeeProvider getTradingFeeProvider()
        throws CTTKException
    {
        final String endpoint = "/v1/account_infos";

        final HttpResponse response = requestPostForV1(endpoint,
            ParamsBuilder.create()
                .putIfNotNull("request", endpoint)
                .putIfNotNull("nonce", nonce())
                .build());

        checkResponseError(null, response);

        try {
            return convertList(response, BitfinexUserAccountResponse.class, (data) -> data.get(0).getTradingFeeProvider());
        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getTradingFeeProvider", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public Wallet getWallet(GetWalletRequest request)
        throws CTTKException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    private HttpResponse requestPostForV1(String endpoint, Map<String, ?> params)
        throws CTTKException
    {
        Request request;
        try {
            request = new Request.Builder()
                .url(getHostUrl(endpoint))
                .headers(createHttpHeadersForV1(params))
                .post(RequestBody.create(JSON, objectMapper.writeValueAsString(params)))
                .build();
        } catch (JsonProcessingException e) {
            throw new CTTKException(e)
                .setCxId(getCxId());
        }
        debug(request, params);

        try {
            try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
                return toHttpResponse(httpResponse);
            }
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }

    }

    private Headers createHttpHeadersForV1(Map<String, ?> params) {
        try {
            final String base64Payload = CryptoUtils.base64(objectMapper.writeValueAsString(params));
            final String signature = CryptoUtils.hmacDigest(base64Payload, credential.getSecretKey());

            return new Headers.Builder()
                .add("X-BFX-APIKEY", credential.getAccessKey())
                .add("X-BFX-PAYLOAD", base64Payload)
                .add("X-BFX-SIGNATURE", signature)
                .build();
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private void checkOrderResponseError(CreateOrderRequest request, HttpResponse response)
        throws CTTKException
    {
        // status 400
        // precision 에러는 내지않고 잘라서 주문이 들어간다.
        final String body = response.getBody();
        String exchangeMessage = null;
        try {
            Map<?, ?> map = objectMapper.readValue(body, Map.class);
            exchangeMessage = Objects.toString(map.get("message"));
        } catch (IOException e) {
            logger.error("Unable to parse:: {}", body, e);
        }
        if (exchangeMessage == null) return;

        if (exchangeMessage.contains("Invalid price.")
            || exchangeMessage.contains("Key price should be a decimal number, e.g. \"123.456\""))
        {
            throw new InvalidPriceRangeException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(response.getStatus())
                .setOrderQuantity(request.getQuantity())
                .setOrderPrice(request.getPrice())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency());
        } else if (exchangeMessage.contains("Invalid order: minimum size for ")
            || exchangeMessage.contains("Invalid order size (amount=0)")
            || exchangeMessage.contains("Offer amount must be positive."))
        {
            throw new InvalidAmountMinException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(response.getStatus())
                .setOrderQuantity(request.getQuantity())
                .setOrderPrice(request.getPrice())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency());
        } else if (exchangeMessage.contains("Invalid price")) {
            throw new InvalidPriceRangeException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(response.getStatus())
                .setOrderQuantity(request.getQuantity())
                .setOrderPrice(request.getPrice())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency());
        } else if (exchangeMessage.contains("Invalid order: not enough exchange balance")
            || exchangeMessage.contains("the available balance is only"))
        {
            throw new InsufficientBalanceException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(response.getStatus())
                .setOrderQuantity(request.getQuantity())
                .setOrderPrice(request.getPrice())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency());
        } else if (exchangeMessage.contains("Key amount should be a decimal number, e.g. \"123.456\"")) {
            throw new InvalidAmountRangeException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(response.getStatus())
                .setOrderQuantity(request.getQuantity())
                .setOrderPrice(request.getPrice())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency());
        } else if (exchangeMessage.contains("Price too big")) {
            throw new InvalidPriceMaxException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(response.getStatus())
                .setOrderQuantity(request.getQuantity())
                .setOrderPrice(request.getPrice())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency());
        } else if (exchangeMessage.contains("Order price must be positive.")) {
            throw new InvalidPriceMinException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(response.getStatus())
                .setOrderQuantity(request.getQuantity())
                .setOrderPrice(request.getPrice())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency());
        } else if (exchangeMessage.contains("Invalid order")) {
            throw new InvalidOrderException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(response.getStatus())
                .setOrderQuantity(request.getQuantity())
                .setOrderPrice(request.getPrice())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency());
        }
    }
}
