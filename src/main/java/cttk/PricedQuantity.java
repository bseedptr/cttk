package cttk;

import static cttk.impl.util.MathUtils.multiply;

import java.math.BigDecimal;

public interface PricedQuantity {
    BigDecimal getPrice();

    BigDecimal getQuantity();

    default BigDecimal computeTotal() {
        return multiply(getPrice(), getQuantity());
    }
}
