package cttk.impl.huobi;

import cttk.CXIds;
import cttk.impl.AbstractUserDataStreamTest;

public class HuobiUserDataStreamTest
    extends AbstractUserDataStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.HUOBI;
    }

    @Override
    protected boolean useThread() {
        return true;
    }
}
