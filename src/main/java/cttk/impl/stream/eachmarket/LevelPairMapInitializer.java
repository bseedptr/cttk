package cttk.impl.stream.eachmarket;

import static cttk.impl.stream.eachmarket.PollingLevel.LEVEL1;
import static cttk.impl.stream.eachmarket.PollingLevel.LEVEL2;
import static cttk.impl.stream.eachmarket.PollingLevel.LEVEL3;

import java.util.LinkedList;
import java.util.List;

import cttk.CurrencyPair;
import cttk.exception.CTTKException;
import cttk.impl.util.Timer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
public class LevelPairMapInitializer {
    private final LimitedCXClient limitedClient;

    public LevelPairMap init()
        throws CTTKException
    {
        try {
            Timer timer = new Timer();

            List<CurrencyPair> allPairs = limitedClient.getAllCurrencyPairs();
            LevelPairMap map = new LevelPairMap()
                .setPairs(LEVEL1, collectLevel1(allPairs))
                .setPairs(LEVEL2, collectLevel2(allPairs))
                .setPairs(LEVEL3, collectLevel3(allPairs));

            log.debug("LevelPairMap initialized: {}ms, TOTAL: {}(LEVEL1: {}, LEVEL2: {}, LEVEL3: {})",
                timer.stop().getIntervalInMilli(), map.countAll(), map.count(LEVEL1), map.count(LEVEL2), map.count(LEVEL3));

            return map;
        } catch (Exception e) {
            throw CTTKException.createOrCast(e, limitedClient.getCxId());
        }
    }

    private List<CurrencyPair> collectLevel1(List<CurrencyPair> original)
        throws CTTKException
    {
        List<CurrencyPair> collected = new LinkedList<>();
        for (int i = 0; i < original.size(); i++) {
            CurrencyPair pair = original.get(i);
            if (limitedClient.isExistsOpenOrders(pair)) {
                original.remove(i);
                collected.add(pair);
                i--;
            }
        }
        return collected;
    }

    private List<CurrencyPair> collectLevel2(List<CurrencyPair> original)
        throws CTTKException
    {
        List<CurrencyPair> collected = new LinkedList<>();
        List<String> currencies = limitedClient.getCurrenciesHaveBalance();
        for (int i = 0; i < original.size(); i++) {
            CurrencyPair pair = original.get(i);
            if (isCurrenciesHavePair(currencies, pair)) {
                original.remove(i);
                collected.add(pair);
                i--;
            }
        }
        return collected;
    }

    private List<CurrencyPair> collectLevel3(List<CurrencyPair> original) {
        return new LinkedList<>(original);
    }

    private boolean isCurrenciesHavePair(List<String> currencies, CurrencyPair pair) {
        String baseCurrency = pair.getBaseCurrency();
        String quoteCurrency = pair.getQuoteCurrency();
        return currencies.contains(baseCurrency) || currencies.contains(quoteCurrency);
    }
}
