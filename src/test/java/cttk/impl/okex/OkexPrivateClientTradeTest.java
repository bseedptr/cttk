package cttk.impl.okex;

import org.junit.Test;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.impl.AbstractPrivateCXClientTradeTest;
import cttk.impl.util.ExNotRaisedException;

public class OkexPrivateClientTradeTest
    extends AbstractPrivateCXClientTradeTest
{
    @Override
    public String getCxId() {
        return CXIds.OKEX;
    }

    @Test(expected = ExNotRaisedException.class)
    @Override
    public void testInvalidQuantityIncrementPrecision()
        throws CTTKException
    {
        // Please refer to CTTK-500 for detailed explanation
        // Okex automatically truncates the order with invalid precision
        super.testInvalidQuantityIncrementPrecision();
    }

    @Test(expected = ExNotRaisedException.class)
    @Override
    public void testInvalidPriceIncrementPrecision()
        throws CTTKException
    {
        // Please refer to CTTK-500 for detailed explanation
        // Okex automatically truncates the order with invalid precision
        super.testInvalidPriceIncrementPrecision();
    }
}
