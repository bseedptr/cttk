package cttk.impl.bitfinex.stream.response;

import static cttk.impl.bitfinex.BitfinexUtils.toStdCurrencyPair;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cttk.CurrencyPair;
import cttk.dto.OrderBook;
import cttk.dto.OrderBook.SimpleOrder;

public class BitfinexBigDecimalListResponse
    extends ArrayList<BigDecimal>
{
    public OrderBook toOrderBook(CurrencyPair currencyPair) {
        final BigDecimal price = get(1);
        final BigDecimal count = get(2);
        final BigDecimal quantity = get(3);
        final List<SimpleOrder> orders = new ArrayList<>(Arrays.asList(new SimpleOrder(price, quantity.abs(), count)));

        return new OrderBook()
            .setCurrencyPair(toStdCurrencyPair(currencyPair))
            .setDateTime(ZonedDateTime.now())
            .setAsks(quantity.compareTo(BigDecimal.ZERO) <= 0 ? orders : null)
            .setBids(quantity.compareTo(BigDecimal.ZERO) >= 0 ? orders : null);
    }
}
