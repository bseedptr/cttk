package cttk.impl.okex.response;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;

import cttk.dto.Tickers;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class OkexAllTickersResponse
    extends ArrayList<OkexTicker>
{
    public Tickers toTickers() {
        return new Tickers()
            .setTickers(stream().map(OkexTicker::toTicker).collect(toList()));
    }
}
