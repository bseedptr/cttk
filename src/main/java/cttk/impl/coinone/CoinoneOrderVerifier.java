package cttk.impl.coinone;

import static cttk.impl.coinone.CoinoneUtils.PRICE_INCREMENTS;
import static cttk.impl.coinone.CoinoneUtils.PRICE_RANGES;

import java.math.BigDecimal;

import cttk.dto.MarketInfo;
import cttk.impl.MarketInfoOrderVerifier;
import cttk.impl.util.MathUtils;

public class CoinoneOrderVerifier
    extends MarketInfoOrderVerifier
{
    public CoinoneOrderVerifier(MarketInfo marketInfo) {
        super(marketInfo);
    }

    @Override
    public BigDecimal roundPrice(BigDecimal price) {
        if (price == null) return null;
        if (marketInfo == null || marketInfo.getPriceIncrement() != null) {
            return super.roundPrice(price);
        } else {
            final BigDecimal increment = MathUtils.findIncrement(PRICE_RANGES, PRICE_INCREMENTS, price);

            return increment == null ? price : MathUtils.round(price, increment);
        }
    }
}
