package cttk.example;

import java.io.IOException;
import java.util.List;

import cttk.CTTK;
import cttk.ChargingPolicyProvider;
import cttk.ChargingPolicyProviderFactory;
import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.dto.ChargingPolicy;
import cttk.exception.CTTKException;
import cttk.test.TestConfig;

public class ChargingPolicyProviderExample {
    public static void main(String[] args)
        throws IOException
    {
        List<String> cxIds = CTTK.getSupportedCxIds();

        for (String cxId : cxIds) {
            final TestConfig testConfig = TestConfig.readFromClasspathYaml("config/" + cxId.toLowerCase() + ".yml");
            final CurrencyPair currencyPair = testConfig.getValidCurrencyPair();

            try {
                final ChargingPolicyProvider provider = ChargingPolicyProviderFactory.create(cxId);

                final List<ChargingPolicy> buyPolicies = provider.getChargingPolicies(currencyPair, OrderSide.BUY);

                final List<ChargingPolicy> sellPolicies = provider.getChargingPolicies(currencyPair, OrderSide.SELL);

                System.out.printf("%8s: %s%n", cxId, buyPolicies);
                System.out.printf("%8s: %s%n%n", cxId, sellPolicies);
            } catch (CTTKException e) {
                e.printStackTrace();
                continue;
            }
        }
    }
}
