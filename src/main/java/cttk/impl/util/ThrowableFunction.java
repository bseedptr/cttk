package cttk.impl.util;

import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;

@FunctionalInterface
public interface ThrowableFunction<T, R> {
    R apply(T t)
        throws CTTKException, UnsupportedCurrencyPairException;

    static <T> ThrowableFunction<T, T> identity() {
        return t -> t;
    }
}
