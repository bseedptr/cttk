package cttk.impl.bitfinex.response;

import static cttk.impl.bitfinex.BitfinexUtils.toBigDecimal;
import static cttk.impl.bitfinex.BitfinexUtils.toLong;
import static cttk.impl.util.DateTimeUtils.zdtFromEpochMilli;
import static cttk.util.StringUtils.parseBigInteger;

import java.math.BigDecimal;
import java.util.ArrayList;

import cttk.OrderSide;
import cttk.dto.Trade;
import cttk.impl.util.MathUtils;
import cttk.util.StringUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BitfinexRecentTradesResponse
    extends ArrayList<Object>
{
    public Trade toTrade() {
        return new Trade()
            .setSno(parseBigInteger(StringUtils.valueOf(this.get(0))))
            .setTid(StringUtils.valueOf(this.get(0)))
            .setDateTime(zdtFromEpochMilli(toLong(this.get(1))))
            .setSide(toBigDecimal(this.get(2)).compareTo(BigDecimal.ZERO) > 0 ? OrderSide.BUY : OrderSide.SELL)
            .setQuantity(toBigDecimal(this.get(2)).abs())
            .setPrice(toBigDecimal(this.get(3)))
            .setTotal(MathUtils.multiply(toBigDecimal(this.get(3)), toBigDecimal(this.get(2)).abs()));
    }
}