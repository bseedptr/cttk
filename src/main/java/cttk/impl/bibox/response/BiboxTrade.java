package cttk.impl.bibox.response;

import cttk.dto.Trade;
import cttk.impl.bibox.BiboxUtils;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;

@Data
public class BiboxTrade {
    BigInteger id;
    String pair;
    BigDecimal price;
    BigDecimal amount;
    Long time;
    Integer side;

    public Trade toTrade() {
        return new Trade()
            .setSno(id)
            .setTid(String.valueOf(id))
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(time))
            .setSide(BiboxUtils.toOrderSide(side))
            .setQuantity(amount)
            .setPrice(price)
            .setTotal(MathUtils.multiply(price, amount));
    }
}
