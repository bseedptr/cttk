package cttk.auth;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cttk.exception.CTTKException;
import cttk.exception.CXAuthorityException;

public class CachedAuthTokenProvider
    implements AuthTokenProvider
{
    public static final long DEFAULT_CLEANUP_INTERVAL_IN_SECOND = 60;
    public static final long DEFAULT_REFRESH_MARGIN_IN_SECOND = 60;

    final Logger logger = LoggerFactory.getLogger(getClass());

    final Map<Credential, AuthToken> hash = new HashMap<>();
    final AuthTokenProvider authTokenProvider;
    final long refreshMarginInSecond;
    long cleanupIntervalInSecond = DEFAULT_CLEANUP_INTERVAL_IN_SECOND;
    long lastCleanupEpochMilli = 0;

    public CachedAuthTokenProvider(AuthTokenProvider authTokenProvider) {
        this(authTokenProvider, DEFAULT_REFRESH_MARGIN_IN_SECOND);
    }

    public CachedAuthTokenProvider(AuthTokenProvider authTokenProvider, long refreshMarginInSecond) {
        super();
        this.authTokenProvider = authTokenProvider;
        this.refreshMarginInSecond = refreshMarginInSecond;
        cleanup();
    }

    public CachedAuthTokenProvider setCleanupIntervalInSecond(long cleanupIntervalInSecond) {
        this.cleanupIntervalInSecond = cleanupIntervalInSecond;
        return this;
    }

    @Override
    public AuthToken getAuthToken(Credential credential)
        throws CTTKException
    {
        AuthToken token = hash.get(credential);
        if (token == null || token.getExpireDateTime().isBefore(ZonedDateTime.now())) {
            token = authTokenProvider.getAuthToken(credential);
            hash.put(credential, token);
        }

        refreshAuthToken(credential, token);
        cleanup();

        return token;
    }

    @Override
    public void refreshAuthToken(Credential credential, AuthToken authToken)
        throws CXAuthorityException
    {
        if (authToken == null) return;
        if (ZonedDateTime.now().until(authToken.getExpireDateTime(), ChronoUnit.SECONDS) < refreshMarginInSecond) {
            final ExecutorService executor = Executors.newCachedThreadPool();
            executor.execute(() -> {
                try {
                    authTokenProvider.refreshAuthToken(credential, authToken);
                } catch (CTTKException e) {
                    logger.error("Uanbel to refresh auth token", e);
                }
            });
            executor.shutdown();
        }
    }

    private void cleanup() {

        final long epochMilli = System.currentTimeMillis();

        if (epochMilli > this.lastCleanupEpochMilli - (this.cleanupIntervalInSecond * 1000)) {
            final List<Credential> toBeRemoved = hash.entrySet().stream()
                .filter(e -> e.getValue().getExpireDateTime().isBefore(ZonedDateTime.now()))
                .map(e -> e.getKey())
                .collect(Collectors.toList());

            toBeRemoved.forEach(c -> hash.remove(c));
            this.lastCleanupEpochMilli = epochMilli;
        }
    }
}
