package cttk.impl.bitfinex;

import java.io.IOException;
import java.util.Map;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.exception.CXServerException;
import cttk.impl.AbstractCXClient;
import cttk.impl.util.HttpResponse;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.Response;

public abstract class AbstractBitfinexClient
    extends AbstractCXClient
{
    protected static final String BASE_URL = "https://api.bitfinex.com";

    @Override
    public String getCxId() {
        return CXIds.BITFINEX;
    }

    @Override
    protected void checkErrorByResponseMessage(Object requestObj, HttpResponse response)
        throws CTTKException
    {
        if (response.is2xxSuccessful()) return;

        BitfinexUtils.checkErrorByResponseMessage(requestObj, response.getBody(), response.getStatus());
    }

    protected HttpResponse requestGet(String endpoint)
        throws CTTKException
    {
        return requestGet(endpoint, null);
    }

    protected HttpResponse requestGet(String endpoint, Map<String, String> params)
        throws CTTKException
    {
        final Request request = new Request.Builder()
            .url(getHostUrl(endpoint, params))
            .build();

        debug(request);

        try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
            return toHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    protected String getHostUrl(String endpoint) {
        return getHostUrl(BASE_URL, endpoint);
    }

    protected String getHostUrl(String endpoint, Map<String, String> params) {
        return getHostUrl(BASE_URL, endpoint, params);
    }

    protected FormBody createFormBody(Map<String, String> params) {
        FormBody.Builder builder = new FormBody.Builder();

        if (params != null) params.forEach((k, v) -> builder.addEncoded(k, v));

        return builder.build();
    }
}
