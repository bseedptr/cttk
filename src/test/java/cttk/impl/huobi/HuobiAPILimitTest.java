package cttk.impl.huobi;

import org.junit.Test;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.impl.AbstractAPILimitTest;

public class HuobiAPILimitTest
    extends AbstractAPILimitTest
{
    @Override
    public String getCxId() {
        return CXIds.HUOBI;
    }

    @Test
    @Override
    public void testPublicAPILimit()
        throws CTTKException, InterruptedException
    {
        try {
            Thread.sleep(DEFAULT_API_LIMIT_RECOVERY_TIME_IN_SECOND * 1000);
        } catch (InterruptedException e) {
            // ignore exception here 
        }
        testCase((args) -> {
            return publicClient.getOrderBook(this.getValidCurrencyPair().getBaseCurrency(), this.getValidCurrencyPair().getQuoteCurrency(), 100);
        }, false, 1000);
        // We can't reach public API limit
        logger.info("We can't reach public API limit in " + getCxId());
    }

    @Test
    public void testPrivateAPILimit()
        throws CTTKException, InterruptedException
    {
        try {
            Thread.sleep(DEFAULT_API_LIMIT_RECOVERY_TIME_IN_SECOND * 1000);
        } catch (InterruptedException e) {
            // ignore exception here 
        }
        testCase((args) -> {
            return privateClient.getAllBalances();
        }, false, 1000);
        // We can't reach private API limit
        logger.info("We can't reach private API limit in " + getCxId());
    }

}
