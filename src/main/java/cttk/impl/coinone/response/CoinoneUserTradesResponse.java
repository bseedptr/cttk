package cttk.impl.coinone.response;

import static cttk.PricingType.LIMIT;
import static cttk.dto.UserOrders.Category.CLOSED;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;
import cttk.impl.coinone.CoinoneUtils;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import cttk.impl.util.Objects;
import cttk.impl.util.ZoneIds;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CoinoneUserTradesResponse
    extends CoinoneResponse
{
    List<CoinoneUserTrade> completeOrders;

    public UserTrades toUserTrades(CurrencyPair currencyPair) {
        return new UserTrades()
            .setTrades(toUserTradeList(currencyPair, completeOrders));
    }

    private List<UserTrade> toUserTradeList(CurrencyPair currencyPair, List<CoinoneUserTrade> coinoneUserTrades) {
        if (coinoneUserTrades == null)
            return null;
        return coinoneUserTrades.stream().map(i -> i.toUserTrade(currencyPair)).collect(Collectors.toList());
    }

    public UserOrders toUserOrders(CurrencyPair currencyPair) {
        return new UserOrders()
            .setCategory(CLOSED)
            .setOrders(toUserOrderList(currencyPair, completeOrders));
    }

    private List<UserOrder> toUserOrderList(CurrencyPair currencyPair, List<CoinoneUserTrade> coinoneUserTrades) {
        if (coinoneUserTrades == null)
            return null;
        return coinoneUserTrades.stream().map(i -> i.toUserOrder(currencyPair)).collect(Collectors.toList());
    }
}

@Data
class CoinoneUserTrade {
    Long timestamp;
    BigDecimal price;
    BigDecimal qty;
    String type;
    BigDecimal feeRate;
    BigDecimal fee;
    String orderId;

    public UserTrade toUserTrade(CurrencyPair currencyPair) {
        final OrderSide side = OrderSide.of(type);
        final String feeCurrency = CoinoneUtils.guessFeeCurrency(side, currencyPair);

        return new UserTrade()
            .setTid(orderId)
            .setBaseCurrency(currencyPair.getBaseCurrency())
            .setQuoteCurrency(Objects.nvl(currencyPair.getQuoteCurrency(), "KRW"))
            .setCurrencyPair(currencyPair)
            .setDateTime(DateTimeUtils.zdtFromEpochSecond(timestamp, ZoneIds.SEOUL))
            .setOid(orderId)
            .setSide(side)
            .setPrice(price)
            .setQuantity(qty)
            .setTotal(MathUtils.multiply(price, qty))
            .setFee(fee)
            .setFeeRate(feeRate)
            .setFeeCurrency(feeCurrency);
    }

    public UserOrder toUserOrder(CurrencyPair currencyPair) {
        final OrderSide side = OrderSide.of(type);
        final String feeCurrency = CoinoneUtils.guessFeeCurrency(side, currencyPair);

        return new UserOrder()
            .setPricingType(LIMIT)
            .setOid(orderId)
            .setBaseCurrency(currencyPair.getBaseCurrency())
            .setQuoteCurrency(Objects.nvl(currencyPair.getQuoteCurrency(), "KRW"))
            .setCurrencyPair(currencyPair)
            .setDateTime(DateTimeUtils.zdtFromEpochSecond(timestamp, ZoneIds.SEOUL))
            .setOid(orderId)
            .setSide(side)
            .setFilledQuantity(qty)
            .setPrice(price)
            .setQuantity(qty)
            .setTotal(MathUtils.multiply(price, qty))
            .setStatusType(OrderStatus.FILLED)
            .setFee(fee)
            .setFeeRate(feeRate)
            .setFeeCurrency(feeCurrency);
    }
}