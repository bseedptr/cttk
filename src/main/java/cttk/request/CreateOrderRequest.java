package cttk.request;

import java.math.BigDecimal;

import cttk.AbstractCurrencyPair;
import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.PricedQuantity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class CreateOrderRequest
    extends AbstractCurrencyPair<CreateOrderRequest>
    implements PricedQuantity
{
    OrderSide side;
    BigDecimal quantity;
    BigDecimal price;

    public CreateOrderRequest setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
        return this;
    }

    public CreateOrderRequest setQuantity(String quantity) {
        this.quantity = new BigDecimal(quantity);
        return this;
    }

    public CreateOrderRequest setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public CreateOrderRequest setPrice(String price) {
        this.price = new BigDecimal(price);
        return this;
    }

    public static CreateOrderRequest of(
        CurrencyPair pair,
        OrderSide side,
        BigDecimal quantity,
        BigDecimal price)
    {
        return new CreateOrderRequest()
            .setCurrencyPair(pair)
            .setSide(side)
            .setQuantity(quantity)
            .setPrice(price);
    }
}
