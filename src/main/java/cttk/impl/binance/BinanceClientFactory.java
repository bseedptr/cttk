package cttk.impl.binance;

import cttk.CXClientFactory;
import cttk.CXStreamOpener;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.UserDataStreamOpener;
import cttk.auth.Credential;
import cttk.impl.binance.stream.BinanceStreamOpener;
import cttk.impl.binance.stream.BinanceUserDataStreamOpener;

public class BinanceClientFactory
    implements CXClientFactory
{
    @Override
    public PublicCXClient create() {
        return new BinancePublicClient();
    }

    @Override
    public PrivateCXClient create(Credential credential) {
        return new BinancePrivateClient(credential);
    }

    @Override
    public CXStreamOpener createCXStreamOpener() {
        return new BinanceStreamOpener();
    }

    @Override
    public UserDataStreamOpener createUserDataStreamOpener(Credential credential) {
        return new BinanceUserDataStreamOpener(credential);
    }
}
