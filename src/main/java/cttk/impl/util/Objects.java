package cttk.impl.util;

public class Objects {
    public static final <T> T nvl(@SuppressWarnings("unchecked") T... arr) {
        if (arr == null) return null;
        for (T obj : arr) {
            if (obj != null) return obj;
        }
        return null;
    }
}
