package cttk.exception;

import java.math.BigDecimal;

import cttk.CurrencyPair;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class MaxNumOrderException
    extends CXAPIRequestException
    implements CurrencyPair
{
    private static final long serialVersionUID = 1L;

    protected String baseCurrency;
    protected String quoteCurrency;
    protected BigDecimal orderPrice;
    protected BigDecimal orderQuantity;

    public MaxNumOrderException() {
        super();
    }

    public MaxNumOrderException(String message, Throwable cause) {
        super(message, cause);
    }

    public MaxNumOrderException(String message) {
        super(message);
    }

    public MaxNumOrderException(Throwable cause) {
        super(cause);
    }

    @Override
    public String toString() {
        return super.toString()
            + ", Base Currency = " + baseCurrency
            + ", Quote Currency = " + quoteCurrency
            + ", Order Price = " + orderPrice
            + ", Order Quantity = " + orderQuantity;
    }

    @Override
    public MaxNumOrderException setCxId(String cxId) {
        super.setCxId(cxId);
        return this;
    }

    @Override
    public MaxNumOrderException setResponseStatusCode(Integer responseStatusCode) {
        super.setResponseStatusCode(responseStatusCode);
        return this;
    }

    @Override
    public MaxNumOrderException setExchangeMessage(String exchangeMessage) {
        super.setExchangeMessage(exchangeMessage);
        return this;
    }

    @Override
    public MaxNumOrderException setExchangeErrorCode(String exchangeErrorCode) {
        super.setExchangeErrorCode(exchangeErrorCode);
        return this;
    }

    public MaxNumOrderException setCurrencyPair(CurrencyPair currencyPair) {
        this.baseCurrency = currencyPair == null ? null : currencyPair.getBaseCurrency();
        this.quoteCurrency = currencyPair == null ? null : currencyPair.getQuoteCurrency();
        return this;
    }
}
