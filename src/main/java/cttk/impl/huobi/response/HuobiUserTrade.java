package cttk.impl.huobi.response;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.dto.UserTrade;
import cttk.impl.huobi.HuobiUtils;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;

@Data
public class HuobiUserTrade {
    Long id;
    @JsonProperty("order-id")
    Long orderId;
    @JsonProperty("match-id")
    Long matchId;
    @JsonProperty("created-at")
    Long createTimestamp;
    String symbol;
    String type;
    String source;
    BigDecimal price;
    @JsonProperty("filled-amount")
    BigDecimal filledAmount;
    @JsonProperty("filled-fees")
    BigDecimal filledFees;
    @JsonProperty("filled-points")
    BigDecimal filledPoints;

    public UserTrade getUserTrade() {
        CurrencyPair pair = HuobiUtils.parseMarketSymbol(symbol);
        OrderSide side = HuobiUtils.toOrderSide(type);
        String feeCurrency = HuobiUtils.guessFeeCurrency(side, pair);
        return new UserTrade()
            .setCurrencyPair(pair)
            .setTid(String.valueOf(id))
            .setMatchId(String.valueOf(matchId))
            .setOid(String.valueOf(orderId))
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(createTimestamp))
            .setSide(side)
            .setPrice(price)
            .setQuantity(filledAmount)
            .setTotal(MathUtils.multiply(price, filledAmount))
            .setFee(filledFees)
            .setFeeCurrency(feeCurrency);
    }
}
