package cttk.impl.bithumb;

import cttk.CXIds;
import cttk.impl.AbstractOrderVerifierTest;

public class BithumbOrderVerifierTest
    extends AbstractOrderVerifierTest
{
    @Override
    public String getCxId() {
        return CXIds.BITHUMB;
    }

    @Override
    protected long getThreadSleepMillis() {
        return 3000L;
    }
}
