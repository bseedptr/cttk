package cttk.impl.binance.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class BinanceResultResponse {
    Boolean success;
    @JsonProperty("msg")
    String message;
}
