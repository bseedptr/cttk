package cttk.impl.binance.stream;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Trades;
import cttk.impl.binance.stream.response.BinanceWebsocketOrderBookResponse;
import cttk.impl.binance.stream.response.BinanceWebsocketTickerResponse;
import cttk.impl.binance.stream.response.BinanceWebsocketTradeResponse;
import cttk.impl.stream.MessageConverter;

public class BinanceWebsocketMessageConverterFactory {

    private static final ObjectMapper createObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper;
    }

    public static MessageConverter<Ticker> createTickerMessageConverter() {
        return new MessageConverter<Ticker>() {
            private final ObjectMapper mapper = createObjectMapper();

            @Override
            public Ticker convert(String message)
                throws Exception
            {
                return mapper.readValue(message, BinanceWebsocketTickerResponse.class).toTicker();
            }
        };
    }

    public static MessageConverter<OrderBook> createOrderBookMessageConverter() {
        return new MessageConverter<OrderBook>() {
            private final ObjectMapper mapper = createObjectMapper();

            @Override
            public OrderBook convert(String message)
                throws Exception
            {
                return mapper.readValue(message, BinanceWebsocketOrderBookResponse.class).toOrderBook();
            }
        };
    }

    public static MessageConverter<Trades> createTradesMessageConverter() {
        return new MessageConverter<Trades>() {
            private final ObjectMapper mapper = createObjectMapper();

            @Override
            public Trades convert(String message)
                throws Exception
            {
                return mapper.readValue(message, BinanceWebsocketTradeResponse.class).toTrades();
            }
        };
    }
}
