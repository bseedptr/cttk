package cttk.impl.upbit;

import static cttk.impl.util.Objects.nvl;

import java.math.BigDecimal;

import cttk.CurrencyPair;
import cttk.TradingFeeProvider;
import cttk.dto.TradingFee;
import cttk.exception.CTTKException;

public class UpbitTradingFeeProvider
    implements TradingFeeProvider
{
    @Override
    public TradingFee getFee(CurrencyPair currencyPair)
        throws CTTKException
    {
        if (currencyPair == null || currencyPair.getQuoteCurrency() == null) return null;
        switch (currencyPair.getQuoteCurrency().trim().toUpperCase()) {
            case "KRW":
                return new TradingFee(
                    nvl(getTradingFeeRate("krw"), new BigDecimal("0.0005")),
                    nvl(getTradingFeeRate("krw"), new BigDecimal("0.0005")));
            case "BTC":
                return new TradingFee(
                    nvl(getTradingFeeRate("btc"), new BigDecimal("0.0025")),
                    nvl(getTradingFeeRate("btc"), new BigDecimal("0.0025")));
            case "ETH":
                return new TradingFee(
                    nvl(getTradingFeeRate("eth"), new BigDecimal("0.0025")),
                    nvl(getTradingFeeRate("eth"), new BigDecimal("0.0025")));
            case "USDT":
                return new TradingFee(
                    nvl(getTradingFeeRate("usdt"), new BigDecimal("0.0025")),
                    nvl(getTradingFeeRate("usdt"), new BigDecimal("0.0025")));
            default:
                return new TradingFee(new BigDecimal("0.0025"), new BigDecimal("0.0025"));
        }
    }

    private static BigDecimal getTradingFeeRate(String quoteCurrency) {
        final String val = System.getProperty("cttk.upbit.trading-fee-rate." + quoteCurrency.toLowerCase());
        return val == null ? null : new BigDecimal(val);
    }
}
