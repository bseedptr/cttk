package cttk.impl.hitbtc.stream;

public class HitbtcChannels {
    public static final String getTickerChannel() {
        return "subscribeTicker";
    }

    public static final String getOrderBookChannel() {
        return "subscribeOrderbook";
    }

    public static final String getTradeChannel() {
        return "subscribeTrades";
    }
}
