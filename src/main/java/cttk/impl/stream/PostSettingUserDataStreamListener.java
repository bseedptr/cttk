package cttk.impl.stream;

import cttk.AbstractCXIdHolder;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.UserData;

public class PostSettingUserDataStreamListener
    implements CXStreamListener<UserData>
{
    final String cxId;
    final CXStreamListener<UserData> listener;

    public PostSettingUserDataStreamListener(
        final String cxId,
        final CXStreamListener<UserData> listener)
    {
        this.cxId = cxId;
        this.listener = listener;
    }

    @Override
    public void onOpen(CXStream<UserData> stream) {
        listener.onOpen(stream);
    }

    @Override
    public void onMessage(UserData message) {
        if (message != null && message.getData() instanceof AbstractCXIdHolder) {
            ((AbstractCXIdHolder<?>) message.getData()).setCxId(cxId);
        }
        listener.onMessage(message);
    }

    @Override
    public void onClosing(CXStream<UserData> stream, int code, String reason) {
        listener.onClosing(stream, code, reason);
    }

    @Override
    public void onClosed(CXStream<UserData> stream, int code, String reason) {
        listener.onClosed(stream, code, reason);
    }

    @Override
    public void onFailure(CXStream<UserData> stream, Throwable t) {
        listener.onFailure(stream, t);
    }
}
