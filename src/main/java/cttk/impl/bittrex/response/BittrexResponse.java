package cttk.impl.bittrex.response;

import lombok.Data;

@Data
public class BittrexResponse<T> {
    String success;
    String message;
    T result;
}
