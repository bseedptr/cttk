package cttk.impl.upbit.stream;

public class UpbitChannels {
    public static final String getTickerChannel() {
        return "ticker";
    }

    public static final String getOrderBookChannel() {
        return "orderbook";
    }

    public static final String getTradeChannel() {
        return "trade";
    }
}
