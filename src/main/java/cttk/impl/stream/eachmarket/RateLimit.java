package cttk.impl.stream.eachmarket;

import static cttk.CXIds.BITHUMB;
import static cttk.CXIds.COINONE;
import static cttk.CXIds.HUOBI;
import static cttk.CXIds.KORBIT;

import cttk.exception.CTTKException;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class RateLimit {
    private final int maxRequestCount;
    private final int maxRequestIntervalMillis;

    public static RateLimit createDefault(String cxId)
        throws CTTKException
    {
        switch (cxId) {
            case BITHUMB:
                return new RateLimit(6, 1000);
            case COINONE:
                return new RateLimit(3, 1000);
            case HUOBI:
                return new RateLimit(3, 1000);
            case KORBIT:
                return new RateLimit(6, 1000);
            default:
                throw new CTTKException("Default rate limit is not defined.").setCxId(cxId);
        }
    }

    /**
     * For example, (10, 1000, 100) means allow max 10 requests per 1000ms and buffered sleep time is 100 ms.
     * @param maxRequestCount the max request count
     * @param maxRequestIntervalMillis the max request interval millis
     */
    public RateLimit(int maxRequestCount, int maxRequestIntervalMillis) {
        this.maxRequestCount = maxRequestCount;
        this.maxRequestIntervalMillis = maxRequestIntervalMillis;
    }
}
