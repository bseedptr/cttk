package cttk.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import cttk.OrderStatus;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.impl.util.MathUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
public class UserOrdersDiff {
    final List<UserOrder> added;
    final List<UserOrder> removed;
    final List<UserOrder> valueChanged;
    @Setter List<UserOrder> closed;

    private UserOrdersDiff(List<UserOrder> added, List<UserOrder> removed, List<UserOrder> valueChanged) {
        super();
        this.added = added;
        this.removed = removed;
        this.valueChanged = valueChanged;
    }

    public boolean isChanged() {
        return isAdded() || isRemoved() || isValueChanged();
    }

    public boolean isAdded() {
        return !added.isEmpty();
    }

    public boolean isRemoved() {
        return !removed.isEmpty();
    }

    public boolean isValueChanged() {
        return !valueChanged.isEmpty();
    }

    public boolean existClosed() {
        return closed != null && !closed.isEmpty();
    }

    public List<UserOrder> getTradedOrders() {
        return closed == null ? Collections.emptyList() : closed.stream().filter(userOrder -> userOrder.getStatusType() != OrderStatus.CANCELED).collect(Collectors.toList());
    }

    public static UserOrdersDiff diff(UserOrders prev, UserOrders curr) {
        return diff(prev == null ? null : prev.getOrders(), curr == null ? null : curr.getOrders());
    }

    public static UserOrdersDiff diff(List<UserOrder> prev, List<UserOrder> curr) {
        final Map<String, UserOrder> prevMap = toMap(prev);
        final Map<String, UserOrder> currMap = toMap(curr);

        final List<UserOrder> added = new ArrayList<>();
        final List<UserOrder> removed = new ArrayList<>();
        final List<UserOrder> valueChanged = new ArrayList<>();

        prevMap.forEach((k, v) -> {
            if (!currMap.containsKey(k)) removed.add(v);
        });

        currMap.forEach((k, v) -> {
            if (!prevMap.containsKey(k)) {
                added.add(v);
            } else if (isDiff(v, prevMap.get(k))) {
                valueChanged.add(v);
            }
        });

        return new UserOrdersDiff(added, removed, valueChanged);
    }

    private static boolean isDiff(UserOrder o1, UserOrder o2) {
        return o1.getStatusType() != o2.getStatusType()
            || !MathUtils.equals(o1.getFilledQuantity(), o2.getFilledQuantity());
    }

    private static Map<String, UserOrder> toMap(List<UserOrder> prev) {
        return prev == null
            ? Collections.emptyMap()
            : prev.stream().collect(Collectors.toMap(i -> i.getOid(), i -> i));
    }
}
