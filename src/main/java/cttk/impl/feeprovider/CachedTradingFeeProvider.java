package cttk.impl.feeprovider;

import java.util.HashMap;
import java.util.Map;

import cttk.AbstractCXIdHolder;
import cttk.CurrencyPair;
import cttk.TradingFeeProvider;
import cttk.dto.TradingFee;
import cttk.exception.CTTKException;
import lombok.ToString;

@ToString(callSuper = true)
public class CachedTradingFeeProvider
    extends AbstractCXIdHolder<CachedTradingFeeProvider>
    implements TradingFeeProvider
{
    final TradingFeeProvider tradingFeeProvider;

    final Map<String, TradingFee> cache = new HashMap<>();

    public CachedTradingFeeProvider(TradingFeeProvider tradingFeeProvider) {
        super();
        this.tradingFeeProvider = tradingFeeProvider;
    }

    @Override
    public TradingFee getFee(CurrencyPair currencyPair)
        throws CTTKException
    {
        if (currencyPair == null) return null;
        final String symbol = currencyPair.getPairString();
        if (!cache.containsKey(symbol)) {
            cache.put(symbol, tradingFeeProvider.getFee(currencyPair));
        }
        return cache.get(symbol);
    }

    public void clearCache() {
        this.cache.clear();
    }
}
