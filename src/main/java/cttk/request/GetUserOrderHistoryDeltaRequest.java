package cttk.request;

import cttk.DurationGettable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class GetUserOrderHistoryDeltaRequest
    extends AbstractGetDeltaRequest<GetUserOrderHistoryDeltaRequest>
    implements DurationGettable
{
}
