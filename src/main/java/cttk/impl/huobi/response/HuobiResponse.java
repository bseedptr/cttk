package cttk.impl.huobi.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class HuobiResponse {
    String ch;
    String status;
    @JsonProperty("err-code")
    String errorCode;
    @JsonProperty("err-msg")
    String errorMessage;
    @JsonProperty("ts")
    Long timestamp;
}
