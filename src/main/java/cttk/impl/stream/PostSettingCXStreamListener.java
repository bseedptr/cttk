package cttk.impl.stream;

import cttk.AbstractCurrencyPair;
import cttk.CXIdGettable;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.CurrencyPair;
import cttk.CurrencyPairSetter;
import cttk.DateTimeHolder;
import cttk.DateTimeSetter;

public class PostSettingCXStreamListener<T extends AbstractCurrencyPair<T>>
    implements CXIdGettable, CurrencyPairSetter, CXStreamListener<T>, DateTimeSetter
{
    final String cxId;
    final CurrencyPair currencyPair;
    final CXStreamListener<T> listener;

    public PostSettingCXStreamListener(
        final String cxId,
        final CurrencyPair currencyPair,
        final CXStreamListener<T> listener)
    {
        this.cxId = cxId;
        this.currencyPair = currencyPair;
        this.listener = listener;
    }

    @Override
    public String getCxId() {
        return cxId;
    }

    @Override
    public void onOpen(CXStream<T> stream) {
        listener.onOpen(stream);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public void onMessage(T message) {
        if (message != null
            && message instanceof DateTimeHolder
            && ((DateTimeHolder) message).getDateTime() == null)
        {
            setDateTime((DateTimeHolder) message);
        }
        listener.onMessage(setCxIdAndCurrencyPair(message, currencyPair));
    }

    @Override
    public void onClosing(CXStream<T> stream, int code, String reason) {
        listener.onClosing(stream, code, reason);
    }

    @Override
    public void onClosed(CXStream<T> stream, int code, String reason) {
        listener.onClosed(stream, code, reason);
    }

    @Override
    public void onFailure(CXStream<T> stream, Throwable t) {
        listener.onFailure(stream, t);
    }
}