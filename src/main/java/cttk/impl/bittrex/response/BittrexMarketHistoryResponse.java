package cttk.impl.bittrex.response;

import java.util.List;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.dto.Trade;
import cttk.dto.Trades;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BittrexMarketHistoryResponse
    extends BittrexResponse<List<BittrexMarketHistory>>
{
    public Trades toTrades(CurrencyPair pair) {
        return new Trades()
            .setCurrencyPair(pair)
            .setTrades(getTradeList());
    }

    private List<Trade> getTradeList() {
        if (result == null) return null;

        return result.stream()
            .map(i -> i.toTrade())
            .collect(Collectors.toList());
    }
}