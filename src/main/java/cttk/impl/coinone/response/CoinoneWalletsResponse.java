package cttk.impl.coinone.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import cttk.dto.Wallet;
import cttk.dto.Wallets;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CoinoneWalletsResponse
    extends CoinoneResponse
{
    CoinoneWalletAddress walletAddress;

    public Wallets toWallets() {
        List<Wallet> list = walletAddress.map.entrySet().stream()
            .map(e -> new Wallet().setCurrency(e.getKey()).setAddress(e.getValue()))
            .collect(Collectors.toList());

        return new Wallets().setWallets(list);
    }
}

@Data
class CoinoneWalletAddress {

    public Map<String, String> map = new HashMap<>();

    @JsonAnyGetter
    public Map<String, String> any() {
        return this.map;
    }

    @JsonAnySetter
    public void set(String name, String value) {
        this.map.put(name, value);
    }
}