package cttk.example;

import cttk.CXClientFactory;
import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.UserData;
import cttk.auth.CXCredentialsProvider;
import cttk.request.OpenUserDataStreamRequest;

public class UserDataStreamExample {
    public static void main(String[] args)
        throws Exception
    {
        // You can set cxId as BINANCE, BITFINEX, HITBTC, KRAKEN and POLONIEX for now.
        // In case of Binance, Bitfinex, Hitbtc interval value will be ignored.
        // On the other hand if one of BITHUMB, COINONE, HUOBI or KORBIT each market polling version will be opened.
        final String cxId = CXIds.BITFINEX;
        final CXStream<UserData> userDataStream = CXClientFactory
            .createUserDataStreamOpener(cxId, CXCredentialsProvider.getDefaultCredential(cxId).getDataKey())
            .openUserDataStream(new OpenUserDataStreamRequest().setIntervalInMilli(5000l), CXStreamListener.createPrint());

        Thread.sleep(600000l);
        userDataStream.close();
    }
}
