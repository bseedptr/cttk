package cttk.impl.upbit.stream;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import cttk.CurrencyPair;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Trades;
import cttk.impl.stream.MessageConverter;
import cttk.impl.upbit.stream.response.UpbitWebsocketOrderBookResponse;
import cttk.impl.upbit.stream.response.UpbitWebsocketTickerResponse;
import cttk.impl.upbit.stream.response.UpbitWebsocketTradeResponse;

public class UpbitWebsocketMessageConverterFactory {

    private static final ObjectMapper createObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper;
    }

    public static MessageConverter<Ticker> createTickerMessageConverter(CurrencyPair currencyPair) {
        return new MessageConverter<Ticker>() {
            private final ObjectMapper mapper = createObjectMapper();

            @Override
            public Ticker convert(String message)
                throws Exception
            {
                return mapper.readValue(message, UpbitWebsocketTickerResponse.class).toTicker(currencyPair);
            }
        };
    }

    public static MessageConverter<OrderBook> createOrderBookMessageConverter(CurrencyPair currencyPair) {
        return new MessageConverter<OrderBook>() {
            private final ObjectMapper mapper = createObjectMapper();

            @Override
            public OrderBook convert(String message)
                throws Exception
            {
                return mapper.readValue(message, UpbitWebsocketOrderBookResponse.class).toOrderBook(currencyPair);
            }
        };
    }

    public static MessageConverter<Trades> createTradesMessageConverter(CurrencyPair currencyPair) {
        return new MessageConverter<Trades>() {
            private final ObjectMapper mapper = createObjectMapper();

            @Override
            public Trades convert(String message)
                throws Exception
            {
                return mapper.readValue(message, UpbitWebsocketTradeResponse.class).toTrades(currencyPair);
            }
        };
    }
}
