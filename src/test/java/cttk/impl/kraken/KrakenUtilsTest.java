package cttk.impl.kraken;

import org.junit.Assert;
import org.junit.Test;

import cttk.CurrencyPair;
import cttk.CurrencyPair.SimpleCurrencyPair;

public class KrakenUtilsTest {
    @Test
    public void testToCurrencyPair() {
        CurrencyPair cp = KrakenUtils.parseMarketSymbol("DASHZUSD");
        Assert.assertEquals("DASH", cp.getBaseCurrency());
        Assert.assertEquals("USD", cp.getQuoteCurrency());

        cp = KrakenUtils.parseMarketSymbol("EOSETH");
        Assert.assertEquals("EOS", cp.getBaseCurrency());
        Assert.assertEquals("ETH", cp.getQuoteCurrency());
    }

    @Test
    public void testGuessFeeCurrencyAlwaysReturnQuoteCurrency() {
        SimpleCurrencyPair EOS_ETH = new SimpleCurrencyPair("EOS", "ETH");
        SimpleCurrencyPair QTUM_ETH = new SimpleCurrencyPair("QTUM", "ETH");
        SimpleCurrencyPair BTC_ETH = new SimpleCurrencyPair("BTC", "ETH");

        Assert.assertEquals("Should be 'ETH' regardless of base currency.", EOS_ETH.getQuoteCurrency(), KrakenUtils.guessFeeCurrency(EOS_ETH));
        Assert.assertEquals("Should be 'ETH' regardless of base currency.", QTUM_ETH.getQuoteCurrency(), KrakenUtils.guessFeeCurrency(QTUM_ETH));
        Assert.assertEquals("Should be 'ETH' regardless of base currency.", BTC_ETH.getQuoteCurrency(), KrakenUtils.guessFeeCurrency(BTC_ETH));
    }

    @Test
    public void testCheckOrderIDFormat() {
        String validOrderId = "OELDKX-TKJWU-SHVD4P";
        Assert.assertTrue(KrakenUtils.checkOrderIDFormat(validOrderId));

        String invalidOrderId = "1111111111";
        Assert.assertFalse(KrakenUtils.checkOrderIDFormat(null));
        Assert.assertFalse(KrakenUtils.checkOrderIDFormat(invalidOrderId));
    }
}
