package cttk.impl.bibox.stream;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.UserData;
import cttk.auth.Credential;
import cttk.exception.CTTKException;
import cttk.impl.bibox.BiboxUtils;
import cttk.impl.bibox.stream.response.BiboxWebsocketUserDataResponse;
import cttk.impl.stream.AbstractWebsocketListener;
import cttk.impl.util.CryptoUtils;
import cttk.impl.util.JsonUtils;
import cttk.impl.util.OParamsBuilder;
import cttk.request.OpenUserDataStreamRequest;
import cttk.util.StringUtils;
import okhttp3.Response;
import okhttp3.WebSocket;
import okio.ByteString;

public class BiboxUserDataWebsocketListener
    extends AbstractWebsocketListener<UserData, OpenUserDataStreamRequest>
{
    private final ObjectMapper objectMapper = JsonUtils.createDefaultObjectMapper();
    private Credential credential;

    public BiboxUserDataWebsocketListener(
        final CXStream<UserData> stream,
        final CXStreamListener<UserData> listener)
    {
        super(stream,
            BiboxChannels.getUserDataChannel(),
            null, // ignore CurrencyPair
            null, // ignore converter
            listener,
            CXIds.BIBOX);
    }

    public BiboxUserDataWebsocketListener setCredential(Credential credential) {
        this.credential = credential;
        return this;
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response)
    {
        webSocket.send(getLoginPayload());
        logger.info("[{}] Open : {}", id, channel);
        this.listener.onOpen(stream);
    }

    @Override
    public void onClosed(WebSocket webSocket, int code, String reason) {
        logger.info("[{}] Closed: - {} {} {}", id, channel, code, reason);
        this.listener.onClosed(stream, code, reason);
    }

    @Override
    protected void handleMessage(WebSocket webSocket, String text)
        throws Exception
    {
        if (!StringUtils.isEmpty(text) && text.contains("{\"ping\"")) {
            webSocket.send(text.replace("\"ping\"", "\"pong\""));
            return;
        }

        BiboxUtils.checkStreamErrorByResponseMessage(request, text);
        List<UserData> userDataList = convert(text);
        if (!userDataList.isEmpty()) {
            userDataList.forEach(listener::onMessage);
        }
    }

    @Override
    protected String toString(ByteString bytes) {
        return bytes == null ? null : bytes.hex();
    }

    private List<UserData> convert(String message)
        throws IOException, CTTKException
    {
        return objectMapper.readValue(message, BiboxWebsocketUserDataResponse.class).toUserDataList();
    }

    private String getLoginPayload() {
        final String msg = String.format("{\"apikey\":\"%s\",\"channel\":\"bibox_sub_spot_ALL_ALL_login\",\"event\":\"addChannel\"}", credential.getAccessKey());
        final String sign = CryptoUtils.hmacMD5(msg, credential.getSecretKey());

        final Map<String, Object> authParams = OParamsBuilder.create()
            .put("apikey", credential.getAccessKey())
            .put("channel", channel)
            .put("event", "addChannel")
            .put("sign", sign)
            .build();
        try {
            return objectMapper.writeValueAsString(authParams);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
