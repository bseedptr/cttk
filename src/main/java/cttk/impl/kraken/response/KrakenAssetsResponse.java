package cttk.impl.kraken.response;

import static cttk.impl.kraken.KrakenUtils.toKrakenCurrencySymbol;
import static cttk.impl.kraken.KrakenUtils.toStdCurrencySymbol;

import java.util.Map;
import java.util.stream.Collectors;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class KrakenAssetsResponse
    extends KrakenResponse<Map<String, KrakenAsset>>
{
    public Map<String, String> getKrakenToStdCurrencySymbolMap() {
        return this.result.entrySet().stream().collect(Collectors.toMap(e -> e.getKey(), e -> toStdCurrencySymbol(e.getValue().altname)));
    }

    public Map<String, String> getStdToKrakenCurrencySymbolMap() {
        return this.result.entrySet().stream().collect(Collectors.toMap(e -> toKrakenCurrencySymbol(e.getValue().altname), e -> e.getKey()));
    }
}

@Data
class KrakenAsset {
    String aclass;
    String altname;
    Integer decimals;
    Integer display_decimals;
}