package cttk.impl.binance.response;

import java.math.BigDecimal;
import java.math.BigInteger;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.OrderSide;
import cttk.dto.Trade;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;

@Data
public class BinanceAggTradeResponse {
    @JsonProperty("a")
    Long tradeId;         // Aggregate tradeId
    @JsonProperty("p")
    BigDecimal price;   // price
    @JsonProperty("q")
    BigDecimal quantity;   // quantity
    @JsonProperty("f")
    Long firstTradeId;         // first trade id
    @JsonProperty("l")
    Long lastTradeId;         // last trade id
    @JsonProperty("T")
    Long timestamp;         // Timestamp
    @JsonProperty("m")
    Boolean isBuyerMaker;      // was the buyer the maker?
    @JsonProperty("M")
    Boolean isBestMatch;      // was the trade the best price match

    public Trade toTrade() {
        return new Trade()
            .setSno(BigInteger.valueOf(lastTradeId))
            .setTid(String.valueOf(tradeId))
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(timestamp))
            .setPrice(price)
            .setQuantity(quantity)
            .setSide(isBuyerMaker ? OrderSide.BUY : OrderSide.SELL)
            .setTotal(MathUtils.multiply(price, quantity));
    }
}
