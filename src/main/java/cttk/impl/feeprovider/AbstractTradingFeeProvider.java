package cttk.impl.feeprovider;

import java.math.BigDecimal;

import cttk.AbstractCXIdHolder;
import cttk.TradingFeeProvider;
import cttk.dto.TradingFee;
import cttk.util.StringUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public abstract class AbstractTradingFeeProvider<T extends AbstractTradingFeeProvider<T>>
    extends AbstractCXIdHolder<T>
    implements TradingFeeProvider
{
    protected TradingFee fee;

    @SuppressWarnings("unchecked")
    public T setFee(TradingFee fee) {
        this.fee = fee;
        return (T) this;
    }

    @SuppressWarnings("unchecked")
    public T setFee(BigDecimal takerFee, BigDecimal makerFee) {
        this.fee = new TradingFee(takerFee, makerFee);
        return (T) this;
    }

    @SuppressWarnings("unchecked")
    public T setFee(String takerFee, String makerFee) {
        this.fee = new TradingFee(StringUtils.parseBigDecimal(takerFee), StringUtils.parseBigDecimal(makerFee));
        return (T) this;
    }

    @SuppressWarnings("unchecked")
    public T setFee(BigDecimal fee) {
        this.fee = new TradingFee().setFees(fee);
        return (T) this;
    }

    @SuppressWarnings("unchecked")
    public T setFee(String fee) {
        this.fee = new TradingFee().setFees(fee);
        return (T) this;
    }
}
