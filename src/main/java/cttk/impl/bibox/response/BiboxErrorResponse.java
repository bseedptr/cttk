package cttk.impl.bibox.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BiboxErrorResponse
    extends BiboxResultResponse<BiboxError>
{
}

@Data
class BiboxError {
    String code;
    String msg;
}