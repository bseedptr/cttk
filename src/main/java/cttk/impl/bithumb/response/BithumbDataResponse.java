package cttk.impl.bithumb.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BithumbDataResponse<T>
    extends BithumbStatusResponse
{
    protected T data;
}
