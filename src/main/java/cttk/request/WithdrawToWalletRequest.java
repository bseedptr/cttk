package cttk.request;

import java.math.BigDecimal;

import cttk.AbstractCurrencyHolder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class WithdrawToWalletRequest
    extends AbstractCurrencyHolder<WithdrawToWalletRequest>
{
    BigDecimal quantity;
    String address;
    String destinationTag;
    String paymentId;
    String feePriority;
    String withdrawlKey; // for kraken
    String password; //for okex, bibox
    String totpCode; //for bibox

    public WithdrawToWalletRequest setQuantity(String quantity) {
        this.quantity = new BigDecimal(quantity);
        return this;
    }

    public WithdrawToWalletRequest setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
        return this;
    }

    public boolean hasWithdrawlKey() {
        return withdrawlKey != null && !withdrawlKey.trim().isEmpty();
    }
}
