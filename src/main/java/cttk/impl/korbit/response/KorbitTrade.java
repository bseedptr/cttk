package cttk.impl.korbit.response;

import static cttk.impl.util.DateTimeUtils.zdtFromEpochMilli;

import java.math.BigDecimal;
import java.math.BigInteger;

import cttk.dto.Trade;
import cttk.impl.util.MathUtils;
import cttk.impl.util.ZoneIds;
import lombok.Data;

@Data
public class KorbitTrade {
    Long timestamp;
    BigInteger tid;
    BigDecimal price;
    BigDecimal amount;

    public Trade toTrade() {
        return new Trade()
            .setDateTime(zdtFromEpochMilli(timestamp, ZoneIds.SEOUL))
            .setSno(tid)
            .setTid(String.valueOf(tid))
            .setPrice(price)
            .setQuantity(amount)
            .setTotal(MathUtils.multiply(price, amount));
    }
}