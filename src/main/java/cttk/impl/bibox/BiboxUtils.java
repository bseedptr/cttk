package cttk.impl.bibox;

import static cttk.OrderStatus.CANCELED;
import static cttk.OrderStatus.FILLED;
import static cttk.OrderStatus.PARTIALLY_FILLED;
import static cttk.OrderStatus.UNFILLED;
import static cttk.util.StringUtils.toUpper;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import cttk.AccountType;
import cttk.CXIds;
import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.PricingType;
import cttk.TransferTypeParam;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXAuthorityException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.util.CryptoUtils;
import cttk.impl.util.JsonUtils;
import cttk.util.StringUtils;

/**
 * Provides Bibox specific utility functions
 * @author HyunSu Lee
 * @since 2018-08-13
 */
public class BiboxUtils {
    public static ObjectMapper objectMapper = JsonUtils.createDefaultObjectMapper();
    final static String cxId = CXIds.BIBOX;

    public static String getMarketSymbol(CurrencyPair currencyPair) {
        return currencyPair == null ? null
            : toUpper(currencyPair.getBaseCurrency()) + "_" + toUpper(currencyPair.getQuoteCurrency());
    }

    public static OrderSide parseOrderSide(String side) {
        if (side == null) return null;
        switch (toUpper(side)) {
            case "2":
                return OrderSide.SELL;
            case "1":
                return OrderSide.BUY;
            default:
                return OrderSide.of(side);
        }
    }

    public static CurrencyPair parseMarketSymbol(String market) {
        final String[] arr = market.split("_");
        return CurrencyPair.of(toUpper(arr[0]), toUpper(arr[1]));
    }

    public static OrderSide toOrderSide(Integer sideCode) {
        if (sideCode == null) return null;
        switch (sideCode) {
            case 2:
                return OrderSide.SELL;
            case 1:
                return OrderSide.BUY;
            default:
                return null;
        }
    }

    public static Integer toOrderSideCode(OrderSide side) {
        if (side == null) return null;
        switch (side) {
            case BUY:
                return 1;
            case SELL:
                return 2;
            default:
                return null;
        }
    }

    public static Integer toOrderTypeCode(PricingType type) {
        if (type == null) return null;
        switch (type) {
            case MARKET:
                return 1;
            case LIMIT:
                return 2;
            default:
                return null;
        }
    }

    public static Integer toFilterTypeCode(TransferTypeParam param) {
        if (param == null) return null;
        switch (param) {
            case ALL:
                return 0;
            case IN_PROGRESS:
                return 1;
            case COMPLETED:
                return 2;
            case FAILED:
                return 3;
            default:
                return null;
        }
    }

    public static Integer toAccountTypeCode(AccountType type) {
        if (type == null) return null;
        switch (type) {
            case REGULAR:
                return 0;
            case MARGIN:
                return 1;
            default:
                return null;
        }
    }

    public static int toPayBixCode(boolean useBix) {
        return useBix ? 1 : 0;
    }

    public static String parseAccountType(Integer code) {
        if (code == null) return null;
        switch (code) {
            case 0:
                return "regular";
            case 1:
                return "margin";
            default:
                throw new RuntimeException("Not supported code [" + code + "] (valid: 0, 1).");
        }
    }

    public static String parseOrderType(Integer code) {
        if (code == null) return null;
        switch (code) {
            case 1:
                return "market";
            case 2:
                return "limit";
            default:
                throw new RuntimeException("Not supported code [" + code + "] (valid: 1, 2).");
        }
    }

    public static String parseOrderStatus(Integer code) {
        if (code == null) return null;
        switch (code) {
            case 1:
                return "pending";
            case 2:
                return "partCompleted";
            case 3:
                return "completed";
            case 4:
                return "partCanceled";
            case 5:
                return "canceled";
            case 6:
                return "canceling";
            default:
                throw new RuntimeException("Not supported code [" + code + "] (valid: 1 ~ 6).");
        }
    }

    public static OrderStatus parseOrderStatusType(String status) {
        if (status == null) return null;
        switch (status) {
            case "pending":
                return UNFILLED;
            case "partCompleted":
                return PARTIALLY_FILLED;
            case "completed":
                return FILLED;
            case "canceling":
            case "partCanceled":
            case "canceled":
                return CANCELED;
            default:
                throw new RuntimeException("Not supported order status(valid: pending, partCompleted, completed, canceling, partCanceled, canceled).");
        }
    }

    public static String parseWithdrawStatus(Integer code) {
        if (code == null) return null;
        switch (code) {
            case -2:
                return "auditFailed";
            case -1:
                return "userTerminated";
            case 0:
                return "pendingReview";
            case 1:
                return "accepted";
            case 2:
                return "inTheCurrency";
            case 3:
                return "coinCompleted";
            default:
                throw new RuntimeException("Not supported code(valid: -2 ~ 3).");
        }
    }

    public static boolean parsePayBix(Integer code) {
        return code == 1;
    }

    public static BigDecimal parsePercentString(final String str) {
        if (str == null) return null;
        if (str.endsWith("%")) {
            return StringUtils.parseBigDecimal(str.substring(0, str.length() - 1));
        } else {
            return StringUtils.parseBigDecimal(str);
        }
    }

    public static String decodeWebsocketData(String data)
        throws IOException
    {
        return CryptoUtils.decodeGzip(CryptoUtils.decodeBase64(data));
    }

    /**
     * Bibox send error response with {error:...} or {result:{error:...}} or {result:[...],error:{...}} currently.
     * We expect to unified in one format in the near future.
     *
     * @since 2018.10.31
     */
    public static Map<String, Object> parseResponseError(ObjectMapper mapper, final String json)
        throws IOException
    {
        Map<String, Map<String, Object>> map = mapper.readValue(json, new TypeReference<Map<String, Object>>() {});
        if (map.containsKey("error")) {
            return map.get("error");
        } else {
            return (Map<String, Object>) map.get("result").get("error");
        }
    }

    /**
     *
     * @param requestObj
     * @param body
     * @throws CTTKException
     * @see <a href="https://github.com/Biboxcom/API_Docs_en/wiki/WS_error_code">WS_error_code</a>
     */
    public static void checkStreamErrorByResponseMessage(Object requestObj, String body)
        throws CTTKException
    {
        if (!body.contains("\"error\"")) return;

        final String exchangeErrorCode;
        final String exchangeMessage;
        try {
            final Map<String, Object> error = BiboxUtils.parseResponseError(objectMapper, body);
            exchangeErrorCode = (String) error.get("code");
            exchangeMessage = (String) error.get("msg");
        } catch (IOException e) {
            throw CTTKException.create("json parse error\n" + body, e, cxId);
        }

        switch (exchangeErrorCode) {
            case "2003": // Cookie expired
                throw new CXAuthorityException()
                    .setCxId(cxId)
                    .setExchangeMessage(exchangeMessage)
                    .setExchangeErrorCode(exchangeErrorCode);
            case "3009": // Illegal subscription channel
                if (requestObj instanceof CurrencyPair) {
                    CurrencyPair request = (CurrencyPair) requestObj;
                    throw new UnsupportedCurrencyPairException()
                        .setCxId(cxId)
                        .setExchangeMessage(body)
                        .setExchangeMessage(exchangeErrorCode)
                        .setCurrencyPair(request);
                }
                break;
            case "3017": // Illegal subscribe event
            case "3010": // websocket connection error
            case "4003": // The server is busy, please try again later
            default:
                break;
        }
        throw new CXAPIRequestException()
            .setCxId(cxId)
            .setExchangeMessage(body)
            .setExchangeErrorCode(exchangeErrorCode);
    }
}
