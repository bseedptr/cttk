package cttk.impl.bitfinex.stream;

import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.UserData;
import cttk.UserDataStreamOpener;
import cttk.auth.Credential;
import cttk.exception.CTTKException;
import cttk.request.OpenUserDataStreamRequest;

public class BitfinexUserDataStreamOpener
    implements UserDataStreamOpener
{
    private final Credential credential;

    public BitfinexUserDataStreamOpener(Credential credential) {
        super();
        this.credential = credential;
    }

    @Override
    public CXStream<UserData> openUserDataStream(OpenUserDataStreamRequest request, CXStreamListener<UserData> listener)
        throws CTTKException
    {
        // there is no additional configuration needed to create stream except credential and listener
        return new BitfinexUserDataWebsocketStream(credential, listener);
    }
}
