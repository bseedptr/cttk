package cttk.impl.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

public class UriUtils {
    private static final String DEFAULT_CHAR_SET = "UTF-8";

    public static String toEncodedQueryParamsString(Map<String, String> map) {
        return map.entrySet().stream()
            .map(p -> encode(p.getKey()) + "=" + encode(p.getValue()))
            .reduce((p1, p2) -> p1 + "&" + p2)
            .orElse("");
    }

    public static String toSortedQueryParamsString(Map<String, String> map) {
        return map.entrySet().stream()
            .sorted(Map.Entry.comparingByKey())
            .map(p -> encode(p.getKey()) + "=" + encode(p.getValue()))
            .reduce((p1, p2) -> p1 + "&" + p2)
            .orElse("");
    }

    public static String encode(String value) {
        return encode(value, DEFAULT_CHAR_SET);
    }

    public static String encode(String value, String charSet) {
        String encoded;
        try {
            encoded = URLEncoder.encode(value, charSet);
        } catch (UnsupportedEncodingException e) {
            encoded = value;
        }
        return encoded;
    }
}
