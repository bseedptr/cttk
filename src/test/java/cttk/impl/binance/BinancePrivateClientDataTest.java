package cttk.impl.binance;

import static cttk.dto.UserOrders.Category.ALL;
import static cttk.test.TestUtils.hasTestProfile;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import cttk.CXIds;
import cttk.dto.UserAccount;
import cttk.dto.UserOrders;
import cttk.dto.Wallet;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.AbstractPrivateCXClientDataTest;
import cttk.request.GetUserAccountRequest;
import cttk.request.GetUserOrderHistoryRequest;

public class BinancePrivateClientDataTest
    extends AbstractPrivateCXClientDataTest
{
    @AfterClass
    public static void cleanup() {
        cleanup(BinancePrivateClientDataTest.class);
    }

    @Override
    public String getCxId() {
        return CXIds.BINANCE;
    }

    @Override
    public void testGetUserAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();

        UserAccount userAccount = client.getUserAccount(new GetUserAccountRequest());

        Assert.assertNotNull(userAccount);
        Assert.assertNotNull(userAccount.getUpdatedDateTime());
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetBankAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetBankAccount();
    }

    @Test
    public void testGetWallet()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();

        // Binance require trade key to get wallet address
        org.junit.Assume.assumeTrue(hasTestProfile("test-trade"));

        final String currency = getValidCurrencyPair().getQuoteCurrency();
        Wallet wallet = client.getWallet(currency);

        Assert.assertNotNull(wallet);
        Assert.assertNotNull(wallet.getAddress());
        Assert.assertEquals(currency, wallet.getCurrency());
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetAllWallets()
        throws CTTKException
    {
        super.testGetAllWallets();
    }

    @Test
    public void testGetUserOrderHistoryCategory()
        throws CTTKException
    {
        threadSleep();

        final GetUserOrderHistoryRequest request = new GetUserOrderHistoryRequest()
            .setCurrencyPair(getValidCurrencyPair());
        final UserOrders userOrders = client.getUserOrderHistory(request);

        logger.debug("{}", userOrders);

        Assert.assertEquals(userOrders.getCategory(), ALL);
    }
}
