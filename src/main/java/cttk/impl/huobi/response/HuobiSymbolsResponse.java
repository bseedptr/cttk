package cttk.impl.huobi.response;

import static cttk.impl.huobi.HuobiUtils.toStdCurrencySymbol;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.dto.MarketInfo;
import cttk.dto.MarketInfos;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HuobiSymbolsResponse
    extends HuobiResponse
{
    List<HuobiSymbol> data;

    public MarketInfos toMarketInfos() {
        return new MarketInfos().setMarketInfos(data.stream().map(i -> i.toMarketInfo()).collect(Collectors.toList()));
    }
}

@Data
class HuobiSymbol {
    @JsonProperty("base-currency")
    String baseCurrency;
    @JsonProperty("quote-currency")
    String quoteCurrency;
    @JsonProperty("price-precision")
    Integer pricePrecision;
    @JsonProperty("amount-precision")
    Integer amountPrecision;
    String symbol;

    public MarketInfo toMarketInfo() {
        return new MarketInfo()
            .setCurrencyPair(toStdCurrencySymbol(baseCurrency), toStdCurrencySymbol(quoteCurrency))
            .setPriceIncrement(BigDecimal.ONE.movePointLeft(pricePrecision))
            .setQuantityIncrement(BigDecimal.ONE.movePointLeft(amountPrecision));
    }
}