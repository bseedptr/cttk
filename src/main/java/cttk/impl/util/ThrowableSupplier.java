package cttk.impl.util;

import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;

public interface ThrowableSupplier<T> {
    T get()
        throws CTTKException, UnsupportedCurrencyPairException;
}
