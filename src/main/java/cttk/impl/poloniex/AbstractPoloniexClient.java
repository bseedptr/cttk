package cttk.impl.poloniex;

import java.io.IOException;
import java.util.Map;

import cttk.CXIds;
import cttk.exception.APILimitExceedException;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXAuthorityException;
import cttk.exception.OrderNotFoundException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.AbstractCXClient;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ThrowableFunction;

abstract class AbstractPoloniexClient
    extends AbstractCXClient
{
    @Override
    public String getCxId() {
        return CXIds.POLONIEX;
    }

    @Override
    protected void checkErrorByResponseMessage(Object requestObj, HttpResponse response)
        throws CTTKException
    {
        // 일반적으로 에러상황에 200번대 외의 status를 주지만, status가 200이고 result에 error가 들어있는 경우도 있다.
        final String body = response.getBody();
        final Integer statusCode = response.getStatus();

        String exchangeMessage;
        try {
            if (!body.contains("error")) return;
            Map<?, ?> map = objectMapper.readValue(body, Map.class);
            if (map.containsKey("error"))
                exchangeMessage = (String) map.get("error");
            else if (map.containsKey("result")) {
                Map<?, ?> mapInner = ((Map<?, ?>) map.get("result"));
                exchangeMessage = (String) mapInner.get("error");
            } else {
                throw new CXAPIRequestException("json parse error")
                    .setCxId(this.getCxId())
                    .setExchangeMessage(body)
                    .setResponseStatusCode(statusCode);
            }
        } catch (IOException e) {
            return;
        }
        if (exchangeMessage == null) return;

        if (exchangeMessage.contains("Invalid currencyPair")
            || exchangeMessage.contains("Unsupported currency pair")
            || exchangeMessage.contains("Invalid currency pair"))
        {
            throw CXAPIRequestException.createByRequestObj(requestObj, exchangeMessage)
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode);
        }
        // Nonce error, this is handled by RetryablepoloniexPrivateClient class
        else if (exchangeMessage.contains("Nonce must be greater than")) {
            throw new CXAPIRequestException(exchangeMessage)
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode);
        }
        // API limit
        else if (exchangeMessage.contains("API calls per second")) {
            throw new APILimitExceedException(exchangeMessage)
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode);
        }
        // API key error
        else if (exchangeMessage.contains("Invalid API key")) {
            throw new CXAuthorityException(exchangeMessage)
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode);
        } else if (exchangeMessage.contains("Invalid order number")
            || exchangeMessage.contains("Order not found"))
        {
            throw new OrderNotFoundException(exchangeMessage)
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode);
        }
        // Default fall back
        else {
            throw new CXAPIRequestException(exchangeMessage)
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode);
        }
        // New OrderStatusResponse error message
    }

    @Override
    protected <T, R> R convert(HttpResponse response, Class<T> claz, ThrowableFunction<T, R> mapper)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        try {
            return mapper.apply(objectMapper.readValue(response.getBody(), claz));
        } catch (IOException e) {
            throw new CXAPIRequestException("json parse error\n" + response.getBody(), e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }
}
