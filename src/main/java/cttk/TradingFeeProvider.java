package cttk;

import cttk.dto.TradingFee;
import cttk.exception.CTTKException;

public interface TradingFeeProvider {
    TradingFee getFee(CurrencyPair currencyPair)
        throws CTTKException;
}