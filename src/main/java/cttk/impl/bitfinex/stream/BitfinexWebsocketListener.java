package cttk.impl.bitfinex.stream;

import static cttk.impl.bitfinex.BitfinexUtils.getMarketSymbol;
import static cttk.impl.util.Objects.nvl;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.zip.GZIPInputStream;

import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.dto.OrderBook;
import cttk.exception.CTTKException;
import cttk.impl.bitfinex.BitfinexUtils;
import cttk.impl.stream.AbstractWebsocketListener;
import cttk.impl.stream.MessageConverter;
import cttk.request.AbstractCXStreamRequest;
import cttk.request.OpenOrderBookStreamRequest;
import okhttp3.Response;
import okhttp3.WebSocket;
import okio.ByteString;

public class BitfinexWebsocketListener<T, R extends AbstractCXStreamRequest<R>>
    extends AbstractWebsocketListener<T, R>
{
    private boolean isFirst = true;

    public BitfinexWebsocketListener(
        final CXStream<T> stream,
        final String channel,
        final R request,
        final MessageConverter<T> converter,
        final CXStreamListener<T> listener)
    {
        super(stream, channel, request, converter, listener, CXIds.BITFINEX);
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        webSocket.send(getSubscriptionPayload());
        logger.info("[{}] Open : {}", id, channel);
        this.listener.onOpen(stream);
    }

    public String getSubscriptionPayload() {
        if (request instanceof OpenOrderBookStreamRequest) {
            OpenOrderBookStreamRequest openOrderBookStreamRequest = (OpenOrderBookStreamRequest) request;
            final String prec = openOrderBookStreamRequest.getPrecision() == null ? "P0" : "P" + openOrderBookStreamRequest.getPrecision();
            final Integer len = nvl(openOrderBookStreamRequest.getCount(), 100);
            return "{\"event\": \"subscribe\", \"channel\": \"" + channel + "\", \"pair\": \"" + getMarketSymbol(request) + "\", \"prec\": \"" + prec + "\", \"len\": " + len + "}";
        } else {
            return "{\"event\": \"subscribe\", \"channel\": \"" + channel + "\", \"pair\": \"" + getMarketSymbol(request) + "\"}";
        }
    }

    @Override
    protected void handleMessage(WebSocket webSocket, String text)
        throws Exception, CTTKException
    {
        BitfinexUtils.checkErrorByResponseMessage(request, text, null);
        if (text.contains("\"event\":\"info\"")) {
            logger.info("[{}] Info: channel: {}, msg: {}", id, channel, text);
        } else if (text.contains("\"event\":\"error\"")) {
            logger.info("[{}] Error: channel: {}, msg: {}", id, channel, text);
        } else if (text.contains("\"event\":\"subscribed\"")) {
            logger.info("[{}] Subscribed: {}", id, channel);
        } else if (text.contains("\"event\":\"unsubscribed\"")) {
            logger.info("[{}] UnSubscribed: {}", id, channel);
        } else if (text.contains("\"hb\"]")) {
            logger.info("[{}] GET HB: {}", id, channel);
        } else {
            T message = converter.convert(text);
            if (message != null) {
                if (message instanceof OrderBook) {
                    if (!isFirst) ((OrderBook) message).setFull(false);
                    isFirst = false;
                }
                listener.onMessage(message);
            }
        }
    }

    @Override
    protected String toString(ByteString bytes)
        throws Exception
    {
        StringWriter sw = new StringWriter();
        try (
            final PrintWriter pw = new PrintWriter(sw);
            final ByteArrayInputStream in = new ByteArrayInputStream(bytes.toByteArray());
            final BufferedReader br = new BufferedReader(new InputStreamReader(new GZIPInputStream(in)));)
        {
            String line;
            for (int i = 0; (line = br.readLine()) != null; i++) {
                if (i > 0) pw.println();
                pw.print(line);
            }
        }
        return sw.toString();
    }
}
