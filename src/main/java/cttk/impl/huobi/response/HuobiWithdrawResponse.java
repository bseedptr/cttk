package cttk.impl.huobi.response;

import cttk.dto.WithdrawResult;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HuobiWithdrawResponse
    extends HuobiResponse
{
    String data;

    public WithdrawResult getWithdrawResult() {
        return new WithdrawResult().setWithdrawId(data);
    }
}
