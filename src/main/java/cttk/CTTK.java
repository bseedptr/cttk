package cttk;

import java.io.File;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cttk.dto.MarketInfos;
import cttk.exception.CTTKException;

public class CTTK {

    private static final Logger LOGGER = LoggerFactory.getLogger(CTTK.class);

    public static List<String> getSupportedCxIds() {
        final List<String> cxIds = new ArrayList<>();

        try {
            cxIds.addAll(getCxIdsFromJarClasspaths());
            cxIds.addAll(getCxIdsFromDirClasspaths());
        } catch (URISyntaxException | IOException e) {
            throw new RuntimeException(e);
        }

        return cxIds.stream().distinct().collect(Collectors.toList());
    }

    private static List<String> getCxIdsFromDirClasspaths()
        throws IOException, URISyntaxException
    {
        final List<String> cxIds = new ArrayList<>();

        final URLClassLoader classLoader = (URLClassLoader) Thread.currentThread().getContextClassLoader();

        final Enumeration<URL> urls = classLoader.getResources("cttk/impl");

        while (urls.hasMoreElements()) {
            final URL u = urls.nextElement();
            try {
                final File dir = toFile(u);
                if (dir.isDirectory()) {
                    for (final File f : dir.listFiles()) {
                        if (f.isDirectory()) {
                            final Path path = f.toPath();
                            final String cxId = path.getName(path.getNameCount() - 1).toString().toUpperCase();
                            addCxId(cxIds, cxId);
                        }
                    }
                } else {
                    cxIds.addAll(getCxIdsFromJarFile(dir));
                }
            } catch (Exception e) {
                LOGGER.error("Unable to process {}", u, e);
            }
        }

        return cxIds;
    }

    private static List<String> getCxIdsFromJarClasspaths()
        throws IOException, URISyntaxException
    {
        final List<String> cxIds = new ArrayList<>();

        final URLClassLoader classLoader = (URLClassLoader) Thread.currentThread().getContextClassLoader();

        for (final URL u : classLoader.getURLs()) {
            try {
                if (isEndsWith(u, ".jar") || isEndsWith(u, ".jar!/")) {
                    cxIds.addAll(getCxIdsFromJarURL(u));
                } else {
                    cxIds.addAll(getCxIdsFromJarFile(toFile(u)));
                }
            } catch (Exception e) {
                LOGGER.error("Unable to process {}", u, e);
            }
        }

        return cxIds;
    }

    private static boolean isEndsWith(URL u, String extension)
        throws URISyntaxException
    {
        return u.toURI().toString().endsWith(extension);
    }

    private static File toFile(URL u)
        throws URISyntaxException
    {
        String urlString = u.toString();
        if (urlString.contains("!")) {
            urlString = urlString.substring(0, urlString.indexOf('!'));
        }
        if (urlString.startsWith("jar:") && urlString.endsWith(".jar")
            || urlString.startsWith("zip:") && urlString.endsWith(".zip"))
        {
            urlString = urlString.substring(4);
        }
        return Paths.get(new URI(urlString)).toFile();
    }

    private static List<String> getCxIdsFromJarURL(URL url)
        throws IOException
    {
        final List<String> cxIds = new ArrayList<>();
        URLConnection urlCon = url.openConnection();

        if (urlCon instanceof JarURLConnection) {
            final JarFile jarFile = ((JarURLConnection) urlCon).getJarFile();
            cxIds.addAll(getCxIdsFromEnumeration(jarFile.entries()));
        }
        return cxIds;
    }

    private static List<String> getCxIdsFromJarFile(File file)
        throws IOException
    {
        final List<String> cxIds = new ArrayList<>();

        if (file.getName().endsWith(".jar") || file.getName().endsWith(".zip")) {
            try (final JarFile jarFile = new JarFile(file);) {
                cxIds.addAll(getCxIdsFromEnumeration(jarFile.entries()));
            }
        }
        return cxIds;
    }

    private static List<String> getCxIdsFromEnumeration(final Enumeration<JarEntry> enmr) {
        final List<String> cxIds = new ArrayList<>();

        while (enmr.hasMoreElements()) {
            final JarEntry je = enmr.nextElement();
            if (je.isDirectory() && je.getName().matches("cttk/impl/[^/]+/")) {
                final String temp = je.getName().substring("cttk/impl/".length());
                final String cxId = temp.substring(0, temp.length() - 1).toUpperCase();
                addCxId(cxIds, cxId);
            }
        }
        return cxIds;
    }

    private static void addCxId(final List<String> cxIds, final String cxId) {
        try {
            CXClientFactory.createPublicCXClient(cxId);
            cxIds.add(cxId);
        } catch (CTTKException e) {
            // do not add
        }
    }

    public static Map<String, MarketInfos> getCxIdToMarketInfosMap() {
        final Map<String, MarketInfos> map = new HashMap<>();

        for (final String cxId : getSupportedCxIds()) {
            try {
                final PublicCXClient client = CXClientFactory.createPublicCXClient(cxId);
                map.put(cxId, client.getMarketInfos());
            } catch (Exception e) {
                LOGGER.error("Unexpected error", e);
            }
        }

        return map;
    }
}
