package cttk.impl.hitbtc;

import cttk.CXIds;
import cttk.impl.AbstractPublicCXClientTest;

public class HitbtcPublicClientTest
    extends AbstractPublicCXClientTest
{
    @Override
    public String getCxId() {
        return CXIds.HITBTC;
    }

}