package cttk;

import static cttk.test.TestUtils.hasTestProfile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cttk.dto.MarketInfos;
import cttk.exception.UnsupportedCXException;

public class CTTKTest {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Before
    public void setup()
        throws UnsupportedCXException, IOException
    {
        org.junit.Assume.assumeFalse(hasTestProfile("skip-public"));
    }

    @Test
    public void testGetSupportedCxIds() {
        final List<String> cxIds = CTTK.getSupportedCxIds();

        Assert.assertNotNull(cxIds);
        Assert.assertTrue(cxIds.size() > 0);

        logger.debug("Supported CX Ids: {}", cxIds);
    }

    @Test
    public void testGetCxIdToMarketInfosMap() {
        final Map<String, MarketInfos> map = CTTK.getCxIdToMarketInfosMap();

        Assert.assertNotNull(map);
        Assert.assertTrue(map.size() > 0);
    }
}
