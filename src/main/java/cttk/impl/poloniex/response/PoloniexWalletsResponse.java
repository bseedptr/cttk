package cttk.impl.poloniex.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import cttk.dto.Wallet;
import cttk.dto.Wallets;
import cttk.impl.poloniex.PoloniexUtils;
import lombok.Data;

@Data
public class PoloniexWalletsResponse {
    private Map<String, String> map = new HashMap<>();

    @JsonAnyGetter
    public Map<String, String> any() {
        return this.map;
    }

    @JsonAnySetter
    public void set(String name, String value) {
        this.map.put(name, value);
    }

    public Wallet toWallet(String currency) {
        return new Wallet()
            .setCurrency(currency)
            .setAddress(this.map.get(PoloniexUtils.toPoloniexCurrencySymbol(currency)));
    }

    public Wallets toWallets() {
        List<Wallet> list = map.entrySet().stream()
            .map(e -> new Wallet()
                .setCurrency(PoloniexUtils.toStdCurrencySymbol(e.getKey()))
                .setAddress(e.getValue()))
            .collect(Collectors.toList());

        return new Wallets().setWallets(list);
    }
}
