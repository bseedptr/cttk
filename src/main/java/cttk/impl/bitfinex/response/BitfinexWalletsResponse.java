package cttk.impl.bitfinex.response;

import static cttk.impl.bitfinex.BitfinexUtils.toStdCurrencySymbol;

import java.util.ArrayList;

import cttk.dto.Wallet;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BitfinexWalletsResponse
    extends ArrayList<Object>
{
    public Wallet toWallet() {
        final String currency = toStdCurrencySymbol((String) get(1));
        return new Wallet()
            .setCurrency(currency);
    }
}