package cttk.impl.bibox.stream.response;

import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.impl.bibox.BiboxUtils;
import cttk.impl.util.JsonUtils;
import lombok.Data;

public class BiboxWebsocketListResponse<T>
    extends ArrayList<BiboxWebsocketResponse>
{
    public static ObjectMapper objectMapper = JsonUtils.createDefaultObjectMapper();

    private Class<T> clazz;

    public BiboxWebsocketListResponse(Class<T> clazz) {
        this.clazz = clazz;
    }

    public T getBiboxDataFirst()
        throws CTTKException, IOException
    {
        if (this.size() != 1) {
            throw new CTTKException("unexpected data size, " + this.toString())
                .setCxId(CXIds.BIBOX);
        }
        BiboxWebsocketResponse first = this.get(0);
        return convertElement(first);
    }

    private T convertElement(BiboxWebsocketResponse elem)
        throws IOException
    {
        if (elem.binary == 0) {
            return objectMapper.convertValue(elem.data, clazz);
        } else {
            return objectMapper.readValue(BiboxUtils.decodeWebsocketData((String) elem.data), clazz);
        }
    }
}

@Data
class BiboxWebsocketResponse {
    String channel;
    int binary;
    @JsonProperty("data_type")
    int dataType;
    Object data;
}
