package cttk.impl.binance;

import cttk.CXIds;
import cttk.impl.AbstractPollingCXStreamTest;

public class BinancePollingStreamTest
    extends AbstractPollingCXStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.BINANCE;
    }

    @Override
    protected long getThreadSleepMillis() {
        return 1000l;
    }
}
