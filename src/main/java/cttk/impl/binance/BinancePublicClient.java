package cttk.impl.binance;

import static cttk.impl.binance.BinanceUtils.getMarketSymbol;
import static cttk.impl.util.DateTimeUtils.toEpochMilli;
import static cttk.impl.util.Objects.nvl;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import cttk.PublicCXClient;
import cttk.dto.MarketInfos;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Tickers;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.binance.response.BinanceAggTradeResponse;
import cttk.impl.binance.response.BinanceExchangeInfoResponse;
import cttk.impl.binance.response.BinanceOrderBookResponse;
import cttk.impl.binance.response.BinanceTickerResponse;
import cttk.impl.binance.response.BinanceTradeResponse;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ParamsBuilder;
import cttk.request.GetOrderBookRequest;
import cttk.request.GetRecentTradesRequest;
import cttk.request.GetTickerRequest;
import cttk.request.GetTradeHistoryRequest;

public class BinancePublicClient
    extends AbstractBinanceClient
    implements PublicCXClient
{

    @Override
    public MarketInfos getMarketInfos()
        throws CTTKException
    {
        final HttpResponse response = requestGet("exchangeInfo");

        checkResponseError(null, response);

        return convert(response, BinanceExchangeInfoResponse.class, i -> i.toMarketInfos());
    }

    @Override
    public Ticker getTicker(GetTickerRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet("ticker/24hr",
            ParamsBuilder.build("symbol", getMarketSymbol(request)));

        checkResponseError(request, response);

        return convert(response, BinanceTickerResponse.class, r -> r.getTicker());
    }

    @Override
    public Tickers getAllTickers()
        throws CTTKException
    {
        final HttpResponse response = requestGet("ticker/24hr");

        checkResponseError(null, response);

        try {
            return convertList(response, BinanceTickerResponse.class,
                l -> new Tickers().setTickers(l.stream().map(r -> r.getTicker()).collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException(e)
                .setCxId(this.getCxId())
                .setResponseStatusCode(response.getStatus())
                .setExchangeMessage(response.getBody());
        }
    }

    @Override
    public OrderBook getOrderBook(GetOrderBookRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        checkOrderBookLimitParamValue(request.getCount());
        final HttpResponse response = requestGet("depth",
            ParamsBuilder.create()
                .put("symbol", getMarketSymbol(request))
                .putIfNotNull("limit", nvl(request.getCount(), 100))
                .build());

        checkResponseError(request, response);

        return convert(response, BinanceOrderBookResponse.class, r -> r.getOrderBook(request));
    }

    private static final Set<Integer> ALLOWED_LIMITS = new HashSet<>(Arrays.asList(5, 10, 20, 50, 100, 500, 1000));

    private void checkOrderBookLimitParamValue(Integer count)
        throws CTTKException
    {
        if (count != null && !ALLOWED_LIMITS.contains(count)) {
            throw new CTTKException("invalid limit: " + count).setCxId(this.getCxId());
        }
    }

    @Override
    public Trades getRecentTrades(GetRecentTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {

        return request.getLastTradeId() != null
            ? getAggregatedTrades(toGetTradeHistoryRequest(request))
            : getTrades(request);
    }

    private GetTradeHistoryRequest toGetTradeHistoryRequest(GetRecentTradesRequest request) {
        return new GetTradeHistoryRequest()
            .setCxId(request.getCxId())
            .setCurrencyPair(request)
            .setCount(request.getCount())
            .setLastTradeId(request.getLastTradeId())
            .setPeriod(request.getPeriod());
    }

    @Override
    public Trades getTradeHistory(GetTradeHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return getAggregatedTrades(request);
    }

    private Trades getTrades(GetRecentTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {

        final HttpResponse response = requestGet("trades",
            ParamsBuilder.create()
                .put("symbol", getMarketSymbol(request))
                .putIfNotNull("limit", request.getCount())
                .build());

        checkResponseError(request, response);

        try {
            return convertList(response, BinanceTradeResponse.class, l -> new Trades()
                .setTrades(l.stream().map(t -> t.getTrade()).collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException(e)
                .setCxId(this.getCxId())
                .setResponseStatusCode(response.getStatus())
                .setExchangeMessage(response.getBody());
        }
    }

    private Trades getAggregatedTrades(GetTradeHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // TODO: historicalTrades support full data but require MARKET_DATA key
        final HttpResponse response = requestGet("aggTrades",
            ParamsBuilder.create()
                .put("symbol", getMarketSymbol(request))
                .putIfNotNull("limit", request.getCount())
                .putIfNotNull("fromId", request.getLastTradeId())
                .putIfNotNull("startTime", toEpochMilli(request.getStartDateTime()))
                .putIfNotNull("endTime", toEpochMilli(request.getEndDateTime()))
                .build());

        checkResponseError(request, response);

        try {
            return convertList(response, BinanceAggTradeResponse.class, l -> new Trades()
                .setCurrencyPair(request)
                .setTrades(l.stream().map(t -> t.toTrade()).collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException(e)
                .setCxId(this.getCxId())
                .setResponseStatusCode(response.getStatus())
                .setExchangeMessage(response.getBody());
        }
    }
}
