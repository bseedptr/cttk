package cttk.impl.okex.stream;

import cttk.CXStreamListener;
import cttk.impl.stream.AbstractWebsocketStream;
import cttk.impl.stream.MessageConverter;
import cttk.request.AbstractCXStreamRequest;
import okhttp3.WebSocketListener;

public class OkexWebsocketStream<T, R extends AbstractCXStreamRequest<R>>
    extends AbstractWebsocketStream<T, R>
{
    private static final String WSS_API_OKEX_PRO_WS = "wss://real.okex.com:10442/ws/v3";

    public OkexWebsocketStream(
        final String channel,
        final R request,
        final MessageConverter<T> converter,
        final CXStreamListener<T> listener)
    {
        super(channel, request, converter, listener);
    }

    @Override
    protected String getUrl() {
        return WSS_API_OKEX_PRO_WS;
    }

    @Override
    protected WebSocketListener createWebSocketListener() {
        return new OkexWebsocketListener<>(this, channel, request, converter, listener);
    }
}
