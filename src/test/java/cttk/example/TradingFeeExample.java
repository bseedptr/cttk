package cttk.example;

import java.io.IOException;
import java.util.List;

import cttk.CTTK;
import cttk.CXClientFactory;
import cttk.CurrencyPair;
import cttk.PrivateCXClient;
import cttk.TradingFeeProvider;
import cttk.auth.CXCredentialsProvider;
import cttk.dto.TradingFee;
import cttk.test.TestConfig;

public class TradingFeeExample {

    public static void main(String[] args)
        throws IOException
    {
        List<String> cxIds = CTTK.getSupportedCxIds();

        for (String cxId : cxIds) {
            final TestConfig testConfig = TestConfig.readFromClasspathYaml("config/" + cxId.toLowerCase() + ".yml");
            final CurrencyPair currencyPair = testConfig.getValidCurrencyPair();

            try {
                PrivateCXClient client = CXClientFactory.createPrivateCXClient(cxId, CXCredentialsProvider.getDefaultCredential(cxId).getDataKey());

                TradingFeeProvider tradingFeeProvider = client.getTradingFeeProvider();

                TradingFee tradingFee = tradingFeeProvider.getFee(currencyPair);

                Examples.print(cxId, tradingFee);

            } catch (Exception e) {
                continue;
            }
        }
    }

}
