package cttk;

import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCXException;
import cttk.impl.chargingpolicy.AllChargingPolicyProvider;
import cttk.impl.chargingpolicy.ByQuoteChargingPolicyProvider;
import cttk.impl.chargingpolicy.CXTokenPayableChargingPolicyProvider;
import cttk.impl.chargingpolicy.DefaultChargingPolicyProvider;
import cttk.util.StringUtils;

public class ChargingPolicyProviderFactory {
    public static ChargingPolicyProvider create(final String cxId)
        throws CTTKException
    {
        if (StringUtils.isEmpty(cxId)) throw new UnsupportedCXException("CxId is required.").setCxId(cxId);
        switch (cxId.trim().toUpperCase()) {
            case CXIds.BIBOX:
                return new CXTokenPayableChargingPolicyProvider(cxId, "BIX");
            case CXIds.BINANCE:
                return new CXTokenPayableChargingPolicyProvider(cxId, "BNB");
            case CXIds.HITBTC:
            case CXIds.UPBIT:
                return new ByQuoteChargingPolicyProvider(cxId);
            case CXIds.KRAKEN:
                return new AllChargingPolicyProvider(cxId);
            case CXIds.BITFINEX:
            case CXIds.BITHUMB:
            case CXIds.COINONE:
            case CXIds.HUOBI:
            case CXIds.KORBIT:
            case CXIds.OKEX:
            case CXIds.POLONIEX:
                return new DefaultChargingPolicyProvider(cxId);
            default:
                throw new UnsupportedCXException("Specify ChargingPolicyProvider explicitly.").setCxId(cxId);
        }
    }
}
