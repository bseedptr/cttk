package cttk.impl.bibox.response;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import cttk.dto.Trade;
import cttk.dto.Trades;

public class BiboxTrades extends ArrayList<BiboxTrade> {
    public Trades toTrades() {
        return new Trades().setTrades(toTradeList(this));
    }

    private List<Trade> toTradeList(List<BiboxTrade> trades) {
        if (trades == null)
            return null;
        return trades.stream().map(BiboxTrade::toTrade).collect(Collectors.toList());
    }
}
