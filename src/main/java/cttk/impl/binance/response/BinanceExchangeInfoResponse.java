package cttk.impl.binance.response;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cttk.CurrencyPair;
import cttk.dto.MarketInfo;
import cttk.dto.MarketInfos;
import cttk.impl.binance.BinanceUtils;
import lombok.Data;

@Data
public class BinanceExchangeInfoResponse {
    String timezone;
    long serverTime;
    List<RateLimit> rateLimits;
    List<Symbol> symbols;

    @JsonIgnore
    public List<CurrencyPair> getCurrencyPairs() {
        if (symbols == null) return null;

        return symbols.stream()
            .map(i -> CurrencyPair.of(i.getBaseAsset(), i.getQuoteAsset()))
            .collect(Collectors.toList());
    }

    public MarketInfos toMarketInfos() {
        if (symbols == null) return null;
        return new MarketInfos()
            .setMarketInfos(symbols.stream().map(i -> i.toMarketInfo()).collect(Collectors.toList()));
    }
}

@Data
class Symbol {
    String symbol;
    String status;
    String baseAsset;
    Integer baseAssetPrecision;
    String quoteAsset;
    Integer quotePrecision;
    List<String> orderTypes;
    Boolean icebergAllowed;
    List<Filter> filters;

    public MarketInfo toMarketInfo() {
        final Filter priceFilter = getFilterByType("PRICE_FILTER");
        final Filter sizeFilter = getFilterByType("LOT_SIZE");
        final Filter notionalFilter = getFilterByType("MIN_NOTIONAL");
        return new MarketInfo()
            .setStatus(status)
            .setCurrencyPair(BinanceUtils.toStdCurrencySymbol(baseAsset), BinanceUtils.toStdCurrencySymbol(quoteAsset))
            .setPriceIncrement(priceFilter.getTickSize())
            .setMinPrice(priceFilter.getMinPrice().intValue() == 0 ? null : priceFilter.getMinPrice())
            .setMaxPrice(priceFilter.getMaxPrice().intValue() == 0 ? null : priceFilter.getMaxPrice())
            .setQuantityIncrement(sizeFilter.getStepSize())
            .setMinQuantity(sizeFilter.getMinQty())
            .setMaxQuantity(sizeFilter.getMaxQty())
            .setMinTotal(notionalFilter.getMinNotional())
            .setOrderTypes(orderTypes);
    }

    private Filter getFilterByType(String type) {
        return filters == null ? null
            : filters.stream().filter(i -> i.getFilterType().equals(type)).findFirst().get();
    }
}

@Data
class Filter {
    String filterType;

    BigDecimal minPrice;
    BigDecimal maxPrice;
    BigDecimal tickSize;

    BigDecimal minQty;
    BigDecimal maxQty;
    BigDecimal stepSize;

    BigDecimal minNotional;
}

@Data
class RateLimit {
    String rateLimitType;
    String interval;
    Long limit;
}