package cttk.impl.huobi;

import static cttk.impl.huobi.HuobiUtils.getMarketSymbol;

import cttk.PublicCXClient;
import cttk.dto.MarketInfos;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Tickers;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.huobi.response.HuobiOrderBookResponse;
import cttk.impl.huobi.response.HuobiSymbolsResponse;
import cttk.impl.huobi.response.HuobiTickerResponse;
import cttk.impl.huobi.response.HuobiTradeHistoryResponse;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.Objects;
import cttk.impl.util.ParamsBuilder;
import cttk.request.GetOrderBookRequest;
import cttk.request.GetRecentTradesRequest;
import cttk.request.GetTickerRequest;
import cttk.request.GetTradeHistoryRequest;

public class HuobiPublicClient
    extends AbstractHuobiClient
    implements PublicCXClient
{
    @Override
    public MarketInfos getMarketInfos()
        throws CTTKException
    {
        final HttpResponse response = requestGet("/v1/common/symbols", null);

        checkResponseError(null, response);

        return super.convert(response, HuobiSymbolsResponse.class, i -> i.toMarketInfos());
    }

    @Override
    public Tickers getAllTickers()
        throws CTTKException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public Ticker getTicker(GetTickerRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet("/market/detail/merged",
            ParamsBuilder.build("symbol", getMarketSymbol(request)));

        checkResponseError(request, response);

        return convert(response, HuobiTickerResponse.class, (i -> i.getTicker(request)));
    }

    @Override
    public OrderBook getOrderBook(GetOrderBookRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final String type = HuobiUtils.toOrderBookTypeParamValue(request.getPrecision());
        final HttpResponse response = requestGet("/market/depth",
            ParamsBuilder.create()
                .put("symbol", getMarketSymbol(request))
                .put("type", type)
                .build());

        checkResponseError(request, response);

        return convert(response, HuobiOrderBookResponse.class, (i -> i.getOrderBook(request)));
    }

    @Override
    public Trades getRecentTrades(GetRecentTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet("/market/history/trade",
            ParamsBuilder.create()
                .put("symbol", getMarketSymbol(request))
                .put("size", Objects.nvl(request.getCount(), 100))
                .build());

        checkResponseError(request, response);

        return convert(response, HuobiTradeHistoryResponse.class, (i -> i.toTrades()));
    }

    @Override
    public Trades getTradeHistory(GetTradeHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }
}
