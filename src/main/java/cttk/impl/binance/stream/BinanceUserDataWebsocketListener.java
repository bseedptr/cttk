package cttk.impl.binance.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.ObjectMapper;

import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.UserData;
import cttk.impl.binance.BinanceUtils;
import cttk.impl.binance.stream.response.BinanceExecutionReportResponse;
import cttk.impl.binance.stream.response.BinanceOutboundAccountInfoResponse;
import cttk.impl.stream.AbstractWebsocketListener;
import cttk.impl.util.JsonUtils;
import cttk.request.OpenUserDataStreamRequest;
import okhttp3.Response;
import okhttp3.WebSocket;
import okio.ByteString;

public class BinanceUserDataWebsocketListener
    extends AbstractWebsocketListener<UserData, OpenUserDataStreamRequest>
{
    private final ObjectMapper objectMapper = JsonUtils.createDefaultObjectMapper();

    public BinanceUserDataWebsocketListener(
        final CXStream<UserData> stream,
        final CXStreamListener<UserData> listener)
    {
        super(stream,
            "userDataStream",
            null, // ignore CurrencyPair
            null, // ignore converter
            listener,
            CXIds.BINANCE);
    }

    protected String getId() {
        return String.format("%s-%s-%020d", getClass().getSimpleName(), "userDataStream", System.currentTimeMillis());
    }

    protected void handleMessage(WebSocket webSocket, String text)
        throws Exception
    {
        BinanceUtils.checkErrorByResponseMessage(request, text, null);
        final List<UserData> userDataList = convert(text);
        if (userDataList != null && !userDataList.isEmpty()) {
            userDataList.forEach(d -> listener.onMessage(d));
        }
    }

    public List<UserData> convert(String message)
        throws Exception
    {
        List<UserData> userDataList = new ArrayList<>();
        final String eventType = getEventType(message);

        if ("executionReport".equals(eventType)) {
            final BinanceExecutionReportResponse response = objectMapper.readValue(message, BinanceExecutionReportResponse.class);
            userDataList.add(UserData.of(response.toUserOrder().setCxId(CXIds.BINANCE)));
            if ("TRADE".equals(response.getCurrentExecutionType())) {
                userDataList.add(UserData.of(response.toUserTrade().setCxId(CXIds.BINANCE)));
            }
        } else if ("outboundAccountInfo".equals(eventType)) {
            final BinanceOutboundAccountInfoResponse response = objectMapper.readValue(message, BinanceOutboundAccountInfoResponse.class);
            userDataList.add(UserData.of(response.toBalances().setCxId(CXIds.BINANCE)));
            // TODO send commission rate related fields
        } else {
            throw new Exception("Unknown eventType [" + eventType + "]");
        }

        return userDataList;
    }

    private String getEventType(String message) {
        final Matcher matcher = Pattern.compile("\"e\"\\s*[:]\\s*\"([^\"]+)\"").matcher(message);
        return matcher.find() ? matcher.group(1) : null;
    }

    protected String toString(ByteString bytes)
        throws Exception
    {
        return bytes == null ? null : bytes.hex();
    }

    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        if (t instanceof java.io.EOFException) {
            webSocket.close(NORMAL_CLOSURE_STATUS, null);
            logger.info("[{}] Closing: - {}", id, channel);
        } else {
            super.onFailure(webSocket, t, response);
        }
    }
}