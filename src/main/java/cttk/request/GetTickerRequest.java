package cttk.request;

import cttk.AbstractCurrencyPair;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class GetTickerRequest
    extends AbstractCurrencyPair<GetTickerRequest>
{
}
