package cttk.impl.huobi.response;

import java.util.List;
import java.util.stream.Collectors;

import cttk.dto.UserOrders;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HuobiUserOrdersResponse
    extends HuobiResponse
{
    List<HuobiUserOrder> data;

    public UserOrders getUserOrders() {
        return new UserOrders()
            .setOrders(data.stream().map(d -> d.getUserOrder()).filter(d -> d != null).collect(Collectors.toList()));
    }
}
