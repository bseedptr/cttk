package cttk.request;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class OpenTradeStreamRequest
    extends AbstractCXStreamRequest<OpenTradeStreamRequest>
{
}
