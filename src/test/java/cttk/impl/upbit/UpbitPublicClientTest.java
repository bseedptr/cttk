package cttk.impl.upbit;

import org.junit.Assert;
import org.junit.Test;

import cttk.CXIds;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.AbstractPublicCXClientTest;
import cttk.request.GetRecentTradesRequest;

public class UpbitPublicClientTest
    extends AbstractPublicCXClientTest
{
    @Override
    public String getCxId() {
        return CXIds.UPBIT;
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetTradeHistory()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetTradeHistory();
    }

    @Test(expected = UnsupportedCurrencyPairException.class)
    public void testGetRecentTradesWithUnsupportedQuoteCurrency()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();

        final GetRecentTradesRequest request = new GetRecentTradesRequest()
            .setCurrencyPair(getInvalidCurrencyPair())
            .setCount(100);

        final Trades trades = client.getRecentTrades(request);

        Assert.assertNotNull(trades);
    }
}
