package cttk.test;

import java.util.Arrays;
import java.util.stream.Collectors;

public class TestUtils {
    public static boolean hasTestProfile(String profile) {
        String profiles = System.getProperty("cttk.test.profiles");
        if (profiles == null || profiles.trim().isEmpty()) return false;
        return Arrays.stream(profiles.trim().split(","))
            .map(i -> i.trim()).collect(Collectors.toSet()).contains(profile);
    }
}
