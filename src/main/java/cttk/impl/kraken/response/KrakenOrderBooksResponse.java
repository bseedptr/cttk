package cttk.impl.kraken.response;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.dto.OrderBook;
import cttk.dto.OrderBook.SimpleOrder;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.kraken.KrakenUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class KrakenOrderBooksResponse
    extends KrakenResponse<HashMap<String, KrakenOrderBook>>
{
    public OrderBook getOrderBook(KrakenAssetPairs krakenAssetPairs, CurrencyPair currencyPair)
        throws UnsupportedCurrencyPairException
    {
        KrakenOrderBook kob = this.result.get(KrakenUtils.getMarketSymbol(krakenAssetPairs, currencyPair));
        if (kob == null) {
            throw new UnsupportedCurrencyPairException(currencyPair.getPairString());
        }

        return new OrderBook()
            .setCurrencyPair(currencyPair)
            .setAsks(toOrderList(kob.asks))
            .setBids(toOrderList(kob.bids));
    }

    static List<SimpleOrder> toOrderList(List<List<String>> arrOrders) {
        if (arrOrders == null) return null;
        return arrOrders.stream()
            .map(l -> new SimpleOrder(l.get(0), l.get(1)))
            .collect(Collectors.toList());
    }
}

@Data
class KrakenOrderBook {
    List<List<String>> asks; // ask side array of array entries(<price>, <volume>, <timestamp>)
    List<List<String>> bids; // bid side array of array entries(<price>, <volume>, <timestamp>)
}