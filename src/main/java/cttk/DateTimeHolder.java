package cttk;

import java.time.ZonedDateTime;

public interface DateTimeHolder<T extends DateTimeHolder<T>>
    extends DateTimeGettable
{
    T setDateTime(ZonedDateTime now);
}
