package cttk.impl.kraken.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class KrakenCancelOrderResponse
    extends KrakenResponse<KrakenCancelOrderResult>
{
    public Long getCount() {
        return result == null ? null : result.getCount();
    }
}

@Data
class KrakenCancelOrderResult {
    Long count;
}