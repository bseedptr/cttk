package cttk;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCXException;
import cttk.test.TestConfig;

public class OrderVerifierFactoryTest {
    @Test
    public void testWithSupportedExchanges()
        throws CTTKException, IOException
    {
        final String[] supportedCXIds = {
            CXIds.BIBOX,
            CXIds.BINANCE,
            CXIds.BITFINEX,
            CXIds.HITBTC,
            CXIds.HUOBI,
            CXIds.KORBIT,
            CXIds.KRAKEN,
            CXIds.OKEX,
            CXIds.COINONE,
            CXIds.UPBIT,
            CXIds.BITHUMB,
            CXIds.POLONIEX
        };

        for (String cxId : supportedCXIds) {
            final TestConfig testConfig = TestConfig.readFromClasspathYaml("config/" + cxId.toLowerCase() + ".yml");
            final CurrencyPair currencyPair = testConfig.getValidCurrencyPair();
            final PublicCXClient publicClient = CXClientFactory.createPublicCXClient(cxId);
            final OrderVerifier orderVerifier = OrderVerifierFactory.create(cxId, publicClient.getMarketInfos().getMarketInfo(currencyPair));
            Assert.assertNotNull(orderVerifier);
        }

    }

    @Test
    public void testWithUnsupportedExchanges()
        throws IOException, CTTKException
    {
        final String[] supportedCXIds = {
            CXIds.BITTREX,
            CXIds.COINBASE
        };

        for (String cxId : supportedCXIds) {
            final TestConfig testConfig = TestConfig.readFromClasspathYaml("config/" + cxId.toLowerCase() + ".yml");
            final CurrencyPair currencyPair = testConfig.getValidCurrencyPair();
            final PublicCXClient publicClient = CXClientFactory.createPublicCXClient(cxId);

            try {
                OrderVerifierFactory.create(cxId, publicClient.getMarketInfos().getMarketInfo(currencyPair));
            } catch (UnsupportedCXException e) {
                // It's is the expected event
                continue;
            }
            Assert.assertFalse("Expected to unsupported for [" + cxId + "]", true);
        }
    }
}
