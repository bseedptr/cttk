package cttk.impl.bibox.stream.response;

import java.io.IOException;

import cttk.dto.Ticker;
import cttk.exception.CTTKException;
import cttk.impl.bibox.response.BiboxTicker;

public class BiboxWebsocketTickerResponse extends BiboxWebsocketListResponse<BiboxTicker> {
    public BiboxWebsocketTickerResponse() {
        super(BiboxTicker.class);
    }

    public Ticker toTicker()
        throws IOException, CTTKException
    {
        return this.getBiboxDataFirst().toTicker();
    }
}
