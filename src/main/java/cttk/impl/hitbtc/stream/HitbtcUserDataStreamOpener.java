package cttk.impl.hitbtc.stream;

import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.UserData;
import cttk.UserDataStreamOpener;
import cttk.auth.Credential;
import cttk.exception.CTTKException;
import cttk.request.OpenUserDataStreamRequest;

public class HitbtcUserDataStreamOpener
    implements UserDataStreamOpener
{
    private final Credential credential;

    public HitbtcUserDataStreamOpener(Credential credential) {
        super();
        this.credential = credential;
    }

    @Override
    public CXStream<UserData> openUserDataStream(OpenUserDataStreamRequest request, CXStreamListener<UserData> listener)
        throws CTTKException
    {
        // there is no additional configuration needed to create stream except credential and listener
        return new HitbtcUserDataWebsocketStream(credential, listener);
    }
}
