package cttk.request;

import cttk.AbstractCurrencyHolder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class GetBalanceRequest
    extends AbstractCurrencyHolder<GetBalanceRequest>
{
}
