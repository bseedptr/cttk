package cttk.impl.kraken;

import static cttk.util.StringUtils.toUpper;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.impl.kraken.response.KrakenAssetPairs;

public class KrakenUtils {
    public static String getMarketSymbol(KrakenAssetPairs krakenAssetPairs, final CurrencyPair currencyPair) {
        if (currencyPair == null) return null;
        return krakenAssetPairs.getKrakenAssetPair(toKrakenAssetSymbol(currencyPair.getBaseCurrency()), toKrakenAssetSymbol(currencyPair.getQuoteCurrency()));
    }

    public static String toKrakenAssetSymbol(final String currency) {
        if (currency == null) return null;
        final String upper = toUpper(currency);
        switch (upper) {
            case "BCH":
            case "DASH":
            case "EOS":
            case "GNO":
            case "USDT":
                return upper;
            case "FEE":
                return "K" + upper;
            case "CAD":
            case "EUR":
            case "GBP":
            case "JPY":
            case "KRW":
            case "USD":
                return "Z" + upper;
            case "BTC":
                return "XXBT";
            default:
                return upper.length() > 3 ? upper : "X" + upper;
        }
    }

    public static String toKrakenCurrencySymbol(final String symbol) {
        switch (symbol) {
            case "BTC":
                return "XBT";
            default:
                return symbol;
        }
    }

    private static final String ORDER_ID_REGEX = "\\w{6}[-]\\w{5}[-]\\w{6}";

    public static boolean checkOrderIDFormat(String oid) {
        return oid != null && oid.matches(ORDER_ID_REGEX);
    }

    public static String toStdCurrencySymbol(final String symbol) {
        if (symbol == null) return null;
        final String upper = toUpper(symbol);

        switch (upper) {
            case "BCH":
            case "DASH":
            case "EOS":
            case "GNO":
            case "USDT":
                return upper;
            case "KFEE":
                return "FEE";
            case "ZCAD":
                return "CAD";
            case "ZEUR":
                return "EUR";
            case "ZGBP":
                return "GBP";
            case "ZJPY":
                return "JPY";
            case "ZKRW":
                return "KRW";
            case "ZUSD":
                return "USD";
            case "XBT":
            case "XXBT":
                return "BTC";
            default:
                return upper.startsWith("X") && upper.length() > 3 ? upper.substring(1) : upper;
        }
    }

    public static CurrencyPair parseMarketSymbol(String symbol) {
        if (symbol == null) return null;

        final String upper = toUpper(symbol.trim());
        final int len = upper.length();
        final int idx = upper.endsWith("USDT")
            || upper.endsWith("ZCAD")
            || upper.endsWith("ZEUR")
            || upper.endsWith("ZGBP")
            || upper.endsWith("ZJPY")
            || upper.endsWith("ZKRW")
            || upper.endsWith("ZUSD")
            || upper.endsWith("XETH")
            || upper.endsWith("XXBT")
                ? len - 4
                : len - 3;

        return CurrencyPair.of(
            toStdCurrencySymbol(upper.substring(0, idx)),
            toStdCurrencySymbol(upper.substring(idx)));
    }

    public static String toString(OrderSide orderSide) {
        if (orderSide == null) return null;
        switch (orderSide) {
            case BUY:
                return "buy";
            case SELL:
                return "sell";
            default:
                return null;
        }
    }

    public static OrderSide parseOrderSide(String side) {
        if (side == null) return null;
        switch (toUpper(side)) {
            case "S":
                return OrderSide.SELL;
            case "B":
                return OrderSide.BUY;
            default:
                return OrderSide.of(side);
        }
    }

    public static String guessFeeCurrency(CurrencyPair pair) {
        return pair.getQuoteCurrency();
    }
}
