package cttk.impl.bibox;

import cttk.CXIds;
import cttk.impl.AbstractOrderVerifierTest;

public class BiboxOrderVerifierTest
    extends AbstractOrderVerifierTest
{
    @Override
    public String getCxId() {
        return CXIds.BIBOX;
    }
}
