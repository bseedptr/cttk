package cttk.impl.bibox;

import java.util.Map;

import org.junit.Test;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.AbstractPrivateCXClientWithdrawTest;
import cttk.request.WithdrawToBankRequest;
import cttk.request.WithdrawToWalletRequest;
import cttk.test.PropertyUtils;
import cttk.test.TOTPUtil;

public class BiboxPrivateClientWithdrawTest
    extends AbstractPrivateCXClientWithdrawTest
{
    @Override
    public String getCxId() {
        return CXIds.BIBOX;
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testWithdrawToBankAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        client.withdraw(new WithdrawToBankRequest());
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testWithdrawBTCToWallet()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        Map<String, String> props = new PropertyUtils().getProperties(CXIds.BIBOX);
        String totpSecret = props.get("totpSecret");
        String password = props.get("password");
        String totpCode = TOTPUtil.getTotpCode(totpSecret);

        client.withdraw(new WithdrawToWalletRequest()
            .setTotpCode(totpCode)
            .setPassword(password)
            .setCurrency("BTC")
            .setQuantity("0.00001")
            .setAddress("0xdadaee00dbe12fa5cec8d1ad3ed4b251e65ad203")); // cx@voostlab.com of Bithumb
    }
}
