package cttk.impl.hitbtc;

import static cttk.impl.hitbtc.HitbtcUtils.toStdCurrencyPair;

import java.io.IOException;
import java.util.stream.Collectors;

import cttk.PublicCXClient;
import cttk.dto.MarketInfos;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Tickers;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.hitbtc.response.HitbtcOrderBookResponse;
import cttk.impl.hitbtc.response.HitbtcSymbolResponse;
import cttk.impl.hitbtc.response.HitbtcTickerResponse;
import cttk.impl.hitbtc.response.HitbtcTradesResponse;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ParamsBuilder;
import cttk.request.GetOrderBookRequest;
import cttk.request.GetRecentTradesRequest;
import cttk.request.GetTickerRequest;
import cttk.request.GetTradeHistoryRequest;

public class HitbtcPublicClient
    extends AbstractHitbtcClient
    implements PublicCXClient
{
    public HitbtcPublicClient() {
        super();
    }

    @Override
    public MarketInfos getMarketInfos()
        throws CTTKException
    {
        final HttpResponse response = requestGet("/public/symbol");

        checkResponseError(null, response);

        try {
            return convertList(response, HitbtcSymbolResponse.class,
                e -> new MarketInfos().setMarketInfos(e.stream().map(HitbtcSymbolResponse::toMarketInfo).collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException(e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public Ticker getTicker(GetTickerRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet("/public/ticker/" + HitbtcUtils.getMarketSymbol(request));

        checkResponseError(request, response);

        return convert(response, HitbtcTickerResponse.class, HitbtcTickerResponse::toTicker);
    }

    @Override
    public Tickers getAllTickers()
        throws CTTKException
    {
        final HttpResponse response = requestGet("/public/ticker");

        checkResponseError(null, response);

        try {
            return convertList(response, HitbtcTickerResponse.class,
                e -> new Tickers().setTickers(e.stream().map(HitbtcTickerResponse::toTicker).collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException(e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public OrderBook getOrderBook(GetOrderBookRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet(
            "/public/orderbook/" + HitbtcUtils.getMarketSymbol(request),
            ParamsBuilder.buildIfNotNull("limit", request.getCount()));

        checkResponseError(request, response);

        return convert(response, HitbtcOrderBookResponse.class, e -> e.toOrderBook(request));
    }

    @Override
    public Trades getRecentTrades(GetRecentTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet("/public/trades/" + HitbtcUtils.getMarketSymbol(request),
            ParamsBuilder.create()
                .putIfNotNull("by", request.getLastTradeId() == null ? null : "id")
                .putIfNotNull("from", request.getLastTradeId())
                .putIfNotNull("limit", request.getCount())
                .build());

        checkResponseError(request, response);

        try {
            return convertList(response, HitbtcTradesResponse.class,
                e -> new Trades()
                    .setCurrencyPair(toStdCurrencyPair(request))
                    .setTrades(e.stream().map(HitbtcTradesResponse::toTrade).collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException(e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public Trades getTradeHistory(GetTradeHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet("/public/trades/" + HitbtcUtils.getMarketSymbol(request),
            ParamsBuilder.create()
                .put("sort", "ASC")
                .put("by", "timestamp")
                .put("from", request.getStartDateTime().toLocalDateTime())
                .put("till", request.getEndDateTime().toLocalDateTime())
                .putIfNotNull("limit", request.getCount())
                .build());

        checkResponseError(request, response);

        try {
            return convertList(response, HitbtcTradesResponse.class,
                e -> new Trades()
                    .setCurrencyPair(toStdCurrencyPair(request))
                    .setTrades(e.stream().map(HitbtcTradesResponse::toTrade).collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException(e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }
}
