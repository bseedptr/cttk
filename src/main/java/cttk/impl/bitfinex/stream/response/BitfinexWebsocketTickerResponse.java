package cttk.impl.bitfinex.stream.response;

import static cttk.impl.bitfinex.BitfinexUtils.toStdCurrencyPair;
import static cttk.impl.bitfinex.BitfinexUtils.toBigDecimal;
import static cttk.impl.util.MathUtils.multiply100;

import java.util.ArrayList;

import cttk.CurrencyPair;
import cttk.dto.Ticker;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BitfinexWebsocketTickerResponse
    extends ArrayList<Object>
{
    public Ticker toTicker(CurrencyPair currencyPair) {
        return new Ticker()
            .setCurrencyPair(toStdCurrencyPair(currencyPair))
            .setHighestBid(toBigDecimal(this.get(1)))
            .setLowestAsk(toBigDecimal(this.get(3)))
            .setChange(toBigDecimal(this.get(5)))
            .setChangePercent(multiply100(toBigDecimal(this.get(6))))
            .setLastPrice(toBigDecimal(this.get(7)))
            .setVolume(toBigDecimal(this.get(8)))
            .setHighPrice(toBigDecimal(this.get(9)))
            .setLowPrice(toBigDecimal(this.get(10)));
    }
}