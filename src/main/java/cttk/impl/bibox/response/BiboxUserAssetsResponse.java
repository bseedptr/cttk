package cttk.impl.bibox.response;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.dto.Balance;
import cttk.dto.Balances;
import cttk.impl.util.MathUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BiboxUserAssetsResponse
    extends BiboxResultWrapperResponse<BiboxUserAssets>
{
    public Balances toBalances() {
        return unwrap().toBalances();
    }
}

@Data
class BiboxUserAssets {
    @JsonProperty("total_btc")
    BigDecimal totalBtc;
    @JsonProperty("total_cny")
    BigDecimal totalCny;
    @JsonProperty("total_usd")
    BigDecimal totalUsd;
    @JsonProperty("assets_list")
    List<BiboxUserAsset> assetsList;

    Balances toBalances() {
        return new Balances()
            .setBalances(assetsList.stream()
                .map(BiboxUserAsset::toBalance)
                .filter(this::isNotZero)
                .collect(Collectors.toList()));
    }

    boolean isNotZero(Balance balance) {
        return MathUtils.isNotZero(balance.getTotal());
    }
}

@Data
class BiboxUserAsset {
    @JsonProperty("coin_symbol")
    String coinSymbol;
    @JsonProperty("balance")
    BigDecimal available;
    BigDecimal freeze;
    @JsonProperty("BTCValue")
    BigDecimal btcValue;
    @JsonProperty("CNYValue")
    BigDecimal cnyValue;
    @JsonProperty("USDValue")
    BigDecimal usdValue;

    Balance toBalance() {
        return new Balance()
            .setCurrency(coinSymbol)
            .setAvailable(available)
            .setInUse(freeze)
            .setTotal(MathUtils.add(available, freeze));
    }
}
