package cttk.impl.hitbtc.stream.response;

import static cttk.impl.hitbtc.HitbtcUtils.getStatusType;
import static cttk.impl.hitbtc.HitbtcUtils.parseMarketSymbol;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

import cttk.OrderSide;
import cttk.PricingType;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.impl.util.MathUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HitbtcUserDataOrderResponse
    extends HitbtcUserDataWebsocketResponse
{
    List<HitbtcUserDataOrder> result;

    public UserOrders toUserOrders() {
        return new UserOrders().setOrders(toUserOrderList(result));
    }

    static List<UserOrder> toUserOrderList(List<HitbtcUserDataOrder> list) {
        if (list == null) return null;

        return list.stream()
            .map(t -> t.toUserOrder())
            .collect(Collectors.toList());
    }
}

@Data
class HitbtcUserDataOrder {
    String id;
    String clientOrderId;
    String symbol;
    String side;
    String status;
    String type;
    String timeInForce;
    BigDecimal price;
    BigDecimal quantity;
    BigDecimal cumQuantity;
    String createdAt;
    String updatedAt;
    String reportType;
    String originalRequestClientOrderId;

    public UserOrder toUserOrder() {
        return new UserOrder()
            .setCurrencyPair(parseMarketSymbol(symbol))
            .setOid(id)
            .setClientOrderId(clientOrderId)
            .setDateTime(ZonedDateTime.parse(createdAt))
            .setSide(OrderSide.of(side))
            .setPricingType(PricingType.of(type))
            .setStatus(status)
            .setStatusType(getStatusType(status))
            .setPrice(price)
            .setQuantity(quantity)
            .setFilledQuantity(cumQuantity)
            .setRemainingQuantity(MathUtils.subtract(quantity, cumQuantity))
            .setTotal(MathUtils.multiply(price, quantity))
            .setCompletedDateTime(ZonedDateTime.parse(updatedAt));
    }
}
