package cttk.exception;

import java.math.BigDecimal;

import cttk.CurrencyPair;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class InvalidQuantityIncrementPrecisionException
    extends InvalidOrderException
{
    private static final long serialVersionUID = 1L;

    public InvalidQuantityIncrementPrecisionException() {
        super();
    }

    public InvalidQuantityIncrementPrecisionException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidQuantityIncrementPrecisionException(String message) {
        super(message);
    }

    public InvalidQuantityIncrementPrecisionException(Throwable cause) {
        super(cause);
    }

    @Override
    public InvalidQuantityIncrementPrecisionException setCxId(String cxId) {
        super.setCxId(cxId);
        return this;
    }

    @Override
    public InvalidQuantityIncrementPrecisionException setResponseStatusCode(Integer responseStatusCode) {
        super.setResponseStatusCode(responseStatusCode);
        return this;
    }

    @Override
    public InvalidQuantityIncrementPrecisionException setExchangeMessage(String exchangeMessage) {
        super.setExchangeMessage(exchangeMessage);
        return this;
    }

    @Override
    public InvalidQuantityIncrementPrecisionException setExchangeErrorCode(String exchangeErrorCode) {
        super.setExchangeErrorCode(exchangeErrorCode);
        return this;
    }

    @Override
    public InvalidQuantityIncrementPrecisionException setCurrencyPair(CurrencyPair currencyPair) {
        super.setCurrencyPair(currencyPair);
        return this;
    }

    @Override
    public InvalidQuantityIncrementPrecisionException setOrderPrice(BigDecimal orderPrice) {
        super.setOrderPrice(orderPrice);
        return this;
    }

    @Override
    public InvalidQuantityIncrementPrecisionException setOrderQuantity(BigDecimal orderQuantity) {
        super.setOrderQuantity(orderQuantity);
        return this;
    }

    @Override
    public InvalidQuantityIncrementPrecisionException setBaseCurrency(String baseCurrency) {
        super.setBaseCurrency(baseCurrency);
        return this;
    }

    @Override
    public InvalidQuantityIncrementPrecisionException setQuoteCurrency(String quoteCurrency) {
        super.setQuoteCurrency(quoteCurrency);
        return this;
    }
}