package cttk.impl.bitfinex.stream.response;

import static cttk.impl.bitfinex.BitfinexUtils.toStdCurrencySymbol;
import static cttk.util.StringUtils.parseBigDecimal;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import cttk.CXIds;
import cttk.dto.Balance;
import cttk.dto.Balances;

public class BitfinexUserDataWalletsSnapshotResponse
    extends ArrayList<ArrayList<String>>
{
    public Balances toBalances() {
        final List<Balance> balanceList = this.stream()
            .map(i -> {
                Balance balance = new Balance();

                final String type = i.get(0);
                final String currency = toStdCurrencySymbol(i.get(1));
                final BigDecimal total = parseBigDecimal(i.get(2));
                final BigDecimal available = parseBigDecimal(i.get(4));

                balance.setCxId(CXIds.BITFINEX);
                balance.setType(type);
                balance.setCurrency(currency);
                balance.setTotal(total);
                if (available != null) {
                    balance.setAvailable(available);
                }
                if (total != null && available != null) {
                    balance.setInUse(total.subtract(available));
                }

                return balance;
            }).collect(Collectors.toList());

        return new Balances().setBalances(balanceList);
    }
}
