package cttk.impl.upbit;

import cttk.CXIds;
import cttk.impl.AbstractOrderVerifierTest;

public class UpbitOrderVerifierTest
    extends AbstractOrderVerifierTest
{
    @Override
    public String getCxId() {
        return CXIds.UPBIT;
    }
}
