package cttk.impl.bitfinex.response;

import static cttk.impl.bitfinex.BitfinexUtils.toBigDecimal;

import java.util.ArrayList;

import cttk.CurrencyPair;
import cttk.dto.Ticker;
import cttk.impl.bitfinex.BitfinexUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BitfinexTickersResponse
    extends ArrayList<Object>
{
    public Ticker toTicker() {
        return new Ticker()
            .setCurrencyPair(parseMarketSymbol((String) this.get(0)))
            .setHighestBid(toBigDecimal(this.get(1)))
            .setLowestAsk(toBigDecimal(this.get(3)))
            .setChange(toBigDecimal(this.get(5)))
            .setChangePercent(MathUtils.multiply100(toBigDecimal(this.get(6))))
            .setLastPrice(toBigDecimal(this.get(7)))
            .setVolume(toBigDecimal(this.get(8)))
            .setHighPrice(toBigDecimal(this.get(9)))
            .setLowPrice(toBigDecimal(this.get(10)));
    }

    private CurrencyPair parseMarketSymbol(String symbol) {
        return BitfinexUtils.parseMarketSymbolWithT(symbol);
    }
}