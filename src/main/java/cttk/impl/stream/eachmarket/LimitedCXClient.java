package cttk.impl.stream.eachmarket;

import static java.util.stream.Collectors.toList;

import java.util.List;

import cttk.CurrencyPair;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.ScanDirection;
import cttk.dto.Balance;
import cttk.dto.Balances;
import cttk.dto.MarketInfos;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.exception.CTTKException;
import cttk.impl.util.RetryUtils;
import cttk.impl.util.ThrowableSupplier;
import cttk.impl.util.Timer;
import cttk.request.GetOpenUserOrdersRequest;
import cttk.request.GetUserOrderHistoryRequest;
import lombok.extern.slf4j.Slf4j;

/**
 * Wrapper client of PublicCXClient and PrivateCXClient.
 * Also enhance functionalities such as rate limit, retry request.
 * @author	Hyunsu Lee
 * @since	2018-10-01
 */
@Slf4j
public class LimitedCXClient {
    private final PublicCXClient publicClient;
    private final PrivateCXClient privateClient;
    private final int maxPollRetryCount;
    private final RateLimit rateLimit;
    private final Timer timer;
    private int counter;

    public LimitedCXClient(PublicCXClient publicClient, PrivateCXClient privateClient, int maxPollRetryCount, RateLimit rateLimit) {
        this.publicClient = publicClient;
        this.privateClient = privateClient;
        this.maxPollRetryCount = maxPollRetryCount;
        this.rateLimit = rateLimit;
        this.timer = new Timer();
        this.counter = -1;
    }

    public PrivateCXClient getPrivateClient() {
        return privateClient;
    }

    public String getCxId() {
        return publicClient.getCxId();
    }

    public boolean isExistsOpenOrders(CurrencyPair pair)
        throws CTTKException
    {
        return getOpenUserOrders(pair).isNotEmpty();
    }

    public MarketInfos getMarketInfos()
        throws CTTKException
    {
        return getDataWithRetry(() -> publicClient.getMarketInfos());
    }

    public UserOrders getOpenUserOrders(CurrencyPair pair)
        throws CTTKException
    {
        GetOpenUserOrdersRequest request = new GetOpenUserOrdersRequest().setCurrencyPair(pair);
        return getDataWithRetryAndCheckRateLimit(1, () -> privateClient.getOpenUserOrders(request));
    }

    public Balances getBalances()
        throws CTTKException
    {
        return getDataWithRetryAndCheckRateLimit(1, () -> privateClient.getAllBalances());
    }

    public UserOrders getUserOrderHistory(UserOrder fromOrder, CurrencyPair pair)
        throws CTTKException
    {
        GetUserOrderHistoryRequest request = new GetUserOrderHistoryRequest()
            .setStartDateTime(fromOrder.getDateTime())
            .setFromId(fromOrder.getOid())
            .setScanDirection(ScanDirection.PREV)
            .setCurrencyPair(pair);
        return getDataWithRetryAndCheckRateLimit(1, () -> privateClient.getUserOrderHistory(request));
    }

    public List<CurrencyPair> getAllCurrencyPairs()
        throws CTTKException
    {
        return getMarketInfos()
            .getMarketInfos()
            .stream()
            .map(CurrencyPair::toSimpleCurrencyPair)
            .collect(toList());
    }

    public List<String> getCurrenciesHaveBalance()
        throws CTTKException
    {
        return getBalances()
            .getBalances()
            .stream()
            .filter(Balance::isTotalExists)
            .map(Balance::getCurrency)
            .collect(toList());
    }

    private <T> T getDataWithRetry(ThrowableSupplier<T> supplier)
        throws CTTKException
    {
        return RetryUtils.getDataWithRetry(maxPollRetryCount, supplier);
    }

    public <T> T getDataWithRetryAndCheckRateLimit(int incremented, ThrowableSupplier<T> supplier)
        throws CTTKException
    {
        checkRateLimitAndSleep(incremented);
        return RetryUtils.getDataWithRetry(maxPollRetryCount, supplier);
    }

    private void checkRateLimitAndSleep(int incremented) {
        final int maxRequest = rateLimit.getMaxRequestCount();
        final int limitTime = rateLimit.getMaxRequestIntervalMillis();

        synchronized (this) {
            int newCounter = counter + incremented;
            if (newCounter >= maxRequest) {
                final long elapsedTime = timer.stop().getIntervalInMilli();
                final long remainingTime = limitTime - elapsedTime;

                log.debug("Elapsed: {}ms, Limit: {}/{}ms, Average: {}ms per 1 request, Start: {}, End: {}",
                    elapsedTime, maxRequest, limitTime, (elapsedTime * .1 / newCounter), timer.getStart(), timer.getEnd());

                if (remainingTime > 0) {
                    log.debug("Sleep: {}ms", remainingTime);

                    sleepQuite(remainingTime);
                }
                newCounter = 0;
                timer.start();
            }
            counter = newCounter;
        }
    }

    private void sleepQuite(long sleepTime) {
        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            log.error("{}", e);
        }
    }
}
