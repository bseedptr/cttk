package cttk.impl.huobi.response;

import java.util.List;
import java.util.stream.Collectors;

import cttk.dto.UserTrades;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HuobiUserTradesResponse
    extends HuobiResponse
{
    List<HuobiUserTrade> data;

    public UserTrades getUserTrades() {
        return new UserTrades()
            .setTrades(data.stream()
                .map(d -> d.getUserTrade())
                .collect(Collectors.toList()));
    }
}
