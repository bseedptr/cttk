package cttk.request;

import cttk.AbstractCurrencyPair;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class GetOrderBookRequest
    extends AbstractCurrencyPair<GetOrderBookRequest>
{
    private Integer count;
    private Integer precision;

    public GetOrderBookRequest refineCountWithMax(int maxCount) {
        if (this.count != null && this.count > maxCount) this.count = maxCount;
        return this;
    }
}
