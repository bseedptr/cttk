package cttk.impl.coinone;

import cttk.CXIds;
import cttk.impl.AbstractUserDataStreamTest;

public class CoinoneUserDataStreamTest
    extends AbstractUserDataStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.COINONE;
    }
}
