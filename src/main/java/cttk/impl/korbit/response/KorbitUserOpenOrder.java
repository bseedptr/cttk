package cttk.impl.korbit.response;

import static cttk.impl.util.DateTimeUtils.zdtFromEpochMilli;

import java.math.BigDecimal;

import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.PricingType;
import cttk.dto.UserOrder;
import cttk.impl.korbit.KorbitUtils;
import cttk.impl.util.MathUtils;
import cttk.impl.util.ZoneIds;
import lombok.Data;

@Data
public class KorbitUserOpenOrder {
    String id;
    Long timestamp;
    String type;
    Quantity price;
    Quantity total;
    Quantity open;
    Quantity native_total;  // currently not used

    public UserOrder toUserOrder() {
        if (id == null) {
            return null;
        }

        return new UserOrder()
            .setCurrencyPair(KorbitUtils.toStdCurrencySymbol(total.getCurrency()), KorbitUtils.toStdCurrencySymbol(price.getCurrency()))
            .setOid(id)
            .setDateTime(zdtFromEpochMilli(timestamp, ZoneIds.SEOUL))
            .setSide(OrderSide.of(type))
            .setPricingType(PricingType.LIMIT)
            .setPrice(price.getValue())
            .setQuantity(total.getValue())
            .setFilledQuantity(MathUtils.subtract(total.getValue(), open.getValue()))
            .setRemainingQuantity(open.getValue())
            .setTotal(MathUtils.multiply(price.getValue(), total.getValue()))
            .setStatusType(getStatusType());
    }

    private OrderStatus getStatusType() {
        if (total.value.compareTo(open.value) == 0) {
            return OrderStatus.UNFILLED;
        } else if (open.value.compareTo(BigDecimal.ZERO) == 0) {
            return OrderStatus.FILLED;
        } else {
            return OrderStatus.PARTIALLY_FILLED;
        }
    }
}

@Data
class Quantity {
    String currency;
    BigDecimal value;
}
