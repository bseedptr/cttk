package cttk.impl.bibox;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import cttk.dto.Ticker;
import cttk.exception.UnsupportedMethodException;
import cttk.request.OpenTickerStreamRequest;
import org.junit.Assert;
import org.junit.Test;

import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreams;
import cttk.dto.OrderBook;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.AbstractCXStreamTest;
import cttk.request.OpenOrderBookStreamRequest;
import cttk.util.ReopenPolicy;

public class BiboxStreamTest
    extends AbstractCXStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.BIBOX;
    }

    @Test
    public void testOrderSnapshotStreamTest()
        throws Exception
    {
        threadSleep();

        final OpenOrderBookStreamRequest request = new OpenOrderBookStreamRequest()
            .setIntervalInMilli(getIntervalInMilli())
            .setCurrencyPair(getCurrencyPair());
        final CountingCXStreamListener<OrderBook> listener = new CountingCXStreamListener<>(request, 1);
        final CXStream<OrderBook> stream = CXStreams.openOrderBookSnapshotStream(getCxId(), request, listener);
        listener.await();
        stream.close();

        Assert.assertTrue(listener.getCount() > 0);
        Assert.assertTrue(listener.hasRequestedData());
    }

    @Test
    public void testInvalidCurrencyPair()
        throws Exception
    {
        threadSleep();

        final OpenTickerStreamRequest request = new OpenTickerStreamRequest()
            .setIntervalInMilli(getIntervalInMilli())
            .setCurrencyPair(getInvalidCurrencyPair());
        final CountingCXStreamListener<Ticker> listener = spy(new CountingCXStreamListener<>(request, 1));
        final CXStream<Ticker> stream = streamOpener.openTickerStream(request, listener);

        listener.await();
        stream.close();

        verify(listener, atLeastOnce()).onOpen(any(CXStream.class));
        verify(listener, atLeastOnce()).onFailure(any(CXStream.class), any(UnsupportedCurrencyPairException.class));
        verify(listener, times(0)).onMessage(any(Ticker.class));
    }
}
