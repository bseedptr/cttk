package cttk.auth;

import cttk.exception.CTTKException;
import cttk.exception.CXAuthorityException;

public interface AuthTokenProvider {
    AuthToken getAuthToken(Credential credential)
        throws CXAuthorityException, CTTKException;

    void refreshAuthToken(Credential credential, AuthToken authToken)
        throws CXAuthorityException, CTTKException;
}
