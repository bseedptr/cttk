package cttk.impl.stream.eachmarket;

public enum PollingLevel {
    LEVEL1, // Open orders O
    LEVEL2, // Open orders X, Balances O
    LEVEL3; // Open orders X, Balances X
}
