package cttk;

import java.util.HashMap;
import java.util.Map;

import cttk.auth.Credential;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCXException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.PostSettingPrivateCXClient;
import cttk.impl.PostSettingPublicCXClient;
import cttk.impl.stream.PollingCXStreamOpener;
import cttk.impl.stream.PostSettingCXStreamOpener;
import cttk.impl.stream.PostSettingUserDataStreamOpener;

public interface CXClientFactory {

    default PublicCXClient create()
        throws CTTKException
    {
        throw new UnsupportedMethodException();
    }

    default PrivateCXClient create(Credential credential)
        throws CTTKException
    {
        throw new UnsupportedMethodException();
    }

    default CXStreamOpener createCXStreamOpener()
        throws CTTKException
    {
        throw new UnsupportedMethodException();
    }

    default UserDataStreamOpener createUserDataStreamOpener(Credential credential)
        throws CTTKException
    {
        throw new UnsupportedMethodException();
    }

    public static final CXClientFactoryCache CACHE = new CXClientFactoryCache();

    public static PublicCXClient createPublicCXClient(final String cxId)
        throws CTTKException
    {
        try {
            final PublicCXClient client = CACHE.getCXClientFactory(cxId).create();
            if (client == null) {
                throw new UnsupportedCXException().setCxId(cxId);
            }
            return new PostSettingPublicCXClient(client);
        } catch (UnsupportedMethodException e) {
            throw new UnsupportedCXException().setCxId(cxId);
        }
    }

    public static PrivateCXClient createPrivateCXClient(final String cxId, Credential credential)
        throws CTTKException
    {
        try {
            final PrivateCXClient client = CACHE.getCXClientFactory(cxId).create(credential);
            if (client == null) {
                throw new UnsupportedCXException().setCxId(cxId);
            }
            return new PostSettingPrivateCXClient(client);
        } catch (UnsupportedMethodException e) {
            throw new UnsupportedCXException().setCxId(cxId);
        }
    }

    public static CXStreamOpener createCXStreamOpener(final String cxId)
        throws CTTKException
    {
        try {
            final CXStreamOpener opener = CACHE.getCXClientFactory(cxId).createCXStreamOpener();
            if (opener == null) {
                throw new UnsupportedCXException().setCxId(cxId);
            }
            return new PostSettingCXStreamOpener(opener);
        } catch (UnsupportedMethodException e) {
            throw new UnsupportedCXException().setCxId(cxId);
        }
    }

    public static CXStreamOpener createPollingCXStreamOpener(final String cxId)
        throws CTTKException
    {
        return new PollingCXStreamOpener(createPublicCXClient(cxId));
    }

    public static UserDataStreamOpener createUserDataStreamOpener(final String cxId, Credential credential)
        throws CTTKException
    {
        try {
            final UserDataStreamOpener opener = CACHE.getCXClientFactory(cxId).createUserDataStreamOpener(credential);
            if (opener == null) {
                throw new UnsupportedCXException().setCxId(cxId);
            }
            return new PostSettingUserDataStreamOpener(cxId, opener);
        } catch (UnsupportedMethodException e) {
            throw new UnsupportedCXException().setCxId(cxId);
        }
    }

    public static class CXClientFactoryCache {
        private final Map<String, CXClientFactory> MAP = new HashMap<>();

        private CXClientFactoryCache() {
            super();
        }

        public synchronized CXClientFactory getCXClientFactory(String cxId)
            throws UnsupportedCXException, CTTKException
        {
            if (cxId == null || cxId.trim().isEmpty()) {
                throw new UnsupportedCXException()
                    .setCxId(cxId);
            }
            final String upperCxId = cxId.trim().toUpperCase();
            if (!MAP.containsKey(upperCxId)) {
                MAP.put(upperCxId, createCXClientFactory(cxId));
            }

            return MAP.get(upperCxId);
        }

        private static CXClientFactory createCXClientFactory(String cxId)
            throws CTTKException
        {
            try {
                final String factoryClassName = getFactoryClassName(cxId);
                return (CXClientFactory) Class.forName(factoryClassName).newInstance();
            } catch (Exception e) {
                throw new UnsupportedCXException()
                    .setCxId(cxId);
            }
        }

        private static String getFactoryClassName(String cxId) {
            return "cttk.impl." + cxId.trim().toLowerCase() + "."
                + cxId.trim().substring(0, 1).toUpperCase()
                + cxId.trim().substring(1).toLowerCase() + "ClientFactory";
        }
    }
}
