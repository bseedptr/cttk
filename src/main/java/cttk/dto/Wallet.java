package cttk.dto;

import java.util.Arrays;
import java.util.List;

import cttk.AbstractCurrencyHolder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Wallet
    extends AbstractCurrencyHolder<Wallet>
{
    private String wid;
    private List<String> addresses;
    private String destinationTag;

    public Wallet setAddress(String address) {
        addresses = Arrays.asList(address);
        return this;
    }

    public String getAddress() {
        return addresses == null || addresses.isEmpty() ? null : addresses.get(0);
    }
}
