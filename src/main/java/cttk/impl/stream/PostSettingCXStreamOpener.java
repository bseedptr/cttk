package cttk.impl.stream;

import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.CXStreamOpener;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCXException;
import cttk.request.OpenOrderBookStreamRequest;
import cttk.request.OpenTickerStreamRequest;
import cttk.request.OpenTradeStreamRequest;

public class PostSettingCXStreamOpener
    implements CXStreamOpener
{
    private final CXStreamOpener opener;

    public PostSettingCXStreamOpener(CXStreamOpener opener)
        throws UnsupportedCXException
    {
        super();
        if (opener == null) {
            throw new UnsupportedCXException("opener cannot be null");
        }
        this.opener = opener;
    }

    @Override
    public String getCxId() {
        return opener.getCxId();
    }

    @Override
    public CXStream<Ticker> openTickerStream(OpenTickerStreamRequest request, CXStreamListener<Ticker> listener)
        throws CTTKException
    {
        return opener.openTickerStream(request, new PostSettingCXStreamListener<>(opener.getCxId(), request, listener));
    }

    @Override
    public CXStream<OrderBook> openOrderBookStream(OpenOrderBookStreamRequest request, CXStreamListener<OrderBook> listener)
        throws CTTKException
    {
        return opener.openOrderBookStream(request, new PostSettingCXStreamListener<>(opener.getCxId(), request, listener));
    }

    @Override
    public CXStream<Trades> openTradeStream(OpenTradeStreamRequest request, CXStreamListener<Trades> listener)
        throws CTTKException
    {
        return opener.openTradeStream(request, new PostSettingCXStreamListener<>(opener.getCxId(), request, listener));
    }

}
