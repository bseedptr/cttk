package cttk.util;

import cttk.OrderVerificationResult;
import cttk.OrderVerifier;
import cttk.OrderVerifierFactory;
import cttk.dto.MarketInfo;
import cttk.dto.MarketInfos;
import cttk.exception.CTTKException;
import cttk.exception.InvalidOrderException;
import cttk.request.CreateOrderRequest;

import java.util.Objects;

public class CTTKExceptionUtils {
    public static InvalidOrderException createByOrderVerifier(CreateOrderRequest request, String message, String cxId, MarketInfos marketInfos)
        throws CTTKException
    {
        MarketInfo marketInfo = marketInfos.getMarketInfo(request);
        if (Objects.isNull(marketInfo)) return null;

        OrderVerifier orderVerifier = OrderVerifierFactory.create(cxId, marketInfo);
        OrderVerificationResult result = orderVerifier.verify(request);

        if (!result.isValid()) {
            return result.getReasons().get(0).createException(request, message);
        }
        return null;
    }
}
