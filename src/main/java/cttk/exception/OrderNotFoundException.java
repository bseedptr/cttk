package cttk.exception;

public class OrderNotFoundException
    extends CXAPIRequestException
{

    public OrderNotFoundException() {
        super();
    }

    public OrderNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public OrderNotFoundException(String message) {
        super(message);
    }

    public OrderNotFoundException(Throwable cause) {
        super(cause);
    }

    @Override
    public OrderNotFoundException setCxId(String cxId) {
        super.setCxId(cxId);
        return this;
    }

    @Override
    public OrderNotFoundException setResponseStatusCode(Integer responseStatusCode) {
        super.setResponseStatusCode(responseStatusCode);
        return this;
    }

    @Override
    public OrderNotFoundException setExchangeMessage(String exchangeMessage) {
        super.setExchangeMessage(exchangeMessage);
        return this;
    }
}
