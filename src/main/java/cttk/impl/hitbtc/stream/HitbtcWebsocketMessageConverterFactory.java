package cttk.impl.hitbtc.stream;

import java.io.IOException;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import cttk.CurrencyPair;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Trades;
import cttk.impl.hitbtc.stream.response.HitbtcWebsocketOrderBookResponse;
import cttk.impl.hitbtc.stream.response.HitbtcWebsocketTickerResponse;
import cttk.impl.hitbtc.stream.response.HitbtcWebsocketTradesResponse;
import cttk.impl.stream.MessageConverter;

public class HitbtcWebsocketMessageConverterFactory {

    private static final ObjectMapper createObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper;
    }

    public static MessageConverter<Ticker> createTickerMessageConverter(CurrencyPair currencyPair) {
        return new MessageConverter<Ticker>() {
            private final ObjectMapper mapper = createObjectMapper();

            @Override
            public Ticker convert(String message)
                throws IOException
            {
                return mapper.readValue(message, HitbtcWebsocketTickerResponse.class).toTicker(currencyPair);
            }
        };
    }

    public static MessageConverter<OrderBook> createOrderBookMessageConverter(CurrencyPair currencyPair) {
        return new MessageConverter<OrderBook>() {
            private final ObjectMapper mapper = createObjectMapper();

            @Override
            public OrderBook convert(String message)
                throws IOException
            {
                return mapper.readValue(message, HitbtcWebsocketOrderBookResponse.class).toOrderBook(currencyPair);
            }
        };
    }

    public static MessageConverter<Trades> createTradesMessageConverter(CurrencyPair currencyPair) {
        return new MessageConverter<Trades>() {
            private final ObjectMapper mapper = createObjectMapper();

            @Override
            public Trades convert(String message)
                throws IOException
            {
                return mapper.readValue(message, HitbtcWebsocketTradesResponse.class).toTrades(currencyPair);
            }
        };
    }
}
