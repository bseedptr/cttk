package cttk.impl.upbit.stream;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.OrderStatus;
import cttk.PrivateCXClient;
import cttk.UserData;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.exception.CTTKException;
import cttk.impl.stream.PollingUserDataThread;
import cttk.request.GetUserOrderRequest;
import cttk.request.OpenUserDataStreamRequest;

public class UpbitPollingUserDataThread
    extends PollingUserDataThread
{
    UpbitPollingUserDataThread(
        final CXStream<UserData> cxStream,
        final PrivateCXClient client,
        final OpenUserDataStreamRequest request,
        final CXStreamListener<UserData> listener)
    {
        super(cxStream, client, request, listener);
    }

    @Override
    protected List<UserOrder> getUpdatedCloseOrders(List<UserOrder> orders) {
        if (orders.size() == 1) return getCloseUserOrder(orders.get(0));
        UserOrders filledUserOrder = getUserOrderHistory(OrderStatus.FILLED);
        if (filledUserOrder.isEmpty()) {
            statusUpdatedUserOrderFailList.addAll(orders);
            return Collections.emptyList();
        }
        UserOrdersFindResult filledOrderResult = UserOrdersFindResult.findInSearchTargetList(orders, filledUserOrder.getList());
        if (filledOrderResult.existMissingOrder()) {
            return filteringCanceledOrders(filledOrderResult);
        }
        return filledOrderResult.getFoundOrders();
    }

    private List<UserOrder> getCloseUserOrder(UserOrder userOrder) {
        try {
            // Upbit GetUserOrderRequest.setWithTrades not working 
            UserOrder updatedUserOrder = userOrder.getStatusType() != OrderStatus.UNFILLED ? userOrder : getDataWithRetry(() -> client.getUserOrder(GetUserOrderRequest.from(userOrder)));
            return Collections.singletonList(updatedUserOrder);
        } catch (CTTKException e) {
            logger.error("Unable to get updated order status {}", userOrder, e);
            statusUpdatedUserOrderFailList.add(userOrder);
            return Collections.emptyList();
        }
    }

    private List<UserOrder> filteringCanceledOrders(UserOrdersFindResult filledOrderResult) {
        UserOrders canceledUserOrder = getUserOrderHistory(OrderStatus.CANCELED);
        if (canceledUserOrder.isEmpty()) {
            statusUpdatedUserOrderFailList.addAll(filledOrderResult.getMissingOrders());
            return filledOrderResult.getFoundOrders();
        }
        UserOrdersFindResult canceledOrderResult = UserOrdersFindResult.findInSearchTargetList(filledOrderResult.getMissingOrders(), canceledUserOrder.getList());
        if (canceledOrderResult.existMissingOrder()) {
            statusUpdatedUserOrderFailList.addAll(canceledOrderResult.getMissingOrders());
        }
        return Stream.concat(filledOrderResult.getFoundOrders().stream(), canceledOrderResult.getFoundOrders().stream()).collect(Collectors.toList());
    }
}
