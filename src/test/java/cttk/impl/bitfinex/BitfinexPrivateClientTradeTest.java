package cttk.impl.bitfinex;

import static cttk.test.TestUtils.hasTestProfile;

import java.io.IOException;

import org.junit.Test;

import cttk.CXIds;
import cttk.OrderSide;
import cttk.dto.UserOrder;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.AbstractPrivateCXClientTradeTest;
import cttk.impl.util.ExNotRaisedException;
import cttk.request.CancelOrderRequest;
import cttk.request.CreateMarketPriceOrderRequest;

public class BitfinexPrivateClientTradeTest
    extends AbstractPrivateCXClientTradeTest
{
    private static final String MIN_QUANTITY = "0.06";

    @Override
    public String getCxId() {
        return CXIds.BITFINEX;
    }

    @Override
    protected long getThreadSleepMillis() {
        return 8000l;
    }

    @Test
    public void testMarketBuy()
        throws CTTKException, UnsupportedCurrencyPairException, IOException
    {
        threadSleep();
        org.junit.Assume.assumeTrue(hasTestProfile("test-market-trade"));
        UserOrder userOrder = client.createMarketPriceOrder(
            new CreateMarketPriceOrderRequest()
                .setCurrencyPair(getValidCurrencyPair())
                .setQuantity(MIN_QUANTITY)
                .setSide(OrderSide.BUY));

        logger.info("Order result\n"
            + "    ORDER: {}", userOrder);

        client.cancelOrder(new CancelOrderRequest()
            .setOrderId(userOrder.getOid()));
    }

    @Test
    public void testMarketSell()
        throws CTTKException, UnsupportedCurrencyPairException, IOException
    {
        threadSleep();
        org.junit.Assume.assumeTrue(hasTestProfile("test-market-trade"));
        UserOrder userOrder = client.createMarketPriceOrder(
            new CreateMarketPriceOrderRequest()
                .setCurrencyPair(getValidCurrencyPair())
                .setQuantity(MIN_QUANTITY)
                .setSide(OrderSide.SELL));

        logger.info("Order result\n"
            + "    ORDER: {}", userOrder);

        client.cancelOrder(new CancelOrderRequest()
            .setOrderId(userOrder.getOid()));
    }

    @Override
    @Test(expected = ExNotRaisedException.class)
    public void testInvalidPriceIncrementPrecision()
        throws CTTKException
    {
        threadSleep();
        // Refer CTTK-493 for detailed explanation
        // Bitfinex automatically truncates the order with invalid precision
        super.testInvalidPriceIncrementPrecision();
    }

    @Override
    @Test(expected = ExNotRaisedException.class)
    public void testInvalidQuantityIncrementPrecision()
        throws CTTKException
    {
        threadSleep();
        // Refer CTTK-493 for detailed explanation
        // Bitfinex automatically truncates the order with invalid precision
        super.testInvalidQuantityIncrementPrecision();
    }
}
