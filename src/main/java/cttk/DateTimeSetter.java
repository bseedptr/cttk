package cttk;

import java.time.ZonedDateTime;

public interface DateTimeSetter {
    default <T extends DateTimeHolder<T>> T setDateTime(T obj) {
        if (obj != null && obj.getDateTime() == null) {
            obj.setDateTime(ZonedDateTime.now());
        }
        return obj;
    }
}
