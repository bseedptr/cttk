package cttk.exception;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class InvalidWithdrawException
    extends CXAPIRequestException
{
    private static final long serialVersionUID = 1L;

    protected String toAddress;
    protected String withdrwalKey; // Kraken specific
    protected String withdrawalCurrency;
    protected BigDecimal withdrawalAmount;

    public InvalidWithdrawException() {
        super();
    }

    public InvalidWithdrawException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidWithdrawException(String message) {
        super(message);
    }

    public InvalidWithdrawException(Throwable cause) {
        super(cause);
    }

    @Override
    public String toString() {
        return super.toString()
            + ", To Address = " + toAddress
            + ", Withdrawal Key = " + withdrwalKey
            + ", Withdrawal Currency = " + withdrawalCurrency
            + ", Withdrawal Quantity = " + withdrawalAmount;
    }

    @Override
    public InvalidWithdrawException setCxId(String cxId) {
        super.setCxId(cxId);
        return this;
    }

    @Override
    public InvalidWithdrawException setResponseStatusCode(Integer responseStatusCode) {
        super.setResponseStatusCode(responseStatusCode);
        return this;
    }

    @Override
    public InvalidWithdrawException setExchangeMessage(String exchangeMessage) {
        super.setExchangeMessage(exchangeMessage);
        return this;
    }
}
