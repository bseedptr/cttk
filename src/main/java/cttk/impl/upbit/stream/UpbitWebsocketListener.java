package cttk.impl.upbit.stream;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;

import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.impl.stream.AbstractWebsocketListener;
import cttk.impl.stream.MessageConverter;
import cttk.impl.upbit.UpbitUtils;
import cttk.request.AbstractCXStreamRequest;
import okhttp3.Response;
import okhttp3.WebSocket;
import okio.ByteString;

public class UpbitWebsocketListener<T, R extends AbstractCXStreamRequest<R>>
    extends AbstractWebsocketListener<T, R>
{
    public UpbitWebsocketListener(
        final CXStream<T> stream,
        final String channel,
        final R request,
        final MessageConverter<T> converter,
        final CXStreamListener<T> listener)
    {
        super(stream, channel, request, converter, listener, CXIds.UPBIT);
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        webSocket.send(getSubscriptionPayload());
        logger.info("[{}] Open : {}", id, channel);
        this.listener.onOpen(stream);
    }

    public String getSubscriptionPayload() {
        // TODO support precision
        return "[{\"ticket\": \"" + id + "\"}, {\"type\": \"" + channel + "\", \"codes\": [\"" + UpbitUtils.getMarketSymbol(request) + "\"]}]";
    }

    @Override
    protected void handleMessage(WebSocket webSocket, String text)
        throws Exception
    {
        logger.debug("{}", text);
        UpbitUtils.checkErrorByResponseMessage(request, text, null);
        T message = converter.convert(text);
        if (message != null) listener.onMessage(message);
    }

    @Override
    protected String toString(ByteString bytes)
        throws Exception
    {
        StringWriter sw = new StringWriter();
        try (
            final PrintWriter pw = new PrintWriter(sw);
            final ByteArrayInputStream in = new ByteArrayInputStream(bytes.toByteArray());
            final BufferedReader br = new BufferedReader(new InputStreamReader(in)))
        {
            String line;
            for (int i = 0; (line = br.readLine()) != null; i++) {
                if (i > 0) pw.println();
                pw.print(line);
            }
        }
        return sw.toString();
    }
}
