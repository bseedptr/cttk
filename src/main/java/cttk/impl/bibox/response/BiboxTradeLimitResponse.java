package cttk.impl.bibox.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import cttk.CurrencyPair;

import cttk.dto.MarketInfo;
import cttk.util.StringUtils;
import lombok.Data;

import java.math.BigDecimal;
import java.util.HashMap;

public class BiboxTradeLimitResponse extends BiboxResultResponse<BiboxTradeLimit> {
    public BigDecimal getMinPrice(CurrencyPair currencyPair) {
        if (result.getMinPrices().containsKey(currencyPair.getQuoteCurrency())) {
            return StringUtils.parseBigDecimal(result.getMinPrices().get(currencyPair.getQuoteCurrency()));
        } else if (result.getMinPrices().containsKey("default")) {
            return StringUtils.parseBigDecimal(result.getMinPrices().get("default"));
        }
        return null;
    }

    public BigDecimal getMinQuantity(CurrencyPair currencyPair) {
        if (result.getMinQuantities().containsKey(currencyPair.getQuoteCurrency())) {
            return StringUtils.parseBigDecimal(result.getMinQuantities().get(currencyPair.getQuoteCurrency()));
        } else if (result.getMinQuantities().containsKey("default")) {
            return StringUtils.parseBigDecimal(result.getMinQuantities().get("default"));
        }
        return null;
    }

    public BigDecimal getMinTotal(CurrencyPair currencyPair) {
        if (result.getMinTotals().containsKey(currencyPair.getQuoteCurrency())) {
            return StringUtils.parseBigDecimal(result.getMinTotals().get(currencyPair.getQuoteCurrency()));
        } else if (result.getMinTotals().containsKey("default")) {
            return StringUtils.parseBigDecimal(result.getMinTotals().get("default"));
        }
        return null;
    }
}

@Data
class BiboxTradeLimit {
    @JsonProperty("min_trade_price")
    HashMap<String, String> minPrices;
    @JsonProperty("min_trade_amount")
    HashMap<String, String> minQuantities;
    @JsonProperty("min_trade_money")
    HashMap<String, String> minTotals;
}
