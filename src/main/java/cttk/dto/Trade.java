package cttk.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.ZonedDateTime;

import cttk.DateTimeHolder;
import cttk.FillType;
import cttk.OrderSide;
import cttk.PricedQuantity;
import cttk.PricingType;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Trade
    implements DateTimeHolder<Trade>, PricedQuantity
{
    BigInteger sno;

    ZonedDateTime dateTime;
    String gtid;
    String tid;
    OrderSide side;
    PricingType pricingType;
    FillType fillType;
    BigDecimal quantity;
    BigDecimal price;
    BigDecimal total;
}
