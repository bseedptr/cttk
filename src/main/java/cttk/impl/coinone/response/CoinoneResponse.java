package cttk.impl.coinone.response;

import lombok.Data;

@Data
public class CoinoneResponse {
    String result;
    String errorCode;
}
