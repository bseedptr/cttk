package cttk.impl.korbit.response;

import cttk.dto.UserOrder;
import lombok.Data;

@Data
public class KorbitCreateOrderResponse {
    String orderId;
    String status;
    String currency_pair;

    public UserOrder toUserOrder() {
        return new UserOrder()
            .setOid(orderId)
            .setStatus(status);
    }
}