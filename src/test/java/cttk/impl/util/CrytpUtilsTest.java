package cttk.impl.util;

import org.junit.Assert;
import org.junit.Test;

import cttk.impl.util.CryptoUtils;

public class CrytpUtilsTest {
    @Test
    public void test()
        throws Exception
    {
        final String data = "data";
        final String key = "secretKey";

        final String expectedHex = "3b7b934f4e33962b2a7dde351b50d7a177c192f2b5b30d6fd543fdaa645fbfef1e5cd1a46b8f3785f6ceea9d72112ee5654d3e74aa3875c76525223d35c78f42";
        final String hex = CryptoUtils.hexString(CryptoUtils.hmacSha512(data, key));

        Assert.assertEquals(expectedHex, hex);

        final String expectedBase64 = "M2I3YjkzNGY0ZTMzOTYyYjJhN2RkZTM1MWI1MGQ3YTE3N2MxOTJmMmI1YjMwZDZmZDU0M2ZkYWE2NDVmYmZlZjFlNWNkMWE0NmI4ZjM3ODVmNmNlZWE5ZDcyMTEyZWU1NjU0ZDNlNzRhYTM4NzVjNzY1MjUyMjNkMzVjNzhmNDI=";
        final String base64 = CryptoUtils.base64(hex);
        Assert.assertEquals(expectedBase64, base64);

    }
}
