package cttk.impl.bithumb;

import cttk.CXIds;
import cttk.impl.AbstractPollingCXStreamTest;

public class BithumbPollingStreamTest
    extends AbstractPollingCXStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.BITHUMB;
    }
}
