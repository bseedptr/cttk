package cttk.impl.binance;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.ZonedDateTime;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import cttk.CXClientFactory;
import cttk.PublicCXClient;
import cttk.auth.Credential;
import cttk.dto.Transfers;
import cttk.dto.UserOrders;
import cttk.dto.UserTrades;
import cttk.exception.CTTKException;
import cttk.impl.AbstractPrivateClientDeltaTest;
import cttk.request.GetTransfersDeltaRequest;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserOrderHistoryDeltaRequest;
import cttk.request.GetUserOrderHistoryRequest;
import cttk.request.GetUserTradesDeltaRequest;
import cttk.request.GetUserTradesRequest;
import lombok.extern.slf4j.Slf4j;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CXClientFactory.class)
@PowerMockIgnore("javax.net.ssl.*")
@Slf4j
public class BinancePrivateClientDeltaTest
    extends AbstractPrivateClientDeltaTest
{
    BinancePrivateClient client;
    GetUserOrderHistoryDeltaRequest orderHistoryRequest;
    GetUserTradesDeltaRequest tradesRequest;
    GetTransfersDeltaRequest transferRequest;

    @Mock
    UserOrders orders1, orders2;
    @Mock
    UserTrades trades1, trades2;

    @Before
    public void setup()
        throws CTTKException
    {
        client = PowerMockito.spy(createDummyPrivateClient());
        orderHistoryRequest = new GetUserOrderHistoryDeltaRequest().setStartDateTime(ZonedDateTime.now().minusYears(2));
        tradesRequest = new GetUserTradesDeltaRequest().setStartDateTime(ZonedDateTime.now().minusYears(2));
        transferRequest = new GetTransfersDeltaRequest().setStartDateTime(ZonedDateTime.now().minusYears(2));
    }

    BinancePrivateClient createDummyPrivateClient()
        throws CTTKException
    {
        PowerMockito.mockStatic(CXClientFactory.class);
        PowerMockito.when(CXClientFactory.createPublicCXClient(anyString())).thenReturn(mock(PublicCXClient.class));
        return new BinancePrivateClient(mock(Credential.class));
    }

    @Test
    public void testGetUserOrderHistoryDeltaWhenNoMoreData()
        throws Exception
    {
        when(orders1.getList()).thenReturn(makeUserOrderFixtures("1", "2", "3"));
        when(orders1.isEmpty()).thenReturn(false);
        when(orders2.getList()).thenReturn(makeUserOrderFixtures());
        when(orders2.isEmpty()).thenReturn(true);
        doReturn(orders1, orders2).when(client).getUserOrderHistory(any(GetUserOrderHistoryRequest.class));

        UserOrders result = client.getUserOrderHistory(orderHistoryRequest);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat(ids, containsInAnyOrder("1", "2", "3"));
    }

    @Test
    public void testGetUserTradesDeltaWhenNoMoreData()
        throws Exception
    {
        when(trades1.getList()).thenReturn(makeUserTradeFixtures("1", "2", "3"));
        when(trades1.isEmpty()).thenReturn(false);
        when(trades2.getList()).thenReturn(makeUserTradeFixtures());
        when(trades2.isEmpty()).thenReturn(true);
        doReturn(trades1, trades2).when(client).getUserTrades(any(GetUserTradesRequest.class));

        UserTrades result = client.getUserTrades(tradesRequest);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat(ids, containsInAnyOrder("1", "2", "3"));
    }

    @Test
    public void testGetTransfersDeltaWhenNoMoreData()
        throws Exception
    {
        Transfers depositTransfers = mock(Transfers.class);
        Transfers withdrawTransfers = mock(Transfers.class);
        when(depositTransfers.isEmpty()).thenReturn(true);
        when(withdrawTransfers.isEmpty()).thenReturn(true);
        doReturn(depositTransfers).when(client).getDepositHistory(any(GetTransfersRequest.class));
        doReturn(withdrawTransfers).when(client).getWithdrawHistory(any(GetTransfersRequest.class));

        Transfers result = client.getTransfers(transferRequest);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat(ids, hasSize(0));
    }

    @Test
    public void testGetTransfersDeltaWhenDuplication()
        throws Exception
    {
        Transfers depositTransfers1 = mock(Transfers.class);
        when(depositTransfers1.getLatest()).thenReturn(makeTransferFixture("3"));
        when(depositTransfers1.getList()).thenReturn((makeTransferFixtures("1", "2", "3")));
        Transfers depositTransfers2 = mock(Transfers.class);
        when(depositTransfers2.getLatest()).thenReturn(makeTransferFixture("5"));
        when(depositTransfers2.getList()).thenReturn(makeTransferFixtures("3", "4", "5"));
        Transfers withdrawTransfers1 = mock(Transfers.class);
        when(withdrawTransfers1.getLatest()).thenReturn(makeTransferFixture("103"));
        when(withdrawTransfers1.getList()).thenReturn(makeTransferFixtures("101", "102", "103"));
        Transfers withdrawTransfers2 = mock(Transfers.class);
        when(withdrawTransfers2.getLatest()).thenReturn(makeTransferFixture("105"));
        when(withdrawTransfers2.getList()).thenReturn(makeTransferFixtures("103", "104", "105"));
        doReturn(depositTransfers1, depositTransfers2).when(client).getDepositHistory(any(GetTransfersRequest.class));
        doReturn(withdrawTransfers1, withdrawTransfers2).when(client).getWithdrawHistory(any(GetTransfersRequest.class));

        GetTransfersDeltaRequest request = new GetTransfersDeltaRequest().setStartDateTime(ZonedDateTime.now());
        Transfers result = client.getTransfers(request);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat("Id should be unique.", ids, containsInAnyOrder("1", "2", "3", "4", "5", "101", "102", "103", "104", "105"));
    }
}
