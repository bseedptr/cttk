package cttk.dto;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cttk.AbstractCXIdHolder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ToString(callSuper = true)
public class Balances
    extends AbstractCXIdHolder<Balances>
{
    List<Balance> balances;
    ZonedDateTime updatedDateTime;

    public int size() {
        return balances == null ? 0 : balances.size();
    }

    public boolean hasBalances() {
        return balances != null && !balances.isEmpty();
    }

    @JsonIgnore
    public Set<String> getValidCurrencySet() {
        return balances == null
            ? Collections.emptySet()
            : balances
                .stream()
                .filter(i -> i.getTotal().compareTo(BigDecimal.ZERO) > 0)
                .map(i -> i.getCurrency())
                .distinct()
                .collect(Collectors.toSet());
    }
}
