package cttk.impl.coinone.response;

import static cttk.impl.coinone.CoinoneUtils.toStdCurrencySymbol;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import cttk.TransferType;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.ZoneIds;
import cttk.request.GetTransfersRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CoinoneTransfersResponse
    extends CoinoneResponse
{
    List<CoinoneTransfer> transactions;

    public Transfers toTransfers(GetTransfersRequest request) {
        return new Transfers()
            .setTransfers(toTransferList(toStdCurrencySymbol(request.getCurrency()), transactions));
    }

    private List<Transfer> toTransferList(String currency, List<CoinoneTransfer> transactions) {
        if (transactions == null) return null;
        return transactions.stream().map(i -> i.toTransfer(currency)).collect(Collectors.toList());
    }
}

@Data
class CoinoneTransfer {
    String txid;
    String type;
    String from;
    String to;
    Integer confirmations;
    BigDecimal quantity;
    Long timestamp;

    public Transfer toTransfer(String currency) {
        return new Transfer()
            .setCurrency(currency)
            .setType(TransferType.of(type))
            .setTid(txid)
            .setTxId(txid)
            .setAmount(quantity)
            .setFromAddress(from)
            .setAddress(to)
            .setUpdatedDateTime(DateTimeUtils.zdtFromEpochSecond(timestamp, ZoneIds.SEOUL))
            .setCompletedDateTime(DateTimeUtils.zdtFromEpochSecond(timestamp, ZoneIds.SEOUL))
            .setConfirmations(confirmations);
    }
}