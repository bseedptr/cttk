package cttk.impl.upbit;

import cttk.CXClientFactory;
import cttk.CXStreamOpener;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.UserDataStreamOpener;
import cttk.auth.Credential;
import cttk.impl.RetryPrivateCXClient;
import cttk.impl.upbit.stream.UpbitStreamOpener;
import cttk.impl.upbit.stream.UpbitUserDataStreamOpener;

public class UpbitClientFactory
    implements CXClientFactory
{
    @Override
    public PublicCXClient create() {
        return new UpbitPublicClient();
    }

    @Override
    public PrivateCXClient create(Credential credential) {
        return new RetryPrivateCXClient(5, 5, new UpbitPrivateClient(credential), "이미 사용된 임시 번호입니다");
    }

    @Override
    public CXStreamOpener createCXStreamOpener() {
        return new UpbitStreamOpener();
    }

    @Override
    public UserDataStreamOpener createUserDataStreamOpener(Credential credential) {
        return new UpbitUserDataStreamOpener(create(credential));
    }
}
