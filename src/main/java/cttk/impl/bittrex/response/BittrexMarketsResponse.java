package cttk.impl.bittrex.response;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.dto.MarketInfo;
import cttk.dto.MarketInfos;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BittrexMarketsResponse
    extends BittrexResponse<List<BittrexMarket>>
{
    public MarketInfos toMarketInfos() {
        return new MarketInfos()
            .setMarketInfos(toMarketInfoList());
    }

    private List<MarketInfo> toMarketInfoList() {
        return result == null ? null
            : result.stream()
                .map(i -> i.toMarketInfo())
                .collect(Collectors.toList());
    }
}

@Data
class BittrexMarket {
    @JsonProperty("MarketCurrency")
    String marketCurrency;
    @JsonProperty("BaseCurrency")
    String baseCurrency;
    @JsonProperty("MarketCurrencyLong")
    String marketCurrencyLong;
    @JsonProperty("BaseCurrencyLong")
    String baseCurrencyLong;
    @JsonProperty("MinTradeSize")
    BigDecimal minTradeSize;
    @JsonProperty("MarketName")
    String marketName;
    @JsonProperty("IsActive")
    Boolean isActive;
    @JsonProperty("Created")
    String created;

    public MarketInfo toMarketInfo() {
        return new MarketInfo()
            .setStatus(isActive != null && isActive ? "active" : "inactive")
            .setCurrencyPair(marketCurrency, baseCurrency)
            .setMinQuantity(minTradeSize);
    }
}