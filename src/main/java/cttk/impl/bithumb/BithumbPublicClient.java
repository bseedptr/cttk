package cttk.impl.bithumb;

import static cttk.util.StringUtils.toUpper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import cttk.PublicCXClient;
import cttk.dto.MarketInfo;
import cttk.dto.MarketInfos;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Tickers;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.bithumb.response.BithumbAllTickersResponse;
import cttk.impl.bithumb.response.BithumbOrderBookResponse;
import cttk.impl.bithumb.response.BithumbTickerResponse;
import cttk.impl.bithumb.response.BithumbTradesResponse;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.Objects;
import cttk.impl.util.ParamsBuilder;
import cttk.request.GetOrderBookRequest;
import cttk.request.GetRecentTradesRequest;
import cttk.request.GetTickerRequest;
import cttk.request.GetTradeHistoryRequest;
import cttk.util.StringUtils;
import okhttp3.ConnectionPool;
import okhttp3.Request;
import okhttp3.Response;

public class BithumbPublicClient
    extends AbstractBithumbClient
    implements PublicCXClient
{
    private static final String MAX_QUANTITIES_STRING = "BTC: 300 | ETH: 2,500 | DASH: 4,000 | LTC: 15,000 | ETC: 30,000 | XRP: 2,500,000 | BCH: 1,200 | XMR: 10,000 | ZEC: 2,500 | QTUM: 30,000 | BTG: 1,200 | EOS: 20,000 | ICX: 250,000 | VET: 10,000,000 | TRX: 10,000,000 | ELF: 800,000 | MITH: 1,000,000 | MCO: 100,000 | OMG: 100,000 | KNC: 500,000 | GNT: 1,000,000 | HSR: 100,000 | ZIL: 5,000,000 | ETHOS: 300,000 | PAY: 1,000,000 | WAX: 3,000,000 | POWR: 2,000,000 | LRC: 1,200,000 | GTO: 2,500,000 | STEEM: 250,000 | STRAT: 120,000 | ZRX: 700,000 | REP: 20,000 | AE: 250,000 | XEM: 2,500,000 | SNT: 7,000,000 | ADA: 3,500,000 | PPT: 200,000 | CTXC: 2,700,000 | CMT: 6,000,000 | THETA: 6,000,000 | WTC: 200,000 | ITC: 200,000 | TRUE: 1,500,000 | ABT: 4,800,000 | RNT: 1,000,000 | PLY: 4,000,000 | WAVES: 250,000 | LINK: 200,000 | ENJ: 10,000,000 | PST: 400,000 | SALT: 100,000 | RDN: 1,000,000 | LOOM: 5,000,000 | BHPC: 70,000 | PIVX: 700,000 | INS: 1,000,000 | BCD: 300,000 | BZNT: 10,000,000 | XLM: 2,000,000 | OCN: 50,000,000 | BSV: 6,000 | TMTG: 100,000,000 | BAT: 4,000,000";
    protected final Logger logger = LoggerFactory.getLogger(getClass());
    public static Map<String, BigDecimal> getMaxQuantityMap() {
        final Map<String, BigDecimal> map = new HashMap<>();
        for (String maxStr : MAX_QUANTITIES_STRING.split("[|]")) {
            final String currency = maxStr.substring(0, maxStr.indexOf(':')).trim();
            final BigDecimal maxQuantity = new BigDecimal(maxStr.substring(maxStr.indexOf(':') + 1).replaceAll(",", "").trim());
            map.put(currency, maxQuantity);
            System.out.println(currency + "\t" + maxQuantity);
        }
        return map;
    }

    public BithumbPublicClient() {
        super();
    }

    public BithumbPublicClient(ConnectionPool connectionPool) {
        super(connectionPool);
    }

    @Override
    public MarketInfos getMarketInfos()
        throws CTTKException
    {
        final HttpResponse response = requestGet("public/ticker/ALL");

        checkResponseError(null, response);

        final MarketInfos marketInfos = convert(response, BithumbAllTickersResponse.class, BithumbAllTickersResponse::toMarketInfos);
        fillMarketInfo(marketInfos);
        return marketInfos;
    }

    private void fillMarketInfo(final MarketInfos marketInfos) {
        Map<String, BithumbMarketData> marketData = getMarketData();
        if (marketData.isEmpty()) {
            return;
        }
        Map<String, BigDecimal> maxQuantityMap = getMaxQuantityMap();
        for (MarketInfo marketInfo : marketInfos.getMarketInfos()) {
            BithumbMarketData data = marketData.get(marketInfo.getBaseCurrency());
            if (data != null && "KRW".equals(marketInfo.getQuoteCurrency())) {
                marketInfo.setMinQuantity(data.getTrade().getLimitMin());
                //marketInfo.setQuantityPrecision(data.getLength());
                marketInfo.setQuantityPrecision(4);
                marketInfo.setMaxQuantity(maxQuantityMap.get(marketInfo.getBaseCurrency()));
            }
        }
    }

    private Map<String, BithumbMarketData> getMarketData() {
        String marketDataJson = getMarketDataJsonFromWeb();
        if (StringUtils.isEmpty(marketDataJson)) {
            marketDataJson = getMarketDataJsonFromFile();
        }

        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
        try {
            return mapper.readValue(marketDataJson, new TypeReference<Map<String, BithumbMarketData>>() {});
        } catch (IOException e) {
            logger.error("fail to convert marketJsonString to map", e);
            return Collections.emptyMap();
        }
    }

    private String getMarketDataJsonFromWeb() {
        try {
            Request request = new Request.Builder().url("https://www.bithumb.com/trade/order/BTC").build();
            Response res = this.httpClient.newCall(request).execute();

            String jsVarDefine = "var COIN = ";
            Pattern pattern = Pattern.compile(jsVarDefine + ".*(?=;)", Pattern.MULTILINE);
            Matcher matcher = pattern.matcher(res.body().string());
            if (matcher.find()) {
                return matcher.group().substring(jsVarDefine.length());
            }
        } catch (IOException e) {
            logger.error("fail to get market data from bithumb homepage", e);
        }
        return "";
    }

    private String getMarketDataJsonFromFile() {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(
            Thread.currentThread().getContextClassLoader().getResourceAsStream("data/bithumb-market.json"), StandardCharsets.UTF_8)))
        {
            String line;
            StringBuilder marketDataJsonBuilder = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                marketDataJsonBuilder.append(line);
            }
            return marketDataJsonBuilder.toString();
        } catch (IOException e) {
            logger.error("fail to get market data from bithumb-market.json file", e);
            return "";
        }
    }

    @Override
    public Ticker getTicker(GetTickerRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        checkQuoteKRW(request);
        final HttpResponse response = requestGet("public/ticker/" + toUpper(request.getBaseCurrency()));

        checkResponseError(request, response);

        return convert(response, BithumbTickerResponse.class, (i -> i.toTicker()));
    }

    @Override
    public Tickers getAllTickers()
        throws CTTKException
    {
        final HttpResponse response = requestGet("public/ticker/ALL");

        checkResponseError(null, response);

        return convert(response, BithumbAllTickersResponse.class, (i -> i.toTickers()));
    }

    @Override
    public OrderBook getOrderBook(GetOrderBookRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        checkQuoteKRW(request);
        checkOrderBookGroupOrdersParamValue(request.getPrecision());
        request.refineCountWithMax(MAX_ORDERBOOK_COUNT);

        final HttpResponse response = requestGet("public/orderbook/" + toUpper(request.getBaseCurrency()),
            ParamsBuilder.create()
                .putIfNotNull("count", Objects.nvl(request.getCount(), 50))
                .putIfNotNull("group_orders", request.getPrecision())
                .build());

        checkResponseError(request, response);

        return convert(response, BithumbOrderBookResponse.class, (i -> i.toOrderBook()));
    }

    private void checkOrderBookGroupOrdersParamValue(Integer precision)
        throws CTTKException
    {
        if (precision != null
            && precision != 0
            && precision != 1)
        {
            throw new CTTKException("Invalid precision: " + precision).setCxId(this.getCxId());
        }
    }

    @Override
    public Trades getRecentTrades(GetRecentTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        checkQuoteKRW(request);
        request.refineCountWithMax(MAX_RECENT_TRANSACTION_COUNT);

        final HttpResponse response = requestGet("public/transaction_history/" + toUpper(request.getBaseCurrency()),
            ParamsBuilder.create()
                .putIfNotNull("count", request.getCount())
                .putIfNotNull("cont_no", request.getLastTradeId())
                .build());

        checkResponseError(request, response);

        return convert(response, BithumbTradesResponse.class, (i -> i.toTrades()));
    }

    @Override
    public Trades getTradeHistory(GetTradeHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        checkQuoteKRW(request);
        request.refineCountWithMax(MAX_RECENT_TRANSACTION_COUNT);

        final HttpResponse response = requestGet("public/transaction_history/" + toUpper(request.getBaseCurrency()),
            ParamsBuilder.create()
                .putIfNotNull("count", request.getCount())
                .putIfNotNull("cont_no", request.getLastTradeId())
                .build());

        checkResponseError(request, response);

        return convert(response, BithumbTradesResponse.class, (i -> i.toTrades()));
    }
}
