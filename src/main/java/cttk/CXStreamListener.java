package cttk;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface CXStreamListener<T> {
    default void onOpen(CXStream<T> stream) {
        // do something if necessary
    }

    void onMessage(T message);

    default void onClosing(CXStream<T> stream, int code, String reason) {
        // do something if necessary
    }

    default void onClosed(CXStream<T> stream, int code, String reason) {
        // do something if necessary
    }

    void onFailure(CXStream<T> stream, Throwable t);

    public static <T> CXStreamListener<T> createPrint() {
        return new CXStreamListener<T>() {
            Logger logger = LoggerFactory.getLogger(getClass());

            @Override
            public void onMessage(T message) {
                logger.info("{}", message);
            }

            @Override
            public void onFailure(CXStream<T> stream, Throwable t) {
                logger.error("Stream: {}", stream, t);
            }
        };
    }
}
