package cttk.impl.upbit.response;

import java.math.BigDecimal;

import cttk.OrderSide;
import cttk.PricingType;
import cttk.dto.UserOrder;
import cttk.impl.upbit.UpbitUtils;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;

@Data
public class UpbitCreateOrderResponse {
    String uuid;
    String side;
    String ord_type;
    BigDecimal price;
    BigDecimal avg_price;

    String state;
    String market;
    String created_at;
    BigDecimal volume;
    BigDecimal remaining_volume;
    BigDecimal executed_volume;

    BigDecimal reserved_fee;
    BigDecimal remaining_fee;
    BigDecimal paid_fee;
    BigDecimal locked;

    Integer trade_count;

    public UserOrder toUserOrder() {
        return new UserOrder()
            .setCurrencyPair(UpbitUtils.parseMarketSymbol(market))
            .setDateTime(DateTimeUtils.parseZdt(created_at))
            .setOid(uuid)
            .setSide(OrderSide.of(side))
            .setPrice(price)
            .setStatus(state)
            .setQuantity(volume)
            .setAvgPrice(avg_price)
            .setRemainingQuantity(remaining_volume)
            .setFilledQuantity(executed_volume)
            .setFee(paid_fee)
            .setPricingType(PricingType.of(ord_type))
            .setStatusType(UpbitUtils.getStatusType(state));
    }
}
