package cttk.example;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.List;

import cttk.CTTK;
import cttk.CXClientFactory;
import cttk.CXIds;
import cttk.PrivateCXClient;
import cttk.TransferType;
import cttk.auth.CXCredentialsProvider;
import cttk.dto.Transfers;
import cttk.request.GetTransfersRequest;
import cttk.test.TestConfig;

public class GetUserTransfersExample {

    public static void main(String[] args)
        throws IOException
    {
        List<String> cxIds = CTTK.getSupportedCxIds();

        for (String cxId : cxIds) {
            final TestConfig testConfig = TestConfig.readFromClasspathYaml("config/" + cxId.toLowerCase() + ".yml");

            try {
                final PrivateCXClient client = CXClientFactory.createPrivateCXClient(cxId, CXCredentialsProvider.getDefaultCredential(cxId).getDataKey());
                final GetTransfersRequest request = new GetTransfersRequest()
                    .setCurrency(testConfig.getTransferTestCurrency());
                if (cxId.equals(CXIds.KORBIT)) {
                    request
                        .setType(TransferType.DEPOSIT);
                } else if (cxId.equals(CXIds.HUOBI)) {
                    request
                        .setType(TransferType.DEPOSIT)
                        .setFromId("1");
                } else if (cxId.equals(CXIds.POLONIEX)) {
                    request
                        .setStartDateTime(ZonedDateTime.now().minusYears(2))
                        .setEndDateTime(ZonedDateTime.now());
                }
                final Transfers transfers = client.getTransfers(request);

                Examples.print(cxId, transfers);

            } catch (java.io.FileNotFoundException e) {
                continue;
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
        }
    }

}
