package cttk.impl.poloniex;

import static cttk.OrderSide.BUY;
import static cttk.OrderSide.SELL;

import org.junit.Assert;
import org.junit.Test;

import cttk.CurrencyPair.SimpleCurrencyPair;

public class PoloniexUtilsTest {
    @Test
    public void testGuessFeeCurrencyIfOderSideIsSELLThenQuoteCurrency() {
        SimpleCurrencyPair pair = new SimpleCurrencyPair("EOS", "ETH");
        String feeCurrency = PoloniexUtils.guessFeeCurrency(SELL, pair);

        Assert.assertEquals("Should be 'ETH' if order side is SELL.", pair.getQuoteCurrency(), feeCurrency);
    }

    @Test
    public void testGuessFeeCurrencyIfOderSideIsBUYThenBaseCurrency() {
        SimpleCurrencyPair pair = new SimpleCurrencyPair("EOS", "ETH");
        String feeCurrency = PoloniexUtils.guessFeeCurrency(BUY, pair);

        Assert.assertEquals("Should be 'EOS' if order side is BUY.", pair.getBaseCurrency(), feeCurrency);
    }
}