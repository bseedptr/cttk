package cttk.impl.huobi.response;

import java.util.ArrayList;
import java.util.List;

import cttk.dto.Trade;
import cttk.dto.Trades;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HuobiTradeHistoryResponse
    extends HuobiResponse
{
    List<HuobiTradeTick> data;

    public Trades toTrades() {
        return new Trades()
            .setTrades(getTradeList());
    }

    private List<Trade> getTradeList() {
        if (data == null) return null;
        List<Trade> list = new ArrayList<>();
        data.forEach(i -> {
            list.addAll(i.getTradeList());
        });
        return list;
    }
}
