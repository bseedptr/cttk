package cttk.impl.binance.response;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import cttk.impl.binance.stream.response.BinanceExecutionReportResponse;
import cttk.impl.binance.stream.response.BinanceOutboundAccountInfoResponse;
import cttk.impl.util.JsonUtils;

public class DeserializationTest {
    final ObjectMapper mapper = JsonUtils.createDefaultObjectMapper();

    @Test
    public void testDeserializeExecutionReport()
        throws IOException
    {
        try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("data/binance/executionReport.json")) {
            final BinanceExecutionReportResponse response = mapper.readValue(is, BinanceExecutionReportResponse.class);
            Assert.assertNotNull(response);
        }
    }

    @Test
    public void testDeserializeOutboundAccountInfo()
        throws IOException
    {
        try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("data/binance/outboundAccountInfo.json")) {
            final BinanceOutboundAccountInfoResponse response = mapper.readValue(is, BinanceOutboundAccountInfoResponse.class);
            Assert.assertNotNull(response);
        }
    }
}
