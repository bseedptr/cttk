package cttk.impl.binance.stream.response;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.OrderSide;
import cttk.dto.Trade;
import cttk.dto.Trades;
import cttk.impl.binance.BinanceUtils;
import cttk.impl.util.DateTimeUtils;
import cttk.util.StringUtils;
import lombok.Data;

@Data
public class BinanceWebsocketTradeResponse {
    @JsonProperty("e")
    String eventType;
    @JsonProperty("E")
    Long eventTime;
    @JsonProperty("s")
    String symbol;
    @JsonProperty("t")
    String tradeId;
    @JsonProperty("p")
    BigDecimal price;
    @JsonProperty("q")
    BigDecimal quantity;
    @JsonProperty("b")
    String buyerOrderId;
    @JsonProperty("a")
    String sellerOrderId;
    @JsonProperty("T")
    Long tradeTime;
    @JsonProperty("m")
    Boolean isBuyerMaker;

    public Trades toTrades() {
        try {
            Trade trade = new Trade()
                .setSno(StringUtils.parseBigInteger(tradeId))
                .setTid(tradeId)
                .setPrice(price)
                .setQuantity(quantity)
                .setSide(isBuyerMaker ? OrderSide.BUY : OrderSide.SELL)
                .setDateTime(DateTimeUtils.zdtFromEpochMilli(tradeTime));

            return new Trades()
                .setCurrencyPair(BinanceUtils.parseMarketSymbol(symbol))
                .setTrades(new ArrayList<>(Arrays.asList(trade)));
        } catch (Exception e) {
            return new Trades();
        }
    }
}
