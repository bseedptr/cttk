package cttk;

import cttk.dto.MarketInfos;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Tickers;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.request.GetOrderBookRequest;
import cttk.request.GetRecentTradesRequest;
import cttk.request.GetTickerRequest;
import cttk.request.GetTradeHistoryRequest;

/**
 * PrivateCXClient processes public API requests to cryptocurrency exchanges.
 * @author	dj.lee
 * @since	2018-01-16
 */
public interface PublicCXClient
    extends CurrencySetter, CurrencyPairSetter, DateTimeSetter
{
    String getCxId();

    MarketInfos getMarketInfos()
        throws CTTKException, CTTKException;

    Ticker getTicker(GetTickerRequest request)
        throws CTTKException, UnsupportedCurrencyPairException;

    default Ticker getTicker(String baseCurrency, String quoteCurrency)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return getTicker(new GetTickerRequest()
            .setCurrencyPair(baseCurrency, quoteCurrency));
    }

    Tickers getAllTickers()
        throws CTTKException;

    OrderBook getOrderBook(GetOrderBookRequest request)
        throws CTTKException, UnsupportedCurrencyPairException;

    default OrderBook getOrderBook(String baseCurrency, String quoteCurrency, int count)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return getOrderBook(new GetOrderBookRequest()
            .setCurrencyPair(baseCurrency, quoteCurrency)
            .setCount(count));
    }

    Trades getRecentTrades(GetRecentTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException;

    default Trades getRecentTrades(String baseCurrency, String quoteCurrency, int count)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return getRecentTrades(new GetRecentTradesRequest()
            .setCurrencyPair(baseCurrency, quoteCurrency)
            .setCount(count));
    }

    Trades getTradeHistory(GetTradeHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException;
}
