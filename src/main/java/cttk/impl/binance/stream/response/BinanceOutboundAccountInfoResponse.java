package cttk.impl.binance.stream.response;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.dto.Balance;
import cttk.dto.Balances;
import cttk.impl.binance.BinanceUtils;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;

@Data
public class BinanceOutboundAccountInfoResponse {
    @JsonProperty("e")
    String eventType;

    @JsonProperty("E")
    Long unixTimeInMilli;

    @JsonProperty("m")
    Integer makerCommissionBips;

    @JsonProperty("t")
    Integer takerCommissionBips;

    @JsonProperty("b")
    Integer buyerCommissionBips;

    @JsonProperty("s")
    Integer sellerCommissionBips;

    @JsonProperty("T")
    Boolean canTrade;

    @JsonProperty("W")
    Boolean canWithdraw;

    @JsonProperty("D")
    Boolean canDeposit;

    @JsonProperty("u")
    Long lastUpdateUnixTime;

    @JsonProperty("B")
    List<BinanceAssetBalance> balances;

    public Balances toBalances() {
        return new Balances()
            .setBalances(toBalanceList(balances))
            .setUpdatedDateTime(DateTimeUtils.zdtFromEpochMilli(lastUpdateUnixTime));
    }

    private static List<Balance> toBalanceList(List<BinanceAssetBalance> balances) {
        return balances == null ? null
            : balances.stream().map(i -> i.toBalance()).collect(Collectors.toList());
    }
}

@Data
class BinanceAssetBalance {
    @JsonProperty("a")
    String currency;

    @JsonProperty("f")
    BigDecimal available;

    @JsonProperty("l")
    BigDecimal inUse;

    public Balance toBalance() {
        return new Balance()
            .setCurrency(BinanceUtils.toStdCurrencySymbol(currency))
            .setAvailable(available)
            .setInUse(inUse)
            .setTotal(MathUtils.add(available, inUse));
    }
}
