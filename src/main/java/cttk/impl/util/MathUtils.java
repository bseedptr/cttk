package cttk.impl.util;

import static cttk.impl.util.Objects.nvl;
import static cttk.util.StringUtils.parseBigDecimal;
import static java.math.BigDecimal.ZERO;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class MathUtils {
    public static final BigDecimal HUNDRED = new BigDecimal("100");

    public static BigDecimal abs(BigDecimal v) {
        return v == null ? null : v.abs();
    }

    public static BigDecimal add(BigDecimal a, BigDecimal b) {
        return a == null && b == null ? null
            : nvl(a, BigDecimal.ZERO).add(nvl(b, BigDecimal.ZERO));
    }

    public static BigDecimal add(String a, String b) {
        return add(parseBigDecimal(a), parseBigDecimal(b));
    }

    public static String addOneToLongValueString(String num) {
        return num == null ? null : String.valueOf(Long.parseLong(num.trim()) + 1);
    }

    public static String subOneToLongValueString(String num) {
        return num == null ? null : String.valueOf(Long.parseLong(num.trim()) - 1);
    }

    public static BigDecimal subtract(BigDecimal a, BigDecimal b) {
        return a == null && b == null ? null
            : nvl(a, BigDecimal.ZERO).subtract(nvl(b, BigDecimal.ZERO));
    }

    public static BigDecimal subtract(String a, String b) {
        return subtract(parseBigDecimal(a), parseBigDecimal(b));
    }

    public static BigDecimal multiply(BigDecimal a, BigDecimal b) {
        return a == null || b == null ? null : a.multiply(b);
    }

    public static BigDecimal multiply(String a, String b) {
        return multiply(parseBigDecimal(a), parseBigDecimal(b));
    }

    public static BigDecimal multiply100(BigDecimal a) {
        return a == null ? null : a.multiply(HUNDRED);
    }

    public static BigDecimal divide(BigDecimal a, BigDecimal b) {
        return divide(a, b, RoundingMode.HALF_UP);
    }

    public static BigDecimal divide(BigDecimal a, BigDecimal b, RoundingMode roundingMode) {
        return a == null || b == null ? null : a.divide(b, max(max(a.scale(), b.scale()), 2), roundingMode);
    }

    public static BigDecimal divide(String a, String b) {
        return divide(parseBigDecimal(a), parseBigDecimal(b));
    }

    public static BigDecimal round(final BigDecimal value, final BigDecimal increment) {
        if (value == null) return null;
        if (increment == null) return value;
        return value.divide(increment, 0, RoundingMode.HALF_EVEN).multiply(increment);
    }

    public static BigDecimal round(final BigDecimal value, final int precision) {
        if (value == null) return null;
        if (precision <= 0) return value;
        return value.round(new MathContext(precision));
    }

    public static BigDecimal findIncrement(final BigDecimal[] ranges, final BigDecimal[] increments, final BigDecimal value) {
        if (ranges == null || increments == null || value == null) return null;

        for (int i = 0, size = Math.min(ranges.length, increments.length); i < size; i++) {
            if (value.compareTo(ranges[i]) >= 0) {
                return increments[i];
            }
        }

        return ranges.length < increments.length ? increments[ranges.length] : null;
    }

    public static boolean equals(BigDecimal a, BigDecimal b) {
        return a == null && b == null
            || a != null && b != null && a.compareTo(b) == 0;
    }

    public static boolean isNullOrZero(BigDecimal value) {
        return value == null || value.compareTo(BigDecimal.ZERO) == 0;
    }

    public static Integer subtract(Integer a, Integer b) {
        return a == null && b == null ? null : nvl(a, 0) - nvl(b, 0);
    }

    public static Integer min(Integer a, Integer b) {
        if (a == null && b == null) {
            return null;
        } else if (a == null) {
            return b;
        } else if (b == null) {
            return a;
        } else {
            return Math.min(a, b);
        }
    }

    public static Integer max(Integer a, Integer b) {
        if (a == null && b == null) {
            return null;
        } else if (a == null) {
            return b;
        } else if (b == null) {
            return a;
        } else {
            return Math.max(a, b);
        }
    }

    public static boolean gt(Integer a, Integer b) {
        return a != null && b != null ? a > b : false;
    }

    public static boolean gte(Integer a, Integer b) {
        return a != null && b != null ? a >= b : false;
    }

    public static boolean isNotZero(BigDecimal value) {
        return value == null ? false : value.compareTo(ZERO) != 0;
    }

    public static boolean isNegative(BigDecimal value) {
        return value == null ? false : value.compareTo(ZERO) < 0;
    }
}
