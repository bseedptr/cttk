package cttk.impl.binance;

import static cttk.util.StringUtils.concat;
import static cttk.util.StringUtils.toUpper;

import java.io.IOException;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import cttk.CXIds;
import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.exception.APILimitExceedException;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXAuthorityException;
import cttk.exception.InvalidAmountRangeException;
import cttk.exception.InvalidPriceRangeException;
import cttk.exception.InvalidTotalRangeException;
import cttk.exception.OrderNotFoundException;

/**
 * Provides Binance specific utility functions
 * @author	Dongjoo Lee
 * @since	2018-03-12
 */
public class BinanceUtils {
    public static ObjectMapper objectMapper = new ObjectMapper();
    final static String cxId = CXIds.BINANCE;

    public static String getMarketSymbol(CurrencyPair currencyPair) {
        return currencyPair == null ? null
            : concat(toBinanceCurrencySymbol(currencyPair.getBaseCurrency()), toBinanceCurrencySymbol(currencyPair.getQuoteCurrency()));
    }

    /**
     * There are four quote currencies: BTC, ETH, BNB and USDT
     * @param symbol the currency pair symbol
     * @return CurrencyPair
     */
    public static CurrencyPair parseMarketSymbol(final String symbol) {
        if (symbol == null || symbol.trim().isEmpty()) return null;

        final String upper = toUpper(symbol.trim());
        final int idx = upper.endsWith("USDT") ? upper.length() - 4 : upper.length() - 3;
        return CurrencyPair.of(toStdCurrencySymbol(symbol.substring(0, idx)), toStdCurrencySymbol(symbol.substring(idx)));
    }

    /**
     * Convert the given currency symbol to the standardized symbol
     * @param currency the currency symbol
     * @return the standard currency symbol
     */
    public static String toStdCurrencySymbol(String currency) {
        if (currency == null) return null;
        final String upper = toUpper(currency);
        switch (upper) {
            case "BCHABC":
                return "BCH";
            default:
                return upper;
        }
    }

    public static String toBinanceCurrencySymbol(String currency) {
        if (currency == null) return null;
        final String upper = toUpper(currency);
        switch (upper) {
            case "BCH":
                return "BCHABC";
            default:
                return upper;
        }
    }

    public static String toString(OrderSide orderSide) {
        switch (orderSide) {
            case BUY:
                return "BUY";
            case SELL:
                return "SELL";
            default:
                return null;
        }
    }

    public static OrderStatus getStatusType(final String status) {
        switch (status) {
            case "NEW":
                return OrderStatus.UNFILLED;
            case "PARTIALLY_FILLED":
                return OrderStatus.PARTIALLY_FILLED;
            case "FILLED":
                return OrderStatus.FILLED;
            case "CANCELED":
                return OrderStatus.CANCELED;
            case "REJECTED":
                return OrderStatus.REJECTED;
            case "EXPIRED":
                return OrderStatus.EXPIRED;
            default:
                return null;
        }
    }

    public static void checkErrorByResponseMessage(Object requestObj, String body, Integer statusCode)
        throws CTTKException
    {
        Map<?, ?> map;
        String exchangeErrorCode;

        try {
            map = objectMapper.readValue(body, Map.class);
            exchangeErrorCode = map.get("code").toString();
        } catch (IOException | NumberFormatException | NullPointerException e) {
            return;
        }

        final String exchangeMessage = map.containsKey("msg") ? map.get("msg").toString() : "";

        switch (exchangeErrorCode) {
            case "-2015":
                throw new CXAuthorityException(exchangeMessage)
                    .setCxId(cxId)
                    .setExchangeMessage(body)
                    .setResponseStatusCode(statusCode)
                    .setExchangeErrorCode(exchangeErrorCode);
            case "-1121":
                throw CXAPIRequestException.createByRequestObj(requestObj, exchangeMessage)
                    .setCxId(cxId)
                    .setExchangeMessage(body)
                    .setResponseStatusCode(statusCode)
                    .setExchangeErrorCode(exchangeErrorCode);
            case "-1003":
                throw new APILimitExceedException(exchangeMessage)
                    .setCxId(cxId)
                    .setExchangeMessage(body)
                    .setResponseStatusCode(statusCode)
                    .setExchangeErrorCode(exchangeErrorCode);
            case "-2013":
                throw new OrderNotFoundException(exchangeMessage)
                    .setCxId(cxId)
                    .setExchangeMessage(body)
                    .setResponseStatusCode(statusCode)
                    .setExchangeErrorCode(exchangeErrorCode);
            case "-2011":
                if ("UNKNOWN_ORDER".equals(exchangeMessage)) {
                    throw new OrderNotFoundException(exchangeMessage)
                        .setCxId(cxId)
                        .setExchangeMessage(body)
                        .setResponseStatusCode(statusCode)
                        .setExchangeErrorCode(exchangeErrorCode);
                }
                break;
        }
        throw new CXAPIRequestException(exchangeMessage)
            .setCxId(cxId)
            .setExchangeMessage(body)
            .setResponseStatusCode(statusCode)
            .setExchangeErrorCode(exchangeErrorCode);
    }
}
