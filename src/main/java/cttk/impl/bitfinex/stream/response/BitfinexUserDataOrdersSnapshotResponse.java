package cttk.impl.bitfinex.stream.response;

import static cttk.impl.bitfinex.BitfinexUtils.getPricingType;
import static cttk.impl.bitfinex.BitfinexUtils.getStatusType;
import static cttk.impl.bitfinex.BitfinexUtils.parseMarketSymbolWithT;
import static cttk.impl.util.DateTimeUtils.zdtFromEpochMilli;
import static cttk.util.StringUtils.parseBigDecimal;
import static cttk.util.StringUtils.parseLong;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import cttk.CXIds;
import cttk.OrderSide;
import cttk.dto.UserOrder;
import cttk.impl.util.MathUtils;

public class BitfinexUserDataOrdersSnapshotResponse
    extends ArrayList<ArrayList<String>>
{
    public List<UserOrder> toUserOrderList() {
        final List<UserOrder> userOrderlist = this.stream()
            .filter(i -> i.get(0) != null)
            .map(i -> {
                UserOrder uo = new UserOrder();

                final String id = i.get(0);
                final String clientOId = i.get(2);
                final String symbol = i.get(3);
                final String orderStatus = i.get(13);
                final String orderPricingType = i.get(8);
                final Long mtsCreate = parseLong(i.get(4));
                final BigDecimal amount = parseBigDecimal(i.get(6));
                final BigDecimal price = parseBigDecimal(i.get(16));

                if (clientOId != null) {
                    uo.setClientOrderId(clientOId);
                }
                uo.setCxId(CXIds.BITFINEX);
                uo.setCurrencyPair(parseMarketSymbolWithT(symbol));
                uo.setOid(id);
                uo.setStatus(orderStatus);
                uo.setStatusType(getStatusType(orderStatus));
                uo.setPricingType(getPricingType(orderPricingType));
                uo.setDateTime(zdtFromEpochMilli(mtsCreate));
                uo.setSide(amount.compareTo(BigDecimal.ZERO) > 0 ? OrderSide.BUY : OrderSide.SELL);
                uo.setPrice(price);
                uo.setQuantity(amount.abs());
                uo.setTotal(MathUtils.multiply(price, amount.abs()));

                return uo;
            }).collect(Collectors.toList());

        return userOrderlist;
    }
}
