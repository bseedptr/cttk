package cttk.dto;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import cttk.dto.OrderBook.SimpleOrder;

public class OrderBookTest {
    @Test
    public void testGroupOrderBook()
        throws IOException
    {
        for (int scale = 1; scale < 8; scale++) {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.registerModule(new JavaTimeModule());
            try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("data/orderbook.json")) {
                final OrderBook orderBook = objectMapper.readValue(is, OrderBook.class);
                final OrderBook groupedOrderBook = orderBook.group(scale);

                for (SimpleOrder o : groupedOrderBook.getAsks()) {
                    Assert.assertEquals(scale, o.getPrice().scale());
                }

                for (SimpleOrder o : groupedOrderBook.getBids()) {
                    Assert.assertEquals(scale, o.getPrice().scale());
                }
            }
        }
    }
}
