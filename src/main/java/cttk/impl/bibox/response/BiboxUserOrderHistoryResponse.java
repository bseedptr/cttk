package cttk.impl.bibox.response;

import java.util.List;
import java.util.stream.Collectors;

import cttk.dto.UserTrades;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BiboxUserOrderHistoryResponse
    extends BiboxResultWrapperResponse<BiboxUserOrderHistoryList>
{
    public UserTrades toUserTrades() {
        return unwrap().toUserTrades();
    }
}

@Data
class BiboxUserOrderHistoryList {
    Integer count;
    Integer page;
    List<BiboxUserOrderHistory> items;

    UserTrades toUserTrades() {
        // TODO should be separated by full result return and partial result return
        return new UserTrades().setTrades(items.stream().map(BiboxUserOrderHistory::toUserTrade).collect(Collectors.toList()));
    }
}
