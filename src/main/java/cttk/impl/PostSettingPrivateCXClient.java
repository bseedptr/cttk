package cttk.impl;

import cttk.AbstractCXIdHolder;
import cttk.PrivateCXClient;
import cttk.TradingFeeProvider;
import cttk.dto.Balances;
import cttk.dto.BankAccount;
import cttk.dto.Transfers;
import cttk.dto.UserAccount;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrades;
import cttk.dto.Wallet;
import cttk.dto.Wallets;
import cttk.dto.WithdrawResult;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.request.CancelOrderRequest;
import cttk.request.CreateMarketPriceOrderRequest;
import cttk.request.CreateOrderRequest;
import cttk.request.GetBankAccountRequest;
import cttk.request.GetOpenUserOrdersRequest;
import cttk.request.GetTransfersDeltaRequest;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserAccountRequest;
import cttk.request.GetUserOrderHistoryDeltaRequest;
import cttk.request.GetUserOrderHistoryRequest;
import cttk.request.GetUserOrderRequest;
import cttk.request.GetUserTradesDeltaRequest;
import cttk.request.GetUserTradesRequest;
import cttk.request.GetWalletRequest;
import cttk.request.WithdrawToBankRequest;
import cttk.request.WithdrawToWalletRequest;

public class PostSettingPrivateCXClient
    implements PrivateCXClient
{
    private final PrivateCXClient client;

    public PostSettingPrivateCXClient(PrivateCXClient client) {
        super();
        this.client = client;
    }

    public PrivateCXClient getClient() {
        return client;
    }

    @Override
    public String getCxId() {
        return client.getCxId();
    }

    @Override
    public UserAccount getUserAccount(GetUserAccountRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return setCxId(client.getUserAccount(request));
    }

    @Override
    public TradingFeeProvider getTradingFeeProvider()
        throws CTTKException
    {
        final TradingFeeProvider o = client.getTradingFeeProvider();
        if (o != null && o instanceof AbstractCXIdHolder) {
            ((AbstractCXIdHolder<?>) o).setCxId(getCxId());
        }
        return o;
    }

    @Override
    public BankAccount getBankAccount(GetBankAccountRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return setCxId(client.getBankAccount(request));
    }

    @Override
    public WithdrawResult withdraw(WithdrawToBankRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return setCxId(client.withdraw(request));
    }

    @Override
    public Wallet getWallet(GetWalletRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return setCxIdAndCurrency(client.getWallet(request), request);
    }

    @Override
    public Wallets getAllWallets()
        throws CTTKException
    {
        return setCxId(client.getAllWallets());
    }

    @Override
    public WithdrawResult withdraw(WithdrawToWalletRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return setCxIdAndCurrency(client.withdraw(request), request);
    }

    @Override
    public Balances getAllBalances()
        throws CTTKException
    {
        return setCxId(client.getAllBalances());
    }

    @Override
    public UserOrder getUserOrder(GetUserOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return setCxIdAndCurrencyPair(client.getUserOrder(request), request);
    }

    @Override
    public UserOrders getOpenUserOrders(GetOpenUserOrdersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return setCxId(client.getOpenUserOrders(request));
    }

    @Override
    public UserOrders getAllOpenUserOrders(boolean withTrades)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return setCxId(client.getAllOpenUserOrders(withTrades));
    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return setCxId(client.getUserOrderHistory(request));
    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return setCxId(client.getUserOrderHistory(request));
    }

    @Override
    public UserOrder createOrder(CreateOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return setCxIdAndCurrencyPair(client.createOrder(request), request);
    }

    @Override
    public void cancelOrder(CancelOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        client.cancelOrder(request);
    }

    @Override
    public UserOrder createMarketPriceOrder(CreateMarketPriceOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return setCxIdAndCurrencyPair(client.createMarketPriceOrder(request), request);
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return setCxId(client.getUserTrades(request));
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return setCxId(client.getUserTrades(request));
    }

    @Override
    public Transfers getTransfers(GetTransfersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return setCxId(client.getTransfers(request));
    }

    @Override
    public Transfers getTransfers(GetTransfersDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return setCxId(client.getTransfers(request));
    }
}
