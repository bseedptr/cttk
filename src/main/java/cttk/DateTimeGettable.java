package cttk;

import java.time.ZonedDateTime;

public interface DateTimeGettable {
    ZonedDateTime getDateTime();
}
