package cttk.test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

import org.junit.Assert;

import cttk.CXIds;
import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.OrderVerifier;
import cttk.OrderVerifierFactory;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.dto.MarketInfo;
import cttk.dto.OrderBook;
import cttk.dto.UserOrder;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.util.MathUtils;
import cttk.impl.util.RetryUtils;
import cttk.request.CancelOrderRequest;
import cttk.request.CreateOrderRequest;
import cttk.request.GetOrderBookRequest;
import cttk.request.GetUserOrderRequest;
import cttk.util.StringUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TradeTestUtils {

    public static OrderbookPrices getOrderbookPrices(
        PublicCXClient pubClient,
        CurrencyPair currencyPair,
        OrderVerifier orderVerifier,
        BigDecimal factor)
        throws CTTKException
    {
        final OrderBook orderBook = pubClient.getOrderBook(new GetOrderBookRequest()
            .setCurrencyPair(currencyPair));

        final Optional<BigDecimal> opMaxBid = orderBook.getHighestBid();
        final Optional<BigDecimal> opMinAsk = orderBook.getLowestAsk();

        BigDecimal buyPrice = orderVerifier.roundPrice(MathUtils.divide(opMaxBid.get(), factor)).stripTrailingZeros();
        BigDecimal sellPrice = orderVerifier.roundPrice(opMinAsk.get().multiply(factor)).stripTrailingZeros();

        log.info("Test bid with\n"
            + "    Current Max Bid: {}\n",
            opMaxBid.get());

        OrderbookPrices prices = new OrderbookPrices();
        prices.setBuyPrice(buyPrice);
        prices.setSellPrice(sellPrice);
        return prices;
    }

    public static UserOrder createUnreachableBuyOrder(
        PublicCXClient pubClient,
        PrivateCXClient priClient,
        CurrencyPair currencyPair,
        BigDecimal quantity)
        throws CTTKException
    {

        final OrderBook orderBook = pubClient.getOrderBook(new GetOrderBookRequest().setCurrencyPair(currencyPair));

        final Optional<BigDecimal> opMaxBid = orderBook.getHighestBid();

        final MarketInfo marketInfo = pubClient.getMarketInfos().getMarketInfo(currencyPair);
        final OrderVerifier orderVerifier = OrderVerifierFactory.create(priClient.getCxId(), marketInfo);

        final BigDecimal price = orderVerifier.roundPrice(MathUtils.divide(opMaxBid.get(), new BigDecimal("2"))).stripTrailingZeros();
        log.info("{}", marketInfo);
        log.info("Test bid with\n"
            + "    Current Max Bid: {}\n"
            + "    Test Bid       : {}\n"
            + "    Quantity       : {}\n"
            + "    Total          : {}\n"
            + "    Min Total      : {}",
            opMaxBid.get(), price, quantity, price.multiply(quantity), marketInfo.getMinTotal());

        return testCreateOrder(priClient, currencyPair, OrderSide.BUY, quantity, price);
    }

    public static UserOrder createOrder(
        PrivateCXClient privateClient,
        CurrencyPair pair,
        OrderSide side,
        BigDecimal quantity,
        BigDecimal price)
        throws CTTKException
    {
        return privateClient.createOrder(CreateOrderRequest.of(pair, side, quantity, price));
    }

    public static void cancelOrder(PrivateCXClient client, UserOrder userOrder)
        throws CTTKException
    {
        client.cancelOrder(CancelOrderRequest.from(userOrder));
    }

    public static UserOrder testCreateOrder(
        PrivateCXClient client,
        CurrencyPair currencyPair,
        OrderSide side,
        BigDecimal quantity,
        BigDecimal price)
        throws CTTKException
    {
        final UserOrder userOrder = createOrder(client, currencyPair, side, quantity, price);

        log.info("Order result\n"
            + "    ORDER: {}", userOrder);

        Assert.assertNotNull(userOrder);
        Assert.assertNotNull(userOrder.getOid());

        UserOrder readUserOrder = getUserOrderUsingRetry(client, userOrder, Objects::nonNull);

        Assert.assertNotNull(readUserOrder);
        Assert.assertNotNull(readUserOrder.getOid());
        Assert.assertEquals(OrderStatus.UNFILLED, readUserOrder.getStatusType());

        return readUserOrder;
    }

    public static void testCancelOrder(PrivateCXClient client, UserOrder userOrder)
        throws CTTKException
    {
        cancelOrder(client, userOrder);

        // see CTTK-415
        final Set<String> canceledOrderIsNotSupportedCxIds = new HashSet<>(
            Arrays.asList(CXIds.BITHUMB, CXIds.COINONE, CXIds.HITBTC, CXIds.KORBIT, CXIds.POLONIEX));

        if (canceledOrderIsNotSupportedCxIds.contains(client.getCxId())) {
            getUserOrderUsingRetry(client, userOrder, Objects::isNull);
        } else {
            getUserOrderUsingRetry(client, userOrder, order -> OrderStatus.CANCELED == order.getStatusType());
        }
    }

    private static UserOrder getUserOrderUsingRetry(
        PrivateCXClient client,
        UserOrder userOrder,
        Predicate<UserOrder> predicate)
    {
        try {
            return RetryUtils.getDataWithRetry(500, 6, () -> {
                UserOrder order = client.getUserOrder(GetUserOrderRequest.from(userOrder));
                if (predicate.test(order)) {
                    return order;
                }
                throw new RuntimeException();
            });
        } catch (UnsupportedMethodException e) {
            log.warn("UnsupportedMethodException", e);
        } catch (CTTKException e) {
            log.error("fail to get userOrder", e);
            Assert.fail();
        }
        return null;
    }

    public static void testCreateCancelOrder(
        PrivateCXClient client,
        CurrencyPair currencyPair,
        OrderSide side,
        BigDecimal quantity,
        BigDecimal price)
        throws CTTKException
    {
        UserOrder createdOrder = testCreateOrder(client, currencyPair, side, quantity, price);
        testCancelOrder(client, createdOrder);

    }
}
