package cttk.exception;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class UnsupportedCXException
    extends CTTKException
{
    private static final long serialVersionUID = 1L;

    public UnsupportedCXException() {
        super();
    }

    public UnsupportedCXException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsupportedCXException(String message) {
        super(message);
    }

    public UnsupportedCXException(Throwable cause) {
        super(cause);
    }

    @Override
    public UnsupportedCXException setCxId(String cxId) {
        super.setCxId(cxId);
        return this;
    }
}
