package cttk.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cttk.AbstractCXIdHolder;
import cttk.DateTimeGettableListHolder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Transfers
    extends AbstractCXIdHolder<Transfers>
    implements DateTimeGettableListHolder<Transfers, Transfer>
{
    List<Transfer> transfers;

    @JsonIgnore
    @Override
    public List<Transfer> getList() {
        return transfers;
    }

    @Override
    public Transfers setList(List<Transfer> list) {
        this.transfers = list;
        return this;
    }

    public Transfers addTransfers(List<Transfer> list) {
        if (list == null) return this;
        if (this.transfers == null) {
            this.transfers = new ArrayList<>();
        }
        this.transfers.addAll(list);
        return this;
    }

    @JsonIgnore
    public Transfer getLatestById() {
        return isEmpty()
            ? null
            : getList().stream()
                .max((a, b) -> Long.valueOf(a.getTid()).compareTo(Long.valueOf(b.getTid())))
                .get();
    }

    @JsonIgnore
    public Transfer getOldestById() {
        return isEmpty()
            ? null
            : getList().stream()
                .max((a, b) -> Long.valueOf(b.getTid()).compareTo(Long.valueOf(a.getTid())))
                .get();
    }
}
