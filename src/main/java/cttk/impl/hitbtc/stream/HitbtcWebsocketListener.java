package cttk.impl.hitbtc.stream;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.zip.GZIPInputStream;

import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.CurrencyPair;
import cttk.dto.OrderBook;
import cttk.impl.hitbtc.HitbtcUtils;
import cttk.impl.stream.AbstractWebsocketListener;
import cttk.impl.stream.MessageConverter;
import cttk.request.AbstractCXStreamRequest;
import okhttp3.Response;
import okhttp3.WebSocket;
import okio.ByteString;

public class HitbtcWebsocketListener<T, R extends AbstractCXStreamRequest<R>>
    extends AbstractWebsocketListener<T, R>
{
    private boolean isFirst = true;

    public HitbtcWebsocketListener(
        final CXStream<T> stream,
        final String channel,
        final R request,
        final MessageConverter<T> converter,
        final CXStreamListener<T> listener)
    {
        super(stream, channel, request, converter, listener, CXIds.HITBTC);
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        webSocket.send(getSubscriptionPayload());
        logger.info("[{}] Open : {}", id, channel);
        this.listener.onOpen(stream);
    }

    public String getSubscriptionPayload() {
        String hitbtcBaseCurrency = HitbtcUtils.toHitbtcCurrencySymbol(request.getBaseCurrency());
        String hitbtcQuoteCurrency = HitbtcUtils.toHitbtcCurrencySymbol(request.getQuoteCurrency());

        return "{\"method\":" + "\"" + channel + "\"," +
            "\"id\": \"" + id + "\"," +
            "\"params\": " + "{\"symbol\":\"" + CurrencyPair.getPairString(hitbtcBaseCurrency, hitbtcQuoteCurrency).replace("-", "") + "\"}" + "}";
    }

    @Override
    protected void handleMessage(WebSocket webSocket, String text)
        throws Exception
    {
        HitbtcUtils.checkErrorByResponseMessage(request, text, null);

        if (text.contains("\"result\":true")) {
            logger.info("[{}] Subscribed: {}", id, channel);
        } else {
            T message = converter.convert(text);
            if (message != null) {
                if (message instanceof OrderBook) {
                    if (!isFirst) ((OrderBook) message).setFull(false);
                    isFirst = false;
                }
                listener.onMessage(message);
            }
        }
    }

    @Override
    protected String toString(ByteString bytes)
        throws IOException
    {
        StringWriter sw = new StringWriter();
        try (
            final PrintWriter pw = new PrintWriter(sw);
            final ByteArrayInputStream in = new ByteArrayInputStream(bytes.toByteArray());
            final BufferedReader br = new BufferedReader(new InputStreamReader(new GZIPInputStream(in)));)
        {
            String line;
            for (int i = 0; (line = br.readLine()) != null; i++) {
                if (i > 0) pw.println();
                pw.print(line);
            }
        } catch (IOException e) {
            throw e;
        }
        return sw.toString();
    }
}
