package cttk.impl.huobi;

import cttk.CXStreamOpener;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.auth.Credential;
import cttk.impl.AbstractEMPUDSOpenerCXClientFactory;
import cttk.impl.huobi.stream.HuobiStreamOpener;

public class HuobiClientFactory
    extends AbstractEMPUDSOpenerCXClientFactory
{
    @Override
    public PublicCXClient create() {
        return new HuobiPublicClient();
    }

    @Override
    public PrivateCXClient create(Credential credential) {
        return new HuobiPrivateClient(credential);
    }

    @Override
    public CXStreamOpener createCXStreamOpener() {
        return new HuobiStreamOpener();
    }
}
