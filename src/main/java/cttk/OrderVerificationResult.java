package cttk;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class OrderVerificationResult
    extends AbstractCurrencyPair<OrderVerificationResult>
{
    protected boolean valid;
    protected List<InvalidOrderType> reasons;

    protected BigDecimal roundedPrice;
    protected BigDecimal roundedQuantity;

    public OrderVerificationResult addReason(InvalidOrderType reason) {
        if (reason != null) {
            if (reasons == null) reasons = new ArrayList<>();
            reasons.add(reason);
        }
        return this;
    }
}
