package cttk.impl.upbit;

import org.junit.Assert;
import org.junit.Test;

import cttk.CXIds;
import cttk.CXStream;
import cttk.dto.Trades;
import cttk.impl.AbstractCXStreamTest;
import cttk.request.OpenTradeStreamRequest;

public class UpbitStreamTest
    extends AbstractCXStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.UPBIT;
    }

    @Override
    protected long getThreadSleepMillis() {
        return 1000l;
    }

    @Test
    @Override
    public void testTradeStream()
        throws InterruptedException, Exception
    {
        threadSleep();

        final OpenTradeStreamRequest request = new OpenTradeStreamRequest()
            .setIntervalInMilli(getIntervalInMilli())
            .setCurrencyPair(getCurrencyPair());
        final CountingCXStreamListener<Trades> listener = new CountingCXStreamListener<>(request, 2);
        final CXStream<Trades> stream = streamOpener.openTradeStream(request, listener);
        listener.await();
        stream.close();

        Assert.assertTrue(listener.getCount() > 0);
    }
}
