package cttk.impl.stream;

import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.request.AbstractCXStreamRequest;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

public abstract class AbstractWebsocketStream<T, R extends AbstractCXStreamRequest<R>>
    implements CXStream<T>
{
    final protected String channel;
    final protected R request;
    final protected MessageConverter<T> converter;
    final protected CXStreamListener<T> listener;

    protected WebSocket websocket;

    public AbstractWebsocketStream(
        final String channel,
        final R request,
        final MessageConverter<T> converter,
        final CXStreamListener<T> listener)
    {
        super();

        this.channel = channel;
        this.request = request;
        this.converter = converter;
        this.listener = listener;

        open();
    }

    @Override
    public CXStream<T> open() {
        final OkHttpClient okHttpClient = createOkHttpClient(10);

        websocket = okHttpClient.newWebSocket(
            new Request.Builder().url(getUrl()).build(),
            createWebSocketListener());

        okHttpClient.dispatcher().executorService().shutdown();

        return this;
    }

    protected abstract String getUrl();

    protected abstract WebSocketListener createWebSocketListener();

    @Override
    public void close() {
        if (websocket != null) websocket.close(NORMAL_CLOSURE_STATUS, null);
    }

    @Override
    public CXStreamListener<T> getListener() {
        return listener;
    }
}
