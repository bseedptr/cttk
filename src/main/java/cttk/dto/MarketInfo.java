package cttk.dto;

import java.math.BigDecimal;
import java.util.List;

import cttk.AbstractCurrencyPair;
import cttk.impl.util.MathUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(chain = true)
public class MarketInfo
    extends AbstractCurrencyPair<MarketInfo>
{
    String status;

    Integer pricePrecision;
    BigDecimal priceIncrement;
    BigDecimal minPrice;
    BigDecimal maxPrice;
    Integer quantityPrecision;
    BigDecimal quantityIncrement;
    BigDecimal minQuantity;
    BigDecimal maxQuantity;
    BigDecimal minTotal;
    BigDecimal maxTotal;

    List<String> orderTypes;

    public BigDecimal roundPrice(final BigDecimal price, final int defaultScale) {
        return round(price, pricePrecision, priceIncrement, defaultScale);
    }

    public BigDecimal roundPrice(final BigDecimal price) {
        return round(price, pricePrecision, priceIncrement);
    }

    public BigDecimal roundQuantity(final BigDecimal quantity, final int defaultScale) {
        return round(quantity, quantityPrecision, quantityIncrement, defaultScale);
    }

    public BigDecimal roundQuantity(final BigDecimal quantity) {
        return round(quantity, quantityPrecision, quantityIncrement);
    }

    public static BigDecimal round(
        final BigDecimal value,
        final Integer precision,
        final BigDecimal increment)
    {
        if (value == null) {
            return null;
        } else if (precision != null) {
            return MathUtils.round(value, precision).stripTrailingZeros();
        } else if (increment != null) {
            return MathUtils.round(value, increment).stripTrailingZeros();
        } else {
            return value.stripTrailingZeros();
        }
    }

    public static BigDecimal round(
        final BigDecimal value,
        final Integer precision,
        final BigDecimal increment,
        int defaultScale)
    {
        if (value == null) {
            return null;
        } else if (precision != null) {
            return MathUtils.round(value, precision).stripTrailingZeros();
        } else if (increment != null) {
            return MathUtils.round(value, increment).stripTrailingZeros();
        } else if (defaultScale > -1) {
            return value.setScale(defaultScale).stripTrailingZeros();
        } else {
            return value.stripTrailingZeros();
        }
    }
}
