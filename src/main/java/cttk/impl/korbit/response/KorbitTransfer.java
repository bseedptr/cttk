package cttk.impl.korbit.response;

import static cttk.impl.korbit.KorbitUtils.toStdCurrencySymbol;
import static cttk.impl.util.DateTimeUtils.zdtFromEpochMilli;

import java.math.BigDecimal;

import cttk.TransferType;
import cttk.dto.Transfer;
import cttk.impl.util.ZoneIds;
import lombok.Data;

@Data
public class KorbitTransfer {
    String id;
    String type;
    String currency;
    BigDecimal amount;
    Long completed_at;
    Long updated_at;
    Long created_at;
    String status;
    BigDecimal fee;
    Details details;

    public Transfer toTransfer() {
        return new Transfer()
            .setCurrency(toStdCurrencySymbol(currency))
            .setType(TransferType.of(type))
            .setTid(id)
            .setTxId(details == null ? null : details.transaction_id)
            .setAmount(amount)
            .setFee(fee)
            .setStatus(status)
            .setAddress(details == null ? null : details.address)
            .setAddressTag(details == null ? null : details.destination_tag)
            .setCreatedDateTime(zdtFromEpochMilli(created_at, ZoneIds.SEOUL))
            .setUpdatedDateTime(zdtFromEpochMilli(updated_at, ZoneIds.SEOUL))
            .setCompletedDateTime(zdtFromEpochMilli(completed_at, ZoneIds.SEOUL));
    }
}

@Data
class Details {
    String transaction_id;
    String address;
    String destination_tag;
    String bank;
    String account_number;
    String owner;
}