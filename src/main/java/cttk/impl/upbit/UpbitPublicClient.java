package cttk.impl.upbit;

import static cttk.impl.upbit.UpbitUtils.PRICE_INCREMENTS;
import static cttk.impl.upbit.UpbitUtils.PRICE_RANGES;
import static cttk.impl.util.Objects.nvl;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import cttk.PublicCXClient;
import cttk.dto.MarketInfo;
import cttk.dto.MarketInfos;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Tickers;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.upbit.response.UpbitMarketResponse;
import cttk.impl.upbit.response.UpbitOrderBookResponse;
import cttk.impl.upbit.response.UpbitTickerResponse;
import cttk.impl.upbit.response.UpbitTradeResponse;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.MathUtils;
import cttk.impl.util.ParamsBuilder;
import cttk.request.GetOrderBookRequest;
import cttk.request.GetRecentTradesRequest;
import cttk.request.GetTickerRequest;
import cttk.request.GetTradeHistoryRequest;

public class UpbitPublicClient
    extends AbstractUpbitClient
    implements PublicCXClient
{
    public UpbitPublicClient() {
        super();
    }

    @Override
    public MarketInfos getMarketInfos()
        throws CTTKException
    {
        final Tickers tickers = getAllTickers();

        final List<MarketInfo> marketInfoList = tickers.getTickers().stream()
            .map(t -> {
                final MarketInfo marketInfo = new MarketInfo().setCurrencyPair(t);
                if ("KRW".equals(marketInfo.getQuoteCurrency())) {
                    marketInfo.setPriceIncrement(MathUtils.findIncrement(PRICE_RANGES, PRICE_INCREMENTS, t.getLastPrice()));
                }
                return marketInfo;
            })
            .collect(Collectors.toList());

        marketInfoList.forEach(UpbitPublicClient::fillMarketInfo);

        return new MarketInfos()
            .setCxId(getCxId())
            .setMarketInfos(marketInfoList);
    }

    private static void fillMarketInfo(final MarketInfo marketInfo) {
        switch (marketInfo.getQuoteCurrency().trim().toUpperCase()) {
            case "BTC":
                marketInfo.setPriceIncrement(new BigDecimal("0.00000001"));
                marketInfo.setMinTotal(new BigDecimal("0.0005"));
                break;
            case "ETH":
                marketInfo.setPriceIncrement(new BigDecimal("0.00000001"));
                marketInfo.setMinTotal(new BigDecimal("0.0005"));
                break;
            case "USDT":
                marketInfo.setPriceIncrement(new BigDecimal("0.001"));
                marketInfo.setMinTotal(new BigDecimal("0.0005"));
                break;
            case "KRW":
                marketInfo.setMinTotal(new BigDecimal("500"));
                break;
        }
    }

    @Override
    public Tickers getAllTickers()
        throws CTTKException
    {
        final HttpResponse response0 = requestGet("/v1/market/all");

        checkResponseError(null, response0);

        String pairsString = null;
        HttpResponse response = null;
        try {
            pairsString = convertList(response0, UpbitMarketResponse.class, e -> String.join(",", e.stream().map(i -> i.getMarket()).collect(Collectors.toList())));
            response = requestGet("/v1/ticker", ParamsBuilder.build("markets", pairsString));

            checkResponseError(null, response);

            return convertList(response, UpbitTickerResponse.class, e -> new Tickers().setTickers(e.stream().map(UpbitTickerResponse::toTicker).collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getTicker", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public Ticker getTicker(GetTickerRequest request)
        throws CTTKException
    {
        final HttpResponse response = requestGet("/v1/ticker",
            ParamsBuilder.build("markets", UpbitUtils.getMarketSymbol(request.getBaseCurrency(), request.getQuoteCurrency())));

        checkResponseError(request, response);

        try {
            return convertList(response, UpbitTickerResponse.class, i -> {
                if (i.get(0) != null) return i.get(0).toTicker();
                return new Ticker();
            });
        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getTicker", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public OrderBook getOrderBook(GetOrderBookRequest request)
        throws CTTKException
    {
        final HttpResponse response = requestGet("/v1/orderbook",
            ParamsBuilder.build("markets", UpbitUtils.getMarketSymbol(request.getBaseCurrency(), request.getQuoteCurrency())));

        checkResponseError(request, response);

        return convert(response, UpbitOrderBookResponse.class).toOrderBook();
    }

    @Override
    public Trades getRecentTrades(GetRecentTradesRequest request)
        throws CTTKException
    {
        final HttpResponse response = requestGet("/v1/trades/ticks",
            ParamsBuilder.create()
                .putIfNotNull("market", UpbitUtils.getMarketSymbol(request.getBaseCurrency(), request.getQuoteCurrency()))
                .putIfNotNull("count", nvl(request.getCount(), 1000))
                .build());

        checkResponseError(request, response);

        try {
            return convertList(response, UpbitTradeResponse.class,
                e -> new Trades().setTrades(e.stream().map(UpbitTradeResponse::toTrade).collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException("Failed to getRecentTrades", e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public Trades getTradeHistory(GetTradeHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }
}
