package cttk.impl.binance.response;

import static cttk.impl.bitfinex.BitfinexUtils.toStdCurrencySymbol;
import static cttk.util.StringUtils.toUpper;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import cttk.TransferStatus;
import cttk.TransferType;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BinanceWithdrawHistoryResponse
    extends BinanceResultResponse
{
    List<BinanceWithdraw> withdrawList;

    public Transfers toTransfers() {
        if (withdrawList == null) return new Transfers();
        return new Transfers()
            .setTransfers(withdrawList.stream().map(i -> i.toTransfer()).collect(Collectors.toList()));
    }
}

@Data
class BinanceWithdraw {
    Integer status;
    Long applyTime;
    BigDecimal amount;
    String id;
    String address;
    String addressTag;
    String asset;
    String txId;

    public Transfer toTransfer() {
        return new Transfer()
            .setCurrency(toStdCurrencySymbol(asset))
            .setType(TransferType.WITHDRAWAL)
            .setTid(id)
            .setTxId(txId)
            .setAmount(amount)
            .setStatus(status.toString())
            .setStatusType(getStatusType())
            .setAddress(address)
            .setAddressTag(addressTag)
            .setCompletedDateTime(DateTimeUtils.zdtFromEpochMilli(applyTime));
    }

    private TransferStatus getStatusType() {
        switch (status) {
            case 1:
                return TransferStatus.CANCELED;
            case 3:
                return TransferStatus.REJECTED;
            case 0:
            case 2:
            case 4:
                return TransferStatus.PENDING;
            case 5:
                return TransferStatus.FAILED;
            case 6:
                return TransferStatus.SUCCESS;
        }
        return null;
    }
}
