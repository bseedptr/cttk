package cttk.impl;

import static cttk.test.TestUtils.hasTestProfile;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Before;
import org.junit.Test;

import cttk.CXClientFactory;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.auth.CXCredentialsProvider;
import cttk.exception.APILimitExceedException;
import cttk.exception.CTTKException;
import cttk.impl.poloniex.ThrowableFunction;
import cttk.impl.util.ExNotRaisedException;

abstract public class AbstractAPILimitTest
    extends AbstractCXClientTest
{
    protected PublicCXClient publicClient;
    protected PrivateCXClient privateClient;
    protected int threadNumber = 12;
    protected int countDownNum = 100000;

    @Before
    public void setup()
        throws CTTKException, IOException
    {
        org.junit.Assume.assumeTrue(hasTestProfile("test-api-limit"));
        publicClient = CXClientFactory.createPublicCXClient(getCxId());
        privateClient = CXClientFactory.createPrivateCXClient(getCxId(), CXCredentialsProvider.getDefaultCredential(getCxId()).getWithdrawKeyOrDataKey());
    }

    public class LimitWorker<T, R>
        extends Thread
    {
        private CountDownLatch countDownLatch;
        private String cxId;
        private AtomicInteger exceptionCount;
        private Thread mainThread;
        private boolean running = true;
        ThrowableFunction<T, R> function;

        public LimitWorker(String cxId, CountDownLatch countDownLatch,
            AtomicInteger exceptionCount, Thread mainThread, ThrowableFunction<T, R> function)
        {
            this.cxId = cxId;
            this.countDownLatch = countDownLatch;
            this.exceptionCount = exceptionCount;
            this.mainThread = mainThread;
            this.function = function;
        }

        public void run() {
            try {
                while (running) {
                    function.apply(null);
                    countDownLatch.countDown();
                }
            } catch (APILimitExceedException e) {
                logger.info("APILimitExceedException exception raised as expected");
                assertEquals(this.cxId, e.getCxId());
                exceptionCount.getAndIncrement();
                if (!mainThread.isInterrupted())
                    mainThread.interrupt();
                return;
            } catch (CTTKException e) {
                throw new RuntimeException(e);
            }
        }

        public boolean isRunning() {
            return running;
        }

        public void setRunning(boolean running) {
            this.running = running;
        }
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public <T, R> void testCase(ThrowableFunction<T, R> function, boolean countCheck, int iterNum)
        throws CTTKException
    {
        CountDownLatch latch = new CountDownLatch(iterNum);
        AtomicInteger exceptionCount = new AtomicInteger(0);
        LimitWorker[] threads = new LimitWorker[threadNumber];
        for (int i = 0; i < threadNumber; i++) {
            threads[i] = new LimitWorker(this.getCxId(), latch,
                exceptionCount, Thread.currentThread(),
                function);
            threads[i].start();
        }
        try {
            latch.await(180, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            if (countCheck && exceptionCount.get() == 0) {
                throw new ExNotRaisedException(raiseExceptionMsg);
            }
            for (int i = 0; i < threadNumber; i++) {
                threads[i].setRunning(false);

                try {
                    threads[i].join();
                } catch (InterruptedException e1) {}
            }
            return;
        }
        for (int i = 0; i < threadNumber; i++) {
            threads[i].setRunning(false);
            try {
                threads[i].join();
            } catch (InterruptedException e) {}
        }
        if (countCheck && exceptionCount.get() == 0) {
            throw new ExNotRaisedException(raiseExceptionMsg);
        }
    }

    @Test
    public void testPublicAPILimit()
        throws CTTKException, InterruptedException
    {
        try {
            Thread.sleep(DEFAULT_API_LIMIT_RECOVERY_TIME_IN_SECOND * 1000);
        } catch (InterruptedException e) {
            // ignore exception here 
        }
        testCase((args) -> {
            return publicClient.getAllTickers();
        }, true, countDownNum);
    }

    @Test
    public void testPrivateAPILimit()
        throws CTTKException, InterruptedException
    {
        try {
            Thread.sleep(DEFAULT_API_LIMIT_RECOVERY_TIME_IN_SECOND * 1000);
        } catch (InterruptedException e) {
            // ignore exception here 
        }
        testCase((args) -> {
            return privateClient.getAllBalances();
        }, true, countDownNum);
    }
}
