package cttk.impl.coinbase;

import static cttk.util.StringUtils.toUpper;

import java.time.ZonedDateTime;

import cttk.CurrencyPair;

public class CoinbaseUtils {
    public static String getMarketSymbol(CurrencyPair currencyPair) {
        return currencyPair == null ? null
            : toUpper(currencyPair.getBaseCurrency()) + "-" + toUpper(currencyPair.getQuoteCurrency());
    }

    public static ZonedDateTime parseZDT(String time) {
        if (time == null) return null;
        if (time.length() > 24) {
            return ZonedDateTime.parse(time.substring(0, 24) + "Z");
        } else {
            return ZonedDateTime.parse(time);
        }
    }
}
