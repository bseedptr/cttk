package cttk.impl.okex.response;

import static cttk.impl.okex.OkexUtilsV3.toStdCurrencySymbolV3;
import static java.util.stream.Collectors.toList;

import java.math.BigDecimal;
import java.util.ArrayList;

import cttk.TransferType;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.impl.okex.OkexUtilsV3;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class OkexWithdrawalResponse
    extends ArrayList<OkexWithdrawal>
{
    public Transfers toTransfers() {
        return new Transfers()
            .setTransfers(stream()
                .map(OkexWithdrawal::toTransfer)
                .collect(toList()));
    }
}

@Data
class OkexWithdrawal {
    String withdrawal_id;
    String txid;
    String currency;
    BigDecimal amount;
    String fee; // ex) 0.00001eth, must defat with string
    String from;
    String to;
    String timestamp;
    int status;

    Transfer toTransfer() {
        return new Transfer()
            .setTid(withdrawal_id)
            .setTxId(txid)
            .setCurrency(toStdCurrencySymbolV3(currency))
            .setFromAddress(from)
            .setAddress(to)
            .setAmount(amount)
            .setFee(new BigDecimal(fee.replaceAll("[^\\d.]", "")))
            .setStatus(String.valueOf(status))
            .setStatusType(OkexUtilsV3.toStdWithdrawalTransferStatusTypeV3(status))
            .setType(TransferType.WITHDRAWAL)
            .setCreatedDateTime(DateTimeUtils.parseZdt(timestamp))
            .setUpdatedDateTime(DateTimeUtils.parseZdt(timestamp))
            .setCompletedDateTime(DateTimeUtils.parseZdt(timestamp));
    }
}
