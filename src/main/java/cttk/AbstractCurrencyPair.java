package cttk;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
public abstract class AbstractCurrencyPair<T extends AbstractCurrencyPair<T>>
    extends AbstractCXIdHolder<T>
    implements CurrencyPair
{
    protected String baseCurrency;
    protected String quoteCurrency;

    @SuppressWarnings("unchecked")
    public T setBaseCurrency(String baseCurrency) {
        this.baseCurrency = baseCurrency;
        return (T) this;
    }

    @SuppressWarnings("unchecked")
    public T setQuoteCurrency(String quoteCurrency) {
        this.quoteCurrency = quoteCurrency;
        return (T) this;
    }

    @SuppressWarnings("unchecked")
    public T setCurrencyPair(CurrencyPair currencyPair) {
        this.baseCurrency = currencyPair == null ? null : currencyPair.getBaseCurrency();
        this.quoteCurrency = currencyPair == null ? null : currencyPair.getQuoteCurrency();
        return (T) this;
    }

    @SuppressWarnings("unchecked")
    public T setCurrencyPair(String baseCurrency, String quoteCurrency) {
        this.baseCurrency = baseCurrency;
        this.quoteCurrency = quoteCurrency;
        return (T) this;
    }
}