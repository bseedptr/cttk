package cttk.util;

import org.junit.Assert;
import org.junit.Test;

import cttk.util.StringUtils;

public class StringUtilsTest {
    @Test
    public void testIsBlanck() {
        Assert.assertTrue(StringUtils.isBlank("   "));
        Assert.assertTrue(StringUtils.isBlank("   \t"));
        Assert.assertTrue(StringUtils.isBlank("   \n\r"));

        Assert.assertFalse(StringUtils.isBlank("a"));
        Assert.assertFalse(StringUtils.isBlank("a "));
        Assert.assertFalse(StringUtils.isBlank("a \t"));
    }

    @Test
    public void testIsEmpty() {
        Assert.assertTrue(StringUtils.isEmpty(""));

        Assert.assertFalse(StringUtils.isEmpty(" "));
        Assert.assertFalse(StringUtils.isEmpty("\t"));
        Assert.assertFalse(StringUtils.isEmpty("a"));
    }

    @Test
    public void testToUpper() {
        Assert.assertEquals("AAAA", StringUtils.toUpper("aaAa"));
        Assert.assertNull(StringUtils.toUpper(null));
    }

    @Test
    public void testToLower() {
        Assert.assertEquals("aaaa", StringUtils.toLower("aaAa"));
        Assert.assertNull(StringUtils.toLower(null));
    }
}
