package cttk.impl.korbit.response;

import lombok.Data;

@Data
public class KorbitTokenResponse {
    String token_type;
    String access_token;
    Long expires_in;
    String refresh_token;
}
