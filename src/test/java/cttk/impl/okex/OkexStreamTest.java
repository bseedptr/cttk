package cttk.impl.okex;

import cttk.CXIds;
import cttk.impl.AbstractCXStreamTest;

public class OkexStreamTest
    extends AbstractCXStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.OKEX;
    }
}
