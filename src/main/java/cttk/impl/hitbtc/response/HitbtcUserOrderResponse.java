package cttk.impl.hitbtc.response;

import static cttk.impl.hitbtc.HitbtcUtils.getStatusType;
import static cttk.impl.hitbtc.HitbtcUtils.parseMarketSymbol;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import cttk.OrderSide;
import cttk.PricingType;
import cttk.dto.UserOrder;
import cttk.impl.util.MathUtils;
import lombok.Data;

@Data
public class HitbtcUserOrderResponse {
    String id;
    String clientOrderId;
    String symbol;
    String side;
    String status;
    String type;
    String timeInForce;
    BigDecimal price;
    BigDecimal quantity;
    BigDecimal cumQuantity;
    String createdAt;
    String updatedAt;

    public UserOrder toUserOrder() {
        if (id == null) {
            return null;
        }

        return new UserOrder()
            .setCurrencyPair(parseMarketSymbol(symbol))
            .setOid(id)
            .setClientOrderId(clientOrderId)
            .setDateTime(ZonedDateTime.parse(createdAt))
            .setSide(OrderSide.of(side))
            .setPricingType(PricingType.of(type))
            .setStatus(status)
            .setStatusType(getStatusType(status))
            .setPrice(price)
            .setQuantity(quantity)
            .setFilledQuantity(cumQuantity)
            .setRemainingQuantity(MathUtils.subtract(quantity, cumQuantity))
            .setTotal(MathUtils.multiply(price, quantity))
            .setCompletedDateTime(ZonedDateTime.parse(updatedAt));
    }
}