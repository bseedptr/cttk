package cttk.impl.poloniex;

import cttk.CXClientFactory;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.UserDataStreamOpener;
import cttk.auth.Credential;
import cttk.impl.RetryPrivateCXClient;
import cttk.impl.poloniex.stream.PoloniexUserDataStreamOpener;

public class PoloniexClientFactory
    implements CXClientFactory
{
    @Override
    public PublicCXClient create() {
        return new PoloniexPublicClient();
    }

    @Override
    public PrivateCXClient create(Credential credential) {
        return new RetryPrivateCXClient(5, 5, new PoloniexPrivateClient(credential), "Nonce must be");
    }

    @Override
    public UserDataStreamOpener createUserDataStreamOpener(Credential credential) {
        return new PoloniexUserDataStreamOpener(create(credential));
    }
}
