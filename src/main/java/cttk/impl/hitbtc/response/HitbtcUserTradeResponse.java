package cttk.impl.hitbtc.response;

import static cttk.impl.hitbtc.HitbtcUtils.guessFeeCurrency;
import static cttk.impl.hitbtc.HitbtcUtils.parseMarketSymbol;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.dto.UserTrade;
import cttk.impl.util.MathUtils;
import lombok.Data;

@Data
public class HitbtcUserTradeResponse {
    String id;
    String orderId;
    String clientOrderId;
    String symbol;
    String side;
    BigDecimal price;
    BigDecimal quantity;
    BigDecimal fee;
    String timestamp;

    public UserTrade toUserTrade() {
        CurrencyPair pair = parseMarketSymbol(symbol);
        String feeCurrency = guessFeeCurrency(pair);
        return new UserTrade()
            .setCurrencyPair(pair)
            .setTid(id)
            .setOid(orderId)
            .setClientOrderId(clientOrderId)
            .setDateTime(ZonedDateTime.parse(timestamp))
            .setSide(OrderSide.of(side))
            .setPrice(price)
            .setQuantity(quantity)
            .setTotal(MathUtils.multiply(price, quantity))
            .setFee(fee)
            .setFeeCurrency(feeCurrency);
    }
}
