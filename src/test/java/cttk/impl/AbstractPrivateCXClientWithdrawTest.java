package cttk.impl;

import static cttk.test.TestUtils.hasTestProfile;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import cttk.CXClientFactory;
import cttk.PrivateCXClient;
import cttk.auth.CXCredentialsProvider;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCXException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;

abstract public class AbstractPrivateCXClientWithdrawTest
    extends AbstractCXClientTest
{
    protected PrivateCXClient client;

    @Before
    public void setup()
        throws CTTKException, IOException
    {
        org.junit.Assume.assumeTrue(hasTestProfile("test-withdraw"));
        client = CXClientFactory.createPrivateCXClient(getCxId(),
            CXCredentialsProvider.getDefaultCredential(getCxId()).getWithdrawKeyOrDataKey());
    }

    @Test(expected = UnsupportedMethodException.class)
    public void testWrongWithdrawAPIKey()
        throws UnsupportedCXException, CTTKException, IOException
    {
        throw new UnsupportedMethodException();
    }

    @Test
    abstract public void testWithdrawToBankAccount()
        throws UnsupportedCurrencyPairException, CTTKException;

    @Test
    abstract public void testWithdrawBTCToWallet()
        throws UnsupportedCurrencyPairException, CTTKException;
}
