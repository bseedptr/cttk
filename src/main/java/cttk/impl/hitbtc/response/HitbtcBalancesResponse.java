package cttk.impl.hitbtc.response;

import static cttk.impl.hitbtc.HitbtcUtils.toStdCurrencySymbol;

import java.math.BigDecimal;

import cttk.dto.Balance;
import cttk.impl.util.MathUtils;
import lombok.Data;

@Data
public class HitbtcBalancesResponse {
    String currency;
    BigDecimal available;
    BigDecimal reserved;

    BigDecimal getTotal() {
        return MathUtils.add(available, reserved);
    }

    public Balance toBalance() {
        return new Balance()
            .setCurrency(toStdCurrencySymbol(currency))
            .setTotal(getTotal())
            .setAvailable(available)
            .setInUse(reserved);
    }
}