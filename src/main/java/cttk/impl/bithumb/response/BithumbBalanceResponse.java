package cttk.impl.bithumb.response;

import static cttk.impl.bithumb.BithumbUtils.toStdCurrencySymbol;
import static cttk.util.StringUtils.toUpper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cttk.dto.Balance;
import cttk.dto.Balances;
import cttk.util.StringUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BithumbBalanceResponse
    extends BithumbDataResponse<BithumbBalance>
{
    public Balances getBalances() {
        return data == null ? null : data.getBalances();
    }
}

@Data
@EqualsAndHashCode(callSuper = true)
class BithumbBalance
    extends HashMap<String, String>
{
    public Balances getBalances() {
        final Map<String, Balance> balanceMap = new HashMap<>();

        forEach((k, v) -> {
            final int idx = k.lastIndexOf('_');
            final String symbol = toStdCurrencySymbol(k.substring(idx + 1));
            final BigDecimal quantity = StringUtils.parseBigDecimal(v);
            balanceMap.putIfAbsent(symbol, new Balance().setCurrency(symbol));
            final Balance balance = balanceMap.get(symbol);
            switch (toUpper(k.substring(0, idx))) {
                case "TOTAL":
                    balance.setTotal(quantity);
                    break;
                case "AVAILABLE":
                    balance.setAvailable(quantity);
                    break;
                case "IN_USE":
                    balance.setInUse(quantity);
                    break;
            }
        });

        return new Balances().setBalances(new ArrayList<>(balanceMap.values()));
    }
}
