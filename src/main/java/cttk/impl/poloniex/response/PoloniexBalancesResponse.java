package cttk.impl.poloniex.response;

import static cttk.impl.poloniex.PoloniexUtils.toStdCurrencySymbol;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import cttk.dto.Balance;
import cttk.dto.Balances;
import cttk.exception.UnsupportedCurrencyException;
import cttk.impl.util.MathUtils;
import lombok.Data;

@Data
public class PoloniexBalancesResponse {
    private Map<String, PoloniexBalance> map = new HashMap<>();

    @JsonAnyGetter
    public Map<String, PoloniexBalance> any() {
        return this.map;
    }

    @JsonAnySetter
    public void set(String name, PoloniexBalance value) {
        this.map.put(name, value);
    }

    public Balance toBalance(String currency)
        throws UnsupportedCurrencyException
    {
        if (!map.containsKey(currency)) throw new UnsupportedCurrencyException(currency);
        return map.get(currency).toBalance(currency);
    }

    public Balances toBalances() {
        List<Balance> list = map.entrySet().stream()
            .map(e -> e.getValue().toBalance(e.getKey()))
            .collect(Collectors.toList());

        return new Balances().setBalances(list);
    }
}

@Data
class PoloniexBalance {
    BigDecimal available;
    BigDecimal onOrders;
    BigDecimal btcValue;

    BigDecimal getTotal() {
        return MathUtils.add(available, onOrders);
    }

    public Balance toBalance(String currency) {
        return new Balance()
            .setCurrency(toStdCurrencySymbol(currency))
            .setTotal(getTotal())
            .setAvailable(available)
            .setInUse(onOrders);
    }
}