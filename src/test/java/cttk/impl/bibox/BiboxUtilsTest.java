package cttk.impl.bibox;

import java.io.IOException;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import cttk.impl.util.JsonUtils;

public class BiboxUtilsTest {
    @Test
    public void testParsePercent() {
        Assert.assertNotNull(BiboxUtils.parsePercentString("-1.4%"));
        Assert.assertNotNull(BiboxUtils.parsePercentString("-1.4"));
        Assert.assertNull(BiboxUtils.parsePercentString(null));
    }

    @Test
    public void testParseResponseError()
        throws IOException
    {
        String json = "{\"error\":{\"code\":\"3016\",\"msg\":\"交易对错误\"},\"cmd\":\"depth\",\"ver\":\"1.1\"}";
        Map<String, Object> map = BiboxUtils.parseResponseError(JsonUtils.createDefaultObjectMapper(), json);

        Assert.assertEquals("3016", map.get("code"));
        Assert.assertEquals("交易对错误", map.get("msg"));
    }

    @Test
    public void testParseResponseErrorWhenMessageWasWrapped()
        throws IOException
    {
        String json = "{\"result\":{\"error\":{\"code\":\"3016\",\"msg\":\"交易对错误\"}},\"cmd\":\"depth\",\"ver\":\"1.1\"}";
        Map<String, Object> map = BiboxUtils.parseResponseError(JsonUtils.createDefaultObjectMapper(), json);

        Assert.assertEquals("3016", map.get("code"));
        Assert.assertEquals("交易对错误", map.get("msg"));
    }

    @Test
    public void testParseResponseErrorWhenMessagesWereDuplicated()
        throws IOException
    {
        String json = "{\"result\":[{\"error\":{\"code\":\"3016\",\"msg\":\"交易对错误\"},\"cmd\":\"orderpending/trade\"}],\"error\":{\"code\":\"3016\",\"msg\":\"交易对错误\"},\"cmd\":\"orderpending/trade\"}";
        Map<String, Object> map = BiboxUtils.parseResponseError(JsonUtils.createDefaultObjectMapper(), json);

        Assert.assertEquals("3016", map.get("code"));
        Assert.assertEquals("交易对错误", map.get("msg"));
    }
}