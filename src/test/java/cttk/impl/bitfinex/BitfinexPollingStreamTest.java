package cttk.impl.bitfinex;

import cttk.CXIds;
import cttk.impl.AbstractPollingCXStreamTest;

public class BitfinexPollingStreamTest
    extends AbstractPollingCXStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.BITFINEX;
    }

    @Override
    protected long getIntervalInMilli() {
        return 3000l;
    }
}
