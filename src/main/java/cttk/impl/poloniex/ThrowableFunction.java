package cttk.impl.poloniex;

import cttk.exception.CTTKException;

@FunctionalInterface
public interface ThrowableFunction<T, R> {
    R apply(T t)
        throws CTTKException;
}
