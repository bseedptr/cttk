package cttk.impl.huobi;

import static cttk.dto.UserOrders.Category.ALL;
import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import cttk.CXIds;
import cttk.TransferType;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.dto.UserOrders;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.AbstractPrivateCXClientDataTest;
import cttk.impl.util.ExNotRaisedException;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserOrderHistoryRequest;

public class HuobiPrivateClientDataTest
    extends AbstractPrivateCXClientDataTest
{
    @AfterClass
    public static void cleanup() {
        cleanup(HuobiPrivateClientDataTest.class);
    }

    @Override
    public String getCxId() {
        return CXIds.HUOBI;
    }

    @Override
    protected long getThreadSleepMillis() {
        return 1000l;
    }

    @Override
    @Test(expected = UnsupportedMethodException.class)
    public void testGetBankAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetBankAccount();
    }

    @Override
    @Test(expected = UnsupportedMethodException.class)
    public void testGetWallet()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetWallet();
    }

    @Override
    @Test(expected = UnsupportedMethodException.class)
    public void testGetAllWallets()
        throws CTTKException
    {
        super.testGetAllWallets();
    }

    @Test
    @Override
    public void testGetTransfers()
        throws CTTKException
    {
        final Transfers transfers = client.getTransfers(new GetTransfersRequest()
            .setCurrency("eth")
            .setType(TransferType.DEPOSIT)
            .setFromId("1")
            .setCount(100));

        Assert.assertNotNull(transfers);
        Assert.assertTrue(transfers.isNotEmpty());
        for (Transfer transfer : transfers.getTransfers()) {
            Assert.assertNotNull(transfer.getStatus());
            Assert.assertNotNull(transfer.getStatusType());
        }
    }

    @Test
    public void testGetTransfersWithdrawal()
        throws CTTKException
    {
        final Transfers transfers = client.getTransfers(new GetTransfersRequest()
            .setCurrency("eth")
            .setType(TransferType.WITHDRAWAL)
            .setFromId("1")
            .setCount(100));

        Assert.assertNotNull(transfers);
        Assert.assertTrue(transfers.isNotEmpty());
        for (Transfer transfer : transfers.getTransfers()) {
            Assert.assertNotNull(transfer.getStatus());
            Assert.assertNotNull(transfer.getStatusType());
        }
    }

    @Test
    @Override
    public void testGetTransfersWrongCurrency()
        throws CTTKException
    {
        try {
            client.getTransfers(new GetTransfersRequest()
                .setCurrency(invalidCurrency)
                .setType(TransferType.DEPOSIT)
                .setFromId("1")
                .setCount(100));
        } catch (UnsupportedCurrencyException e) {
            assertEquals(e.getCurrency(), invalidCurrency);
            assertEquals(e.getCxId(), this.getCxId());
            return;
        }
        throw new ExNotRaisedException(raiseExceptionMsg);

    }

    @Override
    @Test(expected = UnsupportedMethodException.class)
    public void testGetAllOpenUserOrders()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetAllOpenUserOrders();
    }

    @Test
    public void testGetUserOrderHistoryCategory()
        throws CTTKException
    {
        threadSleep();

        final GetUserOrderHistoryRequest request = new GetUserOrderHistoryRequest()
            .setCurrencyPair(getValidCurrencyPair());
        final UserOrders userOrders = client.getUserOrderHistory(request);

        logger.debug("{}", userOrders);

        Assert.assertEquals(userOrders.getCategory(), ALL);
    }

}
