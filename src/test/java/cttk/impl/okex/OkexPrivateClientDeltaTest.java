package cttk.impl.okex;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.anyMap;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.powermock.api.support.membermodification.MemberMatcher.method;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberModifier;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import cttk.CXClientFactory;
import cttk.PublicCXClient;
import cttk.auth.Credential;
import cttk.dto.UserAccount;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.AbstractPrivateClientDeltaTest;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ThrowableFunction;
import cttk.request.GetTransfersDeltaRequest;
import cttk.request.GetUserOrderHistoryDeltaRequest;
import cttk.request.GetUserTradesDeltaRequest;
import lombok.extern.slf4j.Slf4j;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ CXClientFactory.class, OkexPrivateClient.class })
@PowerMockIgnore("javax.net.ssl.*")
@Slf4j
public class OkexPrivateClientDeltaTest
    extends AbstractPrivateClientDeltaTest
{
    OkexPrivateClient client;
    GetUserOrderHistoryDeltaRequest orderHistoryRequest;
    GetUserTradesDeltaRequest tradesRequest;

    @Before
    public void setup()
        throws CTTKException
    {
        client = PowerMockito.spy(createDummyPrivateClient());
        orderHistoryRequest = new GetUserOrderHistoryDeltaRequest().setStartDateTime(ZonedDateTime.now().minusYears(2));
        tradesRequest = new GetUserTradesDeltaRequest().setStartDateTime(ZonedDateTime.now().minusYears(2));
    }

    OkexPrivateClient createDummyPrivateClient()
        throws CTTKException
    {
        PowerMockito.mockStatic(CXClientFactory.class);
        PowerMockito.when(CXClientFactory.createPublicCXClient(anyString())).thenReturn(mock(PublicCXClient.class));
        MemberModifier.stub(method(OkexPrivateClient.class, "getUserAccount")).toReturn(mock(UserAccount.class));
        return new OkexPrivateClient(mock(Credential.class));
    }

    @Test(expected = UnsupportedMethodException.class)
    public void testGetTransfersDelta()
        throws CTTKException
    {
        client.getTransfers(new GetTransfersDeltaRequest());
    }

    @Test
    public void testGetAllOpenUserOrdersPagenation()
        throws Exception
    {
        Map<String, List<String>> headers1 = new HashMap<String, List<String>>() {{
            put("OK-BEFORE", new ArrayList<String>() {{ add("12836626382");}});
            put("OK-AFTER", new ArrayList<String>() {{ add("12434264952");}});
        }};
        Map<String, List<String>> headers2 = new HashMap<String, List<String>>() {{
            put("OK-BEFORE", new ArrayList<String>() {{ add("12878926579");}});
            put("OK-AFTER", new ArrayList<String>() {{ add("12878926579");}});
        }};
        Map<String, List<String>> headers3 = new HashMap<String, List<String>>() {{
        }};

        PowerMockito.doReturn(
            (new HttpResponse().setHeaders(headers1).setStatus(200)),
            (new HttpResponse().setHeaders(headers2).setStatus(200)),
            (new HttpResponse().setHeaders(headers3).setStatus(200))
        ).when(client, "requestGetWithSign", anyString(), anyString(), anyMap());
        PowerMockito.doReturn(
            null,
            null
        ).when(client, "convert", any(HttpResponse.class), any(Class.class), any(ThrowableFunction.class));

        client.getAllOpenUserOrders(any(Boolean.class));
        PowerMockito.verifyPrivate(client, times(3)).invoke("requestGetWithSign", anyString(), anyString(), anyMap());
    }
}
