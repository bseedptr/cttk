package cttk.impl.coinbase;

import cttk.CXIds;
import cttk.impl.AbstractPollingCXStreamTest;

public class CoinbasePollingStreamTest
    extends AbstractPollingCXStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.COINBASE;
    }
}
