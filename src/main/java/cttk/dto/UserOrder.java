package cttk.dto;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cttk.AbstractCurrencyPair;
import cttk.CurrencyPair;
import cttk.DateTimeHolder;
import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.PricedQuantity;
import cttk.PricingType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(chain = true)
public class UserOrder
    extends AbstractCurrencyPair<UserOrder>
    implements DateTimeHolder<UserOrder>, PricedQuantity
{
    Long index;
    String oid;
    String clientOrderId;
    ZonedDateTime dateTime;
    OrderSide side;
    PricingType pricingType;
    String status;
    OrderStatus statusType;
    BigDecimal price;
    BigDecimal quantity;
    BigDecimal avgPrice;
    BigDecimal filledQuantity;
    BigDecimal remainingQuantity;
    BigDecimal total;
    BigDecimal fee;
    String feeCurrency;
    BigDecimal feeRate;
    ZonedDateTime completedDateTime;
    ZonedDateTime canceledDateTime;

    List<UserTrade> trades;

    public boolean hasTrades() {
        return trades != null && !trades.isEmpty();
    }

    @JsonIgnore
    public BigDecimal getTradedQuantity() {
        return trades == null || trades.size() == 0 ? null
            : trades.stream().map(i -> i.getQuantity()).reduce((a, b) -> a.add(b)).get();
    }

    @JsonIgnore
    public List<String> getTids() {
        if (trades == null) return null;
        return trades.stream()
            .map(t -> t.getTid())
            .collect(Collectors.toList());
    }

    public List<UserTrade> getTrades() {
        if (trades != null) {
            final CurrencyPair cp = this;
            trades.forEach(i -> i.setCurrencyPair(cp));
        }
        return trades;
    }

    public static boolean equalsById(UserOrder a, UserOrder b) {
        return a == null && b == null
            || a != null && b != null && a.getOid().equals(b.getOid());
    }

}
