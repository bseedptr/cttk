package cttk.impl.hitbtc.response;

import java.math.BigDecimal;

import cttk.dto.TradingFee;
import lombok.Data;

@Data
public class HitbtcTradingFeeResponse {
    BigDecimal takeLiquidityRate;       // Default fee rate
    BigDecimal provideLiquidityRate;    // Default fee rate for market making trades

    public TradingFee getTradingFee() {
        return new TradingFee()
            .setTakerFee(takeLiquidityRate)
            .setMakerFee(provideLiquidityRate);
    }
}
