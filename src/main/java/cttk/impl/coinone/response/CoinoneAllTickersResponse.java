package cttk.impl.coinone.response;

import static cttk.impl.coinone.CoinoneUtils.PRICE_INCREMENTS;
import static cttk.impl.coinone.CoinoneUtils.PRICE_RANGES;
import static cttk.impl.coinone.CoinoneUtils.toStdCurrencySymbol;
import static cttk.impl.util.DateTimeUtils.zdtFromEpochSecond;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import cttk.dto.MarketInfo;
import cttk.dto.MarketInfos;
import cttk.dto.Ticker;
import cttk.dto.Tickers;
import cttk.impl.util.MathUtils;
import cttk.impl.util.ZoneIds;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CoinoneAllTickersResponse
    extends CoinoneResponse
{
    String timestamp;
    Map<String, CoinoneTicker> map = new HashMap<>();

    @JsonAnyGetter
    public Map<String, CoinoneTicker> any() {
        return this.map;
    }

    @JsonAnySetter
    public void set(String name, CoinoneTicker value) {
        this.map.put(name, value);
    }

    public Tickers toAllTickers() {
        return new Tickers()
            .setTickers(toTickerList());
    }

    private List<Ticker> toTickerList() {
        final ZonedDateTime dateTime = zdtFromEpochSecond(Long.parseLong(timestamp), ZoneIds.SEOUL);
        return this.map.entrySet().stream()
            .map(e -> e.getValue().toTicker()
                .setCurrencyPair(toStdCurrencySymbol(e.getKey()), "KRW")
                .setDateTime(dateTime))
            .collect(Collectors.toList());
    }

    public MarketInfos toMarketInfos() {
        return new MarketInfos()
            .setMarketInfos(toMarketInfoList());
    }

    private List<MarketInfo> toMarketInfoList() {
        return this.map.entrySet().stream()
            .map(e -> {
                return new MarketInfo()
                    .setCurrencyPair(toStdCurrencySymbol(e.getKey()), "KRW")
                    .setPriceIncrement(MathUtils.findIncrement(PRICE_RANGES, PRICE_INCREMENTS, e.getValue().last));
            })
            .collect(Collectors.toList());
    }
}

@Data
class CoinoneTicker {
    String currency;

    BigDecimal first;
    BigDecimal last;
    BigDecimal low;
    BigDecimal high;
    BigDecimal volume;

    BigDecimal yesterday_first;
    BigDecimal yesterday_last;
    BigDecimal yesterday_low;
    BigDecimal yesterday_high;
    BigDecimal yesterday_volume;

    public Ticker toTicker() {
        return new Ticker()
            .setOpenPrice(first)
            .setLastPrice(last)
            .setLowPrice(low)
            .setHighPrice(high)
            .setVolume(volume);
    }
}