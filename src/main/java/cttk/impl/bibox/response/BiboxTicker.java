package cttk.impl.bibox.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import cttk.dto.Ticker;
import cttk.impl.bibox.BiboxUtils;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class BiboxTicker {
    String pair;
    @JsonProperty("last")
    BigDecimal lastPrice;
    @JsonProperty("last_usd")
    BigDecimal lastPriceOfUsd;
    @JsonProperty("last_cny")
    BigDecimal lastPriceOfCny;
    @JsonProperty("high")
    BigDecimal highPrice;
    @JsonProperty("low")
    BigDecimal lowPrice;
    @JsonProperty("buy")
    BigDecimal bidPrice;
    @JsonProperty("buy_amount")
    BigDecimal bidQuantity;
    @JsonProperty("sell")
    BigDecimal askPrice;
    @JsonProperty("sell_amount")
    BigDecimal askQuantity;
    @JsonProperty("vol")
    BigDecimal volume;
    String percent;
    Long timestamp;

    // websocket value
    @JsonProperty("base_last_cny")
    BigDecimal baseLastCny;

    public Ticker toTicker() {
        return new Ticker()
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(timestamp))
            .setLastPrice(lastPrice)
            .setLowPrice(lowPrice)
            .setHighPrice(highPrice)
            .setVolume(volume)
            .setHighestBid(bidPrice)
            .setLowestAsk(askPrice)
            .setChangePercent(BiboxUtils.parsePercentString(percent));
    }
}
