package cttk.request;

import cttk.AbstractCurrencyPair;
import cttk.CXStreamConfig;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class AbstractCXStreamRequest<T extends AbstractCXStreamRequest<T>>
    extends AbstractCurrencyPair<T>
    implements CXStreamConfig
{
    /**
     * Stream is implemented by polling, min interval is set by intervalInMilli.
     */
    protected long intervalInMilli = DEFAULT_INTERVAL_IN_MILLI;

    /**
     * If poll request fails, implementing class retry with backoff interval at most maxPollRetryCount times.
     * If poll request fails at the amount of maxPollRetryCount, the stream thread will stop.
     */
    protected int maxPollRetryCount = DEFAULT_MAX_POLL_RETRY_COUNT;

    @SuppressWarnings("unchecked")
    public T setIntervalInMilli(long intervalInMilli) {
        this.intervalInMilli = intervalInMilli;
        return (T) this;
    }

    @SuppressWarnings("unchecked")
    public T setMaxPollRetryCount(int maxPollRetryCount) {
        this.maxPollRetryCount = maxPollRetryCount;
        return (T) this;
    }

}
