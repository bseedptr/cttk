package cttk.impl.bittrex.response;

import java.util.List;
import java.util.stream.Collectors;

import cttk.dto.Ticker;
import cttk.dto.Tickers;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BittrexMarketSummariesResponse
    extends BittrexResponse<List<BittrexMarketSummary>>
{
    public Tickers toTickers() {
        return new Tickers()
            .setTickers(toTickerList(result));
    }

    private static List<Ticker> toTickerList(List<BittrexMarketSummary> list) {
        if (list == null) return null;
        return list.stream().map(i -> i.toTicker()).collect(Collectors.toList());
    }
}
