package cttk.impl.binance.response;

import lombok.Data;

@Data
public class BinanceServerTimeResponse {
    Long serverTime;
}
