package cttk.impl.util;

import lombok.Getter;

@Getter
public class Timer {
    long start;
    long end;

    public Timer() {
        start();
    }

    public Timer start() {
        start = System.currentTimeMillis();
        return this;
    }

    public Timer stop() {
        end = System.currentTimeMillis();
        return this;
    }

    public long getIntervalInMilli() {
        return end - start;
    }

    public double getIntervalInSecond() {
        return (end - start) / 1000.0;
    }

    public static Timer create() {
        return new Timer();
    }
}
