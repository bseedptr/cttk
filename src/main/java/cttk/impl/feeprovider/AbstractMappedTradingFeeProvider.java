package cttk.impl.feeprovider;

import java.util.HashMap;
import java.util.Map;

import cttk.CurrencyPair;
import cttk.dto.TradingFee;
import cttk.exception.CTTKException;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public abstract class AbstractMappedTradingFeeProvider<T extends AbstractMappedTradingFeeProvider<T>>
    extends AbstractTradingFeeProvider<T>
{
    Map<String, TradingFee> feeMap = new HashMap<>();

    @Override
    public TradingFee getFee(CurrencyPair currencyPair)
        throws CTTKException
    {
        return feeMap.get(getKey(currencyPair));
    }

    public abstract String getKey(CurrencyPair currencyPair);
}
