package cttk.impl.chargingpolicy;

import java.util.ArrayList;
import java.util.List;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.dto.ChargingPolicy;
import cttk.dto.ChargingPolicy.Criteria;
import cttk.exception.CTTKException;

public class DefaultChargingPolicyProvider
    extends AbstractChargingPolicyProvider
{
    public DefaultChargingPolicyProvider(String cxId) {
        super(cxId);
    }

    @Override
    public List<ChargingPolicy> getChargingPolicies(CurrencyPair currencyPair, OrderSide orderSide)
        throws CTTKException
    {
        checkParameters(currencyPair, orderSide);

        List<ChargingPolicy> policies = new ArrayList<>();
        if (orderSide == OrderSide.BUY)
            policies.add(new ChargingPolicy(currencyPair, orderSide, currencyPair.getBaseCurrency(), Criteria.SUB));
        else
            policies.add(new ChargingPolicy(currencyPair, orderSide, currencyPair.getQuoteCurrency(), Criteria.SUB));
        return policies;
    }
}
