package cttk.impl.bibox;

import static cttk.impl.bibox.BiboxUtils.getMarketSymbol;
import static cttk.impl.util.Objects.nvl;

import cttk.PublicCXClient;
import cttk.dto.MarketInfo;
import cttk.dto.MarketInfos;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Tickers;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.bibox.response.BiboxAllTickersResponse;
import cttk.impl.bibox.response.BiboxMarketAllResponse;
import cttk.impl.bibox.response.BiboxOrderBookResponse;
import cttk.impl.bibox.response.BiboxTickerResponse;
import cttk.impl.bibox.response.BiboxTradeLimitResponse;
import cttk.impl.bibox.response.BiboxTradeResponse;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ParamsBuilder;
import cttk.request.GetOrderBookRequest;
import cttk.request.GetRecentTradesRequest;
import cttk.request.GetTickerRequest;
import cttk.request.GetTradeHistoryRequest;

public class BiboxPublicClient
    extends AbstractBiboxClient
    implements PublicCXClient
{
    @Override
    public MarketInfos getMarketInfos()
        throws CTTKException
    {
        final HttpResponse response = requestGet("mdata",
            ParamsBuilder.create()
                .put("cmd", "marketAll")
                .build());

        checkResponseError(null, response);

        MarketInfos marketInfos = convert(response, BiboxMarketAllResponse.class, BiboxMarketAllResponse::toMarketInfos);

        fillMarketInfos(marketInfos);

        return marketInfos;
    }

    private void fillMarketInfos(final MarketInfos marketInfos)
        throws CTTKException
    {
        final HttpResponse response = requestGet("orderpending",
            ParamsBuilder.create()
                .put("cmd", "tradeLimit")
                .build());

        BiboxTradeLimitResponse biboxTradeLimitResponse = convert(response, BiboxTradeLimitResponse.class);
        marketInfos.getMarketInfos().forEach(marketInfo -> {
            marketInfo.setMinPrice(biboxTradeLimitResponse.getMinPrice(marketInfo));
            marketInfo.setMinQuantity(biboxTradeLimitResponse.getMinQuantity(marketInfo));
            marketInfo.setMinTotal(biboxTradeLimitResponse.getMinTotal(marketInfo));
        });
    }

    @Override
    public Ticker getTicker(GetTickerRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet("mdata",
            ParamsBuilder.create()
                .put("cmd", "ticker")
                .put("pair", getMarketSymbol(request))
                .build());
        checkResponseError(request, response);
        return convert(response, BiboxTickerResponse.class, BiboxTickerResponse::toTicker);
    }

    @Override
    public Tickers getAllTickers()
        throws CTTKException
    {
        final HttpResponse response = requestGet("mdata",
            ParamsBuilder.create()
                .put("cmd", "marketAll")
                .build());
        checkResponseError(null, response);
        return convert(response, BiboxAllTickersResponse.class, BiboxAllTickersResponse::toTickers);
    }

    @Override
    public OrderBook getOrderBook(GetOrderBookRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        request.refineCountWithMax(MAX_ORDERBOOK_COUNT);
        final HttpResponse response = requestGet("mdata",
            ParamsBuilder.create()
                .put("cmd", "depth")
                .put("pair", getMarketSymbol(request))
                .putIfNotNull("size", nvl(request.getCount(), 100))
                .build());
        checkResponseError(request, response);
        return convert(response, BiboxOrderBookResponse.class, BiboxOrderBookResponse::getOrderBook);
    }

    @Override
    public Trades getRecentTrades(GetRecentTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        request.refineCountWithMax(MAX_RECENT_TRANSACTION_COUNT);
        final HttpResponse response = requestGet("mdata",
            ParamsBuilder.create()
                .put("cmd", "deals")
                .put("pair", getMarketSymbol(request))
                .putIfNotNull("size", nvl(request.getCount(), 100))
                .build());
        checkResponseError(request, response);
        return convert(response, BiboxTradeResponse.class, (BiboxTradeResponse::toTrades));
    }

    @Override
    public Trades getTradeHistory(GetTradeHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }
}
