package cttk.impl.coinone;

import cttk.CXIds;
import cttk.impl.AbstractPollingCXStreamTest;

public class CoinonePollingStreamTest
    extends AbstractPollingCXStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.COINONE;
    }
}
