package cttk.example;

import cttk.CXClientFactory;
import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.dto.Trade;
import cttk.dto.Trades;
import cttk.request.OpenTradeStreamRequest;

public class OpenTradeStreamExample {

    public static void main(String[] args)
        throws Exception
    {
        final CXStream<Trades> stream = CXClientFactory
            .createCXStreamOpener(CXIds.BINANCE)
            .openTradeStream(
                new OpenTradeStreamRequest().setCurrencyPair("ETH", "BTC"),
                createPrintOrderBook());

        Thread.sleep(60000);
        stream.close();
    }

    public static CXStreamListener<Trades> createPrintOrderBook() {
        return new CXStreamListener<Trades>() {

            @Override
            public void onMessage(Trades trades) {
                System.out.println(trades.size());
                for (Trade t : trades.getTrades()) {
                    System.out.println(t);
                }
                System.out.println("========================================");
            }

            @Override
            public void onFailure(CXStream<Trades> stream, Throwable t) {
                t.printStackTrace();
            }
        };
    }
}
