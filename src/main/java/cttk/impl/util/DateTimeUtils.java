package cttk.impl.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeUtils {
    private static final BigDecimal GIGS_BD = new BigDecimal("1000000000");
    private static final BigInteger GIGS_BI = new BigInteger("1000000000");

    public static ZonedDateTime parseZdt(String str) {
        return str == null ? null : ZonedDateTime.parse(str);
    }

    public static ZonedDateTime zdtFromEpochSecond(Long epochSecond) {
        return zdtFromEpochSecond(epochSecond, ZoneOffset.UTC);
    }

    public static ZonedDateTime zdtFromEpochSecond(Long epochSecond, ZoneId zoneId) {
        return epochSecond == null ? null : ZonedDateTime.ofInstant(Instant.ofEpochSecond(epochSecond), zoneId);
    }

    public static ZonedDateTime zdtFromEpochSecond(BigDecimal epochSecond) {
        return zdtFromEpochSecond(epochSecond, ZoneOffset.UTC);
    }

    public static ZonedDateTime zdtFromEpochSecond(BigDecimal epochSecond, ZoneId zoneId) {
        if (epochSecond == null) return null;
        long nanoSeconds = epochSecond.multiply(GIGS_BD).toBigInteger()
            .subtract(epochSecond.toBigInteger().multiply(GIGS_BI))
            .longValue();

        return ZonedDateTime.ofInstant(Instant.ofEpochSecond(epochSecond.longValue(), nanoSeconds), zoneId);
    }

    public static ZonedDateTime zdtFromEpochMilli(Long epochMilli) {
        return zdtFromEpochMilli(epochMilli, ZoneOffset.UTC);
    }

    public static ZonedDateTime zdtFromEpochMilli(Long epochMilli, ZoneId zoneId) {
        return epochMilli == null ? null : ZonedDateTime.ofInstant(Instant.ofEpochMilli(epochMilli), zoneId);
    }

    public static ZonedDateTime zdtFromEpochMicro(Long epochMicro) {
        return zdtFromEpochMicro(epochMicro, ZoneOffset.UTC);
    }

    public static ZonedDateTime zdtFromEpochMicro(Long epochMicro, ZoneId zoneId) {
        return epochMicro == null ? null
            : ZonedDateTime.ofInstant(Instant.ofEpochSecond(getSecondFromMicro(epochMicro), getNanoFromMicro(epochMicro)), zoneId);
    }

    private static Long getSecondFromMicro(Long epochMicro) {
        return epochMicro == null ? null : epochMicro / 1000000l;
    }

    private static Long getNanoFromMicro(Long epochMicro) {
        return epochMicro == null ? null : (epochMicro % 1000000l) * 1000;
    }

    private static final DateTimeFormatter DTF = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static String toISODateString(ZonedDateTime dateTime) {
        return dateTime == null ? null : DTF.format(dateTime);
    }

    public static String toISODateTimeString(ZonedDateTime dateTime) {
        return dateTime == null ? null : DateTimeFormatter.ISO_DATE_TIME.format(dateTime);
    }

    public static Long toEpochMilli(ZonedDateTime dateTime) {
        return dateTime == null ? null : dateTime.toInstant().toEpochMilli();
    }

    public static boolean isBefore(ZonedDateTime from, ZonedDateTime to) {
        return (from == null || to == null) ? false : from.isBefore(to);
    }
}
