package cttk.impl.okex;

import static cttk.CurrencyPair.SimpleCurrencyPair;
import static cttk.OrderSide.BUY;
import static cttk.OrderSide.SELL;

import org.junit.Assert;
import org.junit.Test;

import cttk.CurrencyPair;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OkexUtilsTest {
    @Test
    public void testParseMarketSymbol() {
        final CurrencyPair pair = OkexUtils.getMarketSymbol("bch_btc");
        Assert.assertEquals("BCH", pair.getBaseCurrency());
        Assert.assertEquals("BTC", pair.getQuoteCurrency());
    }

    @Test
    public void testGuessFeeCurrencyIfOderSideIsBUYThenBaseCurrency() {
        SimpleCurrencyPair pair = new SimpleCurrencyPair("EOS", "ETH");
        String feeCurrency = OkexUtils.guessFeeCurrency(BUY, pair);

        Assert.assertEquals("Should be 'EOS'.", pair.getBaseCurrency(), feeCurrency);
    }

    @Test
    public void testGuessFeeCurrencyIfOderSideIsSELLThenQuoteCurrency() {
        SimpleCurrencyPair pair = new SimpleCurrencyPair("EOS", "ETH");
        String feeCurrency = OkexUtils.guessFeeCurrency(SELL, pair);

        Assert.assertEquals("Should be 'ETH'.", pair.getQuoteCurrency(), feeCurrency);
    }
}
