package cttk.impl.korbit.response;

import static cttk.impl.util.DateTimeUtils.zdtFromEpochMilli;
import static cttk.util.StringUtils.toUpper;

import java.math.BigDecimal;

import cttk.OrderSide;
import cttk.dto.UserTrade;
import cttk.impl.util.ZoneIds;
import lombok.Data;

@Data
public class KorbitUserTrade {
    Long timestamp;
    Long completedAt;
    String id;
    String type;
    KorbitFee fee;
    KorbitFillsDetail fillsDetail;

    public UserTrade toUserTrade() {
        return new UserTrade()
            .setCurrencyPair(toUpper(fillsDetail.amount.currency), toUpper(fillsDetail.price.currency))
            .setTid(id)
            .setOid(fillsDetail.getOrderId())
            .setDateTime(zdtFromEpochMilli(completedAt, ZoneIds.SEOUL))
            .setSide(OrderSide.of(type))
            .setPrice(fillsDetail.price.value)
            .setQuantity(fillsDetail.amount.value)
            .setTotal(fillsDetail.native_amount.value)
            .setFee(fee.value)
            .setFeeCurrency(toUpper(fee.currency));
    }
}

@Data
class KorbitFee {
    String currency;
    BigDecimal value;
}

@Data
class KorbitFillsDetail {
    String orderId;
    KorbitFillsDetailPrice price;
    KorbitFillsDetailAmount amount;
    KorbitFillsDetailNativeAmount native_amount;
}

@Data
class KorbitFillsDetailPrice {
    String currency;
    BigDecimal value;
}

@Data
class KorbitFillsDetailAmount {
    String currency;
    BigDecimal value;
}

@Data
class KorbitFillsDetailNativeAmount {
    String currency;
    BigDecimal value;
}