package cttk.impl.huobi.response;

import java.util.List;

import cttk.dto.UserAccount;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HuobiAccountsResponse
    extends HuobiResponse
{
    List<HuobiAccount> data;

    public String getWorkingAccountId() {
        return data.stream().filter(d -> d.getState().equals("working")).findFirst().map(d -> String.valueOf(d.getId())).orElse("");
    }

    public UserAccount getUserAccount() {
        return data == null ? null
            : data.stream().filter(d -> d.getState().equals("working"))
                .findFirst().map(i -> new UserAccount()
                    .setAid(String.valueOf(i.getId()))
                    .setState(i.getState())
                    .setType(i.getType()))
                .orElse(new UserAccount());
    }
}
