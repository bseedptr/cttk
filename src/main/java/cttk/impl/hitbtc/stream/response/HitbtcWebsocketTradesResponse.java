package cttk.impl.hitbtc.stream.response;

import static cttk.impl.hitbtc.HitbtcUtils.toStdCurrencyPair;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.dto.Trade;
import cttk.dto.Trades;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HitbtcWebsocketTradesResponse
    extends HitbtcWebsocketResponse<HitbtcTradesTick>
{
    public Trades toTrades(CurrencyPair currencyPair) {
        return new Trades()
            .setCurrencyPair(toStdCurrencyPair(currencyPair))
            .setTrades(toTradeList(params.data));
    }

    static List<Trade> toTradeList(List<HitbtcTrade> list) {
        if (list == null) return null;

        return list.stream()
            .map(t -> t.toTrade())
            .collect(Collectors.toList());
    }
}

@Data
class HitbtcTradesTick {
    List<HitbtcTrade> data;
    String symbol;
}

@Data
class HitbtcTrade {
    BigInteger id;
    BigDecimal price;
    BigDecimal quantity;
    String side;
    String timestamp;

    public Trade toTrade() {
        return new Trade()
            .setSno(id)
            .setTid(String.valueOf(id))
            .setDateTime(ZonedDateTime.parse(timestamp))
            .setPrice(price)
            .setQuantity(quantity)
            .setSide(OrderSide.of(side));
    }
}