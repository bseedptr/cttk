package cttk.impl.bibox;

import static cttk.CXIds.BIBOX;
import static cttk.dto.UserOrders.Category.ALL;
import static cttk.dto.UserOrders.Category.OPEN;
import static cttk.impl.bibox.BiboxUtils.getMarketSymbol;
import static cttk.impl.bibox.BiboxUtils.toAccountTypeCode;
import static cttk.impl.bibox.BiboxUtils.toFilterTypeCode;
import static cttk.impl.bibox.BiboxUtils.toOrderSideCode;
import static cttk.impl.bibox.BiboxUtils.toOrderTypeCode;
import static cttk.impl.bibox.BiboxUtils.toPayBixCode;
import static cttk.impl.util.Objects.nvl;
import static cttk.util.DeltaUtils.sleepWithException;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonProcessingException;

import cttk.AbstractCurrencyHolder;
import cttk.AbstractCurrencyPair;
import cttk.AccountType;
import cttk.CXClientFactory;
import cttk.CurrencyPair;
import cttk.CurrencyPair.SimpleCurrencyPair;
import cttk.PricingType;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.TradingFeeProvider;
import cttk.TransferTypeParam;
import cttk.auth.Credential;
import cttk.dto.Balances;
import cttk.dto.BankAccount;
import cttk.dto.MarketInfos;
import cttk.dto.Transfers;
import cttk.dto.UserAccount;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrades;
import cttk.dto.Wallet;
import cttk.dto.Wallets;
import cttk.dto.WithdrawResult;
import cttk.exception.CTTKException;
import cttk.exception.InsufficientBalanceException;
import cttk.exception.InvalidAmountMinException;
import cttk.exception.InvalidOrderException;
import cttk.exception.InvalidPriceMinException;
import cttk.exception.InvalidTotalMinException;
import cttk.exception.OrderNotFoundException;
import cttk.exception.UnsupportedCurrencyException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.bibox.response.BiboxUserAssetsResponse;
import cttk.impl.bibox.response.BiboxUserInfoResponse;
import cttk.impl.bibox.response.BiboxUserOrderHistoryResponse;
import cttk.impl.bibox.response.BiboxUserOrderPendingHistoryListResponse;
import cttk.impl.bibox.response.BiboxUserOrderPendingListResponse;
import cttk.impl.bibox.response.BiboxUserOrderResponse;
import cttk.impl.bibox.response.BiboxUserTradeResponse;
import cttk.impl.bibox.response.BiboxUserTransferListResponse;
import cttk.impl.util.CryptoUtils;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ParamsBuilder;
import cttk.impl.util.RandomUtils;
import cttk.request.CancelOrderRequest;
import cttk.request.CreateMarketPriceOrderRequest;
import cttk.request.CreateOrderRequest;
import cttk.request.GetBankAccountRequest;
import cttk.request.GetOpenUserOrdersRequest;
import cttk.request.GetTransfersDeltaRequest;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserAccountRequest;
import cttk.request.GetUserOrderHistoryDeltaRequest;
import cttk.request.GetUserOrderHistoryRequest;
import cttk.request.GetUserOrderRequest;
import cttk.request.GetUserTradesDeltaRequest;
import cttk.request.GetUserTradesRequest;
import cttk.request.GetWalletRequest;
import cttk.request.WithdrawToBankRequest;
import cttk.request.WithdrawToWalletRequest;
import lombok.AllArgsConstructor;
import lombok.Data;

public class BiboxPrivateClient
    extends AbstractBiboxClient
    implements PrivateCXClient
{
    private final Credential credential;
    private MarketInfos marketInfos = null;
    private String accountId;

    public BiboxPrivateClient(Credential credential) {
        this.credential = credential;
        try {
            final PublicCXClient pubClient = CXClientFactory.createPublicCXClient(this.getCxId());
            this.marketInfos = pubClient.getMarketInfos();
            this.accountId = getUserAccount(new GetUserAccountRequest()).getAid();
        } catch (CTTKException e) {
            this.accountId = CryptoUtils.getMD5String(credential.getAccessKey());
            logger.error("Failed to getMarketInfos while creating BiboxPrivateClient. UnsupporedCurrencyException for private data could not be working correctly");
        }
    }

    @Override
    public Balances getAllBalances()
        throws CTTKException
    {
        Map<String, String> params = ParamsBuilder.build("select", 1);

        final HttpResponse response = requestPostWithSign("transfer/assets", params);
        checkResponseError(null, response);
        return convert(response, BiboxUserAssetsResponse.class, BiboxUserAssetsResponse::toBalances);
    }

    @Override
    public UserOrder getUserOrder(GetUserOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        Map<String, String> params = ParamsBuilder.build("id", request.getOid());

        final HttpResponse response = requestPostWithSign("orderpending/order", params);

        try {
            checkResponseError(request, response);
        } catch (OrderNotFoundException e) {
            return null;
        }
        return convert(response, BiboxUserOrderResponse.class, BiboxUserOrderResponse::toUserOrder);
    }

    @Override
    public UserOrders getOpenUserOrders(GetOpenUserOrdersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        checkSupportedCurrencyPair(request);
        Map<String, String> params = ParamsBuilder.create()
            .putIfNotNull("coin_symbol", request.getBaseCurrency())
            .putIfNotNull("currency_symbol", request.getQuoteCurrency())
            .put("page", nvl(request.getPageNo(), 1))
            .put("size", nvl(request.getCount(), 10))
            .build();

        final HttpResponse response = requestPostWithSign("orderpending/orderPendingList", params);
        checkResponseError(request, response);
        return convert(response, BiboxUserOrderPendingListResponse.class, BiboxUserOrderPendingListResponse::toOpenUserOrders);
    }

    @Override
    public UserOrders getAllOpenUserOrders(boolean withTrades)
        throws CTTKException
    {
        final UserOrders userOrders = new UserOrders();

        for (int i = 1; true; i++) {
            final UserOrders tempUserOrders = getAllOpenUserOrdersWithPageNo(i);
            if (tempUserOrders == null || tempUserOrders.isEmpty()) {
                break;
            } else {
                userOrders.addAll(tempUserOrders);
            }

            try {
                Thread.sleep(200L);
            } catch (InterruptedException e) {
                logger.error("Unexpected error", e);
            }
        }

        return userOrders.setCategory(OPEN);
    }

    protected UserOrders getAllOpenUserOrdersWithPageNo(int pageNo)
        throws CTTKException
    {
        Map<String, String> params = ParamsBuilder.create()
            .put("page", pageNo)
            .put("size", 200)
            .build();
        final HttpResponse response = requestPostWithSign("orderpending/orderPendingList", params);
        checkResponseError(null, response);
        return convert(response, BiboxUserOrderPendingListResponse.class, BiboxUserOrderPendingListResponse::toOpenUserOrders);
    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        checkSupportedCurrencyPair(request);
        Map<String, String> params = ParamsBuilder.create()
            .putIfNotNull("coin_symbol", request.getBaseCurrency())
            .putIfNotNull("currency_symbol", request.getQuoteCurrency())
            .put("page", nvl(request.getPageNo(), 1))
            .put("size", nvl(request.getCount(), 10))
            .build();

        final HttpResponse response = requestPostWithSign("orderpending/pendingHistoryList", params);

        try {
            checkResponseError(request, response);
        } catch (OrderNotFoundException e) {
            return new UserOrders().setCategory(ALL);
        }

        final UserOrders userOrders = convert(response, BiboxUserOrderPendingHistoryListResponse.class, BiboxUserOrderPendingHistoryListResponse::toUserOrders);
        return userOrders.setCategory(ALL);
    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP p: page,<<<(exception)
        final GetUserOrderHistoryRequest partialRequest = new GetUserOrderHistoryRequest()
            .setCurrencyPair(request)
            .setCount(50);

        final UserOrders userOrders = new UserOrders();
        if (request.hasStartDateTime()) {
            int pageNo = 1;
            while (true) {
                partialRequest.setPageNo(pageNo++);
                final UserOrders tempUserOrders = getUserOrderHistory(partialRequest);

                if (tempUserOrders.isEmpty()) {
                    break;
                } else if (tempUserOrders.hasOlderOne(request.getStartDateTime())) {
                    tempUserOrders.filterByDateTimeGTE(request.getStartDateTime());
                    userOrders.addAll(tempUserOrders);
                    break;
                } else {
                    userOrders.addAll(tempUserOrders);
                }

                sleepWithException(request.getSleepTimeMillis(), BIBOX);
            }

            return userOrders.setCategory(ALL);
        } else {
            return getUserOrderHistory(partialRequest);
        }
    }

    @Override
    public UserOrder createOrder(CreateOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        Map<String, String> params = ParamsBuilder.create()
            .put("pair", getMarketSymbol(request))
            .put("account_type", toAccountTypeCode(AccountType.REGULAR))
            .put("order_type", toOrderTypeCode(PricingType.LIMIT))
            .put("order_side", toOrderSideCode(request.getSide()))
            .put("pay_bix", toPayBixCode(false))
            .put("price", request.getPrice())
            .put("amount", request.getQuantity())
            .build();

        final HttpResponse response = requestPostWithSign("orderpending/trade", params);
        checkOrderResponseError(request, response);
        checkResponseError(request, response);
        return fillResponseByRequest(request, convert(response, BiboxUserTradeResponse.class, BiboxUserTradeResponse::toUserOrder));
    }

    @Override
    public UserOrder createMarketPriceOrder(CreateMarketPriceOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public void cancelOrder(CancelOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        Map<String, String> params = ParamsBuilder.build("orders_id", request.getOrderId());

        final HttpResponse response = requestPostWithSign("orderpending/cancelTrade", params);
        checkResponseError(request, response);
    }

    @Override
    public Wallet getWallet(GetWalletRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        checkSupportedCurrencyPair(request);
        Map<String, String> params = ParamsBuilder.create()
            .putIfNotNull("coin_symbol", request.getBaseCurrency())
            .putIfNotNull("currency_symbol", request.getQuoteCurrency())
            .put("page", nvl(request.getPageNo(), 1))
            .put("size", nvl(request.getCount(), 10))
            .build();

        final HttpResponse response = requestPostWithSign("orderpending/orderHistoryList", params);
        checkResponseError(request, response);
        return convert(response, BiboxUserOrderHistoryResponse.class, BiboxUserOrderHistoryResponse::toUserTrades);
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP p: page,<<<(exception)
        final GetUserTradesRequest partialRequest = new GetUserTradesRequest()
            .setCurrencyPair(request)
            .setCount(50);

        final UserTrades userTrades = new UserTrades();
        if (request.hasStartDateTime()) {
            int pageNo = 1;
            while (true) {
                partialRequest.setPageNo(pageNo++);
                final UserTrades tempUserTrades = getUserTrades(partialRequest);

                if (tempUserTrades.isEmpty()) {
                    break;
                } else if (tempUserTrades.hasOlderOne(request.getStartDateTime())) {
                    tempUserTrades.filterByDateTimeGTE(request.getStartDateTime());
                    userTrades.addAll(tempUserTrades);
                    break;
                } else {
                    userTrades.addAll(tempUserTrades);
                }

                sleepWithException(request.getSleepTimeMillis(), BIBOX);
            }

            return userTrades;
        } else {
            return getUserTrades(partialRequest);
        }
    }

    @Override
    public Transfers getTransfers(GetTransfersRequest request)
        throws CTTKException
    {
        checkSupportedCurrency(request);

        final Transfers transfers = new Transfers();
        final Transfers withdraws = getWithdraws(request);
        final Transfers deposits = getDeposits(request);

        transfers.addTransfers(withdraws == null ? null : withdraws.getTransfers());
        transfers.addTransfers(deposits == null ? null : deposits.getTransfers());

        return transfers;
    }

    private Transfers getWithdraws(GetTransfersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        Map<String, String> params = ParamsBuilder.create()
            .putIfNotNull("coin_symbol", request.getCurrency())
            .put("filter_type", toFilterTypeCode(nvl(request.getTransferTypeParam(), TransferTypeParam.ALL)))
            .put("page", nvl(request.getPage(), 1))
            .put("size", nvl(request.getCount(), 10))
            .build();

        final HttpResponse response = requestPostWithSign("transfer/transferOutList", params);
        checkResponseError(null, response);
        return convert(response, BiboxUserTransferListResponse.class, i -> i.toUserTransfers(accountId));
    }

    private Transfers getDeposits(GetTransfersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        Map<String, String> params = ParamsBuilder.create()
            .putIfNotNull("coin_symbol", request.getCurrency())
            .put("filter_type", toFilterTypeCode(nvl(request.getTransferTypeParam(), TransferTypeParam.ALL)))
            .put("page", nvl(request.getPage(), 1))
            .put("size", nvl(request.getCount(), 10))
            .build();

        final HttpResponse response = requestPostWithSign("transfer/transferInList", params);
        checkResponseError(null, response);
        return convert(response, BiboxUserTransferListResponse.class, i -> i.toUserTransfers(accountId));
    }

    @Override
    public Transfers getTransfers(GetTransfersDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP p: page,<<<(exception)
        checkSupportedCurrency(request);

        if (request.hasStartDateTime()) {
            // Collect respectively and merge them
            final Transfers transfers = new Transfers();
            final Transfers withdraws = getTransfersDelta(request, true);
            final Transfers deposits = getTransfersDelta(request, false);

            transfers.addTransfers(withdraws == null ? null : withdraws.getTransfers());
            transfers.addTransfers(deposits == null ? null : deposits.getTransfers());

            return transfers;
        } else {
            return getTransfers(new GetTransfersRequest().setCurrency(request).setCount(50));
        }
    }

    private Transfers getTransfersDelta(GetTransfersDeltaRequest request, boolean isWithdraw)
        throws CTTKException
    {
        final GetTransfersRequest partialRequest = new GetTransfersRequest()
            .setCurrency(request)
            .setCount(50);

        final Transfers transfers = new Transfers();
        int pageNo = 1;
        while (true) {
            partialRequest.setPage(pageNo++);
            final Transfers tempTransfers = isWithdraw ? getWithdraws(partialRequest) : getDeposits(partialRequest);

            if (tempTransfers.isEmpty()) {
                break;
            } else if (tempTransfers.hasOlderOne(request.getStartDateTime())) {
                tempTransfers.filterByDateTimeGTE(request.getStartDateTime());
                transfers.addAll(tempTransfers);
                break;
            } else {
                transfers.addAll(tempTransfers);
            }

            sleepWithException(request.getSleepTimeMillis(), BIBOX);
        }
        return transfers;
    }

    @Override
    public WithdrawResult withdraw(WithdrawToWalletRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public UserAccount getUserAccount(GetUserAccountRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        Map<String, String> params = ParamsBuilder.buildEmpty();

        final HttpResponse response = requestPostWithSign("user/userInfo", params);
        checkResponseError(null, response);
        return convert(response, BiboxUserInfoResponse.class, BiboxUserInfoResponse::toUserAccount);
    }

    @Override
    public TradingFeeProvider getTradingFeeProvider()
        throws CTTKException
    {
        return new BiboxTradingFeeProvider();
    }

    @Override
    public BankAccount getBankAccount(GetBankAccountRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public WithdrawResult withdraw(WithdrawToBankRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public Wallets getAllWallets()
        throws CTTKException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    private HttpResponse requestPostWithSign(String cmd, Map<String, String> params)
        throws CTTKException
    {
        String endpoint = cmd.split("/")[0];
        String cmds = wrapJsonString(cmd, params);
        String apiKey = credential.getAccessKey();
        String sign = CryptoUtils.hmacMD5(cmds, credential.getSecretKey());

        return requestPost(endpoint,
            ParamsBuilder.create()
                .put("cmds", cmds)
                .put("index", RandomUtils.generateRandomInt())
                .put("apikey", apiKey)
                .put("sign", sign)
                .build());
    }

    private String wrapJsonString(String cmd, Map<String, String> body)
        throws CTTKException
    {
        try {
            List<Command> cmds = new ArrayList<>();
            cmds.add(new Command(cmd, body));
            return objectMapper.writeValueAsString(cmds);
        } catch (JsonProcessingException e) {
            throw CTTKException.create(e, getCxId());
        }
    }

    @Data
    @AllArgsConstructor
    private static class Command {
        String cmd;
        Map<String, String> body;
    }

    private void checkSupportedCurrencyPair(AbstractCurrencyPair<?> currencyPair)
        throws UnsupportedCurrencyPairException, CTTKException
    {
        if (marketInfos == null || currencyPair.isEitherIsNull())
            return;

        final List<CurrencyPair> allPairs = marketInfos
            .getMarketInfos()
            .stream()
            .map(CurrencyPair::toSimpleCurrencyPair)
            .collect(Collectors.toList());
        final SimpleCurrencyPair targetPair = new SimpleCurrencyPair(currencyPair.getBaseCurrency(), currencyPair.getQuoteCurrency());

        if (!allPairs.contains(targetPair)) {
            throw new UnsupportedCurrencyPairException("Not supported currency pair")
                .setCxId(this.getCxId())
                .setBaseCurrency(currencyPair.getBaseCurrency())
                .setQuoteCurrency(currencyPair.getQuoteCurrency());
        }
    }

    private void checkSupportedCurrency(AbstractCurrencyHolder<?> currencyHolder)
        throws CTTKException
    {
        if (currencyHolder.getCurrency() != null && marketInfos != null && !marketInfos.getAllCurrencies().contains(currencyHolder.getCurrency())) {
            throw new UnsupportedCurrencyException()
                .setCurrency(currencyHolder.getCurrency())
                .setCxId(this.getCxId());
        }
    }

    private void checkOrderResponseError(CreateOrderRequest request, HttpResponse response)
        throws CTTKException
    {
        final String body = response.getBody();
        try {
            @SuppressWarnings("unchecked")
            Map<String, Map<String, ?>> map = objectMapper.readValue(body, HashMap.class);
            Map<String, ?> error = map.get("error");
            if (error != null) {
                final String exchangeErrorCode = String.valueOf(error.get("code"));
                final String exchangeMessage = (String) error.get("msg");

                switch (exchangeErrorCode) {
                    case "2027":
                        throw new InsufficientBalanceException("2027 - Not enough balance")
                            .setCxId(this.getCxId())
                            .setResponseStatusCode(response.getStatus())
                            .setExchangeMessage(exchangeMessage)
                            .setOrderPrice(request.getPrice())
                            .setOrderQuantity(request.getQuantity())
                            .setBaseCurrency(request.getBaseCurrency())
                            .setQuoteCurrency(request.getQuoteCurrency())
                            .setExchangeErrorCode(exchangeErrorCode);
                    case "2068":
                        throwInvalidOrderException(new InvalidAmountMinException("2068 - Min amount value is 0.0001"), request, response, exchangeErrorCode, exchangeMessage);
                    case "2078":
                        throwInvalidOrderException(new InvalidPriceMinException("2078 - Min price value is 0.00000001"), request, response, exchangeErrorCode, exchangeMessage);
                    case "2085":
                        throwInvalidOrderException(new InvalidTotalMinException("2085 - the trade amount is low"), request, response, exchangeErrorCode, exchangeMessage);
                    case "3000": {
                        if (request.getQuantity().compareTo(BigDecimal.ZERO) <= 0) {
                            throwInvalidOrderException(new InvalidAmountMinException("2068 - Min amount value is 0.0001"), request, response, exchangeErrorCode, exchangeMessage);
                        }
                        if (request.getPrice().compareTo(BigDecimal.ZERO) <= 0) {
                            throwInvalidOrderException(new InvalidPriceMinException("2078 - Min price value is 0.00000001"), request, response, exchangeErrorCode, exchangeMessage);
                        }
                    }
                }
            }
        } catch (IOException e) {
            throw CTTKException.create("json parse error\n" + body, e, getCxId());
        }
    }

    private void throwInvalidOrderException(
        InvalidOrderException invalidOrderException,
        CreateOrderRequest request,
        HttpResponse response,
        String exchangeErrorCode,
        String exchangeMessage)
        throws InvalidOrderException
    {
        throw invalidOrderException.setCxId(this.getCxId())
            .setOrderPrice(request.getPrice())
            .setOrderQuantity(request.getQuantity())
            .setBaseCurrency(request.getBaseCurrency())
            .setQuoteCurrency(request.getQuoteCurrency())
            .setResponseStatusCode(response.getStatus())
            .setExchangeMessage(exchangeMessage)
            .setExchangeErrorCode(exchangeErrorCode);
    }

}
