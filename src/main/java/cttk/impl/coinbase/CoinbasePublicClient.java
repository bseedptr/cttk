package cttk.impl.coinbase;

import static cttk.impl.coinbase.CoinbaseUtils.getMarketSymbol;
import static cttk.impl.util.Objects.nvl;

import cttk.PublicCXClient;
import cttk.dto.MarketInfos;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Tickers;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.coinbase.response.CoinbaseProductOrderbookResponse;
import cttk.impl.coinbase.response.CoinbaseProductStatsResponse;
import cttk.impl.coinbase.response.CoinbaseProductTickerResponse;
import cttk.impl.coinbase.response.CoinbaseProductTradesResponse;
import cttk.impl.coinbase.response.CoinbaseProductsResponse;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ParamsBuilder;
import cttk.request.GetOrderBookRequest;
import cttk.request.GetRecentTradesRequest;
import cttk.request.GetTickerRequest;
import cttk.request.GetTradeHistoryRequest;

public class CoinbasePublicClient
    extends AbstractCoinbaseClient
    implements PublicCXClient
{
    public CoinbasePublicClient() {
        super();
    }

    @Override
    public MarketInfos getMarketInfos()
        throws CTTKException
    {
        final HttpResponse response = requestGet("products");

        checkResponseError(null, response);

        return convert(response, CoinbaseProductsResponse.class, i -> i.toMarketInfos());
    }

    @Override
    public Tickers getAllTickers()
        throws CTTKException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public Ticker getTicker(GetTickerRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response1 = requestGet("products/" + getMarketSymbol(request) + "/ticker");
        final HttpResponse response2 = requestGet("products/" + getMarketSymbol(request) + "/stats");
        checkResponseError(request, response1);
        checkResponseError(request, response2);
        final Ticker ticker = convert(response1, CoinbaseProductTickerResponse.class, i -> i.toTicker(request));

        return convert(response2, CoinbaseProductStatsResponse.class, i -> i.fillTicker(ticker));
    }

    @Override
    public OrderBook getOrderBook(GetOrderBookRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        checkOrderBookLevelParamValue(request.getPrecision());
        
        final HttpResponse response = requestGet("products/" + getMarketSymbol(request) + "/book",
            ParamsBuilder.buildIfNotNull("level", nvl(request.getPrecision(), 3)));
        checkResponseError(request, response);
        return convert(response, CoinbaseProductOrderbookResponse.class, i -> i.toOrderBook(request));
    }

    private void checkOrderBookLevelParamValue(Integer precision)
        throws CTTKException
    {
        if (precision != null
            && precision != 1
            && precision != 2
            && precision != 3)
        {
            throw new CTTKException("invalid level: " + precision).setCxId(this.getCxId());
        }
    }

    @Override
    public Trades getRecentTrades(GetRecentTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet("products/" + getMarketSymbol(request) + "/trades",
            ParamsBuilder.buildIfNotNull("before", getLastTradeIdNullIfZero(request)));

        checkResponseError(request, response);

        Trades trades = convert(response, CoinbaseProductTradesResponse.class, i -> i.toTrades(request));
        trades.setLast(getCbBefore(response));
        return trades;
    }

    @Override
    public Trades getTradeHistory(GetTradeHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    private static Long getLastTradeIdNullIfZero(GetRecentTradesRequest request) {
        Long val = request.getLastTradeId();
        return (val == null || val == 0l) ? null : val;
    }

    private static Long getCbBefore(HttpResponse response) {
        if (response == null) return null;
        String str = response.getHeaderValue("cb-before");
        if (str == null || str.trim().isEmpty()) return null;
        return Long.parseLong(str.trim());
    }
}
