package cttk.impl.okex.response;

import static cttk.impl.okex.OkexUtilsV3.toStdCurrencySymbolV3;
import static java.util.stream.Collectors.toList;

import java.math.BigDecimal;
import java.util.ArrayList;

import cttk.dto.MarketInfo;
import cttk.dto.MarketInfos;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class OkexMarketInfoResponse
    extends ArrayList<OkexMarketInfo>
{
    public MarketInfos toMarketInfos() {
        return new MarketInfos().setMarketInfos(stream().map(OkexMarketInfo::toMarketInfo).collect(toList()));
    }
}

@Data
class OkexMarketInfo {
    String base_currency;
    BigDecimal base_increment;
    BigDecimal base_min_size;
    String instrument_id;
    BigDecimal min_size; // size corresponds to quantity
    String product_id;
    String quote_currency;
    BigDecimal quote_increment;
    BigDecimal size_increment;
    BigDecimal tick_size;

    MarketInfo toMarketInfo() {
        return new MarketInfo()
            .setCurrencyPair(toStdCurrencySymbolV3(base_currency), toStdCurrencySymbolV3(quote_currency))
            .setQuantityIncrement(size_increment)
            .setMinQuantity(min_size)
            .setMinPrice(tick_size)
            .setPriceIncrement(tick_size);
    }
}
