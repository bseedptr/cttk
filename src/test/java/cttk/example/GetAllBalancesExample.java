package cttk.example;

import cttk.CXClientFactory;
import cttk.CXIds;
import cttk.PrivateCXClient;
import cttk.auth.CXCredentialsProvider;
import cttk.dto.Balances;

public class GetAllBalancesExample {
    public static void main(String[] args) {
        for (String cxId : new String[] {
            CXIds.BINANCE,
            CXIds.BITFINEX,
            CXIds.OKEX,
            CXIds.KRAKEN,
            CXIds.HITBTC,
            CXIds.POLONIEX,
            CXIds.HUOBI
        })
        {
            try {
                PrivateCXClient client = CXClientFactory.createPrivateCXClient(cxId, CXCredentialsProvider.getDefaultCredential(cxId).getDataKey());
                Balances balances = client.getAllBalances();

                Examples.print(cxId, balances);

            } catch (java.io.FileNotFoundException e) {
                continue;
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
        }
    }
}
