package cttk.impl.bitfinex.response;

import static cttk.impl.bitfinex.BitfinexUtils.toStdCurrencyPair;
import static cttk.impl.bitfinex.BitfinexUtils.toBigDecimal;

import java.util.ArrayList;

import cttk.CurrencyPair;
import cttk.dto.Ticker;
import cttk.impl.util.MathUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BitfinexTickerResponse
    extends ArrayList<Object>
{
    public Ticker toTicker(CurrencyPair currencyPair) {
        return new Ticker()
            .setCurrencyPair(toStdCurrencyPair(currencyPair))
            .setHighestBid(toBigDecimal(this.get(0)))
            .setLowestAsk(toBigDecimal(this.get(2)))
            .setChange(toBigDecimal(this.get(4)))
            .setChangePercent(MathUtils.multiply100(toBigDecimal(this.get(5))))
            .setLastPrice(toBigDecimal(this.get(6)))
            .setVolume(toBigDecimal(this.get(7)))
            .setHighPrice(toBigDecimal(this.get(8)))
            .setLowPrice(toBigDecimal(this.get(9)));
    }
}
