package cttk.impl.poloniex.response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.PricingType;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.impl.poloniex.PoloniexUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class PoloniexAllUserOrdersResponse
    extends HashMap<String, List<PoloniexUserOrder>>
{
    public UserOrders toUserOrders(PricingType pricingType) {
        return new UserOrders()
            .setOrders(getUserOrderList(pricingType));
    }

    List<UserOrder> getUserOrderList(PricingType pricingType) {
        final List<UserOrder> orderList = new ArrayList<>();
        this.forEach((k, v) -> {
            if (v != null && !v.isEmpty()) {
                final CurrencyPair cp = PoloniexUtils.parseMarketSymbol(k);
                orderList.addAll(v.stream().map(i -> i.toUserOrder(cp, pricingType)).filter(i -> i != null).collect(Collectors.toList()));
            }
        });
        return orderList;
    }
}
