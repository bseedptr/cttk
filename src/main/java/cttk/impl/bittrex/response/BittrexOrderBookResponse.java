package cttk.impl.bittrex.response;

import java.util.List;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.dto.OrderBook;
import cttk.dto.OrderBook.SimpleOrder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BittrexOrderBookResponse
    extends BittrexResponse<BittrexOrderBook>
{
    public OrderBook toOrderBook(CurrencyPair currencyPair) {
        return new OrderBook()
            .setCurrencyPair(currencyPair)
            .setAsks(toSimpleOrderList(result.sell))
            .setBids(toSimpleOrderList(result.buy));
    }

    private static List<SimpleOrder> toSimpleOrderList(List<BittrexOrder> list) {
        if (list == null) return null;
        return list.stream()
            .map(i -> i.toSimpleOrder())
            .collect(Collectors.toList());
    }
}
