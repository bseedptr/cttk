package cttk.impl.huobi;

import java.io.IOException;
import java.util.Map;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXServerException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.AbstractCXClient;
import cttk.impl.util.HttpHeaders;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.MediaType;
import cttk.impl.util.ThrowableFunction;
import cttk.impl.util.UriUtils;
import okhttp3.Request;
import okhttp3.Response;

abstract class AbstractHuobiClient
    extends AbstractCXClient
{
    protected final String HOST_URL = "https://api.huobi.pro";

    protected static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36";

    public AbstractHuobiClient() {
        super();
    }

    @Override
    public String getCxId() {
        return CXIds.HUOBI;
    }

    @Override
    protected void checkErrorByResponseMessage(Object requestObj, HttpResponse response)
        throws CTTKException
    {
        HuobiUtils.checkErrorByResponseMessage(requestObj, response.getBody(), response.getStatus());
    }

    protected HttpResponse requestGet(String endPoint, Map<String, String> params)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final Request request = new Request.Builder()
            .url(HOST_URL + endPoint + (params == null || params.isEmpty() ? "" : "?" + UriUtils.toEncodedQueryParamsString(params)))
            .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED)
            .addHeader(HttpHeaders.USER_AGENT, USER_AGENT)
            .get()
            .build();

        debug(request);

        try (Response response = super.httpClient.newCall(request).execute()) {
            return toHttpResponse(response);
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    @Override
    protected <T, R> R convert(HttpResponse response, Class<T> claz, ThrowableFunction<T, R> mapper)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final String body = response.getBody();
        try {
            final T obj = objectMapper.readValue(body, claz);

            return mapper.apply(obj);
        } catch (IOException e) {
            throw new CXAPIRequestException("json parse error\n", e)
                .setCxId(this.getCxId())
                .setResponseStatusCode(response.getStatus())
                .setExchangeMessage(body);
        }

    }
}
