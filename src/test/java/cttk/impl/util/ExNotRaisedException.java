package cttk.impl.util;

import cttk.exception.CTTKException;

public class ExNotRaisedException
    extends CTTKException
{
    private static final long serialVersionUID = 1L;

    public ExNotRaisedException() {
        super();
    }

    public ExNotRaisedException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExNotRaisedException(String message) {
        super(message);
    }

    public ExNotRaisedException(Throwable cause) {
        super(cause);
    }

    @Override
    public ExNotRaisedException setCxId(String cxId) {
        super.setCxId(cxId);
        return this;
    }
}
