package cttk.impl.util;

import java.util.List;
import java.util.Map;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class HttpResponse {
    int status;
    String body;
    Map<String, List<String>> headers;

    public boolean is2xxSuccessful() {
        return status >= 200 && status < 300;
    }

    public boolean is4xx() {
        return status >= 400 && status < 500;
    }

    public List<String> getHeaderValues(String name) {
        if (headers == null || !headers.containsKey(name)) return null;
        return headers.get(name);
    }

    public String getHeaderValue(String name) {
        if (headers == null || !headers.containsKey(name)) return null;
        List<String> values = headers.get(name);
        if (values.isEmpty()) return null;
        return values.get(0);
    }
}
