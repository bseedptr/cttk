package cttk.impl.korbit.response;

import static cttk.impl.util.DateTimeUtils.zdtFromEpochMilli;

import java.util.List;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.dto.OrderBook;
import cttk.impl.util.ZoneIds;
import lombok.Data;

@Data
public class KorbitOrderBookResponse {
    String timestamp;
    List<List<String>> bids;
    List<List<String>> asks;

    public OrderBook toOrderBook(CurrencyPair currencyPair) {
        return new OrderBook()
            .setCurrencyPair(currencyPair)
            .setDateTime(zdtFromEpochMilli(Long.parseLong(timestamp), ZoneIds.SEOUL))
            .setAsks(toOrderList(asks))
            .setBids(toOrderList(bids));
    }

    private List<OrderBook.SimpleOrder> toOrderList(List<List<String>> korbitOrders) {
        if (korbitOrders == null) return null;
        return korbitOrders.stream().map(k -> toOrder(k)).collect(Collectors.toList());
    }

    private OrderBook.SimpleOrder toOrder(List<String> korbitOrder) {
        String price = korbitOrder.get(0);
        String quantity = korbitOrder.get(1);

        return OrderBook.SimpleOrder.of(price, quantity);
    }
}