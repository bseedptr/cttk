package cttk.request;

import java.time.ZonedDateTime;

import cttk.AbstractCurrencyHolder;
import cttk.DurationGettable;
import cttk.ScanDirection;
import cttk.SortType;
import cttk.TransferType;
import cttk.TransferTypeParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class GetTransfersRequest
    extends AbstractCurrencyHolder<GetTransfersRequest>
    implements DurationGettable
{
    // binance, korbit, huobi
    TransferType type;

    // btithumb, hitbtc(=limit), huobi(=size), korbit(=limit), bibox(=size)
    Integer count = 40;

    // bitfinex
    String method; // The method of the deposit/withdrawal (can be “bitcoin”, “litecoin”, “darkcoin”, “wire”).

    // bithumb, korbit, kraken(=ofs)
    Integer offset = 0;
    // bibox
    Integer page = 1;

    SortType sort;
    String by;
    String status;

    // huobi
    String fromId;
    String tillId;
    ScanDirection scanDirection;

    // upbit
    // submitting : 처리 중
    // submitted : 처리 완료
    // almost_accepted : 출금대기중
    // rejected : 거부
    // accepted : 승인됨
    // processing : 처리 중
    // done : 완료
    // canceled : 취소됨
    String state;
    TransferTypeParam transferTypeParam;

    // kraken
    private ZonedDateTime startDateTime;
    private ZonedDateTime endDateTime;

    // bibox
    long withdrawId;

    public boolean isScanDirectionPrev() {
        return scanDirection == ScanDirection.PREV;
    }
}