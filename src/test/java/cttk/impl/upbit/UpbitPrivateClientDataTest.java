package cttk.impl.upbit;

import static cttk.dto.UserOrders.Category.CLOSED;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isOneOf;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.time.ZonedDateTime;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import cttk.CXIds;
import cttk.OrderStatus;
import cttk.SortType;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.AbstractPrivateCXClientDataTest;
import cttk.request.GetUserOrderHistoryDeltaRequest;
import cttk.request.GetUserOrderHistoryRequest;

public class UpbitPrivateClientDataTest
    extends AbstractPrivateCXClientDataTest
{
    @AfterClass
    public static void cleanup() {
        cleanup(UpbitPrivateClientDataTest.class);
    }

    @Override
    public String getCxId() {
        return CXIds.UPBIT;
    }

    @Test
    public void testGetAllOpenUserOrders()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetAllOpenUserOrders();
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetAllWallets()
        throws UnsupportedCurrencyPairException, CTTKException
    {
        super.testGetAllWallets();
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetUserAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetUserAccount();
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetBankAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetBankAccount();
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetWallet()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetWallet();
    }

    @Test
    public void testGetUserOrderHistoryCategory()
        throws CTTKException
    {
        threadSleep();

        final GetUserOrderHistoryRequest request = new GetUserOrderHistoryRequest()
            .setCurrencyPair(getValidCurrencyPair());
        final UserOrders userOrders = client.getUserOrderHistory(request);

        logger.debug("{}", userOrders);

        Assert.assertEquals(userOrders.getCategory(), CLOSED);
    }

    @Test
    public void testGetUserOrderHistoryByOrderStatus()
        throws CTTKException
    {
        GetUserOrderHistoryRequest request = new GetUserOrderHistoryRequest()
            .setCurrencyPair(getValidCurrencyPair())
            .setStartDateTime(ZonedDateTime.now().minusDays(14))
            .setEndDateTime(ZonedDateTime.now());

        assertUserOrders(client.getUserOrderHistory(request), request.setStatus(OrderStatus.FILLED));
        assertUserOrders(client.getUserOrderHistory(request.setStatus(OrderStatus.FILLED)), request);
        assertUserOrders(client.getUserOrderHistory(request.setStatus(OrderStatus.CANCELED).setSort(SortType.DESC)), request);
    }

    private void assertUserOrders(UserOrders userOrders, GetUserOrderHistoryRequest request) {
        assertThat(userOrders.isNotEmpty(), is(true));
        ZonedDateTime beforeOrderDateTime = userOrders.get(0).getDateTime();
        for (UserOrder userOrder : userOrders.getList()) {
            assertUserOrder(userOrder);
            assertThat(userOrder.getPairString(), is(request.getPairString()));
            assertThat(userOrder.getStatusType(), is(request.getStatus()));
            assertThat(userOrder.getDateTime(), request.getSort() == SortType.DESC ? lessThanOrEqualTo(beforeOrderDateTime) : greaterThanOrEqualTo(beforeOrderDateTime));
            beforeOrderDateTime = userOrder.getDateTime();
        }
    }

    @Test
    public void testGetUserOrderHistoryDeltaRequest()
        throws CTTKException
    {
        GetUserOrderHistoryDeltaRequest request = new GetUserOrderHistoryDeltaRequest()
            .setCurrencyPair(getValidCurrencyPair())
            .setStartDateTime(ZonedDateTime.now().minusDays(14))
            .setEndDateTime(ZonedDateTime.now());

        UserOrders userOrders = client.getUserOrderHistory(request);

        assertThat(userOrders.isNotEmpty(), is(true));
        userOrders.getList().forEach(userOrder -> {
            assertThat(userOrder.getStatusType(), isOneOf(OrderStatus.FILLED, OrderStatus.CANCELED));
            assertThat(userOrder.getDateTime(), greaterThanOrEqualTo(request.getStartDateTime()));
            assertThat(userOrder.getPairString(), is(request.getPairString()));
            assertUserOrder(userOrder);
        });
    }

    private void assertUserOrder(UserOrder userOrder) {
        assertThat(userOrder.getOid(), notNullValue());
        assertThat(userOrder.getSide(), notNullValue());
        assertThat(userOrder.getPricingType(), notNullValue());
        assertThat(userOrder.getPrice(), notNullValue());
        assertThat(userOrder.getQuantity(), notNullValue());
        assertThat(userOrder.getTotal(), notNullValue());
    }
}
