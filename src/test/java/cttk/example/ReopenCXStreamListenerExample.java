package cttk.example;

import cttk.CXClientFactory;
import cttk.CXIds;
import cttk.CXStreamListener;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCXException;
import cttk.request.OpenTradeStreamRequest;
import cttk.util.ReopenPolicy;

public class ReopenCXStreamListenerExample {
    public static void main(String[] args)
        throws UnsupportedCXException, CTTKException, InterruptedException
    {
        //        CXStreams.openOrderBookSnapshotStream(
        //            CXIds.BITHUMB,
        //            new OpenOrderBookStreamRequest().setCurrencyPair("ETH", "KRW"),
        //            CXStreamListener.createPrint(),
        //            ReopenPolicy.createDefault());

        CXClientFactory
            .createPollingCXStreamOpener(CXIds.BITHUMB)
            .openTradeStream(
                new OpenTradeStreamRequest().setCurrencyPair("ETH", "BTC"),
                CXStreamListener.createPrint(),
                ReopenPolicy.createDefault());

        Thread.sleep(60000);
    }
}
