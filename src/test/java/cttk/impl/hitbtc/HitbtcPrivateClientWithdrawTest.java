package cttk.impl.hitbtc;

import org.junit.Test;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.AbstractPrivateCXClientWithdrawTest;

public class HitbtcPrivateClientWithdrawTest
    extends AbstractPrivateCXClientWithdrawTest
{
    @Override
    public String getCxId() {
        return CXIds.HITBTC;
    }

    // FIXME
    @Test
    @Override
    public void testWithdrawToBankAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
    }

    // FIXME
    @Test
    @Override
    public void testWithdrawBTCToWallet()
        throws CTTKException, UnsupportedCurrencyPairException
    {
    }
}
