package cttk.impl.okex;

import cttk.CXIds;
import cttk.impl.AbstractOrderVerifierTest;

public class OkexOrderVerifierTest
    extends AbstractOrderVerifierTest
{
    @Override
    public String getCxId() {
        return CXIds.OKEX;
    }
}
