package cttk.impl.hitbtc.stream.response;

import static cttk.impl.hitbtc.HitbtcUtils.toStdCurrencyPair;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import cttk.CurrencyPair;
import cttk.dto.Ticker;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HitbtcWebsocketTickerResponse
    extends HitbtcWebsocketResponse<HitbtcTickerTick>
{
    public Ticker toTicker(CurrencyPair currencyPair) {
        return new Ticker()
            .setCurrencyPair(toStdCurrencyPair(currencyPair))
            .setDateTime(ZonedDateTime.parse(params.timestamp))
            .setHighestBid(params.bid)
            .setLowestAsk(params.ask)
            .setOpenPrice(params.open)
            .setHighPrice(params.high)
            .setLowPrice(params.low)
            .setLastPrice(params.last)
            .setVolume(params.volume)
            .setQuoteVolume(params.volumeQuote);
    }
}

@Data
class HitbtcTickerTick {
    BigDecimal ask;
    BigDecimal bid;
    BigDecimal last;
    BigDecimal open;
    BigDecimal low;
    BigDecimal high;
    BigDecimal volume;
    BigDecimal volumeQuote;
    String timestamp;
    String symbol;
}