package cttk.impl.kraken;

import cttk.CXIds;
import cttk.impl.AbstractUserDataStreamTest;

public class KrakenUserDataStreamTest
    extends AbstractUserDataStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.KRAKEN;
    }

    @Override
    public void testUserDataStream()
        throws Exception
    {
        // available, inUse not support
        // TODO test kraken user data stream
    }
}
