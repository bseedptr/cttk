package cttk.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cttk.CXStream;
import cttk.CXStreamListener;

public abstract class CXStreamPipe<T>
    implements CXStreamListener<T>
{
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    protected CXStreamListener<T> successor;

    public CXStreamPipe(CXStreamListener<T> successor) {
        super();
        this.successor = successor;
    }

    @Override
    public void onOpen(CXStream<T> stream) {
        if (successor != null) successor.onOpen(stream);
    }

    @Override
    public void onMessage(T message) {
        try {
            T processed = handleMessage(message);
            if (successor != null && processed != null) successor.onMessage(processed);
        } catch (Exception e) {
            logger.error("Message processing error.\n{}", message, e);
        }
    }

    @Override
    public void onClosing(CXStream<T> stream, int code, String reason) {
        if (successor != null) successor.onClosing(stream, code, reason);
    }

    @Override
    public void onClosed(CXStream<T> stream, int code, String reason) {
        if (successor != null) successor.onClosed(stream, code, reason);
    }

    @Override
    public void onFailure(CXStream<T> stream, Throwable t) {
        if (successor != null) successor.onFailure(stream, t);
    }

    public T handleMessage(T message)
        throws Exception
    {
        return message;
    }
}
