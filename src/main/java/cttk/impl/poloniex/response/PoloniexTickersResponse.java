package cttk.impl.poloniex.response;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import cttk.CurrencyPair;
import cttk.dto.MarketInfo;
import cttk.dto.MarketInfos;
import cttk.dto.Ticker;
import cttk.dto.Tickers;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.poloniex.PoloniexUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;

@Data
public class PoloniexTickersResponse {
    private Map<String, PoloniexTicker> map = new HashMap<>();

    @JsonAnyGetter
    public Map<String, PoloniexTicker> any() {
        return this.map;
    }

    @JsonAnySetter
    public void set(String name, PoloniexTicker value) {
        this.map.put(name, value);
    }

    public Ticker toTicker(CurrencyPair currency)
        throws CTTKException
    {
        String cpSymbol = PoloniexUtils.getMarketSymbol(currency);
        if (!this.map.containsKey(cpSymbol)) {
            throw new UnsupportedCurrencyPairException()
                .setCurrencyPair(currency);
        }

        return this.map.get(cpSymbol).toTicker(currency);
    }

    public Tickers toTickers() {
        final List<Ticker> list = map.entrySet().stream()
            .map(e -> e.getValue().toTicker(PoloniexUtils.parseMarketSymbol(e.getKey())))
            .collect(Collectors.toList());

        return new Tickers().setTickers(list);
    }

    public MarketInfos toMarketInfos() {
        if (map == null) return null;
        return new MarketInfos()
            .setMarketInfos(map.keySet().stream()
                .map(i -> new MarketInfo().setCurrencyPair(PoloniexUtils.parseMarketSymbol(i)))
                .collect(Collectors.toList()));
    }
}

@Data
class PoloniexTicker {
    Integer id;
    BigDecimal last;
    BigDecimal lowestAsk;
    BigDecimal highestBid;
    BigDecimal percentChange;
    BigDecimal baseVolume;
    BigDecimal quoteVolume;
    String isFrozen;
    BigDecimal high24hr;
    BigDecimal low24hr;

    Ticker toTicker(CurrencyPair currencyPair) {
        return new Ticker()
            .setCurrencyPair(currencyPair)
            .setId(String.valueOf(id))
            .setLastPrice(last)
            .setLowPrice(low24hr)
            .setHighPrice(high24hr)
            .setVolume(quoteVolume)
            .setQuoteVolume(baseVolume)
            .setHighestBid(highestBid)
            .setLowestAsk(lowestAsk)
            .setChangePercent(MathUtils.multiply(percentChange, new BigDecimal(100)))
            .setIsFrozen("1".equals(isFrozen));
    }
}