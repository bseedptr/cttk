package cttk.impl.coinone;

import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.auth.Credential;
import cttk.impl.AbstractEMPUDSOpenerCXClientFactory;
import cttk.impl.RetryPrivateCXClient;

public class CoinoneClientFactory
    extends AbstractEMPUDSOpenerCXClientFactory
{
    @Override
    public PublicCXClient create() {
        return new CoinonePublicClient();
    }

    @Override
    public PrivateCXClient create(Credential credential) {
        return new RetryPrivateCXClient(5, 5, new CoinonePrivateClient(credential), "Nonce is must be bigger then last nonce");
    }
}
