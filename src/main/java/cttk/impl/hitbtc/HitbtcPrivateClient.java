package cttk.impl.hitbtc;

import static cttk.CXIds.HITBTC;
import static cttk.dto.UserOrders.Category.ALL;
import static cttk.dto.UserOrders.Category.OPEN;
import static cttk.impl.hitbtc.HitbtcUtils.getMarketSymbol;
import static cttk.impl.hitbtc.HitbtcUtils.toHitbtcCurrencySymbol;
import static cttk.impl.util.Objects.nvl;
import static cttk.util.DeltaUtils.sleep;
import static cttk.util.DeltaUtils.sleepWithException;
import static cttk.util.StringUtils.toUpper;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonProcessingException;

import cttk.CurrencyPair;
import cttk.PrivateCXClient;
import cttk.SortType;
import cttk.TradingFeeProvider;
import cttk.auth.Credential;
import cttk.dto.Balances;
import cttk.dto.BankAccount;
import cttk.dto.TradingFee;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.dto.UserAccount;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;
import cttk.dto.Wallet;
import cttk.dto.Wallets;
import cttk.dto.WithdrawResult;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXServerException;
import cttk.exception.InsufficientBalanceException;
import cttk.exception.InvalidAmountRangeException;
import cttk.exception.InvalidPriceRangeException;
import cttk.exception.OrderNotFoundException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.feeprovider.CachedTradingFeeProvider;
import cttk.impl.hitbtc.response.HitbtcBalancesResponse;
import cttk.impl.hitbtc.response.HitbtcTradingFeeResponse;
import cttk.impl.hitbtc.response.HitbtcTransferResponse;
import cttk.impl.hitbtc.response.HitbtcUserOrderResponse;
import cttk.impl.hitbtc.response.HitbtcUserTradeResponse;
import cttk.impl.hitbtc.response.HitbtcWithdrawResponse;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.MathUtils;
import cttk.impl.util.ParamsBuilder;
import cttk.request.CancelOrderRequest;
import cttk.request.CreateMarketPriceOrderRequest;
import cttk.request.CreateOrderRequest;
import cttk.request.GetBankAccountRequest;
import cttk.request.GetOpenUserOrdersRequest;
import cttk.request.GetTransfersDeltaRequest;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserAccountRequest;
import cttk.request.GetUserOrderHistoryDeltaRequest;
import cttk.request.GetUserOrderHistoryRequest;
import cttk.request.GetUserOrderRequest;
import cttk.request.GetUserTradesDeltaRequest;
import cttk.request.GetUserTradesRequest;
import cttk.request.GetWalletRequest;
import cttk.request.WithdrawToBankRequest;
import cttk.request.WithdrawToWalletRequest;
import cttk.util.DeltaUtils;
import cttk.util.StringUtils;
import okhttp3.Credentials;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HitbtcPrivateClient
    extends AbstractHitbtcClient
    implements PrivateCXClient
{
    protected final Credential credential;

    public HitbtcPrivateClient(Credential credential) {
        super();
        this.credential = credential;
    }

    @Override
    public UserAccount getUserAccount(GetUserAccountRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public TradingFeeProvider getTradingFeeProvider()
        throws CTTKException
    {
        return new CachedTradingFeeProvider(new TradingFeeProvider() {
            @Override
            public TradingFee getFee(CurrencyPair currencyPair)
                throws CTTKException
            {
                HttpResponse response = requestGet("/trading/fee/" + getMarketSymbol(currencyPair));
                checkResponseError(currencyPair, response);
                return convert(response, HitbtcTradingFeeResponse.class, e -> e.getTradingFee());

            }
        });
    }

    @Override
    public BankAccount getBankAccount(GetBankAccountRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public WithdrawResult withdraw(WithdrawToBankRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public Wallet getWallet(GetWalletRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public Wallets getAllWallets()
        throws CTTKException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public WithdrawResult withdraw(WithdrawToWalletRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("/account/crypto/withdraw",
            ParamsBuilder.create()
                .putIfNotNull("currency", toHitbtcCurrencySymbol(request.getCurrency()))
                .putIfNotNull("amount", String.valueOf(request.getQuantity()))
                .putIfNotNull("address", request.getAddress())
                .putIfNotNull("paymentId", request.getPaymentId())
                .build());

        checkResponseError(request, response);

        return convert(response, HitbtcWithdrawResponse.class, e -> new WithdrawResult(e.getId()));
    }

    @Override
    public Balances getAllBalances()
        throws CTTKException
    {
        final HttpResponse response = requestGet("/trading/balance");

        checkResponseError(null, response);

        try {
            return convertList(response, HitbtcBalancesResponse.class,
                e -> new Balances().setBalances(e.stream()
                    .map(HitbtcBalancesResponse::toBalance)
                    .filter(i -> i.getTotal().compareTo(BigDecimal.ZERO) > 0)
                    .collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException(e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    /**
     * Canceled order cannot be checked
     * @param request
     * @return
     * @throws CTTKException
     * @throws UnsupportedCurrencyPairException
     */
    @Override
    public UserOrder getUserOrder(GetUserOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        if (StringUtils.isEmpty(request.getClientOrderId())) {
            throw new CTTKException("clientOrderId should be provided").setCxId(this.getCxId());
        }
        final HttpResponse response = requestGet("/order/" + request.getClientOrderId());

        try {
            checkResponseError(request, response);
        } catch (OrderNotFoundException e) {
            return null;
        }

        final UserOrder userOrder = convert(response, HitbtcUserOrderResponse.class, i -> i.toUserOrder());
        if (request.isWithTrades()) {
            userOrder.setTrades(getOrderTrades(userOrder.getOid()));
        }

        return userOrder;
    }

    public List<UserTrade> getOrderTrades(final String serverOrderId)
        throws CTTKException
    {
        final HttpResponse response = requestGet("/history/order/" + serverOrderId + "/trades");

        checkResponseError(null, response);

        try {
            return convertList(response, HitbtcUserTradeResponse.class,
                e -> e.stream().map(HitbtcUserTradeResponse::toUserTrade).collect(Collectors.toList()));
        } catch (IOException e) {
            throw new CXAPIRequestException(e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public UserOrders getOpenUserOrders(GetOpenUserOrdersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet("/order",
            ParamsBuilder.buildIfNotNull("symbol", getMarketSymbol(request)));

        checkResponseError(request, response);

        try {
            return convertList(response, HitbtcUserOrderResponse.class,
                e -> new UserOrders().setOrders(e.stream()
                    .map(i -> i.toUserOrder())
                    .filter(i -> i != null)
                    .collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException(e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public UserOrders getAllOpenUserOrders(boolean withTrades)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet("/order");

        checkResponseError(null, response);

        try {
            return convertList(response, HitbtcUserOrderResponse.class,
                e -> new UserOrders().setOrders(e.stream()
                    .map(i -> i.toUserOrder())
                    .filter(i -> i != null)
                    .collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException(e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet("/history/order",
            ParamsBuilder.create()
                .putIfNotNull("symbol", getMarketSymbol(request))
                // TODO check from and till
                .putIfNotNull("from", request.getStartEpochInMilli())
                .putIfNotNull("till", request.getEndEpochInMilli())
                .putIfNotNull("limit", request.getCount())
                .putIfNotNull("offset", request.getOffset())
                .build());

        try {
            checkResponseError(request, response);
        } catch (OrderNotFoundException e) {
            return new UserOrders().setCategory(OPEN);
        }

        UserOrders userOrders;
        try {
            userOrders = convertList(response, HitbtcUserOrderResponse.class,
                e -> new UserOrders().setOrders(e.stream()
                    .map(i -> i.toUserOrder())
                    .filter(i -> i != null)
                    .collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException(e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
        return userOrders.setCategory(ALL);
    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP t-d: time-desc,<<<(exception)
        final GetUserOrderHistoryRequest partialRequest = new GetUserOrderHistoryRequest()
            .setCurrencyPair(request)
            .setStartDateTime(request.getStartDateTime())
            .setEndDateTime(nvl(request.getEndDateTime(), ZonedDateTime.now()))
            .setCount(100);

        if (request.hasStartDateTime()) {
            final Map<String, UserOrder> map = new LinkedHashMap<>();
            UserOrder prevFirst = null;
            UserOrder prevLast = null;
            while (true) {
                final UserOrders userOrders = getUserOrderHistory(partialRequest);
                final UserOrder first = userOrders.getOldest();
                final UserOrder last = userOrders.getLatest();

                if (userOrders.isEmpty() || UserOrder.equalsById(prevFirst, first)) break;
                map.putAll(DeltaUtils.convertToMap(userOrders));

                final int s = DeltaUtils.calcNextSeconds(prevLast, last);
                partialRequest.setEndDateTime(first.getDateTime().plusSeconds(s));
                prevFirst = first;
                prevLast = last;

                sleepWithException(request.getSleepTimeMillis(), HITBTC);
            }

            return DeltaUtils.convertUserOrdersFromMap(map).setCategory(ALL);
        } else {
            return getUserOrderHistory(partialRequest);
        }
    }

    @Override
    public UserOrder createOrder(CreateOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPost("/order",
            ParamsBuilder.create()
                .putIfNotNull("symbol", getMarketSymbol(request))
                .putIfNotNull("side", HitbtcUtils.toString(request.getSide()))
                .putIfNotNull("quantity", request.getQuantity())
                .putIfNotNull("price", request.getPrice())
                .build());

        try {
            @SuppressWarnings("unchecked")
            Map<String, Map<String, ?>> map = objectMapper.readValue(response.getBody(), HashMap.class);
            Map<String, ?> error = map.get("error");
            if (error != null) {
                final String exchangeErrorCode = String.valueOf(error.get("code"));
                final String exchangeMessage = (String) error.get("message");
                final String message = (String) error.get("description");
                final Integer statusCode = response.getStatus();
                switch (exchangeErrorCode) {
                    case "2010":
                        throw new InvalidAmountRangeException(exchangeMessage)
                            .setCxId(this.getCxId())
                            .setExchangeMessage(exchangeMessage)
                            .setResponseStatusCode(statusCode)
                            .setBaseCurrency(request.getBaseCurrency())
                            .setQuoteCurrency(request.getQuoteCurrency())
                            .setOrderPrice(request.getPrice())
                            .setOrderQuantity(request.getQuantity())
                            .setExchangeErrorCode(exchangeErrorCode);
                    case "2020":
                        throw new InvalidPriceRangeException(exchangeMessage)
                            .setCxId(this.getCxId())
                            .setExchangeMessage(exchangeMessage)
                            .setResponseStatusCode(statusCode)
                            .setBaseCurrency(request.getBaseCurrency())
                            .setQuoteCurrency(request.getQuoteCurrency())
                            .setOrderPrice(request.getPrice())
                            .setOrderQuantity(request.getQuantity())
                            .setExchangeErrorCode(exchangeErrorCode);
                    case "20001":
                        throw new InsufficientBalanceException(message)
                            .setCxId(this.getCxId())
                            .setExchangeMessage(exchangeMessage)
                            .setResponseStatusCode(statusCode)
                            .setBaseCurrency(request.getBaseCurrency())
                            .setQuoteCurrency(request.getQuoteCurrency())
                            .setOrderPrice(request.getPrice())
                            .setOrderQuantity(request.getQuantity())
                            .setExchangeErrorCode(exchangeErrorCode);
                }
            }

        } catch (IOException e) {
            throw CTTKException.create(e, getCxId());
        }

        checkResponseError(request, response);

        return fillResponseByRequest(request, convert(response, HitbtcUserOrderResponse.class, e -> e.toUserOrder()));
    }

    @Override
    public void cancelOrder(CancelOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final String endpoint = "/order/" + request.getClientOrderId();

        final HttpResponse response = requestDelete(endpoint);

        checkResponseError(null, response);

    }

    @Override
    public UserOrder createMarketPriceOrder(CreateMarketPriceOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet("/history/trades",
            ParamsBuilder.create()
                .putIfNotNull("symbol", HitbtcUtils.getMarketSymbol(request))
                .putIfNotNull("sort", HitbtcUtils.toHitbtcSortType(request.getSort()))
                .putIfNotNull("offset", request.getOffset())
                .putIfNotNull("limit", request.getCount())
                .putIfNotNull("by", request.getBy())
                .putIfNotNull("from", request.getStartEpochInSecond())
                .putIfNotNull("till", request.getEndEpochInSecond())
                .putIfNotNull("from", request.getFromId())
                .putIfNotNull("till", request.getTillId())
                .build());

        checkResponseError(request, response);

        try {
            return convertList(response, HitbtcUserTradeResponse.class,
                e -> new UserTrades().setTrades(e.stream().map(HitbtcUserTradeResponse::toUserTrade).collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException(e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP t-a: time-asc,>>>(break)
        // GROUP i-a: id-asc,>>>(break)
        final GetUserTradesRequest partialRequest = new GetUserTradesRequest()
            .setCurrencyPair(request)
            .setCount(100)
            .setSort(SortType.ASC); // Need to set asc explicitly

        if (request.hasStartDateTime()) {
            partialRequest.setBy("timestamp")
                .setStartDateTime(request.getStartDateTime())
                .setEndDateTime(nvl(request.getEndDateTime(), ZonedDateTime.now()));
            final Map<String, UserTrade> map = new LinkedHashMap<>();
            UserTrade prevLast = null;
            while (true) {
                final UserTrades userTrades = getUserTrades(partialRequest);
                final UserTrade last = userTrades.getLatest();

                if (userTrades.isEmpty() || UserTrade.equalsById(prevLast, last)) break;
                map.putAll(DeltaUtils.convertToMap(userTrades));

                partialRequest.setStartDateTime(userTrades.getMaxDateTime());
                prevLast = last;

                if (!sleep(request.getSleepTimeMillis())) break;
            }

            return DeltaUtils.convertUserTradesFromMap(map);
        } else if (request.hasFromId()) {
            partialRequest.setBy("id");
            final UserTrades userTrades = new UserTrades();
            String fromId = request.getFromId();
            while (true) {
                partialRequest.setFromId(fromId);
                final UserTrades tempUserTrades = getUserTrades(partialRequest);

                if (tempUserTrades.isEmpty()) break;
                userTrades.addAll(tempUserTrades);
                fromId = MathUtils.addOneToLongValueString(tempUserTrades.getLatest().getTid());

                if (!sleep(request.getSleepTimeMillis())) break;
            }

            return userTrades;
        } else {
            return getUserTrades(partialRequest);
        }
    }

    @Override
    public Transfers getTransfers(GetTransfersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet("/account/transactions",
            ParamsBuilder.create()
                .putIfNotNull("currency", toHitbtcCurrencySymbol(toUpper(request.getCurrency())))
                .putIfNotNull("sort", HitbtcUtils.toHitbtcSortType(request.getSort()))
                .putIfNotNull("by", request.getBy())
                .putIfNotNull("from", "timestamp".equals(request.getBy()) ? request.getStartEpochInSecond() : request.getFromId())
                .putIfNotNull("till", "timestamp".equals(request.getBy()) ? request.getEndEpochInSecond() : request.getTillId())
                .putIfNotNull("limit", request.getCount())
                .build());

        checkResponseError(request, response);

        try {
            return convertList(response, HitbtcTransferResponse.class,
                e -> new Transfers().setTransfers(e.stream().map(HitbtcTransferResponse::toTransfer).collect(Collectors.toList())));
        } catch (IOException e) {
            throw new CXAPIRequestException(e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
    }

    @Override
    public Transfers getTransfers(GetTransfersDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP t-a: time-asc,>>>(break)
        // GROUP i-a: id-asc,>>>(break)
        final GetTransfersRequest partialRequest = new GetTransfersRequest()
            .setCurrency(request)
            .setCount(100)
            .setSort(SortType.ASC); // Need to set asc explicitly

        if (request.hasStartDateTime()) {
            partialRequest.setBy("timestamp")
                .setStartDateTime(request.getStartDateTime())
                .setEndDateTime(nvl(request.getEndDateTime(), ZonedDateTime.now()));
            final Map<String, Transfer> map = new LinkedHashMap<>();
            Transfer prevLast = null;
            while (true) {
                final Transfers transfers = getTransfers(partialRequest);
                final Transfer last = transfers.getLatest();

                if (transfers.isEmpty() || Transfer.equalsById(prevLast, last)) break;
                map.putAll(DeltaUtils.convertToMap(transfers));

                partialRequest.setStartDateTime(transfers.getMaxDateTime());
                prevLast = last;

                if (!sleep(request.getSleepTimeMillis())) break;
            }

            return DeltaUtils.convertTransfersFromMap(map);
        } else if (request.hasFromId()) {
            partialRequest.setBy("index");
            final Transfers transfers = new Transfers();
            String fromId = request.getFromId();
            while (true) {
                partialRequest.setFromId(fromId);
                final Transfers tempTransfers = getTransfers(partialRequest);
                if (tempTransfers.isEmpty()) break;

                transfers.addAll(tempTransfers);
                fromId = MathUtils.addOneToLongValueString(tempTransfers.getLatest().getIndexAsString(1));

                if (!sleep(request.getSleepTimeMillis())) break;
            }

            return transfers;
        } else {
            return getTransfers(partialRequest);
        }
    }

    @Override
    protected HttpResponse requestGet(String endpoint)
        throws CTTKException
    {
        return requestGet(endpoint, null);
    }

    @Override
    protected HttpResponse requestGet(String endpoint, Map<String, String> params)
        throws CTTKException
    {
        final Request request = new Request.Builder()
            .url(getHostUrl(endpoint, params))
            .addHeader("Authorization", Credentials.basic(credential.getAccessKey(), credential.getSecretKey()))
            .build();

        debug(request);

        try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
            return toHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    protected HttpResponse requestDelete(String endpoint)
        throws CTTKException
    {
        final Request request = new Request.Builder()
            .url(getHostUrl(endpoint))
            .addHeader("Authorization", Credentials.basic(credential.getAccessKey(), credential.getSecretKey()))
            .delete()
            .build();

        debug(request);

        try (final Response response = super.httpClient.newCall(request).execute()) {
            return toHttpResponse(response);
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    protected HttpResponse requestPost(final String endpoint, final Map<String, String> params)
        throws CTTKException
    {
        Request request;
        try {
            request = new Request.Builder()
                .url(getHostUrl(endpoint))
                .addHeader("Authorization", Credentials.basic(credential.getAccessKey(), credential.getSecretKey()))
                .post(RequestBody.create(JSON, objectMapper.writeValueAsString(params)))
                .build();
        } catch (JsonProcessingException e) {
            throw new CTTKException(e).setCxId(this.getCxId());
        }

        debug(request, params);

        try {
            try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
                return toHttpResponse(httpResponse);
            }
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }
}
