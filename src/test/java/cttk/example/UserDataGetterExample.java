package cttk.example;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cttk.CXIds;
import cttk.auth.CXCredentialsProvider;
import cttk.dto.Balances;
import cttk.dto.MarketInfo;
import cttk.dto.MarketInfos;
import cttk.dto.Transfers;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCXException;
import cttk.exception.UnsupportedCurrencyException;
import cttk.util.UserDataGetter;

public class UserDataGetterExample {

    static final Logger logger = LoggerFactory.getLogger(UserDataGetterExample.class);

    public static void main(String[] args)
        throws IOException, UnsupportedCXException, CTTKException
    {
        final String cxId = CXIds.KORBIT;

        final boolean testOrder = true, testTrade = true, testTransfer = true;

        final UserDataGetter userDataGetter = new UserDataGetter(cxId, CXCredentialsProvider.getDefaultCredential(cxId).getTradeKey());

        final MarketInfos marketInfos = userDataGetter.getMarketInfos();
        final Balances balances = userDataGetter.getAllBalances();
        final Set<String> validCurrencySet = balances.getValidCurrencySet();

        System.out.println(validCurrencySet);

        if (testOrder) {
            if (cxId.equals(CXIds.UPBIT) || cxId.equals(CXIds.KRAKEN) || cxId.equals(CXIds.HITBTC) || cxId.equals(CXIds.BIBOX)) {
                final UserOrders userOrders = userDataGetter.getUserOrderHistory(null, null, ZonedDateTime.now().minusDays(30),
                    ZonedDateTime.now(), null);
                Examples.print(cxId, userOrders);
            } else {
                for (MarketInfo marketInfo : marketInfos.getMarketInfos()) {
                    System.out.println(marketInfo.getPairString());
                    if (validCurrencySet.contains(marketInfo.getBaseCurrency())
                        || validCurrencySet.contains(marketInfo.getQuoteCurrency()))
                    {
                        final UserOrder lastUserOrder = !cxId.equals(CXIds.BINANCE) ? null : new UserOrder().setOid("165052308").setDateTime(ZonedDateTime.parse("2018-06-12T11:52:08+09:00"));

                        final UserOrders userOrders = userDataGetter.getUserOrderHistory(marketInfo, lastUserOrder, ZonedDateTime.now().minusDays(30),
                            ZonedDateTime.now(), null);
                        Examples.print(cxId, userOrders);

                        try {
                            Thread.sleep(500l);
                        } catch (InterruptedException e) {
                            logger.error("Unexpected error", e);
                        }
                    }
                }
            }
        }

        if (testTrade) {
            if (cxId.equals(CXIds.POLONIEX) || cxId.equals(CXIds.KRAKEN) || cxId.equals(CXIds.HITBTC) || cxId.equals(CXIds.UPBIT) || cxId.equals(CXIds.BIBOX)) {
                UserTrades userTrades = userDataGetter.getUserTrades(null, null, ZonedDateTime.now().minusDays(30), ZonedDateTime.now(), null);
                Examples.print(cxId, userTrades);

                try {
                    Thread.sleep(500l);
                } catch (InterruptedException e) {
                    logger.error("Unexpected error", e);
                }
            } else {
                for (MarketInfo marketInfo : marketInfos.getMarketInfos()) {
                    System.out.println(marketInfo.getPairString());

                    UserTrades userTrades = userDataGetter.getUserTrades(marketInfo, null, ZonedDateTime.now().minusDays(30), ZonedDateTime.now(), null);
                    Examples.print(cxId, userTrades);

                    try {
                        Thread.sleep(500l);
                    } catch (InterruptedException e) {
                        logger.error("Unexpected error", e);
                    }
                }
            }
        }

        if (testTransfer) {
            if (cxId.equals(CXIds.BINANCE) || cxId.equals(CXIds.POLONIEX) || cxId.equals(CXIds.UPBIT) || cxId.equals(CXIds.BIBOX)) {
                Transfers transfers = userDataGetter.getTransfers(null, null, ZonedDateTime.now().minusDays(200), ZonedDateTime.now(), null);
                Examples.print(cxId, transfers);
            } else {
                for (String currency : marketInfos.getAllCurrencies()) {
                    try {
                        System.out.println(currency);
                        Transfers transfers = userDataGetter.getTransfers(currency, null, ZonedDateTime.now().minusDays(200), ZonedDateTime.now(), null);
                        Examples.print(cxId, transfers);

                        Thread.sleep(1000l);
                    } catch (UnsupportedCurrencyException e) {
                        logger.error("Transfer is not supported for [{}]", e.getCurrency(), e);
                    } catch (InterruptedException e) {
                        logger.error("Unexpected error", e);
                    }
                }
            }
        }
    }

}
