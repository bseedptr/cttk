package cttk.dto;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cttk.AbstractCXIdHolder;
import cttk.CurrencyPairListHolder;
import cttk.DateTimeGettableListHolder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(chain = true)
public class UserTrades
    extends AbstractCXIdHolder<UserTrades>
    implements CurrencyPairListHolder<UserTrades, UserTrade>, DateTimeGettableListHolder<UserTrades, UserTrade>
{
    List<UserTrade> trades;

    @JsonIgnore
    @Override
    public List<UserTrade> getList() {
        return trades;
    }

    @Override
    public UserTrades setList(List<UserTrade> list) {
        this.trades = list;
        return this;
    }

    /**
     * Remove trades whose order id is not found in the provided order ids.
     * If the orderIds is null, do not remove any trades.
     * @param orderIds the set of order ids
     * @return this instance
     */
    public UserTrades filterByOrderIdIn(Set<String> orderIds) {
        if (orderIds == null) return this;
        trades = trades.stream()
            .filter(t -> orderIds.contains(t.getOid()))
            .collect(Collectors.toList());
        return this;
    }

    @JsonIgnore
    public UserTrade getLatestById() {
        return isEmpty()
            ? null
            : getList().stream()
                .max((a, b) -> Long.valueOf(a.getTid()).compareTo(Long.valueOf(b.getTid())))
                .get();
    }

    @JsonIgnore
    public UserTrade getOldestById() {
        return isEmpty()
            ? null
            : getList().stream()
                .max((a, b) -> Long.valueOf(b.getTid()).compareTo(Long.valueOf(a.getTid())))
                .get();
    }
}
