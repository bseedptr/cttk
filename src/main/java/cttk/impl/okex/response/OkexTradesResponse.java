package cttk.impl.okex.response;

import static java.util.stream.Collectors.toList;

import java.math.BigDecimal;
import java.util.ArrayList;

import cttk.OrderSide;
import cttk.dto.Trade;
import cttk.dto.Trades;
import cttk.impl.util.DateTimeUtils;
import cttk.util.StringUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class OkexTradesResponse
    extends ArrayList<OkexTrade>
{
    public Trades toTrades() {
        return new Trades().setTrades(stream().map(OkexTrade::toTrade).collect(toList()));
    }
}

@Data
class OkexTrade {
    String timestamp;
    String trade_id;
    BigDecimal price;
    BigDecimal size;
    String side;

    Trade toTrade() {
        return new Trade()
            .setSno(StringUtils.parseBigInteger(trade_id))
            .setTid(trade_id)
            .setSide(OrderSide.of(side))
            .setDateTime(DateTimeUtils.parseZdt(timestamp))
            .setQuantity(size)
            .setPrice(price)
            .setTotal(price.multiply(size));
    }
}
