#!/bin/bash

./gradlew javadoc

VERSION=$(./gradlew -q printVersion)

if [[ $VERSION == *SNAPSHOT ]]; then
	BASE=snapshot
	echo $BASE
else
	BASE=release
	echo $BASE
fi

aws s3 sync build/docs/javadoc s3://docs.cttk.voostlab.io/javadoc/$BASE/$VERSION/ --delete --acl public-read

# to update latest javadoc, give any arguments when run this script
# ex) ./sync-docs.sh update-latest
# this will be called in jenkins CI

if [[ $# > 0 ]]; then
	mkdir -p build/docs/latest
	HTML_FILE=build/docs/latest/index.html
	
	echo "<html>" > $HTML_FILE
	echo "<head><meta http-equiv=\"refresh\" content=\"1;url=../$VERSION/index.html\" /><title>CTTK javadoc - latest $BASE ($VERSION)</title></head>" >> $HTML_FILE
	echo "<body>Click <a href=\"../$VERSION/index.html\">here</a> to go to the new page.</body>" >> $HTML_FILE
	echo "</html>" >> $HTML_FILE
	#aws s3 cp build/docs/latest/index.html s3://docs.cttk.voostlab.io/javadoc/$BASE/latest/ --acl public-read
	
	aws s3 cp s3://docs.cttk.voostlab.io/javadoc/$BASE/$VERSION/ s3://docs.cttk.voostlab.io/javadoc/$BASE/latest/ --recursive --acl public-read
fi	
