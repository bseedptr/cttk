package cttk.impl.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXAuthorityException;
import cttk.exception.UnsupportedCurrencyException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;

public class RetryUtils {
    public static final long DEFAULT_BASE_BACKOFF_MILLIS = 500l;

    private final static Logger logger = LoggerFactory.getLogger(RetryUtils.class);

    public static <T> T getDataWithRetry(
        final int maxRetryCount,
        final ThrowableSupplier<T> supplier)
        throws CTTKException
    {
        return getDataWithRetry(DEFAULT_BASE_BACKOFF_MILLIS, maxRetryCount, supplier);
    }

    public static <T> T getDataWithRetry(
        final long baseBackoffMillis,
        final int maxRetryCount,
        final ThrowableSupplier<T> supplier)
        throws CTTKException
    {
        long backoffMillis = baseBackoffMillis;
        Exception exception = null;
        for (int i = 0; i < maxRetryCount; i++) {
            try {
                return supplier.get();
            } catch (UnsupportedMethodException | UnsupportedCurrencyException | UnsupportedCurrencyPairException | CXAuthorityException e) {
                throw e;
            } catch (Exception e) {
                exception = e;
                if (i + 1 < maxRetryCount) {
                    logger.debug("Unable to get data. Retry after {}ms.", backoffMillis);
                    try {
                        Thread.sleep(backoffMillis);
                    } catch (InterruptedException e1) {
                        throw CTTKException.create(e1);
                    }
                    backoffMillis *= 2;
                }
            }
        }
        throw CTTKException.createOrCast(exception);
    }

    public static <T> T getDataWithRetryErrorMessage(
        final long baseBackoffMillis,
        final int maxRetryCount,
        final ThrowableSupplier<T> supplier,
        final String errorMessage)
        throws CTTKException
    {
        long backoffMillis = baseBackoffMillis;
        Exception exception = null;
        for (int i = 0; i < maxRetryCount; i++) {
            try {
                return supplier.get();
            } catch (CXAPIRequestException e) {
                exception = e;
                if (e.getExchangeMessage() == null
                    || !e.getExchangeMessage().contains(errorMessage))
                {
                    throw e;
                } else if (i + 1 < maxRetryCount) {
                    logger.debug("Unable to get data. Retry after {}ms. {}", backoffMillis);
                    try {
                        Thread.sleep(backoffMillis);
                    } catch (InterruptedException e1) {
                        throw CTTKException.create(e1);
                    }
                    backoffMillis *= 2;
                }
            } catch (Exception e) {
                exception = e;
            }
        }
        throw CTTKException.createOrCast(exception);
    }
}
