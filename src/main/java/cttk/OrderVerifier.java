package cttk;

import java.math.BigDecimal;

public interface OrderVerifier {
    /**
     * Round the price using precision, decimal point or tick size.
     * If there is no available information, same value will be returned.
     * @param price the price
     * @return rounded price
     */
    BigDecimal roundPrice(BigDecimal price);

    /**
     * Round the quantity using precision, decimal point or tick size.
     * If there is no available information, same value will be returned.
     * @param quantity the quantity
     * @return rounded quantity
     */
    BigDecimal roundQuantity(BigDecimal quantity);

    /**
     * Check price is in the defined boundary.
     * If there is no boundary, true is returned.
     * @param price the price
     * @return true if the price is in the defined boundary
     */
    boolean checkPriceRange(BigDecimal price);

    /**
     * Check quantity is in the defined boundary.
     * If there is no boundary, true is returned.
     * @param quantity the quantity
     * @return true if the quantity is in the defined boundary.
     */
    boolean checkQuantityRange(BigDecimal quantity);

    /**
     * Check if the given total is valid or not.
     * @param total the total
     * @return true if the total is bigger than min total.
     */
    boolean checkTotalRange(BigDecimal total);

    default boolean checkPriceRange(PricedQuantity pricedQuantity) {
        return pricedQuantity != null && checkPriceRange(pricedQuantity.getPrice());
    }

    default boolean checkQuantityRange(PricedQuantity pricedQuantity) {
        return pricedQuantity != null && checkQuantityRange(pricedQuantity.getQuantity());
    }

    default boolean checkTotalRange(PricedQuantity pricedQuantity) {
        return pricedQuantity != null && checkTotalRange(pricedQuantity.computeTotal());
    }

    default OrderVerificationResult verify(PricedQuantity pricedQuantity) {
        if (pricedQuantity == null) {
            throw new NullPointerException("pricedQuantity is null");
        }

        final OrderVerificationResult result = new OrderVerificationResult();

        boolean valid = true;

        final BigDecimal roundedPrice = roundPrice(pricedQuantity.getPrice());
        if (!checkPriceRange(pricedQuantity.getPrice())) {
            valid = false;
            result.addReason(InvalidOrderType.INVALID_PRICE_RANGE);
        }

        if (!checkQuantityRange(pricedQuantity.getQuantity())) {
            valid = false;
            result.addReason(InvalidOrderType.INVALID_QUANTITY_RANGE);
        }

        if (!checkTotalRange(pricedQuantity.computeTotal())) {
            valid = false;
            result.addReason(InvalidOrderType.INVALID_TOTAL_RANGE);
        }

        if (roundedPrice.compareTo(pricedQuantity.getPrice()) != 0) {
            valid = false;
            result.addReason(InvalidOrderType.INVALID_PRICE_INCREMENT);
            result.setRoundedPrice(roundedPrice);
        }

        final BigDecimal roundedQuantiy = roundQuantity(pricedQuantity.getQuantity());
        if (roundedQuantiy.compareTo(pricedQuantity.getQuantity()) != 0) {
            valid = false;
            result.addReason(InvalidOrderType.INVALID_QUANTITY_INCREMENT);
            result.setRoundedQuantity(roundedQuantiy);
        }

        return result.setValid(valid);
    }

    /**
     * Dummy verifier that does nothing but just return input value or true.
     */
    public static final OrderVerifier DUMMY = new OrderVerifier() {
        @Override
        public BigDecimal roundPrice(BigDecimal price) {
            return price;
        }

        @Override
        public boolean checkPriceRange(BigDecimal price) {
            return true;
        }

        @Override
        public BigDecimal roundQuantity(BigDecimal quantity) {
            return quantity;
        }

        @Override
        public boolean checkQuantityRange(BigDecimal quantity) {
            return true;
        }

        @Override
        public boolean checkTotalRange(PricedQuantity pricedQuantity) {
            return true;
        }

        @Override
        public boolean checkTotalRange(BigDecimal total) {
            return true;
        }
    };
}
