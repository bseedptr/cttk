package cttk.util;

import java.math.BigDecimal;
import java.math.BigInteger;

public class StringUtils {
    public static final String toUpper(String str) {
        return str == null ? null : str.toUpperCase();
    }

    public static final String toLower(String str) {
        return str == null ? null : str.toLowerCase();
    }

    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

    public static boolean isBlank(String str) {
        int strLen;
        if (str == null || (strLen = str.length()) == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if ((Character.isWhitespace(str.charAt(i)) == false)) {
                return false;
            }
        }
        return true;
    }

    public static boolean equals(String str1, String str2) {
        return str1 == null && str2 == null
            || str1 != null && str2 != null && str1.equals(str2);
    }

    public static final String concat(String str1, String str2) {
        if (str1 == null && str2 == null)
            return null;
        else if (str1 == null)
            return str2;
        else if (str2 == null)
            return str1;
        else
            return str1 + str2;
    }

    public static final String concat(String... strings) {
        if (strings == null) return null;
        StringBuilder sb = new StringBuilder();
        for (String str : strings) {
            if (str != null) sb.append(str);
        }
        if (sb.length() == 0) return null;
        return sb.toString();
    }

    public static final String valueOf(Object obj) {
        return obj == null ? null : String.valueOf(obj);
    }

    public static final BigDecimal parseBigDecimal(String val) {
        return isBlank(val) ? null : new BigDecimal(val.trim());
    }

    public static final BigInteger parseBigInteger(String val) {
        return isBlank(val) ? null : new BigInteger(val.trim());
    }

    public static final Long parseLong(String val) {
        try {
            return Long.parseLong(val.trim());
        } catch (Exception e) {
            return null;
        }
    }
}
