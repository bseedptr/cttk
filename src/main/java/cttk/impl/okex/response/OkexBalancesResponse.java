package cttk.impl.okex.response;

import static cttk.impl.okex.OkexUtilsV3.toStdCurrencySymbolV3;
import static java.util.stream.Collectors.toList;

import java.math.BigDecimal;
import java.util.ArrayList;

import cttk.dto.Balance;
import cttk.dto.Balances;
import cttk.impl.util.MathUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class OkexBalancesResponse
    extends ArrayList<OkexBalance>
{
    public Balances toBalances() {
        return new Balances().setBalances(stream().map(OkexBalance::toBalance).collect(toList()));
    }
}

@Data
class OkexBalance {
    String currency;
    BigDecimal balance;
    BigDecimal hold;
    BigDecimal available;
    long id;

    Balance toBalance() {
        return new Balance()
            .setCurrency(toStdCurrencySymbolV3(currency))
            .setTotal(MathUtils.add(available, hold))
            .setAvailable(available)
            .setInUse(hold);
    }
}
