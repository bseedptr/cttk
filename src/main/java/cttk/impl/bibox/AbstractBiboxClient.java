package cttk.impl.bibox;

import static cttk.impl.util.HttpHeaders.CONTENT_TYPE;
import static cttk.impl.util.MediaType.APPLICATION_JSON;

import java.io.IOException;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;

import cttk.AbstractCurrencyHolder;
import cttk.AbstractCurrencyPair;
import cttk.CXIds;
import cttk.exception.APILimitExceedException;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXAuthorityException;
import cttk.exception.CXServerException;
import cttk.exception.OrderNotFoundException;
import cttk.exception.UnsupportedCurrencyException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.AbstractCXClient;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ThrowableFunction;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public abstract class AbstractBiboxClient
    extends AbstractCXClient
{
    protected static final String BASE_URL = "https://api.bibox.com/v1";
    protected static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    protected static final int MAX_ORDERBOOK_COUNT = 200;
    protected static final int MAX_RECENT_TRANSACTION_COUNT = 200;

    public AbstractBiboxClient() {
        super();
    }

    @Override
    public String getCxId() {
        return CXIds.BIBOX;
    }

    protected HttpResponse requestGet(String path, Map<String, String> params)
        throws CTTKException
    {
        final Request request = new Request.Builder()
            .url(getHostUrl(path, params))
            .get()
            .build();

        debug(request);

        try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
            return toHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    protected HttpResponse requestPost(String path, Map<String, String> params)
        throws CTTKException
    {
        Request request = null;

        try {
            request = new Request.Builder()
                .url(getHostUrl(path))
                .addHeader(CONTENT_TYPE, APPLICATION_JSON)
                .post(RequestBody.create(JSON, objectMapper.writeValueAsString(params)))
                .build();
        } catch (JsonProcessingException e) {
            throw new CTTKException(e)
                .setCxId(getCxId());
        }

        debug(request, params);

        try {

            try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
                return toHttpResponse(httpResponse);
            }
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    protected String getHostUrl(String path, Map<String, String> params) {
        return getHostUrl(BASE_URL, path, params);
    }

    protected String getHostUrl(String endpoint) {
        return getHostUrl(BASE_URL, endpoint);
    }

    @Override
    protected <T, R> R convert(HttpResponse response, Class<T> claz, ThrowableFunction<T, R> mapper)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        try {
            return mapper.apply(objectMapper.readValue(response.getBody(), claz));
        } catch (IOException e) {
            throw CTTKException.create("json parse error\n" + response.getBody(), e, getCxId());
        }
    }

    @Override
    protected void checkErrorByResponseMessage(Object requestObj, HttpResponse response)
        throws CTTKException
    {
        // Detect common error of Bibox
        final String body = response.getBody();
        final Integer statusCode = response.getStatus();
        try {
            if (!body.contains("\"error\"")) return;

            final Map<String, Object> error = BiboxUtils.parseResponseError(objectMapper, body);
            final String exchangeErrorCode = (String) error.get("code");
            final String exchangeMessage = (String) error.get("msg");
            switch (exchangeErrorCode) {
                case "2040":
                    throw new OrderNotFoundException("2040 - Operation failed! No record")
                        .setCxId(this.getCxId())
                        .setResponseStatusCode(statusCode)
                        .setExchangeMessage(exchangeMessage)
                        .setExchangeErrorCode(exchangeErrorCode);
                case "2091":
                    throw new APILimitExceedException("2091 - API rate limit exceeded")
                        .setCxId(this.getCxId())
                        .setResponseStatusCode(statusCode)
                        .setExchangeMessage(exchangeMessage)
                        .setExchangeErrorCode(exchangeErrorCode);
                case "3000":
                    throw new CXAPIRequestException("3000 - Invalid request parameter")
                        .setCxId(this.getCxId())
                        .setResponseStatusCode(statusCode)
                        .setExchangeMessage(exchangeMessage)
                        .setExchangeErrorCode(exchangeErrorCode);
                case "3012":
                    throw new CXAuthorityException("3012 - Invalid API Key")
                        .setCxId(this.getCxId())
                        .setResponseStatusCode(statusCode)
                        .setExchangeMessage(exchangeMessage)
                        .setExchangeErrorCode(exchangeErrorCode);
                case "3016":
                    if (requestObj instanceof AbstractCurrencyPair<?>) {
                        AbstractCurrencyPair<?> request = (AbstractCurrencyPair<?>) requestObj;
                        throw new UnsupportedCurrencyPairException("3016 - Invalid currency pair")
                            .setCxId(this.getCxId())
                            .setCurrencyPair(request)
                            .setResponseStatusCode(statusCode)
                            .setExchangeMessage(exchangeMessage)
                            .setExchangeErrorCode(exchangeErrorCode);
                    } else if (requestObj instanceof AbstractCurrencyHolder<?>) {
                        AbstractCurrencyHolder<?> request = (AbstractCurrencyHolder<?>) requestObj;
                        throw new UnsupportedCurrencyException("3016 - Invalid currency")
                            .setCxId(this.getCxId())
                            .setCurrency(request.getCurrency())
                            .setExchangeMessage(exchangeMessage)
                            .setResponseStatusCode(statusCode)
                            .setExchangeErrorCode(exchangeErrorCode);
                    } else {
                        throw new CXAPIRequestException()
                            .setCxId(this.getCxId())
                            .setExchangeMessage(exchangeMessage)
                            .setResponseStatusCode(statusCode)
                            .setExchangeErrorCode(exchangeErrorCode);
                    }
                case "3025":
                    throw new CXAuthorityException("3025 - Invalid signature")
                        .setCxId(this.getCxId())
                        .setResponseStatusCode(statusCode)
                        .setExchangeMessage(exchangeMessage)
                        .setExchangeErrorCode(exchangeErrorCode);
                default:
                    throw new CXAPIRequestException(exchangeErrorCode + " - Unexpected error occurred. Check exchange docs. https://github.com/Biboxcom/API_Docs_en/wiki/REST_error_code")
                        .setCxId(this.getCxId())
                        .setResponseStatusCode(statusCode)
                        .setExchangeMessage(exchangeMessage)
                        .setExchangeErrorCode(exchangeErrorCode);
            }
        } catch (IOException e) {
            throw CTTKException.create("json parse error\n" + body, e, getCxId());
        }
    }
}
