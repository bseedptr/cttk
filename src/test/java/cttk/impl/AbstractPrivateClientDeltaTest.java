package cttk.impl;

import static java.util.stream.Collectors.toList;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;

abstract public class AbstractPrivateClientDeltaTest {
    protected UserOrder makeUserOrderFixture(String id) {
        return new UserOrder().setOid(id).setDateTime(ZonedDateTime.now());
    }

    protected UserTrade makeUserTradeFixture(String id) {
        return new UserTrade().setTid(id).setDateTime(ZonedDateTime.now());
    }

    protected Transfer makeTransferFixture(String id) {
        return new Transfer().setTid(id).setCreatedDateTime(ZonedDateTime.now());
    }

    protected List<UserOrder> makeUserOrderFixtures(String... ids) {
        return Arrays.stream(ids)
            .map(id -> new UserOrder().setOid(id).setDateTime(ZonedDateTime.now()))
            .collect(toList());
    }

    protected List<UserTrade> makeUserTradeFixtures(String... ids) {
        return Arrays.stream(ids)
            .map(id -> new UserTrade().setTid(id).setDateTime(ZonedDateTime.now()))
            .collect(toList());
    }

    protected List<Transfer> makeTransferFixtures(String... ids) {
        return Arrays.stream(ids)
            .map(id -> new Transfer().setTid(id).setCreatedDateTime(ZonedDateTime.now()))
            .collect(toList());
    }

    protected List<String> extractIds(UserOrders userOrders) {
        return userOrders.getList().stream().map(UserOrder::getOid).collect(toList());
    }

    protected List<String> extractIds(UserTrades userTrades) {
        return userTrades.getList().stream().map(UserTrade::getTid).collect(toList());
    }

    protected List<String> extractIds(Transfers transfers) {
        return transfers.getList().stream().map(Transfer::getTid).collect(toList());
    }
}
