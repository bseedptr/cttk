package cttk.impl.huobi.stream.response;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

import cttk.OrderSide;
import cttk.dto.Trade;
import cttk.dto.Trades;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HuobiWebsocketTradesResponse
    extends HuobiWebsocketResponse<HuobiTradesTick>
{
    public Trades toTrades() {
        return new Trades()
            .setTrades(toTradeList(tick.data));
    }

    static List<Trade> toTradeList(List<HuobiTrade> list) {
        if (list == null) return null;

        return list.stream()
            .map(t -> t.toTrade())
            .collect(Collectors.toList());
    }
}

@Data
class HuobiTradesTick {
    long id;
    long ts;
    List<HuobiTrade> data;
}

@Data
class HuobiTrade {
    BigInteger id;
    long ts;
    BigDecimal price;
    BigDecimal amount;
    String direction;

    public Trade toTrade() {
        return new Trade()
            .setSno(id)
            .setTid(String.valueOf(id))
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(ts))
            .setPrice(price)
            .setQuantity(amount)
            .setSide(OrderSide.of(direction));
    }
}