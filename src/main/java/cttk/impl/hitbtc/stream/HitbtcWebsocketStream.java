package cttk.impl.hitbtc.stream;

import cttk.CXStreamListener;
import cttk.impl.stream.AbstractWebsocketStream;
import cttk.impl.stream.MessageConverter;
import cttk.request.AbstractCXStreamRequest;
import okhttp3.WebSocketListener;

public class HitbtcWebsocketStream<T, R extends AbstractCXStreamRequest<R>>
    extends AbstractWebsocketStream<T, R>
{
    private static final String WSS_API_HITBTC_WS = "wss://api.hitbtc.com/api/2/ws";

    public HitbtcWebsocketStream(
        final String channel,
        final R request,
        final MessageConverter<T> converter,
        final CXStreamListener<T> listener)
    {
        super(channel, request, converter, listener);
    }

    @Override
    protected String getUrl() {
        return WSS_API_HITBTC_WS;
    }

    @Override
    protected WebSocketListener createWebSocketListener() {
        return new HitbtcWebsocketListener<>(this, channel, request, converter, listener);
    }
}
