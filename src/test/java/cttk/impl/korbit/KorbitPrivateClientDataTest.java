package cttk.impl.korbit;

import static cttk.dto.UserOrders.Category.ALL;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import cttk.CXIds;
import cttk.TransferType;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.dto.UserAccount;
import cttk.dto.UserOrders;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.UnsupportedCurrencyException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.AbstractPrivateCXClientDataTest;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserAccountRequest;
import cttk.request.GetUserOrderHistoryRequest;

public class KorbitPrivateClientDataTest
    extends AbstractPrivateCXClientDataTest
{
    @AfterClass
    public static void cleanup() {
        cleanup(KorbitPrivateClientDataTest.class);
    }

    @Override
    public String getCxId() {
        return CXIds.KORBIT;
    }

    @Test
    @Override
    public void testGetUserAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        GetUserAccountRequest guar = new GetUserAccountRequest();
        UserAccount userAccount = client.getUserAccount(guar);

        Assert.assertNotNull(userAccount);
        Assert.assertNotNull(userAccount.getNameCheckedDateTime());
        Assert.assertNotNull(userAccount.getName());
        Assert.assertNotNull(userAccount.getEmail());
        Assert.assertNotNull(userAccount.getPhone());
        //Assert.assertNotNull(userAccount.getBirthday());
    }

    @Test
    @Override
    public void testGetBankAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // 현재는 krw가 bank를 연동하지 않아서 정보가 오지 않는다.
        //super.testGetBankAccount();
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetWallet()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetWallet();
    }

    @Test
    @Override
    public void testGetTransfers()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        Transfers transfers = client.getTransfers(
            new GetTransfersRequest()
                .setType(TransferType.DEPOSIT)
                .setCurrency("ETH"));

        Assert.assertNotNull(transfers);
        Assert.assertTrue(transfers.isNotEmpty());
        for (Transfer transfer : transfers.getTransfers()) {
            Assert.assertNotNull(transfer.getStatus());
        }
    }

    @Test(expected = CXAPIRequestException.class)
    // Korbit error response can't distinguish UnsupportedCurrencyException
    @Override
    public void testGetUserTradeWrongCurrency()
        throws CTTKException
    {
        super.testGetUserTradeWrongCurrency();
    }

    @Test(expected = CXAPIRequestException.class)
    // Korbit error response can't distinguish UnsupportedCurrencyException
    @Override
    public void testGetOpenUserOrdersWrongCurrency()
        throws CTTKException
    {
        super.testGetOpenUserOrdersWrongCurrency();
    }

    @Test(expected = CXAPIRequestException.class)
    @Override
    // Korbit error response can't distinguish UnsupportedCurrencyException
    public void testGetTransfersWrongCurrency()
        throws CTTKException
    {
        try {
            client.getTransfers(
                new GetTransfersRequest()
                    .setType(TransferType.DEPOSIT)
                    .setCurrency(invalidCurrency));
        } catch (UnsupportedCurrencyException e) {
            Assert.assertEquals(this.getCxId(), e.getCxId());
            Assert.assertEquals(invalidCurrency, e.getCurrency());
        }
    }

    @Test(expected = CXAPIRequestException.class)
    @Override
    // Korbit error response can't distinguish UnsupportedCurrencyException
    public void testGetUserOrderHistoryWrongCurrency()
        throws CTTKException
    {
        super.testGetUserOrderHistoryWrongCurrency();
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetAllOpenUserOrders()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetAllOpenUserOrders();
    }

    @Test
    public void testGetUserOrderHistoryCategory()
        throws CTTKException
    {
        threadSleep();

        final GetUserOrderHistoryRequest request = new GetUserOrderHistoryRequest()
            .setCurrencyPair(getValidCurrencyPair());
        final UserOrders userOrders = client.getUserOrderHistory(request);

        logger.debug("{}", userOrders);

        Assert.assertEquals(userOrders.getCategory(), ALL);
    }
}
