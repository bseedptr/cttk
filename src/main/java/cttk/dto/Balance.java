package cttk.dto;

import static java.math.BigDecimal.ZERO;

import java.math.BigDecimal;

import cttk.AbstractCurrencyHolder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ToString(callSuper = true)
public class Balance
    extends AbstractCurrencyHolder<Balance>
{
    BigDecimal total;
    BigDecimal available;
    BigDecimal inUse;
    BigDecimal tradeInUse;
    BigDecimal withdrawalInUse;

    // bitfinex deposit, exchange, trading
    String type;

    public boolean isTotalExists() {
        return getTotal().compareTo(ZERO) > 0;
    }
}
