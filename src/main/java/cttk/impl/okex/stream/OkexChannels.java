package cttk.impl.okex.stream;

import cttk.CurrencyPair;
import cttk.impl.okex.OkexUtilsV3;

public class OkexChannels {
    public static final String getTickerChannel(CurrencyPair currencyPair) {
        return "spot/ticker:" + OkexUtilsV3.toOkexMarketSymbolV3(currencyPair);
    }

    public static final String getOrderBookChannel(CurrencyPair currencyPair) {
        return "spot/depth:" + OkexUtilsV3.toOkexMarketSymbolV3(currencyPair);
    }

    public static final String getTradeChannel(CurrencyPair currencyPair) {
        return "spot/trade:" + OkexUtilsV3.toOkexMarketSymbolV3(currencyPair);
    }
}
