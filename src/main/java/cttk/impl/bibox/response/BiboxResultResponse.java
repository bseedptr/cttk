package cttk.impl.bibox.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BiboxResultResponse<T>
    extends BiboxCommandResponse
{
    protected T result;
}
