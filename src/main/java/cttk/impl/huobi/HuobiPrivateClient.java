package cttk.impl.huobi;

import com.fasterxml.jackson.core.JsonProcessingException;
import cttk.PrivateCXClient;
import cttk.ScanDirection;
import cttk.TradingFeeProvider;
import cttk.TransferType;
import cttk.auth.Credential;
import cttk.dto.Balances;
import cttk.dto.BankAccount;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.dto.UserAccount;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;
import cttk.dto.Wallet;
import cttk.dto.Wallets;
import cttk.dto.WithdrawResult;
import cttk.exception.CTTKException;
import cttk.exception.CXServerException;
import cttk.exception.InsufficientBalanceException;
import cttk.exception.InvalidAmountMaxException;
import cttk.exception.InvalidAmountMinException;
import cttk.exception.InvalidAmountRangeException;
import cttk.exception.InvalidPriceIncrementPrecisionException;
import cttk.exception.InvalidPriceMaxException;
import cttk.exception.InvalidPriceMinException;
import cttk.exception.InvalidPriceRangeException;
import cttk.exception.InvalidQuantityIncrementPrecisionException;
import cttk.exception.OrderNotFoundException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.feeprovider.DefaultTradingFeeProvider;
import cttk.impl.huobi.response.HuobiAccountsResponse;
import cttk.impl.huobi.response.HuobiBalancesResponse;
import cttk.impl.huobi.response.HuobiDataResponse;
import cttk.impl.huobi.response.HuobiTransfersResponse;
import cttk.impl.huobi.response.HuobiUserOrderResponse;
import cttk.impl.huobi.response.HuobiUserOrdersResponse;
import cttk.impl.huobi.response.HuobiUserTradesResponse;
import cttk.impl.huobi.response.HuobiWithdrawResponse;
import cttk.impl.util.CryptoUtils;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.HttpHeaders;
import cttk.impl.util.HttpMethod;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.MediaType;
import cttk.impl.util.ParamsBuilder;
import cttk.impl.util.UriUtils;
import cttk.request.CancelOrderRequest;
import cttk.request.CreateMarketPriceOrderRequest;
import cttk.request.CreateOrderRequest;
import cttk.request.GetBankAccountRequest;
import cttk.request.GetOpenUserOrdersRequest;
import cttk.request.GetTransfersDeltaRequest;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserAccountRequest;
import cttk.request.GetUserOrderHistoryDeltaRequest;
import cttk.request.GetUserOrderHistoryRequest;
import cttk.request.GetUserOrderRequest;
import cttk.request.GetUserTradesDeltaRequest;
import cttk.request.GetUserTradesRequest;
import cttk.request.GetWalletRequest;
import cttk.request.WithdrawToBankRequest;
import cttk.request.WithdrawToWalletRequest;
import cttk.util.DeltaUtils;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;

import static cttk.dto.UserOrders.Category.ALL;
import static cttk.impl.huobi.HuobiUtils.getMarketSymbol;
import static cttk.util.DeltaUtils.sleep;
import static cttk.util.DeltaUtils.sleepWithException;

public class HuobiPrivateClient
    extends AbstractHuobiClient
    implements PrivateCXClient
{
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    private static final okhttp3.MediaType JSON = okhttp3.MediaType.parse("application/json; charset=utf-8");

    static {
        SIMPLE_DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    private static final String SIGNATURE_METHOD = "HmacSHA256";
    private static final String SIGNATURE_VERSION = "2";
    private static final String SIGNATURE = "Signature";

    protected Credential credential;

    private String HOSTNAME;

    public HuobiPrivateClient(Credential credential) {
        this.credential = credential;

        try {
            HOSTNAME = new URL(HOST_URL).getHost();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String getAccountId()
        throws CTTKException
    {
        final HttpResponse response = requestGetWithSign("/v1/account/accounts", null);

        checkResponseError(null, response);

        return convert(response, HuobiAccountsResponse.class, (r -> r.getWorkingAccountId()));
    }

    @Override
    public Balances getAllBalances()
        throws CTTKException
    {
        final String path = "/v1/account/accounts/" + this.getAccountId() + "/balance";
        final HttpResponse response = requestGetWithSign(path, null);

        checkResponseError(null, response);

        return super.convert(response, HuobiBalancesResponse.class, (r -> r.getBalances()));
    }

    @Override
    public UserOrder getUserOrder(GetUserOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final String path = "/v1/order/orders/" + request.getOid();
        final HttpResponse response = requestGetWithSign(path, null);
        try {
            checkResponseError(request, response);
        } catch (OrderNotFoundException e) {
            return null;
        }
        return convert(response, HuobiUserOrderResponse.class, (r -> {
            if (r.getData() == null) {
                return null;
            } else {
                return r.getData().getUserOrder();
            }
        }));
    }

    @Override
    public UserOrders getOpenUserOrders(GetOpenUserOrdersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGetWithSign("/v1/order/orders",
            ParamsBuilder.create()
                .put("symbol", getMarketSymbol(request))
                .put("states", "pre-submitted,submitting,submitted,partial-filled")
                .putIfNotNull("size", request.getCount())
                .build());

        checkResponseError(request, response);

        return convert(response, HuobiUserOrdersResponse.class, (r -> r.getUserOrders()));
    }

    @Override
    public UserOrders getAllOpenUserOrders(boolean withTrades)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGetWithSign("/v1/order/orders",
            ParamsBuilder.create()
                .put("symbol", getMarketSymbol(request))
                .put("states", "pre-submitted,submitting,submitted,partial-filled,partial-canceled,filled,canceled")
                .putIfNotNull("types", HuobiUtils.toTypesParamValueString(request.getSide()))
                .putIfNotNull("start-date", DateTimeUtils.toISODateString(request.getStartDateTime()))
                .putIfNotNull("end-date", DateTimeUtils.toISODateString(request.getEndDateTime()))
                .putIfNotNull("from", request.getFromId())
                .putIfNotNull("direct", HuobiUtils.toDirectString(request.getScanDirection()))
                .putIfNotNull("size", request.getCount())
                .build());

        try {
            checkResponseError(request, response);
        } catch (OrderNotFoundException e) {
            return new UserOrders().setCategory(ALL);
        }

        final UserOrders userOrders = convert(response, HuobiUserOrdersResponse.class, (r -> r.getUserOrders()));
        return userOrders.setCategory(ALL);
    }

    @Override
    public UserOrders getUserOrderHistory(GetUserOrderHistoryDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP t-d: time-desc,<<<(exception)
        // GROUP i-a: id-asc,>>>(break)
        if (request.hasStartDateTime())
            return getUserOrderHistoryByTime(request);
        else if (request.hasFromId())
            return getUserOrderHistoryDeltaById(request);
        else
            return getUserOrderHistory(new GetUserOrderHistoryRequest()
                .setCurrencyPair(request)
                .setCount(100));
    }

    private UserOrders getUserOrderHistoryByTime(GetUserOrderHistoryDeltaRequest request)
        throws CTTKException
    {
        final GetUserOrderHistoryRequest partialRequest = new GetUserOrderHistoryRequest()
            .setCurrencyPair(request)
            .setCount(100)
            .setStartDateTime(request.getStartDateTime())
            .setEndDateTime(request.getEndDateTime())
            .setScanDirection(ScanDirection.NEXT);
        return getUserOrderHistoryDelta(partialRequest, request.getSleepTimeMillis());
    }

    private UserOrders getUserOrderHistoryDeltaById(GetUserOrderHistoryDeltaRequest request)
        throws CTTKException
    {
        final GetUserOrderHistoryRequest partialRequest = new GetUserOrderHistoryRequest()
            .setCurrencyPair(request)
            .setCount(100)
            .setFromId(request.getFromId())
            .setScanDirection(ScanDirection.PREV);
        return getUserOrderHistoryDelta(partialRequest, request.getSleepTimeMillis());
    }

    private UserOrders getUserOrderHistoryDelta(GetUserOrderHistoryRequest request, Long sleepTime)
        throws CTTKException
    {
        final Map<String, UserOrder> userOrderMap = new LinkedHashMap<>();
        String fromId = request.getFromId();
        while (true) {
            request.setFromId(fromId);
            final UserOrders userOrders = getUserOrderHistory(request);

            if (userOrders.isEmpty())
                break;

            final UserOrder finalUserOrder = (request.isScanDirectionPrev()) ? userOrders.getLatestById() : userOrders.getOldestById();
            if (userOrders.size() == 1) {
                userOrderMap.put(finalUserOrder.getOid(), finalUserOrder);
                break;
            }

            userOrderMap.putAll(DeltaUtils.convertToMap(userOrders));
            fromId = finalUserOrder.getOid();
            if (request.isScanDirectionPrev())
                break;
            else
                sleepWithException(sleepTime, getCxId());

        }
        return new UserOrders().addAll(DeltaUtils.convertUserOrdersFromMap(userOrderMap)).setCategory(ALL);
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGetWithSign("/v1/order/matchresults", ParamsBuilder.create()
            .put("symbol", getMarketSymbol(request))
            .putIfNotNull("start-date", DateTimeUtils.toISODateString(request.getStartDateTime()))
            .putIfNotNull("end-date", DateTimeUtils.toISODateString(request.getEndDateTime()))
            .putIfNotNull("from", request.getFromId())
            .putIfNotNull("direct", HuobiUtils.toDirectString(request.getScanDirection()))
            .putIfNotNull("size", request.getCount())
            .build());

        checkResponseError(request, response);

        return convert(response, HuobiUserTradesResponse.class, (r -> r.getUserTrades()));
    }

    @Override
    public UserTrades getUserTrades(GetUserTradesDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP t-d: time-desc,<<<(exception)
        // GROUP i-a: id-asc,>>>(break)
        if (request.hasStartDateTime())
            return getUserTradesByTime(request);
        else if (request.hasFromId())
            return getUserTradesDeltaById(request);
        else
            return getUserTrades(new GetUserTradesRequest()
                .setCurrencyPair(request)
                .setCount(100));
    }

    private UserTrades getUserTradesByTime(GetUserTradesDeltaRequest deltaRequest)
        throws CTTKException
    {
        final GetUserTradesRequest partialRequest = new GetUserTradesRequest()
            .setCurrencyPair(deltaRequest)
            .setCount(100)
            .setStartDateTime(deltaRequest.getStartDateTime())
            .setEndDateTime(deltaRequest.getEndDateTime())
            .setScanDirection(ScanDirection.NEXT);
        return getUserTradesDelta(partialRequest, deltaRequest.getSleepTimeMillis());
    }

    private UserTrades getUserTradesDeltaById(GetUserTradesDeltaRequest deltaRequest)
        throws CTTKException
    {
        final GetUserTradesRequest partialRequest = new GetUserTradesRequest()
            .setCurrencyPair(deltaRequest)
            .setCount(100)
            .setFromId(deltaRequest.getFromId())
            .setScanDirection(ScanDirection.PREV);
        return getUserTradesDelta(partialRequest, deltaRequest.getSleepTimeMillis());
    }

    private UserTrades getUserTradesDelta(GetUserTradesRequest request, Long sleepTime)
        throws CTTKException
    {
        final Map<String, UserTrade> userTradeMap = new LinkedHashMap<>();
        String fromId = request.getFromId();
        while (true) {
            request.setFromId(fromId);
            final UserTrades userTrades = getUserTrades(request);

            if (userTrades.isEmpty())
                break;

            final UserTrade finalUserTrade = (request.isScanDirectionPrev()) ? userTrades.getLatestById() : userTrades.getOldestById();
            if (userTrades.size() == 1) {
                userTradeMap.put(finalUserTrade.getTid(), finalUserTrade);
                break;
            }

            userTradeMap.putAll(DeltaUtils.convertToMap(userTrades));
            fromId = finalUserTrade.getTid();
            if (request.isScanDirectionPrev())
                break;
            else
                sleepWithException(sleepTime, getCxId());
        }
        return new UserTrades().addAll(DeltaUtils.convertUserTradesFromMap(userTradeMap));
    }

    @Override
    public Transfers getTransfers(GetTransfersRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGetWithSign("/v1/query/deposit-withdraw",
            ParamsBuilder.create()
                .put("currency", HuobiUtils.toHuobiCurrencySymbol(request.getCurrency()))
                .put("type", HuobiUtils.toTransferTypeString(request.getType()))
                .put("from", request.getFromId())
                .put("size", request.getCount())
                .build());

        checkResponseError(request, response);

        return convert(response, HuobiTransfersResponse.class, (r -> r.getTransfers()));
    }

    @Override
    public Transfers getTransfers(GetTransfersDeltaRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // GROUP i-a: id-asc,>>>(break)
        if (!request.hasFromId())
            request.setFromId("0");
        return new Transfers()
            .addAll(getDepositsDelta(request))
            .addAll(getWithdrawalsDelta(request));
    }

    private Transfers getDepositsDelta(GetTransfersDeltaRequest deltaRequest)
        throws CTTKException
    {
        final GetTransfersRequest partialRequest = new GetTransfersRequest()
            .setType(TransferType.DEPOSIT)
            .setFromId(deltaRequest.getFromId())
            .setCurrency(deltaRequest)
            .setCount(100)
            .setScanDirection(ScanDirection.PREV);
        return getTransfersDelta(partialRequest, deltaRequest.getSleepTimeMillis());
    }

    private Transfers getWithdrawalsDelta(GetTransfersDeltaRequest deltaRequest)
        throws CTTKException
    {
        final GetTransfersRequest partialRequest = new GetTransfersRequest()
            .setType(TransferType.WITHDRAWAL)
            .setFromId(deltaRequest.getFromId())
            .setCurrency(deltaRequest)
            .setCount(100)
            .setScanDirection(ScanDirection.PREV);
        return getTransfersDelta(partialRequest, deltaRequest.getSleepTimeMillis());
    }

    private Transfers getTransfersDelta(GetTransfersRequest request, Long sleepTime)
        throws CTTKException
    {
        final Map<String, Transfer> transferMap = new LinkedHashMap<>();
        String fromId = request.getFromId();
        while (true) {
            request.setFromId(fromId);
            final Transfers transfers = getTransfers(request);

            if (transfers.isEmpty())
                break;

            final Transfer finalTransfer = (request.isScanDirectionPrev()) ? transfers.getLatestById() : transfers.getOldestById();
            if (transfers.size() == 1) {
                transferMap.put(finalTransfer.getTid(), finalTransfer);
                break;
            }

            transferMap.putAll(DeltaUtils.convertToMap(transfers));
            fromId = finalTransfer.getTid();
            if (!sleep(sleepTime)) break;
        }
        return new Transfers().addAll(DeltaUtils.convertTransfersFromMap(transferMap));
    }

    @Override
    public UserOrder createMarketPriceOrder(CreateMarketPriceOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPostWithSign("/v1/order/orders/place",
            ParamsBuilder.create()
                .put("symbol", getMarketSymbol(request))
                .put("amount", String.valueOf(request.getQuantity()))
                .put("type", HuobiUtils.toString(request.getSide()) + "-market")
                .put("account-id", this.getAccountId())
                .put("source-id", "api")
                .build());

        checkResponseError(request, response);

        return convert(response, HuobiDataResponse.class, i -> new UserOrder().setOid(i.getData()));
    }

    @Override
    public UserOrder createOrder(CreateOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPostWithSign("/v1/order/orders/place",
            ParamsBuilder.create()
                .put("symbol", getMarketSymbol(request))
                .put("price", String.valueOf(request.getPrice()))
                .put("amount", String.valueOf(request.getQuantity()))
                .put("type", HuobiUtils.toString(request.getSide()) + "-limit")
                .put("account-id", this.getAccountId())
                .put("source-id", "api")
                .build());

        checkOrderResponseError(request, response);
        checkResponseError(request, response);

        return fillResponseByRequest(request, convert(response, HuobiDataResponse.class, i -> i.getUserOrder()));
    }

    @Override
    public void cancelOrder(CancelOrderRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPostWithSign("/v1/order/orders/" + request.getOrderId() + "/submitcancel");

        checkResponseError(request, response);

        convert(response, HuobiDataResponse.class);
    }

    @Override
    public WithdrawResult withdraw(WithdrawToWalletRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestPostWithSign("/v1/dw/withdraw/api/create",
            ParamsBuilder.create()
                .put("currency", request.getCurrency())
                .put("amount", request.getQuantity())
                .put("address", request.getAddress())
                .putIfNotNull("addr-tag", request.getDestinationTag())
                .build());

        checkResponseError(request, response);

        return convert(response, HuobiWithdrawResponse.class, i -> i.getWithdrawResult());
    }

    public void cancelWithdraw(WithdrawToWalletRequest request) {
        //String url = HOST_URL + "/v1/dw/withdraw/api/" + request.getPaymentId() + "/cancel";
    }

    @Override
    public UserAccount getUserAccount(GetUserAccountRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGetWithSign("/v1/account/accounts", null);

        checkResponseError(request, response);

        return convert(response, HuobiAccountsResponse.class, (r -> r.getUserAccount()));
    }

    @Override
    public TradingFeeProvider getTradingFeeProvider()
        throws CTTKException
    {
        return new DefaultTradingFeeProvider().setFee("0.002");
    }

    @Override
    public BankAccount getBankAccount(GetBankAccountRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public WithdrawResult withdraw(WithdrawToBankRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public Wallet getWallet(GetWalletRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    @Override
    public Wallets getAllWallets()
        throws CTTKException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }

    protected HttpResponse requestGetWithSign(String endPoint, Map<String, String> params)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        if (params == null) params = new HashMap<>();
        params.putAll(createSignParams());
        params.put(SIGNATURE, createSign(HttpMethod.GET, endPoint, params));

        final Request request = new Request.Builder()
            .url(HOST_URL + endPoint + (params.isEmpty() ? "" : "?" + UriUtils.toEncodedQueryParamsString(params)))
            .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED)
            .addHeader(HttpHeaders.USER_AGENT, USER_AGENT)
            .get()
            .build();

        debug(request);

        try (Response httpResponse = super.httpClient.newCall(request).execute()) {
            return super.toHttpResponse(httpResponse);
        } catch (IOException ioe) {
            throw new CXServerException(ioe)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    protected HttpResponse requestPostWithSign(final String endpoint)
        throws UnsupportedCurrencyPairException, CTTKException
    {
        return requestPostWithSign(endpoint, null);
    }

    protected HttpResponse requestPostWithSign(final String endpoint, final Map<String, String> params)
        throws UnsupportedCurrencyPairException, CTTKException
    {
        Map<String, String> signParams = createSignParams();
        signParams.put(SIGNATURE, createSign(HttpMethod.POST, endpoint, signParams));

        Request request;
        try {
            request = new Request.Builder()
                .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .addHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
                .url(HOST_URL + endpoint + "?" + UriUtils.toEncodedQueryParamsString(signParams))
                .post(RequestBody.create(JSON, objectMapper.writeValueAsString(params)))
                .build();
        } catch (JsonProcessingException e) {
            throw new CTTKException(e).setCxId(getCxId());
        }

        debug(request);

        try {
            try (Response httpResponse = super.httpClient.newCall(request).execute()) {
                return super.toHttpResponse(httpResponse);
            }
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    private final Map<String, String> createSignParams()
        throws CTTKException
    {
        return ParamsBuilder.create()
            .put("AccessKeyId", credential.getAccessKey())
            .put("SignatureMethod", SIGNATURE_METHOD)
            .put("SignatureVersion", SIGNATURE_VERSION)
            .put("Timestamp", SIMPLE_DATE_FORMAT.format(new Date()))
            .build();
    }

    private String createSign(HttpMethod method, String requestPath, Map<String, String> params) {
        StringBuilder builder = new StringBuilder()
            .append(method.name() + "\n")
            .append(HOSTNAME + "\n")
            .append(requestPath + "\n")
            .append(UriUtils.toSortedQueryParamsString(params));

        return CryptoUtils.hmacSha256Base64(builder.toString(), credential.getSecretKey());
    }

    private void checkOrderResponseError(CreateOrderRequest request, HttpResponse response)
        throws CTTKException
    {
        final String body = response.getBody();
        final Integer statusCode = response.getStatus();
        String exchangeErrorCode = null;
        String exchangeMessage = null;
        try {
            Map<?, ?> map = objectMapper.readValue(body, Map.class);
            if (!"error".equals(map.get("status"))) return;
            exchangeErrorCode = Objects.toString(map.get("err-code"), "");
            exchangeMessage = Objects.toString(map.get("err-msg"), "");
        } catch (IOException e) {
            return;
        }

        if (exchangeErrorCode.contains("order-invalid-price")) {
            throw new InvalidPriceRangeException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setExchangeErrorCode(exchangeErrorCode)
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency());
        } else if (exchangeErrorCode.contains("invalid-amount")) {
            throw new InvalidAmountRangeException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setExchangeErrorCode(exchangeErrorCode)
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency());
        } else if (exchangeErrorCode.contains("account-frozen-balance-insufficient-error")) {
            throw new InsufficientBalanceException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setExchangeErrorCode(exchangeErrorCode)
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency());
        } else if (exchangeErrorCode.contains("order-orderprice-precision-error")) {
            throw new InvalidPriceIncrementPrecisionException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setExchangeErrorCode(exchangeErrorCode)
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency());
        } else if (exchangeErrorCode.contains("order-orderamount-precision-error")) {
            throw new InvalidQuantityIncrementPrecisionException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setExchangeErrorCode(exchangeErrorCode)
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency());
        } else if (exchangeErrorCode.contains("order-amount-min-error")) {
            // order-limitorder-amount-min-error
            // order-marketorder-amount-min-error
            throw new InvalidAmountMinException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setExchangeErrorCode(exchangeErrorCode)
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency());
        } else if (exchangeErrorCode.contains("order-price-min-error")) {
            // order-limitorder-price-min-error
            throw new InvalidPriceMinException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setExchangeErrorCode(exchangeErrorCode)
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency());
        } else if (exchangeErrorCode.contains("order-price-max-error")) {
            // order-limitorder-price-max-error
            throw new InvalidPriceMaxException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setExchangeErrorCode(exchangeErrorCode)
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency());
        } else if (exchangeErrorCode.contains("amount-max-error")) {
            // order-limitorder-amount-max-error
            throw new InvalidAmountMaxException()
                .setCxId(this.getCxId())
                .setExchangeMessage(body)
                .setResponseStatusCode(statusCode)
                .setExchangeErrorCode(exchangeErrorCode)
                .setOrderPrice(request.getPrice())
                .setOrderQuantity(request.getQuantity())
                .setBaseCurrency(request.getBaseCurrency())
                .setQuoteCurrency(request.getQuoteCurrency());
        }
    }
}
