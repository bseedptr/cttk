package cttk.impl.bitfinex.stream.response;

import static cttk.impl.bitfinex.BitfinexUtils.toStdCurrencyPair;
import static cttk.impl.util.DateTimeUtils.zdtFromEpochSecond;
import static cttk.util.StringUtils.parseBigDecimal;
import static cttk.util.StringUtils.parseBigInteger;
import static cttk.util.StringUtils.parseLong;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.dto.Trade;
import cttk.dto.Trades;
import cttk.impl.util.MathUtils;

public class BitfinexStringListListResponse
    extends ArrayList<ArrayList<String>>
{
    public Trades toTrades(CurrencyPair currencyPair) {
        final List<Trade> list = this.stream()
            .filter(i -> parseLong(i.get(0)) != null)
            .map(i -> {
                final String id = i.get(0);
                final BigDecimal price = parseBigDecimal(i.get(2));
                final BigDecimal quantity = parseBigDecimal(i.get(3));

                return new Trade()
                    .setSno(parseBigInteger((id)))
                    .setTid(id)
                    .setDateTime(zdtFromEpochSecond(parseLong(i.get(1))))
                    .setSide(quantity.compareTo(BigDecimal.ZERO) > 0 ? OrderSide.BUY : OrderSide.SELL)
                    .setPrice(price)
                    .setQuantity(quantity.abs())
                    .setTotal(MathUtils.multiply(price, quantity.abs()));
            })
            .collect(Collectors.toList());

        return new Trades()
            .setCurrencyPair(toStdCurrencyPair(currencyPair))
            .setTrades(list);
    }
}
