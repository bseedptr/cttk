package cttk.impl.bithumb;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class BithumbMarketData {
    private String name;
    private int length;
    private int outLength;
    private Trade trade;
    private String live;
    private String menuOut;
    private String menuIn;
    private String menuAsset;
    private String ios;
    private String openIndex;
    private Front front;

    @Data
    public static class Trade {
        private String complete;
        private String order;
        private String myOrder;
        private BigDecimal limitMin;
    }

    @Data
    public static class In {
        private String allow;
        private String msg;
    }

    @Data
    public static class Out {
        private String allow;
        private String msg;
    }

    @Data
    public static class Front {
        private In in;
        private Out out;
    }
}
