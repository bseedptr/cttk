package cttk.impl.huobi;

import cttk.CXIds;
import cttk.impl.AbstractOrderVerifierTest;

public class HuobiOrderVerifierTest
    extends AbstractOrderVerifierTest
{
    @Override
    public String getCxId() {
        return CXIds.HUOBI;
    }
}
