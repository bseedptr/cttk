package cttk;

public enum TransferTypeParam {
    ALL, IN_PROGRESS, COMPLETED, FAILED;
}
