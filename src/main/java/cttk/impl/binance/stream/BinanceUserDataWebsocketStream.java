package cttk.impl.binance.stream;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.UserData;
import cttk.auth.Credential;
import cttk.exception.CTTKException;
import cttk.exception.CXServerException;
import cttk.impl.binance.AbstractBinanceClient;
import cttk.impl.binance.response.BinanceListenKeyResponse;
import cttk.impl.util.HttpHeaders;
import cttk.impl.util.HttpMethod;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.MediaType;
import cttk.impl.util.ParamsBuilder;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.Response;
import okhttp3.WebSocket;

public class BinanceUserDataWebsocketStream
    extends AbstractBinanceClient
    implements CXStream<UserData>
{
    private final Credential credential;
    private final CXStreamListener<UserData> listener;

    protected WebSocket websocket;
    protected String listenKey;
    protected Long lastListenKeyRefreshedTime;
    private boolean isOpen = false;
    private Thread keepALiveThread;

    public BinanceUserDataWebsocketStream(
        final Credential credential,
        final CXStreamListener<UserData> listener)
        throws CTTKException
    {
        this.credential = credential;
        this.listener = listener;

        open();
    }

    @Override
    public CXStream<UserData> open()
        throws CTTKException
    {
        openWebsocket();

        isOpen = true;
        keepALiveThread = new Thread(() -> {
            final Long refreshTimeThreshold = TimeUnit.HOURS.toMillis(23) + TimeUnit.MINUTES.toMillis(25);
            while (isOpen) {
                try {
                    Thread.sleep(30 * 60 * 1000);
                    Long elapsedTime = System.currentTimeMillis() - lastListenKeyRefreshedTime;
                    if (elapsedTime > refreshTimeThreshold) {
                        reOpenWebsocket();
                    } else {
                        updateUserDataListenKey(listenKey);
                    }
                } catch (CTTKException e) {
                    logger.error("Unexpected error", e);
                    if (e.getMessage().contains("This listenKey does not exist")) {
                        reOpenWebsocket();
                    }
                } catch (InterruptedException e) {
                    if (isOpen) logger.error("Unexpected error", e);
                }
            }
        });

        keepALiveThread.start();

        return this;
    }

    @Override
    public void close()
        throws Exception
    {
        closeWebsocket();
        isOpen = false;
        if (keepALiveThread != null && keepALiveThread.isAlive()) {
            keepALiveThread.interrupt();
        }
    }

    @Override
    public CXStreamListener<UserData> getListener() {
        return listener;
    }

    private void reOpenWebsocket() {
        try {
            closeWebsocket();
            openWebsocket();
        } catch (CTTKException e) {
            logger.error("Unexpected error", e);
        }
    }

    private void openWebsocket()
        throws CTTKException
    {
        listenKey = getUserDataListenKey();
        lastListenKeyRefreshedTime = System.currentTimeMillis();

        final OkHttpClient okHttpClient = createOkHttpClient(10);

        websocket = okHttpClient.newWebSocket(
            new Request.Builder().url(getUrl()).build(),
            new BinanceUserDataWebsocketListener(this, listener));

        okHttpClient.dispatcher().executorService().shutdown();
    }

    private void closeWebsocket()
        throws CTTKException
    {
        if (websocket != null) websocket.close(NORMAL_CLOSURE_STATUS, null);
        this.closeUserDataListenKey(listenKey);
    }

    public String getUserDataListenKey()
        throws CTTKException
    {
        final HttpResponse response = request(HttpMethod.POST, PUBLIC_URL + "/userDataStream");

        return convert(response, BinanceListenKeyResponse.class, i -> i.getListenKey());
    }

    public void updateUserDataListenKey(String listenKey)
        throws CTTKException
    {
        request(HttpMethod.PUT, PUBLIC_URL + "/userDataStream",
            ParamsBuilder.build("listenKey", listenKey));
    }

    public void closeUserDataListenKey(String listenKey)
        throws CTTKException
    {
        if (listenKey != null) {
            request(HttpMethod.DELETE, PUBLIC_URL + "/userDataStream",
                ParamsBuilder.build("listenKey", listenKey));
        }
    }

    private String getUrl() {
        return "wss://stream.binance.com:9443/ws/" + listenKey;
    }

    private static final String HEADER_ACCESS_KEY = "X-MBX-APIKEY";

    protected HttpResponse request(HttpMethod httpMethod, String url)
        throws CTTKException
    {
        return request(httpMethod, url, null);
    }

    protected HttpResponse request(HttpMethod httpMethod, String url, Map<String, String> params)
        throws CTTKException
    {
        final Builder builder = new Request.Builder();
        builder.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED);
        builder.addHeader(HEADER_ACCESS_KEY, credential.getAccessKey());

        if (httpMethod == HttpMethod.GET) {
            throw new CTTKException("invalid request").setCxId(this.getCxId());
        } else {
            builder.url(url);

            final FormBody formBody = createFormBody(params);

            if (httpMethod == HttpMethod.POST) {
                builder.post(formBody);
            } else if (httpMethod == HttpMethod.PUT) {
                builder.put(formBody);
            } else if (httpMethod == HttpMethod.DELETE) {
                builder.delete(formBody);
            }
        }

        final Request request = builder.build();

        debug(request, params);

        try (Response httpResponse = super.httpClient.newCall(request).execute()) {
            return toHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }
}
