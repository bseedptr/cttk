package cttk.impl.bibox;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import cttk.CurrencyPair;
import cttk.TradingFeeProvider;
import cttk.dto.TradingFee;
import cttk.exception.CTTKException;

public class BiboxTradingFeeProviderTest {
    private TradingFeeProvider tradingFeeProvider;

    @Before
    public void setUp() {
        tradingFeeProvider = new BiboxTradingFeeProvider();
    }

    @Test
    public void testGetFeeAnyCurrencyShouldBeSameFee()
        throws CTTKException
    {
        final TradingFee EOS_ETH = tradingFeeProvider.getFee(CurrencyPair.of("EOS", "ETH"));
        final TradingFee BTC_USDT = tradingFeeProvider.getFee(CurrencyPair.of("BTC", "USDT"));
        final TradingFee ETH_DAI = tradingFeeProvider.getFee(CurrencyPair.of("ETH", "DAI"));

        Assert.assertEquals("Bibox fees are fixed at 0.1%.", new BigDecimal("0.001"), EOS_ETH.getMakerFee());
        Assert.assertEquals("Bibox fees are fixed at 0.1%.", new BigDecimal("0.001"), BTC_USDT.getMakerFee());
        Assert.assertEquals("Bibox fees are fixed at 0.1%.", new BigDecimal("0.001"), ETH_DAI.getMakerFee());
    }
}