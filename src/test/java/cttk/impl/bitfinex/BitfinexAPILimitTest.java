package cttk.impl.bitfinex;

import cttk.CXIds;
import cttk.impl.AbstractAPILimitTest;

public class BitfinexAPILimitTest
    extends AbstractAPILimitTest
{
    @Override
    public String getCxId() {
        return CXIds.BITFINEX;
    }

}
