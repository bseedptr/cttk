package cttk.auth;

import java.util.HashMap;
import java.util.Map;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCXException;
import cttk.impl.korbit.KorbitAuthTokenProvider;

public class AuthTokenProviderFactory {
    static final Map<String, AuthTokenProvider> INSTANCES = new HashMap<>();

    public synchronized static AuthTokenProvider getInstance(final String cxId)
        throws CTTKException
    {
        if (cxId == null || cxId.trim().isEmpty()) {
            throw new UnsupportedCXException().setCxId(cxId);
        }

        final String upperCxId = cxId.trim().toUpperCase();

        if (!INSTANCES.containsKey(upperCxId)) {
            INSTANCES.put(upperCxId, create(upperCxId));
        }
        return INSTANCES.get(upperCxId);
    }

    private static AuthTokenProvider create(final String upperCxId)
        throws CTTKException
    {
        switch (upperCxId) {
            case CXIds.KORBIT:
                return new CachedAuthTokenProvider(new KorbitAuthTokenProvider());
            default:
                throw new UnsupportedCXException().setCxId(upperCxId);
        }
    }
}
