package cttk.impl.huobi.response;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.CurrencyPair;
import cttk.dto.Ticker;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HuobiTickerResponse
    extends HuobiResponse
{
    HuobiTicker tick;

    public Ticker getTicker(CurrencyPair currencyPair) {
        return new Ticker()
            .setCurrencyPair(currencyPair)
            .setOpenPrice(tick.getOpen())
            .setLastPrice(tick.getClose())
            .setHighPrice(tick.getHigh())
            .setLowPrice(tick.getLow())
            .setVolume(tick.getAmount())
            .setQuoteVolume(tick.getVolumn())
            .setHighestBid(tick.bid.get(0))
            .setLowestAsk(tick.ask.get(0))
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(super.getTimestamp()));
    }
}

@Data
class HuobiTicker {
    Long id;
    Long version;
    BigDecimal open;
    BigDecimal close;
    BigDecimal high;
    BigDecimal low;
    BigDecimal count;
    @JsonProperty("vol")
    BigDecimal volumn;
    BigDecimal amount;
    List<BigDecimal> ask;
    List<BigDecimal> bid;
}
