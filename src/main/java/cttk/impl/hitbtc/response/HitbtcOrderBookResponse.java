package cttk.impl.hitbtc.response;

import static cttk.impl.hitbtc.HitbtcUtils.toStdCurrencyPair;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.dto.OrderBook;
import lombok.Data;

@Data
public class HitbtcOrderBookResponse {
    List<HitbtcOrder> ask;
    List<HitbtcOrder> bid;
    String timestamp;

    private ZonedDateTime getDateTime() {
        return timestamp == null ? null : ZonedDateTime.parse(timestamp);
    }

    public OrderBook toOrderBook(CurrencyPair currencyPair) {
        return new OrderBook()
            .setCurrencyPair(toStdCurrencyPair(currencyPair))
            .setAsks(toOrderList(ask))
            .setBids(toOrderList(bid))
            .setDateTime(getDateTime());
    }

    public static List<OrderBook.SimpleOrder> toOrderList(List<HitbtcOrder> hitbtcOrders) {
        if (hitbtcOrders == null) return null;
        return hitbtcOrders.stream()
            .map(e -> new OrderBook.SimpleOrder(e.getPrice(), e.getSize()))
            .collect(Collectors.toList());
    }
}

@Data
class HitbtcOrder {
    String size;
    String price;
}