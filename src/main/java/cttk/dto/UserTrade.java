package cttk.dto;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import cttk.AbstractCurrencyPair;
import cttk.DateTimeHolder;
import cttk.OrderSide;
import cttk.PricedQuantity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(chain = true)
public class UserTrade
    extends AbstractCurrencyPair<UserTrade>
    implements DateTimeHolder<UserTrade>, PricedQuantity
{
    Long gtid;
    String tid;
    String matchId;
    String oid;
    String clientOrderId;
    ZonedDateTime dateTime;
    OrderSide side;
    String status;
    BigDecimal price;
    BigDecimal quantity;
    BigDecimal total;
    BigDecimal fee;
    String feeCurrency;
    BigDecimal feeRate;
    Boolean isMaker;
    Boolean isBestMatch;
    String category;

    public static boolean equalsById(UserTrade a, UserTrade b) {
        return a == null && b == null
            || a != null && b != null && a.getTid().equals(b.getTid());
    }
}
