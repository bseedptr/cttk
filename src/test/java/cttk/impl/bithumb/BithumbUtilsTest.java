package cttk.impl.bithumb;

import static cttk.OrderSide.BUY;
import static cttk.OrderSide.SELL;

import org.junit.Assert;
import org.junit.Test;

public class BithumbUtilsTest {
    @Test
    public void testGuessFeeCurrencyIfOderSideIsSELLThenAlwaysKRW() {
        String feeCurrencyOfETH = BithumbUtils.guessFeeCurrency(SELL, "ETH");
        String feeCurrencyOfBTC = BithumbUtils.guessFeeCurrency(SELL, "BTC");
        String feeCurrencyOfEOS = BithumbUtils.guessFeeCurrency(SELL, "EOS");

        Assert.assertEquals("Should be always 'KRW' if order side is SELL.", "KRW", feeCurrencyOfETH);
        Assert.assertEquals("Should be always 'KRW' if order side is SELL.", "KRW", feeCurrencyOfBTC);
        Assert.assertEquals("Should be always 'KRW' if order side is SELL.", "KRW", feeCurrencyOfEOS);
    }

    @Test
    public void testGuessFeeCurrencyIfOderSideIsBUYThenBaseCurrency() {
        String feeCurrency = BithumbUtils.guessFeeCurrency(BUY, "ETH");

        Assert.assertEquals("Should be'ETH'.", "ETH", feeCurrency);
    }
}