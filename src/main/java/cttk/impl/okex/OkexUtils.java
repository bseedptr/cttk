package cttk.impl.okex;

import static cttk.OrderSide.BUY;
import static cttk.util.StringUtils.toLower;
import static cttk.util.StringUtils.toUpper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.ObjectMapper;

import cttk.CXIds;
import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.PricingType;
import cttk.exception.APILimitExceedException;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXAuthorityException;
import cttk.exception.InvalidAPIKeyVersionException;
import cttk.exception.OrderNotFoundException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.request.AbstractCXStreamRequest;
import okhttp3.Response;

public abstract class OkexUtils {
    private static ObjectMapper objectMapper = new ObjectMapper();
    private final static String cxId = CXIds.OKEX;

    public static String getMarketSymbol(final CurrencyPair currencyPair) {
        return currencyPair == null ? null
            : toLower(currencyPair.getBaseCurrency()) + "_" + toLower(currencyPair.getQuoteCurrency());
    }

    public static CurrencyPair getMarketSymbol(String string) {
        if (string == null) return null;
        final String[] arr = string.split("_");
        return CurrencyPair.of(toStdCurrencySymbol(arr[0]), toStdCurrencySymbol(arr[1]));
    }

    public static PricingType getPricingType(final String type) {
        if (type.endsWith("_market"))
            return PricingType.MARKET;
        else
            return PricingType.LIMIT;
    }

    public static OrderStatus getStatusType(final int status) {
        switch (status) {
            case 0:
                return OrderStatus.UNFILLED;
            case 1:
                return OrderStatus.PARTIALLY_FILLED;
            case 2:
                return OrderStatus.FILLED;
            case 3:
            case -1:
                return OrderStatus.CANCELED;
            default:
                return null;
        }
    }

    /**
     * Convert the given currency symbol to the standardized symbol
     * @param currency the currency symbol
     * @return the standard currency symbol
     */
    public static String toStdCurrencySymbol(String currency) {
        return currency == null ? null : currency.trim().toUpperCase();
    }

    public static OrderSide parseOrderSide(String side) {
        if (side == null) return null;
        switch (toUpper(side)) {
            case "SELL_MARKET":
                return OrderSide.SELL;
            case "BUY_MARKET":
                return OrderSide.BUY;
            default:
                return OrderSide.of(side);
        }
    }

    public static String guessFeeCurrency(OrderSide side, CurrencyPair pair) {
        return side == BUY ? pair.getBaseCurrency() : pair.getQuoteCurrency();
    }

    public static Throwable streamCheckResponseError(Throwable t, Response response) {
        if (response != null) {
            int statusCode = response.code();
            String exchangeMessage = response.body().toString();
            if (statusCode == 503) {
                t = new APILimitExceedException(t)
                    .setCxId(CXIds.OKEX)
                    .setExchangeMessage(exchangeMessage)
                    .setResponseStatusCode(statusCode);
            }
        }

        return t;
    }

    public static void checkStreamErrorByResponseMessage(Object requestObj, String body)
        throws CTTKException
    {
        Map<?, ?> map;
        String exchangeErrorCode;

        try {
            map = objectMapper.readValue(body, Map.class);
            exchangeErrorCode = map.get("errorCode").toString();
        } catch (IOException | NumberFormatException | NullPointerException e) {
            return;
        }

        final String message = map.containsKey("message") ? map.get("message").toString() : "";

        switch (exchangeErrorCode) {
            case "30001": // OK_ACCESS_KEY cannot be blank
            case "30002": // OK_ACCESS_SIGN cannot be blank
            case "30004": // OK_ACCESS_PASSPHRASE cannot be blank
            case "30005": // Invalid OK_ACCESS_TIMESTAMP
            case "30006": // Invalid OK_ACCESS_KEY
            case "30008": // Timestamp request expired
            case "30013": // Invalid sign
            case "30027": // Login failure
            case "30041": // User not logged in / User must be logged in
            case "30042": // Already logged in
                throw new CXAuthorityException(message)
                    .setCxId(cxId)
                    .setExchangeMessage(body)
                    .setExchangeErrorCode(exchangeErrorCode);
            case "30040": { // {0} Channel : {1} doesn't exist
                if (requestObj instanceof AbstractCXStreamRequest) {
                    AbstractCXStreamRequest request = (AbstractCXStreamRequest) requestObj;
                    Pattern p = Pattern.compile("Channel (?<channel>[a-zA-Z/]+):(?<baseCurrency>[a-zA-Z]+)-(?<quoteCurrency>[a-zA-Z]+) doesn't exist");
                    Matcher m = p.matcher(body);
                    if (m.find()) {
                        throw new UnsupportedCurrencyPairException(message)
                            .setCxId(cxId)
                            .setQuoteCurrency(request.getQuoteCurrency())
                            .setBaseCurrency(request.getBaseCurrency())
                            .setExchangeMessage(body)
                            .setExchangeErrorCode(exchangeErrorCode);
                    }
                }
                break;
            }
            case "30026": // Requested too frequent; endpoint limit exceeded
                throw new APILimitExceedException(message)
                    .setCxId(cxId)
                    .setExchangeMessage(body)
                    .setExchangeErrorCode(exchangeErrorCode);

        }
        throw new CXAPIRequestException(message)
            .setCxId(cxId)
            .setExchangeMessage(body)
            .setExchangeErrorCode(exchangeErrorCode);
    }

    public static void checkErrorByResponseMessage(Object requestObj, String body, Integer statusCode)
        throws CXAPIRequestException
    {
        String exchangeErrorCode;
        try {
            final Map<?, ?> errorMap = objectMapper.readValue(body, HashMap.class);
            if (errorMap.containsKey("code")) {
                exchangeErrorCode = String.valueOf(errorMap.get("code"));
            } else if (errorMap.containsKey("error_code")) {
                exchangeErrorCode = String.valueOf(errorMap.get("error_code"));
            } else {
                return;
            }
        } catch (IOException e) {
            return;
        }
        if (exchangeErrorCode == null || exchangeErrorCode.equals("")) return;

        // Error codes of v3 start from 30000
        switch (exchangeErrorCode) {
            case "30031": // token does not exist
            case "30032": // pair does not exist
                throw CXAPIRequestException.createByRequestObj(requestObj)
                    .setCxId(cxId)
                    .setExchangeMessage(body)
                    .setResponseStatusCode(statusCode)
                    .setExchangeErrorCode(exchangeErrorCode);
            case "30016": // you are using v1 apiKey, please use v1 endpoint
                throw new InvalidAPIKeyVersionException("Check your API Key version. Only v3 is supported.")
                    .setCxId(cxId)
                    .setExchangeMessage(body)
                    .setResponseStatusCode(statusCode)
                    .setExchangeErrorCode(exchangeErrorCode);
            case "30014": // request too frequent
            case "30026":
                throw new APILimitExceedException()
                    .setCxId(cxId)
                    .setExchangeMessage(body)
                    .setResponseStatusCode(statusCode)
                    .setExchangeErrorCode(exchangeErrorCode);
            case "20015":
            case "33014": // order does not exist
                throw new OrderNotFoundException()
                    .setCxId(cxId)
                    .setExchangeMessage(body)
                    .setResponseStatusCode(statusCode)
                    .setExchangeErrorCode(exchangeErrorCode);
            case "30006": // invalid OK_ACCESS_KEY
            case "30012": // invalid authorization
            case "30013": // invalid sign
            case "30015": // request header "OK_ACCESS_PASSPHRASE" incorrect
            case "30027": // Login failure
            case "30041": // User not logged in
                throw new CXAuthorityException()
                    .setCxId(cxId)
                    .setExchangeMessage(body)
                    .setResponseStatusCode(statusCode)
                    .setExchangeErrorCode(exchangeErrorCode);
            default:
                throw new CXAPIRequestException()
                    .setCxId(cxId)
                    .setExchangeMessage(body)
                    .setResponseStatusCode(statusCode)
                    .setExchangeErrorCode(exchangeErrorCode);
        }
    }
}
