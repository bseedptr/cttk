package cttk.impl.bibox.response;

import cttk.dto.Trades;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BiboxTradeResponse
    extends BiboxResultResponse<BiboxTrades>
{
    public Trades toTrades() {
        return this.result.toTrades();
    }
}
