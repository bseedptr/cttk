package cttk.impl.huobi.response;

import static cttk.impl.huobi.HuobiUtils.toStdCurrencySymbol;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.TransferStatus;
import cttk.TransferType;
import cttk.dto.Transfer;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;

@Data
public class HuobiTransfer {
    Long id;
    String type;
    String chain;
    String currency;
    @JsonProperty("tx-hash")
    String txHash;
    BigDecimal amount;
    String address;
    @JsonProperty("address-tag")
    String addressTag;
    BigDecimal fee;
    String state;
    @JsonProperty("created-at")
    Long createdAt;
    @JsonProperty("updated-at")
    Long updatedAt;

    public Transfer getTransfer() {
        final TransferType transferType = TransferType.of(type);
        return new Transfer()
            .setCurrency(toStdCurrencySymbol(currency))
            .setChain(chain)
            .setType(transferType)
            .setTid(id.toString())
            .setTxId(txHash)
            .setAmount(amount)
            .setFee(fee)
            .setStatus(state)
            .setStatusType(getStatusType(transferType))
            .setAddress(address)
            .setAddressTag(addressTag)
            .setCreatedDateTime(DateTimeUtils.zdtFromEpochMilli(createdAt))
            .setUpdatedDateTime(DateTimeUtils.zdtFromEpochMilli(updatedAt));
    }

    private TransferStatus getStatusType(TransferType transferType) {
        if (transferType == TransferType.DEPOSIT) {
            switch (state.toUpperCase()) {
                case "UNKNOWN":
                    return TransferStatus.UNKNOWN;
                case "CONFIRMING":
                    return TransferStatus.PENDING;
                case "CONFIRMED":
                    return TransferStatus.CONFIRMED;
                case "SAFE":
                    return TransferStatus.SUCCESS;
                case "ORPHAN":
                    return TransferStatus.FAILED;
            }
        } else if (transferType == TransferType.WITHDRAWAL) {
            switch (state.toUpperCase()) {
                case "CANCELED":
                    return TransferStatus.CANCELED;
                case "SUBMITTED":
                case "REEXAMINE":
                case "PASS":
                case "PRE-TRANSFER":
                    return TransferStatus.PENDING;
                case "WALLET-TRANSFER":
                    return TransferStatus.CONFIRMED;
                case "REJECT":
                case "WALLET-REJECT":
                    return TransferStatus.REJECTED;
                case "CONFIRMED":
                    return TransferStatus.SUCCESS;
                case "CONFIRM-ERROR":
                    return TransferStatus.FAILED;
                case "REPEALED":
                    return TransferStatus.FAILED;
            }
        }
        return null;
    }
}
