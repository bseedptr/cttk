package cttk.impl.upbit.response;

import static java.util.stream.Collectors.toList;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.dto.OrderBook;
import cttk.dto.OrderBook.SimpleOrder;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class UpbitOrderBookResponse
    extends ArrayList<UpbitOrderBook>
{
    public OrderBook toOrderBook() {
        return new OrderBook()
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(get(0).timestamp))
            .setAsks(get(0).toAsks())
            .setBids(get(0).toBids());
    }
}

@Data
class UpbitOrderBook {
    String market;
    long timestamp;
    @JsonProperty("total_ask_size")
    BigDecimal totalAskSize;
    @JsonProperty("total_bid_size")
    BigDecimal totalBidSize;
    @JsonProperty("orderbook_units")
    List<PriceSizePair> orderBookUnits;

    List<SimpleOrder> toAsks() {
        return orderBookUnits
            .stream()
            .map(i -> new SimpleOrder(i.getAskPrice(), i.getAskSize()))
            .collect(toList());
    }

    List<SimpleOrder> toBids() {
        return orderBookUnits
            .stream()
            .map(i -> new SimpleOrder(i.getBidPrice(), i.getBidSize()))
            .collect(toList());
    }

    @Data
    static class PriceSizePair {
        @JsonProperty("ask_price")
        BigDecimal askPrice;
        @JsonProperty("ask_size")
        BigDecimal askSize;
        @JsonProperty("bid_price")
        BigDecimal bidPrice;
        @JsonProperty("bid_size")
        BigDecimal bidSize;
    }
}