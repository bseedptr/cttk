package cttk.impl.stream.eachmarket;

import static cttk.impl.stream.eachmarket.PollingLevel.LEVEL1;
import static cttk.impl.stream.eachmarket.PollingLevel.LEVEL2;
import static cttk.impl.stream.eachmarket.PollingLevel.LEVEL3;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cttk.CurrencyPair.SimpleCurrencyPair;

public class LevelPairMapTest {
    private static SimpleCurrencyPair BTC_KRW;
    private static SimpleCurrencyPair ETH_KRW;
    private static SimpleCurrencyPair EOS_KRW;
    private static SimpleCurrencyPair XRP_KRW;
    private static SimpleCurrencyPair LTC_KRW;
    private LevelPairMap map;

    @BeforeClass
    public static void setUpGlobal() {
        BTC_KRW = new SimpleCurrencyPair("btc", "krw");
        ETH_KRW = new SimpleCurrencyPair("eth", "krw");
        EOS_KRW = new SimpleCurrencyPair("eos", "krw");
        XRP_KRW = new SimpleCurrencyPair("xrp", "krw");
        LTC_KRW = new SimpleCurrencyPair("ltc", "krw");
    }

    @Before
    public void setUp() {
        map = new LevelPairMap()
            .setPairs(LEVEL1, asList(BTC_KRW, ETH_KRW))
            .setPairs(LEVEL2, asList(EOS_KRW))
            .setPairs(LEVEL3, asList(XRP_KRW, LTC_KRW));
    }

    @Test
    public void testGetPairs() {
        assertThat(map.getPairs(LEVEL1), hasItems(BTC_KRW, ETH_KRW));
        assertThat(map.getPairs(LEVEL2), hasItems(EOS_KRW));
        assertThat(map.getPairs(LEVEL3), hasItems(XRP_KRW, LTC_KRW));
    }

    @Test
    public void testChangeLevel() {
        assertTrue(map.exists(LEVEL1, BTC_KRW));

        map.changeLevel(BTC_KRW, LEVEL2);

        assertFalse(map.exists(LEVEL1, BTC_KRW));
        assertTrue(map.exists(LEVEL2, BTC_KRW));
        assertFalse(map.exists(LEVEL3, BTC_KRW));
    }

    @Test
    public void testCount() {
        assertThat(map.count(LEVEL1), is(2));
        assertThat(map.count(LEVEL2), is(1));
        assertThat(map.count(LEVEL3), is(2));
    }

    @Test
    public void testCountAll() {
        assertThat(map.countAll(), is(5));
    }
}