package cttk.impl.upbit.response;

import cttk.dto.WithdrawResult;
import lombok.Data;

@Data
public class UpbitWithdrawResponse {
    String type;
    String uuid;
    String currency;
    String txid;
    String state;
    String created_at;
    String done_at;
    String amount;
    String fee;
    String krw_amount;

    public WithdrawResult toWithdrawResult() {
        return new WithdrawResult()
            .setCurrency(currency)
            .setWithdrawId(uuid);
    }
}
