package cttk.impl.bithumb;

import static cttk.impl.bithumb.BithumbUtils.PRICE_INCREMENTS;
import static cttk.impl.bithumb.BithumbUtils.PRICE_RANGES;
import static cttk.impl.util.Objects.nvl;

import java.math.BigDecimal;

import cttk.dto.MarketInfo;
import cttk.impl.MarketInfoOrderVerifier;
import cttk.impl.util.MathUtils;

public class BithumbOrderVerifier
    extends MarketInfoOrderVerifier
{
    public BithumbOrderVerifier(MarketInfo marketInfo) {
        super(marketInfo);
    }

    @Override
    public BigDecimal roundPrice(BigDecimal price) {
        if (price == null) return null;

        final BigDecimal increment = nvl(MathUtils.findIncrement(PRICE_RANGES, PRICE_INCREMENTS, price), getPriceIncrement());

        return increment == null ? price : MathUtils.round(price, increment);
    }
}
