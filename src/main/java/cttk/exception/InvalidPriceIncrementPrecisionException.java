package cttk.exception;

import java.math.BigDecimal;

import cttk.CurrencyPair;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class InvalidPriceIncrementPrecisionException
    extends InvalidOrderException
{
    private static final long serialVersionUID = 1L;

    public InvalidPriceIncrementPrecisionException() {
        super();
    }

    public InvalidPriceIncrementPrecisionException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidPriceIncrementPrecisionException(String message) {
        super(message);
    }

    public InvalidPriceIncrementPrecisionException(Throwable cause) {
        super(cause);
    }

    @Override
    public InvalidPriceIncrementPrecisionException setCxId(String cxId) {
        super.setCxId(cxId);
        return this;
    }

    @Override
    public InvalidPriceIncrementPrecisionException setResponseStatusCode(Integer responseStatusCode) {
        super.setResponseStatusCode(responseStatusCode);
        return this;
    }

    @Override
    public InvalidPriceIncrementPrecisionException setExchangeMessage(String exchangeMessage) {
        super.setExchangeMessage(exchangeMessage);
        return this;
    }

    @Override
    public InvalidPriceIncrementPrecisionException setExchangeErrorCode(String exchangeErrorCode) {
        super.setExchangeErrorCode(exchangeErrorCode);
        return this;
    }

    @Override
    public InvalidPriceIncrementPrecisionException setCurrencyPair(CurrencyPair currencyPair) {
        super.setCurrencyPair(currencyPair);
        return this;
    }

    @Override
    public InvalidPriceIncrementPrecisionException setOrderPrice(BigDecimal orderPrice) {
        super.setOrderPrice(orderPrice);
        return this;
    }

    @Override
    public InvalidPriceIncrementPrecisionException setOrderQuantity(BigDecimal orderQuantity) {
        super.setOrderQuantity(orderQuantity);
        return this;
    }

    @Override
    public InvalidPriceIncrementPrecisionException setBaseCurrency(String baseCurrency) {
        super.setBaseCurrency(baseCurrency);
        return this;
    }

    @Override
    public InvalidPriceIncrementPrecisionException setQuoteCurrency(String quoteCurrency) {
        super.setQuoteCurrency(quoteCurrency);
        return this;
    }
}