package cttk.request;

import java.math.BigDecimal;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class WithdrawToBankRequest {
    String bankCode;
    String accountNo;
    BigDecimal amount;

    public WithdrawToBankRequest setAmount(String amount) {
        this.amount = new BigDecimal(amount);
        return this;
    }

    public WithdrawToBankRequest setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }
}
