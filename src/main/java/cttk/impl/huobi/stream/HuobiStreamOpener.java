package cttk.impl.huobi.stream;

import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.CXStreamOpener;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.request.OpenOrderBookStreamRequest;
import cttk.request.OpenTickerStreamRequest;
import cttk.request.OpenTradeStreamRequest;

public class HuobiStreamOpener
    implements CXStreamOpener
{
    @Override
    public String getCxId() {
        return CXIds.HUOBI;
    }

    @Override
    public CXStream<Ticker> openTickerStream(final OpenTickerStreamRequest request, CXStreamListener<Ticker> listener) {
        return new HuobiWebsocketStream<Ticker, OpenTickerStreamRequest>(
            HuobiChannels.getTickerChannel(request),
            request,
            HuobiWebsocketMessageConverterFactory.createTickerMessageConverter(),
            listener);
    }

    @Override
    public CXStream<OrderBook> openOrderBookStream(final OpenOrderBookStreamRequest request, CXStreamListener<OrderBook> listener) {
        return new HuobiWebsocketStream<OrderBook, OpenOrderBookStreamRequest>(
            HuobiChannels.getOrderBookChannel(request),
            request,
            HuobiWebsocketMessageConverterFactory.createOrderBookMessageConverter(),
            listener);
    }

    @Override
    public CXStream<Trades> openTradeStream(final OpenTradeStreamRequest request, CXStreamListener<Trades> listener)
        throws CTTKException
    {
        return new HuobiWebsocketStream<Trades, OpenTradeStreamRequest>(
            HuobiChannels.getTradesChannel(request),
            request,
            HuobiWebsocketMessageConverterFactory.createTradesMessageConverter(),
            listener);
    }

}
