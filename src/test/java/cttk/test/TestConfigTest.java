package cttk.test;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

public class TestConfigTest {
    @Test
    public void testReadFromClasspath()
        throws IOException
    {
        TestConfig testConfig = TestConfig.readFromClasspathYaml("config/bithumb.yml");

        Assert.assertNotNull(testConfig);
        Assert.assertEquals(testConfig.getValidCurrencyPair().getQuoteCurrency(), "KRW");
    }
}
