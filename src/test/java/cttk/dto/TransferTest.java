package cttk.dto;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TransferTest {
    @Test
    public void testEqualsById() {
        Transfer a = new Transfer().setTid("1");
        Transfer b = new Transfer().setTid("1");

        assertTrue(Transfer.equalsById(a, b));
    }

    @Test
    public void testEqualsWhenBothAreNull() {
        Transfer a = null;
        Transfer b = null;

        assertTrue(Transfer.equalsById(a, b));
    }

    @Test
    public void testEqualsByIdWhenEitherIsNull() {
        Transfer a = null;
        Transfer b = new Transfer().setTid("2");
        assertFalse(Transfer.equalsById(a, b));

        a = new Transfer().setTid("1");
        b = null;
        assertFalse(Transfer.equalsById(a, b));
    }

    @Test
    public void testEqualsByIdWhenIdIsDifferent() {
        Transfer a = new Transfer().setTid("1");
        Transfer b = new Transfer().setTid("2");

        assertFalse(Transfer.equalsById(a, b));
    }
}