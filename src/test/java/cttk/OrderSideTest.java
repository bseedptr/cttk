package cttk;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import cttk.impl.bibox.BiboxUtils;
import cttk.impl.kraken.KrakenUtils;
import cttk.impl.okex.OkexUtils;

public class OrderSideTest {
    @Test
    public void testParseOrderSide() {
        assertEquals(OrderSide.SELL, OrderSide.of("ask"));
        assertEquals(OrderSide.SELL, OrderSide.of("sell"));
        assertEquals(OrderSide.SELL, KrakenUtils.parseOrderSide("s"));
        assertEquals(OrderSide.SELL, BiboxUtils.parseOrderSide("2"));
        assertEquals(OrderSide.SELL, OkexUtils.parseOrderSide("sell_market"));

        assertEquals(OrderSide.BUY, OrderSide.of("bid"));
        assertEquals(OrderSide.BUY, OrderSide.of("buy"));
        assertEquals(OrderSide.BUY, KrakenUtils.parseOrderSide("b"));
        assertEquals(OrderSide.BUY, BiboxUtils.parseOrderSide("1"));
        assertEquals(OrderSide.BUY, OkexUtils.parseOrderSide("buy_market"));

        assertNull(KrakenUtils.parseOrderSide(null));
        assertNull(BiboxUtils.parseOrderSide(null));
        assertNull(OkexUtils.parseOrderSide(null));
        assertNull(KrakenUtils.parseOrderSide("sell_market"));
        assertNull(BiboxUtils.parseOrderSide("s"));
        assertNull(OkexUtils.parseOrderSide("1"));
        assertNull(OrderSide.of("1"));
    }
}
