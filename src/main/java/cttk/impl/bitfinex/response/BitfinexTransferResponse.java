package cttk.impl.bitfinex.response;

import static cttk.impl.bitfinex.BitfinexUtils.stringTimestampToLong;
import static cttk.impl.bitfinex.BitfinexUtils.toStdCurrencySymbol;

import java.math.BigDecimal;

import cttk.TransferStatus;
import cttk.TransferType;
import cttk.dto.Transfer;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;

@Data
public class BitfinexTransferResponse {
    String id;
    String currency;
    String method;
    String type;
    String txid;
    BigDecimal amount;
    String description;
    String address;
    String status;
    String timestamp;
    String timestamp_created;
    BigDecimal fee;

    public Transfer toTransfer() {
        return new Transfer()
            .setCurrency(toStdCurrencySymbol(currency))
            .setChain(method)
            .setType(TransferType.of(type))
            .setTid(id)
            .setTxId(txid)
            .setAmount(amount)
            .setFee(fee.negate())
            .setStatus(status)
            .setStatusType(getStatusType())
            .setAddress(address)
            .setCreatedDateTime(DateTimeUtils.zdtFromEpochSecond(stringTimestampToLong(timestamp_created)))
            .setUpdatedDateTime(DateTimeUtils.zdtFromEpochSecond(stringTimestampToLong(timestamp)))
            .setCompletedDateTime(DateTimeUtils.zdtFromEpochSecond(stringTimestampToLong(timestamp)))
            .setDescription(description);
    }

    private TransferStatus getStatusType() {
        switch (status.toUpperCase()) {
            case "SENDING":
            case "PROCESSING":
            case "PENDING":
            case "POSTPENDING":
            case "USER EMAILED":
            case "APPROVED":
            case "USER APPROVED":
                return TransferStatus.PENDING;
            case "COMPLETED":
                return TransferStatus.SUCCESS;
            case "CANCELED":
            case "PENDING CANCELLATION":
                return TransferStatus.CANCELED;
            case "UNCONFIRMED":
                return TransferStatus.UNCONFIRMED;
        }
        return null;
    }
}
