package cttk.impl.poloniex;

import cttk.CXIds;
import cttk.impl.AbstractOrderVerifierTest;

public class PoloniexOrderVerifierTest
    extends AbstractOrderVerifierTest
{
    @Override
    public String getCxId() {
        return CXIds.POLONIEX;
    }
}
