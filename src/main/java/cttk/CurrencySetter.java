package cttk;

public interface CurrencySetter
    extends CXIdGettable
{
    default <T extends AbstractCurrencyHolder<T>> T setCxIdAndCurrency(T obj, CurrencyGettable currency) {
        return obj == null ? null : setCxId(obj.setCurrency(currency));
    }

    default <T extends AbstractCurrencyHolder<T>> T setCurrency(T obj, CurrencyGettable currency) {
        return obj == null ? null : obj.setCurrency(currency);
    }
}
