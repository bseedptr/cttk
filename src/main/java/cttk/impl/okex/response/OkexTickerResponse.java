package cttk.impl.okex.response;

import java.math.BigDecimal;

import cttk.dto.Ticker;
import cttk.impl.okex.OkexUtilsV3;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class OkexTickerResponse
    extends OkexTicker
{
}

@Data
class OkexTicker {
    String instrument_id;
    BigDecimal last;
    BigDecimal best_bid;
    BigDecimal best_ask;
    BigDecimal open_24h;
    BigDecimal high_24h;
    BigDecimal low_24h;
    BigDecimal base_volume_24h;
    BigDecimal quote_volume_24h;
    String timestamp;

    public Ticker toTicker() {
        return new Ticker()
            .setCurrencyPair(OkexUtilsV3.toStdMarketSymbolV3(instrument_id))
            .setDateTime(DateTimeUtils.parseZdt(timestamp))
            .setOpenPrice(open_24h)
            .setLastPrice(last)
            .setLowPrice(low_24h)
            .setHighPrice(high_24h)
            .setVolume(base_volume_24h)
            .setQuoteVolume(quote_volume_24h)
            .setHighestBid(best_bid)
            .setLowestAsk(best_ask);
    }
}