package cttk.impl.bibox.stream.response;

import java.math.BigDecimal;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.dto.Balance;
import cttk.dto.Balances;
import cttk.impl.util.MathUtils;
import lombok.Data;

public class BiboxWebsocketUserDataAssets {
    public Map<String, BiboxWebsocketUserDataAsset> normal;
    public Map<String, Map<String, BiboxWebsocketUserDataAsset>> credit;

    public Balances toBalances() {
        if (normal == null) return null;

        return new Balances()
            .setBalances(normal
                .entrySet()
                .stream()
                .map(it -> it.getValue().toBalance(it.getKey()))
                .collect(Collectors.toList()));
    }
}

@Data
class BiboxWebsocketUserDataAsset {
    @JsonProperty("balance")
    BigDecimal available;
    BigDecimal freeze;

    public Balance toBalance(String currency) {
        return new Balance()
            .setCurrency(currency)
            .setAvailable(available)
            .setInUse(freeze)
            .setTotal(MathUtils.add(available, freeze));
    }
}

