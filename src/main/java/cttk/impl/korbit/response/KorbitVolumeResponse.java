package cttk.impl.korbit.response;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import cttk.TradingFeeProvider;
import cttk.dto.TradingFee;
import cttk.impl.feeprovider.ByBaseTradingFeeProvider;
import lombok.Data;

@Data
public class KorbitVolumeResponse {
    Long timestamp;
    BigDecimal total_volume;

    @JsonIgnore
    final Map<String, KorbitTradingFee> map = new HashMap<>();

    @JsonAnyGetter
    public Map<String, KorbitTradingFee> any() {
        return this.map;
    }

    @JsonAnySetter
    public void set(String name, KorbitTradingFee value) {
        this.map.put(name, value);
    }

    public TradingFeeProvider getTradingFeeProvider() {
        final Map<String, TradingFee> feeMap = map.entrySet().stream()
            .collect(Collectors.toMap(
                e -> e.getKey().substring(0, e.getKey().indexOf('_')).toUpperCase(),
                e -> e.getValue().getTradingFee()));

        return new ByBaseTradingFeeProvider()
            .setFeeMap(feeMap);
    }
}

@Data
class KorbitTradingFee {
    BigDecimal taker_fee;
    BigDecimal maker_fee;
    BigDecimal volume;

    public TradingFee getTradingFee() {
        return new TradingFee().setFees(taker_fee, maker_fee);
    }
}
