package cttk.impl.upbit.stream;

import cttk.CXStreamListener;
import cttk.PrivateCXClient;
import cttk.UserData;
import cttk.impl.stream.PollingUserDataStream;
import cttk.request.OpenUserDataStreamRequest;

class UpbitPollingUserDataStream
    extends PollingUserDataStream
{
    UpbitPollingUserDataStream(final PrivateCXClient client, final OpenUserDataStreamRequest request, final CXStreamListener<UserData> listener) {
        this.pollingUserDataThread = new UpbitPollingUserDataThread(this, client, request, listener);
        open();
    }
}
