package cttk.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

public class PropertyUtils {
    final Yaml yaml = new Yaml();
    final String baseDir;

    public PropertyUtils() {
        this(Paths.get(getUserHome(), ".cttk/props").toString());
    }

    public PropertyUtils(String baseDir) {
        this.baseDir = baseDir;
    }

    @SuppressWarnings("unchecked")
    public Map<String, String> getProperties(final String cxId) {
        try (InputStream is = new FileInputStream(Paths.get(baseDir, cxId.toLowerCase() + ".yml").toFile())) {
            return yaml.loadAs(is, Map.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String getUserHome() {
        return System.getProperty("user.home");
    }
}
