package cttk.exception;

import cttk.CurrencyGettable;
import cttk.CurrencyPair;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class CXAPIRequestException
    extends CTTKException
{
    private static final long serialVersionUID = 1L;

    protected Integer responseStatusCode;
    protected String exchangeMessage;
    protected String exchangeErrorCode;

    public CXAPIRequestException() {
        super();
    }

    public CXAPIRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public CXAPIRequestException(String message) {
        super(message);
    }

    public CXAPIRequestException(Throwable cause) {
        super(cause);
    }

    @Override
    public CXAPIRequestException setCxId(String cxId) {
        super.setCxId(cxId);
        return this;
    }

    @Override
    public String toString() {
        return super.toString()
            + "Exchange Message = " + exchangeMessage
            + ", Response Status = " + responseStatusCode;
    }

    public static CXAPIRequestException createByRequestObj(Object requestObj) {
        return createByRequestObj(requestObj, null);
    }

    public static CXAPIRequestException createByRequestObj(Object requestObj, String message) {
        if (requestObj instanceof CurrencyPair) {
            CurrencyPair request = (CurrencyPair) requestObj;
            return new UnsupportedCurrencyPairException(message)
                .setCurrencyPair(request);
        }
        else if (requestObj instanceof CurrencyGettable) {
            CurrencyGettable request = (CurrencyGettable) requestObj;
            return new UnsupportedCurrencyException(message)
                .setCurrency(request.getCurrency());
        }
        else {
            return new CXAPIRequestException(message);
        }
    }
}
