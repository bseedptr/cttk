package cttk.impl.kraken.response;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import cttk.dto.MarketInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class KrakenAssetPairsResponse
    extends KrakenResponse<Map<String, KrakenAssetPair>>
{
}

@Data
class KrakenAssetPair {
    String altname;
    String aclass_base;
    String base;
    String aclass_quote;
    String quote;
    String lot;
    Integer pair_decimals;
    Integer lot_decimals;
    Integer lot_multiplier;
    List<BigDecimal> leverage_buy;
    List<BigDecimal> leverage_sell;
    List<List<BigDecimal>> fees;
    List<List<BigDecimal>> fees_maker;
    BigDecimal margin_call;
    BigDecimal margin_stop;

    public MarketInfo toMarketInfo() {
        return new MarketInfo()
            .setPriceIncrement(BigDecimal.ONE.movePointLeft(pair_decimals))
            .setQuantityIncrement(BigDecimal.ONE.movePointLeft(lot_decimals));
    }
}