package cttk.impl.poloniex.response;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.PricingType;
import cttk.dto.Trade;
import cttk.dto.Trades;
import cttk.dto.UserOrder;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;
import cttk.impl.poloniex.PoloniexUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class PoloniexTradesResponse
    extends ArrayList<PoloniexTrade>
{
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public Trades toTrades(CurrencyPair currencyPair) {
        return new Trades()
            .setCurrencyPair(currencyPair)
            .setTrades(toTradeList(this));
    }

    static List<Trade> toTradeList(List<PoloniexTrade> poloniexList) {
        if (poloniexList == null) return null;
        return poloniexList.stream().map(PoloniexTrade::toTrade).collect(Collectors.toList());
    }

    public UserTrades toUserTrades(final CurrencyPair currencyPair) {
        return new UserTrades()
            .setTrades(getUserTradeList(currencyPair));
    }

    public List<UserTrade> getUserTradeList(final CurrencyPair currencyPair) {
        return this.stream().map(i -> i.toUserTrade(currencyPair)).collect(Collectors.toList());
    }

    public UserOrder toUserOrder(String oid, PricingType pricingType, OrderStatus orderStatus) {
        List<UserTrade> list = getUserTradeList(null);

        BigDecimal total = BigDecimal.ZERO;
        BigDecimal amount = BigDecimal.ZERO;
        BigDecimal fee = BigDecimal.ZERO;
        // price should be averaged
        BigDecimal price = BigDecimal.ZERO;
        ZonedDateTime completedDate = LocalDateTime.parse("1970-01-01 00:00:00",
            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")).atZone(ZoneOffset.UTC);

        int count = 0;

        for (UserTrade trade : list) {
            trade.setOid(oid);
            total = MathUtils.add(total, trade.getTotal());
            amount = MathUtils.add(amount, trade.getQuantity());
            price = MathUtils.add(price, trade.getPrice());
            fee = MathUtils.add(fee, trade.getFee());
            if (trade.getDateTime().isAfter(completedDate)) {
                completedDate = trade.getDateTime();
            }
            count++;
        }

        UserTrade tradeOne = list.get(0);

        // Avg of price
        price = MathUtils.divide(price, BigDecimal.valueOf(count));

        return new UserOrder()
            .setOid(oid)
            .setBaseCurrency(tradeOne.getBaseCurrency())
            .setQuoteCurrency(tradeOne.getQuoteCurrency())
            .setSide(tradeOne.getSide())
            .setQuantity(amount)
            .setStatusType(orderStatus)
            .setTotal(total)
            .setPricingType(pricingType)
            .setFee(fee)
            .setFeeRate(tradeOne.getFeeRate())
            .setAvgPrice(price)
            .setCompletedDateTime(completedDate)
            .setFilledQuantity(amount)
            .setTrades(list)
            .setFeeCurrency(tradeOne.getFeeCurrency())
            .setFeeRate(tradeOne.getFeeRate());

    }
}

@Data
class PoloniexTrade {
    BigInteger globalTradeID;
    String tradeID;
    String orderNumber;
    String date;
    String type;    // buy, sell
    BigDecimal rate;
    BigDecimal amount;
    BigDecimal total;
    BigDecimal fee;
    String category;
    @JsonProperty("currencyPair")
    String currencyPairStr;

    public Trade toTrade() {
        return new Trade()
            .setSno(globalTradeID)
            .setGtid(String.valueOf(globalTradeID))
            .setTid(tradeID)
            .setDateTime(parseZdt(date))
            .setSide(getOrderSide())
            .setQuantity(amount)
            .setPrice(rate)
            .setTotal(total);
    }

    public UserTrade toUserTrade(CurrencyPair currencyPair) {
        currencyPair = currencyPair == null ? getCurrencyPairObject() : currencyPair;
        OrderSide side = getOrderSide();
        String feeCurrency = PoloniexUtils.guessFeeCurrency(side, currencyPair);
        return new UserTrade()
            .setCurrencyPair(currencyPair)
            .setGtid(globalTradeID.longValue())
            .setTid(tradeID)
            .setOid(orderNumber)
            .setDateTime(parseZdt(date))
            .setSide(side)
            .setPrice(rate)
            .setQuantity(amount)
            .setTotal(total)
            .setFee(PoloniexUtils.calculateFee(side, fee, rate, amount))
            .setFeeRate(fee)
            .setFeeCurrency(feeCurrency)
            .setCategory(category);
    }

    public CurrencyPair getCurrencyPairObject() {
        String[] currencyPairArr = currencyPairStr.split("_");
        return CurrencyPair.of(PoloniexUtils.toStdCurrencySymbol(currencyPairArr[0]), PoloniexUtils.toStdCurrencySymbol(currencyPairArr[1]));
    }

    private OrderSide getOrderSide() {
        return OrderSide.of(type);
    }

    private static final DateTimeFormatter DTF = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private static ZonedDateTime parseZdt(String str) {
        if (str == null || str.trim().isEmpty()) return null;
        return LocalDateTime.parse(str, DTF).atZone(ZoneOffset.UTC);
    }
}
