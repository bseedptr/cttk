package cttk.impl.coinone;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import cttk.CXIds;
import cttk.CurrencyPair.SimpleCurrencyPair;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.AbstractPublicCXClientTest;
import cttk.impl.util.ExNotRaisedException;
import cttk.request.GetOrderBookRequest;
import cttk.request.GetRecentTradesRequest;
import cttk.request.GetTickerRequest;

public class CoinonePublicClientTest
    extends AbstractPublicCXClientTest
{
    // Only KRW market exists in Coinone
    private final SimpleCurrencyPair invalidCurrencyPair = new SimpleCurrencyPair("AJSKLF", "KRW");

    @Override
    public String getCxId() {
        return CXIds.COINONE;
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetTradeHistory()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetTradeHistory();
    }

    @Test(expected = ExNotRaisedException.class)
    // Coinone sends default 'BTC' orderbook if they receive invalid currency.
    @Override
    public void testGetOrderBookWrongCurrency()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();

        final GetOrderBookRequest request = new GetOrderBookRequest()
            .setCurrencyPair(invalidCurrencyPair)
            .setCount(50);
        try {
            client.getOrderBook(request);
        } catch (UnsupportedCurrencyPairException e) {
            assertEquals(invalidCurrencyPair.getBaseCurrency(), e.getBaseCurrency());
            assertEquals(invalidCurrencyPair.getQuoteCurrency(), e.getQuoteCurrency());
            assertEquals(this.getCxId(), e.getCxId());
            return;
        }

        throw new ExNotRaisedException(raiseExceptionMsg);
    }

    @Test(expected = ExNotRaisedException.class)
    // Coinone sends default 'BTC' recent trades if they receive invalid currency.
    public void testGetRecentTradesWithUnsupportedQuoteCurrency()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();

        final GetRecentTradesRequest request = new GetRecentTradesRequest()
            .setCurrencyPair(invalidCurrencyPair)
            .setCount(100);

        final Trades trades = client.getRecentTrades(request);

        assertNotNull(trades);

        throw new ExNotRaisedException(raiseExceptionMsg);
    }

    @Test(expected = ExNotRaisedException.class)
    // Coinone sends default all tickers if they receive invalid currency.
    public void testGetTickerWrongCurrency()
        throws CTTKException
    {
        threadSleep();
        final GetTickerRequest request = new GetTickerRequest().setCurrencyPair(invalidCurrencyPair);
        try {
            client.getTicker(request);
        } catch (UnsupportedCurrencyPairException e) {
            assertEquals(e.getBaseCurrency(), invalidCurrencyPair.getBaseCurrency());
            assertEquals(e.getQuoteCurrency(), invalidCurrencyPair.getQuoteCurrency());
            assertEquals(e.getCxId(), this.getCxId());
            return;
        }
        throw new ExNotRaisedException(raiseExceptionMsg);

    }
}