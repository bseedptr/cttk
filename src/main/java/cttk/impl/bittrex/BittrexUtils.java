package cttk.impl.bittrex;

import static cttk.util.StringUtils.toUpper;

import cttk.CurrencyPair;

public class BittrexUtils {
    public static String getMarketSymbol(CurrencyPair currencyPair) {
        return currencyPair == null ? null
            : toUpper(currencyPair.getQuoteCurrency()) + "-" + toUpper(currencyPair.getBaseCurrency());
    }

    public static CurrencyPair parseMarketSymbol(String marketSymbol) {
        if (marketSymbol == null || marketSymbol.trim().isEmpty()) return null;
        int idx = marketSymbol.indexOf("-");
        return CurrencyPair.of(marketSymbol.substring(idx + 1), marketSymbol.substring(0, idx));
    }
}
