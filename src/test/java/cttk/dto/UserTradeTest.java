package cttk.dto;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class UserTradeTest {
    @Test
    public void testEqualsById() {
        UserTrade a = new UserTrade().setTid("1");
        UserTrade b = new UserTrade().setTid("1");

        assertTrue(UserTrade.equalsById(a, b));
    }

    @Test
    public void testEqualsWhenBothAreNull() {
        UserTrade a = null;
        UserTrade b = null;

        assertTrue(UserTrade.equalsById(a, b));
    }

    @Test
    public void testEqualsByIdWhenEitherIsNull() {
        UserTrade a = null;
        UserTrade b = new UserTrade().setTid("2");
        assertFalse(UserTrade.equalsById(a, b));

        a = new UserTrade().setTid("1");
        b = null;
        assertFalse(UserTrade.equalsById(a, b));
    }

    @Test
    public void testEqualsByIdWhenIdIsDifferent() {
        UserTrade a = new UserTrade().setTid("1");
        UserTrade b = new UserTrade().setTid("2");

        assertFalse(UserTrade.equalsById(a, b));
    }
}