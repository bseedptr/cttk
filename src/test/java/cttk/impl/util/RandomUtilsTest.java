package cttk.impl.util;

import org.junit.Assert;
import org.junit.Test;

public class RandomUtilsTest {
    @Test
    public void testGenerateRandomInt() {
        int num = RandomUtils.generateRandomInt();

        Assert.assertTrue(Integer.MAX_VALUE >= num);
        Assert.assertTrue(Integer.MIN_VALUE <= num);
    }
}