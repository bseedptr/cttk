package cttk.impl.bibox;

import static cttk.TransferTypeParam.ALL;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import cttk.CXIds;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.dto.UserOrders;
import cttk.dto.UserOrders.Category;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.AbstractPrivateCXClientDataTest;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserOrderHistoryRequest;

public class BiboxPrivateClientDataTest
    extends AbstractPrivateCXClientDataTest
{
    @AfterClass
    public static void cleanup() {
        cleanup(BiboxPrivateClientDataTest.class);
    }

    @Override
    public String getCxId() {
        return CXIds.BIBOX;
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetBankAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetBankAccount();
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetWallet()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetWallet();
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetAllWallets()
        throws CTTKException
    {
        super.testGetAllWallets();
    }

    @Test
    @Override
    public void testGetTransfers()
        throws UnsupportedCurrencyPairException, CTTKException
    {
        threadSleep();

        final GetTransfersRequest request = new GetTransfersRequest()
            .setCurrency(getTransferTestCurrency())
            .setTransferTypeParam(ALL);
        final Transfers transfers = client.getTransfers(request);

        logger.debug("{}", transfers);

        Assert.assertNotNull(transfers);
        Assert.assertTrue(transfers.isNotEmpty());
        for (Transfer transfer : transfers.getTransfers()) {
            Assert.assertNotNull(transfer);
            Assert.assertNotNull(transfer.getTid());
            Assert.assertNotNull(transfer.getUserId());
            Assert.assertNotNull(transfer.getCurrency());
            Assert.assertNotNull(transfer.getType());
            Assert.assertNotNull(transfer.getAddress());
            Assert.assertNotNull(transfer.getAmount());
            Assert.assertNotNull(transfer.getCreatedDateTime());
            Assert.assertNotNull(transfer.getStatus());
            Assert.assertThat("Non null value is required to generate unique tId.", transfers.first().getTid(), not(containsString("null")));
        }
    }

    @Test
    public void testGetUserOrderHistoryCategory()
        throws CTTKException
    {
        threadSleep();

        final GetUserOrderHistoryRequest request = new GetUserOrderHistoryRequest()
            .setCurrencyPair(getValidCurrencyPair());
        final UserOrders userOrders = client.getUserOrderHistory(request);

        logger.debug("{}", userOrders);

        Assert.assertEquals(userOrders.getCategory(), Category.ALL);
    }
}
