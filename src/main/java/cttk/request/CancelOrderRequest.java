package cttk.request;

import java.math.BigDecimal;

import cttk.AbstractCurrencyPair;
import cttk.OrderSide;
import cttk.dto.UserOrder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class CancelOrderRequest
    extends AbstractCurrencyPair<CancelOrderRequest>
{
    String orderId;
    String clientOrderId;   // should be used for HitBTC
    OrderSide side;

    //coinone
    BigDecimal price; //KRW price.
    BigDecimal quantity; //BTC/BCH/ETH/ETC/XRP/QTUM/IOTA/LTC/BTG quantity.

    public static CancelOrderRequest from(UserOrder userOrder) {
        return userOrder == null ? null : new CancelOrderRequest()
            .setCurrencyPair(userOrder)
            .setOrderId(userOrder.getOid())
            .setClientOrderId(userOrder.getClientOrderId())
            .setSide(userOrder.getSide())
            .setQuantity(userOrder.getQuantity())
            .setPrice(userOrder.getPrice());
    }
}
