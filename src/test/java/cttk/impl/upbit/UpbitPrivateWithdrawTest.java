package cttk.impl.upbit;

import org.junit.Test;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.AbstractPrivateCXClientWithdrawTest;
import cttk.request.WithdrawToBankRequest;

public class UpbitPrivateWithdrawTest
    extends AbstractPrivateCXClientWithdrawTest
{
    @Override
    public String getCxId() {
        return CXIds.UPBIT;
    }

    @Test
    @Override
    public void testWithdrawToBankAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        client.withdraw(new WithdrawToBankRequest().setAmount("5000"));
    }

    @Test
    @Override
    public void testWithdrawBTCToWallet()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // TODO: Decide testPair address
    }
}
