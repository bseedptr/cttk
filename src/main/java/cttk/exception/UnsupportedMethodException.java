package cttk.exception;

public class UnsupportedMethodException
    extends CTTKException
{
    private static final long serialVersionUID = 1L;

    public UnsupportedMethodException() {
        super();
    }

    public UnsupportedMethodException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsupportedMethodException(String message) {
        super(message);
    }

    public UnsupportedMethodException(Throwable cause) {
        super(cause);
    }

    @Override
    public UnsupportedMethodException setCxId(String cxId) {
        super.setCxId(cxId);
        return this;
    }
}
