package cttk.request;

import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cttk.AbstractCurrencyHolder;
import cttk.DurationGettable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class GetTransfersDeltaRequest
    extends AbstractCurrencyHolder<GetTransfersDeltaRequest>
    implements DurationGettable
{
    private String fromId;
    private ZonedDateTime startDateTime;
    private ZonedDateTime endDateTime;
    private Long sleepTimeMillis;

    @JsonIgnore
    public boolean hasFromId() {
        return fromId != null;
    }
}
