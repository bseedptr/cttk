package cttk.impl.hitbtc.response;

import static cttk.impl.hitbtc.HitbtcUtils.toStdCurrencySymbol;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import cttk.TransferStatus;
import cttk.TransferType;
import cttk.dto.Transfer;
import lombok.Data;

@Data
public class HitbtcTransferResponse {
    String id;
    Long index;
    String currency;
    BigDecimal amount;
    BigDecimal fee;
    String networkFee;
    String address;
    String paymentId;
    String hash;
    String status;  // pending, failed, success
    String type;    // payout, payin, deposit, withdraw, bankToExchange, exchangeToBank
    String createdAt;
    String updatedAt;

    public Transfer toTransfer() {
        return new Transfer()
            .setCurrency(toStdCurrencySymbol(currency))
            .setType(TransferType.of(type))
            .setTid(id)
            .setTxId(hash)
            .setIndex(index)
            .setAmount(amount)
            .setFee(fee)
            .setStatus(status)
            .setStatusType(getStatusType())
            .setAddress(address)
            .setAddressTag(paymentId)
            .setCreatedDateTime(ZonedDateTime.parse(createdAt))
            .setUpdatedDateTime(ZonedDateTime.parse(updatedAt));
    }

    private TransferStatus getStatusType() {
        switch (status.toUpperCase()) {
            case "PENDING":
                return TransferStatus.PENDING;
            case "SUCCESS":
                return TransferStatus.SUCCESS;
            case "FAILED":
                return TransferStatus.FAILED;
        }
        return null;
    }
}