package cttk.impl.bibox.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BiboxUserCancelTradeResponse
    extends BiboxResultResponse<String>
{
}
