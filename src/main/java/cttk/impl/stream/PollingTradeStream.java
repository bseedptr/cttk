package cttk.impl.stream;

import java.math.BigInteger;

import cttk.CXIds;
import cttk.CXStreamConfig;
import cttk.CXStreamListener;
import cttk.PublicCXClient;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.util.ThrowableFunction;
import cttk.request.GetRecentTradesRequest;

public class PollingTradeStream
    extends PollingCXStream<Trades>
{
    public PollingTradeStream(final PublicCXClient client, final CXStreamListener<Trades> listener, final GetRecentTradesRequest request) {
        super(client, createTradesGetter(null, request), listener);
    }

    public PollingTradeStream(final PublicCXClient client, final CXStreamConfig config, final CXStreamListener<Trades> listener, final GetRecentTradesRequest request) {
        super(client, createTradesGetter(config, request), config, listener);
    }

    private static final ThrowableFunction<PublicCXClient, Trades> createTradesGetter(final CXStreamConfig streamConfig, final GetRecentTradesRequest request) {
        return new ThrowableFunction<PublicCXClient, Trades>() {

            final CXStreamConfig config = streamConfig;
            final GetRecentTradesRequest req = request;

            BigInteger lastSno = BigInteger.ZERO;
            String period = "day"; // for first call

            @Override
            public Trades apply(PublicCXClient client)
                throws CTTKException, UnsupportedCurrencyPairException
            {
                req.setLastTradeId(lastSno.longValue());
                req.setTime(period);
                Trades data = client.getRecentTrades(req);

                if (data != null) {
                    data.removeIfSnoLessThanOrEqual(lastSno);

                    // coinone
                    if (CXIds.COINONE.equals(client.getCxId())) {
                        period = "hour";
                    }
                    // korbit
                    else if (CXIds.KORBIT.equals(client.getCxId())) {
                        if (config != null && config.getIntervalInMilli() >= 60 * 1000) {
                            period = "hour";
                        } else {
                            period = "minute";
                        }
                    }
                    // kraken
                    if (data.hasLast()) {
                        lastSno = BigInteger.valueOf(data.getLast());
                    }
                    // bithumb, okex and poloniex
                    else if (data.hasTrades()) {
                        lastSno = data.getMaxSno();
                    }
                    // TODO should handle others cases
                    else {}
                } else {
                    return null;
                }
                return data.hasTrades() ? data : null;
            }
        };
    }
}
