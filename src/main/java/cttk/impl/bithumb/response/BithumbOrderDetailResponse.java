package cttk.impl.bithumb.response;

import static cttk.impl.util.DateTimeUtils.zdtFromEpochSecond;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import cttk.OrderSide;
import cttk.dto.UserTrade;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BithumbOrderDetailResponse
    extends BithumbDataResponse<ArrayList<BithumbOrderUserTrade>>
{
    public List<UserTrade> getUserTradeList() {
        if (data == null) return null;
        return data.stream().map(i -> i.toUserTrade()).collect(Collectors.toList());
    }
}

@Data
class BithumbOrderUserTrade {
    String transaction_date;
    String type;
    String order_currency;
    String payment_currency;
    BigDecimal units_traded;
    BigDecimal price;
    BigDecimal fee;
    BigDecimal total;

    public UserTrade toUserTrade() {
        return new UserTrade()
            .setDateTime(zdtFromEpochSecond(Long.parseLong(transaction_date)))
            .setSide(OrderSide.of(type))
            .setBaseCurrency(order_currency)
            .setQuoteCurrency(payment_currency)
            .setQuantity(units_traded)
            .setPrice(price)
            .setFee(fee)
            .setTotal(total);
    }
}
