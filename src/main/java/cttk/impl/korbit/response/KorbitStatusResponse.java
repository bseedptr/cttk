package cttk.impl.korbit.response;

import cttk.dto.WithdrawResult;
import lombok.Data;

@Data
public class KorbitStatusResponse {

    public static final String VALID_STATUS = "success";

    String orderId;
    String transferId;
    String currencyPair;
    String status;

    public WithdrawResult getWithdrawResult() {
        return new WithdrawResult().setWithdrawId(transferId);
    }
}
