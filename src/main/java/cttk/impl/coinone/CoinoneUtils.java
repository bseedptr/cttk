package cttk.impl.coinone;

import static cttk.OrderSide.SELL;

import java.math.BigDecimal;

import cttk.CurrencyPair;
import cttk.OrderSide;

public class CoinoneUtils {
    public static final BigDecimal[] PRICE_RANGES = {
        new BigDecimal("3500000"),
        new BigDecimal("1000000"),
        new BigDecimal("500000"),
        new BigDecimal("50000"),
        new BigDecimal("1000"),
        new BigDecimal("100"),
        new BigDecimal("1"),
    };

    public static final BigDecimal[] PRICE_INCREMENTS = {
        new BigDecimal("1000"),
        new BigDecimal("500"),
        new BigDecimal("100"),
        new BigDecimal("50"),
        new BigDecimal("10"),
        new BigDecimal("1"),
        new BigDecimal("0.1"),
        new BigDecimal("0.01")
    };

    /**
     * Convert the given currency symbol to the standardized symbol
     * @param currency the currency symbol
     * @return standard currency symbol
     */
    public static String toStdCurrencySymbol(String currency) {
        return currency == null ? null : currency.trim().toUpperCase();
    }

    public static String toString(OrderSide orderSide) {
        switch (orderSide) {
            case BUY:
                return "buy";
            case SELL:
                return "sell";
            default:
                return null;
        }
    }

    public static int toIsAsk(OrderSide orderSide) {
        switch (orderSide) {
            case SELL:
                return 1;
            case BUY:
            default:
                return 0;
        }
    }

    public static String guessFeeCurrency(OrderSide side, CurrencyPair pair) {
        return side == SELL ? "KRW" : pair.getBaseCurrency();
    }
}
