package cttk.dto;

import java.time.ZonedDateTime;

import cttk.AbstractCXIdHolder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * https://bseedptr.atlassian.net/wiki/spaces/CTTK/pages/21692471/API
 * @author	dj.lee
 * @since	2018-03-22
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class UserAccount
    extends AbstractCXIdHolder<UserAccount>
{
    String aid;
    String state;
    String type;
    ZonedDateTime createdDateTime;
    ZonedDateTime updatedDateTime;

    String email;
    String name;
    String phone;
    String birthday;
    String gender;

    // Korbit
    Boolean notifyTrades;
    Boolean notifyDepositWithdrawal;

    String userLevel;
    ZonedDateTime nameCheckedDateTime;

    // coinone 
    Boolean isMobileAuthenticated;
    Boolean isEmailAuthenticated;
    String securityLevel;

    Integer makerCommission;
    Integer takerCommission;
    Integer buyerCommission;
    Integer sellerCommission;

    Boolean canTrade;
    Boolean canWithdraw;
    Boolean canDeposit;
}
