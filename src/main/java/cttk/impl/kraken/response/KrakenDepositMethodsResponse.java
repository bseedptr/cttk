package cttk.impl.kraken.response;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class KrakenDepositMethodsResponse
    extends KrakenResponse<List<KrakenDepositMethod>>
{

}