package cttk;

import cttk.exception.CTTKException;
import cttk.request.OpenUserDataStreamRequest;
import cttk.util.ReopenCXStreamListener;
import cttk.util.ReopenPolicy;

public interface UserDataStreamOpener {
    CXStream<UserData> openUserDataStream(OpenUserDataStreamRequest request, CXStreamListener<UserData> listener)
        throws CTTKException;

    default CXStream<UserData> openUserDataStream(OpenUserDataStreamRequest request, CXStreamListener<UserData> listener, ReopenPolicy reopenPolicy)
        throws CTTKException
    {
        return openUserDataStream(request, ReopenCXStreamListener.create(listener, reopenPolicy));
    }
}
