package cttk.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import cttk.dto.Balance;
import cttk.dto.Balances;
import cttk.impl.util.MathUtils;

public class BalancesDiff {
    final List<Balance> added;
    final List<Balance> removed;
    final List<Balance> valueChanged;

    private BalancesDiff(List<Balance> added, List<Balance> removed, List<Balance> valueChanged) {
        super();
        this.added = added;
        this.removed = removed;
        this.valueChanged = valueChanged;
    }

    public boolean isChanged() {
        return isAdded() || isRemoved() || isValueChanged();
    }

    public boolean isAdded() {
        return !added.isEmpty();
    }

    public boolean isRemoved() {
        return !removed.isEmpty();
    }

    public boolean isValueChanged() {
        return !valueChanged.isEmpty();
    }

    public static BalancesDiff diff(Balances prev, Balances curr) {
        return diff(prev == null ? null : prev.getBalances(), curr == null ? null : curr.getBalances());
    }

    public static BalancesDiff diff(List<Balance> prev, List<Balance> curr) {
        final Map<String, Balance> prevMap = toMap(prev);
        final Map<String, Balance> currMap = toMap(curr);

        final List<Balance> added = new ArrayList<>();
        final List<Balance> removed = new ArrayList<>();
        final List<Balance> valueChanged = new ArrayList<>();

        prevMap.forEach((k, v) -> {
            if (!currMap.containsKey(k)) removed.add(v);
        });

        currMap.forEach((k, v) -> {
            if (!prevMap.containsKey(k)) {
                added.add(v);
            } else if (isDiff(v, prevMap.get(k))) {
                valueChanged.add(v);
            }
        });

        return new BalancesDiff(added, removed, valueChanged);
    }

    private static boolean isDiff(Balance o1, Balance o2) {
        return !MathUtils.equals(o1.getTotal(), o2.getTotal())
            || !MathUtils.equals(o1.getAvailable(), o2.getAvailable())
            || !MathUtils.equals(o1.getInUse(), o2.getInUse());
    }

    private static Map<String, Balance> toMap(List<Balance> prev) {
        return prev == null
            ? Collections.emptyMap()
            : prev.stream().collect(Collectors.toMap(i -> i.getCurrency(), i -> i));
    }
}
