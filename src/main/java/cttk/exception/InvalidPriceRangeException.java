package cttk.exception;

import java.math.BigDecimal;

import cttk.CurrencyPair;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class InvalidPriceRangeException
    extends InvalidOrderException
{
    private static final long serialVersionUID = 1L;

    public InvalidPriceRangeException() {
        super();
    }

    public InvalidPriceRangeException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidPriceRangeException(String message) {
        super(message);
    }

    public InvalidPriceRangeException(Throwable cause) {
        super(cause);
    }

    @Override
    public InvalidPriceRangeException setCxId(String cxId) {
        super.setCxId(cxId);
        return this;
    }

    @Override
    public InvalidPriceRangeException setResponseStatusCode(Integer responseStatusCode) {
        super.setResponseStatusCode(responseStatusCode);
        return this;
    }

    @Override
    public InvalidPriceRangeException setExchangeMessage(String exchangeMessage) {
        super.setExchangeMessage(exchangeMessage);
        return this;
    }

    @Override
    public InvalidPriceRangeException setExchangeErrorCode(String exchangeErrorCode) {
        super.setExchangeErrorCode(exchangeErrorCode);
        return this;
    }

    @Override
    public InvalidPriceRangeException setCurrencyPair(CurrencyPair currencyPair) {
        super.setCurrencyPair(currencyPair);
        return this;
    }

    @Override
    public InvalidPriceRangeException setOrderPrice(BigDecimal orderPrice) {
        super.setOrderPrice(orderPrice);
        return this;
    }

    @Override
    public InvalidPriceRangeException setOrderQuantity(BigDecimal orderQuantity) {
        super.setOrderQuantity(orderQuantity);
        return this;
    }

    @Override
    public InvalidPriceRangeException setBaseCurrency(String baseCurrency) {
        super.setBaseCurrency(baseCurrency);
        return this;
    }

    @Override
    public InvalidPriceRangeException setQuoteCurrency(String quoteCurrency) {
        super.setQuoteCurrency(quoteCurrency);
        return this;
    }
}