package cttk.impl.poloniex.response;

import java.util.HashMap;
import java.util.Set;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
public class PoloniexCurrenciesResponse
    extends HashMap<String, PoloniexCurrency>
{
    private static final long serialVersionUID = 1L;

    public Set<String> toSymbols() {
        return this.keySet();
    }
}

@Data
@Accessors(chain = true)
class PoloniexCurrency {
    Integer id;
    String name;
    String txFee;
    Integer minConf;
    String depositAddress;
    Integer disabled;
    Integer delisted;
    Integer frozen;
}
