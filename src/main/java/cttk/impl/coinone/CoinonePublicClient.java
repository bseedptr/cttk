package cttk.impl.coinone;

import static cttk.impl.util.Objects.nvl;

import java.math.BigDecimal;

import cttk.PublicCXClient;
import cttk.dto.MarketInfo;
import cttk.dto.MarketInfos;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Tickers;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.coinone.response.CoinoneAllTickersResponse;
import cttk.impl.coinone.response.CoinoneOrderBookResponse;
import cttk.impl.coinone.response.CoinoneTickerResponse;
import cttk.impl.coinone.response.CoinoneTradesResponse;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ParamsBuilder;
import cttk.request.GetOrderBookRequest;
import cttk.request.GetRecentTradesRequest;
import cttk.request.GetTickerRequest;
import cttk.request.GetTradeHistoryRequest;

public class CoinonePublicClient
    extends AbstractCoinoneClient
    implements PublicCXClient
{
    public CoinonePublicClient() {
        super();
    }

    @Override
    public MarketInfos getMarketInfos()
        throws CTTKException
    {
        final HttpResponse response = requestGet("/ticker/", ParamsBuilder.buildIfNotNull("currency", "all"));

        checkResponseError(null, response);

        final MarketInfos marketInfos = convert(response, CoinoneAllTickersResponse.class, (i -> i.toMarketInfos()));

        if (marketInfos.getMarketInfos() != null) {
            marketInfos.getMarketInfos().forEach(CoinonePublicClient::fillMarketInfo);
        }

        return marketInfos;
    }

    private static void fillMarketInfo(final MarketInfo marketInfo) {
        marketInfo.setQuantityIncrement(new BigDecimal("0.0001"));
        marketInfo.setMaxQuantity(new BigDecimal("1000000"));
        marketInfo.setMaxPrice(new BigDecimal("1000000000"));

        switch (marketInfo.getBaseCurrency().trim().toUpperCase()) {
            case "BTC":
                marketInfo.setPriceIncrement(new BigDecimal("1000"));
                marketInfo.setMinQuantity(new BigDecimal("0.0001"));
                break;
            case "BCH":
                marketInfo.setPriceIncrement(new BigDecimal("500"));
                marketInfo.setMinQuantity(new BigDecimal("0.001"));
                break;
            case "ETH":
                marketInfo.setPriceIncrement(new BigDecimal("100"));
                marketInfo.setMinQuantity(new BigDecimal("0.01"));
                break;
            case "ETC":
                marketInfo.setPriceIncrement(new BigDecimal("10"));
                marketInfo.setMinQuantity(new BigDecimal("0.1"));
                break;
            case "XRP":
                marketInfo.setPriceIncrement(new BigDecimal("1"));
                marketInfo.setMinQuantity(new BigDecimal("1"));
                break;
            case "QTUM":
                marketInfo.setPriceIncrement(new BigDecimal("10"));
                marketInfo.setMinQuantity(new BigDecimal("0.1"));
                break;
            case "LTC":
                marketInfo.setPriceIncrement(new BigDecimal("50"));
                marketInfo.setMinQuantity(new BigDecimal("0.1"));
                break;
            case "IOTA":
                marketInfo.setPriceIncrement(new BigDecimal("10"));
                marketInfo.setMinQuantity(new BigDecimal("0.1"));
                break;
            case "BTG":
                marketInfo.setPriceIncrement(new BigDecimal("50"));
                marketInfo.setMinQuantity(new BigDecimal("0.01"));
                break;
            case "OMG":
                marketInfo.setPriceIncrement(new BigDecimal("10"));
                marketInfo.setMinQuantity(new BigDecimal("0.1"));
                break;
            case "EOS":
                marketInfo.setPriceIncrement(new BigDecimal("10"));
                marketInfo.setMinQuantity(new BigDecimal("0.1"));
                break;
            case "DATA":
                marketInfo.setPriceIncrement(new BigDecimal("1"));
                marketInfo.setMinQuantity(new BigDecimal("1"));
                break;
            case "ZIL":
                marketInfo.setPriceIncrement(new BigDecimal("1"));
                marketInfo.setMinQuantity(new BigDecimal("1"));
                break;
            case "KNC":
                marketInfo.setPriceIncrement(new BigDecimal("1"));
                marketInfo.setMinQuantity(new BigDecimal("1"));
                break;
            case "ZRX":
                marketInfo.setPriceIncrement(new BigDecimal("1"));
                marketInfo.setMinQuantity(new BigDecimal("1"));
                break;
        }
    }

    @Override
    public Ticker getTicker(GetTickerRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        checkQuoteKRW(request);
        final HttpResponse response = requestGet("/ticker/", ParamsBuilder.buildIfNotNull("currency", request.getBaseCurrency()));

        checkResponseError(request, response);

        return convert(response, CoinoneTickerResponse.class, (i -> i.toTicker(request)));
    }

    @Override
    public Tickers getAllTickers()
        throws CTTKException
    {
        final HttpResponse response = requestGet("/ticker/", ParamsBuilder.buildIfNotNull("currency", "all"));

        checkResponseError(null, response);

        return convert(response, CoinoneAllTickersResponse.class, (i -> i.toAllTickers()));
    }

    @Override
    public OrderBook getOrderBook(GetOrderBookRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        checkQuoteKRW(request);
        final HttpResponse response = requestGet("/orderbook/", ParamsBuilder.buildIfNotNull("currency", request.getBaseCurrency()));

        checkResponseError(request, response);

        return convert(response, CoinoneOrderBookResponse.class, (i -> i.toOrderBook(request)));
    }

    @Override
    public Trades getRecentTrades(GetRecentTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        checkQuoteKRW(request);
        final HttpResponse response = requestGet("/trades/",
            ParamsBuilder.create()
                .putIfNotNull("currency", request.getBaseCurrency())
                .putIfNotNull("period", nvl(request.getPeriod(), "day"))
                .build());

        checkResponseError(request, response);

        return convert(response, CoinoneTradesResponse.class, (i -> i.toTrades(request)));
    }

    @Override
    public Trades getTradeHistory(GetTradeHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        throw new UnsupportedMethodException().setCxId(this.getCxId());
    }
}
