package cttk.impl.bithumb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.powermock.api.support.membermodification.MemberMatcher.method;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberModifier;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import cttk.CXClientFactory;
import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.PublicCXClient;
import cttk.auth.Credential;
import cttk.dto.UserAccount;
import cttk.dto.UserOrder;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.bithumb.response.BithumbCreateOrderResponse;
import cttk.impl.util.HttpResponse;
import cttk.request.CreateOrderRequest;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ CXClientFactory.class, BithumbPrivateClient.class })
@PowerMockIgnore("javax.net.ssl.*")
public class BithumbResponseConvertingTest {
    BithumbPrivateClient client;

    @Before
    public void setup()
        throws CTTKException
    {
        client = spy(createDummyPrivateClient());
    }

    BithumbPrivateClient createDummyPrivateClient()
        throws CTTKException
    {
        PowerMockito.mockStatic(CXClientFactory.class);
        PowerMockito.when(CXClientFactory.createPublicCXClient(anyString())).thenReturn(mock(PublicCXClient.class));
        MemberModifier.stub(method(BithumbPrivateClient.class, "getUserAccount")).toReturn(mock(UserAccount.class));
        return new BithumbPrivateClient(mock(Credential.class));
    }

    UserOrder convertBithumbCreateOrderResponseToUserOrder(CreateOrderRequest request, HttpResponse response)
        throws UnsupportedCurrencyPairException, CTTKException
    {
        return client.convert(response, BithumbCreateOrderResponse.class, (i -> i.toUserOrder(request)));
    }

    @Test
    public void testConvertBithumbCreateOrderResponseToUserOrder()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        CreateOrderRequest request = new CreateOrderRequest()
            .setCurrencyPair(CurrencyPair.of("baseCurrency", "quoteCurrency"))
            .setSide(OrderSide.of("BUY"))
            .setPrice(new BigDecimal("289000"))
            .setQuantity(new BigDecimal("0.8"));

        HttpResponse responseDataNull = new HttpResponse();
        responseDataNull.setBody("{\"status\": \"0000\", \"order_id\": \"1428646963419\"}");
        UserOrder resultDataNull = convertBithumbCreateOrderResponseToUserOrder(request, responseDataNull);

        HttpResponse responseDataEmpty = new HttpResponse();
        responseDataEmpty.setBody("{\"status\": \"0000\", \"order_id\": \"1428646963419\", \"data\": []}");
        UserOrder resultDataEmpty = convertBithumbCreateOrderResponseToUserOrder(request, responseDataEmpty);

        HttpResponse responseDataLess = new HttpResponse();
        responseDataLess.setBody("{\"status\": \"0000\", \"order_id\": \"1428646963419\", \"data\": [{\"units\": \"0.61460000\", \"fee\": \"0.00061460\", \"price\": \"284000\", \"total\": 174546, \"contNo\": \"15313\"}]}");
        UserOrder resultDataLess = convertBithumbCreateOrderResponseToUserOrder(request, responseDataLess);

        HttpResponse responseDataFit = new HttpResponse();
        responseDataFit.setBody("{\"status\": \"0000\", \"order_id\": \"1428646963419\", \"data\": [{\"units\": \"0.61460000\", \"fee\": \"0.00061460\", \"price\": \"284000\", \"total\": 174546, \"contNo\": \"15313\"}, {\"units\": \"0.18540000\", \"fee\": \"0.00018540\", \"price\": \"289000\", \"total\": 53581, \"contNo\": \"15314\"}]}");
        UserOrder resultDataFit = convertBithumbCreateOrderResponseToUserOrder(request, responseDataFit);

        assertEquals(OrderStatus.UNFILLED, resultDataNull.getStatusType());
        assertEquals(OrderStatus.UNFILLED, resultDataEmpty.getStatusType());
        assertEquals(OrderStatus.PARTIALLY_FILLED, resultDataLess.getStatusType());
        assertEquals(OrderStatus.FILLED, resultDataFit.getStatusType());

        assertTrue(resultDataNull.getTrades() == null);
        assertTrue(resultDataEmpty.getTrades().size() == 0);

        assertTrue(resultDataLess.getTrades().size() != 0);
        assertTrue(resultDataFit.getTrades().size() != 0);
        assertTrue(resultDataFit.getOid() != null);
        assertTrue(resultDataFit.getTrades().get(0).getTotal() != null);
        assertTrue(resultDataFit.getTrades().get(0).getFee() != null);
        assertTrue(resultDataFit.getTrades().get(0).getTid() != null);
    }

}
