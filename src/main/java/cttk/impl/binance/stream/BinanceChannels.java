package cttk.impl.binance.stream;

import static cttk.util.StringUtils.toLower;

import cttk.CurrencyPair;

public class BinanceChannels {
    // github.com/binance-exchange/binance-official-api-docs/blob/master/web-socket-streams.md
    public static final String getTickerChannel(CurrencyPair currencyPair) {
        return getSymbol(currencyPair) + "@ticker";
    }

    public static final String getOrderBookChannel(CurrencyPair currencyPair) {
        return getSymbol(currencyPair) + "@depth";
    }

    public static final String getTradeChannel(CurrencyPair currencyPair) {
        return getSymbol(currencyPair) + "@trade";
    }

    private static final String getSymbol(CurrencyPair currencyPair) {
        return toLower(currencyPair.getBaseCurrency()) + toLower(currencyPair.getQuoteCurrency());
    }
}
