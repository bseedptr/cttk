package cttk.impl.bibox.response;

import cttk.dto.UserOrder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BiboxUserTradeResponse
    extends BiboxResultWrapperResponse<Long>
{
    public UserOrder toUserOrder() {
        return new UserOrder().setOid(String.valueOf(unwrap()));
    }
}
