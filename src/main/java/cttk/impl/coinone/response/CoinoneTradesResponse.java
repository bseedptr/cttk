package cttk.impl.coinone.response;

import static cttk.impl.util.DateTimeUtils.zdtFromEpochSecond;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.dto.Trade;
import cttk.dto.Trades;
import cttk.impl.util.MathUtils;
import cttk.impl.util.ZoneIds;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CoinoneTradesResponse
    extends CoinoneResponse
{
    String timestamp;
    String currency;
    List<CoinoneCompleteOrder> completeOrders;

    public Trades toTrades(CurrencyPair currencyPair) {
        return new Trades()
            .setCurrencyPair(currencyPair)
            .setTrades(toTradeList(completeOrders));
    }

    private List<Trade> toTradeList(List<CoinoneCompleteOrder> coinoneCompleteOrders) {
        if (coinoneCompleteOrders == null)
            return null;
        return coinoneCompleteOrders.stream().map(CoinoneCompleteOrder::toTrade).collect(Collectors.toList());
    }

}

@Data
class CoinoneCompleteOrder {
    Long timestamp;
    BigDecimal price;
    BigDecimal qty;

    public Trade toTrade() {
        return new Trade()
            .setSno(BigInteger.valueOf(timestamp))
            .setDateTime(zdtFromEpochSecond(timestamp, ZoneIds.SEOUL))
            .setPrice(price)
            .setQuantity(qty)
            .setTotal(MathUtils.multiply(price, qty));
    }
}