package cttk;

import static cttk.util.StringUtils.toUpper;

public enum PricingType {
    LIMIT, MARKET;

    public static final PricingType of(String type) {
        switch (toUpper(type)) {
            case "LIMIT":
            case "L":
                return LIMIT;
            case "MARKET":
            case "M":
                return MARKET;
            default:
                return null;
        }
    }
}
