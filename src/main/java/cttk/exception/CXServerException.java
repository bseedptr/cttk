package cttk.exception;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import okhttp3.Request;

@Getter
@Setter
@Accessors(chain = true)
public class CXServerException
    extends CXAPIRequestException
{
    private static final long serialVersionUID = 1L;
    protected Request request;

    public CXServerException() {
        super();
    }

    public CXServerException(String message, Throwable cause) {
        super(message, cause);
    }

    public CXServerException(String message) {
        super(message);
    }

    public CXServerException(Throwable cause) {
        super(cause);
    }

    @Override
    public CXServerException setCxId(String cxId) {
        super.setCxId(cxId);
        return this;
    }

    @Override
    public CXServerException setResponseStatusCode(Integer responseStatusCode) {
        super.setResponseStatusCode(responseStatusCode);
        return this;
    }

    @Override
    public CXServerException setExchangeMessage(String exchangeMessage) {
        super.setExchangeMessage(exchangeMessage);
        return this;
    }

    @Override
    public String toString() {
        return super.toString()
            + ", Request = " + request;
    }
}
