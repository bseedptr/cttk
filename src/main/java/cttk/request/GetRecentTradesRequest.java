package cttk.request;

import cttk.AbstractCurrencyPair;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class GetRecentTradesRequest
    extends AbstractCurrencyPair<GetRecentTradesRequest>
{
    private Integer count;

    private Long lastTradeId; // coinbase before, kraken since, data.binance fromId

    // coinone: hour, day; korbit: minute, hour, day
    private String period;

    public GetRecentTradesRequest refineCountWithMax(int maxCount) {
        if (this.count != null && this.count > maxCount) this.count = maxCount;
        return this;
    }

    public GetRecentTradesRequest setLastTradeId(Long lastTradeId) {
        this.lastTradeId = lastTradeId;
        return this;
    }

    public GetRecentTradesRequest setTime(String time) {
        this.period = time;
        return this;
    }
}
