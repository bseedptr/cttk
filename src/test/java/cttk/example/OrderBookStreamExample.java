package cttk.example;

import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.CXStreams;
import cttk.CurrencyPair;
import cttk.dto.OrderBook;
import cttk.request.OpenOrderBookStreamRequest;

public class OrderBookStreamExample {
    public static void main(String[] args)
        throws Exception
    {
        openBinanceOrderBookStream();
    }

    public static void openBinanceOrderBookStream()
        throws Exception
    {
        final String cxId = CXIds.KORBIT;
        final CurrencyPair currencyPair = CurrencyPair.of("ETH", "KRW");

        final CXStream<OrderBook> stream = CXStreams.openOrderBookSnapshotStream(cxId,
            new OpenOrderBookStreamRequest().setCurrencyPair(currencyPair),
            CXStreamListener.createPrint());

        Thread.sleep(10000l);
        stream.close();
    }
}
