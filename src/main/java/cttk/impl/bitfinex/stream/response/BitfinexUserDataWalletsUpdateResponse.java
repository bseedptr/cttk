package cttk.impl.bitfinex.stream.response;

import static cttk.impl.bitfinex.BitfinexUtils.toStdCurrencySymbol;
import static cttk.util.StringUtils.parseBigDecimal;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import cttk.CXIds;
import cttk.dto.Balance;
import cttk.dto.Balances;

public class BitfinexUserDataWalletsUpdateResponse
    extends ArrayList<String>
{
    public Balances toBalances() {
        Balance balance = new Balance();

        final String type = get(0);
        final String currency = toStdCurrencySymbol(get(1));
        final BigDecimal total = parseBigDecimal(get(2));
        final BigDecimal available = parseBigDecimal(get(4));

        balance.setCxId(CXIds.BITFINEX);
        balance.setType(type);
        balance.setCurrency(currency);
        balance.setTotal(total);

        if (available != null) {
            balance.setAvailable(available);
        }
        if (total != null && available != null) {
            balance.setInUse(total.subtract(available));
        }

        List<Balance> singleBalanceList = new LinkedList<>();
        singleBalanceList.add(balance);

        return new Balances().setBalances(singleBalanceList);
    }
}
