package cttk.impl.huobi.response;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.CurrencyPair;
import cttk.dto.OrderBook;
import cttk.dto.OrderBook.SimpleOrder;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HuobiOrderBookResponse
    extends HuobiResponse
{
    HuobiOrderBookTick tick;

    public OrderBook getOrderBook(CurrencyPair currencyPair) {
        return new OrderBook()
            .setCurrencyPair(currencyPair)
            .setBids(tick.getBids().stream().map(b -> SimpleOrder.of(String.valueOf(b.get(0)), String.valueOf(b.get(1)))).collect(Collectors.toList()))
            .setAsks(tick.getAsks().stream().map(a -> SimpleOrder.of(String.valueOf(a.get(0)), String.valueOf(a.get(1)))).collect(Collectors.toList()))
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(tick.getTimestamp()));
    }
}

@Data
class HuobiOrderBookTick {
    List<List<Object>> bids;
    List<List<Object>> asks;
    Long version;
    @JsonProperty("ts")
    Long timestamp;
}
