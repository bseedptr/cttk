package cttk;

import static cttk.util.StringUtils.toUpper;

public enum FillType {
    PARTIAL_FILL, FILL;

    public static FillType of(String type) {
        switch (toUpper(type)) {
            case "PARTIAL_FILL":
                return PARTIAL_FILL;
            case "FILL":
                return FILL;
            default:
                return null;
        }
    }
}
