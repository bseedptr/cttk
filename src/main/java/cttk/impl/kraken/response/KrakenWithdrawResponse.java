package cttk.impl.kraken.response;

import cttk.dto.WithdrawResult;
import lombok.Data;

public class KrakenWithdrawResponse
    extends KrakenResponse<KrakenWithdraw>
{
    public WithdrawResult toWithdrawResult() {
        return new WithdrawResult().setWithdrawId(result.refid);
    }

}

@Data
class KrakenWithdraw {
    String refid;
}