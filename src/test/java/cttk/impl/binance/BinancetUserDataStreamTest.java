package cttk.impl.binance;

import cttk.CXIds;
import cttk.impl.AbstractUserDataStreamTest;

public class BinancetUserDataStreamTest
    extends AbstractUserDataStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.BINANCE;
    }
}
