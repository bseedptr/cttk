package cttk.impl.kraken.response;

import cttk.dto.Balance;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
public class KrakenTradeBalanceResponse
    extends KrakenResponse<KrakenTradeBalance>
{
    public Balance fillBalance(Balance balance) {
        // TODO implement here

        return balance;
    }
}

@Data
@Accessors(chain = true)
class KrakenTradeBalance {
    String eb;  //eb = equivalent balance (combined balance of all currencies)
    String tb;  //tb = trade balance (combined balance of all equity currencies)
    String m;   //m = margin amount of open positions
    String n;   //n = unrealized net profit/loss of open positions
    String c;   //c = cost basis of open positions
    String v;   //v = current floating valuation of open positions
    String e;   //e = equity = trade balance + unrealized net profit/loss
    String mf;  //mf = free margin = equity - initial margin (maximum margin available to open new positions)
    String ml;  //ml = margin level = (equity / initial margin) * 100
}