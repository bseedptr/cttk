package cttk;

import cttk.exception.InvalidAmountMinException;
import cttk.exception.InvalidAmountRangeException;
import cttk.exception.InvalidOrderException;
import cttk.exception.InvalidPriceIncrementPrecisionException;
import cttk.exception.InvalidPriceMinException;
import cttk.exception.InvalidPriceRangeException;
import cttk.exception.InvalidQuantityIncrementPrecisionException;
import cttk.exception.InvalidTotalRangeException;
import cttk.request.CreateOrderRequest;

import java.math.BigDecimal;

public enum InvalidOrderType {
    INVALID_PRICE_INCREMENT {
        @Override
        public InvalidOrderException createException(CreateOrderRequest request, String message) {
            return new InvalidPriceIncrementPrecisionException(message);
        }
    },
    INVALID_PRICE_RANGE {
        @Override
        public InvalidOrderException createException(CreateOrderRequest request, String message) {
            if (request.getPrice().compareTo(BigDecimal.ZERO) <= 0) {
                return new InvalidPriceMinException(message);
            }
            return new InvalidPriceRangeException(message);
        }
    },
    INVALID_QUANTITY_INCREMENT {
        @Override
        public InvalidOrderException createException(CreateOrderRequest request, String message) {
            return new InvalidQuantityIncrementPrecisionException(message);
        }
    },
    INVALID_QUANTITY_RANGE {
        @Override
        public InvalidOrderException createException(CreateOrderRequest request, String message) {
            if (request.getPrice().compareTo(BigDecimal.ZERO) <= 0) {
                return new InvalidAmountMinException(message);
            }
            return new InvalidAmountRangeException(message);
        }
    },
    INVALID_TOTAL_RANGE {
        @Override
        public InvalidOrderException createException(CreateOrderRequest request, String message) {
            return new InvalidTotalRangeException(message).setTotal(request.getQuantity().multiply(request.getPrice()));
        }
    };

    abstract public InvalidOrderException createException(CreateOrderRequest request, String message);
}
