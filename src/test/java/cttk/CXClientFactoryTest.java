package cttk;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import cttk.auth.CXCredentials;
import cttk.auth.CXCredentialsProvider;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCXException;

public class CXClientFactoryTest {

    @Test
    public void testBithumb()
        throws CTTKException, IOException
    {
        PublicCXClient pubClient = CXClientFactory.createPublicCXClient(CXIds.BITHUMB);
        Assert.assertEquals(CXIds.BITHUMB, pubClient.getCxId());

        final CXCredentials credentials = new CXCredentialsProvider("src/test/resources/credentials").get(CXIds.BITHUMB);
        PrivateCXClient prvClient = CXClientFactory.createPrivateCXClient(CXIds.BITHUMB, credentials.getDataKey());
        Assert.assertEquals(CXIds.BITHUMB, prvClient.getCxId());

    }

    @Test
    public void testBittrex()
        throws CTTKException, IOException
    {
        PublicCXClient pubClient = CXClientFactory.createPublicCXClient(CXIds.BITTREX);
        Assert.assertEquals(CXIds.BITTREX, pubClient.getCxId());
    }

    @Test
    public void testUpbit()
        throws CTTKException
    {
        CXClientFactory.createPublicCXClient(CXIds.UPBIT);
    }

    @Test(expected = UnsupportedCXException.class)
    public void testBittrexPrivate()
        throws IOException, CTTKException
    {
        final CXCredentials credentials = new CXCredentialsProvider("src/test/resources/credentials").get(CXIds.BITTREX);
        CXClientFactory.createPrivateCXClient(CXIds.BITTREX, credentials.getDataKey());
    }
}
