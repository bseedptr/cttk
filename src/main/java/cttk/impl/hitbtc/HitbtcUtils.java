package cttk.impl.hitbtc;

import static cttk.util.StringUtils.concat;
import static cttk.util.StringUtils.toUpper;

import java.io.IOException;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import cttk.CXIds;
import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.SortType;
import cttk.exception.APILimitExceedException;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXAuthorityException;
import cttk.exception.InvalidAmountMinException;
import cttk.exception.InvalidAmountRangeException;
import cttk.exception.InvalidPriceMinException;
import cttk.exception.InvalidPriceRangeException;
import cttk.exception.OrderNotFoundException;
import cttk.request.CreateOrderRequest;

public class HitbtcUtils {
    public static ObjectMapper objectMapper = new ObjectMapper();
    final static String cxId = CXIds.HITBTC;

    public static String getMarketSymbol(CurrencyPair currencyPair) {
        if (currencyPair == null) return null;
        return concat(toHitbtcCurrencySymbol(toUpper(currencyPair.getBaseCurrency())), toHitbtcCurrencySymbol(toUpper(currencyPair.getQuoteCurrency())));
    }

    public static OrderStatus getStatusType(String status) {
        switch (status) {
            case "new":
            case "suspended":
                return OrderStatus.UNFILLED;
            case "partiallyFilled":
                return OrderStatus.PARTIALLY_FILLED;
            case "filled":
                return OrderStatus.FILLED;
            case "canceled":
                return OrderStatus.CANCELED;
            case "expired":
                return OrderStatus.EXPIRED;
            default:
                return null;
        }
    }

    public static CurrencyPair parseMarketSymbol(String symbol) {
        if (symbol.length() == 4) {
            return toStdCurrencyPair(CurrencyPair.of(symbol.substring(0, 1), symbol.substring(1, 4)));
        } else if (symbol.length() == 5) {
            return toStdCurrencyPair(CurrencyPair.of(symbol.substring(0, 2), symbol.substring(2, 5)));
        } else if (symbol.length() == 7) {
            return toStdCurrencyPair(CurrencyPair.of(symbol.substring(0, 4), symbol.substring(4, 7)));
        } else if (symbol.length() == 8) {
            return toStdCurrencyPair(CurrencyPair.of(symbol.substring(0, 5), symbol.substring(5, 8)));
        } else if (symbol.length() == 10) {
            return toStdCurrencyPair(CurrencyPair.of(symbol.substring(0, 7), symbol.substring(7, 10)));
        } else { // symbol.length() == 6
            return toStdCurrencyPair(CurrencyPair.of(symbol.substring(0, 3), symbol.substring(3, 6)));
        }
    }

    public static CurrencyPair toStdCurrencyPair(CurrencyPair currencyPair) {
        return currencyPair == null ? null
            : CurrencyPair.of(toStdCurrencySymbol(currencyPair.getBaseCurrency()), toStdCurrencySymbol(currencyPair.getQuoteCurrency()));
    }

    /**
     * Convert the given currency symbol to the standardized symbol
     * @param currency the currency symbol
     * @return standard currency symbol
     */
    public static String toStdCurrencySymbol(String currency) {
        if (currency == null) return null;

        currency = currency.trim().toUpperCase();
        if ("USD".equals(currency)) currency = "USDT";
        return currency;
    }

    public static String toHitbtcCurrencySymbol(String currency) {
        if (currency == null) return null;

        if ("USDT".equals(currency.toUpperCase())) currency = "USD";
        return currency;
    }

    public static String toString(OrderSide orderSide) {
        if (orderSide == null) return null;
        switch (orderSide) {
            case BUY:
                return "buy";
            case SELL:
                return "sell";
            default:
                return null;
        }
    }

    public static String toHitbtcSortType(SortType sort) {
        if (sort == null) return null;
        switch (sort) {
            case DESC:
                return "DESC";
            case ASC:
                return "ASC";
            default:
                return null;
        }
    }

    public static String guessFeeCurrency(CurrencyPair pair) {
        return toStdCurrencySymbol(pair.getQuoteCurrency());
    }

    public static void checkErrorByResponseMessage(Object requestObj, String body, Integer statusCode)
        throws CTTKException
    {
        Map<?, ?> map;

        try {
            map = (Map<?, ?>) ((Map<?, ?>) objectMapper.readValue(body, Map.class)).get("error");
            if (map == null) return;
        } catch (IOException e) {
            return;
        }

        final String exchangeErrorCode = map.containsKey("code") ? map.get("code").toString() : "-1";
        final String exchangeMessage = map.containsKey("message") ? map.get("message").toString() : "";

        switch (exchangeErrorCode) {
            case "2001":
            case "2002":
                throw CXAPIRequestException.createByRequestObj(requestObj, exchangeMessage)
                    .setCxId(cxId)
                    .setExchangeMessage(body)
                    .setResponseStatusCode(statusCode)
                    .setExchangeErrorCode(exchangeErrorCode);
            case "2010":
            case "2012":
                if (requestObj instanceof CreateOrderRequest) {
                    CreateOrderRequest request = (CreateOrderRequest) requestObj;
                    throw new InvalidAmountRangeException(exchangeMessage)
                        .setCxId(cxId)
                        .setExchangeMessage(body)
                        .setResponseStatusCode(statusCode)
                        .setExchangeErrorCode(exchangeErrorCode)
                        .setOrderPrice(request.getPrice())
                        .setOrderQuantity(request.getQuantity())
                        .setBaseCurrency(request.getBaseCurrency())
                        .setQuoteCurrency(request.getQuoteCurrency());
                } else {
                    throw new InvalidAmountRangeException(exchangeMessage)
                        .setCxId(cxId)
                        .setExchangeMessage(body)
                        .setResponseStatusCode(statusCode)
                        .setExchangeErrorCode(exchangeErrorCode);
                }
            case "2011":
                if (requestObj instanceof CreateOrderRequest) {
                    CreateOrderRequest request = (CreateOrderRequest) requestObj;
                    throw new InvalidAmountMinException(exchangeMessage)
                        .setCxId(cxId)
                        .setExchangeMessage(body)
                        .setResponseStatusCode(statusCode)
                        .setExchangeErrorCode(exchangeErrorCode)
                        .setOrderPrice(request.getPrice())
                        .setOrderQuantity(request.getQuantity())
                        .setBaseCurrency(request.getBaseCurrency())
                        .setQuoteCurrency(request.getQuoteCurrency());
                } else {
                    throw new InvalidAmountMinException(exchangeMessage)
                        .setCxId(cxId)
                        .setExchangeMessage(body)
                        .setResponseStatusCode(statusCode)
                        .setExchangeErrorCode(exchangeErrorCode);
                }
            case "2020":
            case "2022":
                if (requestObj instanceof CreateOrderRequest) {
                    CreateOrderRequest request = (CreateOrderRequest) requestObj;
                    throw new InvalidPriceRangeException(exchangeMessage)
                        .setCxId(cxId)
                        .setExchangeMessage(body)
                        .setResponseStatusCode(statusCode)
                        .setExchangeErrorCode(exchangeErrorCode)
                        .setOrderPrice(request.getPrice())
                        .setOrderQuantity(request.getQuantity())
                        .setBaseCurrency(request.getBaseCurrency())
                        .setQuoteCurrency(request.getQuoteCurrency());
                } else {
                    throw new InvalidPriceRangeException(exchangeMessage)
                        .setCxId(cxId)
                        .setExchangeMessage(body)
                        .setResponseStatusCode(statusCode)
                        .setExchangeErrorCode(exchangeErrorCode);
                }
            case "2021":
                if (requestObj instanceof CreateOrderRequest) {
                    CreateOrderRequest request = (CreateOrderRequest) requestObj;
                    throw new InvalidPriceMinException(exchangeMessage)
                        .setCxId(cxId)
                        .setExchangeMessage(body)
                        .setResponseStatusCode(statusCode)
                        .setExchangeErrorCode(exchangeErrorCode)
                        .setOrderPrice(request.getPrice())
                        .setOrderQuantity(request.getQuantity())
                        .setBaseCurrency(request.getBaseCurrency())
                        .setQuoteCurrency(request.getQuoteCurrency());
                } else {
                    throw new InvalidPriceMinException(exchangeMessage)
                        .setCxId(cxId)
                        .setExchangeMessage(body)
                        .setResponseStatusCode(statusCode)
                        .setExchangeErrorCode(exchangeErrorCode);
                }
            case "429":
                throw new APILimitExceedException(exchangeMessage)
                    .setCxId(cxId)
                    .setExchangeMessage(body)
                    .setResponseStatusCode(statusCode)
                    .setExchangeErrorCode(exchangeErrorCode);
            case "1002":
                throw new CXAuthorityException(exchangeMessage)
                    .setCxId(cxId)
                    .setExchangeMessage(body)
                    .setResponseStatusCode(statusCode)
                    .setExchangeErrorCode(exchangeErrorCode);
            case "20002":
                throw new OrderNotFoundException(exchangeMessage)
                    .setCxId(cxId)
                    .setExchangeMessage(body)
                    .setResponseStatusCode(statusCode)
                    .setExchangeErrorCode(exchangeErrorCode);
            case "10001":
                if (exchangeMessage.contains("price")) {
                    if (requestObj instanceof CreateOrderRequest) {
                        CreateOrderRequest request = (CreateOrderRequest) requestObj;
                        throw new InvalidPriceRangeException(exchangeMessage)
                            .setCxId(cxId)
                            .setExchangeMessage(body)
                            .setResponseStatusCode(statusCode)
                            .setExchangeErrorCode(exchangeErrorCode)
                            .setOrderPrice(request.getPrice())
                            .setOrderQuantity(request.getQuantity())
                            .setBaseCurrency(request.getBaseCurrency())
                            .setQuoteCurrency(request.getQuoteCurrency());
                    } else {
                        throw new InvalidPriceRangeException(exchangeMessage)
                            .setCxId(cxId)
                            .setExchangeMessage(body)
                            .setResponseStatusCode(statusCode)
                            .setExchangeErrorCode(exchangeErrorCode);
                    }
                }
        }
    }
}