package cttk.impl.bibox.response;

import java.util.List;
import java.util.stream.Collectors;

import cttk.dto.UserOrders;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BiboxUserOrderPendingListResponse
    extends BiboxResultWrapperResponse<BiboxUserOrderPendingList>
{
    public UserOrders toUserOrders() {
        return unwrap().toUserOrders();
    }

    public UserOrders toOpenUserOrders() {
        return unwrap().toOpenUserOrders();
    }
}

@Data
class BiboxUserOrderPendingList {
    Integer count;
    Integer page;
    List<BiboxUserOrderPending> items;

    UserOrders toUserOrders() {
        return new UserOrders()
            .setOrders(items
                .stream()
                .map(BiboxUserOrderPending::toUserOrder)
                .collect(Collectors.toList()));
    }

    UserOrders toOpenUserOrders() {
        return new UserOrders().setOrders(
            items.stream()
                .filter(BiboxUserOrderPending::isOpenOrder)
                .map(BiboxUserOrderPending::toUserOrder)
                .collect(Collectors.toList()));
    }
}
