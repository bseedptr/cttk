package cttk.impl.feeprovider;

import cttk.CurrencyPair;
import lombok.ToString;

@ToString(callSuper = true)
public class ByBaseTradingFeeProvider
    extends AbstractMappedTradingFeeProvider<ByBaseTradingFeeProvider>
{
    @Override
    public String getKey(CurrencyPair currencyPair) {
        return currencyPair == null ? null : currencyPair.getBaseCurrency();
    }
}
