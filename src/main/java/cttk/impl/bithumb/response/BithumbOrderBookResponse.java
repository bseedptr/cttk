package cttk.impl.bithumb.response;

import java.util.List;
import java.util.stream.Collectors;

import cttk.dto.OrderBook;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.ZoneIds;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BithumbOrderBookResponse
    extends BithumbDataResponse<BithumbOrderBook>
{
    public OrderBook toOrderBook() {
        return new OrderBook()
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(this.data.timestamp, ZoneIds.SEOUL))
            .setBaseCurrency(this.data.order_currency)
            .setQuoteCurrency(this.data.payment_currency)
            .setAsks(toOrderList(data.asks))
            .setBids(toOrderList(data.bids));
    }

    public static List<OrderBook.SimpleOrder> toOrderList(List<BithumbOrder> bithumbOrders) {
        if (bithumbOrders == null) return null;
        return bithumbOrders.stream().map(BithumbOrder::toOrder).collect(Collectors.toList());
    }
}

@Data
class BithumbOrderBook {
    Long timestamp;
    String payment_currency;
    String order_currency;
    List<BithumbOrder> bids;
    List<BithumbOrder> asks;
}

@Data
class BithumbOrder {
    String price;
    String quantity;

    public OrderBook.SimpleOrder toOrder() {
        return OrderBook.SimpleOrder.of(price, quantity);
    }
}