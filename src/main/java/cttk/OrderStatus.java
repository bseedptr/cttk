package cttk;

public enum OrderStatus {
    UNFILLED, PARTIALLY_FILLED, FILLED,

    CANCELED, REJECTED, EXPIRED;

    public static boolean isOpen(OrderStatus status) {
        if (status == null) return false;
        switch (status) {
            case UNFILLED:
            case PARTIALLY_FILLED:
                return true;
            default:
                return false;
        }
    }
}
