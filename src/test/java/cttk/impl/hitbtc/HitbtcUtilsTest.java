package cttk.impl.hitbtc;

import org.junit.Assert;
import org.junit.Test;

import cttk.CurrencyPair.SimpleCurrencyPair;

public class HitbtcUtilsTest {
    @Test
    public void testGuessFeeCurrencyAlwaysReturnQuoteCurrency() {
        SimpleCurrencyPair EOS_ETH = new SimpleCurrencyPair("EOS", "ETH");
        SimpleCurrencyPair QTUM_ETH = new SimpleCurrencyPair("QTUM", "ETH");
        SimpleCurrencyPair BTC_ETH = new SimpleCurrencyPair("BTC", "ETH");

        Assert.assertEquals("Should be 'ETH' regardless of base currency.", EOS_ETH.getQuoteCurrency(), HitbtcUtils.guessFeeCurrency(EOS_ETH));
        Assert.assertEquals("Should be 'ETH' regardless of base currency.", QTUM_ETH.getQuoteCurrency(), HitbtcUtils.guessFeeCurrency(QTUM_ETH));
        Assert.assertEquals("Should be 'ETH' regardless of base currency.", BTC_ETH.getQuoteCurrency(), HitbtcUtils.guessFeeCurrency(BTC_ETH));
    }
}