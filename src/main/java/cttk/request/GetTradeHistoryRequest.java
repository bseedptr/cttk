package cttk.request;

import java.time.ZonedDateTime;

import cttk.AbstractCurrencyPair;
import cttk.DurationGettable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class GetTradeHistoryRequest
    extends AbstractCurrencyPair<GetTradeHistoryRequest>
    implements DurationGettable
{
    private Integer count;
    private ZonedDateTime startDateTime;
    private ZonedDateTime endDateTime;

    private Long lastTradeId; // coinbase before, kraken since, data.binance fromId

    // coinone: hour, day; korbit: minute, hour, day
    private String period;

    public GetTradeHistoryRequest refineCountWithMax(int maxCount) {
        if (this.count != null && this.count > maxCount) this.count = maxCount;
        return this;
    }

    public GetTradeHistoryRequest setLastTradeId(Long lastTradeId) {
        this.lastTradeId = lastTradeId;
        return this;
    }

    public GetTradeHistoryRequest setTime(String time) {
        this.period = time;
        return this;
    }
}
