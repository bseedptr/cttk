package cttk.impl.util;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import cttk.impl.util.UriUtils;

public class UriUtilsTest {
    @Test
    public void testToQueryParamsString()
        throws UnsupportedEncodingException
    {
        Map<String, String> map = new HashMap<>();
        map.put("endpoint", "/info/balance");
        map.put("order_currency", "BTC");
        map.put("payment_currency", "KRW");
        String encoded = UriUtils.toEncodedQueryParamsString(map);
        Assert.assertEquals("endpoint=%2Finfo%2Fbalance&order_currency=BTC&payment_currency=KRW", encoded);
    }
}