package cttk.impl.upbit.stream.response;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.dto.Trade;
import cttk.dto.Trades;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;

@Data
public class UpbitWebsocketTradeResponse {
    String code;
    Long timestamp;
    String trade_date;
    String trade_time;
    Long trade_timestamp;
    BigDecimal trade_price;
    BigDecimal trade_volume;
    String ask_bid;
    BigDecimal prev_closing_price;
    String change;
    Long change_price;
    BigInteger sequential_id;
    String stream_type;

    public Trades toTrades(CurrencyPair currencyPair) {
        return new Trades()
            .setCurrencyPair(currencyPair)
            .setTrades(toTradeList());
    }

    private List<Trade> toTradeList() {
        List<Trade> singleTradeList = new LinkedList<>();
        Trade trade = new Trade()
            .setSno(sequential_id)
            .setTid(String.valueOf(sequential_id))
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(trade_timestamp))
            .setPrice(trade_price)
            .setQuantity(trade_volume)
            .setSide(OrderSide.of(ask_bid));
        singleTradeList.add(trade);

        return singleTradeList;
    }
}
