package cttk.impl.bithumb;

import org.junit.Test;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.AbstractPrivateCXClientWithdrawTest;
import cttk.request.WithdrawToBankRequest;
import cttk.request.WithdrawToWalletRequest;

public class BithumbPrivateClientWithdrawTest
    extends AbstractPrivateCXClientWithdrawTest
{
    @Override
    public String getCxId() {
        return CXIds.BITHUMB;
    }

    // FIXME
    @Test(expected = CTTKException.class)
    @Override
    public void testWithdrawToBankAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        client.withdraw(new WithdrawToBankRequest()
            .setBankCode("020")
            .setAccountNo("1002153800385")
            .setAmount("10000"));
    }

    // FIXME
    @Test(expected = CTTKException.class)
    @Override
    public void testWithdrawBTCToWallet()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        client.withdraw(new WithdrawToWalletRequest()
            .setCurrency("BTC")
            .setAddress("1DHiQzGYN14xzZrk93FrcDZtK7DZgAVTy2")
            .setQuantity("0.01"));
    }
}