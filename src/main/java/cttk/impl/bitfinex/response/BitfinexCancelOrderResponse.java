package cttk.impl.bitfinex.response;

import lombok.Data;

@Data
public class BitfinexCancelOrderResponse {
    Long id;
    String symbol;
    String exchange;
    String price;
    String avg_execution_price;
    String side;
    String type;
    String timestamp;
    boolean is_live;
    boolean is_cancelled;
    boolean is_hidden;
    boolean was_forced;
    String original_amount;
    String remaining_amount;
    String executed_amount;
}