package cttk.impl.binance.response;

import static cttk.impl.bitfinex.BitfinexUtils.toStdCurrencySymbol;
import static cttk.util.StringUtils.toUpper;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.TransferStatus;
import cttk.TransferType;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;

@Data
public class BinanceDepositHistoryResponse {
    Boolean success;
    @JsonProperty("msg")
    String message;
    List<BinanceDeposit> depositList;

    public Transfers toTransfers() {
        if (depositList == null) return new Transfers();
        return new Transfers()
            .setTransfers(depositList.stream().map(i -> i.toTransfer()).collect(Collectors.toList()));
    }
}

@Data
class BinanceDeposit {
    String txId;
    Integer status;
    Long insertTime;
    BigDecimal amount;
    String address;
    String addressTag;
    String asset;

    public Transfer toTransfer() {
        return new Transfer()
            .setCurrency(toStdCurrencySymbol(asset))
            .setType(TransferType.DEPOSIT)
            .setTid(txId)
            .setTxId(txId)
            .setAmount(amount)
            .setStatus(status.toString())
            .setStatusType(getStatusType())
            .setAddress(address)
            .setAddressTag(addressTag)
            .setCompletedDateTime(DateTimeUtils.zdtFromEpochMilli(insertTime));
    }

    private TransferStatus getStatusType() {
        switch (status) {
            case 0:
                return TransferStatus.PENDING;
            case 1:
                return TransferStatus.SUCCESS;
        }
        return null;
    }
}