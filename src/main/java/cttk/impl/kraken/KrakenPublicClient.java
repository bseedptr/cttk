package cttk.impl.kraken;

import static cttk.impl.kraken.KrakenUtils.getMarketSymbol;

import cttk.PublicCXClient;
import cttk.dto.MarketInfos;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Tickers;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.kraken.response.KrakenAssetPairs;
import cttk.impl.kraken.response.KrakenOrderBooksResponse;
import cttk.impl.kraken.response.KrakenTickersResponse;
import cttk.impl.kraken.response.KrakenTradesResponse;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ParamsBuilder;
import cttk.request.GetOrderBookRequest;
import cttk.request.GetRecentTradesRequest;
import cttk.request.GetTickerRequest;
import cttk.request.GetTradeHistoryRequest;

public class KrakenPublicClient
    extends AbstractKrakenClient
    implements PublicCXClient
{
    @Override
    public MarketInfos getMarketInfos()
        throws CTTKException
    {
        return getAssetPairs().toMarketInfos();
    }

    @Override
    public Ticker getTicker(GetTickerRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet("Ticker",
            ParamsBuilder.build("pair", getMarketSymbol(getAssetPairs(), request)));

        checkResponseError(request, response);

        return convert(response, KrakenTickersResponse.class, i -> i.getTicker(getAssetPairs(), request));
    }

    @Override
    public Tickers getAllTickers()
        throws CTTKException
    {
        final KrakenAssetPairs assetPairs = getAssetPairs();

        final HttpResponse response = requestGet("Ticker",
            ParamsBuilder.build("pair", String.join(",", assetPairs.getAllAssetPairs())));

        checkResponseError(null, response);

        return convert(response, KrakenTickersResponse.class, i -> i.toTickers());
    }

    @Override
    public OrderBook getOrderBook(GetOrderBookRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet("Depth",
            ParamsBuilder.create()
                .putIfNotNull("pair", getMarketSymbol(getAssetPairs(), request))
                .putIfNotNull("count", request.getCount())
                .build());

        checkResponseError(request, response);

        return convert(response, KrakenOrderBooksResponse.class, i -> i.getOrderBook(getAssetPairs(), request));
    }

    @Override
    public Trades getRecentTrades(GetRecentTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet("Trades",
            ParamsBuilder.create()
                .putIfNotNull("pair", getMarketSymbol(getAssetPairs(), request))
                .putIfNotNull("since", request.getLastTradeId())
                .build());

        checkResponseError(request, response);

        return convert(response, KrakenTradesResponse.class, i -> i.getTrade(getAssetPairs(), request));
    }

    @Override
    public Trades getTradeHistory(GetTradeHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final HttpResponse response = requestGet("Trades",
            ParamsBuilder.create()
                .putIfNotNull("pair", getMarketSymbol(getAssetPairs(), request))
                .putIfNotNull("since", request.getLastTradeId())
                .build());

        checkResponseError(request, response);

        return convert(response, KrakenTradesResponse.class, i -> i.getTrade(getAssetPairs(), request));
    }
}
