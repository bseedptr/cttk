package cttk.impl.hitbtc;

import java.io.IOException;
import java.util.Map;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXServerException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.AbstractCXClient;
import cttk.impl.util.HttpResponse;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.Response;

public abstract class AbstractHitbtcClient
    extends AbstractCXClient
{
    protected static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    protected static final String BASE_URL = "https://api.hitbtc.com/api/2";

    @Override
    public String getCxId() {
        return CXIds.HITBTC;
    }

    protected HttpResponse requestGet(String endpoint)
        throws CTTKException
    {
        return requestGet(endpoint, null);
    }

    protected HttpResponse requestGet(String endpoint, Map<String, String> params)
        throws CTTKException
    {
        final Request request = new Request.Builder()
            .url(getHostUrl(endpoint, params))
            .build();

        debug(request);

        try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
            return toHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    protected String getHostUrl(String endpoint) {
        return super.getHostUrl(BASE_URL, endpoint);
    }

    protected String getHostUrl(String endpoint, Map<String, String> params) {
        return super.getHostUrl(BASE_URL, endpoint, params);
    }

    @Override
    protected HttpResponse toHttpResponse(Response httpResponse)
        throws UnsupportedCurrencyPairException, CTTKException
    {
        String body;
        try {
            body = httpResponse.body().string();
        } catch (IOException e) {
            throw new CXAPIRequestException(e)
                .setCxId(this.getCxId())
                .setExchangeMessage(httpResponse.body().toString())
                .setResponseStatusCode(httpResponse.code());
        }

        final HttpResponse response = new HttpResponse()
            .setStatus(httpResponse.code())
            .setBody(body);

        debug(response);

        return response;
    }

    @Override
    protected void checkErrorByResponseMessage(Object requestObj, HttpResponse response)
        throws CTTKException
    {
        HitbtcUtils.checkErrorByResponseMessage(requestObj, response.getBody(), response.getStatus());
    }
}
