package cttk.impl.poloniex.response;

import java.util.List;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.dto.OrderBook;
import cttk.dto.OrderBook.SimpleOrder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PoloniexOrderBookResponse {
    List<String[]> asks;
    List<String[]> bids;

    public OrderBook toOrderBook(CurrencyPair currencyPair) {
        return new OrderBook()
            .setCurrencyPair(currencyPair)
            .setAsks(toOrderList(asks))
            .setBids(toOrderList(bids));
    }

    private List<SimpleOrder> toOrderList(List<String[]> arrs) {
        if (arrs == null) return null;
        return arrs.stream().map(arr -> new SimpleOrder(arr[0], arr[1])).collect(Collectors.toList());
    }
}