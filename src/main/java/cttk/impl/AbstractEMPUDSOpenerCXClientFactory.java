package cttk.impl;

import cttk.CXClientFactory;
import cttk.UserDataStreamOpener;
import cttk.auth.Credential;
import cttk.exception.CTTKException;
import cttk.impl.stream.eachmarket.EachMarketPollingUserDataStreamOpener;

public abstract class AbstractEMPUDSOpenerCXClientFactory
    implements CXClientFactory
{
    @Override
    public UserDataStreamOpener createUserDataStreamOpener(Credential credential)
        throws CTTKException
    {
        return new EachMarketPollingUserDataStreamOpener(create(), create(credential));
    }
}
