package cttk.impl.hitbtc.response;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import cttk.dto.Ticker;
import cttk.impl.hitbtc.HitbtcUtils;
import lombok.Data;

@Data
public class HitbtcTickerResponse {
    BigDecimal ask;
    BigDecimal bid;
    BigDecimal last;
    BigDecimal open;
    BigDecimal low;
    BigDecimal high;
    BigDecimal volume;
    BigDecimal volumeQuote;
    String timestamp;
    String symbol;

    public Ticker toTicker() {
        return new Ticker()
            .setCurrencyPair(HitbtcUtils.parseMarketSymbol(symbol))
            .setLastPrice(last)
            .setOpenPrice(open)
            .setHighPrice(high)
            .setLowPrice(low)
            .setHighestBid(bid)
            .setLowestAsk(ask)
            .setVolume(volume)
            .setQuoteVolume(volumeQuote)
            .setDateTime(ZonedDateTime.parse(timestamp));
    }
}