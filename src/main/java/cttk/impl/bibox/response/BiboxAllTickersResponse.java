package cttk.impl.bibox.response;

import cttk.dto.Tickers;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.stream.Collectors;

@Data
@EqualsAndHashCode(callSuper = true)
public class BiboxAllTickersResponse
    extends BiboxResultResponse<List<BiboxMarketAll>>
{
    public Tickers toTickers() {
        return new Tickers()
            .setTickers(result.stream().map(BiboxMarketAll::toTicker).collect(Collectors.toList()));
    }
}
