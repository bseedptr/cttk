package cttk.impl.korbit.response;

import static cttk.impl.util.DateTimeUtils.zdtFromEpochMilli;

import cttk.dto.UserAccount;
import cttk.impl.util.ZoneIds;
import lombok.Data;

@Data
public class KorbitUserAccountResponse {
    String email;
    String nameCheckedAt;
    String name;
    String phone;
    String birthday;
    String gender;
    Prefs prefs;
    String userLevel;

    public UserAccount toUserAccount() {
        return new UserAccount()
            .setEmail(email)
            .setName(name)
            .setPhone(phone)
            .setBirthday(birthday)
            .setGender(gender)
            .setNotifyTrades(prefs.notifyTrades)
            .setNotifyDepositWithdrawal(prefs.notifyDepositWithdrawal)
            .setUserLevel(userLevel)
            .setNameCheckedDateTime(zdtFromEpochMilli(Long.parseLong(nameCheckedAt), ZoneIds.SEOUL));
    }
}

@Data
class Prefs {
    boolean notifyTrades;
    boolean notifyDepositWithdrawal;
}
