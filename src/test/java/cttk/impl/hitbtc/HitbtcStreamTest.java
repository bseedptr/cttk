package cttk.impl.hitbtc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import org.junit.Assert;
import org.junit.Test;

import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreams;
import cttk.dto.OrderBook;
import cttk.impl.AbstractCXStreamTest;
import cttk.request.OpenOrderBookStreamRequest;

public class HitbtcStreamTest
    extends AbstractCXStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.HITBTC;
    }

    @Test
    public void testOrderBookStreamWithPipe()
        throws InterruptedException, Exception
    {
        threadSleep();

        final OpenOrderBookStreamRequest request = new OpenOrderBookStreamRequest()
            .setIntervalInMilli(getIntervalInMilli())
            .setCurrencyPair(getCurrencyPair());
        final CountingCXStreamListener<OrderBook> listener = spy(new CountingCXStreamListener<>(request, 1));
        final CXStream<OrderBook> stream = CXStreams.openOrderBookSnapshotStream(getCxId(), request, listener);
        listener.await();
        stream.close();

        Assert.assertTrue(listener.getCount() > 0);
        Assert.assertTrue(listener.hasRequestedData());
        verify(listener, atLeastOnce()).onMessage(any(OrderBook.class));
    }
        

    @Test
    public void testInvalidCurrencyPair()
        throws Exception
    {
        threadSleep();
        
        final OpenOrderBookStreamRequest request = new OpenOrderBookStreamRequest()
            .setIntervalInMilli(getIntervalInMilli())
            .setCurrencyPair(getInvalidCurrencyPair());
        final CountingCXStreamListener<OrderBook> listener = spy(new CountingCXStreamListener<>(request, 1));
        final CXStream<OrderBook> stream = CXStreams.openOrderBookSnapshotStream(getCxId(), request, listener);
        listener.await();
        stream.close();
        
        verify(listener, atLeastOnce()).onOpen(any());
        verify(listener, atLeastOnce()).onFailure(any(CXStream.class), any());
    }
}
