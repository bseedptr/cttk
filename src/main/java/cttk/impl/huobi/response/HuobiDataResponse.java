package cttk.impl.huobi.response;

import cttk.dto.UserOrder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HuobiDataResponse
    extends HuobiResponse
{
    String data;

    public UserOrder getUserOrder() {
        return new UserOrder().setOid(data);
    }
}
