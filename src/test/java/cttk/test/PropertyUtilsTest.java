package cttk.test;

import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import cttk.CXIds;

public class PropertyUtilsTest {
    private PropertyUtils utils;

    @Before
    public void setUp() {
        utils = new PropertyUtils();
    }

    @Test(expected = Exception.class)
    public void testBithumb() {
        utils.getProperties(CXIds.BITHUMB);
    }

    @Test(expected = Exception.class)
    public void testBittrex() {
        utils.getProperties(CXIds.BITTREX);
    }

    @Test
    public void testBibox() {
        Map<String, String> properties = utils.getProperties(CXIds.BIBOX);

        Assert.assertNotNull(properties);
        Assert.assertNotNull("Bibox: Key 'totpSecret' is required.", properties.get("totpSecret"));
        Assert.assertNotNull("Bibox: Key 'password' is required.", properties.get("password"));
    }
}