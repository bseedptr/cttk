package cttk.impl.korbit.response;

import static cttk.impl.korbit.KorbitUtils.toStdCurrencySymbol;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import cttk.dto.Balance;
import cttk.dto.Balances;
import lombok.Data;

@Data
public class KorbitBallancesResponse {
    Map<String, KorbitBalance> map = new HashMap<>();

    @JsonAnyGetter
    public Map<String, KorbitBalance> any() {
        return this.map;
    }

    @JsonAnySetter
    public void set(String name, KorbitBalance value) {
        this.map.put(name, value);
    }

    public Balances toBalances() {
        return new Balances()
            .setBalances(getBalanceList());
    }

    public List<Balance> getBalanceList() {
        if (this.map.entrySet() == null)
            return null;
        return this.map.entrySet().stream().map(i -> i.getValue().toBalance(i.getKey())).collect(Collectors.toList());
    }

}

@Data
class KorbitBalance {
    BigDecimal available;
    BigDecimal trade_in_use;
    BigDecimal withdrawal_in_use;

    public Balance toBalance(String currency) {
        final Balance b = new Balance()
            .setCurrency(toStdCurrencySymbol(currency))
            .setAvailable(available)
            .setTradeInUse(trade_in_use)
            .setWithdrawalInUse(withdrawal_in_use);

        b.setInUse(b.getTradeInUse().add(b.getWithdrawalInUse()));
        b.setTotal(b.getAvailable().add(b.getInUse()));

        return b;
    }
}