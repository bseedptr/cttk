package cttk.impl.chargingpolicy;

import cttk.ChargingPolicyProvider;
import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.exception.InvalidChargingPolicyException;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public abstract class AbstractChargingPolicyProvider
    implements ChargingPolicyProvider
{
    protected final String cxId;

    protected void checkParameters(CurrencyPair currencyPair, OrderSide orderSide)
        throws InvalidChargingPolicyException
    {
        if (currencyPair == null || orderSide == null) {
            throw new InvalidChargingPolicyException("Both CurrencyPair and OrderSide are required.")
                .setCxId(cxId)
                .setCurrencyPair(currencyPair)
                .setOrderSide(orderSide);
        }
    }
}
