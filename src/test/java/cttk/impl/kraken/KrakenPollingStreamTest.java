package cttk.impl.kraken;

import cttk.CXIds;
import cttk.impl.AbstractPollingCXStreamTest;

public class KrakenPollingStreamTest
    extends AbstractPollingCXStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.KRAKEN;
    }
}
