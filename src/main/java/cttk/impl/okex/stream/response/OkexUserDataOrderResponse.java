package cttk.impl.okex.stream.response;

import static cttk.OrderStatus.FILLED;
import static cttk.OrderStatus.PARTIALLY_FILLED;
import static cttk.impl.okex.OkexUtils.getMarketSymbol;
import static cttk.impl.util.MathUtils.multiply;
import static cttk.impl.util.MathUtils.subtract;

import java.math.BigDecimal;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.dto.UserOrder;
import cttk.dto.UserTrade;
import cttk.impl.okex.OkexUtils;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class OkexUserDataOrderResponse
    extends OkexUserDataResponse<OkexUserDataOrder>
{
    public UserOrder toUserOrder() {
        return super.data.toUserOrder();
    }

    public UserTrade toUserTrade() {
        return super.data.toUserTrade();
    }

    public boolean isUserTrade() {
        OrderStatus status = OkexUtils.getStatusType(super.data.status);
        return status == FILLED || status == PARTIALLY_FILLED;
    }
}

@Data
class OkexUserDataOrder {
    String symbol;
    BigDecimal tradeAmount;
    Long createdDate;
    String orderId;
    BigDecimal completedTradeAmount;
    BigDecimal averagePrice;
    BigDecimal tradePrice;
    String tradeType;
    Integer status;
    BigDecimal tradeUnitPrice;

    UserOrder toUserOrder() {
        return new UserOrder()
            .setCurrencyPair(getMarketSymbol(symbol))
            .setOid(orderId)
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(createdDate))
            .setSide(OkexUtils.parseOrderSide(tradeType))
            .setStatus(String.valueOf(status))
            .setStatusType(OkexUtils.getStatusType(status))
            .setPricingType(OkexUtils.getPricingType(tradeType))
            .setPrice(tradeUnitPrice)
            .setQuantity(tradeAmount)
            .setAvgPrice(averagePrice)
            .setFilledQuantity(completedTradeAmount)
            .setRemainingQuantity(subtract(tradeAmount, completedTradeAmount))
            .setTotal(multiply(tradeUnitPrice, tradeAmount));
    }

    UserTrade toUserTrade() {
        CurrencyPair pair = getMarketSymbol(symbol);
        OrderSide side = OkexUtils.parseOrderSide(tradeType);
        return new UserTrade()
            .setCurrencyPair(pair)
            .setSide(side)
            .setOid(orderId)
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(createdDate))
            .setPrice(tradeUnitPrice)
            .setQuantity(tradeAmount)
            .setTotal(multiply(tradeUnitPrice, tradeAmount))
            .setFeeCurrency(OkexUtils.guessFeeCurrency(side, pair));
    }
}
