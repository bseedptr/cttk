package cttk.impl.poloniex;

import cttk.CXIds;
import cttk.impl.AbstractPollingCXStreamTest;

public class PoloniexPollingStreamTest
    extends AbstractPollingCXStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.POLONIEX;
    }
}
