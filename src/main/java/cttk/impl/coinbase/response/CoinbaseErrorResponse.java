package cttk.impl.coinbase.response;

import lombok.Data;

@Data
public class CoinbaseErrorResponse {
    String message;
}
