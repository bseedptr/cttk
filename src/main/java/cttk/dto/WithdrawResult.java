package cttk.dto;

import cttk.AbstractCurrencyHolder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class WithdrawResult
    extends AbstractCurrencyHolder<WithdrawResult>
{
    String withdrawId;
}
