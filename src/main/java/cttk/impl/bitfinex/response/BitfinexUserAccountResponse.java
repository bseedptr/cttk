package cttk.impl.bitfinex.response;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import cttk.TradingFeeProvider;
import cttk.dto.TradingFee;
import cttk.impl.feeprovider.ByBaseTradingFeeProvider;
import lombok.Data;

@Data
public class BitfinexUserAccountResponse {
    String taker_fees;
    String maker_fees;
    List<BitfinexFee> fees;

    public TradingFeeProvider getTradingFeeProvider() {
        return new ByBaseTradingFeeProvider()
            .setFee(taker_fees, maker_fees)
            .setFeeMap(getTradingFeeMap());
    }

    private Map<String, TradingFee> getTradingFeeMap() {
        return fees == null ? null
            : fees.stream().collect(Collectors.toMap(i -> i.getPairs(),
                i -> new TradingFee().setFees(i.getTaker_fees(), i.getMaker_fees()).percentToRatio()));
    }
}

@Data
class BitfinexFee {
    String pairs;
    String taker_fees;
    String maker_fees;
}