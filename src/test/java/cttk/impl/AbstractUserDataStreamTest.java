package cttk.impl;

import static cttk.impl.util.Objects.nvl;
import static cttk.test.TestUtils.hasTestProfile;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import cttk.CXClientFactory;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.OrderVerifier;
import cttk.OrderVerifierFactory;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.UserData;
import cttk.auth.CXCredentialsProvider;
import cttk.auth.Credential;
import cttk.dto.Balance;
import cttk.dto.Balances;
import cttk.dto.MarketInfo;
import cttk.dto.OrderBook;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.exception.CTTKException;
import cttk.impl.util.MathUtils;
import cttk.request.CancelOrderRequest;
import cttk.request.CreateOrderRequest;
import cttk.request.GetOrderBookRequest;
import cttk.request.OpenUserDataStreamRequest;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractUserDataStreamTest
    extends AbstractCXClientTest
{
    protected Credential credential;
    protected PublicCXClient pubClient;
    protected PrivateCXClient client;
    private final long streamIntervalInMils = 5000L;

    @Before
    public void setup()
        throws CTTKException, IOException
    {
        org.junit.Assume.assumeTrue(hasTestProfile("test-user-data-stream"));
        credential = CXCredentialsProvider.getDefaultCredential(getCxId()).getTradeKey();

        pubClient = CXClientFactory.createPublicCXClient(getCxId());
        client = CXClientFactory.createPrivateCXClient(getCxId(), credential);
    }

    protected class CountingUserDataStreamListener
        implements CXStreamListener<UserData>
    {
        final AtomicInteger count = new AtomicInteger(0);
        final CountDownLatch latch;

        public CountingUserDataStreamListener(final int count) {
            this.latch = new CountDownLatch(count);
        }

        @Override
        public void onMessage(UserData message) {
            count.incrementAndGet();
            logger.debug("UserData:: {}", message);
            latch.countDown();
        }

        @Override
        public void onFailure(CXStream<UserData> stream, Throwable t) {
            logger.error("Fail", t);
        }

        public void await()
            throws InterruptedException
        {
            latch.await(60, TimeUnit.SECONDS);
        }

        public int getCount() {
            return count.get();
        }
    }

    protected class OrderHistoryUserDataStreamListener
        implements CXStreamListener<UserData>
    {
        final CountDownLatch latch = new CountDownLatch(10);
        // key : Balance.inUse
        final Map<BigDecimal, Balance> balanceMap = new HashMap<>();
        final Map<String, Map<OrderStatus, UserOrder>> userOrdersMap = new HashMap<>();
        public UserOrder createdUserOrder = null;
        public Balance balanceAfterOrderCreate = null;
        private final Balance baseBalance;

        public OrderHistoryUserDataStreamListener(Balance baseBalance) {
            this.baseBalance = baseBalance;
        }

        @Override
        public void onMessage(UserData userData) {
            logger.debug("\nUserData:: {}", userData);
            putUserData(userData);
            if (createdUserOrder == null || !userOrdersMap.containsKey(createdUserOrder.getOid()) || userOrdersMap.get(createdUserOrder.getOid()).size() != 2) {
                latch.countDown();
            } else if (balanceMap.containsKey(baseBalance.getInUse())) {
                BigDecimal expectedInUseAfterOrderCreate = baseBalance.getInUse().add(createdUserOrder.getTotal());
                if (balanceMap.containsKey(expectedInUseAfterOrderCreate)) {
                    balanceAfterOrderCreate = balanceMap.get(expectedInUseAfterOrderCreate);
                    countDownToZero();
                } else if (balanceMap.size() >= 2) {
                    balanceAfterOrderCreate = balanceMap.get(findClosestInUse(expectedInUseAfterOrderCreate));
                    countDownToZero();
                }
            }
            else {
                latch.countDown();
            }
        }

        private void putUserData(UserData userData) {
            switch (userData.getType()) {
                case UserOrder: {
                    putUserOrder((UserOrder) userData.getData());
                    break;
                }
                case UserOrders: {
                    UserOrders userOrders = (UserOrders) userData.getData();
                    if (userOrders.getList() != null) {
                        userOrders.getList().stream().filter(Objects::nonNull).forEach(this::putUserOrder);
                    }
                    break;
                }
                case Balance: {
                    Balance balance = (Balance) userData.getData();
                    if (balance.getCurrency().equals(getValidCurrencyPair().getQuoteCurrency())) {
                        balanceMap.put(balance.getInUse(), balance);
                    }
                    break;
                }
                case Balances: {
                    Balance balances = findBalancesByCurrency(userData);
                    this.balanceMap.put(balances.getInUse(), balances);
                    break;
                }
                default:
                    log.debug("Unexpected data type [{}]:{}\n", userData.getType(), userData.getData());
                    break;
            }
        }

        private void putUserOrder(UserOrder order) {
            if (userOrdersMap.containsKey(order.getOid())) {
                userOrdersMap.get(order.getOid()).put(order.getStatusType(), order);
            } else {
                Map<OrderStatus, UserOrder> map = new HashMap<>();
                map.put(order.getStatusType(), order);
                userOrdersMap.put(order.getOid(), map);
            }
        }

        private Balance findBalancesByCurrency(UserData message) {
            return ((Balances) message.getData()).getBalances().stream()
                .filter(balance -> balance.getCurrency().equals(getValidCurrencyPair().getQuoteCurrency())).findFirst().get();
        }

        private BigDecimal findClosestInUse(BigDecimal target) {
            List<BigDecimal> inUseList = new ArrayList<>(balanceMap.keySet());
            BigDecimal closestInUseToTarget = inUseList.remove(inUseList.size() - 1);
            for (BigDecimal inUse : inUseList) {
                BigDecimal val1 = inUse.subtract(target).abs();
                BigDecimal val2 = closestInUseToTarget.subtract(target).abs();
                if (val1.compareTo(val2) < 0) {
                    closestInUseToTarget = inUse;
                }
            }
            return closestInUseToTarget;
        }

        private void countDownToZero() {
            while (latch.getCount() > 0) {
                latch.countDown();
            }
        }

        @Override
        public void onFailure(CXStream<UserData> stream, Throwable t) {
            logger.error("Fail", t);
        }

        public void await()
            throws InterruptedException
        {
            latch.await(60, TimeUnit.SECONDS);
        }
    }

    @Test
    public void testUserDataStream()
        throws Exception
    {
        // given
        Balance baseBalance = client.getAllBalances().getBalances().stream()
            .filter(balance -> balance.getCurrency().equals(getValidCurrencyPair().getQuoteCurrency())).findFirst().get();
        OrderHistoryUserDataStreamListener listener = new OrderHistoryUserDataStreamListener(baseBalance);
        CXStream<UserData> stream = CXClientFactory
            .createUserDataStreamOpener(getCxId(), credential)
            .openUserDataStream(new OpenUserDataStreamRequest().setIntervalInMilli(streamIntervalInMils), listener);

        // when
        if (useThread()) {
            new Thread(() -> createOrderAndCancelOrder(listener)).start();
        } else {
            createOrderAndCancelOrder(listener);
        }

        // then
        listener.await();
        stream.close();

        Map<OrderStatus, UserOrder> orderMap = listener.userOrdersMap.get(listener.createdUserOrder.getOid());
        Assert.assertTrue(orderMap.containsKey(OrderStatus.UNFILLED));
        Assert.assertTrue(orderMap.containsKey(OrderStatus.CANCELED));
        assertBalance(listener.createdUserOrder, baseBalance, listener.balanceAfterOrderCreate, listener.balanceMap.get(baseBalance.getInUse()));
    }

    protected boolean useThread() {
        return false;
    }

    protected void createOrderAndCancelOrder(OrderHistoryUserDataStreamListener listener) {
        try {
            Thread.sleep(100l);

            final CurrencyPair currencyPair = getValidCurrencyPair();

            final OrderBook orderBook = pubClient.getOrderBook(new GetOrderBookRequest().setCurrencyPair(currencyPair));
            final BigDecimal maxBid = orderBook.getHighestBid().orElseThrow(() -> new NullPointerException("max bid not exist"));

            final MarketInfo marketInfo = pubClient.getMarketInfos().getMarketInfo(currencyPair);
            final OrderVerifier orderVerifier = OrderVerifierFactory.create(getCxId(), marketInfo);

            final BigDecimal price = orderVerifier.roundPrice(MathUtils.divide(maxBid, new BigDecimal("2"))).stripTrailingZeros();
            final BigDecimal quantity = getBuyTestQuantity();

            logger.info("{}", marketInfo);
            logger.info("Test ask with\n"
                + "    Current Min Ask: {}\n"
                + "    Test Ask       : {}\n"
                + "    Quantity       : {}\n"
                + "    Total          : {}\n"
                + "    Min Total      : {}",
                maxBid, price, quantity, price.multiply(quantity), marketInfo.getMinTotal());

            final CreateOrderRequest createOrderRequest = new CreateOrderRequest()
                .setCurrencyPair(getValidCurrencyPair())
                .setSide(OrderSide.BUY)
                .setQuantity(quantity)
                .setPrice(price);

            UserOrder userOrder = client.createOrder(createOrderRequest);

            logger.info("Order result\n"
                + "    ORDER: {}", userOrder);

            Assert.assertNotNull(userOrder);
            Assert.assertNotNull(userOrder.getOid());
            listener.createdUserOrder = userOrder;

            Thread.sleep(streamIntervalInMils * (long) 2.5);

            client.cancelOrder(new CancelOrderRequest()
                .setCurrencyPair(userOrder)
                .setOrderId(userOrder.getOid())
                .setClientOrderId(userOrder.getClientOrderId())
                .setSide(userOrder.getSide())
                .setPrice(userOrder.getPrice())
                .setQuantity(userOrder.getQuantity()));

            logger.info("Order making canceling Process done!!");
        } catch (Exception e) {
            logger.error("Unexpected error", e);
            Assert.fail();
        }
    }

    protected BigDecimal getBuyTestQuantity() {
        if (testConfig == null) readTestConfig();
        return testConfig == null ? null : nvl(testConfig.getBuyTestQuantity(), new BigDecimal("0.2"));
    }

    protected void assertBalance(UserOrder userOrder, Balance baseBalance, Balance balanceAfterOrderCreate, Balance balanceAfterOrderCancel) {
        Assert.assertEquals(baseBalance.getTotal(), balanceAfterOrderCreate.getTotal());

        // 1) prevInUse + total + fee = postInUse 
        // 2) prevAvail - total - fee = postAvail
        // 3) prevTotal               = postTotal
        // fee computation may be inaccurate owing to lack of info to compute it
        // we allow error smaller than total because fee cannot be large than total

        final BigDecimal error1 = baseBalance.getInUse().add(userOrder.getTotal()).subtract(balanceAfterOrderCreate.getInUse()).abs();
        final BigDecimal error2 = baseBalance.getAvailable().subtract(userOrder.getTotal()).subtract(balanceAfterOrderCreate.getAvailable()).abs();

        log.info("1) prevInUse + total - postInUse = error1 : {} + {} - {} = {}", baseBalance.getInUse(), userOrder.getTotal(), balanceAfterOrderCreate.getInUse(), error1);
        log.info("2) prevAvail - total - postAvail = error2 : {} - {} - {} = {}", baseBalance.getAvailable(), userOrder.getTotal(), balanceAfterOrderCreate.getAvailable(), error2);

        Assert.assertTrue(error1.compareTo(error2) == 0);
        Assert.assertTrue(error1.compareTo(userOrder.getTotal()) < 0);
        Assert.assertTrue(error2.compareTo(userOrder.getTotal()) < 0);

        Assert.assertEquals(balanceAfterOrderCreate.getTotal(), balanceAfterOrderCancel.getTotal());
    }
}
