package cttk.impl.bibox.response;

import static cttk.dto.OrderBook.SimpleOrder;

import java.math.BigDecimal;

import cttk.dto.OrderBook;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BiboxOrderBookResponse
    extends BiboxResultResponse<BiboxOrderBook>
{
    public OrderBook getOrderBook() {
        return result.toOrderBook();
    }
}

@Data
class BiboxOrderBookPriceVolumePair {
    BigDecimal price;
    BigDecimal volume;

    public SimpleOrder toSimpleOrder() {
        return new SimpleOrder(price, volume);
    }
}