package cttk.util;

import static cttk.CXIds.BIBOX;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyLong;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;
import cttk.exception.CXAPIRequestException;
import cttk.impl.AbstractPrivateClientDeltaTest;
import cttk.impl.util.ExNotRaisedException;
import cttk.impl.util.Timer;
import lombok.extern.slf4j.Slf4j;

@RunWith(PowerMockRunner.class)
@PrepareForTest(DeltaUtils.class)
@Slf4j
public class DeltaUtilsTest
    extends AbstractPrivateClientDeltaTest
{
    @Test
    public void testSleep() {
        boolean result = DeltaUtils.sleep(500L);

        assertTrue(result);
    }

    @Test
    public void testSleepWhenError()
        throws InterruptedException
    {
        PowerMockito.spy(Thread.class);
        PowerMockito.doThrow(new InterruptedException()).when(Thread.class);
        Thread.sleep(anyLong());

        boolean result = DeltaUtils.sleep(50L);

        assertFalse(result);
    }

    @Test
    public void testSleepWithException()
        throws CXAPIRequestException
    {
        Timer timer = new Timer();
        DeltaUtils.sleepWithException(500L, BIBOX);
        long interval = timer.stop().getIntervalInMilli();

        log.debug("interval: {}", interval);

        assertThat(interval, greaterThanOrEqualTo(500L));
    }

    @Test
    public void testSleepWithExceptionWhenError()
        throws Exception
    {
        PowerMockito.spy(Thread.class);
        PowerMockito.doThrow(new InterruptedException()).when(Thread.class);
        Thread.sleep(anyLong());

        try {
            DeltaUtils.sleepWithException(500L, BIBOX);
        } catch (CXAPIRequestException e) {
            assertEquals(e.getMessage(), "An error occurred while processing delta API. Try agin later.");
            assertEquals(e.getCxId(), BIBOX);
            return;
        }

        throw new ExNotRaisedException();
    }

    @Test
    public void collectToMapOfUserOrders() {
        UserOrders userOrders = new UserOrders().addAll(makeUserOrderFixtures("1", "1", "2", "2", "3", "3", "3", "4"));
        Map<String, UserOrder> map = DeltaUtils.convertToMap(userOrders);
        List<String> ids = map.values().stream().map(UserOrder::getOid).collect(toList());

        assertThat(ids, containsInAnyOrder("1", "2", "3", "4"));
    }

    @Test
    public void collectToMapOfUserTrades() {
        UserTrades userTrades = new UserTrades().addAll(makeUserTradeFixtures("1", "1", "2", "2", "3", "3", "3", "4"));
        Map<String, UserTrade> map = DeltaUtils.convertToMap(userTrades);
        List<String> ids = map.values().stream().map(UserTrade::getTid).collect(toList());

        assertThat(ids, containsInAnyOrder("1", "2", "3", "4"));
    }

    @Test
    public void collectToMapOfTransfers() {
        Transfers transfers = new Transfers().addAll(makeTransferFixtures("1", "1", "2", "2", "3", "3", "3", "4"));
        Map<String, Transfer> map = DeltaUtils.convertToMap(transfers);
        List<String> ids = map.values().stream().map(Transfer::getTid).collect(toList());

        assertThat(ids, containsInAnyOrder("1", "2", "3", "4"));
    }

    @Test
    public void collectUserOrdersFromMap() {
        Map<String, UserOrder> map = new HashMap<>();
        map.put("1", makeUserOrderFixture("1"));
        map.put("2", makeUserOrderFixture("2"));
        map.put("3", makeUserOrderFixture("3"));
        UserOrders userOrders = DeltaUtils.convertUserOrdersFromMap(map);
        List<String> ids = userOrders.getList().stream().map(UserOrder::getOid).collect(toList());

        assertThat(ids, containsInAnyOrder("1", "2", "3"));
    }

    @Test
    public void collectTransfersFromMap() {
        Map<String, Transfer> map = new HashMap<>();
        map.put("1", makeTransferFixture("1"));
        map.put("2", makeTransferFixture("2"));
        map.put("3", makeTransferFixture("3"));
        Transfers transfers = DeltaUtils.convertTransfersFromMap(map);
        List<String> ids = transfers.getList().stream().map(Transfer::getTid).collect(toList());

        assertThat(ids, containsInAnyOrder("1", "2", "3"));
    }

    @Test
    public void collectUserTradesFromMap() {
        Map<String, UserTrade> map = new HashMap<>();
        map.put("1", makeUserTradeFixture("1"));
        map.put("2", makeUserTradeFixture("2"));
        map.put("3", makeUserTradeFixture("3"));
        UserTrades userTrades = DeltaUtils.convertUserTradesFromMap(map);
        List<String> ids = userTrades.getList().stream().map(UserTrade::getTid).collect(toList());

        assertThat(ids, containsInAnyOrder("1", "2", "3"));
    }

    @Test
    public void calcSeconds() {
        Transfer prevLast = makeTransferFixture("1");
        Transfer last = makeTransferFixture("1");
        int s = DeltaUtils.calcNextSeconds(prevLast, last);

        assertEquals(s, 0);
    }

    @Test
    public void calcSecondsWhenDifferent() {
        Transfer prevLast = makeTransferFixture("1");
        Transfer last = makeTransferFixture("2");
        int s = DeltaUtils.calcNextSeconds(prevLast, last);

        assertEquals(s, 1);
    }
}