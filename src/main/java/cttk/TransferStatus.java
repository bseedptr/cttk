package cttk;

public enum TransferStatus {
    PENDING,
    CONFIRMED,
    UNCONFIRMED,
    SUCCESS,
    FAILED,
    CANCELED,
    REJECTED,
    UNKNOWN
}
