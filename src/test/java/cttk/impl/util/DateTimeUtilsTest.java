package cttk.impl.util;

import java.time.ZonedDateTime;

import org.junit.Assert;
import org.junit.Test;

public class DateTimeUtilsTest {
    @Test
    public void testToISODateString() {
        final ZonedDateTime zdt = ZonedDateTime.parse("2018-01-01T00:00:00Z");
        final String dateStr = DateTimeUtils.toISODateString(zdt);
        Assert.assertEquals("2018-01-01", dateStr);
    }

    @Test
    public void testIfFromIsNullThenReturnFalse() {
        ZonedDateTime from = null;
        ZonedDateTime to = ZonedDateTime.parse("2018-02-01T00:00:00Z");

        Assert.assertFalse("2018/02/01 is later than 2018/01/01.", DateTimeUtils.isBefore(from, to));
    }

    @Test
    public void testIsBeforeIfToIsNullThenReturnFalse() {
        ZonedDateTime from = ZonedDateTime.parse("2018-01-01T00:00:00Z");
        ZonedDateTime to = null;

        Assert.assertFalse("2018/02/01 is later than 2018/01/01.", DateTimeUtils.isBefore(from, to));
    }

    @Test
    public void testIsBeforeIfFromIsLaterThanToThenReturnFalse() {
        ZonedDateTime from = ZonedDateTime.parse("2018-02-01T00:00:00Z");
        ZonedDateTime to = ZonedDateTime.parse("2018-01-01T00:00:00Z");

        Assert.assertFalse("2018/02/01 is later than 2018/01/01.", DateTimeUtils.isBefore(from, to));
    }

    @Test
    public void testIsBeforeIfToIsLaterThanFromThenReturnTrue() {
        ZonedDateTime from = ZonedDateTime.parse("2018-01-01T00:00:00Z");
        ZonedDateTime to = ZonedDateTime.parse("2018-02-01T00:00:00Z");

        Assert.assertTrue("2018/02/01 is later than 2018/01/01.", DateTimeUtils.isBefore(from, to));
    }
}
