package cttk;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface DateTimeGettableListHolder<C extends DateTimeGettableListHolder<C, T>, T extends DateTimeGettable> {
    List<T> getList();

    C setList(List<T> list);

    @JsonIgnore
    default boolean isEmpty() {
        return getList() == null || getList().isEmpty();
    }

    @JsonIgnore
    default boolean isNotEmpty() {
        return !isEmpty();
    }

    default int size() {
        return isEmpty() ? 0 : getList().size();
    }

    default T get(int idx) {
        return idx >= 0 && idx < size() ? getList().get(idx) : null;
    }

    default T first() {
        return get(0);
    }

    @SuppressWarnings("unchecked")
    default C addAll(List<T> list) {
        if (list != null) {
            if (getList() == null) setList(new ArrayList<>());
            getList().addAll(list);
        }
        return (C) this;
    }

    @SuppressWarnings("unchecked")
    default C addAll(C listHolder) {
        if (listHolder != null) {
            addAll(listHolder.getList());
        }
        return (C) this;
    }

    default Optional<T> findOneBy(Predicate<T> predicate) {
        return isEmpty()
            ? Optional.empty()
            : getList().stream().filter(predicate).findFirst();
    }

    default List<T> findBy(Predicate<T> predicate) {
        return isEmpty() ? null
            : getList().stream()
                .filter(predicate)
                .collect(Collectors.toList());
    }

    default List<T> findByDateTimeLTE(final ZonedDateTime endDateTime) {
        return findBy(i -> !i.getDateTime().isAfter(endDateTime));
    }

    default List<T> findByDateTimeGTE(final ZonedDateTime startDateTime) {
        return findBy(i -> !i.getDateTime().isBefore(startDateTime));
    }

    default C filterBy(Predicate<T> predicate) {
        return setList(findBy(predicate));
    }

    default C filterByDateTimeLTE(ZonedDateTime endDateTime) {
        return filterBy(i -> !i.getDateTime().isAfter(endDateTime));
    }

    default C filterByDateTimeGTE(ZonedDateTime startDateTime) {
        return filterBy(i -> !i.getDateTime().isBefore(startDateTime));
    }

    @JsonIgnore
    default ZonedDateTime getMinDateTime() {
        return isEmpty() ? null : getOldest().getDateTime();
    }

    @JsonIgnore
    default Long getMinEpochInSecond() {
        return toEpochSecond(getMinDateTime());
    }

    @JsonIgnore
    default Long getMinEpochInMilli() {
        return toEpochMilli(getMinDateTime());
    }

    @JsonIgnore
    default ZonedDateTime getMaxDateTime() {
        return isEmpty() ? null : getLatest().getDateTime();
    }

    @JsonIgnore
    default Long getMaxEpochInSecond() {
        return toEpochSecond(getMaxDateTime());
    }

    @JsonIgnore
    default Long getMaxEpochInMilli() {
        return toEpochMilli(getMaxDateTime());
    }

    @JsonIgnore
    default T getOldest() {
        return isEmpty()
            ? null
            : getList().stream()
                .min((a, b) -> a.getDateTime().compareTo(b.getDateTime()))
                .get();
    }

    @JsonIgnore
    default T getLatest() {
        return isEmpty()
            ? null
            : getList().stream()
                .max((a, b) -> a.getDateTime().compareTo(b.getDateTime()))
                .get();
    }

    @JsonIgnore
    default boolean hasOlderOne(ZonedDateTime dateTime) {
        if (isEmpty()) return false;
        return getMinDateTime().isBefore(dateTime);
    }

    @JsonIgnore
    default boolean hasLaterOne(ZonedDateTime dateTime) {
        if (isEmpty()) return false;
        return getMaxDateTime().isAfter(dateTime);
    }

    public static Long toEpochSecond(ZonedDateTime zdt) {
        return zdt == null ? null : zdt.toEpochSecond();
    }

    public static Long toEpochMilli(ZonedDateTime zdt) {
        return zdt == null ? null : zdt.toInstant().toEpochMilli();
    }
}
