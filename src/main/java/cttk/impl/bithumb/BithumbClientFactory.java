package cttk.impl.bithumb;

import cttk.CXClientFactory;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.UserDataStreamOpener;
import cttk.auth.Credential;
import cttk.exception.CTTKException;
import cttk.impl.bithumb.stream.BithumbUserDataStreamOpener;

public class BithumbClientFactory
    implements CXClientFactory
{
    @Override
    public PublicCXClient create() {
        return new BithumbPublicClient();
    }

    @Override
    public PrivateCXClient create(Credential credential) {
        return new BithumbPrivateClient(credential);
    }

    @Override
    public UserDataStreamOpener createUserDataStreamOpener(Credential credential)
        throws CTTKException
    {
        return new BithumbUserDataStreamOpener(create(), create(credential));
    }
}
