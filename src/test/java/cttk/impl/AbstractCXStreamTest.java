package cttk.impl;

import static cttk.test.TestUtils.hasTestProfile;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import cttk.AbstractCurrencyPair;
import cttk.CXClientFactory;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.CXStreamOpener;
import cttk.CurrencyPair;
import cttk.DateTimeHolder;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Trade;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.request.OpenOrderBookStreamRequest;
import cttk.request.OpenTickerStreamRequest;
import cttk.request.OpenTradeStreamRequest;

public abstract class AbstractCXStreamTest
    extends AbstractCXClientTest
{
    protected CXStreamOpener streamOpener;

    @Before
    public void setup()
        throws CTTKException
    {
        org.junit.Assume.assumeFalse(hasTestProfile("skip-public"));
        streamOpener = CXClientFactory.createCXStreamOpener(getCxId());
    }

    protected long getIntervalInMilli() {
        return 1000l;
    }

    @Test
    public void testTickerStream()
        throws InterruptedException, Exception
    {
        threadSleep();

        final OpenTickerStreamRequest request = new OpenTickerStreamRequest()
            .setIntervalInMilli(getIntervalInMilli())
            .setCurrencyPair(getCurrencyPair());
        final CountingCXStreamListener<Ticker> listener = new CountingCXStreamListener<>(request, 1);
        final CXStream<Ticker> stream = streamOpener.openTickerStream(request, listener);
        listener.await();
        stream.close();

        Assert.assertTrue(listener.getCount() > 0);
        Assert.assertTrue(listener.hasRequestedData());
    }

    @Test
    public void testOrderBookStream()
        throws InterruptedException, Exception
    {
        threadSleep();

        final OpenOrderBookStreamRequest request = new OpenOrderBookStreamRequest()
            .setIntervalInMilli(getIntervalInMilli())
            .setCurrencyPair(getCurrencyPair());
        final CountingCXStreamListener<OrderBook> listener = new CountingCXStreamListener<>(request, 2);
        // TODO use CXStreams to test orderbook stream
        // CXStreams.openOrderBookSnapshotStream(getCxId(), request, listener);
        final CXStream<OrderBook> stream = streamOpener.openOrderBookStream(request, listener);
        listener.await();
        stream.close();

        Assert.assertTrue(listener.getCount() > 0);
        Assert.assertTrue(listener.hasRequestedData());
    }

    @Test
    public void testTradeStream()
        throws InterruptedException, Exception
    {
        threadSleep();

        final OpenTradeStreamRequest request = new OpenTradeStreamRequest()
            .setIntervalInMilli(getIntervalInMilli())
            .setCurrencyPair(getCurrencyPair());
        final CountingCXStreamListener<Trades> listener = new CountingCXStreamListener<>(request, 2);
        final CXStream<Trades> stream = streamOpener.openTradeStream(request, listener);
        listener.await();
        stream.close();

        Assert.assertTrue(listener.getCount() > 0);
        Assert.assertTrue(listener.hasRequestedData());
    }

    protected class CountingCXStreamListener<T extends AbstractCurrencyPair<T>>
        implements CXStreamListener<T>
    {
        final CurrencyPair currencyPair;

        final AtomicInteger count = new AtomicInteger(0);
        final CountDownLatch latch;
        boolean hasRequestedData = false;

        public CountingCXStreamListener(final CurrencyPair currencyPair, final int count) {
            this.currencyPair = currencyPair;
            this.latch = new CountDownLatch(count);
        }

        @Override
        public void onMessage(T message) {
            count.incrementAndGet();
            logger.debug("{}", message);

            hasRequestedData = getCxId().equals(message.getCxId())
                && currencyPair.getBaseCurrency().equals(message.getBaseCurrency())
                && currencyPair.getQuoteCurrency().equals(message.getQuoteCurrency());

            if (hasRequestedData && message instanceof DateTimeHolder) {
                hasRequestedData = ((DateTimeHolder<?>) message).getDateTime() != null;
            }

            if (hasRequestedData && message instanceof Trades) {
                Trades trades = ((Trades) message);
                hasRequestedData = trades.size() > 0;
                if (trades.size() > 0) {
                    for (Trade t : trades.getTrades()) {
                        if (t.getSno() == null) {
                            hasRequestedData = false;
                            break;
                        }
                    }
                } else {
                    hasRequestedData = false;
                }
            }

            latch.countDown();
        }

        @Override
        public void onFailure(CXStream<T> stream, Throwable t) {
            logger.error("Fail", t);
        }

        public void await()
            throws InterruptedException
        {
            latch.await(60, TimeUnit.SECONDS);
        }

        public int getCount() {
            return count.get();
        }

        public boolean hasRequestedData() {
            return hasRequestedData;
        }

        public void setHasRequestedData(boolean hasRequestedData) {
            this.hasRequestedData = hasRequestedData;
        }
    }
}
