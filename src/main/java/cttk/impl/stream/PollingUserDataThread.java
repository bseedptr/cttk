package cttk.impl.stream;

import java.io.Closeable;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.CurrencyPair;
import cttk.OrderStatus;
import cttk.PrivateCXClient;
import cttk.SortType;
import cttk.UserData;
import cttk.dto.Balances;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrades;
import cttk.exception.CTTKException;
import cttk.impl.util.RetryUtils;
import cttk.impl.util.ThrowableSupplier;
import cttk.impl.util.Timer;
import cttk.request.GetUserOrderHistoryDeltaRequest;
import cttk.request.GetUserOrderHistoryRequest;
import cttk.request.GetUserOrderRequest;
import cttk.request.GetUserTradesDeltaRequest;
import cttk.request.OpenUserDataStreamRequest;
import cttk.util.SleepUtils;
import cttk.util.UserOrdersDiff;
import lombok.Getter;
import lombok.ToString;

public class PollingUserDataThread
    extends Thread
    implements Closeable
{
    protected static final UserOrders EMPTY_USER_ORDERS = new UserOrders().setList(Collections.emptyList());

    protected Logger logger = LoggerFactory.getLogger(getClass());

    protected final CXStream<UserData> cxStream;
    protected final PrivateCXClient client;
    protected final CXStreamListener<UserData> listener;
    protected final OpenUserDataStreamRequest request;

    protected final long intervalInMilli;
    protected final long balanceCheckIntervalInMilli;
    protected final int maxPollRetryCount;
    protected long lastBalanceCheckEpochMilli = 0;

    protected List<UserOrder> statusUpdatedUserOrderFailList = new ArrayList<>();
    protected List<UserOrder> userOrdersOfFailToGetUserTrades = new ArrayList<>();

    protected UserOrders prevOrders = new UserOrders().setList(new ArrayList<>());
    protected UserOrder prevCloseOrders = new UserOrder().setDateTime(ZonedDateTime.now());
    protected boolean isOpened = true;

    public PollingUserDataThread(
        final CXStream<UserData> cxStream,
        final PrivateCXClient client,
        final OpenUserDataStreamRequest request,
        final CXStreamListener<UserData> listener)
    {
        this.cxStream = cxStream;
        this.client = client;
        this.intervalInMilli = request.getIntervalInMilli();
        this.balanceCheckIntervalInMilli = request.getBalanceCheckIntervalInMilli();
        this.maxPollRetryCount = request.getMaxPollRetryCount();
        this.request = request;
        this.listener = listener;
    }

    @Override
    public void run() {
        final Timer timer = new Timer();
        listener.onOpen(cxStream);
        while (isOpened) {
            timer.start();
            try {
                runService();
            } catch (Exception e) {
                logger.error("Unable to get data from source", e);
                listener.onFailure(cxStream, e);
                break;
            }
            timer.stop();

            try {
                final long sleepMillis = intervalInMilli - timer.getIntervalInMilli();
                if (sleepMillis <= 0) continue;
                logger.debug("sleep {}ms", sleepMillis);
                sleep(sleepMillis);
            } catch (InterruptedException e) {
                logger.error("Stream interrupted", e);
            }
        }
        logger.info("Stream closed");
        listener.onClosed(cxStream, 0, "");
    }

    protected void runService()
        throws CTTKException
    {
        final List<UserOrder> closeOrderDuringInterval = getCloseOrdersDuringInterval();
        SleepUtils.sleep(1500L);
        final UserOrders currentOpenUserOrders = getAllOpenUserOrders();
        onMessageUserData(calculateUserOrderDiff(closeOrderDuringInterval, currentOpenUserOrders));
        setPrevOrders(currentOpenUserOrders);
    }

    protected List<UserOrder> getCloseOrdersDuringInterval() {
        UserOrders closeOrders = getUserOrderHistoryFromPrevCloseOrder();
        if (closeOrders.isEmpty()) return Collections.emptyList();

        this.prevCloseOrders = closeOrders.getLatest();
        Set<String> prevOidSet = getPrevOrderOIDSet();
        return prevOidSet.isEmpty() ? closeOrders.getList() : closeOrders.getList().stream().filter(userOrder -> !prevOidSet.contains(userOrder.getOid())).collect(Collectors.toList());
    }

    private UserOrders getUserOrderHistoryFromPrevCloseOrder() {
        try {
            GetUserOrderHistoryDeltaRequest deltaRequest = new GetUserOrderHistoryDeltaRequest().setStartDateTime(prevCloseOrders.getDateTime()).setEndDateTime(ZonedDateTime.now());
            UserOrders currencyCloseUserOrder = getDataWithRetry(() -> client.getUserOrderHistory(deltaRequest));
            return currencyCloseUserOrder == null ? EMPTY_USER_ORDERS : currencyCloseUserOrder.filterBy(userOrder -> userOrder.getDateTime().isAfter(prevCloseOrders.getDateTime()));
        } catch (CTTKException e) {
            logger.error("unable to get order history", e);
            return EMPTY_USER_ORDERS;
        }
    }

    protected Set<String> getPrevOrderOIDSet() {
        if (this.prevOrders == null) this.prevOrders = new UserOrders();
        return this.prevOrders.isEmpty() ? Collections.emptySet() : this.prevOrders.getList().stream().map(UserOrder::getOid).collect(Collectors.toSet());
    }

    protected UserOrders getAllOpenUserOrders()
        throws CTTKException
    {
        return getDataWithRetry(() -> client.getAllOpenUserOrders(true));
    }

    private UserOrdersDiff calculateUserOrderDiff(List<UserOrder> closeOrderDuringInterval, UserOrders currentOpenUserOrders) {
        final UserOrdersDiff ordersDiff = UserOrdersDiff.diff(this.prevOrders, currentOpenUserOrders);
        List<UserOrder> closeOrders = ordersDiff.isRemoved() ? getUpdatedCloseOrders(ordersDiff.getRemoved()) : Collections.emptyList();
        ordersDiff.setClosed(Stream.concat(closeOrders.stream(), closeOrderDuringInterval.stream()).collect(Collectors.toList()));
        return ordersDiff;
    }

    private void onMessageUserData(UserOrdersDiff ordersDiff)
        throws CTTKException
    {
        if (ordersDiff.isChanged() || ordersDiff.existClosed()) {
            onMessageChangedUserOrders(ordersDiff);
            onMessageChangedUserTrades(getTradedUserOrders(ordersDiff));
        } else if (needToCheckBalance()) {
            listener.onMessage(UserData.of(getBalancesAndUpdateCheckTime()));
        }
    }

    private UserOrders getTradedUserOrders(UserOrdersDiff ordersDiff) {
        UserOrders tradedUserOrders = new UserOrders().addAll(ordersDiff.getValueChanged()).addAll(ordersDiff.getTradedOrders());
        if (!this.userOrdersOfFailToGetUserTrades.isEmpty()) {
            logger.error("fail to get user trades from user orders, thus retry get user trades {} ", this.userOrdersOfFailToGetUserTrades);
            tradedUserOrders.addAll(this.userOrdersOfFailToGetUserTrades);
            this.userOrdersOfFailToGetUserTrades = new ArrayList<>();
        }
        return tradedUserOrders;
    }

    protected List<UserOrder> getUpdatedCloseOrders(List<UserOrder> orders) {
        UserOrders closedUserOrders = getUserOrderHistory();
        if (closedUserOrders.isEmpty()) {
            statusUpdatedUserOrderFailList.addAll(orders);
            return Collections.emptyList();
        }
        UserOrdersFindResult result = UserOrdersFindResult.findInSearchTargetList(orders, closedUserOrders.getList());
        if (result.existMissingOrder()) {
            statusUpdatedUserOrderFailList.addAll(result.getMissingOrders());
        }
        return result.getFoundOrders();
    }

    protected UserOrders getUserOrderHistory() {
        return getUserOrderHistory(null);
    }

    protected UserOrders getUserOrderHistory(OrderStatus status) {
        try {
            return getDataWithRetry(() -> client.getUserOrderHistory(new GetUserOrderHistoryRequest().setSort(SortType.DESC).setStartDateTime(ZonedDateTime.now().minusMinutes(5)).setStatus(status)));
        } catch (CTTKException e) {
            logger.error("unable to get user order history", e);
            return EMPTY_USER_ORDERS;
        }
    }

    private void onMessageChangedUserOrders(UserOrdersDiff ordersDiff)
        throws CTTKException
    {
        final UserOrders changed = new UserOrders()
            .addAll(ordersDiff.getAdded())
            .addAll(ordersDiff.getValueChanged())
            .addAll(ordersDiff.getClosed());

        listener.onMessage(UserData.of(changed));
        listener.onMessage(UserData.of(getBalancesAndUpdateCheckTime()));
    }

    protected void onMessageChangedUserTrades(UserOrders ordersToCheckTrade) {
        if (ordersToCheckTrade.isNotEmpty()) {
            final UserTrades userTrades = getUserTrades(ordersToCheckTrade);
            if (userTrades.isNotEmpty()) {
                final Set<String> orderIds = ordersToCheckTrade.getDistinctOrderIds();
                userTrades.filterByOrderIdIn(orderIds);
                if (userTrades.isNotEmpty()) listener.onMessage(UserData.of(userTrades));
            }
        }
    }

    protected UserTrades getUserTrades(UserOrders userOrders) {
        UserTrades userTrades = isPossibleGetTradesAtOnce() ? getUserTradesAtOnce(userOrders.getOldest()) : getUserTradesEachCurrency(userOrders);
        return userTrades == null ? new UserTrades() : userTrades;
    }

    private boolean isPossibleGetTradesAtOnce() {
        String cxId = this.client.getCxId();
        return cxId.equals(CXIds.POLONIEX) || cxId.equals(CXIds.KRAKEN) || cxId.equals(CXIds.HITBTC) || cxId.equals(CXIds.UPBIT);
    }

    protected UserTrades getUserTradesAtOnce(UserOrder fromUserOrder) {
        GetUserTradesDeltaRequest deltaRequest = new GetUserTradesDeltaRequest().setStartDateTime(fromUserOrder.getDateTime()).setEndDateTime(ZonedDateTime.now());
        try {
            return getDataWithRetry(() -> this.client.getUserTrades(deltaRequest));
        } catch (CTTKException e) {
            logger.error("Unable to get user trades", e);
            userOrdersOfFailToGetUserTrades.add(fromUserOrder);
            return new UserTrades();
        }
    }

    private UserTrades getUserTradesEachCurrency(UserOrders userOrders) {
        final UserTrades userTrades = new UserTrades();
        GetUserTradesDeltaRequest deltaRequest = new GetUserTradesDeltaRequest().setStartDateTime(userOrders.getOldest().getDateTime()).setEndDateTime(ZonedDateTime.now());
        for (CurrencyPair pair : userOrders.getDistinctCurrencyPairList()) {
            try {
                final UserTrades trades = getDataWithRetry(() -> this.client.getUserTrades(deltaRequest.setCurrencyPair(pair)));
                if (trades != null && trades.isNotEmpty()) userTrades.addAll(trades.getTrades());
            } catch (CTTKException e) {
                logger.error("unable to get trade", e);
                userOrders.getList().stream().filter(userOrder -> userOrder.matched(pair)).forEach(userOrdersOfFailToGetUserTrades::add);
            }
        }
        return userTrades;
    }

    private void setPrevOrders(UserOrders currentOpenUserOrders) {
        this.prevOrders = currentOpenUserOrders;
        if (!this.statusUpdatedUserOrderFailList.isEmpty()) {
            logger.error("fail to get updated orders, thus retry get updated user orders {} ", this.statusUpdatedUserOrderFailList);
            this.prevOrders.addAll(this.statusUpdatedUserOrderFailList);
            this.statusUpdatedUserOrderFailList = new ArrayList<>();
        }
    }

    protected boolean needToCheckBalance() {
        return request.isCheckBalanceAutomatically() && System.currentTimeMillis() - lastBalanceCheckEpochMilli > balanceCheckIntervalInMilli;
    }

    protected Balances getBalancesAndUpdateCheckTime()
        throws CTTKException
    {
        logger.debug("Check balances");
        Balances balances = getDataWithRetry(client::getAllBalances);
        lastBalanceCheckEpochMilli = System.currentTimeMillis();
        return balances;
    }

    protected <T> T getDataWithRetry(ThrowableSupplier<T> supplier)
        throws CTTKException
    {
        return RetryUtils.getDataWithRetry(maxPollRetryCount, supplier);
    }

    protected List<UserOrder> getUpdatedCloseOrdersEachOrders(List<UserOrder> orders) {
        final List<UserOrder> updatedOrders = new ArrayList<>(orders.size());
        for (UserOrder order : orders) {
            try {
                updatedOrders.add(getUserOrder(order));
            } catch (CTTKException e) {
                logger.error("Unable to get updated order status {}", order, e);
                statusUpdatedUserOrderFailList.add(order);
            }
        }
        return updatedOrders;
    }

    protected UserOrder getUserOrder(UserOrder order)
        throws CTTKException
    {
        return client.getUserOrder(GetUserOrderRequest.from(order).setWithTrades(true));
    }

    @Override
    public void close()
        throws IOException
    {
        isOpened = false;
        listener.onClosing(cxStream, 0, "");
        logger.info("Stream closing");
    }

    public CXStreamListener<UserData> getListener() {
        return listener;
    }

    @Getter
    @ToString
    protected static class UserOrdersFindResult {
        private final List<UserOrder> foundOrders;
        private final List<UserOrder> missingOrders;

        private UserOrdersFindResult(List<UserOrder> foundOrders, List<UserOrder> missingOrders) {
            this.foundOrders = foundOrders;
            this.missingOrders = missingOrders;
        }

        public boolean existMissingOrder() {
            return !missingOrders.isEmpty();
        }

        public static UserOrdersFindResult findInSearchTargetList(List<UserOrder> findTarget, List<UserOrder> searchTargetList) {
            List<UserOrder> foundList = new ArrayList<>();
            Map<String, UserOrder> findTargetUserOrder = findTarget.stream().collect(Collectors.toMap(UserOrder::getOid, order -> order));
            for (UserOrder updatedUserOrder : searchTargetList) {
                if (findTargetUserOrder.containsKey(updatedUserOrder.getOid())) {
                    foundList.add(updatedUserOrder);
                    findTargetUserOrder.remove(updatedUserOrder.getOid());
                }
                if (findTargetUserOrder.isEmpty()) break;
            }
            return new UserOrdersFindResult(foundList, new ArrayList<>(findTargetUserOrder.values()));
        }
    }
}