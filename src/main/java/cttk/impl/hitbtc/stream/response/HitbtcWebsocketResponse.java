package cttk.impl.hitbtc.stream.response;

import lombok.Data;

@Data
public class HitbtcWebsocketResponse<T> {
    String jsonrpc;
    String method;
    T params;
}
