package cttk.test;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import cttk.CurrencyPair;

public class TestConfig {
    CurrencyPair validCurrencyPair;
    CurrencyPair invalidCurrencyPair;
    BigDecimal buyTestQuantity;
    BigDecimal sellTestQuantity;
    String transferTestCurrency;

    public String getTransferTestCurrency() {
        return transferTestCurrency;
    }

    public void setTransferTestCurrency(String transferTestCurrency) {
        this.transferTestCurrency = transferTestCurrency;
    }

    public CurrencyPair getValidCurrencyPair() {
        return validCurrencyPair;
    }

    public void setValidCurrencyPair(CurrencyPair validCurrencyPair) {
        this.validCurrencyPair = validCurrencyPair;
    }

    public CurrencyPair getInvalidCurrencyPair() {
        return invalidCurrencyPair;
    }

    public void setInvalidCurrencyPair(CurrencyPair invalidCurrencyPair) {
        this.invalidCurrencyPair = invalidCurrencyPair;
    }

    public BigDecimal getBuyTestQuantity() {
        return buyTestQuantity;
    }

    public void setBuyTestQuantity(BigDecimal buyTestQuantity) {
        this.buyTestQuantity = buyTestQuantity;
    }

    public BigDecimal getSellTestQuantity() {
        return sellTestQuantity;
    }

    public void setSellTestQuantity(BigDecimal sellTestQuantity) {
        this.sellTestQuantity = sellTestQuantity;
    }

    public static TestConfig readFromClasspathYaml(final String classpath)
        throws IOException
    {
        try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(classpath)) {

            SimpleModule simpleModule = new SimpleModule();
            simpleModule.addAbstractTypeMapping(CurrencyPair.class, CurrencyPair.SimpleCurrencyPair.class);

            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            mapper.setPropertyNamingStrategy(PropertyNamingStrategy.KEBAB_CASE);
            mapper.registerModule(simpleModule);
            return mapper.readValue(is, TestConfig.class);
        } catch (IOException e) {
            throw e;
        }
    }
}
