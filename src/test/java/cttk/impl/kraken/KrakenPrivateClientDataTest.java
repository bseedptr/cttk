package cttk.impl.kraken;

import static cttk.dto.UserOrders.Category.CLOSED;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import cttk.CXIds;
import cttk.dto.UserOrders;
import cttk.dto.Wallet;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.exception.UnsupportedMethodException;
import cttk.impl.AbstractPrivateCXClientDataTest;
import cttk.impl.util.ExNotRaisedException;
import cttk.request.GetUserOrderHistoryRequest;

public class KrakenPrivateClientDataTest
    extends AbstractPrivateCXClientDataTest
{
    @AfterClass
    public static void cleanup() {
        cleanup(KrakenPrivateClientDataTest.class);
    }

    @Override
    public String getCxId() {
        return CXIds.KRAKEN;
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetUserAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetUserAccount();
    }

    @Test(expected = UnsupportedMethodException.class)
    @Override
    public void testGetBankAccount()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        super.testGetBankAccount();
    }

    @Test
    @Override
    public void testGetWallet()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        // FIXME
        // When 'BTC' is used as requesting currency, get wallet is ok
        // But, it throw invalid key exception with other currencies.
        //super.testGetUserWallet();

        final String currency = "BTC";
        final Wallet wallet = client.getWallet(currency);

        logger.debug("{}", wallet);

        Assert.assertNotNull(wallet);
        Assert.assertNotNull(wallet.getAddresses());
        Assert.assertEquals(currency, wallet.getCurrency());
    }

    @Test
    @Override
    public void testGetAllWallets()
        throws CTTKException
    {
        // FIXME refine later
        // super.testGetAllUserWallets();
    }

    @Test
    public void testGetUserOrderHistoryCategory()
        throws CTTKException
    {
        threadSleep();

        final GetUserOrderHistoryRequest request = new GetUserOrderHistoryRequest()
            .setCurrencyPair(getValidCurrencyPair());
        final UserOrders userOrders = client.getUserOrderHistory(request);

        logger.debug("{}", userOrders);

        Assert.assertEquals(userOrders.getCategory(), CLOSED);
    }

    @Test(expected = ExNotRaisedException.class)
    @Override
    // Kraken is returning all the open user orders and we are filtering it with the currency pair.
    // so there is no unsupported currency exception thrown but only empty array is the return (CTTK-561)
    public void testGetOpenUserOrdersWrongCurrency()
        throws CTTKException
    {
        super.testGetOpenUserOrdersWrongCurrency();
    }

    @Test(expected = ExNotRaisedException.class)
    // Kraken is returning all the open user orders and we are filtering it with the currency pair.
    // so there is no unsupported currency exception thrown but only empty array is the return (CTTK-561)
    public void testGetUserOrderHistoryWrongCurrency()
        throws CTTKException
    {
        super.testGetUserOrderHistoryWrongCurrency();
    }

    @Test(expected = ExNotRaisedException.class)
    // Kraken is returning all the open user orders and we are filtering it with the currency pair.
    // so there is no unsupported currency exception thrown but only empty array is the return (CTTK-561)
    public void testGetUserTradeWrongCurrency()
        throws CTTKException
    {
        super.testGetUserTradeWrongCurrency();
    }
}
