package cttk.impl.hitbtc.response;

import lombok.Data;

@Data
public class HitbtcWithdrawResponse {
    String id;
}
