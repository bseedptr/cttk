package cttk.impl.okex.response;

import cttk.dto.UserOrder;
import lombok.Data;

@Data
public class OkexCreateOrderResponse {
    String order_id;
    String client_oid;
    boolean result;

    public UserOrder toUserOrder() {
        return new UserOrder()
            .setOid(order_id)
            .setClientOrderId(client_oid);
    }
}
