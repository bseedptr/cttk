package cttk.impl.bibox.response;

import lombok.Data;

@Data
public class BiboxResultWrapperResponse<T> {
    protected BiboxResultResponse<T>[] result;

    protected T unwrap() {
        return result[0].result;
    }
}
