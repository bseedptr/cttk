package cttk.impl.binance;

import cttk.CXIds;
import cttk.impl.AbstractOrderVerifierTest;

public class BinanceOrderVerifierTest
    extends AbstractOrderVerifierTest
{
    @Override
    public String getCxId() {
        return CXIds.BINANCE;
    }

    @Override
    protected long getThreadSleepMillis() {
        return 1000l;
    }
}
