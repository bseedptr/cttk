package cttk.impl.upbit.response;

import java.math.BigDecimal;

import cttk.CurrencyPair;
import cttk.dto.Ticker;
import cttk.impl.upbit.UpbitUtils;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;

@Data
public class UpbitTickerResponse {
    String market;
    String trade_date;
    String trade_time;
    String trade_date_kst;
    String trade_time_kst;
    Long trade_timestamp;

    BigDecimal opening_price;
    BigDecimal high_price;
    BigDecimal low_price;
    BigDecimal trade_price;
    BigDecimal prev_closing_price;

    String change;
    BigDecimal change_price;
    BigDecimal change_rate;
    BigDecimal signed_change_price;
    BigDecimal signed_change_rate;
    BigDecimal trade_volume;
    BigDecimal acc_trade_price;
    BigDecimal acc_trade_price_24h;
    BigDecimal acc_trade_volume;
    BigDecimal acc_trade_volume_24h;

    BigDecimal highest_52_week_price;
    String highest_52_week_date;
    BigDecimal lowest_52_week_price;
    String lowest_52_week_date;
    Long timestamp;

    public Ticker toTicker() {
        CurrencyPair currencyPair = UpbitUtils.parseMarketSymbol(market);

        return new Ticker()
            .setCurrencyPair(currencyPair)
            .setLastPrice(trade_price)
            .setOpenPrice(opening_price)
            .setHighPrice(high_price)
            .setLowPrice(low_price)
            .setVolume(acc_trade_volume_24h)
            .setQuoteVolume(acc_trade_price_24h)
            .setChange(signed_change_price)
            .setChangePercent(MathUtils.multiply100(signed_change_rate))
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(timestamp));
    }
}
