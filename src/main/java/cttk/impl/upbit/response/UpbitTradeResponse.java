package cttk.impl.upbit.response;

import java.math.BigDecimal;
import java.math.BigInteger;

import cttk.OrderSide;
import cttk.dto.Trade;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;

@Data
public class UpbitTradeResponse {
    String market;
    String trade_date_utc;
    String trade_time_utc;
    Long timestamp;
    BigDecimal trade_price;
    BigDecimal trade_volume;
    BigDecimal prev_closing_price;
    BigDecimal change_price;
    String ask_bid;

    public Trade toTrade() {
        return new Trade()
            .setSno(BigInteger.valueOf(timestamp))
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(timestamp))
            .setPrice(trade_price)
            .setQuantity(trade_volume)
            .setTotal(trade_price.multiply(trade_volume))
            .setSide(OrderSide.of(ask_bid));
    }
}
