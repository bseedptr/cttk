package cttk.impl.bithumb;

import cttk.CXIds;
import cttk.impl.AbstractAPILimitTest;

public class BithumbAPILimitTest
    extends AbstractAPILimitTest
{
    @Override
    public String getCxId() {
        return CXIds.BITHUMB;
    }

}
