package cttk.impl.kraken.response;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import cttk.TransferType;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
public class KrakenTransfersResponse
    extends KrakenResponse<KrakenTransfers>
{
    public Transfers toTransfers() {
        return new Transfers()
            .setTransfers(result == null ? null : result.getTransfers());
    }
}

@Data
class KrakenTransfers {
    Long count;
    Map<String, KrakenTransfer> ledger;

    List<Transfer> getTransfers() {
        if (ledger == null) return null;
        return ledger.entrySet().stream()
            .filter(e -> "deposit".equals(e.getValue().getType()) || "withdrawal".equals(e.getValue().getType()))
            .map(e -> e.getValue().toTransfer())
            .collect(Collectors.toList());
    }
}

@Data
@Accessors(chain = true)
class KrakenTransfer {
    String refid;
    BigDecimal time;
    String type;
    String aclass;
    String asset;
    BigDecimal amount;
    BigDecimal fee;
    BigDecimal balance;

    Transfer toTransfer() {
        return new Transfer()
            .setCurrency(asset)
            .setType(TransferType.of(type))
            .setTid(refid)
            .setAmount(amount)
            .setFee(fee)
            .setUpdatedDateTime(DateTimeUtils.zdtFromEpochSecond(time))
            .setCompletedDateTime(DateTimeUtils.zdtFromEpochSecond(time));
    }
}
