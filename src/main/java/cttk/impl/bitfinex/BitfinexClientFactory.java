package cttk.impl.bitfinex;

import cttk.CXClientFactory;
import cttk.CXStreamOpener;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.UserDataStreamOpener;
import cttk.auth.Credential;
import cttk.impl.RetryPrivateCXClient;
import cttk.impl.bitfinex.stream.BitfinexStreamOpener;
import cttk.impl.bitfinex.stream.BitfinexUserDataStreamOpener;

public class BitfinexClientFactory
    implements CXClientFactory
{
    @Override
    public PublicCXClient create() {
        return new BitfinexPublicClient();
    }

    @Override
    public PrivateCXClient create(Credential credential) {
        return new RetryPrivateCXClient(5, 5, new BitfinexPrivateClient(credential), "Nonce is too small");
    }

    @Override
    public CXStreamOpener createCXStreamOpener() {
        return new BitfinexStreamOpener();
    }

    @Override
    public UserDataStreamOpener createUserDataStreamOpener(Credential credential) {
        return new BitfinexUserDataStreamOpener(credential);
    }
}
