package cttk.impl;

import cttk.PublicCXClient;
import cttk.dto.MarketInfos;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Tickers;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCXException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.request.GetOrderBookRequest;
import cttk.request.GetRecentTradesRequest;
import cttk.request.GetTickerRequest;
import cttk.request.GetTradeHistoryRequest;

public class PostSettingPublicCXClient
    implements PublicCXClient
{
    private final PublicCXClient client;

    public PostSettingPublicCXClient(PublicCXClient client)
        throws UnsupportedCXException
    {
        super();
        if (client == null) {
            throw new UnsupportedCXException("client cannot be null");
        }
        this.client = client;
    }

    @Override
    public String getCxId() {
        return client.getCxId();
    }

    @Override
    public MarketInfos getMarketInfos()
        throws CTTKException
    {
        return setCxId(client.getMarketInfos());
    }

    @Override
    public Ticker getTicker(GetTickerRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return setDateTime(setCxIdAndCurrencyPair(client.getTicker(request), request));
    }

    @Override
    public Tickers getAllTickers()
        throws CTTKException
    {
        return setCxId(client.getAllTickers());
    }

    @Override
    public OrderBook getOrderBook(GetOrderBookRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return setDateTime(setCxIdAndCurrencyPair(client.getOrderBook(request), request));
    }

    @Override
    public Trades getRecentTrades(GetRecentTradesRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return setCxIdAndCurrencyPair(client.getRecentTrades(request), request);
    }

    @Override
    public Trades getTradeHistory(GetTradeHistoryRequest request)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        return setCxIdAndCurrencyPair(client.getTradeHistory(request), request);
    }
}
