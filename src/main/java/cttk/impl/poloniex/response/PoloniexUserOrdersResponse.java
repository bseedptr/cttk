package cttk.impl.poloniex.response;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.PricingType;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.impl.poloniex.PoloniexUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class PoloniexUserOrdersResponse
    extends ArrayList<PoloniexUserOrder>
{
    private static final long serialVersionUID = 1L;

    public UserOrders toUserOrders(CurrencyPair currencyPair, PricingType pricingType) {
        return new UserOrders()
            .setOrders(toUserOrderList(currencyPair, pricingType));
    }

    private List<UserOrder> toUserOrderList(CurrencyPair currencyPair, PricingType pricingType) {
        return this.stream().map(i -> i.toUserOrder(currencyPair, pricingType)).filter(i -> i != null).collect(Collectors.toList());
    }
}

@Data
class PoloniexUserOrder {
    String orderNumber;
    String type;
    BigDecimal rate;
    BigDecimal startingAmount;
    BigDecimal amount;
    BigDecimal total;
    String date;    // UTC datetime string, 2018-02-09 05:20:08
    BigDecimal margin;

    UserOrder toUserOrder(CurrencyPair currencyPair, PricingType pricingType) {
        if (orderNumber == null) {
            return null;
        }

        return new UserOrder()
            .setCurrencyPair(currencyPair)
            .setOid(orderNumber)
            .setDateTime(PoloniexUtils.parseZDT(date))
            .setSide(OrderSide.of(type))
            .setPricingType(pricingType)
            .setPrice(rate)
            .setQuantity(startingAmount)
            .setFilledQuantity(MathUtils.subtract(startingAmount, amount))
            .setRemainingQuantity(amount)
            .setTotal(total)
            .setStatusType(getStatusType());
    }

    private OrderStatus getStatusType() {
        if (startingAmount.compareTo(amount) == 0) {
            return OrderStatus.UNFILLED;
        } else if (amount.compareTo(BigDecimal.ZERO) == 0) {
            return OrderStatus.FILLED;
        } else {
            return OrderStatus.PARTIALLY_FILLED;
        }
    }
}