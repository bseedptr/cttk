package cttk.impl;

import static cttk.test.TestUtils.hasTestProfile;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.time.ZonedDateTime;

import org.junit.Before;
import org.junit.Test;

import cttk.CXClientFactory;
import cttk.PublicCXClient;
import cttk.dto.MarketInfo;
import cttk.dto.MarketInfos;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Tickers;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.util.ExNotRaisedException;
import cttk.request.GetOrderBookRequest;
import cttk.request.GetRecentTradesRequest;
import cttk.request.GetTickerRequest;
import cttk.request.GetTradeHistoryRequest;

abstract public class AbstractPublicCXClientTest
    extends AbstractCXClientTest
{
    protected PublicCXClient client;

    @Before
    public void setup()
        throws CTTKException, IOException
    {
        org.junit.Assume.assumeFalse(hasTestProfile("skip-public"));
        client = CXClientFactory.createPublicCXClient(getCxId());
    }

    @Test
    public void testGetMarketInfos()
        throws CTTKException
    {
        threadSleep();

        final MarketInfos marketInfos = client.getMarketInfos();

        logger.debug("{}", marketInfos);

        assertNotNull(marketInfos);
        assertTrue(marketInfos.size() > 0);

        final MarketInfo marketInfo = marketInfos.getMarketInfo(getCurrencyPair());
        assertNotNull(marketInfo);
    }

    @Test
    public void testGetTickerWrongCurrency()
        throws CTTKException
    {
        threadSleep();
        final GetTickerRequest request = new GetTickerRequest().setCurrencyPair(getInvalidCurrencyPair());
        try {
            client.getTicker(request);
        } catch (UnsupportedCurrencyPairException e) {
            assertEquals(e.getBaseCurrency(), getInvalidCurrencyPair().getBaseCurrency());
            assertEquals(e.getQuoteCurrency(), getInvalidCurrencyPair().getQuoteCurrency());
            assertEquals(e.getCxId(), this.getCxId());
            return;
        }
        throw new ExNotRaisedException(raiseExceptionMsg);

    }

    @Test
    public void testGetTicker()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();

        final GetTickerRequest request = new GetTickerRequest().setCurrencyPair(getValidCurrencyPair());
        final Ticker ticker = client.getTicker(request);

        logger.debug("{}", ticker);

        assertNotNull(ticker);
        assertNotNull(ticker.getCxId());
        assertEquals(request.getBaseCurrency(), ticker.getBaseCurrency());
        assertNotNull(ticker.getDateTime());
        assertNotNull(ticker.getLastPrice());
        assertNotNull(ticker.getLowPrice());
        assertNotNull(ticker.getHighPrice());
        assertNotNull(ticker.getVolume());
    }

    @Test
    public void testGetAllTickers()
        throws CTTKException
    {
        threadSleep();

        final Tickers tickers = client.getAllTickers();

        logger.debug("{}", tickers);

        assertNotNull(tickers);
        assertNotNull(tickers.getCxId());
        assertNotNull(tickers.getTickers());
        assertTrue(tickers.size() > 0);
    }

    @Test
    public void testGetOrderBook()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();

        final GetOrderBookRequest request = new GetOrderBookRequest().setCurrencyPair(getValidCurrencyPair());

        final OrderBook orderBook = client.getOrderBook(request);

        logger.debug("{}", orderBook);

        assertNotNull(orderBook);
        assertEquals(request.getBaseCurrency(), orderBook.getBaseCurrency());
        assertEquals(request.getQuoteCurrency(), orderBook.getQuoteCurrency());
        assertNotNull(orderBook.getCxId());
        assertNotNull(orderBook.getDateTime());
        assertNotNull(orderBook.getAsks());
        assertFalse(orderBook.getAsks().isEmpty());
        assertNotNull(orderBook.getBids());
        assertFalse(orderBook.getBids().isEmpty());
    }

    @Test
    public void testGetOrderBookWrongCurrency()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();

        final GetOrderBookRequest request = new GetOrderBookRequest()
            .setCurrencyPair(getInvalidCurrencyPair())
            .setCount(50);
        try {
            client.getOrderBook(request);
        } catch (UnsupportedCurrencyPairException e) {
            logger.error("base cur = " + getInvalidCurrencyPair().getBaseCurrency());
            logger.error("quote cur = " + getInvalidCurrencyPair().getQuoteCurrency());
            assertEquals(getInvalidCurrencyPair().getBaseCurrency(), e.getBaseCurrency());
            assertEquals(getInvalidCurrencyPair().getQuoteCurrency(), e.getQuoteCurrency());
            assertEquals(this.getCxId(), e.getCxId());
            return;
        }

        throw new ExNotRaisedException(raiseExceptionMsg);

    }

    @Test
    public void testGetRecentTrades()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();

        final GetRecentTradesRequest request = new GetRecentTradesRequest()
            .setCurrencyPair(getValidCurrencyPair())
            .setCount(100);

        final Trades trades = client.getRecentTrades(request);

        logger.debug("{}", trades);

        assertNotNull(trades);
        assertEquals(request.getBaseCurrency(), trades.getBaseCurrency());
        assertEquals(request.getQuoteCurrency(), trades.getQuoteCurrency());
        assertTrue(trades.size() > 0);
        assertNotNull(trades.getTrades().get(0).getSno());
        assertNotNull(trades.getTrades().get(0).getDateTime());
        assertNotNull(trades.getTrades().get(0).getPrice());
        assertNotNull(trades.getTrades().get(0).getQuantity());
        assertNotNull(trades.getTrades().get(0).getTotal());
    }

    @Test(expected = UnsupportedCurrencyPairException.class)
    public void testGetRecentTradesWithUnsupportedQuoteCurrency()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();

        final GetRecentTradesRequest request = new GetRecentTradesRequest()
            .setCurrencyPair(getInvalidCurrencyPair())
            .setCount(100);

        final Trades trades = client.getRecentTrades(request);

        assertNotNull(trades);
    }

    @Test
    public void testGetTradeHistory()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();

        final ZonedDateTime startDateTime = ZonedDateTime.now().minusDays(2);
        final ZonedDateTime endDateTime = startDateTime.plusHours(1);

        final GetTradeHistoryRequest request = new GetTradeHistoryRequest()
            .setCurrencyPair(getValidCurrencyPair())
            .setStartDateTime(startDateTime)
            .setEndDateTime(endDateTime);

        final Trades trades = client.getTradeHistory(request);

        logger.debug("{}", trades);

        assertNotNull(trades);
        assertEquals(request.getBaseCurrency(), trades.getBaseCurrency());
        assertEquals(request.getQuoteCurrency(), trades.getQuoteCurrency());
    }

}
