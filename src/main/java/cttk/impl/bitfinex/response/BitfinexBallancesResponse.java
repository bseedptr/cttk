package cttk.impl.bitfinex.response;

import static cttk.impl.bitfinex.BitfinexUtils.toStdCurrencySymbol;

import java.math.BigDecimal;

import cttk.dto.Balance;
import lombok.Data;

@Data
public class BitfinexBallancesResponse {
    String type;
    String currency;
    BigDecimal amount;
    BigDecimal available;

    public Balance toBalance() {
        final Balance b = new Balance()
            .setType(type)
            .setCurrency(toStdCurrencySymbol(currency))
            .setTotal(amount)
            .setAvailable(available);

        b.setInUse(b.getTotal().subtract(b.getAvailable()));

        return b;
    }
}
