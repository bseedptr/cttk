package cttk.impl.okex.stream;

import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.UserData;
import cttk.auth.Credential;
import cttk.exception.CTTKException;
import cttk.impl.okex.AbstractOkexClient;
import okhttp3.OkHttpClient;
import okhttp3.Request.Builder;
import okhttp3.WebSocket;

public class OkexUserDataWebsocketStream
    extends AbstractOkexClient
    implements CXStream<UserData>
{
    private static final String WSS_API_OKEX_WS = "wss://real.okex.com:10441/websocket";
    private final Credential credential;
    private final CXStreamListener<UserData> listener;

    protected WebSocket websocket;

    public OkexUserDataWebsocketStream(
        final Credential credential,
        final CXStreamListener<UserData> listener)
        throws CTTKException
    {
        this.credential = credential;
        this.listener = listener;

        open();
    }

    @Override
    public CXStreamListener<UserData> getListener() {
        return listener;
    }

    @Override
    public CXStream<UserData> open()
        throws CTTKException
    {
        openWebsocket();
        return this;
    }

    @Override
    public void close()
        throws Exception
    {
        closeWebsocket();
    }

    private void openWebsocket()
        throws CTTKException
    {
        final OkHttpClient okHttpClient = createOkHttpClient(10, 25);

        websocket = okHttpClient.newWebSocket(
            new Builder().url(WSS_API_OKEX_WS).build(),
            new OkexUserDataWebsocketListener(this, listener).setCredential(credential));

        okHttpClient.dispatcher().executorService().shutdown();
    }

    private void closeWebsocket()
        throws CTTKException
    {
        if (websocket != null) websocket.close(NORMAL_CLOSURE_STATUS, null);
    }
}
