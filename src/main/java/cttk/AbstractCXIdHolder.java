package cttk;

import lombok.ToString;

@ToString
public abstract class AbstractCXIdHolder<T extends AbstractCXIdHolder<T>>
    implements CXIdGettable
{
    protected String cxId;

    @Override
    public String getCxId() {
        return cxId;
    }

    @SuppressWarnings("unchecked")
    public T setCxId(String cxId) {
        this.cxId = cxId;
        return (T) this;
    }
}
