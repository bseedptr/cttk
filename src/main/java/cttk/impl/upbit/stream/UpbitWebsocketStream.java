package cttk.impl.upbit.stream;

import cttk.CXStreamListener;
import cttk.impl.stream.AbstractWebsocketStream;
import cttk.impl.stream.MessageConverter;
import cttk.request.AbstractCXStreamRequest;
import okhttp3.WebSocketListener;

public class UpbitWebsocketStream<T, R extends AbstractCXStreamRequest<R>>
    extends AbstractWebsocketStream<T, R>
{
    private static final String WSS_API_UPBIT_WS = "wss://api.upbit.com/websocket/v1";

    public UpbitWebsocketStream(
        final String channel,
        final R request,
        final MessageConverter<T> converter,
        final CXStreamListener<T> listener)
    {
        super(channel, request, converter, listener);
    }

    @Override
    protected String getUrl() {
        return WSS_API_UPBIT_WS;
    }

    @Override
    protected WebSocketListener createWebSocketListener() {
        return new UpbitWebsocketListener<>(this, channel, request, converter, listener);
    }
}
