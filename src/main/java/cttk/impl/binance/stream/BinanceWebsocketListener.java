package cttk.impl.binance.stream;

import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.impl.binance.BinanceUtils;
import cttk.impl.stream.AbstractWebsocketListener;
import cttk.impl.stream.MessageConverter;
import cttk.request.AbstractCXStreamRequest;
import okhttp3.WebSocket;
import okio.ByteString;

public class BinanceWebsocketListener<T, R extends AbstractCXStreamRequest<R>>
    extends AbstractWebsocketListener<T, R>
{
    public BinanceWebsocketListener(
        final CXStream<T> stream,
        final String channel,
        final R request,
        final MessageConverter<T> converter,
        final CXStreamListener<T> listener)
    {
        super(stream, channel, request, converter, listener, CXIds.BINANCE);
    }

    protected void handleMessage(WebSocket webSocket, String text)
        throws Exception
    {
        BinanceUtils.checkErrorByResponseMessage(request, text, null);
        if (text.startsWith("{\"ping\":")) {
            webSocket.send(text.replaceFirst("ping", "pong"));
        } else {
            T message = converter.convert(text);
            if (message != null) listener.onMessage(message);
        }
    }

    protected String toString(ByteString bytes)
        throws Exception
    {
        return bytes == null ? null : bytes.hex();
    }
}