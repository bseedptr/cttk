package cttk.impl.bithumb;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import cttk.CXIds;
import cttk.exception.APILimitExceedException;
import cttk.exception.CTTKException;
import cttk.exception.CXAPIRequestException;
import cttk.exception.CXAuthorityException;
import cttk.exception.CXServerException;
import cttk.exception.InsufficientBalanceException;
import cttk.exception.InvalidOrderException;
import cttk.exception.OrderNotFoundException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.AbstractCXClient;
import cttk.impl.util.HttpResponse;
import cttk.impl.util.ThrowableFunction;
import cttk.request.CancelOrderRequest;
import cttk.request.CreateOrderRequest;
import okhttp3.ConnectionPool;
import okhttp3.Request;
import okhttp3.Response;

abstract class AbstractBithumbClient
    extends AbstractCXClient
{
    protected static final String BASE_URL = "https://api.bithumb.com";

    protected static final int MAX_ORDERBOOK_COUNT = 50;
    protected static final int MAX_RECENT_TRANSACTION_COUNT = 100;

    public AbstractBithumbClient() {
        super();
    }

    public AbstractBithumbClient(ConnectionPool connectionPool) {
        super(connectionPool);
    }

    @Override
    public String getCxId() {
        return CXIds.BITHUMB;
    }

    protected HttpResponse requestGet(String endpoint)
        throws CTTKException
    {
        return requestGet(endpoint, null);
    }

    protected HttpResponse requestGet(String endpoint, Map<String, String> params)
        throws CTTKException
    {
        final Request request = new Request.Builder()
            .url(getHostUrl(endpoint, params))
            .build();

        debug(request);

        try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
            return toHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    protected String getHostUrl(String endpoint) {
        return getHostUrl(BASE_URL, endpoint);
    }

    protected String getHostUrl(String endpoint, Map<String, String> params) {
        return getHostUrl(BASE_URL, endpoint, params);
    }

    @Override
    protected <T, R> R convert(HttpResponse response, Class<T> claz, ThrowableFunction<T, R> mapper)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        try {
            return mapper.apply(objectMapper.readValue(response.getBody(), claz));
        } catch (IOException e) {
            throw CTTKException.create("json parse error\n" + response.getBody(), e, getCxId());
        }
    }

    // www.bithumb.com/u1/US127
    @Override
    protected void checkErrorByResponseMessage(Object requestObj, HttpResponse response)
        throws CXAPIRequestException
    {
        // statusCode is 200 even though error was raised
        String exchangeErrorCode;
        String exchangeMessage;
        try {
            final Map<?, ?> map = objectMapper.readValue(response.getBody(), HashMap.class);
            exchangeErrorCode = map.containsKey("status") ? String.valueOf(map.get("status")) : "";
            exchangeMessage = map.containsKey("message") ? String.valueOf(map.get("message")) : "";
        } catch (IOException e) {
            throw new CXAPIRequestException("Error parsing error\n" + response.getBody(), e)
                .setCxId(this.getCxId())
                .setExchangeMessage(response.getBody())
                .setResponseStatusCode(response.getStatus());
        }
        if (exchangeErrorCode.equals("0000")) return;

        final String message = String.format("code: %s, message: %s", exchangeErrorCode, exchangeMessage);

        switch (exchangeErrorCode) {
            case "5300":  //	Invalid Apikey
                throw new CXAuthorityException(message)
                    .setCxId(this.getCxId())
                    .setExchangeMessage(response.getBody())
                    .setResponseStatusCode(response.getStatus())
                    .setExchangeErrorCode(exchangeErrorCode);
            case "5500":  //	Invalid Parameter
            case "5600":  //	CUSTOM NOTICE (상황별 에러 메시지 출력)
                if (exchangeMessage.contains("Please try again") || exchangeMessage.contains("too many connections")) {
                    throw new APILimitExceedException(message)
                        .setCxId(this.getCxId())
                        .setExchangeMessage(response.getBody())
                        .setResponseStatusCode(response.getStatus())
                        .setExchangeErrorCode(exchangeErrorCode);
                } else if (exchangeMessage.contains("거래 진행중인 내역이 존재하지 않습니다")) {
                    throw new OrderNotFoundException(message)
                        .setCxId(this.getCxId())
                        .setExchangeMessage(response.getBody())
                        .setResponseStatusCode(response.getStatus())
                        .setExchangeErrorCode(exchangeErrorCode);
                } else if (exchangeMessage.contains("매수건의 상태가 진행중이 아닙니다. 취소할 수 없습니다")) {
                    throw new OrderNotFoundException(message)
                        .setCxId(this.getCxId())
                        .setExchangeMessage(response.getBody())
                        .setResponseStatusCode(response.getStatus())
                        .setExchangeErrorCode(exchangeErrorCode);
                } else if (exchangeMessage.contains("지원하지 않는 화폐")) {
                    throw CXAPIRequestException.createByRequestObj(requestObj)
                        .setCxId(this.getCxId())
                        .setExchangeMessage(response.getBody())
                        .setResponseStatusCode(response.getStatus())
                        .setExchangeErrorCode(exchangeErrorCode);
                } else if (requestObj instanceof CancelOrderRequest) {
                    // For cancel order, currency, oid, and order side could be wrong
                    // We do not know what is wrong 
                    throw new CXAPIRequestException(message)
                        .setCxId(this.getCxId())
                        .setExchangeMessage(response.getBody())
                        .setResponseStatusCode(response.getStatus())
                        .setExchangeErrorCode(exchangeErrorCode);
                } else {
                    throw CXAPIRequestException.createByRequestObj(requestObj, message)
                        .setCxId(this.getCxId())
                        .setExchangeMessage(response.getBody())
                        .setResponseStatusCode(response.getStatus())
                        .setExchangeErrorCode(exchangeErrorCode);
                }
            case "5900":  //	Unknown Error
            case "5100":  //	Bad Request
            case "5200":  //	Not Member
            case "5302":  //	Method Not Allowed
            case "5400":  //	Database Fail
                break;
        }
        throw new CXAPIRequestException(message)
            .setCxId(this.getCxId())
            .setExchangeMessage(response.getBody())
            .setResponseStatusCode(response.getStatus())
            .setExchangeErrorCode(exchangeErrorCode);
    }
}
