package cttk.impl.bitfinex.stream;

import cttk.CXStreamListener;
import cttk.impl.stream.AbstractWebsocketStream;
import cttk.impl.stream.MessageConverter;
import cttk.request.AbstractCXStreamRequest;
import okhttp3.WebSocketListener;

public class BitfinexWebsocketStream<T, R extends AbstractCXStreamRequest<R>>
    extends AbstractWebsocketStream<T, R>
{
    private static final String WSS_API_BITFINEX_WS = "wss://api.bitfinex.com/ws/";

    public BitfinexWebsocketStream(
        final String channel,
        final R request,
        final MessageConverter<T> converter,
        final CXStreamListener<T> listener)
    {
        super(channel, request, converter, listener);
    }

    @Override
    protected String getUrl() {
        return WSS_API_BITFINEX_WS;
    }

    @Override
    protected WebSocketListener createWebSocketListener() {
        return new BitfinexWebsocketListener<>(this, channel, request, converter, listener);
    }
}
