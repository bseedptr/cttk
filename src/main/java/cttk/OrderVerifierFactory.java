package cttk;

import cttk.dto.MarketInfo;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCXException;
import cttk.impl.MarketInfoOrderVerifier;
import cttk.impl.bithumb.BithumbOrderVerifier;
import cttk.impl.coinone.CoinoneOrderVerifier;
import cttk.impl.upbit.UpbitOrderVerifier;

public class OrderVerifierFactory {
    public static OrderVerifier create(final String cxId, MarketInfo marketInfo)
        throws CTTKException
    {
        if (cxId == null) throw new UnsupportedCXException().setCxId(cxId);
        switch (cxId.trim().toUpperCase()) {
            case CXIds.BIBOX:
            case CXIds.BINANCE:
            case CXIds.BITFINEX:
            case CXIds.HITBTC:
            case CXIds.HUOBI:
            case CXIds.KORBIT:
            case CXIds.KRAKEN:
            case CXIds.OKEX:
            case CXIds.POLONIEX:
                return new MarketInfoOrderVerifier(marketInfo);
            case CXIds.COINONE:
                return new CoinoneOrderVerifier(marketInfo);
            case CXIds.UPBIT:
                return new UpbitOrderVerifier(marketInfo);
            case CXIds.BITHUMB:
                return new BithumbOrderVerifier(marketInfo);
            default:
                throw new UnsupportedCXException().setCxId(cxId);
        }
    }
}
