package cttk.impl.upbit.response;

import java.math.BigDecimal;

import cttk.dto.Balance;
import cttk.impl.util.MathUtils;
import lombok.Data;

@Data
public class UpbitBalanceResponse {
    String currency;
    BigDecimal balance;
    BigDecimal locked;
    BigDecimal avg_krw_buy_price;
    boolean modified;

    public Balance toBalance() {
        return new Balance()
            .setCurrency(currency)
            .setAvailable(balance)
            .setInUse(locked)
            .setTotal(MathUtils.add(balance, locked));
    }

}
