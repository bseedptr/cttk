package cttk.exception;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class InvalidAPIKeyVersionException
    extends CXAPIRequestException
{
    private static final long serialVersionUID = 1L;

    public InvalidAPIKeyVersionException() {
        super();
    }

    public InvalidAPIKeyVersionException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidAPIKeyVersionException(String message) {
        super(message);
    }

    public InvalidAPIKeyVersionException(Throwable cause) {
        super(cause);
    }

    @Override
    public InvalidAPIKeyVersionException setCxId(String cxId) {
        super.setCxId(cxId);
        return this;
    }

    @Override
    public InvalidAPIKeyVersionException setResponseStatusCode(Integer responseStatusCode) {
        super.setResponseStatusCode(responseStatusCode);
        return this;
    }

    @Override
    public InvalidAPIKeyVersionException setExchangeMessage(String exchangeMessage) {
        super.setExchangeMessage(exchangeMessage);
        return this;
    }
}
