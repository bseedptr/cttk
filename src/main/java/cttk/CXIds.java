package cttk;

public class CXIds {
    public static final String BITHUMB = "BITHUMB";
    public static final String COINONE = "COINONE";
    public static final String KORBIT = "KORBIT";

    public static final String HUOBI = "HUOBI";
    public static final String OKEX = "OKEX";
    public static final String BITFINEX = "BITFINEX";

    public static final String BINANCE = "BINANCE";

    public static final String POLONIEX = "POLONIEX";
    public static final String KRAKEN = "KRAKEN";

    public static final String HITBTC = "HITBTC";

    public static final String BITMEX = "BITMEX";
    public static final String UPBIT = "UPBIT";
    public static final String BITTREX = "BITTREX";

    public static final String GEMINI = "GEMINI";
    public static final String BITSTAMP = "BITSTAMP";
    public static final String EXX = "EXX";

    public static final String COINBASE = "COINBASE";
    public static final String BITZ = "BITZ";

    public static final String BITFLYER = "BITFLYER";
    public static final String BIBOX = "BIBOX";
}
