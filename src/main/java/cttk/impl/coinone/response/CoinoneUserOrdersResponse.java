package cttk.impl.coinone.response;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.PricingType;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.ZoneIds;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CoinoneUserOrdersResponse
    extends CoinoneResponse
{
    List<CoinoneUserOrder> limitOrders;

    public UserOrders toUserOrders(CurrencyPair currencyPair, PricingType pricingType) {
        return new UserOrders()
            .setOrders(toUserOrderList(pricingType, currencyPair, limitOrders));
    }

    private static List<UserOrder> toUserOrderList(PricingType pricingType, CurrencyPair currencyPair, List<CoinoneUserOrder> coinoneUserOrders) {
        if (coinoneUserOrders == null) return null;
        return coinoneUserOrders.stream().map(i -> i.toUserOrder(currencyPair, pricingType)).filter(i -> i != null).collect(Collectors.toList());
    }
}

@Data
class CoinoneUserOrder {
    Long index;
    String orderId;
    Long timestamp;
    String type;
    BigDecimal price;
    BigDecimal qty;
    BigDecimal feeRate;

    public UserOrder toUserOrder(CurrencyPair currencyPair, PricingType pricingType) {
        if (orderId == null) {
            return null;
        }

        return new UserOrder()
            .setCurrencyPair(currencyPair)
            .setIndex(index)
            .setOid(orderId.toUpperCase())
            .setDateTime(DateTimeUtils.zdtFromEpochSecond(timestamp, ZoneIds.SEOUL))
            .setSide(OrderSide.of(type))
            .setPricingType(pricingType)
            .setPrice(price)
            .setQuantity(qty)
            .setTotal(price.multiply(qty))
            .setFeeRate(feeRate)
            .setStatusType(OrderStatus.UNFILLED);   // all limit orders is unfilled in Coinone
    }
}
