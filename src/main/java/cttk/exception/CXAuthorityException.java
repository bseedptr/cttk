package cttk.exception;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class CXAuthorityException
    extends CXAPIRequestException
{
    private static final long serialVersionUID = 1L;

    public CXAuthorityException() {
        super();
    }

    public CXAuthorityException(String message, Throwable cause) {
        super(message, cause);
    }

    public CXAuthorityException(String message) {
        super(message);
    }

    public CXAuthorityException(Throwable cause) {
        super(cause);
    }

    @Override
    public CXAuthorityException setCxId(String cxId) {
        super.setCxId(cxId);
        return this;
    }

    @Override
    public CXAuthorityException setResponseStatusCode(Integer responseStatusCode) {
        super.setResponseStatusCode(responseStatusCode);
        return this;
    }

    @Override
    public CXAuthorityException setExchangeMessage(String exchangeMessage) {
        super.setExchangeMessage(exchangeMessage);
        return this;
    }
}
