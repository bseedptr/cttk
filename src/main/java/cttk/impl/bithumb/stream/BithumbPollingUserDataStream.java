package cttk.impl.bithumb.stream;

import cttk.CXStreamListener;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.UserData;
import cttk.exception.CTTKException;
import cttk.impl.stream.eachmarket.EachMarketPollingUserDataStream;
import cttk.impl.stream.eachmarket.EachMarketPollingUserDataThread;
import cttk.impl.stream.eachmarket.PollingLevel;
import cttk.request.OpenUserDataStreamRequest;

class BithumbPollingUserDataStream
    extends EachMarketPollingUserDataStream
{

    BithumbPollingUserDataStream(
        final PublicCXClient publicClient,
        final PrivateCXClient privateClient,
        final OpenUserDataStreamRequest request,
        final CXStreamListener<UserData> listener)
        throws CTTKException
    {
        super(publicClient, privateClient, request, listener);
    }

    @Override
    protected EachMarketPollingUserDataThread createPollingThread(PollingLevel pollingLevel) {
        return new BithumbPollingUserDataThread(this, pollingLevel, listener, limitedClient, request, levelPairMap, lastCloseUserOrderMap);
    }
}
