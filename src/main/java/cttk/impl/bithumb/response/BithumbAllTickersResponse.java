package cttk.impl.bithumb.response;

import static cttk.impl.bithumb.BithumbUtils.PRICE_INCREMENTS;
import static cttk.impl.bithumb.BithumbUtils.PRICE_RANGES;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import cttk.dto.MarketInfo;
import cttk.dto.MarketInfos;
import cttk.dto.Ticker;
import cttk.dto.Tickers;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BithumbAllTickersResponse
    extends BithumbDataResponse<BithumbAllTickers>
{
    public Tickers toTickers() {
        return new Tickers()
            .setTickers(this.data.toTickerList());
    }

    public MarketInfos toMarketInfos() {
        return new MarketInfos()
            .setMarketInfos(this.data.toMarketInfoList());
    }
}

@Data
class BithumbAllTickers {
    Long date;
    Map<String, BithumbTicker> map = new HashMap<>();

    @JsonAnyGetter
    public Map<String, BithumbTicker> any() {
        return this.map;
    }

    @JsonAnySetter
    public void set(String name, BithumbTicker value) {
        if (value == null) {
            this.map.remove(name);
        } else {
            this.map.put(name, value);
        }
    }

    List<Ticker> toTickerList() {
        return this.map.entrySet().stream().map(e -> toTicker(e.getKey(), e.getValue())).collect(Collectors.toList());
    }

    Ticker toTicker(String baseCurrency, BithumbTicker bt) {
        return new Ticker()
            .setCurrencyPair(baseCurrency, "KRW")
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(date))
            .setOpenPrice(bt.opening_price)
            .setLastPrice(bt.closing_price)
            .setLowPrice(bt.min_price)
            .setHighPrice(bt.max_price)
            .setAvgPrice(bt.average_price)
            .setVolume(bt.units_traded)
            .setVolume7day(bt.volume_7day)
            .setHighestBid(bt.buy_price)
            .setLowestAsk(bt.sell_price);
    }

    List<MarketInfo> toMarketInfoList() {
        return this.map.entrySet().stream()
            .map(e -> {
                return new MarketInfo()
                    .setCurrencyPair(e.getKey(), "KRW")
                    .setPriceIncrement(MathUtils.findIncrement(PRICE_RANGES, PRICE_INCREMENTS, e.getValue().closing_price));
            })
            .collect(Collectors.toList());
    }
}