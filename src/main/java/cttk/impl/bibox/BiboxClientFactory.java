package cttk.impl.bibox;

import cttk.CXClientFactory;
import cttk.CXStreamOpener;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.UserDataStreamOpener;
import cttk.auth.Credential;
import cttk.impl.bibox.stream.BiboxUserDataStreamOpener;
import cttk.impl.bibox.stream.BiboxStreamOpener;

public class BiboxClientFactory
    implements CXClientFactory
{
    @Override
    public PublicCXClient create() {
        return new BiboxPublicClient();
    }

    @Override
    public PrivateCXClient create(Credential credential) {
        return new BiboxPrivateClient(credential);
    }

    @Override
    public CXStreamOpener createCXStreamOpener() {
        return new BiboxStreamOpener();
    }

    @Override
    public UserDataStreamOpener createUserDataStreamOpener(Credential credential) {
        return new BiboxUserDataStreamOpener(credential);
    }
}
