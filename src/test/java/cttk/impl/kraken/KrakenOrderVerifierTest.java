package cttk.impl.kraken;

import cttk.CXIds;
import cttk.impl.AbstractOrderVerifierTest;

public class KrakenOrderVerifierTest
    extends AbstractOrderVerifierTest
{
    @Override
    public String getCxId() {
        return CXIds.KRAKEN;
    }
}
