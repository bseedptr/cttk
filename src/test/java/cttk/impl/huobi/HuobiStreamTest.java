package cttk.impl.huobi;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import org.junit.Test;

import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreams;
import cttk.dto.OrderBook;
import cttk.impl.AbstractCXStreamTest;
import cttk.request.OpenOrderBookStreamRequest;

public class HuobiStreamTest
    extends AbstractCXStreamTest
{
    @Override
    public String getCxId() {
        return CXIds.HUOBI;
    }

    @Test
    public void testInvalidCurrencyPair()
        throws Exception
    {
        threadSleep();
        
        final OpenOrderBookStreamRequest request = new OpenOrderBookStreamRequest()
            .setIntervalInMilli(getIntervalInMilli())
            .setCurrencyPair(getInvalidCurrencyPair());
        final CountingCXStreamListener<OrderBook> listener = spy(new CountingCXStreamListener<>(request, 1));
        final CXStream<OrderBook> stream = CXStreams.openOrderBookSnapshotStream(getCxId(), request, listener);
        listener.await();
        stream.close();
        
        verify(listener, atLeastOnce()).onOpen(any());
        verify(listener, atLeastOnce()).onFailure(any(CXStream.class), any());
    }

}
