package cttk.util;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import cttk.CXStreamListener;
import cttk.dto.OrderBook;
import cttk.dto.OrderBook.SimpleOrder;

/**
 * OrderBookStreamSnapshotKeeper keeps snapshot of asks and bids, and update asks and bids when new updated (partial) OrderBook is received.
 * Order maps will not be updated until a full snapshot is received.
 * 
 * @author	dj.lee
 * @since	2018-03-02
 */
public class OrderBookSnapshotPipe
    extends CXStreamPipe<OrderBook>
{
    private boolean isInited = false;
    private Long lastUpdateId;

    private final Map<BigDecimal, BigDecimal> askMap = new ConcurrentHashMap<>();
    private final Map<BigDecimal, BigDecimal> bidMap = new ConcurrentHashMap<>();

    public OrderBookSnapshotPipe(CXStreamListener<OrderBook> successor) {
        super(successor);
    }

    @Override
    public OrderBook handleMessage(OrderBook message)
        throws Exception
    {
        if (message == null) return null;

        if (message.isFull()) {
            resetMapByList(askMap, message.getAsks());
            resetMapByList(bidMap, message.getBids());
            isInited = true;
        } else if (isInited && !isPrevMessage(message)) {
            updateMapByList(askMap, message.getAsks());
            updateMapByList(bidMap, message.getBids());
        }

        this.lastUpdateId = message.getLastUpdateId();

        return !isInited ? null : new OrderBook()
            .setCxId(message.getCxId())
            .setCurrencyPair(message)
            .setFull(true)
            .setDateTime(message.getDateTime())
            .setFirstUpdateId(message.getFirstUpdateId())
            .setLastUpdateId(message.getLastUpdateId())
            .setAsks(SimpleOrder.sortByPriceAsc(toSimpleOrderList(askMap)))
            .setBids(SimpleOrder.sortByPriceDesc(toSimpleOrderList(bidMap)));
    }

    private boolean isPrevMessage(OrderBook message) {
        return this.lastUpdateId != null
            && message.getLastUpdateId() != null
            && message.getLastUpdateId() <= this.lastUpdateId;
    }

    private static void resetMapByList(Map<BigDecimal, BigDecimal> map, List<SimpleOrder> list) {
        if (map == null) return;
        map.clear();

        if (list == null) return;
        list.forEach(i -> {
            if (i.getQuantity().compareTo(BigDecimal.ZERO) != 0) {
                map.put(i.getPrice(), i.getQuantity());
            }
        });
    }

    private static void updateMapByList(Map<BigDecimal, BigDecimal> map, List<SimpleOrder> list) {
        if (map == null || list == null || list.isEmpty()) return;
        list.forEach(i -> {
            if (i.getQuantity().longValue() == 0l) {
                map.remove(i.getPrice());
            } else {
                map.put(i.getPrice(), i.getQuantity());
            }
        });
    }

    private static List<SimpleOrder> toSimpleOrderList(Map<BigDecimal, BigDecimal> map) {
        if (map == null) return null;
        return map.entrySet().stream()
            .map(e -> OrderBook.SimpleOrder.of(e.getKey(), e.getValue()))
            .collect(Collectors.toList());
    }
}
