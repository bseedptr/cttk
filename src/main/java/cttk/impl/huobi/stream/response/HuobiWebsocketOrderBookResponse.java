package cttk.impl.huobi.stream.response;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import cttk.dto.OrderBook;
import cttk.dto.OrderBook.SimpleOrder;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class HuobiWebsocketOrderBookResponse
    extends HuobiWebsocketResponse<HuobiOrderBookTick>
{
    public OrderBook toOrderBook() {
        return new OrderBook()
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(ts))
            .setAsks(toSimpleOrderList(tick.asks))
            .setBids(toSimpleOrderList(tick.bids));
    }

    static List<SimpleOrder> toSimpleOrderList(List<List<BigDecimal>> orders) {
        if (orders == null) return null;
        return orders.stream()
            .map(i -> new SimpleOrder(i.get(0), i.get(1)))
            .collect(Collectors.toList());
    }
}

@Data
class HuobiOrderBookTick {
    long ts;
    long version;
    List<List<BigDecimal>> asks;
    List<List<BigDecimal>> bids;
}