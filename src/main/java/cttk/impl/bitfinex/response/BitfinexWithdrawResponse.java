package cttk.impl.bitfinex.response;

import cttk.dto.WithdrawResult;
import lombok.Data;

@Data
public class BitfinexWithdrawResponse {
    String status;
    String message;
    Long withdrawal_id;

    public WithdrawResult getWithdrawResult() {
        return new WithdrawResult().setWithdrawId(Long.toString(withdrawal_id));
    }

}
