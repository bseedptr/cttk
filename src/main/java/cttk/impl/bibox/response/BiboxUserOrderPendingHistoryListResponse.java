package cttk.impl.bibox.response;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.PricingType;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.impl.bibox.BiboxUtils;
import cttk.impl.util.DateTimeUtils;
import cttk.impl.util.MathUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BiboxUserOrderPendingHistoryListResponse
    extends BiboxResultWrapperResponse<BiboxUserOrderPendingHistoryList>
{
    public UserOrders toUserOrders() {
        return unwrap().toUserOrders();
    }
}

@Data
class BiboxUserOrderPendingHistoryList {
    Integer count;
    Integer page;
    List<BiboxUserOrderPendingHistory> items;

    UserOrders toUserOrders() {
        return new UserOrders()
            .setOrders(items.stream().map(BiboxUserOrderPendingHistory::toUserOrder).collect(Collectors.toList()));
    }
}

@Data
class BiboxUserOrderPendingHistory {
    String id;
    Long createdAt;
    String accountType;
    @JsonProperty("coin_symbol")
    String coinSymbol;
    @JsonProperty("currency_symbol")
    String currencySymbol;
    OrderSide orderSide;
    String orderType;
    BigDecimal price;
    BigDecimal amount;
    BigDecimal money;
    @JsonProperty("deal_amount")
    BigDecimal dealAmount;
    @JsonProperty("deal_percent")
    String dealPercent;
    String status;
    OrderStatus orderStatus;

    public UserOrder toUserOrder() {
        return new UserOrder()
            .setCurrencyPair(coinSymbol, currencySymbol)
            .setOid(id)
            .setDateTime(DateTimeUtils.zdtFromEpochMilli(createdAt))
            .setSide(orderSide)
            .setPricingType(PricingType.of(orderType.toUpperCase()))
            .setStatus(status)
            .setStatusType(orderStatus)
            .setPrice(price)
            .setQuantity(amount)
            .setFilledQuantity(dealAmount)
            .setRemainingQuantity(MathUtils.subtract(amount, dealAmount))
            .setTotal(MathUtils.multiply(price, amount));
    }

    @JsonProperty("account_type")
    public void setAccountType(Integer code) {
        this.accountType = BiboxUtils.parseAccountType(code);
    }

    @JsonProperty("order_side")
    public void setOrderSide(String side) {
        this.orderSide = BiboxUtils.parseOrderSide(side);
    }

    @JsonProperty("order_type")
    public void setOrderType(Integer code) {
        this.orderType = BiboxUtils.parseOrderType(code);
    }

    @JsonProperty("status")
    public void setStatus(Integer code) {
        this.status = BiboxUtils.parseOrderStatus(code);
        this.orderStatus = BiboxUtils.parseOrderStatusType(status);
    }
}