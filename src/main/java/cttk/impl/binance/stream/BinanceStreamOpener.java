package cttk.impl.binance.stream;

import cttk.CXIds;
import cttk.CXStream;
import cttk.CXStreamListener;
import cttk.CXStreamOpener;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.dto.Trades;
import cttk.exception.CTTKException;
import cttk.request.OpenOrderBookStreamRequest;
import cttk.request.OpenTickerStreamRequest;
import cttk.request.OpenTradeStreamRequest;

public class BinanceStreamOpener
    implements CXStreamOpener
{
    @Override
    public String getCxId() {
        return CXIds.BINANCE;
    }

    @Override
    public CXStream<Ticker> openTickerStream(final OpenTickerStreamRequest request, CXStreamListener<Ticker> listener) {
        return new BinanceWebsocketStream<Ticker, OpenTickerStreamRequest>(
            BinanceChannels.getTickerChannel(request),
            request,
            BinanceWebsocketMessageConverterFactory.createTickerMessageConverter(),
            listener);
    }

    @Override
    public CXStream<OrderBook> openOrderBookStream(final OpenOrderBookStreamRequest request, CXStreamListener<OrderBook> listener) {
        return new BinanceWebsocketStream<OrderBook, OpenOrderBookStreamRequest>(
            BinanceChannels.getOrderBookChannel(request),
            request,
            BinanceWebsocketMessageConverterFactory.createOrderBookMessageConverter(),
            listener);
    }

    @Override
    public CXStream<Trades> openTradeStream(final OpenTradeStreamRequest request, CXStreamListener<Trades> listener)
        throws CTTKException
    {
        return new BinanceWebsocketStream<Trades, OpenTradeStreamRequest>(
            BinanceChannels.getTradeChannel(request),
            request,
            BinanceWebsocketMessageConverterFactory.createTradesMessageConverter(),
            listener);
    }
}
