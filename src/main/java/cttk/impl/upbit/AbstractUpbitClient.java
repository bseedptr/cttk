package cttk.impl.upbit;

import java.io.IOException;
import java.util.Map;

import cttk.CXIds;
import cttk.exception.CTTKException;
import cttk.exception.CXServerException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.AbstractCXClient;
import cttk.impl.util.HttpResponse;
import okhttp3.Request;
import okhttp3.Response;

abstract class AbstractUpbitClient
    extends AbstractCXClient
{
    protected static final String BASE_URL = "https://api.upbit.com";

    @Override
    public String getCxId() {
        return CXIds.UPBIT;
    }

    @Override
    protected void checkErrorByResponseMessage(Object requestObj, HttpResponse response)
        throws CTTKException
    {
        if (response.is2xxSuccessful()) return;
        UpbitUtils.checkErrorByResponseMessage(requestObj, response.getBody(), response.getStatus());
    }

    protected HttpResponse requestGet(String endpoint)
        throws CTTKException
    {
        return requestGet(endpoint, null);
    }

    protected HttpResponse requestGet(String endpoint, Map<String, String> params)
        throws CTTKException, UnsupportedCurrencyPairException
    {
        final Request request = new Request.Builder()
            .url(getHostUrl(endpoint, params))
            .build();

        debug(request);

        try (final Response httpResponse = super.httpClient.newCall(request).execute();) {
            return toHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new CXServerException(e)
                .setCxId(getCxId())
                .setRequest(request);
        }
    }

    protected String getHostUrl(String endpoint) {
        return getHostUrl(BASE_URL, endpoint);
    }

    protected String getHostUrl(String endpoint, Map<String, String> params) {
        return getHostUrl(BASE_URL, endpoint, params);
    }

    @Override
    protected HttpResponse toHttpResponse(Response httpResponse)
        throws IOException, CTTKException, UnsupportedCurrencyPairException
    {
        final String body = httpResponse.body().string();
        final HttpResponse response = new HttpResponse()
            .setStatus(httpResponse.code())
            .setBody(body);

        debug(response);

        return response;

    }
}
