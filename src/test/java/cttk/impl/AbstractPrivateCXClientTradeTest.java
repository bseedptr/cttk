package cttk.impl;

import static cttk.test.TestUtils.hasTestProfile;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import cttk.CXClientFactory;
import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.OrderVerifier;
import cttk.OrderVerifierFactory;
import cttk.PrivateCXClient;
import cttk.PublicCXClient;
import cttk.auth.CXCredentialsProvider;
import cttk.dto.MarketInfo;
import cttk.dto.OrderBook;
import cttk.dto.Ticker;
import cttk.exception.CTTKException;
import cttk.exception.CXAuthorityException;
import cttk.exception.InsufficientBalanceException;
import cttk.exception.InvalidAmountMaxException;
import cttk.exception.InvalidAmountRangeException;
import cttk.exception.InvalidOrderException;
import cttk.exception.InvalidPriceIncrementPrecisionException;
import cttk.exception.InvalidPriceMaxException;
import cttk.exception.InvalidPriceRangeException;
import cttk.exception.InvalidQuantityIncrementPrecisionException;
import cttk.exception.InvalidTotalMaxException;
import cttk.exception.InvalidTotalRangeException;
import cttk.exception.OrderNotFoundException;
import cttk.exception.UnsupportedCXException;
import cttk.exception.UnsupportedCurrencyPairException;
import cttk.impl.util.ExNotRaisedException;
import cttk.impl.util.MathUtils;
import cttk.request.CancelOrderRequest;
import cttk.request.GetOrderBookRequest;
import cttk.test.OrderbookPrices;
import cttk.test.TradeTestUtils;

abstract public class AbstractPrivateCXClientTradeTest
    extends AbstractCXClientTest
{
    protected PublicCXClient pubClient;
    protected PrivateCXClient client;
    protected MarketInfo marketInfo;
    protected OrderVerifier orderVerifier;
    protected final String LOW_VALUE = "0.00000000001";

    @Before
    public void setup()
        throws CTTKException, IOException
    {
        org.junit.Assume.assumeTrue(hasTestProfile("test-trade"));
        pubClient = CXClientFactory.createPublicCXClient(getCxId());
        client = CXClientFactory.createPrivateCXClient(getCxId(), CXCredentialsProvider.getDefaultCredential(getCxId()).getTradeKeyOrDataKey());
        marketInfo = pubClient.getMarketInfos().getMarketInfo(getValidCurrencyPair());
        orderVerifier = OrderVerifierFactory.create(getCxId(), marketInfo);

    }

    @Override
    protected long getThreadSleepMillis() {
        return 500l;
    }

    @Test
    public void testWrongTradeAPIKey()
        throws UnsupportedCXException, CTTKException, IOException
    {
        threadSleep();

        final BigDecimal price = new BigDecimal(1000);
        final BigDecimal quantity = new BigDecimal(100);

        try {
            PrivateCXClient wrongKeyClient = CXClientFactory.createPrivateCXClient(getCxId(),
                this.getInvalidCredential(getCxId()).getTradeKey());
            TradeTestUtils.createOrder(wrongKeyClient, getValidCurrencyPair(), OrderSide.BUY, quantity, price);
        } catch (CXAuthorityException e) {
            assertEquals(this.getCxId(), e.getCxId());
            assertNotNull(e.getResponseStatusCode());
            assertNotNull(e.getExchangeMessage());
        }
    }

    @Test
    public void testCreateBuyWrongPrice()
        throws UnsupportedCurrencyPairException, CTTKException
    {
        threadSleep();

        final BigDecimal price = new BigDecimal(-1);
        final BigDecimal quantity = new BigDecimal(2);

        logger.info("Test bid with\n"
            + "    Test Bid       : {}\n"
            + "    Quantity       : {}\n"
            + "    Total          : {}\n",
            price, quantity, price.multiply(quantity));

        try {
            TradeTestUtils.createOrder(client, getValidCurrencyPair(), OrderSide.BUY, quantity, price);
        } catch (InvalidOrderException e) {
            assertEquals(e.getBaseCurrency(), getValidCurrencyPair().getBaseCurrency());
            assertEquals(e.getQuoteCurrency(), getValidCurrencyPair().getQuoteCurrency());
            assertEquals(e.getOrderQuantity(), quantity);
            assertEquals(e.getOrderPrice(), price);
            assertEquals(e.getCxId(), this.getCxId());
            return;
        }
        throw new ExNotRaisedException(raiseExceptionMsg);

    }

    @Test
    public void testCreateBuyLargeQuantity()
        throws UnsupportedCurrencyPairException, CTTKException
    {
        threadSleep();

        final OrderBook orderBook = pubClient.getOrderBook(new GetOrderBookRequest()
            .setCurrencyPair(getValidCurrencyPair()));

        final Optional<BigDecimal> opMaxBid = orderBook.getHighestBid();

        final BigDecimal price = orderVerifier.roundPrice(MathUtils.divide(opMaxBid.get(), new BigDecimal("2")));
        final BigDecimal quantity = new BigDecimal(2400);

        logger.info("{}", marketInfo);
        logger.info("Test bid with\n"
            + "    Current Max Bid: {}\n"
            + "    Test Bid       : {}\n"
            + "    Quantity       : {}\n"
            + "    Total          : {}\n"
            + "    Min Total      : {}",
            opMaxBid.get(), price, quantity, price.multiply(quantity), marketInfo.getMinTotal());

        try {
            TradeTestUtils.createOrder(client, getValidCurrencyPair(), OrderSide.BUY, quantity, price);
        } catch (InsufficientBalanceException e) {
            assertEquals(e.getBaseCurrency(), getValidCurrencyPair().getBaseCurrency());
            assertEquals(e.getQuoteCurrency(), getValidCurrencyPair().getQuoteCurrency());
            assertEquals(e.getOrderQuantity(), quantity);
            assertEquals(e.getOrderPrice(), price);
            assertEquals(e.getCxId(), this.getCxId());
            return;
        }
        throw new ExNotRaisedException(raiseExceptionMsg);

    }

    @Test
    public void testCreateBuyWrongQuantity()
        throws UnsupportedCurrencyPairException, CTTKException
    {
        threadSleep();

        final BigDecimal price = new BigDecimal(10);
        final BigDecimal quantity = new BigDecimal(-2);

        logger.info("Test bid with\n"
            + "    Test Bid       : {}\n"
            + "    Quantity       : {}\n"
            + "    Total          : {}\n",
            price, quantity, price.multiply(quantity));

        try {
            TradeTestUtils.createOrder(client, getValidCurrencyPair(), OrderSide.BUY, quantity, price);
        } catch (InvalidOrderException e) {
            assertEquals(e.getBaseCurrency(), getValidCurrencyPair().getBaseCurrency());
            assertEquals(e.getQuoteCurrency(), getValidCurrencyPair().getQuoteCurrency());
            assertEquals(e.getOrderQuantity(), quantity);
            assertEquals(e.getOrderPrice(), price);
            assertEquals(e.getCxId(), this.getCxId());
            return;
        }
        throw new ExNotRaisedException(raiseExceptionMsg);

    }

    @Test
    public void testCreateBuyWrongCurrency()
        throws UnsupportedCurrencyPairException, CTTKException
    {
        threadSleep();

        final OrderBook orderBook = pubClient.getOrderBook(new GetOrderBookRequest()
            .setCurrencyPair(getValidCurrencyPair()));

        final Optional<BigDecimal> opMaxBid = orderBook.getHighestBid();

        final BigDecimal price = orderVerifier.roundPrice(opMaxBid.get().divide(new BigDecimal("2")));
        final BigDecimal quantity = getBuyTestQuantity();

        logger.info("{}", marketInfo);
        logger.info("Test bid with\n"
            + "    Current Max Bid: {}\n"
            + "    Test Bid       : {}\n"
            + "    Quantity       : {}\n"
            + "    Total          : {}\n"
            + "    Min Total      : {}",
            opMaxBid.get(), price, quantity, price.multiply(quantity), marketInfo.getMinTotal());
        try {
            TradeTestUtils.createOrder(client, getInvalidCurrencyPair(), OrderSide.BUY, quantity, price);
        } catch (UnsupportedCurrencyPairException e) {
            assertEquals(e.getBaseCurrency(), getInvalidCurrencyPair().getBaseCurrency());
            assertEquals(e.getQuoteCurrency(), getInvalidCurrencyPair().getQuoteCurrency());
            assertEquals(e.getCxId(), this.getCxId());
            return;
        }
        throw new ExNotRaisedException(raiseExceptionMsg);
    }

    @Test
    public void testCreateBuy()
        throws UnsupportedCurrencyPairException, CTTKException
    {
        threadSleep();

        final CurrencyPair currencyPair = getValidCurrencyPair();
        final OrderBook orderBook = pubClient.getOrderBook(new GetOrderBookRequest()
            .setCurrencyPair(currencyPair));

        final Optional<BigDecimal> opMaxBid = orderBook.getHighestBid();

        final BigDecimal price = orderVerifier.roundPrice(MathUtils.divide(opMaxBid.get(), new BigDecimal("2"))).stripTrailingZeros();
        final BigDecimal quantity = getBuyTestQuantity();

        logger.info("{}", marketInfo);
        logger.info("Test bid with\n"
            + "    Current Max Bid: {}\n"
            + "    Test Bid       : {}\n"
            + "    Quantity       : {}\n"
            + "    Total          : {}\n"
            + "    Min Total      : {}",
            opMaxBid.get(), price, quantity, price.multiply(quantity), marketInfo.getMinTotal());

        TradeTestUtils.testCreateCancelOrder(client, currencyPair, OrderSide.BUY, quantity, price);

    }

    @Test
    public void testCreateSell()
        throws CTTKException, UnsupportedCurrencyPairException
    {
        threadSleep();

        final CurrencyPair currencyPair = getValidCurrencyPair();
        final OrderBook orderBook = pubClient.getOrderBook(new GetOrderBookRequest()
            .setCurrencyPair(currencyPair));

        final Optional<BigDecimal> opMinAsk = orderBook.getLowestAsk();

        final BigDecimal price = orderVerifier.roundPrice(opMinAsk.get().multiply(new BigDecimal("1.5"))).stripTrailingZeros();
        final BigDecimal quantity = getSellTestQuantity();

        logger.info("{}", marketInfo);
        logger.info("Test ask with\n"
            + "    Current Min Ask: {}\n"
            + "    Test Ask       : {}\n"
            + "    Quantity       : {}\n"
            + "    Total          : {}\n"
            + "    Min Total      : {}",
            opMinAsk.get(), price, quantity, price.multiply(quantity), marketInfo.getMinTotal());

        TradeTestUtils.testCreateCancelOrder(client, currencyPair, OrderSide.SELL, quantity, price);
    }

    @Test
    public void testCancelOrderWrongID()
        throws UnsupportedCurrencyPairException, CTTKException
    {
        threadSleep();

        String nonExistID = "999999999";

        try {
            client.cancelOrder(new CancelOrderRequest()
                .setCurrencyPair(getValidCurrencyPair())
                .setOrderId(nonExistID)
                .setSide(OrderSide.BUY));
        } catch (OrderNotFoundException e) {
            assertEquals(this.getCxId(), e.getCxId());
            assertNotNull(e.getExchangeMessage());
            assertNotNull(e.getResponseStatusCode());
            return;
        }
        throw new ExNotRaisedException(raiseExceptionMsg);

    }

    protected void checkInvalidOrderException(InvalidOrderException e, BigDecimal price, BigDecimal quantity) {
        if (e instanceof InvalidTotalRangeException) {
            Assert.assertEquals(price.multiply(quantity), ((InvalidTotalRangeException) e).getTotal());
        }
        Assert.assertEquals(getValidCurrencyPair().getBaseCurrency(), e.getBaseCurrency());
        Assert.assertEquals(getValidCurrencyPair().getQuoteCurrency(), e.getQuoteCurrency());
        Assert.assertEquals(quantity, e.getOrderQuantity());
        Assert.assertEquals(price, e.getOrderPrice());
        Assert.assertEquals(getCxId(), e.getCxId());

    }

    @Test
    public void testInvalidMinQuantity()
        throws CTTKException
    {
        threadSleep();

        BigDecimal quantity = marketInfo.getMinQuantity();
        BigDecimal minQuantity;
        if (quantity == null) {
            minQuantity = new BigDecimal(LOW_VALUE);
        } else {
            minQuantity = quantity.movePointLeft(1);
        }

        OrderbookPrices prices = TradeTestUtils.getOrderbookPrices(pubClient, getValidCurrencyPair(), orderVerifier, new BigDecimal("2"));

        BigDecimal buyPrice = prices.getBuyPrice();
        try {
            TradeTestUtils.testCreateCancelOrder(client, getValidCurrencyPair(), OrderSide.BUY, minQuantity, buyPrice);
        } catch (InvalidAmountMaxException | InvalidTotalMaxException e) {
            throw new CTTKException("Wrong exception {}", e);
        } catch (InvalidAmountRangeException | InvalidTotalRangeException e) {
            checkInvalidOrderException(e, buyPrice, minQuantity);
            return;
        }
        throw new ExNotRaisedException("No exception thrown from testInvalidMinQuantity");
    }

    @Test
    public void testInvalidPriceIncrementPrecision()
        throws CTTKException
    {
        threadSleep();

        BigDecimal quantity = marketInfo.getMinQuantity();

        if (quantity == null) {
            quantity = new BigDecimal("0.1");
        }
        testInvalidPriceIncrementPrecisionQuantity(quantity);
    }

    protected void testInvalidPriceIncrementPrecisionQuantity(BigDecimal quantity)
        throws CTTKException
    {
        Ticker ticker = pubClient.getTicker(getValidCurrencyPair().getBaseCurrency(), getValidCurrencyPair().getQuoteCurrency());

        BigDecimal price = ticker.getLowPrice();
        BigDecimal priceIncrement = marketInfo.getPriceIncrement();
        Integer pricePrecision = marketInfo.getPricePrecision();
        BigDecimal invalidPriceAdd;
        if (priceIncrement != null) {
            invalidPriceAdd = priceIncrement.movePointLeft(1);
        } else if (pricePrecision != null) {
            if (price.scale() > price.precision()) {
                invalidPriceAdd = BigDecimal.ONE.movePointLeft((price.scale() - price.precision()) + pricePrecision + 1);
            } else {
                invalidPriceAdd = BigDecimal.ONE.movePointLeft(pricePrecision + 1);
            }
        } else {
            invalidPriceAdd = new BigDecimal(LOW_VALUE);
        }

        price = price.add(invalidPriceAdd);
        try {
            TradeTestUtils.testCreateCancelOrder(client, getValidCurrencyPair(), OrderSide.BUY, quantity, price);
        } catch (InvalidPriceIncrementPrecisionException e) {
            checkInvalidOrderException(e, price, quantity);
            return;
        }
        throw new ExNotRaisedException("No exception thrown from testInvalidPriceIncrementPrecision");
    }

    @Test
    public void testInvalidQuantityIncrementPrecision()
        throws CTTKException
    {
        threadSleep();

        BigDecimal quantity = marketInfo.getMinQuantity();

        if (quantity == null) {
            quantity = new BigDecimal("0.1");
        }
        testInvalidQuantityIncrementPrecisionQuantity(quantity);
    }

    protected void testInvalidQuantityIncrementPrecisionQuantity(BigDecimal quantity)
        throws CTTKException
    {
        OrderbookPrices prices = TradeTestUtils.getOrderbookPrices(pubClient, getValidCurrencyPair(), orderVerifier, new BigDecimal("2"));
        BigDecimal price = prices.getBuyPrice();
        BigDecimal minQuantity;
        BigDecimal quanittyIncrement = marketInfo.getQuantityIncrement();
        Integer quantityPrecision = marketInfo.getQuantityPrecision();

        if (quanittyIncrement != null) {
            minQuantity = quantity.add(quanittyIncrement.movePointLeft(1));
        } else if (quantityPrecision != null) {
            if (quantity.scale() > quantity.precision()) {
                minQuantity = quantity.add(BigDecimal.ONE.movePointLeft((quantity.scale() - quantity.precision()) + quantityPrecision + 1));
            } else {
                minQuantity = quantity.add(BigDecimal.ONE.movePointLeft(quantityPrecision + 1));
            }
        } else {
            minQuantity = quantity.add(new BigDecimal(LOW_VALUE));
        }

        try {
            TradeTestUtils.testCreateCancelOrder(client, getValidCurrencyPair(), OrderSide.BUY, minQuantity, price);
        } catch (InvalidQuantityIncrementPrecisionException e) {
            checkInvalidOrderException(e, price, minQuantity);
            return;
        }
        throw new ExNotRaisedException("No exception thrown from testInvalidQuantityIncrementPrecision");
    }

    @Test
    public void testInvalidMinPrice()
        throws CTTKException
    {
        threadSleep();

        BigDecimal price = new BigDecimal(LOW_VALUE);
        BigDecimal quantity = marketInfo.getMinQuantity();
        if (quantity == null) {
            quantity = new BigDecimal("0.1");
        }
        try {
            TradeTestUtils.testCreateCancelOrder(client, getValidCurrencyPair(), OrderSide.BUY, quantity, price);
        } catch (InvalidPriceMaxException | InvalidTotalMaxException e) {
            throw new CTTKException("Wrong exception {} ", e);
        } catch (InvalidPriceRangeException | InvalidTotalRangeException | InvalidPriceIncrementPrecisionException e) {
            checkInvalidOrderException(e, price, quantity);
            return;
        }
        throw new ExNotRaisedException("No exception thrown from testInvalidMinPrice");
    }
}
