package cttk.impl.bibox;

import java.math.BigDecimal;

import cttk.CurrencyPair;
import cttk.TradingFeeProvider;
import cttk.dto.TradingFee;
import cttk.exception.CTTKException;

public class BiboxTradingFeeProvider
    implements TradingFeeProvider
{
    private static final TradingFee FIXED_FEE;

    static {
        BigDecimal fee = new BigDecimal("0.001");
        FIXED_FEE = new TradingFee(fee, fee);
    }

    @Override
    public TradingFee getFee(CurrencyPair currencyPair)
        throws CTTKException
    {
        return FIXED_FEE;
    }
}
