package cttk.exception;

import cttk.CurrencyPair;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class UnsupportedCurrencyPairException
    extends CXAPIRequestException
    implements CurrencyPair
{
    private static final long serialVersionUID = 1L;

    protected String baseCurrency;
    protected String quoteCurrency;

    public UnsupportedCurrencyPairException() {
        super();
    }

    public UnsupportedCurrencyPairException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsupportedCurrencyPairException(String message) {
        super(message);
    }

    public UnsupportedCurrencyPairException(Throwable cause) {
        super(cause);
    }

    @Override
    public String toString() {
        return super.toString()
            + ", Base Currency = " + baseCurrency
            + ", Quote Currency = " + quoteCurrency;
    }

    @Override
    public UnsupportedCurrencyPairException setCxId(String cxId) {
        super.setCxId(cxId);
        return this;
    }

    @Override
    public UnsupportedCurrencyPairException setResponseStatusCode(Integer responseStatusCode) {
        super.setResponseStatusCode(responseStatusCode);
        return this;
    }

    @Override
    public UnsupportedCurrencyPairException setExchangeMessage(String exchangeMessage) {
        super.setExchangeMessage(exchangeMessage);
        return this;
    }

    public UnsupportedCurrencyPairException setCurrencyPair(CurrencyPair currencyPair) {
        this.baseCurrency = currencyPair == null ? null : currencyPair.getBaseCurrency();
        this.quoteCurrency = currencyPair == null ? null : currencyPair.getQuoteCurrency();
        return this;
    }
}
