package cttk.example;

import java.io.IOException;
import java.time.ZonedDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cttk.CXIds;
import cttk.CurrencyPair;
import cttk.auth.CXCredentialsProvider;
import cttk.dto.UserTrades;
import cttk.exception.CTTKException;
import cttk.exception.UnsupportedCXException;
import cttk.util.UserDataGetter;

public class GetUserTradesDeltaExample {

    static final Logger logger = LoggerFactory.getLogger(GetUserTradesDeltaExample.class);

    public static void main(String[] args)
        throws UnsupportedCXException, CTTKException, IOException
    {
        final String cxId = CXIds.OKEX;
        final UserDataGetter userDataGetter = new UserDataGetter(cxId, CXCredentialsProvider.getDefaultCredential(cxId).getTradeKey());

        UserTrades userTrades = userDataGetter.getUserTrades(CurrencyPair.of("EOS", "ETH"), null, ZonedDateTime.now().minusDays(30), ZonedDateTime.now(), null);
        Examples.print(cxId, userTrades);

        try {
            Thread.sleep(500l);
        } catch (InterruptedException e) {
            logger.error("Unexpected error", e);
        }
    }

}
