package cttk.impl.stream.eachmarket;

import static cttk.impl.stream.eachmarket.PollingLevel.LEVEL1;
import static cttk.impl.stream.eachmarket.PollingLevel.LEVEL2;
import static cttk.impl.stream.eachmarket.PollingLevel.LEVEL3;
import static cttk.impl.stream.eachmarket.PollingLevel.values;
import static java.util.Arrays.stream;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import cttk.CurrencyPair;
import lombok.ToString;

@ToString
public class LevelPairMap {
    private final Map<PollingLevel, Set<CurrencyPair>> map = new HashMap<>();

    public Set<CurrencyPair> getPairs(PollingLevel pollingLevel) {
        return map.get(pollingLevel);
    }

    public LevelPairMap setPairs(PollingLevel pollingLevel, List<CurrencyPair> pairs) {
        Set<CurrencyPair> syncPairs = ConcurrentHashMap.newKeySet();
        syncPairs.addAll(pairs);
        map.put(pollingLevel, syncPairs);
        return this;
    }

    public void changeLevel(CurrencyPair pair, PollingLevel pollingLevel) {
        if (map.get(pollingLevel).contains(pair))
            return;
        stream(values()).forEach(t -> map.get(t).remove(pair));
        map.get(pollingLevel).add(pair);
    }

    public int count(PollingLevel pollingLevel) {
        return map.get(pollingLevel).size();
    }

    public int countAll() {
        return count(LEVEL1) + count(LEVEL2) + count(LEVEL3);
    }

    public boolean exists(PollingLevel pollingLevel, CurrencyPair pair) {
        return map.get(pollingLevel).contains(pair);
    }
}
