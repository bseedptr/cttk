package cttk.impl.upbit.response;

import static cttk.OrderStatus.CANCELED;
import static cttk.OrderStatus.FILLED;
import static cttk.OrderStatus.UNFILLED;
import static cttk.impl.util.Objects.nvl;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

import cttk.CurrencyPair;
import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.PricingType;
import cttk.dto.UserOrder;
import cttk.dto.UserTrade;
import cttk.impl.upbit.UpbitUtils;
import cttk.impl.util.DateTimeUtils;
import lombok.Data;

@Data
public class UpbitUserOrderResponse {
    String uuid;
    String side;
    String ord_type;
    BigDecimal price;
    BigDecimal avg_price;

    String state;
    String market;
    String created_at;
    BigDecimal volume;
    BigDecimal remaining_volume;

    BigDecimal reserved_fee;
    BigDecimal remaining_fee;
    BigDecimal paid_fee;
    BigDecimal locked;
    BigDecimal executed_volume;

    Integer trade_count;
    List<UpbitTrade> trades;

    public UserOrder toUserOrder() {
        if (uuid == null)
            return null;

        return new UserOrder()
            .setOid(uuid)
            .setSide(OrderSide.of(side))
            .setPrice(price)
            .setStatus(state)
            .setStatusType(UpbitUtils.getStatusType(state))
            .setPricingType(PricingType.of(ord_type))
            .setCurrencyPair(UpbitUtils.parseMarketSymbol(market))
            .setDateTime(ZonedDateTime.parse(created_at))
            .setQuantity(volume)
            .setRemainingQuantity(remaining_volume)
            .setFee(paid_fee)
            .setFilledQuantity(executed_volume)
            .setTotal(price.multiply(volume))
            .setTrades(toTrades());
    }

    public UserTrade toUserTrade() {
        final CurrencyPair currencyPair = UpbitUtils.parseMarketSymbol(market);
        return new UserTrade()
            .setTid(uuid)
            .setOid(uuid)
            .setSide(OrderSide.of(side))
            .setPrice(price)
            .setStatus(state)
            .setCurrencyPair(currencyPair)
            .setDateTime(DateTimeUtils.parseZdt(created_at))
            .setQuantity(volume)
            .setFee(paid_fee)
            .setFeeCurrency(currencyPair.getQuoteCurrency())
            .setTotal(price.multiply(volume));
    }

    private List<UserTrade> toTrades() {
        if (trades == null) return null;
        return trades.stream().map(i -> i.toUserTrade()).collect(Collectors.toList());
    }
}

@Data
class UpbitTrade {
    String market;
    String uuid;
    BigDecimal price;
    BigDecimal volume;
    String funds;

    BigDecimal ask_fee;
    BigDecimal bid_fee;
    String created_at;
    String side;

    public UserTrade toUserTrade() {
        return new UserTrade()
            .setOid(uuid)
            .setCurrencyPair(UpbitUtils.parseMarketSymbol(market))
            .setPrice(price)
            .setQuantity(volume)
            .setFee(nvl(ask_fee, bid_fee))
            .setDateTime(DateTimeUtils.parseZdt(created_at))
            .setSide(OrderSide.of(side));
    }

}
