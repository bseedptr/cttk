package cttk.impl.util;

import java.util.HashMap;
import java.util.Map;

public class OParamsBuilder {
    final Map<String, Object> params = new HashMap<>();

    protected OParamsBuilder() {
        super();
    }

    public OParamsBuilder put(String key, Object value) {
        params.put(key, value);
        return this;
    }

    public OParamsBuilder putIfNotNull(String key, Object value) {
        if (value != null) params.put(key, value);
        return this;
    }

    public OParamsBuilder putDefaultIfNull(String key, Object value, Object defaultValue) {
        params.put(key, value == null ? defaultValue : value);
        return this;
    }

    public Map<String, Object> build() {
        return params;
    }

    public static OParamsBuilder create() {
        return new OParamsBuilder();
    }

    public static Map<String, Object> build(String key, Object value) {
        return OParamsBuilder.create().put(key, value).build();
    }

    public static Map<String, Object> buildIfNotNull(String key, Object value) {
        return OParamsBuilder.create().putIfNotNull(key, value).build();
    }
}
