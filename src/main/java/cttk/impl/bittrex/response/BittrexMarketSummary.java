package cttk.impl.bittrex.response;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import com.fasterxml.jackson.annotation.JsonProperty;

import cttk.dto.Ticker;
import cttk.impl.bittrex.BittrexUtils;
import lombok.Data;

@Data
public class BittrexMarketSummary {
    @JsonProperty("MarketName")
    String marketName;
    @JsonProperty("High")
    BigDecimal high;
    @JsonProperty("Low")
    BigDecimal low;
    @JsonProperty("Volume")
    BigDecimal volume;
    @JsonProperty("Last")
    BigDecimal last;
    @JsonProperty("BaseVolume")
    BigDecimal baseVolume;
    @JsonProperty("TimeStamp")
    String timeStamp;
    @JsonProperty("Bid")
    BigDecimal bid;
    @JsonProperty("Ask")
    BigDecimal ask;
    @JsonProperty("OpenBuyOrders")
    String openBuyOrders;
    @JsonProperty("OpenSellOrders")
    String openSellOrders;
    @JsonProperty("PrevDay")
    String prevDay;
    @JsonProperty("Created")
    String created;
    @JsonProperty("DisplayMarketName")
    String displayMarketName;

    public Ticker toTicker() {
        return new Ticker()
            .setCurrencyPair(BittrexUtils.parseMarketSymbol(marketName))
            .setLastPrice(last)
            .setHighPrice(high)
            .setLowPrice(low)
            .setHighestBid(bid)
            .setLowestAsk(ask)
            .setVolume(volume)
            .setQuoteVolume(baseVolume)
            .setDateTime(OffsetDateTime.parse(timeStamp + "-00:00").atZoneSameInstant(ZoneOffset.UTC));
    }
}
