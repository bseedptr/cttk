package cttk.request;

import java.time.ZonedDateTime;

import cttk.AbstractCurrencyPair;
import cttk.DurationGettable;
import cttk.OrderSide;
import cttk.OrderStatus;
import cttk.ScanDirection;
import cttk.SortType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class GetUserOrderHistoryRequest
    extends AbstractCurrencyPair<GetUserOrderHistoryRequest>
    implements DurationGettable
{
    private OrderSide side;
    private OrderStatus status;
    private String fromId;
    private String tillId;
    private ZonedDateTime startDateTime;
    private ZonedDateTime endDateTime;
    private Integer offset;
    private Integer count;
    private Integer pageNo;
    private boolean withTrades;
    private SortType sort;
    private ScanDirection scanDirection;

    public GetUserOrderHistoryRequest setSide(OrderSide side) {
        this.side = side;
        return this;
    }

    public GetUserOrderHistoryRequest setSide(String side) {
        this.side = OrderSide.of(side);
        return this;
    }

    public boolean isScanDirectionPrev() {
        return scanDirection == ScanDirection.PREV;
    }
}
