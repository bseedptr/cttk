package cttk.impl.hitbtc;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.ZonedDateTime;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import cttk.auth.Credential;
import cttk.dto.Transfer;
import cttk.dto.Transfers;
import cttk.dto.UserOrder;
import cttk.dto.UserOrders;
import cttk.dto.UserTrade;
import cttk.dto.UserTrades;
import cttk.impl.AbstractPrivateClientDeltaTest;
import cttk.request.GetTransfersDeltaRequest;
import cttk.request.GetTransfersRequest;
import cttk.request.GetUserOrderHistoryDeltaRequest;
import cttk.request.GetUserOrderHistoryRequest;
import cttk.request.GetUserTradesDeltaRequest;
import cttk.request.GetUserTradesRequest;
import cttk.util.DeltaUtils;
import lombok.extern.slf4j.Slf4j;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ UserOrder.class, UserTrade.class, Transfer.class, DeltaUtils.class })
@PowerMockIgnore("javax.net.ssl.*")
@Slf4j
public class HitbtcPrivateClientDeltaTest
    extends AbstractPrivateClientDeltaTest
{
    HitbtcPrivateClient client;
    GetUserOrderHistoryDeltaRequest orderHistoryRequest;
    GetUserTradesDeltaRequest tradesRequest;
    GetTransfersDeltaRequest transferRequest;

    @Mock
    UserOrders orders1, orders2, orders3;
    @Mock
    UserTrades trades1, trades2, trades3;
    @Mock
    Transfers transfers1, transfers2, transfers3;

    @Before
    public void setup() {
        PowerMockito.spy(UserOrder.class);
        PowerMockito.spy(UserTrade.class);
        PowerMockito.spy(Transfer.class);
        PowerMockito.spy(DeltaUtils.class);
        client = spy(new HitbtcPrivateClient(mock(Credential.class)));
        orderHistoryRequest = new GetUserOrderHistoryDeltaRequest().setStartDateTime(ZonedDateTime.now().minusYears(2));
        tradesRequest = new GetUserTradesDeltaRequest().setStartDateTime(ZonedDateTime.now().minusYears(2));
        transferRequest = new GetTransfersDeltaRequest().setStartDateTime(ZonedDateTime.now().minusYears(2));
    }

    @Test
    public void testGetUserOrderHistoryDeltaWhenNoMoreData()
        throws Exception
    {
        PowerMockito.when(UserOrder.equalsById(any(), any())).thenReturn(false, false, true);
        PowerMockito.when(DeltaUtils.calcNextSeconds(any(UserOrder.class), any(UserOrder.class))).thenReturn(0);
        when(orders1.getList()).thenReturn(makeUserOrderFixtures("1", "2", "3"));
        when(orders1.getOldest()).thenReturn(makeUserOrderFixture("1"));
        when(orders1.getLatest()).thenReturn(makeUserOrderFixture("3"));
        when(orders2.getList()).thenReturn(makeUserOrderFixtures("4", "5"));
        when(orders2.getOldest()).thenReturn(makeUserOrderFixture("4"));
        when(orders2.getLatest()).thenReturn(makeUserOrderFixture("5"));
        when(orders3.getList()).thenReturn(makeUserOrderFixtures());
        doReturn(orders1, orders2, orders3).when(client).getUserOrderHistory(any(GetUserOrderHistoryRequest.class));
        PowerMockito.when(UserOrder.equalsById(any(), any())).thenReturn(false, false, true);

        UserOrders result = client.getUserOrderHistory(orderHistoryRequest);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat(ids, containsInAnyOrder("1", "2", "3", "4", "5"));
    }

    @Test
    public void testGetUserOrdersDeltaWhenDuplication()
        throws Exception
    {
        PowerMockito.when(UserOrder.equalsById(any(), any())).thenReturn(false, false, true);
        PowerMockito.when(DeltaUtils.calcNextSeconds(any(UserOrder.class), any(UserOrder.class))).thenReturn(0);
        when(orders1.getList()).thenReturn(makeUserOrderFixtures("1", "2", "3", "4"));
        when(orders1.getOldest()).thenReturn(makeUserOrderFixture("1"));
        when(orders1.getLatest()).thenReturn(makeUserOrderFixture("4"));
        when(orders2.getList()).thenReturn(makeUserOrderFixtures("3", "4", "5", "6", "7"));
        when(orders2.getOldest()).thenReturn(makeUserOrderFixture("3"));
        when(orders2.getLatest()).thenReturn(makeUserOrderFixture("7"));
        when(orders3.isEmpty()).thenReturn(true);
        doReturn(orders1, orders2, orders3).when(client).getUserOrderHistory(any(GetUserOrderHistoryRequest.class));

        UserOrders result = client.getUserOrderHistory(orderHistoryRequest);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat("Id should be unique.", ids, containsInAnyOrder("1", "2", "3", "4", "5", "6", "7"));
        verify(client, times(3)).getUserOrderHistory(any(GetUserOrderHistoryRequest.class));
    }

    @Test
    public void testGetUserTradesDeltaWhenNoMoreData()
        throws Exception
    {
        PowerMockito.when(UserTrade.equalsById(any(), any())).thenReturn(false, false, true);
        when(trades1.getList()).thenReturn(makeUserTradeFixtures("1", "2", "3"));
        when(trades1.isEmpty()).thenReturn(false);
        when(trades1.getLatest()).thenReturn(mock(UserTrade.class));
        when(trades2.getList()).thenReturn(makeUserTradeFixtures("4", "5"));
        when(trades2.isEmpty()).thenReturn(false);
        when(trades2.getLatest()).thenReturn(mock(UserTrade.class));
        when(trades3.getList()).thenReturn(makeUserTradeFixtures());
        when(trades3.isEmpty()).thenReturn(true);
        doReturn(trades1, trades2, trades3).when(client).getUserTrades(any(GetUserTradesRequest.class));

        UserTrades result = client.getUserTrades(tradesRequest);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat(ids, containsInAnyOrder("1", "2", "3", "4", "5"));
    }

    @Test
    public void testGetTransfersDeltaWhenNoMoreData()
        throws Exception
    {
        PowerMockito.when(Transfer.equalsById(any(), any())).thenReturn(false, false, true);
        when(transfers1.getList()).thenReturn(makeTransferFixtures("1", "2", "3"));
        when(transfers1.isEmpty()).thenReturn(false);
        when(transfers1.getLatest()).thenReturn(mock(Transfer.class));
        when(transfers2.getList()).thenReturn(makeTransferFixtures("4", "5"));
        when(transfers2.isEmpty()).thenReturn(false);
        when(transfers2.getLatest()).thenReturn(mock(Transfer.class));
        when(transfers3.getList()).thenReturn(makeTransferFixtures());
        when(transfers3.isEmpty()).thenReturn(true);
        doReturn(transfers1, transfers2, transfers3).when(client).getTransfers(any(GetTransfersRequest.class));

        Transfers result = client.getTransfers(transferRequest);
        List<String> ids = extractIds(result);
        log.debug("{}", ids);

        assertThat(ids, containsInAnyOrder("1", "2", "3", "4", "5"));
    }
}
